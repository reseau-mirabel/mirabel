#!/bin/bash

set -o pipefail -o noclobber -o nounset

LONGOPTS="no-files"
OPTIONS=""
PARSED=$(getopt --options="$OPTIONS" --longoptions="$LONGOPTS" --name "$0" -- "$@")
if [[ $? -ne 0 ]]; then
	exit 1
fi
# Read getopt's output.
eval set -- "$PARSED"
SYNC_CODE=1
SYNC_FILES=1
while true; do
    case "$1" in
        --no-code)
            SYNC_CODE=0
            shift
            ;;
        --no-files)
            SYNC_FILES=0
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

if [[ "$#" -ne 1 ]]; then
	echo "synchro.sh [--no-code] [--no-files] dbname_to_fill"
	exit 2
fi

DBTOFILL="$1"
TODAY="$(date +'%Y-%m-%d')"
DBDUMPSYNC="${HOME}/sql/mirabel2_synchro_${TODAY}.sql.gz"
DBPROD="mirabelv2_prod"

if [[ "$DBTOFILL" == "$DBPROD" ]]; then
	echo "Cannot sync into the same DB."
	exit 1
fi

if [[ -t 0 ]]; then
	# STDIN is a terminal
	read -p "Synchroniser l'instance courante, base ${DBTOFILL}? (y/n) " -n 1 -r
	echo ""
	if [[ ! $REPLY =~ ^[Yy]$ ]]; then
		echo "Abandon."
		exit 1;
	fi
fi

# DB
if [ -t 1 ] ; then
	# STDOUT is a terminal
	echo "Sync DB..."
fi
if [[ ! -e "${DBDUMPSYNC}" ]]; then
	{
		mysqldump --single-transaction --quick --skip-lock-tables --skip-add-locks --ignore-table="${DBPROD}.LogExt" "${DBPROD}"
		printf "\n\n-- ### Force some non-production config ###\n"
		echo "UPDATE Config SET value = 'debug' WHERE \`key\` = 'email.host';"
		echo "UPDATE CronTask SET active = 0;"
		echo "-- UPDATE Config SET value = 1 WHERE \`key\` = 'maintenance.cron.arret';"
	} | gzip > "${DBDUMPSYNC}"
fi
zcat "${DBDUMPSYNC}" | mysql "$DBTOFILL"

perl -i -pe "s/'dataVersion' => '[^']*',/'dataVersion' => '${TODAY}',/" ./src/www/protected/config/local.php

# data
if [[ "$SYNC_FILES" -eq 1 ]]; then
	if [ -t 1 ] ; then
		echo "Sync files..."
	fi
	rsync --quiet -a --delete "${HOME}"/mirabel-prod/data/images data/
	rsync --quiet -a --delete "${HOME}"/mirabel-prod/data/public data/
	rsync --quiet -a --delete "${HOME}"/mirabel-prod/data/upload data/
	cp non-officielle.png src/www/images/
fi

# source code
if [[ "$SYNC_CODE" -eq 1 ]]; then
	if [ -t 1 ] ; then
		echo "Sync source code..."
	fi
	sleep 1
	git clean --force --quiet -- src/www/images/liens
	git pull --ff-only -q
fi
