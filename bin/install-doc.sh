#!/bin/bash

DIR="$(php --run 'define("YII_ENV", "prod"); define("DATA_DIR", getcwd()); $c = array_merge_recursive(require "src/www/protected/config/main.php", require "src/www/protected/config/local.php"); echo $c["params"]["doc-path"];')"
REPO="${1:-git@gitlab.com:reseau-mirabel/mirabel-doc-interne.git}"

cd "${0%/*}" || exit
cd ..

if [ ! -d "$(dirname "$DIR")" ]; then
	echo "Le répertoire parent de $DIR n'existe pas."
	exit 1
fi
if [ -d "$DIR" ]; then
	if [[ -n "$(find "$DIR" -maxdepth 0 -empty)" ]]; then
		# Empty dir
		git clone "$REPO" "$DIR"
		exit
	fi
	if [[ -d "$DIR/.git" ]]; then
		# Already a git dir
		cd "$DIR" || exit 1
		echo "git pull --ff-only"
		git pull --ff-only --quiet
		exit
	fi
	echo "$DIR/ existe déjà et n'est pas vide."
	exit 2
fi

git clone "$REPO" "$DIR"

