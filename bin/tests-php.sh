#!/bin/bash

# Stop if a command fails
set -eu

[[ -e "./manticore.env" ]] && source manticore.env
require_env () {
	for var in "$@" ; do
		if [[ -n ${!var:-} ]]; then
			return
		fi
	done
	echo "$* n'est pas défini."
	echo "Créez un fichier manticore.env ou passez des variables d'environnement."
	exit 1
}
require_env "MIRABEL_MANTICORE_CONFIG"
require_env "MIRABEL_MANTICORE_DIR"
require_env "MIRABEL_MANTICORE_SOCKET" "MIRABEL_MANTICORE_PORT"
if [[ -v MIRABEL_MANTICORE_PORT && -n "$MIRABEL_MANTICORE_PORT" ]] ; then
	MIRABEL_MANTICORE_SERVER="${MIRABEL_MANTICORE_SERVER:-localhost}"
fi

if [ "${1-}" = "--help" ] || [ "${1-}" = "help" ]; then
	cat << EOT
## Examples

full:
    bin/tests-php.sh

fast with stop at first error:
    bin/tests-php.sh -f

single test:
    bin/tests-php.sh -f -- functional site/SiteCest
    bin/tests-php.sh -- unit-with-yii TitresPublishersTest
    bin/tests-php.sh -- unit FileStoreTest

tests from a directory:
    bin/tests-php.sh -- functional grappe

full with coverage:
    bin/tests-php.sh --coverage

See \`./vendor/bin/codecept help run\` for more.
EOT
	exit 0
fi

reset_test_db () {
	# MySQL: load data (test database must exist, and current user access it directly)
	php src/www/protected/yiic_test.php tests reset --interactive=0
	# MySQL: sync the PHP application
	php src/www/protected/yiic_test.php migrate --interactive=0

	# Sphinx: configure
	php src/www/protected/yiic_test.php sphinx confLocal > "$MIRABEL_MANTICORE_CONFIG"
	php src/www/protected/yiic.php sphinx conf --verbose=0 >> "$MIRABEL_MANTICORE_CONFIG" || \
		php src/www/protected/yiic_test.php sphinx confCommon --verbose=0 >> "$MIRABEL_MANTICORE_CONFIG"

	cd "$MIRABEL_MANTICORE_DIR" || exit 1
	if [[ -n "$MIRABEL_MANTICORE_SOCKET" ]] ; then
		[[ -S "$MIRABEL_MANTICORE_SOCKET" ]]
		MANTICORE_IS_ACTIVE=$?
	else
		exec 6<>"/dev/tcp/$MIRABEL_MANTICORE_SERVER/MIRABEL_MANTICORE_PORT"
		MANTICORE_IS_ACTIVE=$?
	fi

	if [[ ! -e indexes/test_titres.spa ]] ; then
		# Sphinx: index if test data never was indexed
		if [[ -n "$MIRABEL_MANTICORE_SOCKET" ]] && [[ -S "$MIRABEL_MANTICORE_SOCKET" ]]; then
			indexer --config "$MIRABEL_MANTICORE_CONFIG" --quiet --all --rotate
		elif exec 6<>"/dev/tcp/$MIRABEL_MANTICORE_SERVER/MIRABEL_MANTICORE_PORT" ; then
			indexer --config "$MIRABEL_MANTICORE_CONFIG" --quiet --all --rotate
		else
			indexer --config "$MIRABEL_MANTICORE_CONFIG" --quiet --all
			echo "Le démon searchd n'est pas accessible."
			echo "Manticore doit être actif pour lancer les tests."
			exit 1
		fi
	else
		# Sphinx: refresh index
		if [[ -n "$MIRABEL_MANTICORE_SOCKET" ]] && [[ -S "$MIRABEL_MANTICORE_SOCKET" ]]; then
			indexer --config "$MIRABEL_MANTICORE_CONFIG" test_editeurs test_titres_full test_titres_delta test_ressources --quiet --rotate
		elif exec 6<>"/dev/tcp/$MIRABEL_MANTICORE_SERVER/MIRABEL_MANTICORE_PORT" ; then
			indexer --config "$MIRABEL_MANTICORE_CONFIG" test_editeurs test_titres_full test_titres_delta test_ressources --quiet --rotate
		else
			echo "Le démon searchd n'est pas accessible."
			echo "Manticore doit être actif pour lancer les tests."
			exit 1
		fi
	fi
	cd -
}

if [ "${1-}" = "--reset-after" ]; then
	shift
	MODE="post"
elif [ "${1-}" = "--coverage" ]; then
	MODE="coverage"
else
	MODE="pre"
fi

# Delete artefacts from a previous run
rm -f tests/_output/*.html

# Delete the application cache (esp. MySQL structure cache)
rm -fr data-tests
mkdir data-tests

if [ "$MODE" != "post" ]; then
	reset_test_db
fi

# stop at first error
set -eu
set -o pipefail

# run the PHP tests
export MIRABEL_MANTICORE_SOCKET
if [ "$MODE" = "coverage" ]; then
	# Use pcov if available (much faster), else xdebug.
	XDEBUG+=()
	if [ "$(php -m | grep -ci pcov)" = "0" ]; then
		EPATH=$(php -i | grep -E '^extension_dir =')
		EPATH="${EPATH##* }"
		XDEBUG=( "-z" "$EPATH/xdebug.so" "-d" extension=xdebug.so "-d" xdebug.default_enable=1 "-d" xdebug.mode=coverage )
	fi
	php "${XDEBUG[@]}" ./vendor/bin/codecept run --html --coverage-html
else
	php ./vendor/bin/codecept run "$@"
fi

if [ "$MODE" = "post" ]; then
	reset_test_db > /dev/null
fi
