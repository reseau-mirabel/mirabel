#!/bin/zsh
zparseopts -D -E -- -ids=ids h=help -help=help

if [[ -n $help ]]; then
	echo "./prod.sh [-h|--help] [--ids] [branch|commit-hash]"
	exit
fi

UPTO="$1"
if [[ -z $UPTO ]]; then
	UPTO="master"
fi

echo "Analyse les commits de *$UPTO* qui ne sont pas dans *production*.
Vérifier que la branche 'production' est à jour:
	git fetch origin production:production"

local noticket=$(git log --oneline --no-merges --cherry-pick --right-only production...$UPTO | grep -v "M#")
local numnoticket=$(echo -n "$noticket" | grep -c '^')
if [[ $numnoticket -gt 0 ]] ; then
	printf "\n\033[39;1m## Commits sans référence à un ticket (%d)\033[0m\n" $numnoticket
fi
if [[ $numnoticket -gt 12 ]] ; then
	echo "git log --color --oneline --no-merges --cherry-pick --right-only production...$UPTO | grep -v "M#""
elif [[ $numnoticket -gt 0 ]] ; then
	echo "$noticket"
fi

local withticket=$(git log --oneline --no-merges --cherry-pick --right-only --grep "M#" production...$UPTO)
local numwithticket=$(echo -n "$withticket" | grep -c '^')
printf "\n\033[39;1m## Commits associés à un ticket (%d)\033[0m\n" "$numwithticket"
if [[ $numwithticket -gt 12 ]] ; then
	echo "git log --oneline --no-merges --cherry-pick --right-only --grep "M#" production...$UPTO"
else
	echo "$withticket"
fi

local -a list=($(git log --oneline --no-merges --cherry-pick --right-only production...$UPTO | perl -ne 'print "$1\n" if /M#(\d+)/' | sort -n | uniq))
printf "\n\033[39;1m## Tickets ayant des commits pas en production (%d)\033[0m\n" "${#list}"
echo "${(pj:  :)list}"

printf "\n\033[39;1m## Commande pour fusionner\033[0m\n"
local numjschanges=$(git log --oneline --cherry-pick --right-only production...$UPTO -- src/js | grep -c '^')
if [[ $numjschanges -gt 0 ]] ; then
	echo "rm -f src/www/protected/widgets/assets/dist/*.js"
fi
printf "git switch production && git pull --ff-only && "
if [[ ${#list[@]} -lt 4 ]] ; then
	printf "git merge --log --no-ff -m 'Fusion. Ferme #%s' $UPTO" "${(pj: #:)list}"
else
	printf "git merge --log --no-ff -m 'Fusion. Ferme %d tickets' -m 'Tickets %s' $UPTO" "${#list[@]}" "${(pj: :)list}"
fi
if [[ $numjschanges -gt 0 ]] ; then
	printf "\npnpm build && git ci -m 'JS compilé' src/www/protected/widgets/assets/dist/\n"
else
	printf " && "
fi
printf "git log --oneline -n2 && git push && git switch -\n"

printf "\n\033[39;1m## Procédure\033[0m
1. Vérifier que les commits sans ticket explicite ne référencent pas implicitement des tickets.
2. Vérifier dans Mantis que les tickets concernés sont terminés (validés ?) ou sûrs.
3. Fermer dans Mantis les tickets vus à l'étape précédente (liste dev-pas-en-prod ?).
4. Fusionner les branches avec la commande git ci-dessus (modifier le commentaire ou la branche).
5. Sauvegarder la base de données de production. (mysqldump)
6. Déployer (script ou manuellement par ssh+git).
7. Décrire la montée de version dans https://tickets.silecs.info/view.php?id=1875

"

local sphinxchanges=$(git log --oneline --cherry-pick --right-only production...$UPTO -- src/www/protected/commands/views)
local numsphinxchanges=$(echo -n "$sphinxchanges" | grep -c '^')
if [[ $numsphinxchanges -gt 0 ]] ; then
	printf "- \033[39;1mReconfigurer Manticore\033[0m en utilisant ~/bin/manticore-create-conf.sh (%d modifs) :\n" "$numsphinxchanges"
	echo "$sphinxchanges"
fi
