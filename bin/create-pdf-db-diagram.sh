#!/bin/bash

set -eu

echo "Dans DBeaver, exporter chaque diagramme dans un fichier PNG,"
echo " sous les noms '1.png' '2.png'..."

optipng ./?.png
img2pdf --output doc/online/Mirabel_SQL-diagram.pdf \
	--pagesize "A4^T" --fit shrink \
	--title "Mir@bel : diagrammes SQL" --author "Mir@bel" --creator "Mir@bel" \
	--viewer-page-layout single --viewer-magnification fit \
   	./?.png
