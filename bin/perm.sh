#!/bin/sh

cd "${0%/*}"
cd ..

# Some directories will be used RW from the web, some are shared web/cli.
chgrp --quiet -R www-data src/www/assets src/www/protected/data src/www/protected/runtime
chmod --quiet -R g=u src/www/assets src/www/protected/data src/www/protected/runtime

if [ ! -f src/www/protected/config/local.php ] ; then
	cp -i src/www/protected/config/local.dist.php src/www/protected/config/local.php
	echo "Please configure src/www/protected/config/local.php"
fi
