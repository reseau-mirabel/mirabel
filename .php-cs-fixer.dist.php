<?php

// php-cs-fixer fix --allow-risky=yes 

/*
 * Most of this settings come from the rule set @PhpCsFixer:
 * https://github.com/FriendsOfPHP/PHP-CS-Fixer/blob/master/doc/ruleSets/PhpCsFixer.rst
 * A few are from @Symfony.
 *
 * The main change is the indentation with \t.
 */

$finder = PhpCsFixer\Finder::create()
	->in("src/www/protected")
	->notPath('migrations/template.php')
	->exclude('runtime')
	->exclude('views')
;

$config = new \PhpCsFixer\Config('yii1');
return $config
	->setRules([
		'@PSR2' => true,
		'@PHP73Migration' => true,
		'align_multiline_comment' => true,
		'array_indentation' => true,
		'array_syntax' => ['syntax' => 'short'],
		'cast_spaces' => ['space' => 'single'],
		'class_attributes_separation' => true,
		'clean_namespace' => true,
		'concat_space' => ['spacing' => 'one'],
		'dir_constant' => true,
		'echo_tag_syntax' => ['format' => 'short'],
		'explicit_indirect_variable' => true, // ${$a} $x->{$a}
		'type_declaration_spaces' => true,
		'heredoc_indentation' => false,
		'is_null' => true,
		'method_argument_space' => ['on_multiline' => 'ignore'],
		'method_chaining_indentation' => true,
		'native_function_casing' => true,
		'native_type_declaration_casing' => true,
		'no_superfluous_elseif' => true,
		'no_useless_else' => true,
		'no_useless_return' => true,
		'no_whitespace_in_blank_line' => true,
		'normalize_index_brace' => true,
		'object_operator_without_whitespace' => true,
		'ordered_class_elements' => true, // traits, then public properties, etc.
		'phpdoc_order' => true,
		//'phpdoc_separation' => true,
		'phpdoc_scalar' => true,
		'return_assignment' => true,
		'simple_to_complex_string_variable' => true,
		'single_blank_line_at_eof' => true,
		'single_space_around_construct' => true,
		'trailing_comma_in_multiline' => true,
		'unary_operator_spaces' => true,
		'whitespace_after_comma_in_array' => true,
	])
	->setIndent("\t")
	->setLineEnding("\n")
	->setFinder($finder)
;
