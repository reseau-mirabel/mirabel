Mir@bel v2
==========

La plateforme Mir@bel permet de mutualiser le signalement des accès en ligne aux revues :
<https://reseau-mirabel.info> — voir les
[conditions d'utilisation](https://reseau-mirabel.info/site/page/conditions-utilisation)
et [*release notes*](https://reseau-mirabel.info/site/page/changelog)

Les données de Mir@bel peuvent être intégrées à Koha via le [plugin Koha développé par Tamil](https://github.com/fredericd/Koha-Plugin-Tamil-Mirabel)
qui utilise l'API de Mir@bel.

Ce code source est sous licence libre [GNU Affero](https://www.gnu.org/licenses/agpl-3.0.en.html).

Préalables
----------

- Linux, Nginx ou Apache, MariaDB
- PHP 8.2+
- [PHP composer](https://getcomposer.org)

Cf <INSTALL.md> pour les détails.

Développement
-------------

Cf <doc/README.md>.
