<?php

namespace tests\functional\cron;

class CreateLinkCest
{
	private array $data;

	public function __construct()
	{
		$this->data = [
			'name' => "MIAR",
			'description' => "Ceci décrit un import de liens MIAR",
			'fqdn' => '\\' . \processes\cron\tasks\ModelLinkImport::class,
			'active' => "0",
			'config' => [
				'source' => "miar",
				'obsoleteDelete' => "",
				'obsoleteSince' => "15 days ago",
				'url' => "",
				'verbose' => "Normal",
			],
			'schedule' => [
				'periodicity' => 'monthly',
				'hours' => ['02:24'],
				'days' => [15],
			],
			'emailTo' => "",
			'emailSubject' => "",
			'timeLimit' =>  0,
		];
	}

	public function create(\FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/cron/create');

		$I->stopFollowingRedirects();
		$I->sendPostRequest('/cron/create', ['body' => json_encode($this->data)]);
		$I->seeCurrentUrlEquals('/cron/create');
		$I->seeElement('.alert-error');

		$this->data['config']['url'] = "https://mirabel.localhost/path/to/miar.csv";
		$I->sendPostRequest('/cron/create', ['body' => json_encode($this->data)]);
		$I->seeFlashMessage('success');

		$I->followRedirect();
		$I->amOnPage($I->grabFromCurrentUrl()); // reload the current page
		$I->see($this->data['name'], 'h1 em');
	}
}
