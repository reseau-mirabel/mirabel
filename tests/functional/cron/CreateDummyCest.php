<?php

namespace tests\functional\cron;

class CreateDummyCest
{
	private array $data;

	public function __construct()
	{
		$this->data = [
			'model' => 1,
			'name' => "Création 1",
			'description' => "",
			'fqdn' => '\\' . \processes\cron\tasks\ModelSleep::class,
			'active' => true,
			'config' => ['sleep' => 94],
			'schedule' => [
				'periodicity' => 'daily',
				'hours' => ['00:4'],
				'days' => [2,4],
			],
			'emailTo' => "",
			'emailSubject' => "",
			'timeLimit' =>  0,
		];
	}

	public function create(\FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/cron/create');
		$I->seeElement('#task-form-content');
		$I->seeElement('script[type="application/json"]');
		$I->see("Ne fait rien.", 'script');
		$json = $I->grabTextFrom('script[type="application/json"]');
		$I->assertArrayHasKey('models', json_decode($json, true));

		$I->stopFollowingRedirects();
		$I->sendPostRequest('/cron/create', ['body' => json_encode($this->data)]);
		$I->seeCurrentUrlEquals('/cron/create');
		$I->seeElement('.alert-error');

		$this->data['schedule']['hours'] = ['00:14'];
		$I->sendPostRequest('/cron/create', ['body' => json_encode($this->data)]);
		$I->seeFlashMessage('success');
		$I->followRedirect();
		$I->amOnPage($I->grabFromCurrentUrl()); // reload the current page
		$I->see($this->data['name'], 'h1 em');

		$this->update($I);
	}	

	public function updateError(\FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->expectThrowable(\CHttpException::class, function() use($I) {
			$I->amOnPage('/cron/update/999');
		});
	}

	private function update(\FunctionalTester $I): void
	{
		$I->click("Modifier");
		$json = $I->grabTextFrom('script[type="application/json"]');
		$dec = json_decode($json, true);
		$I->assertSame($this->data['schedule']['hours'], $dec['state']['schedule']['hours']);
	}
}
