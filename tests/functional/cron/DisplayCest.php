<?php

namespace tests\functional\cron;

class DisplayCest
{
	public function index(\FunctionalTester $I)
	{
		self::setup();
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/cron/index');
		$I->seeNumberOfElements('#cron-tasks-grid tbody tr', 1);
		$I->dontSeeElement('#cron-tasks-grid tbody tr td.empty');

		$I->amOnPage('/cron/index?q[lastRunMessage]=erreur');
		$I->seeNumberOfElements('#cron-tasks-grid tbody tr', 1);
		$I->seeElement('#cron-tasks-grid tbody tr td.empty');

		$I->amOnPage('/cron/index?q[fqdn]=AttributImport');
		$I->seeNumberOfElements('#cron-tasks-grid tbody tr', 1);
		$I->seeElement('#cron-tasks-grid tbody tr td.empty');

		$I->amOnPage('/cron/index?q[fqdn]=Sleep');
		$I->seeNumberOfElements('#cron-tasks-grid tbody tr', 1);
		$I->dontSeeElement('#cron-tasks-grid tbody tr td.empty');
	}

	public function indexCalendar(\FunctionalTester $I)
	{
		self::setup();
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/cron/index');
		$I->click("aujourd'hui", '.pull-right');
		$I->seeCurrentUrlMatches('#^/cron/index-calendar#');
		$I->seeNumberOfElements('#today.calendar-day .run', 1);
	}

	public function view(\FunctionalTester $I)
	{
		self::setup();
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/cron/index');
		$I->seeNumberOfElements('#cron-tasks-grid tbody tr', 1);

		$I->click("Test d'attente", '#cron-tasks-grid');
		$I->seeCurrentUrlEquals('/cron/1');
		$I->see("Test d'attente", 'h1');
		$I->see("0 secondes");
		$I->see("Chaque jour, à 12:07", '.schedule');
	}

	public function viewError(\FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->expectThrowable(\CHttpException::class, function() use($I) {
			$I->amOnPage('/cron/999');
		});
	}

	private static function setup()
	{
		$task = new \processes\cron\ar\Task();
		$task->setAttributes(
			[
				'id' => 1,
				'name' => "Test d'attente",
				'description' => 'à supprimer après usage',
				'fqdn' => '\\' . \processes\cron\tasks\ModelSleep::class,
				'config' => '{"sleep":0}',
				'schedule' => '{"periodicity":"daily","hours":["12:07"],"days":[0,1,2,3,4,5,6]}',
				'active' => true,
				'emailTo' => "bibi@here.localhost\nCe Clampin ce.clampin@truc.localhost",
				'emailSubject' => 'dormir',
				'timelimit' => 10,
				'lastUpdate' => time(),
			],
			false
		);
		$task->save();
	}
}
