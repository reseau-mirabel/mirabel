<?php

namespace tests\functional\cron;

class CreateKbartCest
{
	private array $data;

	public function __construct()
	{
		$this->data = [
			'name' => "KBART 1",
			'description' => "Ceci décrit l'import <script>KBART",
			'fqdn' => '\\' . \processes\cron\tasks\ModelServiceKbart::class,
			'active' => true,
			'config' => [
				'ressourceId' => "143",
				'collectionIds' => "17",
				'url' => "",
				'contenu' => "",
				'acces' => "",
				'ssiConnu' => "1",
				//'ignoreUrl' => "1",
				//'verbose' => 1,
			],
			'schedule' => [
				'periodicity' => 'monthly',
				'hours' => ['02:24'],
				'days' => [1],
			],
			'emailTo' => "someone@mirabel.localhost",
			'emailSubject' => "KBART de test",
			'timeLimit' =>  0,
		];
	}

	public function create(\FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/cron/create');

		$I->stopFollowingRedirects();
		$I->sendPostRequest('/cron/create', ['body' => json_encode($this->data)]);
		$I->seeCurrentUrlEquals('/cron/create');
		$I->see("Le formulaire est incomplet", '.alert-error');

		$this->data['config']['url'] = "https://mirabel.localhost/path/to/kbart.txt";
		$I->sendPostRequest('/cron/create', ['body' => json_encode($this->data)]);
		$I->seeCurrentUrlEquals('/cron/create');
		$I->seeElement('.alert-error');

		$this->data['config']['collectionIds'] = "67 ";
		$I->sendPostRequest('/cron/create', ['body' => json_encode($this->data)]);
		$I->seeFlashMessage('success');

		$I->followRedirect();
		$I->amOnPage($I->grabFromCurrentUrl()); // reload the current page
		$I->see($this->data['name'], 'h1 em');

		$this->delete($I);
	}

	private function delete(\FunctionalTester $I)
	{
		$I->click("Supprimer", '.btn-danger');
		$I->seeFlashMessage('success');
		$I->followRedirect();
		$I->seeCurrentUrlEquals('/cron/index');

		$I->expectThrowable(\CHttpException::class, function() use($I) {
			$I->sendPostRequest('/cron/delete/999', []);
		});
	}
}
