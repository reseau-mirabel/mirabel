<?php

class ImportKbartCest
{
	public function importKnown(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");

		$I->amOnPage('/import/kbart0');
		$I->attachFile("Fichier KBART local", 'kbart_cairn.txt');
		$I->fillField('ImportKbart0[ressourceId]', 3);
		// Pas de collection !
		$I->click('.form-actions button[type="submit"]');
		$I->seeElement('.alert-error');

		$I->attachFile("Fichier KBART local", 'kbart_cairn.txt');
		$I->checkOption("Bouquet Humanities and Social Science (courant)");
		$I->checkOption("Bouquet Humanités (courant)");
		$I->checkOption("Bouquet Général (courant)");
		$I->click('.form-actions button[type="submit"]');

		$I->see("Vérifications sur le contenu", 'h2');
		$I->seeNumberOfElements('#import-read tbody tr.success', 4);
		$I->see("Doublon de la ligne 3 pour les colonnes 'title, accessType'");
		$I->seeElement('#ImportForm_diffusion[disabled]');
		$I->seeElement('#ImportForm_contenu[disabled]');
		$I->click('.form-actions button[type="submit"]');

		$I->see("Titres connus", 'h2');
		$I->seeNumberOfElements('#kbart-known-journals tbody tr', 4);
		$I->see("1 lignes où les titres sont inconnus de Mir@bel.");
		$I->see("Afrique Contemporaine", '#kbart-known-journals tbody tr');
		$hash = $I->grabFromCurrentUrl('#\?hash=(\w+)$#');

		$I->amOnPage("/import/journal-details?hash=$hash&id=2&defaults={\"lacunaire\":0,\"selection\":0}");
		$I->see("Données KBART", 'h3');
		$I->see("0002-0478");
		$I->seeNumberOfElements('#kbart-data tbody tr', 2);
		$I->seeNumberOfElements('#access-existing tbody tr', 2);
		$I->seeNumberOfElements('#access-creation tbody tr', 1);
		$I->seeNumberOfElements('#access-update tbody tr', 2);
		$I->moveBack();

		$I->click('.form-actions button[type="submit"]');
		$I->see("Interventions", 'h2');
		$I->seeNumberOfElements('#interventions tbody tr.success', 4);
		$I->seeNumberOfElements('#interventions tbody tr.error', 0);
	}

	public function importUnknown(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");

		$I->amOnPage('/import/kbart0');
		$I->attachFile("Fichier KBART local", 'kbart_2rows.txt');
		$I->fillField('ImportKbart0[ressourceId]', 144);
		// Pas de collection !
		$I->click('.form-actions button[type="submit"]');
		$I->seeElement('.alert-error');

		$I->attachFile("Fichier KBART local", 'kbart_2rows.txt');
		$I->checkOption("Licence Nationale France (licence nationale France)");
		$I->click('.form-actions button[type="submit"]');

		$I->see("Vérifications sur le contenu", 'h2');
		$I->see("La date '21/01/2012' n'est pas au format ISO.", 'tr.warning');
		$I->selectOption('Accès par défaut', "Libre");
		$I->seeElement('#ImportForm_contenu[disabled]');
		$I->click('.form-actions button[type="submit"]');

		$I->see("Titres connus", 'h2');
		$I->seeNumberOfElements('#kbart-known-journals tbody tr', 0);
		$I->see("2 lignes où les titres sont inconnus de Mir@bel.");
		$I->see("Journal of classics teaching", '#kbart-unknown-journals tbody tr');
		$hash = $I->grabFromCurrentUrl('#\?hash=(\w+)$#');

		$I->expectThrowable(\CHttpException::class, function() use($I, $hash) {
			$I->amOnPage("/import/journal-details?hash=$hash&id=622&defaults={\"lacunaire\":0,\"selection\":0}");
		});
	}

	public function importError(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");

		$I->amOnPage('/import/kbart0');
		$I->attachFile("Fichier KBART local", 'kbart_2rows.txt');
		$I->fillField('ImportKbart0[ressourceId]', 99999);
		$I->expectThrowable(\CHttpException::class, function() use($I) {
			$I->click('.form-actions button[type="submit"]');
		});

		$I->expectThrowable(\CHttpException::class, function() use($I) {
			$I->amOnPage('/import/kbart1');
		});

		$I->expectThrowable(\CHttpException::class, function() use($I) {
			$I->amOnPage('/import/kbart1?hash=x');
		});
	}
}
