<?php

namespace tests\functional\site;

use FunctionalTester;

class ActualiteCest
{
	public function general(FunctionalTester $I)
	{
		$I->amOnPage('/site/actualite');
		$I->see("L'actualité du réseau Mir@bel", 'h1');
		$I->seeElement('h1 > a', ['href' => '/rss/breves']);
		$I->seeNumberOfElements('.breves > li', 9);
		$I->dontSeeLink("plus récentes…");
		$I->dontSeeLink("plus anciennes…");
	}

	public function filtreCategorie(FunctionalTester $I)
	{
		$I->amOnPage(\Yii::app()->createAbsoluteUrl('/site/actualite', ['q' => ['categorie' => 'Partenariats']]));
		$I->see("L'actualité du réseau Mir@bel", 'h1');
		$I->see("Partenariats", 'h1');
		$I->see("Partenariats", 'h2');
		$I->dontSeeElement('h1 > a', ['href' => '/rss/breves']);
		$I->seeNumberOfElements('.breves > li', 6);
		$I->dontSeeLink("plus récentes…");
		$I->dontSeeLink("plus anciennes…");
	}

	public function filtreLien(FunctionalTester $I)
	{
		$I->amOnPage(\Yii::app()->createAbsoluteUrl('/site/actualite', ['q' => ['sourcelienId' => '1']]));
		$I->see("L'actualité du réseau Mir@bel", 'h1');
		$I->see("DOAJ", 'h1');
		$I->see("DOAJ", 'h2');
		$I->dontSeeElement('h1 > a', ['href' => '/rss/breves']);
		$I->seeNumberOfElements('.breves > li', 0);
	}

	public function filtrePartenaire(FunctionalTester $I)
	{
		$I->amOnPage('/partenaire/1');
		$I->dontSee("Actualité", 'h2');

		$I->amOnPage('/partenaire/2');
		$I->dontSee("Actualités", 'h2'); // seulement sur la page authentifiée
		$I->seeElement('.breves-titres');
		$I->seeNumberOfElements('.breves-titres .actu-titre', 1);
		$I->dontSee("Toute l'actualité"); // pas assez de brève pour le lien

		$I->amOnPage(\Yii::app()->createAbsoluteUrl('/site/actualite', ['q' => ['partenaireId' => '2']]));
		$I->see("L'actualité du réseau Mir@bel", 'h1');
		$I->see("Notre-Dame de Paris", 'h1');
		$I->see("partenaire Notre-Dame de Paris", 'h2');
		$I->dontSeeElement('h1 > a', ['href' => '/rss/breves']);
		$I->seeNumberOfElements('.breves > li', 1);
	}

	public function filtreRessource(FunctionalTester $I)
	{
		$I->amOnPage(\Yii::app()->createAbsoluteUrl('/site/actualite', ['q' => ['ressourceId' => '3']]));
		$I->see("L'actualité du réseau Mir@bel", 'h1');
		$I->see("Cairn", 'h1');
		$I->see("ressource Cairn", 'h2');
		$I->dontSeeElement('h1 > a', ['href' => '/rss/breves']);
		$I->seeNumberOfElements('.breves > li', 1);
	}
}
