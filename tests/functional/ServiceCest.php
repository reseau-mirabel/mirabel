<?php

class ServiceCest
{
	public function contact(FunctionalTester $I)
	{
		$I->amOnPage('/revue/366/Slence');
		$I->click('a[title="Proposer une modification via un formulaire de contact"]', '.service-4751');
		$I->see("Signaler un problème sur les accès importés", 'h1');
		$I->fillField('Code de vérification', "44");
	}

	public function createbyUpol(FunctionalTester $I)
	{
		$I->amLoggedInAs('u-pol');
		$I->amOnPage('/revue/366/Slence');
		$I->click('a[title="Proposer un nouvel accès"]');
		$I->see("Nouvel accès en ligne", 'h1');
		$I->see("Les modifications que vous proposez devront être validées", '.alert');
		$I->see("Proposer cette création", 'button');
		$I->logout();
	}

	public function createProposition(FunctionalTester $I)
	{
		$I->amOnPage('/revue/366/Slence');
		$I->click('a[title="Proposer un nouvel accès"]');
		$I->see("Nouvel accès en ligne", 'h1');
		$I->see("Les modifications que vous proposez devront être validées", '.alert');
		$I->see("Proposer cette création", 'button');
	}

	public function create(FunctionalTester $I)
	{
		$I->amLoggedInAs('milady');
		$I->amOnPage('/revue/366/Slence');
		$I->click('a[title="Nouvel accès"]', '#revue-acces .actions');
		$I->see("Nouvel accès en ligne", 'h1');
		$I->see("Les modifications seront appliquées immédiatement.", '.alert');
		$I->see("Créer", 'button');

		$I->stopFollowingRedirects();
		$I->submitForm('#service-form', [
			'Service[titreId]' => '366',
			'Service[ressourceId]' => '193',
			'Service[url]' => 'https://example.com/slence',
			'Service[type]' => 'Intégral',
			'Service[acces]' => 'Libre',
			'Service[dateBarrDebut]' => '1999-06',
			'Intervention[commentaire]' => "Ceci est mon commentaire.",
		]);
		$I->seeResponseCodeIsSuccessful();
		$I->seeCurrentUrlMatches('#/service/create#');
		$I->see("Aucune ressource ne correspond", '.alert-error');

		$I->submitForm('#service-form', [
			'Service[titreId]' => '366',
			'Service[ressourceId]' => '127',
			'Service[url]' => 'https://example.com/slence',
			'Service[type]' => 'Intégral',
			'Service[acces]' => 'Libre',
			'Service[lacunaire]' => '0',
			'Service[selection]' => '0',
			'Service[dateBarrDebut]' => '1999-06',
			'Intervention[commentaire]' => "Ceci est mon commentaire.",
		]);
		$I->seeResponseCodeIsRedirection();
		$I->seeFlashMessage('success', "Modification enregistrée");

		$this->delete($I);
	}

	public function createWithoutForceProposition(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/revue/6/Esprit');
		$I->click("Ajouter un accès en ligne", '#sidebar');
		$I->see("Nouvel accès en ligne", 'h1');
		$I->see("Les modifications que vous proposez devront être validées", '.alert');
		$I->see("Passer par l'état ", 'fieldset');
		$I->see("Proposer cette création", 'button');

		$I->stopFollowingRedirects();

		$I->submitForm('#service-form', [
			'force_proposition' => '0',
			'Service[titreId]' => '6',
			'Service[ressourceId]' => '296',
			'Service[url]' => 'https://example.com/esprit',
			'Service[type]' => 'Intégral',
			'Service[acces]' => 'Libre',
			'Service[dateBarrDebut]' => '1999-06',
			'Intervention[commentaire]' => "Ceci est mon commentaire.",
		]);
		$I->seeResponseCodeIsRedirection();
		$I->seeFlashMessage('success', "Modification enregistrée");
	}

	public function createWithForceProposition(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/service/create?revueId=6');
		$I->stopFollowingRedirects();
		$I->submitForm('#service-form', [
			'force_proposition' => '1',
			'Service[titreId]' => '6',
			'Service[ressourceId]' => '1518',
			'Service[url]' => 'https://example.com/esprit',
			'Service[type]' => 'Intégral',
			'Service[acces]' => 'Libre',
			'Service[dateBarrDebut]' => '1999-06',
			'Intervention[commentaire]' => "Ceci est mon commentaire.",
		]);
		$I->seeResponseCodeIsRedirection();
		$I->seeFlashMessage('success', "Proposition enregistrée");
	}

	private function delete(FunctionalTester $I)
	{
		$I->amOnPage('/revue/366/Slence');
		$I->see("Factiva", '#revue-acces tbody tr:first-of-type');
		$I->startFollowingRedirects();
		$I->click('#revue-acces tbody tr:first-of-type .service-delete');
		$I->seeResponseCodeIsSuccessful();
	}

	public function export(FunctionalTester $I)
	{
		$I->amOnPage('/service/export?Service[ressourceId]=3');
		$I->seeInSource('titre;ISSN;ISSN-E;ISSN-L;"URL Mir@bel";');
		$csv = $I->grabPageSource();
		$I->assertEquals(19, substr_count($csv, "\n"));

		$I->amOnPage('/service/export?Service[ressourceId]=3&format=kbart');
		$I->seeInSource("publication_title\tprint_identifier\tonline_identifier\t");
		$tsv = $I->grabPageSource();
		$I->assertEquals(19, substr_count($tsv, "\n"));

		$I->amOnPage('/service/export?Service[bacon]=global');
		$I->seeInSource("publication_title\tprint_identifier\tonline_identifier\t");
		$tsvBaconGlobal = $I->grabPageSource();
		$I->assertEquals(73, substr_count($tsvBaconGlobal, "\n"));

		$I->amOnPage('/service/export?Service[bacon]=libre');
		$I->seeInSource("publication_title\tprint_identifier\tonline_identifier\t");
		$tsvBaconLibre = $I->grabPageSource();
		$I->assertEquals(27, substr_count($tsvBaconLibre, "\n"));
	}

	public function update(FunctionalTester $I)
	{
		$id = 11394; // Service.id

		$I->amLoggedInAs('milady');
		$I->amOnPage('/revue/366/Slence');
		$I->see("S!lence", 'h1');
		$I->see("lacunaire", "tr.service-$id");
		$I->dontSee("2000-12-21", "tr.service-$id");
		$I->click('a[title="Modifier"]', "tr.service-$id");
		$I->seeInField("Adresse web", 'http://www.revuesilence.net/anciensnum');
		$I->seeCheckboxIsChecked('Couverture lacunaire');
		$I->fillField('Date de fin', '2002-12-21');

		$I->click('Modifier');
		$I->see("S!lence", 'h1');
		$I->see("décembre 2002", "tr.service-$id");
		$I->logout();
	}

	public function notfound(FunctionalTester $I)
	{
		$I->expectThrowable(
			new \CHttpException(404, "Cet accès en ligne n'a pas été trouvé."),
			function() use($I) {
				$I->amOnPage('/service/update/999999');
			}
		);
	}
}
