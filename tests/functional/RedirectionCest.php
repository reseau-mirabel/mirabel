<?php

class RedirectionCest
{
	public function redirections(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/redirection/index');
		$I->seeNumberOfElements('#editeurs table > tbody > tr', 1);
		$I->seeNumberOfElements('#titres table > tbody > tr', 1);
		$I->seeNumberOfElements('#revues table > tbody > tr', 1);
	}

	public function redirectionEditeur(FunctionalTester $I)
	{
		$I->amOnPage('/editeur/3'); // avec redirection
		$I->seeCurrentUrlMatches('#/editeur/19\b#');
		$I->see("Dalloz");
		$I->dontSee("L'éditeur demandé n'existe pas.");

		$I->stopFollowingRedirects();
		$I->amOnPage('/editeur/3'); // avec redirection
		$I->seeResponseCodeIs(301);
		$I->startFollowingRedirects();

		$I->expectThrowable(\CHttpException::class, function() use($I) {
			$I->amOnPage('/editeur/1'); // n'existe pas, sans redirection
		});
	}

	public function redirectionRevue(FunctionalTester $I)
	{
		$I->amOnPage('/revue/7'); // avec redirection
		$I->seeCurrentUrlMatches('#/revue/20\b#');
		$I->see("Recueil Dalloz");
		$I->dontSee("La revue demandée n'existe pas.");

		$I->stopFollowingRedirects();
		$I->amOnPage('/revue/7'); // avec redirection
		$I->seeResponseCodeIs(301);
		$I->startFollowingRedirects();

		$I->expectThrowable(\CHttpException::class, function() use($I) {
			$I->amOnPage('/revue/4'); // n'existe pas, sans redirection
		});
	}

	public function redirectionTitre(FunctionalTester $I)
	{
		$I->amLoggedInAs('milady');
		$I->amOnPage('/titre/view/4'); // avec redirection
		$I->seeCurrentUrlMatches('#titre/366$#');
		$I->see("Silence");

		$I->stopFollowingRedirects();
		$I->amOnPage('/titre/4'); // avec redirection
		$I->seeResponseCodeIs(301);
		$I->startFollowingRedirects();

		$I->expectThrowable(\CHttpException::class, function() use($I) {
			$I->amOnPage('/titre/10'); // n'existe pas, sans redirection
		});
	}
}
