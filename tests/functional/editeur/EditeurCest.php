<?php

namespace tests\functional\editeur;

use CHttpException;
use FunctionalTester;

class EditeurCest
{
	public function ajaxComplete(FunctionalTester $I)
	{
		$I->sendAjaxGetRequest('/editeur/ajax-complete', [
			'term' => 'institut',
		]);
		$response = json_decode($I->grabPageSource(), true);
		$I->assertIsArray($response);
		$I->assertCount(5, $response);
		$I->assertSame(20, $response[0]['id']);

		$I->comment("Search with no result");
		$I->sendAjaxGetRequest('/editeur/ajax-complete', [
			'term' => 'emberlificoter',
			'excludeIds' => 'plouf',
		]);
		$responseEmpty = json_decode($I->grabPageSource(), true);
		$I->assertCount(0, $responseEmpty);
	}

	public function ajaxCompleteById(FunctionalTester $I)
	{
		$I->comment("Search by ID");
		$I->sendAjaxGetRequest('/editeur/ajax-complete', [
			'term' => '5',
		]);
		$response = json_decode($I->grabPageSource(), true);
		$I->assertIsArray($response);
		$I->assertGreaterOrEquals(1, count($response));
		$I->assertSame(5, $response[0]['id']);
	}

	public function ajaxCompleteByIdref(FunctionalTester $I)
	{
		$I->comment("Search by Idref");
		$I->sendAjaxGetRequest('/editeur/ajax-complete', [
			'term' => '026485842', // idref of "Dalloz", ID 19
		]);
		$response = json_decode($I->grabPageSource(), true);
		$I->assertIsArray($response);
		$I->assertGreaterOrEquals(1, count($response));
		$I->assertSame(19, $response[0]['id']);
	}

	public function ajaxCompleteExact(FunctionalTester $I)
	{
		$I->comment("Search with the exact name");
		$I->sendAjaxGetRequest('/editeur/ajax-complete', [
			'term' => 'Larousse',
		]);
		$response = json_decode($I->grabPageSource(), true);
		$I->assertIsArray($response);
		$I->assertGreaterOrEquals(1, count($response));
		$I->assertSame("Larousse", $response[0]['label']);
	}

	public function ajaxCompleteWithExclusion(FunctionalTester $I)
	{
		$I->sendAjaxGetRequest('/editeur/ajax-complete', [
			'term' => 'institut',
			'excludeIds' => '2725',
		]);
		$source = $I->grabPageSource();
		$response = json_decode($source, true);
		$I->assertIsArray($response);
		$I->assertCount(4, $response);

		$I->sendAjaxGetRequest('/editeur/ajax-complete', [
			'term' => 'institut',
			'excludeIds' => '2725,0',
		]);
		$I->assertSame($source, $I->grabPageSource());

		$I->sendAjaxGetRequest('/editeur/ajax-complete', [
			'term' => 'institut',
			'excludeIds[]' => '2725',
		]);
		$I->assertSame($source, $I->grabPageSource());
	}

	public function carte(FunctionalTester $I)
	{
		$I->amOnPage('/editeur/carte');
		$I->see("Cette carte ne s'affiche pas si JavaScript est désactivé dans votre navigateur.", '.alert');
	}

	public function export(FunctionalTester $I)
	{
		$I->amOnPage('/editeur/export?criteria=' . rawurlencode('{"nom":"étude"}'));
		$all = array_filter(explode("\n", $I->grabPageSource()));
		$I->assertCount(3, $all); // 2 results + 1 header

		$I->amOnPage('/editeur/export?criteria=' . rawurlencode('{"pays":"62"}'));
		$part = array_filter(explode("\n", $I->grabPageSource()));
		$I->assertCount(23, $part);
	}

	public function exportIdref(FunctionalTester $I)
	{
		$I->amOnPage('/editeur/export-idref');
		$all = array_filter(explode("\n", $I->grabPageSource()));
		$I->assertCount(2, $all);
	}

	public function idrefHtml(FunctionalTester $I)
	{
		$I->expectThrowable(
			new CHttpException(404, "Aucun éditeur dans Mir@bel n'a cet IdRef."),
			function() use($I) {
				$I->haveHttpHeader("Accept", "text/html,application/xhtml+xml,*/*;q=0.8");
				$I->amOnPage('/editeur/idref?idref=123456789');
			}
		);

		$I->expectThrowable(
			new CHttpException(400, "Paramètre 'idref' non valide"),
			function() use($I) {
				$I->haveHttpHeader("Accept", "text/html,application/xhtml+xml,*/*;q=0.8");
				$I->amOnPage('/editeur/idref?idref=a-b');
			}
		);

		$I->stopFollowingRedirects();
		$I->haveHttpHeader("Accept", "text/html,application/xhtml+xml,*/*;q=0.8");
		$I->amOnPage('/editeur/idref?idref=026485842');
		$I->seeResponseCodeIsRedirection();
	}

	public function idrefJson(FunctionalTester $I)
	{
		$I->haveHttpHeader("Accept", "application/json,*/*;q=0.8");
		$I->sendAjaxGetRequest('/editeur/idref', ['idref' => '026485842,194736725']);
		$response = json_decode($I->grabPageSource(), false);
		$I->assertIsArray($response);
		$I->assertCount(1, $response);
		$I->assertSame(19, $response[0]->id);
	}

	public function pays(FunctionalTester $I)
	{
		$I->expectThrowable(
			new CHttpException(404, "Pays inconnu"),
			function() use($I) {
				$I->amOnPage("/editeur/pays/OOO");
			}
		);

		$I->startFollowingRedirects();
		$I->amOnPage('/editeur/pays/FRA');
		$I->see("22 éditeurs", '.summary');

		$I->amOnPage('/editeur/pays/DZA/Algerie');
		$I->see("Aucun résultat trouvé.");
	}

	public function search(FunctionalTester $I)
	{
		$I->amOnPage('/editeur/search');
		$I->see("Éditeurs", 'h1');
		$I->dontSeeElement('.grid-view');
		$I->dontSee("Critères", '#search-summary');

		$I->fillField('q[idref]', "! ");
		$I->click("Rechercher");
		$I->seeElement('.grid-view');
		$I->click("Suivant");
		$I->see("Résultats de 26 à", '.summary');

		$I->selectOption("Pays", "France");
		$I->click("Rechercher");
		$I->see("[sans idref] [pays : France]", '#search-summary');
		$I->see("Résultats de 1 à", '.summary');

		$I->amOnPage('/editeur/search');
		$I->fillField('q[nom]', "Gallimard ");
		$I->click("Rechercher");
		$I->see("Résultats de 1 à 1", '.summary');
	}

	public function verify(FunctionalTester $I)
	{
		$I->amLoggedInAs('frollo');
		$I->amOnPage("/editeur/331/Alternatives-economiques");
		$I->see("Alternatives économiques", 'h1');
		$derVerif = $I->grabTextFrom('p.verif');
		$I->see($derVerif, 'p.verif');
		$I->click("Indiquer que l'éditeur a été vérifié");
		$I->see("Alternatives économiques", 'h1');
		$I->dontSee($derVerif, 'p.verif');

		$I->expectThrowable(
			new CHttpException(404, "L'éditeur demandé n'existe pas."),
			function() use($I) {
				$I->amOnPage("/editeur/verify/9999999");
			}
		);
		$I->logout();

		$I->amLoggedInAs('editeur-98');
		$I->expectThrowable(
			new CHttpException(403, "Vous n'avez pas les droits nécessaires pour cette opération."),
			function() use($I) {
				$I->amOnPage("/editeur/verify/331");
			}
		);
		$I->logout();
		$I->amOnPage("/editeur/331");
	}

	public function view(FunctionalTester $I)
	{
		$I->amOnPage("/editeur/2847");
		$I->see("Courant alternatif", 'h1');
		$I->seeInSource('<meta property="og:description" name="og:description" content="Mirabel liste les accès en ligne aux revues de l&#039;éditeur Courant Alternatif" />');
		$I->seeInSource('<meta property="og:url" name="og:url" content="http://localhost/editeur/2847/Courant-alternatif" />');
		$I->dontSee("Politiques de publication", 'h2');

		$I->amLoggedInAs('frollo'); // perm editeurs
		$I->amOnPage("/editeur/2847");
		$I->see("Politiques de publication", 'h2');
		$I->seeLink("Gestion des politiques");
		$I->see("Aucun utlisateur de Mir@bel n'est responsable de déclarer la politique.", 'p');

		$I->logout('frollo');
		$I->amOnPage("/editeur/2847");
	}
}
