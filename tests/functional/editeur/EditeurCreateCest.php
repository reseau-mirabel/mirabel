<?php

namespace tests\functional\editeur;

use FunctionalTester;

class EditeurCreateCest
{
	public function createByAjax(FunctionalTester $I)
	{
		$I->sendAjaxPostRequest('/editeur/create-final', [
			'Editeur[nom]' => "Bidon",
			'Editeur[idref]' => "444",
		]);
		$error = json_decode($I->grabPageSource(), true);
		$I->assertNotNull($error);
		$I->assertEquals('error', $error['result']);
		$I->assertArrayNotHasKey('id', $error);

		$I->sendAjaxPostRequest('/editeur/create-final', [
			'Editeur[nom]' => "Bidon",
			'Editeur[prefixe]' => "",
			'Editeur[sigle]' => "",
			'Editeur[description]' => "",
			'Editeur[url]' => "",
			'Editeur[geo]' => "",
			'Editeur[liensJson]' => "",
		]);
		$success = json_decode($I->grabPageSource(), true);
		$I->assertNotNull($success, $I->grabPageSource());
		$I->assertEquals('success', $success['result']);
		$I->assertArrayHasKey('id', $success);

		// cleanup
		$I->amOnPage('/editeur/index');
	}

	public function createAsAuth(FunctionalTester $I)
	{
		$I->amLoggedInAs('frollo');
		$I->amOnPage('/editeur/create-final');
		$I->fillField("Nom", "Publieur");
		$I->fillField("Date de début", "12/1999");
		$I->fillField("Date de fin", "24/12/208");

		$transport = \components\email\Mailer::getTransport();
		$transport->reset();
		$I->assertEquals('suivi-editeurs@mirabel.localhost', \Config::read('suivi.editeurs.email'));

		$I->click("Créer cet éditeur");
		$I->seeElement('.alert-error');

		$I->fillField("Date de fin", "24/12/2008");
		$I->click("Créer cet éditeur");
		$I->seeElement('.alert-success');
		$I->see("Publieur", 'h1');
		$I->dontSee("1999 à 2008", 'h1');
		$I->see("Période d'activité");

		$I->assertEquals(0, $transport->getSentCount());
		$transport->reset();
	}

	public function createByIdref(FunctionalTester $I)
	{
		$I->amOnPage('/editeur/index');
		$I->click('a[title="Proposer un nouvel éditeur"]');
		$I->see("Nouvel éditeur", 'h1');
		$I->see("Créer via IdRef", '.breadcrumbs');
	}

	public function createDirectly(FunctionalTester $I)
	{
		$I->amOnPage('/editeur/create-final');
		$I->see("Nouvel éditeur", 'h1');
		$I->see("Proposer de créer cet éditeur", 'button');
		$currentUrl = $I->grabFromCurrentUrl();

		$I->amLoggedInAs('milady');
		$I->amOnPage('/editeur/create-final');
		$I->seeCurrentUrlEquals($currentUrl);

		$I->dontSee("Proposer de créer cet éditeur", 'button');
		$I->fillField("Nom", "éditions temporaires");
		$I->fillField("Préfixe", "Les ");
		$I->fillField("IdRef", "https://www.idref.fr/0298830");
		$I->fillField("ID Open policy finder", "https://v2.sherpa.ac.uk/id/publisher/7569");

		$I->startFollowingRedirects();
		$I->click("Créer cet éditeur");
		$I->see("Cet identifiant est mal-formé", '.alert');
		$I->see("Nouvel éditeur", 'h1');

		$I->fillField("IdRef", "https://www.idref.fr/029883091");
		$I->fillField("ROR", "https://ror.org/05f82e368");
		$I->click("Créer cet éditeur");
		$I->see("Les éditions temporaires", 'h1');
		$I->dontSee("Période d'activité");
		$I->seeElement('a[title="IdRef 029883091"]');
		$I->seeElement('a[title="Research Organization Registry 05f82e368"]');
		$id = (int) $I->grabFromCurrentUrl('#editeur/(\d+)/editions-temporaires#');
		$I->assertGreaterThan(1, $id);
	}
}
