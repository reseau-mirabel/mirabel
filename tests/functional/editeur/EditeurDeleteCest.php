<?php

namespace tests\functional\editeur;

use CHttpException;
use components\email\SentMessage;
use FunctionalTester;

class EditeurDeleteCest
{
	public function deleteMonitoredFails(FunctionalTester $I)
	{
		$editeurId = 10;
		$editeurName = "Gallimard";

		$I->comment("editeur-partenaire is not authorized to delete monitored publishers");
		$I->amLoggedInAs('editeur-98');
		$I->amOnPage("/editeur/$editeurId");
		$I->see($editeurName, 'h1');
		// InnerBrowser::dontSeeLink() is broken when no URL is given, so we use a workaround.
		$I->dontSee("Supprimer", 'a');
		$I->expectThrowable(
			new CHttpException(403, "Vous n'avez pas accès à la suppression d'éditeurs."),
			function() use($I, $editeurId) {
				$I->amOnPage("/editeur/delete/$editeurId");
			}
		);
		$I->logout();

		$I->comment("milady is authorized to delete publishers monitored by her institute");
		$I->amLoggedInAs('milady');
		$I->amOnPage("/editeur/$editeurId/$editeurName");
		$I->see($editeurName, 'h1');
		$I->see("Supprimer", 'a');
	}

	public function deleteMonitoredSucceeds(FunctionalTester $I)
	{
		$editeurId = 10;
		$editeurName = "Gallimard";

		$transport = \components\email\Mailer::getTransport();
		$transport->reset();

		$I->startFollowingRedirects();
		$I->amLoggedInAs('frollo');
		$I->comment("frollo can delete monitored publishers (perm suiviEditeurs)");
		$I->amOnPage("/editeur/$editeurId/$editeurName");
		$I->click("Supprimer", '#sidebar');
		$I->see("Supprimer", 'h1');
		$I->see("Confirmer la suppression", 'h2');
		$I->dontSeeElement('#constraints');
		$I->fillField('input[name="annoying"]', "Je confirme");
		$I->submitForm('#confirm-delete', ['redirection' => 5, 'annoying' => 'Je confirme'], '.form-actions .btn-danger');
		//$I->click('.form-actions .btn-danger');
		$I->see("supprimé", '.alert-success');

		$I->comment("Redirection after delete");
		$I->amOnPage("/editeur/$editeurId");
		$I->seeCurrentUrlEquals('/editeur/5/Julliard');

		$I->assertEquals(1, $transport->getSentCount());
		$lastEmail = new SentMessage($transport->getLastSentMessage());
		$I->assertEquals("suivi-editeurs@mirabel.localhost", $lastEmail->getFirstRecipient());
		$I->assertEquals("Mir@bel TEST : suppression éditeur « {$editeurName} »", $lastEmail->getSubject());

		$I->comment("frollo cannot delete when an idref is filled");
		$I->amOnPage("/editeur/19");
		$I->seeElement('a[data-idref="026485842"]');
		$I->click("Supprimer", '#sidebar');
		$I->see("Supprimer", 'h1');
		$I->seeElement('#constraints .alert-danger');

		$I->assertEquals(1, $transport->getSentCount());
		$transport->reset();
	}

	public function deleteUnmonitored(FunctionalTester $I)
	{
		$editeurId = 3370;
		$editeurName = "Société des études robespierristes";

		$I->comment("editeur-partenaire is not authorized to delete monitored publishers");
		$I->amLoggedInAs('editeur-98');
		$I->amOnPage("/editeur/$editeurId");
		$I->see($editeurName, 'h1');
		$I->dontSee("Supprimer", '#sidebar a');
		$I->logout();

		$I->comment("milady is authorized to delete unmonitored publishers");
		$I->amLoggedInAs('milady');
		$I->amOnPage("/editeur/$editeurId");
		$I->see($editeurName, 'h1');
		$I->startFollowingRedirects();
		$I->click("Supprimer", '#sidebar');
		$I->see("Supprimer", 'h1');
		$I->seeElement('#constraints .alert-danger');
	}
}
