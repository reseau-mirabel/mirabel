<?php

namespace tests\functional\editeur;

use components\email\SentMessage;
use FunctionalTester;

class EditeurUpdateCest
{
	public function update(FunctionalTester $I)
	{
		$I->amLoggedInAs('milady');
		$I->amOnPage('/editeur/331/Alternatives-economiques');
		$I->click('Modifier');
		$I->seeCurrentUrlEquals('/editeur/update/331');
		$rand = random_int(1, 1000);
		$I->fillField('Description', "Alternatives Economiques s'amuse ($rand) de l'économie comme enjeu collectif et social.");
		$I->fillField('IdRef', "12345678X");
		$I->startFollowingRedirects();

		$transport = \components\email\Mailer::getTransport();
		$transport->reset();

		// update fails
		$I->click('Enregistrer');
		$I->see("Cet identifiant est mal-formé", '.alert-error');

		// update succeeds, no important change
		$I->seeCurrentUrlEquals('/editeur/update/331');
		$I->fillField('IdRef', "029883091");
		$I->click('Enregistrer');
		$I->seeCurrentUrlEquals('/editeur/331/Alternatives-economiques');
		$I->seeElement('.alert-success');
		$I->see("Alternatives Economiques s'amuse ($rand)", 'td');
		$I->seeElement('a[title="IdRef 029883091"]');
		$I->assertEquals(0, $transport->getSentCount());

		// update succeeds, important change
		$I->amOnPage('/editeur/update/331');
		$I->fillField('IdRef', "26177400X");
		$I->click('Enregistrer');
		$I->seeElement('.alert-success');
		$I->assertEquals(1, $transport->getSentCount());
		$lastEmail = new SentMessage($transport->getLastSentMessage());
		$I->assertEquals("suivi-editeurs@mirabel.localhost", $lastEmail->getFirstRecipient());
		$I->assertEquals("Mir@bel TEST : Modification directe « Alternatives économiques »", $lastEmail->getSubject());
		$I->assertStringContainsString("IdRef", $lastEmail->getBody());
		$I->assertStringContainsString("029883091", $lastEmail->getBody());

		// update fails
		$I->amOnPage('/editeur/update/10');
		$I->fillField('IdRef', "26177400X");
		$I->click('Enregistrer');
		$I->see("Cet IdRef est déjà attribué", '.alert-error');
		$I->assertEquals(1, $transport->getSentCount());

		// update succeeds, important change (reset)
		$I->amOnPage('/editeur/update/331');
		$I->fillField('IdRef', "");
		$I->click('Enregistrer');
		$I->seeElement('.alert-success');
		$I->dontSeeElement('a[title="IdRef 029883091"]');
		$I->assertEquals(2, $transport->getSentCount());
	}

	public function updateUnimportant(FunctionalTester $I)
	{
		$I->amLoggedInAs('milady');
		$I->amOnPage('/editeur/update/331');
		$I->fillField('Description', "Cette intervention est sans importance et ne déclenche pas de courriel.");

		$transport = \components\email\Mailer::getTransport();
		$transport->reset();

		$I->click('Enregistrer');
		$I->seeElement('.alert-success');

		$I->assertEquals(0, $transport->getSentCount());
		$transport->reset();
	}
}