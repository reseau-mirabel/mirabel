<?php

class RessourceIndexCest
{
	/**
	 * @dataProvider provideListAlphaParams
	 */
	public function testListAlpha(FunctionalTester $I, \Codeception\Example $ex)
	{
		$I->amOnPage("/ressource/index?{$ex['url']}");
		$I->seeNumberOfElements('.list-results tbody tr', $ex['count']);
	}

	protected function provideListAlphaParams(): array
	{
		return [
			['count' => 3, 'url' => "lettre=c&noweb=1"], // 3 ressources non-web commençant par "C"
			['count' => 4, 'url' => "lettre=C&noweb=0"], // 1 ressource supplémentaire en "C" de type "site web"
			// La lettre est ignorée si suivi ou import
			['count' => 6, 'url' => "lettre=a&import=ressource"],
			['count' => 6, 'url' => "lettre=x&import=ressource"],
			['count' => 3, 'url' => "import=collection&noweb=1"],
			['count' => 1, 'url' => "import=ressource&suivi=1"], // 1 ressource suivie par Partenaire.id=1
			['count' => 1, 'url' => "import=ressource&suivi=1&noweb=1"],
			['count' => 0, 'url' => "import=collection&suivi=1&noweb=1"],
			['count' => 0, 'url' => "suivi=2&noweb=1"], // aucune ressource n'est suivie par le partenaire d'ID 2
			['count' => 1, 'url' => "suivi=-1&noweb=1"], // 1 ressource suivie par au moins un partenaire
		];
	}
}
