<?php

class AnalyseCest
{
	public function compareOds(FunctionalTester $I)
	{
		$I->startFollowingRedirects();
		$I->amLoggedInAs('frollo');
		$I->amOnPage('/analyse/compare-issn');
		$I->attachFile('#upload input[type="file"]', 'issn-compare.ods');
		$I->dontSeeElement('#synthese');

		$I->click("Envoyer", '#upload button');
		$I->dontSeeElement('#upload .control-group.error');
		$I->see("2 lignes contenant des identifiants ISSN", '#synthese');
		$I->seeNumberOfElements('#correspondances tbody tr', 2);

		$I->click("issn-compare.ods");
		$I->seeCurrentUrlMatches('#analyse/download#');
		$uploaded = file_get_contents(codecept_data_dir('issn-compare.ods'));
		$downloaded = $I->grabPageSource();
		$I->assertSame(strlen($uploaded), strlen($downloaded));

		$I->expectThrowable(
			\CHttpException::class,
			function() use($I) {
				$I->amOnPage('/analyse/download?file=a&name=b');
			}
		);
	}

	public function compareFails(FunctionalTester $I)
	{
		$I->startFollowingRedirects();
		$I->amLoggedInAs('frollo');
		$I->amOnPage('/analyse/compare-issn');

		$I->attachFile('#upload input[type="file"]', 'sherpa_0335-5322.json');
		$I->click("Envoyer", '#upload button');
		$I->seeElement('#upload .control-group.error');
		$I->dontSeeElement('#synthese');
	}

	public function reportFails(FunctionalTester $I)
	{
		$I->amLoggedInAs('frollo');
		$I->expectThrowable(
			\CHttpException::class,
			function() use($I) {
				$I->amOnPage('/analyse/report?file=blabla');
			}
		);
	}
}
