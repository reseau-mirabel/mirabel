<?php

namespace tests\functional\politique;

use FunctionalTester;

class PolitiqueDiversCest
{
	public function moderation(FunctionalTester $I)
	{
		$I->amOnPage('/site/search');
		$I->expectThrowable(
			new \CHttpException(403, "Identifiant requis"),
			function() use($I) {
				$I->amOnPage('/politique/moderation');
			}
		);

		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/politique/moderation');
		$I->see("Aucun résultat trouvé");
		$I->amOnPage('/site/search');
	}

	public function roles(FunctionalTester $I)
	{
		$I->amLoggedInAs("editeur-98");

		$I->expectThrowable(
			new \CHttpException(403, "Vous n'avez pas la permission d'agir sur cet éditeur."),
			function() use($I) {
				$I->amOnPage('/politique/roles?editeurId=23');
			}
		);

		$I->amOnPage('/politique/roles?editeurId=5');
		$I->see("Rôles de l'éditeur", 'h1');
		$I->amOnPage('/site/search');
	}
}
