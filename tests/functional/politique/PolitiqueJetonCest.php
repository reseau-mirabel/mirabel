<?php

namespace tests\functional\politique;

use FunctionalTester;

class PolitiqueJetonCest
{
	public function testAccessControl(FunctionalTester $I)
	{
		$I->amLoggedInAs("milady");
		$I->expectThrowable(
			new \CHttpException(403),
			function() use($I) {
				$I->amOnPage('/politique/jetons');
			}
		);
	}

	public function update(FunctionalTester $I)
	{
		$invalid = "mon-mot-de-passe";
		$valid = "mon0mot0de0passe";

		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/politique/jetons');
		$I->seeInField('#record-1 .token', "phrase-de-passe");

		$I->fillField('#record-1 .token', $invalid);
		$I->click('#record-1 button[type="submit"]');
		$I->see("chiffres et des lettres non accentuées", '.alert-error');

		$I->stopFollowingRedirects();
		$I->fillField('#record-1 .token', $valid);
		$I->click('#record-1 button[type="submit"]');
		$I->dontSeeElement('.alert-error');
		$I->seeResponseCodeIsRedirection();

		$I->amOnPage('/politique/jetons');
		$I->seeInField('#record-1 .token', $valid);

		$I->startFollowingRedirects();
		$I->logout();
	}

	public function createThenDelete(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/politique/jetons');

		$I->stopFollowingRedirects();
		$I->fillField('#record-0 .name', "someone");
		$I->fillField('#record-0 .token', "something");
		$I->click('#record-0 button[type="submit"]');
		$I->dontSeeElement('.alert-error');
		$I->seeResponseCodeIsRedirection();

		$I->amOnPage('/politique/jetons');
		$newId = 2;
		$I->seeInField("#record-{$newId} .token", "something");

		// delete
		$I->fillField("#record-{$newId} .token", "");
		$I->click("#record-{$newId}" . ' button[type="submit"]');
		$I->dontSeeElement('.alert-error');
		$I->seeResponseCodeIsRedirection();

		$I->amOnPage('/politique/jetons');
		$I->dontSeeElement("#record-{$newId}");
	}
}
