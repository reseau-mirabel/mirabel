<?php

namespace tests\functional\politique;

use FunctionalTester;

class PolitiqueApiCest
{
	public function apiHelp(FunctionalTester $I)
	{
		$I->amOnPage("/politique/api");
		$I->seeResponseCodeIsSuccessful();
		$I->see("Download the description", '#api-yaml');
	}

	public function apiError(FunctionalTester $I)
	{
		$I->stopFollowingRedirects();

		$I->amOnPage("/politique/api/changes");
		$response401 = json_decode($I->grabPageSource(), false, 16, JSON_THROW_ON_ERROR);
		$I->assertSame(401, $response401->error->code);

		$I->amOnPage("/politique/api/yesterday?api_key=phrase-de-passe");
		$response400 = json_decode($I->grabPageSource(), false, 16, JSON_THROW_ON_ERROR);
		$I->assertSame(400, $response400->error->code, "Wrong path '/yesterday'");

		$I->amOnPage("/politique/api/changes?since=yesterday&api_key=phrase-de-passe");
		$response400 = json_decode($I->grabPageSource(), false, 16, JSON_THROW_ON_ERROR);
		$I->assertSame(400, $response400->error->code, "Wrong value of the 'since' parameter");
	}

	public function apiOK(FunctionalTester $I)
	{
		$I->stopFollowingRedirects();
		$I->amOnPage("/politique/api/publication/6?api_key=phrase-de-passe");
		$response = json_decode($I->grabPageSource(), true, 16, JSON_THROW_ON_ERROR);
		$I->assertArrayHasKey('title', $response);
		$I->assertSame([['title' => "Esprit", 'language' => ""]], $response['title']);

		$I->amOnPage("/politique/api/changes?since=yesterday&api_key=phrase-de-passe");
		$response400 = json_decode($I->grabPageSource(), false, 16, JSON_THROW_ON_ERROR);
		$I->assertSame(400, $response400->error->code);
	}
}
