<?php

namespace tests\functional\politique;

use components\email\Mailer;
use components\email\SentMessage;
use FunctionalTester;
use Politique;
use Titre;
use Yii;

class PolitiqueCest
{
	private $politique;

	private int $uid;

	public function _before(FunctionalTester $I)
	{
		\Config::writeRaw('politique.seuil-affichage', "0");
	}

	public function _after(FunctionalTester $I)
	{
		\Config::writeRaw('politique.seuil-affichage', "5");
	}

	public function attachToPartenaireEditeur(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->stopFollowingRedirects();
		$I->amOnPage("/utilisateur/admin?UtilisateurSearch%5BpartenaireId%5D=-1");
		$I->click('td a.update');
		$I->selectOption('#User_partenaireId', "98"); // Julliard (Partenaire)
		$I->click("Enregistrer", '.form-actions');
		$I->seeResponseCodeIsRedirection();
		$I->seeFlashMessage('success');

		$I->amOnPage("/utilisateur/admin?UtilisateurSearch%5BpartenaireId%5D=98");
		$I->seeNumberOfElements('tbody tr', 2);

		// reset
		Yii::app()->db->createCommand("UPDATE Utilisateur SET partenaireId = NULL WHERE login = 'u-pol'")->execute();
		$I->startFollowingRedirects();
	}

	public function utilisateurFails(FunctionalTester $I)
	{
		$I->amOnPage('/politique/utilisateur');
		$I->fillField('Nom', 'Rogojine');
		$I->fillField('Prénom', 'Féodor');
		$I->fillField("UserPolitique[editeurIds][0]", 447);

		$I->fillField('Adresse électronique', "frollo@ndp.example.org"); // utilisateur-veilleur
		$I->click("Créer ce compte");
		$I->dontSeeCurrentUrlEquals('/site/login');
		$I->see("Un compte existe déjà avec cette adresse électronique", '.help-inline.error');

		$I->fillField('Adresse électronique', "anonyme@politique.localhost"); // utilisateur-politique
		$I->click("Créer ce compte");
		$I->dontSeeCurrentUrlEquals('/site/login');
		$I->see("Un compte existe déjà avec cette adresse électronique", '.help-inline.error');
	}

	public function utilisateur(FunctionalTester $I)
	{
		$editeurId = 447;
		$I->wantTo("create my new user account");
		$password = $this->registerUser($I, 'rogojine@example.com', [$editeurId]);
		$I->wantTo("connect with this new profile");
		$I->loginAs('rogojine@example.com', $password); // guest user
		$I->wantTo("browse /politique/* pages");
		$this->browse($I, $editeurId);
		$I->logout();

		$I->wantTo("validate this account with an admin user");
		$I->amLoggedInAs("d'artagnan");
		$this->adminValidatesUser($I);
		$I->wantTo("update this account with an admin user");
		$password = "ca7am-ahz2f"; // set a new password
		$this->adminUpdatesUser($I, $password);
		$I->logout();

		$I->loginAs('rogojine@example.com', $password); // validated user
		$I->wantTo("link a policy to journals");
		$this->titres($I, $editeurId);
		$this->assign($I, 447);
		$this->detachPolicy($I, $editeurId);
		$this->ajaxPublishAsAuthor($I, $editeurId);
		$this->deletePolicyDirect($I, $editeurId);
		$this->deletePolicyIndirect($I, $editeurId);
		$I->logout();

		$I->wantTo("disable then delete the validated user account (as an admin)");
		$I->amLoggedInAs("d'artagnan");
		$this->adminDisablesUser($I);
		$I->logout();
	}

	private function browse(FunctionalTester $I, int $editeurId)
	{
		$this->uid = (int) Yii::app()->user->getState('id');
		$I->seeCurrentUrlMatches('#^/politique($|\?)#');
		$I->see("Revues", 'h2');
		$I->seeElement('#politiques');
		$I->dontSeeElement('#politiques .politique');
		$I->seeNumberOfElements('#titres > ol > li', 5);
		$I->seeNumberOfElements('#titres a.policy-create', 5);

		$I->click('#titres .titre-716 a.policy-create'); // Créer la politique
		$I->see("Annales : Histoire, Sciences Sociales", 'h2'); // id 716
		$this->politique = $this->savePolicy($I, $editeurId, 716, null); // create
		$I->seeNumberOfElements('#politiques .politique', 1);
		$I->seeNumberOfElements('#politiques .politique li.journal', 1); // journals linked to a policy
		$I->seeNumberOfElements('#titres a.policy-create', 4);
		$I->assertEquals(0, (int) Yii::app()->db->createCommand("SELECT count(*) FROM Politique_Titre WHERE politiqueId = {$this->politique->id}")->queryScalar());

		$I->click("Voir et modifier…", '#politiques .politique a');
		$I->see("vue détaillée", 'h1');
		$this->savePolicy($I, $editeurId, 716, (int) $this->politique->id); // update
		$I->seeNumberOfElements('#politiques .politique', 1);
		$I->seeNumberOfElements('#politiques .politique li.journal', 1); // journals linked to a policy
		$I->assertEquals(0, (int) Yii::app()->db->createCommand("SELECT count(*) FROM Politique_Titre WHERE politiqueId = {$this->politique->id}")->queryScalar());

		$I->dontSee("Associer à d'autres revues…", '#politiques .politique a');

		$I->seeLink("contactez l'équipe Mir@bel");

		// not-validated => cannot assign a policy
		$I->sendAjaxGetRequest('/politique/ajax-candidates?editeurId=19&titreId=1400');
		$I->assertEquals('error', json_decode($I->grabPageSource())->status);
	}

	private function adminValidatesUser(FunctionalTester $I)
	{
		$I->amOnPage('/admin/politique-validation');
		$I->see("Utilisateurs demandant le rattachement à un éditeur", 'h2');
		$I->sendAjaxPostRequest('/admin/politique-validation', [
			'action' => 'owner',
			'utilisateurId' => Yii::app()->db->createCommand("SELECT id FROM Utilisateur WHERE login = 'rogojine@example.com'")->queryScalar(),
			'editeurId' => 447,
		]);
		$I->seeResponseCodeIs(200);
		$I->assertEquals(true, json_decode($I->grabPageSource())->success);
		$I->assertEquals(1, (int) Yii::app()->db->createCommand("SELECT count(*) FROM Politique_Titre WHERE politiqueId = {$this->politique->id}")->queryScalar());
	}

	private function adminUpdatesUser(FunctionalTester $I, string $newPassword)
	{
		$I->amOnPage("/admin/politique-utilisateurs");
		$I->see("rogojine@example.com", 'td.email a');
		$I->see("Féodor Rogojine", 'td.nom');

		$I->click('table td.nom a');
		$I->see("Féodor Rogojine", 'h1');

		$I->click("Modifier", '#sidebar');
		$I->seeCurrentUrlmatches('#/utilisateur/update/\d+$#');
		$I->see("Modifier", 'h1');
		$I->dontSeeElement('input#User_login');
		$I->fillField('Mot de passe', $newPassword);
		$I->fillField('Mot de passe (bis)', $newPassword);
		$I->click("Enregistrer", '.form-actions');
		$I->seeCurrentUrlmatches('#/utilisateur/\d+$#');
		$I->see("rogojine@example.com", 'table tr.email td');
	}

	private function titres(FunctionalTester $I, int $editeurId)
	{
		$I->amOnPage("/politique/index?editeurId=$editeurId");
		$I->seeNumberOfElements('#politiques .politique li.journal', 1);

		$I->wantTo("Create and delete Titre-Politique links");
		$I->click("Associer à d'autres revues…", '#politiques .politique a');
		$I->see("Éditions de l'EHESS", 'h2'); // Editeur.nom
		$I->uncheckOption("Annales : Histoire, Sciences Sociales"); // id 716
		$I->checkOption("Mélanges d'histoire sociale"); // id 1083
		$I->checkOption("Annales : Économies, Sociétés, Civilisations"); // id 1900
		$I->click("Enregistrer", 'button');
		$I->seeCurrentUrlMatches("#/politique/titres/{$this->politique->id}$#");
		$I->click("Retour");
		$I->seeCurrentUrlEquals("/politique?editeurId=$editeurId");
		$I->seeNumberOfElements('#politiques .politique li.journal', 2);
		$links = Yii::app()->db
			->createCommand("SELECT titreId, confirmed FROM Politique_Titre WHERE politiqueId = {$this->politique->id} ORDER BY titreId")
			->queryAll();
		$I->assertEquals([['titreId' => '1083', 'confirmed' => '0'], ['titreId' => '1900', 'confirmed' => '0']], $links);

		$I->wantTo("Create another policy");
		$I->click('#titres .titre-725 a.policy-create'); // Créer la politique
		$I->seeResponseCodeIs(200);
		$this->savePolicy($I, $editeurId, 725, null); // create
		$I->seeNumberOfElements('#politiques .politique', 2);
		$I->assertEquals(
			['725'],
			Yii::app()->db
				->createCommand("SELECT titreId FROM Politique_Titre WHERE politiqueId = " . (1 + $this->politique->id))
				->queryColumn()
		);

		$I->wantTo("Replace an existing Titre-Politique link by another link");
		$I->amOnPage("/politique/titres/{$this->politique->id}");
		$I->seeElement('.titre-1083 input[checked]');
		$I->seeElement('.titre-725 span.label'); // Titre 725 has another policy
		$I->dontSeeElement('.titre-725 input[checked]');
		$I->checkOption("Annales d'histoire économique et sociale"); // id 725
		$I->click("Enregistrer", 'button');
		$I->assertEquals(
			['725', '1083', '1900'],
			Yii::app()->db
				->createCommand("SELECT titreId FROM Politique_Titre WHERE politiqueId = {$this->politique->id} ORDER BY titreId")
				->queryColumn()
		);
		$I->assertEmpty(
			Yii::app()->db
				->createCommand("SELECT titreId FROM Politique_Titre WHERE politiqueId = " . (1 + $this->politique->id))
				->queryColumn()
		);
	}

	private function assign(FunctionalTester $I, int $editeurId)
	{
		$I->amOnPage("/politique/index?editeurId=$editeurId");
		$I->seeNumberOfElements('#titres .label', 3);
		$I->stopFollowingRedirects();

		$I->sendAjaxPostRequest("/politique/assign?titreId=1084&politiqueId={$this->politique->id}");
		$I->seeResponseCodeIsRedirection();
		$I->seeFlashMessage('success');
		$I->amOnPage("/politique/index?editeurId=447");
		$I->seeNumberOfElements('#titres .label', 4);
		$I->assertEquals(
			['725', '1083', '1084', '1900'],
			Yii::app()->db
				->createCommand("SELECT titreId FROM Politique_Titre WHERE politiqueId = {$this->politique->id} ORDER BY titreId")
				->queryColumn()
		);

		$pId = $this->politique->id + 1;
		$I->sendAjaxPostRequest("/politique/assign?titreId=716&politiqueId={$pId}");
		$I->seeResponseCodeIsRedirection();
		$I->seeFlashMessage('success');
		$I->amOnPage("/politique/index?editeurId=447");
		$I->assertEquals(
			['716'],
			Yii::app()->db
				->createCommand("SELECT titreId FROM Politique_Titre WHERE politiqueId = {$pId} ORDER BY titreId")
				->queryColumn()
		);
		$I->startFollowingRedirects();
	}

	private function detachPolicy(FunctionalTester $I, int $editeurId)
	{
		$I->amOnPage("/politique/index?editeurId=$editeurId");
		$I->seeNumberOfElements('#politiques .politique', 2);
		$I->seeNumberOfElements('#titres .label', 5);

		$I->sendAjaxPostRequest('/politique/unlink?titreId=1900');
		$I->amOnPage("/politique/index?editeurId=$editeurId");
		$I->seeNumberOfElements('#politiques .politique', 2);
		$I->seeNumberOfElements('#titres .label', 4);

		$I->expectThrowable(
			new \CHttpException(400),
			function() use($I) {
				$I->amOnPage('/politique/unlink?titreId=1083');
			}
		);
		$I->expectThrowable(
			new \CHttpException(404, "Aucune politique n'est associée à ce titre."),
			function() use($I) {
				$I->sendAjaxPostRequest('/politique/unlink?titreId=888');
			}
		);
	}

	private function ajaxPublishAsAuthor(FunctionalTester $I, int $editeurId)
	{
		$I->amOnPage("/politique/index?editeurId=$editeurId");

		$I->sendAjaxPostRequest("/politique/ajax-publish/99999");
		$response = json_decode($I->grabPageSource(), false, 16, JSON_THROW_ON_ERROR);
		$I->assertSame(404, $response->status);

		$pId = $this->politique->id + 1;
		$I->sendAjaxPostRequest("/politique/ajax-publish/{$pId}");
		$response = json_decode($I->grabPageSource(), false, 16, JSON_THROW_ON_ERROR);
		$I->assertSame("La transmission à Open policy finder est enclenchée.", $response->message);
		$I->assertSame(200, $response->status);
		$I->assertEquals(
			Politique::STATUS_TOPUBLISH,
			Yii::app()->db->createCommand("SELECT status FROM Politique WHERE id = {$pId}")->queryScalar()
		);

		// redirection to the confirmation page (multiple links for this policy)
		$I->sendAjaxPostRequest("/politique/ajax-publish/{$this->politique->id}");
		$response = json_decode($I->grabPageSource(), false, 16, JSON_THROW_ON_ERROR);
		$I->assertSame(302, $response->status);
		$I->assertSame("/politique/titres/{$this->politique->id}?mode=transmission", $response->location);
		$I->assertEquals(
			Politique::STATUS_DRAFT,
			Yii::app()->db->createCommand("SELECT status FROM Politique WHERE id = {$this->politique->id}")->queryScalar()
		);
	}

	private function deletePolicyDirect(FunctionalTester $I, int $editeurId)
	{
		$I->amOnPage("/politique/index?editeurId=$editeurId");
		$I->seeNumberOfElements('#politiques .politique', 2);

		$I->sendAjaxPostRequest('/politique/delete?id=' . $this->politique->id);
		$response = json_decode($I->grabPageSource(), false, 16, JSON_THROW_ON_ERROR);
		$I->assertSame("Politique supprimée", $response->message);
		$I->assertSame(200, $response->status);

		$I->amOnPage("/politique/index?editeurId=$editeurId");
		$I->seeNumberOfElements('#politiques .politique', 1);
		// direct deletion of a draft => record was deleted from the DB
		$I->assertEquals(
			false,
			Yii::app()->db->createCommand("SELECT status FROM Politique WHERE id = {$this->politique->id}")->queryScalar()
		);

		$I->expectThrowable(
			new \CHttpException(404, "Aucune politique n'est associée à ce titre."),
			function() use($I) {
				$I->sendAjaxPostRequest('/politique/unlink?titreId=88');
			}
		);
	}

	private function deletePolicyIndirect(FunctionalTester $I, int $editeurId)
	{
		$pId = $this->politique->id + 1;

		$I->amOnPage("/politique/index?editeurId=$editeurId");
		$I->seeNumberOfElements('#politiques .politique', 1);

		$I->sendAjaxPostRequest('/politique/delete?id=' . $pId);
		$response = json_decode($I->grabPageSource(), false, 16, JSON_THROW_ON_ERROR);
		$I->assertSame("Politique en attente de suppression", $response->message);
		$I->assertSame(200, $response->status);

		$I->amOnPage("/politique/index?editeurId=$editeurId");
		$I->seeNumberOfElements('#politiques .politique', 1);
		// indirect deletion of a "topublish" policy => record is still in the DB
		$I->assertEquals(
			Politique::STATUS_TODELETE,
			Yii::app()->db->createCommand("SELECT status FROM Politique WHERE id = {$pId}")->queryScalar()
		);

		$I->expectThrowable(
			new \CHttpException(404, "Aucune politique n'est associée à ce titre."),
			function() use($I) {
				$I->sendAjaxPostRequest('/politique/unlink?titreId=88');
			}
		);
	}

	private function adminDisablesUser(FunctionalTester $I)
	{
		$I->stopFollowingRedirects();
		$I->amOnPage("/admin/politique-utilisateurs");
		$I->click("Féodor Rogojine", 'td.nom');
		$I->click("Désactiver", '#sidebar');
		$I->seeResponseCodeIsRedirection();
		$I->seeFlashMessage('success');

		$I->amOnPage("/admin/politique-utilisateurs");
		$I->dontSee("Féodor Rogojine", 'td.nom');
		$I->startFollowingRedirects();
	}

	public function utilisateurMulti(FunctionalTester $I)
	{
		$password = $this->registerUser($I, 'aglaia@example.com', [7, 19]);
		$I->wantTo("connect with this new profile");
		$this->multiConnect($I, $password);
		$I->wantTo("Load Sherpa data as a new policy");
		$this->loadSherpa($I, $password);
		$I->wantTo("update my account");
		$this->updateBySelf($I, $password, "mirabel_2021");
		$I->wantTo("delete my account");
		$this->deleteSelf($I, "mirabel_2021");
		$I->amOnPage('/partenaire/1');
	}

	private function multiConnect(FunctionalTester $I, string $password)
	{
		$I->loginAs('aglaia@example.com', $password);
		$I->seeCurrentUrlEquals('/politique');
		$I->dontSee("Revues", 'h2');

		$I->amOnPage("/politique/index?editeurId=19");
		$I->dontSeeElement('#politiques .politique');

		$I->click('#titres .titre-103 a.policy-create'); // Créer la politique
		$I->see("Actualité Législative Dalloz", 'h2'); // id 103
		$this->politique = $this->savePolicy($I, 19, 103, null); // create
		$I->seeNumberOfElements('#politiques .politique', 1);

		$I->amOnPage("/politique/index?editeurId=7");
		$I->dontSeeElement('#politiques .politique');

		// cannot delete the policy of another user
		$I->expectThrowable(
			new \CHttpException(403, "L'accès pour suppression n'est pas autorisé."),
			function() use($I) {
				$I->sendAjaxPostRequest('/politique/delete?id=1');
			}
		);

		// not confirmed => no rights on this publisher
		$I->sendAjaxGetRequest('/politique/ajax-candidates?editeurId=3370&titreId=1400');
		$I->assertEquals('error', json_decode($I->grabPageSource())->status);
		$I->logout();
	}

	private function loadSherpa(FunctionalTester $I, string $password)
	{
		// prepare data
		$t = Titre::model()->findByPk(1400);
		/** @var Titre $t */
		$t->liensJson = '[
			{"sourceId":31,"src":"Sherpa","url":"https:\/\/sherpa.com/x"}
		]';
		$t->save(false);
		// confirm the rights
		Yii::app()->db->createCommand("UPDATE Utilisateur_Editeur SET confirmed = 1 WHERE editeurId = 19")->execute();

		$I->amLoggedInAs('aglaia@example.com', $password);
		$I->amOnPage("/politique/index?editeurId=19");
		$I->seeNumberOfElements('#politiques .politique', 1);

		// wrong journal
		$I->sendAjaxGetRequest('/politique/ajax-candidates?editeurId=19&titreId=999');
		$I->assertEquals('error', json_decode($I->grabPageSource())->status);
		// all is OK
		$I->sendAjaxGetRequest('/politique/ajax-candidates?editeurId=19&titreId=1400');
		$I->assertEquals('success', json_decode($I->grabPageSource())->status);

		$I->sendAjaxPostRequest('/politique/load-sherpa?editeurId=19&titreId=1400');
		$I->amOnPage("/politique/index?editeurId=19");
		$I->seeNumberOfElements('#politiques .politique', 2);

		$I->expectThrowable(
			new \CHttpException(404, "Ce titre n'a pas de données sur sa politique de publication (source Open policy finder)."),
			function() use($I) {
				$I->sendAjaxPostRequest('/politique/load-sherpa?editeurId=19&titreId=1399');
			}
		);

		$I->logout();
	}

	private function updateBySelf(FunctionalTester $I, string $password, string $newPassword)
	{
		$I->amLoggedInAs('aglaia@example.com', $password);
		$I->amOnPage("/politique/index?editeurId=7");
		$I->click('header li.profile a');
		$I->see("aglaia@example.com", 'table tr.email td');
		$I->click('Modifier mon profil…');

		$I->fillField('Adresse électronique', "frollo@ndp.example.org"); // utilisateur-veilleur
		$I->click("Enregistrer", '.form-actions');
		$I->see("Cette adresse électronique est déjà présente dans Mir@bel.", '.help-inline.error');

		$I->fillField("Adresse électronique", "aglaia@petrograd.com");
		$I->fillField('Mot de passe', $newPassword);
		$I->fillField('Mot de passe (bis)', $newPassword);
		$I->startFollowingRedirects();
		$I->click("Enregistrer", '.form-actions');
		$I->see("aglaia@petrograd.com", 'table tr.email td');
		$I->logout();
	}

	private function deleteSelf(FunctionalTester $I, string $password)
	{
		$I->wantTo("connect with the password I set myself, then delete the account");
		$I->loginAs('aglaia@petrograd.com', $password);
		$I->click('header li.profile a');

		$I->click("Supprimer mon compte");
		// The confirmation uses JS
		$I->seeCurrentUrlEquals('/');
		$I->seeElement('header .nav a[title^="Connexion"]');
	}

	/**
	 * @return string password
	 */
	private function registerUser(FunctionalTester $I, string $email, array $editeursIds): string
	{
		$I->wantTo("register a new profile");
		$I->amOnPage('/politique/utilisateur');
		$I->fillField('Nom', 'Rogojine');
		$I->fillField('Prénom', 'Féodor');
		$I->click("Créer ce compte");

		$I->seeElement('.alert-error');
		$I->fillField('Adresse électronique', $email);
		foreach ($editeursIds as $i => $id) {
			$I->fillField("UserPolitique[editeurIds][$i]", $id);
		}

		$transport = Mailer::getTransport();
		$transport->reset();
		$I->click("Créer ce compte");
		$I->seeCurrentUrlEquals('/site/login');
		$I->assertEquals(1, $transport->getSentCount(), "Number of emails sent does not match expectation.");
		$lastEmail = new SentMessage($transport->getLastSentMessage());
		$I->assertEquals($email, $lastEmail->getFirstRecipient());
		$m = [];
		$I->assertEquals(1, preg_match('/^\t(\S{8})\s*$/m', $lastEmail->getBody(), $m));
		return $m[1];
	}

	private function savePolicy(FunctionalTester $I, int $editeurId, int $titreId, ?int $politiqueId): Politique
	{
		$I->wantTo("save a new policy");
		$I->sendAjaxPostRequest('/politique/save', [
			'editeurId' => $editeurId,
			'titreId' => $titreId,
			'model' => 0,
			'politiqueId' => $politiqueId,
			'publisher_policy' =>
				<<<EOJSON
				{"permitted_oa":[{"additional_oa_fee":"no","copyright_owner":"authors","article_version":["published","accepted","submitted"],"location":{"location":["any_website"]},"conditions":["With the express agreement of authors","Under the conditions set by the authors"],"public_notes":["Authors retain all the rights ; no license fixed"]}],"open_access_prohibited":"no"}
				EOJSON,
		]);
		$I->seeResponseCodeIs(200);
		$I->assertEquals(true, json_decode($I->grabPageSource())->success);
		$I->amOnPage("/politique/index?editeurId=$editeurId");
		$p = Politique::model()->findBySql("SELECT * FROM Politique WHERE editeurId = $editeurId AND initialTitreId = $titreId ORDER BY id DESC LIMIT 1");
		$I->assertInstanceOf(Politique::class, $p);
		if ($politiqueId) {
			$I->assertEquals((int) $politiqueId, (int) $p->id, "Update expected, Politique.id should not change");
		}
		return $p;
	}
}
