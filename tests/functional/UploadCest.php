<?php

class UploadCest
{
	public function partenaireLogo(FunctionalTester $I)
	{
		$I->amLoggedInAs('frollo');
		$I->amOnPage('/partenaire/view-admin/2');
		$I->click("Déposer un logo", '#sidebar');
		$I->see("Logo du partenaire", 'h1');
		$I->see("Pas encore de logo");

		$this->upload($I);
		$I->amOnPage('/partenaire/2');
		$I->comment("og:image points to the uploaded image (raw, without resizing).");
		$I->seeInSource('<meta property="og:image" name="og:image" content="http://localhost/upload/view/images/partenaires/002.png');

		$this->remove($I);
		$I->amOnPage('/partenaire/2');
		$I->comment("og:image points to the default image, since there isn't a special one.");
		$I->seeInSource('<meta property="og:image" name="og:image" content="http://localhost/images/logo-mirabel-carre.png');
		$I->amOnPage('/partenaire/view-admin/2');
		$I->see("Ce partenaire n'a pas de logo", '.alert-warning');

		$I->amOnPage('/partenaire/logo/2');
		$I->see("Pas encore de logo");
		$I->dontSee("Supprimer");
	}

	private function upload(FunctionalTester $I)
	{
		$I->stopFollowingRedirects();
		$I->amOnPage('/partenaire/logo/2');
		// File upload is buggy with Yii1. This trick works on this side.
		// On the server side, CFile->saveAs() will fail, so the code must provide an alternative for tests.
		$I->attachFile('#logo-upload-form input[type="file"]', 'images/blue.png'); // attachFile uses hardcoded path to _data/
		$I->submitForm('#logo-upload-form', ['Upload[file]' => codecept_data_dir('image/blue.png')]);
		$I->seeResponseCodeIsRedirection();
		$I->seeFlashMessage('success', "Le logo a été mis à jour.");
	}

	private function remove(FunctionalTester $I)
	{
		$I->startFollowingRedirects();
		$I->amOnPage('/partenaire/logo/2');
		$I->dontSee("Pas encore de logo");
		$I->click("Supprimer");
	}
}
