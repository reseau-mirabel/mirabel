<?php

namespace tests\functional\grappe;

use FunctionalTester;

class GrappeCreateCest
{
	public function create(FunctionalTester $I)
	{
		// Allow wide access, instead of only admins
		\Yii::app()->params->add('grappe-permission', 'avec-partenaire');

		$I->amOnPage('/grappe');
		$I->dontSee('#grappes-miennes');
		$I->see("Aucune grappe n'est actuellement publique.", '.alert');

		$I->amLoggedInAs("milady");
		// Non-admin users can only see a Grappe from their own Partenaire
		$this->index($I, 0, 0);

		$I->amOnPage("/grappe/create");
		$I->see("Nouvelle grappe", 'h1');
		$I->fillField('Grappe[nom]', "Les raisins de la colère");
		$I->fillField("Note interne", "visible uniquement par l'auteur et ses collègues de l'établissement");
		$I->dontSee("Partenaire propriétaire", '#grappe-form'); // Only admins can see this

		$I->click("Enregistrer", ".form-actions");
		$I->see("Les raisins de la colère", 'h1');
		$I->see("partenaire", '#grappe-proprietes .diffusion');
		$I->see("revues", '#grappe-proprietes .niveau');
		$I->see("visible uniquement par l'auteur et ses collègues de l'établissement");

		$I->click("Rafraîchir les titres", '#titres-induits');
		$I->seeElement('#titres-induits');
		$id = $I->grabFromCurrentUrl('#grappe/admin-view/(\d+)$#');

		$this->index($I, 1, 0);

		$I->amLoggedInAs("d'artagnan");
		$this->updateAsAdmin($I);
		$this->addJournals($I);
		$this->viewLikeGuest($I);

		$I->logout();
		$this->indexAsGuest($I);
		$this->checkAccessControl($I, $id);

		$I->amLoggedInAs("milady");
		$this->removeContent($I, $id);
		$this->delete($I, $id);

		\Yii::app()->params->remove('grappe-permission');
	}

	public function createAsAdmin(FunctionalTester $I)
	{
		// Restrict access
		\Yii::app()->params->add('grappe-permission', 'admin');

		$I->amLoggedInAs("milady");
		// Non-admin users cannot create a Grappe, but they can update those from their own Partenaire
		$I->expectThrowable(
			new \CHttpException(403, "Vous n'êtes pas autorisé à effectuer cette action."),
			function() use($I) {
				$I->amOnPage("/grappe/create");
			}
		);
		// Non-admin users cannot access /grappe/index-admin unless they have some content available there.
		$I->expectThrowable(
			new \CHttpException(403, "Vous n'êtes pas autorisé à effectuer cette action."),
			function() use($I) {
				$this->index($I, 0, 1);
			}
		);

		$I->amLoggedInAs("d'artagnan");
		$I->startFollowingRedirects();
		$this->index($I, 0, 1);

		$I->amOnPage("/grappe/create");
		$I->see("Nouvelle grappe", 'h1');
		$I->fillField('Grappe[nom]', "Les raisins de la colère");
		$I->fillField("Note interne", "visible uniquement par l'auteur et ses collègues de l'établissement");
		$I->see("Partenaire propriétaire", '#grappe-form'); // Only admins can see this

		$I->click("Enregistrer", ".form-actions");
		$I->seeElement('.alert-error');

		$I->selectOption("Grappe[diffusion]", (string) \Grappe::DIFFUSION_PARTENAIRE);
		$I->click("Enregistrer", ".form-actions");
		$I->seeElement('.alert-error');

		$I->selectOption('#Grappe_partenaireId', "1");
		$I->click("Enregistrer", ".form-actions");
		$I->dontSeeElement('.alert-error');
		$I->see("Les raisins de la colère", 'h1');
		$I->see("partenaire", '#grappe-proprietes .diffusion');
		$I->see("revues", '#grappe-proprietes .niveau');
		$I->see("visible uniquement par l'auteur et ses collègues de l'établissement");

		// Admin users can see a Grappe from any Partenaire
		$this->index($I, 1, 1);

		// Non-admin users can only see a Grappe from their own Partenaire
		$I->amLoggedInAs("milady");
		$this->index($I, 1, 0);
		$I->click("Les raisins de la colère");
		// Non-admin users cannot create a Grappe, but they can update those from their own Partenaire
		$I->click("Administrer cette grappe");

		\Yii::app()->params->remove('grappe-permission');
	}

	private function index(FunctionalTester $I, int $mine, int $other): void
	{
		$I->amOnPage('/grappe/admin');
		$I->seeNumberOfElements('#grappes-miennes tr.grappe', $mine);
		if ($mine > 0) {
			$I->see("Les raisins de la colère", '#grappes-miennes');
		}
		if ($other > 0) {
			$I->seeNumberOfElements('#grappes-autres tr.grappe', $other);
		}
	}

	private function updateAsAdmin(FunctionalTester $I): void
	{
		$I->amOnPage('/grappe/admin');
		$I->click("Les raisins de la colère", '#grappes-miennes');
		$I->click("Administrer cette grappe");
		$I->click("Modifier", '#grappe-proprietes');
		$I->see("Modifier la grappe", 'h1');
		$I->see("Les raisins de la colère", 'h1');
		$I->seeInField("Nom", "Les raisins de la colère");
		$I->seeElement('#Grappe_partenaireId'); // Only admins can see this
		$I->seeInField("#grappe-diffusion", "mon partenaire");
		$I->selectOption("#grappe-diffusion", "publique");
		$I->seeInField("Niveau", "revues");
		$I->selectOption("Niveau", "revues");
		$I->see("Partenaire propriétaire", '#grappe-form');

		$I->click("Enregistrer", ".form-actions");
		$I->see("publique", '#grappe-proprietes .diffusion');
		$I->see("revues", '#grappe-proprietes .niveau');
	}

	private function addJournals(FunctionalTester $I): void
	{
		$I->click("Ajouter des titres");
		$I->fillField('#par-recherche input', '/revue/search?q%5Btitre%5D=Cahiers');
		$I->click("Lister ces titres", '#par-recherche button');
		$I->click("Ajouter cette liste à la grappe");
		$I->seeNumberOfElements('#titres-directs tbody tr', 5);
	}

	private function viewLikeGuest(FunctionalTester $I): void
	{
		$I->see("revues", '#grappe-proprietes .niveau');
		$I->click("en mode public", '#grappe-proprietes');
		$I->see("2 revues", 'h2');
		$I->dontSee("titres", 'h2');
		$I->dontSee("visible uniquement par l'auteur et ses collègues de l'établissement");
		$I->seeNumberOfElements('section ol > li', 2);
	}

	/**
	 * Guest (not authenticated)
	 */
	private function indexAsGuest(FunctionalTester $I): void
	{
		$I->amOnPage('/grappe');
		$I->see("Aucune grappe n'est actuellement publique.", '.alert');
	}

	/**
	 * Authenticated as "frollo"
	 */
	private function checkAccessControl(FunctionalTester $I, int $id): void
	{
		$I->amLoggedInAs("frollo");
		$I->amOnPage("/grappe/$id");
		$I->dontSeeElement('#grappe-proprietes');
		$I->dontSeeElement('#titres-directs');
		$I->expectThrowable(
			new \CHttpException(403, "Vous n'êtes pas propriétaire de cette grappe."),
			function() use($I, $id) {
				$I->amOnPage("/grappe/update/$id");
			}
		);
		$I->expectThrowable(
			new \CHttpException(403, "Vous n'êtes pas propriétaire de cette grappe."),
			function() use($I, $id) {
				$I->amOnPage("/grappe/delete/$id");
			}
		);
	}

	private function removeContent(FunctionalTester $I, int $id): void
	{
		$I->amOnPage("/grappe/$id/Les-raisins-de-la-colere");
		$I->click("Administrer cette grappe");
		$I->see("Cahiers de l'Institut Maurice Thorez", '#titres-directs');
		$I->click("Supprimer des titres");
		$I->see("Aucune recherche");
		$I->checkOption('input[value="4630"]'); // Cahiers de l'Institut Maurice Thorez
		$I->startFollowingRedirects();
		$I->click("Retirer les éléments sélectionnés");
		$I->seeNumberOfElements('#titres-directs tbody tr', 4);
		$I->dontSee("Cahiers de l'Institut Maurice Thorez", '#titres-directs');
	}

	private function delete(FunctionalTester $I, int $id): void
	{
		$I->expectThrowable(
			new \CHttpException(400, "Only POST is allowed."),
			function() use($I, $id) {
				$I->amOnPage("/grappe/delete/$id");
			}
		);
		$I->amOnPage("/grappe/admin-view/$id");
		$I->stopFollowingRedirects();
		$I->click("Supprimer", '#grappe-proprietes');
		$I->seeFlashMessage('success');
		$I->expectThrowable(
			new \CHttpException(404, "Aucune grappe n'a cet identifiant"),
			function() use($I, $id) {
				$I->amOnPage("/grappe/$id");
			}
		);
		$I->startFollowingRedirects();
	}
}
