<?php

namespace tests\functional\grappe;

use FunctionalTester;

class GlobalListCest
{
	public function all(FunctionalTester $I)
	{
		// Allow wide access, instead of only admins
		\Yii::app()->params->add('grappe-permission', 'avec-partenaire');

		$I->amOnPage("/grappe");
		$I->see("Aucune grappe n'est actuellement publique.", '.alert');

		// Pas de grappe partagée, donc rien de sélectionnable dans la liste globale.
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/grappe');
		$I->click("Gestion de la liste publique");
		$I->fillField('#grappe-search', "atron");
		$I->click("Chercher des grappes");
		$I->see("Aucun résultat");
		$I->logout();

		// Change la diffusion d'une grappe en "partagée"
		$I->amLoggedInAs('frollo');
		$I->amOnPage('/grappe/update/2');
		$I->selectOption('#grappe-diffusion', "partagée");
		$I->click("Enregistrer", 'button');
		$I->logout();

		$I->amOnPage("/grappe");
		$I->see("Aucune grappe n'est actuellement publique.", '.alert');

		// Une grappe partagée, donc sélectionnable dans la liste globale.
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/grappe');
		$I->click("Gestion de la liste publique");
		$I->seeNumberOfElements('liste-globale-selection tbody tr', 0);
		$I->fillField('#grappe-search', "atron");
		$I->click("Chercher des grappes");
		$I->seeNumberOfElements('#liste-globale-candidats tbody tr', 1);
		$I->checkOption('#liste-globale-candidats tbody tr input[type="checkbox"]');
		$I->click("Ajouter les grappes cochées à ma sélection");
		$I->seeNumberOfElements('#liste-globale-selection tbody tr', 1);
		$I->logout();

		// Page publique de la liste globale
		$I->amOnPage("/grappe");
		$I->see("Atron", 'table h2');
		$I->seeNumberOfElements('table.grappe-index tbody tr', 1);
	}

	public function sort(FunctionalTester $I)
	{
		// Crée une seconde grappe partagée et insère les 2 grappes dans la liste globale.
		$sqls = [
			"INSERT INTO Grappe VALUES (3, 'un d’abordage', 'bla &amp; bla', '', 1, 1, '[]', 1, 1708606828, NULL)",
			"UPDATE Grappe SET diffusion = " . \Grappe::DIFFUSION_PARTAGE,
			"INSERT IGNORE INTO Partenaire_Grappe (partenaireId, grappeId, pages, position, hdateCreation) VALUES (NULL, 2, 1, 6, 1708606900), (NULL, 3, 1, 7, 1708606900)"
		];
		foreach ($sqls as $sql) {
			\Yii::app()->db->createCommand($sql)->execute();
		}

		// Page publique de la liste globale
		$I->amOnPage("/grappe");
		$I->seeNumberOfElements('table.grappe-index tbody tr', 2);
		$I->see("Atron", 'table.grappe-index tbody tr:nth-child(1) h2');
		$I->see("abordage", 'table.grappe-index tbody tr:nth-child(2) h2');

		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage("/grappe/admin");
		$I->click("Gérer la liste publique");
		$I->see("Gestion de la liste publique des grappes", 'h1');
		$I->seeNumberOfElements('tbody#sortable > tr', 2);
	}
}
