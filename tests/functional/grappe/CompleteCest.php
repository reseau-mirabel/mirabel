<?php

namespace tests\functional\grappe;

use FunctionalTester;

class CompleteCest
{
	public function unauthentified(FunctionalTester $I)
	{
		$I->expectThrowable(
			new \CHttpException(403, "Identifiant requis"), // Which will redirect to /site/login
			function() use($I) {
				$I->amOnPage('/grappe/complete-mot?term=atr');
			}
		);
	}

	public function ok(FunctionalTester $I)
	{
		$I->amLoggedInAs('milady');

		// Match against an empty set => no result.
		$I->amOnPage('/grappe/complete-mot?term=atr');
		$I->assertSame('[]', $I->grabPageSource());

		\Yii::app()->db->createCommand("UPDATE Grappe SET diffusion = " . \Grappe::DIFFUSION_PARTAGE)->execute();
		$I->amOnPage('/grappe/complete-mot?term=Atr');
		$I->assertSame('[{"id":"Atron","label":"Atron","value":"Atron"}]', $I->grabPageSource());
		// Match only at the start of words.
		$I->amOnPage('/grappe/complete-mot?term=ron');
		$I->assertSame('[]', $I->grabPageSource());
		\Yii::app()->db->createCommand("UPDATE Grappe SET diffusion = " . \Grappe::DIFFUSION_PUBLIC)->execute();
	}
}
