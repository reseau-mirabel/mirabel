<?php

namespace tests\functional\grappe;

use FunctionalTester;

class SelectionCest
{
	public function noAccess(FunctionalTester $I)
	{
		// Allow wide access, instead of only admins
		\Yii::app()->params->add('grappe-permission', 'avec-partenaire');
		$I->amLoggedInAs('milady');

		$I->amOnPage('/partenaire/1');
		$I->dontSee("Paramètres d'affichage", '#sidebar');
	}

	public function scenario(FunctionalTester $I)
	{
		// Allow wide access, instead of only admins
		\Yii::app()->params->add('grappe-permission', 'avec-partenaire');
		\Yii::app()->db->createCommand("UPDATE Utilisateur SET permPartenaire = 1 WHERE login = 'milady'")->execute();
		\Yii::app()->db->createCommand("UPDATE Grappe SET diffusion = " . \Grappe::DIFFUSION_PARTAGE)->execute();

		$I->amLoggedInAs('milady');

		$I->amOnPage('/partenaire/view-admin/1');
		$I->seeElement('#grappes-selection');
		$I->see("Paramètres d'affichage", '#sidebar');

		$I->click("Paramètres d'affichage");
		$I->seeCurrentUrlEquals('/partenaire/grappes-affichage/1');
		$I->see("Affichage des grappes sélectionnées", 'h1');
		$I->seeElement('#grappes-affichage thead a[title="Choisir d\'autres grappes"]');
		$I->seeElement('#grappes-affichage thead a[title="Créer une grappe"]');
		$I->seeNumberOfElements('#grappes-affichage tbody tr', 0);

		$I->click("Choisir d'autres grappes", '#sidebar');
		$I->seeCurrentUrlEquals('/partenaire/grappes-selection/1');
		$I->fillField('#grappe-search', "atr");
		$I->click("Chercher des grappes", '.btn-primary');

		$I->seeCurrentUrlMatches('#^/partenaire/grappes-selection/1#');
		$I->see("Atron", '.grid-view');
		$I->checkOption('.grid-view tbody input[type="checkbox"]');
		$I->click("Ajouter les grappes cochées à ma sélection", '.btn-primary');

		$I->seeCurrentUrlEquals('/partenaire/grappes-affichage/1');
		$I->seeNumberOfElements('#grappes-affichage tbody tr', 1);
		$I->checkOption('#grappes-affichage td:nth-child(3) input[type="checkbox"]');
		$I->checkOption('#grappes-affichage td:nth-child(4) input[type="checkbox"]');
		$I->click("Enregistrer les modifications", '.btn-primary');
		$I->seeCurrentUrlEquals('/partenaire/grappes-affichage/1');

		$I->amOnPage('/partenaire/1');
		$I->seeElement('#grappes-selection');
		$I->see("Sélection de grappes – 3Mousquetaires", '#grappes-selection');
		$I->see("Atron", '#grappes-selection h3 a');
		$I->see("sélection détaillée de grappes", '#grappes-selection > p > a');
		$I->click("sélection détaillée de grappes");

		$I->seeCurrentUrlEquals('/partenaire/grappes/1');
		$I->see("Atron", '.grappe-index h2 a');

		// Reset the DB data
		\Yii::app()->db->createCommand("UPDATE Grappe SET diffusion = " . \Grappe::DIFFUSION_PUBLIC)->execute();
		\Yii::app()->db->createCommand("UPDATE Utilisateur SET permPartenaire = 0 WHERE login = 'milady'")->execute();
	}
}
