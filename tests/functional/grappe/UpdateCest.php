<?php

namespace tests\functional\grappe;

use FunctionalTester;

class UpdateCest
{
	public function view(FunctionalTester $I)
	{
		$I->amLoggedInAs("frollo");
		$I->amOnPage("/grappe");
		$I->see("Aucune grappe n'est actuellement publique.", '.alert');

		$I->click("Administration des grappes");
		$I->see("Atron", '#grappes-miennes');

		$I->click("Atron");
		$I->see("13 revues", 'h2');
	}

	public function updateStable(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage("/grappe/update/2");
		$I->seeOptionIsSelected('#grappe-diffusion', "publique");
		$I->selectOption('#grappe-diffusion', "authentifié");
		$I->click("Enregistrer", '.form-actions');
		$I->see("authentifié", '.diffusion');
		$I->seeElement('tr.partenaire');
		$I->logout();

		$I->amLoggedInAs("frollo");
		$I->amOnPage("/grappe/2");
		$I->click("Administrer cette grappe");
		$I->see("authentifié", '.diffusion');

		$I->click("Modifier", '#grappe-proprietes');
		$I->selectOption('#grappe-diffusion', "publique");
		$I->click("Enregistrer");
		$I->see("publique", '.diffusion');
	}

	public function updateRemoveOwner(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage("/grappe/update/2");
		$I->seeOptionIsSelected("Partenaire propriétaire", "Notre-Dame de Paris");
		$I->selectOption("Partenaire propriétaire", "** Sans propriétaire **");
		$I->click("Enregistrer", '.form-actions');
		$I->dontSeeElement('tr.partenaire');
		$I->amOnPage("/grappe/update/2");
		$I->seeOptionIsSelected("Partenaire propriétaire", "** Sans propriétaire **");
		$I->logout();

		$I->amLoggedInAs("frollo");
		$I->amOnPage("/grappe/2");
		$I->dontSee("Administrer cette grappe");
	}
}
