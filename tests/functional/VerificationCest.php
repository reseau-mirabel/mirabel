<?php

class VerificationCest
{
	public function editeurs(FunctionalTester $I)
	{
		$I->amLoggedInAs('milady');
		$I->amOnPage('/verification/editeurs');
		$I->seeNumberOfElements('#sans-pays li', 0);
		$I->see("21 éditeurs français", '#idref');
		$I->see("22 éditeurs français", '#sherpa');
		$I->seeNumberOfElements('#relations li', 14);
	}

	public function issn(FunctionalTester $I)
	{
		$I->amLoggedInAs('milady');
		$I->amOnPage('/verification');
		$I->see('Vérifications', 'h1');
		$I->click("ISSN-L partagés");
		$I->see("Au moins deux titres de ces revues ont le même ISSN-L.");
		$I->seeNumberOfElements('section#issnl-partages tbody tr', 7);
	}

	public function generales(FunctionalTester $I)
	{
		$I->amLoggedInAs('milady');
		$I->amOnPage('/verification');
		$I->click("Collections temporaires");
		$I->seeNumberOfElements('#collections-temporaires li', 2);
	}

	public function liensEditeurs(FunctionalTester $I)
	{
		$I->amLoggedInAs('milady');
		$I->amOnPage('/verification/liens-editeurs');
		$I->see("Vérification des liens d'éditeurs", 'h1');
		$I->seeNumberOfElements('#verification-liens-grid tbody tr', 2);

		$I->amOnPage('/verification/liens-editeurs?VerifUrlForm[msgContient]=coucou');
		$I->see("Aucun résultat trouvé.", '#verification-liens-grid tbody tr');

		$I->amOnPage('/editeur/2');
	}

	public function liensRessources(FunctionalTester $I)
	{
		$I->amLoggedInAs('milady');
		$I->amOnPage('/verification/liens-ressources');
		$I->see("Vérification des liens de ressources", 'h1');
		$I->seeNumberOfElements('#verification-liens-grid tbody tr', 2);

		$I->amOnPage('/verification/liens-ressources?VerifUrlForm[suiviPar]=2');
		$I->see("Aucun résultat trouvé.", '#verification-liens-grid tbody tr');

		$I->amOnPage('/editeur/2');
	}

	public function liensRevues(FunctionalTester $I)
	{
		$I->amLoggedInAs('milady');
		$I->amOnPage('/verification/liens-revues');
		$I->see("Vérification des liens de revues", 'h1');
		$I->seeNumberOfElements('#verification-liens-grid tbody tr', 2);

		$I->amOnPage('/verification/liens-revues?VerifUrlForm[urlContient]=tralala');
		$I->see("Aucun résultat trouvé.", '#verification-liens-grid tbody tr');

		$I->amOnPage('/editeur/2');
	}

	public function revues(FunctionalTester $I)
	{
		$I->amLoggedInAs('milady');
		$I->amOnPage('/verification/revues');
		$I->seeNumberOfElements('#revues-obsoletepar-duplicate tbody tr', 1);
		$I->seeNumberOfElements('#revues-mismatching-order tbody tr', 0);
		$I->seeElement('table[data-exportable-filename^="Mirabel-TEST_revues-debordement-debut_2"]');
	}

	public function titresEditeurs(FunctionalTester $I)
	{
		$I->amLoggedInAs('milady');
		$I->amOnPage('/verification/titres-editeurs');
		$I->see("Titres sans éditeur", 'h1');
		$I->seeNumberOfElements('#titres-vivants tbody tr', 1);
		$I->seeNumberOfElements('#titres-morts tbody tr', 4);
		$I->amOnPage('/editeur/2');
	}

	public function wikidata(FunctionalTester $I)
	{
		$I->amLoggedInAs('milady');
		$I->amOnPage('/verification');
		$I->click("Wikidata");
		$I->seeNumberOfElements('#wikidata-grid tbody tr', 1);
		$I->see("Aucun résultat trouvé.", '#wikidata-grid tbody td');

		$I->amOnPage('/verification/wikidata?Wikidata[property]=P7363');
		$I->see("Cache Wikidata - P7363 issnl", 'h1');
		$I->seeNumberOfElements('#wikidata-grid tbody tr', 1);
		$I->dontSee("Aucun résultat trouvé.", '#wikidata-grid tbody td');
		$I->amOnPage('/editeur/2');
	}
}
