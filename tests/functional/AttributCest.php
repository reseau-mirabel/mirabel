<?php

class AttributCest
{
	public function importWithJournalCreations(FunctionalTester $I): void
	{
		$I->assertSame(0, (int) \Yii::app()->db->createCommand("SELECT count(*) FROM IndexingRequiredTitre")->queryScalar());

		$I->amLoggedInAs("d'artagnan"); // admin
		$I->amOnPage('/attribut/import');
		$I->attachFile('#q_uploadedFile[type="file"]', 'doaj.csv');
		//$I->submitForm('#upload form', [], 'button[type="submit"]');
		$I->click("Envoyer", '.form-actions button');

		$I->selectOption('#q_colonneIssn1', "Journal ISSN (print version)");
		$I->selectOption('#q_colonneIssn2', "Journal EISSN (online version)");
		$I->selectOption('#q_sourceattributId', 'apc – APC');
		$I->selectOption('#q_colonneAttribut', "APC");
		$I->fillField('#q_filtre', "Yes");
		$I->selectOption('#q_colonneTitre', "Journal title");
		$I->click("Importer dans cet attribut", '.form-actions button');

		$I->see("Rapport sur l'import", 'h2');
		$I->see("Revues non identifiées dans Mir@bel", 'h3');

		$I->click("toutes les valeurs actuelles");
		$I->see("1 enregistrements");

		$I->moveBack();
		$I->click("Liste des 1 titres");
		$I->seeNumberOfElements('table.exportable tbody tr', 1);

		$I->moveBack();
		$I->click("CSV de toutes les valeurs");
		$I->assertSame("titreId	valeur\n1105	Yes\n", $I->grabPageSource());

		$I->amOnPage('/attribut/historique/1');
		$I->seeNumberOfElements('#attribut-valeurs-grid tbody tr', 1);

		$I->comment("Check the Manticore indexing was updated");
		$I->assertSame(1, (int) \Yii::app()->db->createCommand("SELECT count(*) FROM IndexingRequiredTitre")->queryScalar());
	}

	public function importSimple(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$this->createAttribute($I);
		$attributLogId = $this->importIntoAttribute($I);
		$this->importSameFileAgain($I);
		$this->listSourceValues($I, $attributLogId);
	}

	private function createAttribute(FunctionalTester $I)
	{
		$I->wantTo("create a new attribute");
		$I->amOnPage('/attribut/declare');
		$I->assertEmpty(Yii::app()->db->createCommand("SELECT id FROM Sourceattribut WHERE identifiant = 'rose'")->queryScalar());
		$I->fillField('#attribut-create input[name="Sourceattribut[nom]"]', "de la rose");
		$I->fillField('#attribut-create input[name="Sourceattribut[identifiant]"]', "rose");

		$I->stopFollowingRedirects();
		$I->click('#attribut-create button.btn-primary');
		$I->seeResponseCodeIsRedirection();
		$I->seeFlashMessage('success');
		$id = (int) Yii::app()->db->createCommand("SELECT id FROM Sourceattribut WHERE identifiant = 'rose'")->queryScalar();
		$I->assertGreaterOrEquals(1, $id);

		$I->amOnPage('/attribut/declare');
		$I->seeInField("#attribut-update-$id input", "de la rose");
	}

	/**
	 * @return int AttributImportLog.id
	 */
	private function importIntoAttribute(FunctionalTester $I): int
	{
		$I->wantTo("import into an attribute");
		$I->startFollowingRedirects();
		$I->amOnPage('/attribut/declare');
		$I->click("Importer");

		$I->see("Dépôt de fichier", 'h2');
		$I->see("Fichier de revues", 'label');
		$I->seeElement('#q_uploadedFile[type="file"]');
		$I->attachFile('#q_uploadedFile[type="file"]', 'doaj.csv');
		$I->click("Envoyer", '.form-actions button');
		$I->dontSeeElement('.control-group.error');

		$I->see("Configurer l'import du fichier", 'h2');
		$I->selectOption('#q_colonneIssn1', "Journal ISSN (print version)");
		$I->selectOption('#q_colonneIssn2', "Journal EISSN (online version)");
		$I->selectOption('#q_sourceattributId', 'rose – de la rose');
		$I->selectOption('#q_colonneAttribut', "APC");
		$I->click("Importer dans cet attribut", '.form-actions button');

		$I->see("Rapport sur l'import", 'h2');
		$I->see("1 créations ou modifications de valeurs");
		$I->dontSee("Revues non identifiées dans Mir@bel", 'h3');
		$I->seeNumberOfElements('#valeurs table.table tbody tr', 1);

		$m = [];
		preg_match('#<a href="/attribut/import/(\d+)#', $I->grabPageSource(), $m);
		$id = (int) $m[1];

		$I->click("toutes les valeurs actuelles", '#valeurs');
		$I->seeNumberOfElements('table.exportable tbody tr', 1);
		$I->seeLink("Cahiers d'histoire. Revue d'histoire critique");
		$I->moveBack();

		return $id;
	}

	private function importSameFileAgain(FunctionalTester $I)
	{
		$I->seeCurrentUrlMatches('#attribut/report#');
		$I->stopFollowingRedirects();
		$I->click("Correspondance entre colonnes et attribut M", '#import-steps');
		$I->seeResponseCodeIsRedirection();
	}

	private function listSourceValues(FunctionalTester $I, int $attributLogId): void
	{
		$I->amOnPage("/attribut/ajax-list-values/$attributLogId?colNum=29");
		$response = $I->grabPageSource();
		$values = json_decode($response);
		$I->assertSame(['No', 'Yes'], $values);
	}
}
