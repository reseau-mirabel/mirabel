<?php

class SudocCest
{
	public function import(FunctionalTester $I)
	{
		$I->amLoggedInAs('frollo');
		$I->amOnPage("/sudoc/import");
		$I->checkOption('input[type="checkbox"][name*="simulation"]');
		$I->click("Importer ce fichier", 'button');
		$I->see("ne peut être vide", '.alert-error');
	}
}
