<?php

class AdminCest
{
	public function disconnectEverybody(FunctionalTester $I)
	{
		// block connections
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/admin');
		$I->click("Déconnexion générale");
		$I->logout();

		// log-in fails
		$I->amOnPage('/site/login');
		$I->fillField("Identifiant", "frollo");
		$I->fillField("Mot de passe", "mdp");
		$I->click("Se connecter");
		$I->see("maintenance", '.alert-error');

		// enable connections
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/admin');
		$I->stopFollowingRedirects();
		$I->click("Autoriser les connexions");
		$I->seeFlashMessage('info', "Connexion ré-ouverte");
		$I->logout();

		// log-in succeeds
		$I->amOnPage('/site/login');
		$I->fillField("Identifiant", "frollo");
		$I->fillField("Mot de passe", "mdp");
		$I->stopFollowingRedirects();
		$I->click("Se connecter");
		$I->seeResponseCodeIsRedirection();
		$I->amOnPage('/site/contact');
		$I->see("Frollo", '#header .navbar');
	}

	public function politique(FunctionalTester $I)
	{
		$I->amLoggedInAs("frollo");
		$I->expectThrowable(
			new \CHttpException(403, "Vous n'êtes pas autorisé à effectuer cette action."),
			function() use($I) {
				$I->amOnPage('/admin/politique');
			}
		);
		$I->logout();
		$I->amOnPage('/editeur/4');

		$I->amLoggedInAs("milady");
		$I->amOnPage('/admin/politique');
		$I->click("Utilisateurs-politiques");
		$I->seeNumberOfElements('table.items tbody tr', 1);
	}
}
