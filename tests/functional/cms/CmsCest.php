<?php

namespace tests\functional\cms;

use CHttpException;
use FunctionalTester;

class CmsCest
{
	public function render(FunctionalTester $I)
	{
		$I->amOnPage('/site/page/aide');
		$I->see("aide à la recherche", 'h2');
		$I->see("API Mir@bel", '#page-aide a');
	}

	public function createPage(FunctionalTester $I)
	{
		$I->amLoggedInAs("milady");
		$I->amOnPage('/cms');
		$I->click("Nouvelle page", '#sidebar a');

		$I->fillField('#page-name', "chouette"); // Nom
		$I->fillField("Titre de la page", "Le jour de la chouette");
		$I->fillField("Contenu", "## Niveau2\n%NB_REVUES%");

		$I->click("Créer", '.form-actions');
		$I->seeCurrentUrlMatches('#cms/\d+#');
		$I->see("Le jour de la chouette", '.detail-view td');
		$I->seeNumberOfElements('.detail-view td a', 1);
		//$id = $I->grabTextFrom('.detail-view tr:first-child td');

		$I->click('.detail-view td a');
		$I->seeCurrentUrlMatches('#site/page/chouette#');
		$I->see("Niveau2", 'h2');
		$I->see("20", '#page-chouette');

		// delete
		/*
		$I->startFollowingRedirects();
		$I->sendAjaxPostRequest("/cms/delete?id=$id");
		$I->seeResponseCodeIsSuccessful();
		 */
	}

	public function indexWithoutPerm(FunctionalTester $I)
	{
		$I->amLoggedInAs("frollo");
		$I->expectThrowable(
			new CHttpException(403, "Vous n'êtes pas autorisé à effectuer cette action."),
			function() use($I) {
				$I->amOnPage('/cms');
			}
		);
	}

	public function update(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/cms');
		$I->see("Liste des blocs éditoriaux", 'h1');

		$I->click('a[title="Mettre à jour"]', '.button-column');
		$I->see("Modifier le bloc", 'h1');
		$I->see("accueil-actu", 'h1');
	}

	public function view(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/cms');

		$I->click('a[title="Voir"]', '.button-column');
		$I->see("Bloc éditorial", 'h1');
		$I->see("accueil-actu", 'h1');
		$I->seeElement('td a[href="/site/page/actualite"]');
	}
}
