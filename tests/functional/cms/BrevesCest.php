<?php

namespace tests\functional\cms;

use FunctionalTester;

class BrevesCest
{
	public function create(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/cms');
		$I->click("Nouvelle brève", '#sidebar a');

		$I->fillField('#cms-content', "## La petite nouvelle\n\n* Partenaire courant…\n* %PARTENAIRE%\n"); // Contenu

		$I->click("Enregistrer", '.form-actions');
		$I->seeCurrentUrlMatches('#cms/\d+#');
		$I->see("La petite nouvelle", '.detail-view td');
		$I->see("%PARTENAIRE%", '.detail-view td');

		$id = $I->grabTextFrom('.detail-view tr:first-child td');
		$I->stopFollowingRedirects();
		$I->sendAjaxPostRequest("/cms/delete?id=$id");
		$I->seeResponseCodeIsRedirection();
		$I->amOnPage('/cms');
		$I->dontSee("La petite nouvelle", '.detail-view td');
	}

	public function searchByRessource(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/cms');
		$I->click("Brèves", '#sidebar a');
		$I->see("Total de 9 résultats", '.summary');

		$I->selectOption("Ressource liée", "Cairn.info"); // Check that this category exists.
		$I->selectOption("Ressource liée", "Ritimo");
		$I->click("Filtrer les brèves", '.form-actions');
		$I->see("Total de 1 résultat", '.summary');
		$I->see("Présentation de Mir@bel au réseau ISORE", '.grid-view td.contenu');
	}

	public function searchByCategory(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/cms/breves');
		$I->see("Total de 9 résultats", '.summary');

		$I->seeNumberOfElements('select#q_categorie option', 4);
		$I->selectOption("Catégorie", "Partenariats");
		$I->click("Filtrer les brèves", '.form-actions');
		$I->seeOptionIsSelected('select#q_categorie', "Partenariats");
		$I->see("Total de 6 résultats", '.summary');
	}

	public function update(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/cms/18');
		$I->see("Présentation de Mir@bel au réseau ISOR", '.detail-view');

		$I->click("Modifier", '#sidebar a');
		$I->FillField('#cms-content', "Ceci est mon nouveau contenu");
		$I->seeInField('#breve-categories', "Gâteau guérit");
		$I->fillField('#breve-categories', "Monte-Christo ; Joseph Basalmo");
		$I->seeInField('#breve-ressources', "12, 3");
		$I->fillField('#breve-ressources', "4");

		$I->click("Enregistrer cette brève");
		$I->dontSee("Présentation de Mir@bel au réseau ISOR", '.detail-view');
		$I->see("Ceci est mon nouveau contenu", '.detail-view');
		$I->dontSee("Gâteau guérit", '.detail-view');
		$I->see("Monte-Christo", '.detail-view');

		$I->wantTo("Filter by the categories I've just added");
		$I->amOnPage('/cms/breves');
		$I->selectOption("Catégorie", "Joseph Basalmo");
		$I->dontSeeElement('option[value="12"]');
		$I->selectOption("Ressource liée", "4");
	}
}
