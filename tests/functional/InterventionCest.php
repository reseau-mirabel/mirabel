<?php

class InterventionCest
{
	public function adminAndView(FunctionalTester $I)
	{
		$I->amOnPage('/revue/444/A-contrario-Revue-interdisciplinaire-de-sciences-sociales');
		$I->dontSeeLink('historique des interventions', '/intervention/admin');

		$I->amLoggedInAs('milady');
		$I->amOnPage('/revue/444/A-contrario-Revue-interdisciplinaire-de-sciences-sociales');
		$I->seeElement('a[title="historique des interventions"]');

		$I->click('historique des interventions');
		$I->see("Interventions", 'h1');
		$I->see("total de 60", '.grid-view .summary');

		$I->click('a[href="/intervention/98594"]', '.button-column');
		$I->see("Intervention manuelle");
		$I->see("Titre-clé", '#intervention-detail');
	}

	public function search(FunctionalTester $I)
	{
		$I->amLoggedInAs('milady');

		$I->amOnPage('/intervention/admin?q%5Bsuivi%5D=1');
		$I->see("total de 518", '.grid-view .summary');

		$I->amOnPage('/intervention/admin?q%5Bsuivi%5D=1&q%5Bstatut%5D=attente');
		$I->seeElement('.grid-view .empty');
	}

	public function revertThenAccept(FunctionalTester $I)
	{
		$I->amLoggedInAs('milady');
		$I->amOnPage('/intervention/139058');
		$I->see("Modification d'accès en ligne", 'h2');
		$I->see("Vol. 14, no 14", '.content-after');
		$I->click("Annuler cette intervention");
		$I->see("refusé", 'tr.alert');
		$I->click("Accepter");
		$I->see("Accepté", 'tr.alert');
	}

	public function acceptByForce(FunctionalTester $I)
	{
		$int = new Intervention();
		$int->setAttributes(
			[
				'ressourceId' => null,
				'revueId' => null,
				'titreId' => null,
				'editeurId' => null,
				'statut' => 'attente',
				'import' => 0,
				'ip' => $_SERVER['REMOTE_ADDR'] ?? '',
				'contenuJson' => new InterventionDetail(),
				'hdateProp' => time(),
			],
			false
		);
		$titre = new Titre();
		$titre->titre = "Alternatives économiques";
		$titre->prefixe = '';
		$titre->sigle = '';
		$titre->dateDebut = '';
		$titre->dateFin = '';
		$titre->url = '';
		$titre->urlCouverture = '';
		$titre->liensJson = '';
		$titre->periodicite = '';
		$titre->langues = '';
		$titre->statut = 'normal';
		$titre->revueId = 1184;
		$titre->obsoletePar = 1248;
		$int->contenuJson->create($titre);
		$int->save(false);

		$I->amLoggedInAs('milady');
		$I->amOnPage("/intervention/{$int->id}");
		$I->stopFollowingRedirects();
		$I->dontSee("Accepter de force");
		$I->click("Accepter");
		$I->seeCurrentUrlMatches('#^/intervention/accept/\d+\?force=$#');
		$I->seeFlashMessage('error', "il s'agit peut-être d'un doublon");
		$I->seeFlashMessage('error', "Alternatives économiques");

		$I->amOnPage("/intervention/{$int->id}?statut=warning");
		$I->click("Accepter de force");
		$I->seeFlashMessage('success', "Intervention acceptée et appliquée");
		$I->seeResponseCodeIsRedirection();

		// cleanup
		$I->amOnPage("/intervention/{$int->id}");
		$I->click("Annuler cette intervention");
		$I->seeFlashMessage('success');
		$int->delete();
	}
}
