<?php

class DocCest
{
	public function markdown(FunctionalTester $I)
	{
		$I->amLoggedInAs('milady');
		$I->amOnPage('/doc');
		$I->click("sherpa-romeo");
		$I->see("Sherpa-RoMEO", 'h1');
		$I->see("Interrogation ciblée", 'h2');
	}

	public function pdf(FunctionalTester $I)
	{
		$I->amLoggedInAs('milady');
		$I->amOnPage('/doc');
		$I->click("fake.pdf");
		$I->seeResponseCodeIsSuccessful();
	}
}
