<?php

class PossessionCest
{
	public function export(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/possession/export?partenaireId=1');
		$I->seeInSource('possession;bouquet;titre;"titre ID";issn;issne;issnl;"revue ID";"identifiant local"');
		$I->seeInSource('1;;"A contrario. Revue interdisciplinaire de sciences sociales";444;1660-7880;1662-8667;1660-7880;444;419857');
		$I->seeInSource('1;;"Vingtième siècle, revue d\'histoire";98;0294-1759;1950-6678;0294-1759;98;419674');
		$I->logout();

		$I->expectThrowable(\CHttpException::class, function() use($I) {
			$I->amLoggedInAs("frollo");
			$I->amOnPage('/possession/export?partenaireId=1');
		});
	}

	public function exportMore(FunctionalTester $I)
	{
		$I->amLoggedInAs("frollo");
		$I->amOnPage('/possession/export-more?partenaireId=2');
		$csv = $I->grabPageSource();
		$I->assertStringStartsWith('revue_ID;bouquet;titre;titre_ID;issn;issne;issnl;identifiant_local;URL_Mirabel;ppn;ppne;bnfark;bnfarke;date_de_debut;date_de_fin;titre_suivant;periodicite;langues;url', $csv);
		$I->assertStringContainsString('444;;"A contrario. Revue interdisciplinaire de sciences sociales";444;1660-7880;1662-8667;1660-7880;8;http://localhost/', $csv);
		$I->assertEquals(16, substr_count($csv, "\n"), "wrong number of lines in the CSV");
		$I->logout();
		$I->amOnPage('/partenaire/2');
	}

	public function import(FunctionalTester $I)
	{
		$I->amLoggedInAs("frollo");

		$I->expectThrowable(
			new \CHttpException(404, "Ce partenaire n'existe pas."),
			function() use($I) {
				$I->amOnPage('/possession/import?partenaireId=999');
			}
		);
		$I->expectThrowable(
			new \CHttpException(403, "Vous n'avez pas la permission de gérer ces possessions."),
			function() use($I) {
				$I->amOnPage('/possession/import?partenaireId=1');
			}
		);

		$I->amOnPage('/possession/import?partenaireId=2');
		$I->dontSeeElement('#imported-rows');

		$I->checkOption("Modification de l'existant");
		$I->uncheckOption("Simulation");
		$I->attachFile('input[type="file"]', 'possession_import.csv'); // attachFile uses hardcoded path to _data/
		$I->click("Importer");

		$I->seeNumberOfElements('#imported-rows .success.modified', 1);
		$I->seeNumberOfElements('#imported-rows .success.unchanged', 0);
		$I->seeNumberOfElements('#imported-rows .warning', 0);
		$I->seeNumberOfElements('#imported-rows .error', 1);
	}
}
