<?php

class CategorieCest
{
	public function admin(FunctionalTester $I)
	{
		$I->amLoggedInAs('frollo');
		$I->amOnPage('/categorie/admin');
		$I->see("Administration de la thématique", 'h1');
	}

	public function ajax(FunctionalTester $I)
	{
		$I->amLoggedInAs('frollo');
		$I->sendAjaxPostRequest('/categorie/ajax-create', [
			'Categorie[categorie]' => "jaïnisme",
			'Categorie[description]' => "religion exotique pour un Européen",
			'Categorie[parentId]' => "2",
			'Categorie[role]' => "public",
		]);
		$createResponse = json_decode($I->grabPageSource());
		$I->assertSame('success', $createResponse->success);
		$I->assertEquals("Le nouveau thème est enregistré.", $createResponse->message);
		$id = $createResponse->node->id;

		$I->sendAjaxPostRequest('/categorie/ajax-update', [
			'Categorie[id]' => $id,
			'Categorie[description]' => "religion plutôt exotique pour un Européen",
		]);
		$updateResponse = json_decode($I->grabPageSource());
		$I->assertEquals('success', $updateResponse->success);

		$I->sendAjaxPostRequest('/categorie/ajax-delete', [
			'id' => $id,
		]);
		$deleteResponse = json_decode($I->grabPageSource());
		$I->assertEquals('success', $deleteResponse->success);
	}

	public function createCandidate(FunctionalTester $I)
	{
		$I->amLoggedInAs('milady');
		$I->amOnPage('/categorie/create?parentId=4');
		$I->see("Nouveau thème", 'h1');
		$I->see("Ce thème aura un statut de candidat", '.alert');
		$I->stopFollowingRedirects();

		$I->submitForm('#categorie-form', [
			'Categorie[categorie]' => "jaïnisme",
			'Categorie[description]' => "religion exotique pour un Européen",
			'Categorie[parentId]' => "4",
		]);
		$I->see("On ne peut créer de thème de niveau supérieur à 3", '.error span');

		$I->submitForm('#categorie-form', [
			'Categorie[categorie]' => "jaïnisme",
			'Categorie[description]' => "religion exotique pour un Européen",
			'Categorie[parentId]' => "2",
		]);
		$I->seeResponseCodeIsRedirection();
		$I->seeFlashMessage('success', "Nouveau thème");
	}

	/**
	 * @depends createCandidate
	 */
	public function updateCandidate(FunctionalTester $I)
	{
		$I->amLoggedInAs('milady');
		$id = (int) Yii::app()->db->createCommand("SELECT id FROM Categorie WHERE categorie = 'jaïnisme'")->queryScalar();
		$I->amOnPage("/categorie/$id");
		$I->stopFollowingRedirects();
		$I->expectThrowable(\CHttpException::class, function() use($I) {
			$I->click("Modifier", '#sidebar');
		});

		$I->logout();
		$I->amLoggedInAs('frollo');
		$I->amOnPage("/categorie/update/$id");
		$I->submitForm('#categorie-form', [
			'Categorie[description]' => "religion plutôt exotique pour un Européen",
		]);
		$I->seeResponseCodeIsRedirection();
		$I->seeFlashMessage('success');
	}

	/**
	 * @depends updateCandidate
	 */
	public function delete(FunctionalTester $I)
	{
		$I->amLoggedInAs('frollo');
		$I->stopFollowingRedirects();

		$id = (int) Yii::app()->db->createCommand("SELECT id FROM Categorie WHERE categorie = 'jaïnisme'")->queryScalar();
		$I->assertGreaterThan(1, $id);
		$I->sendAjaxPostRequest("/categorie/delete/$id");
		$I->seeResponseCodeIsRedirection();
		$I->seeFlashMessage('success', "Le thème a été supprimé.");
		$I->stopFollowingRedirects();

		$I->sendAjaxPostRequest("/categorie/delete/2");
		$I->seeFlashMessage('error');

		$I->expectThrowable(\CHttpException::class, function() use($I) {
			$I->amOnPage("/categorie/delete/2");
		});
	}

	public function index(FunctionalTester $I)
	{
		$I->amOnPage('/categorie/');
		$I->see("Thématique", 'h1');
		$I->seeElement('#categorie-tree.jstree');
	}

	public function view(FunctionalTester $I)
	{
		$I->amOnPage('/categorie/view/2');
		$I->seeCurrentUrlEquals("/theme/2/Cultures-et-societes");
		$I->see("Cultures et sociétés", 'h1');
		$I->see("Rechercher dans ces revues", 'h2');

		$I->expectThrowable(\CHttpException::class, function() use($I) {
			$I->amOnPage("/categorie/99999999");
		});
	}
}
