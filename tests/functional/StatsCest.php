<?php

class StatsCest
{
	public function activite(FunctionalTester $I)
	{
		$I->amLoggedInAs("milady");
		$I->amOnPage('/stats/activite?partenaireId=1&StatsForm[stateDate]=' . date('Y'));
		$I->see("Activité individuelle");

		$I->expectThrowable(\CHttpException::class, function() use($I) {
			$I->amOnPage('/stats/activite?partenaireId=2');
		});
		$I->expectThrowable(\CHttpException::class, function() use($I) {
			$I->amOnPage('/stats/activite?partenaireId=99999');
		});
		$I->amOnPage('/partenaire/1');
		$I->logout();
	}

	public function editeurs(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/stats/editeurs');
		$I->seeNumberOfElements('section#pays tbody tr', 6);
		$I->seeLink("Madagascar");
		$I->see("30", 'section#idref tr > td:nth-child(2)');
		$I->seeNumberOfElements('section#liens .liens-noms tbody tr', 6);
		$I->seeNumberOfElements('section#liens .liens-domaines tbody tr', 6);
	}

	public function politiques(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/stats/politiques');
		$I->see("Utilisateurs", '#politiques-utilisateurs');
	}

	public function suivi(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/stats/suivi');
		$I->seeNumberOfElements('#suivis-grid tbody tr', 3);

		$I->see("Julliard", 'tr.partenaire-98 > td:nth-child(1)');
		$I->see("Éditeur", 'tr.partenaire-98 > td:nth-child(2)');
		$I->see("0", 'tr.partenaire-98 > td:nth-child(7)');
		$I->see("2", 'tr.partenaire-98 > td:nth-child(8)');

		$I->see("Notre-Dame de Paris", 'tr.partenaire-2 > td:nth-child(1)');
		$I->dontSee("Éditeur", 'tr.partenaire-2 > td:nth-child(2)');
		$I->see("0", 'tr.partenaire-2 > td:nth-child(7)');
		$I->see("2", 'tr.partenaire-2 > td:nth-child(8)');
		$I->see("15", 'tr.partenaire-2 > td:nth-child(10)');
		$I->see("15", 'tr.partenaire-2 > td:nth-child(11)');
	}
}
