<?php

namespace tests\functional\partenaire;

use FunctionalTester;

class PartenaireCest
{
	public function abonnements(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/partenaire/abonnements/1');
		$I->see("Vous avez 10 abonnements actifs sur des ressources", 'p');
	}

	public function admin(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/partenaire/admin');
		$I->seeNumberOfElements('#partenaire-grid tbody tr', 3);
		$I->amOnPage('/partenaire/admin?Partenaire[type]=editeur');
		$I->seeNumberOfElements('#partenaire-grid tbody tr', 1);
	}

	public function carte(FunctionalTester $I)
	{
		$I->amOnPage('/partenaire/carte');
		$I->seeElement('#world-map');
		$I->seeInSource('drawWorldMap(');
	}

	public function createThenDelete(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/partenaire/create');
		$I->fillField("Nom court", 'Z');
		$I->fillField("Présentation", '<p>é</p>');
		$I->click("Créer", '.form-actions');
		$I->seeElement('.alert-error');
		$I->fillField("Nom", 'Zorglub');

		$I->click("Créer", '.form-actions');
		$I->seeCurrentUrlMatches('#/partenaire/\d+$#');
		$I->see("Zorglub", 'h1');
		$partenaire = \Yii::app()->db->createCommand("SELECT * FROM Partenaire WHERE nom = 'Zorglub'")->queryRow();
		$I->assertSame('<p>é</p>', $partenaire['presentation']);

		$I->click("Vue admin", '.btn');
		$I->click("Modifier", '#sidebar');
		$I->seeCurrentUrlMatches('#/partenaire/update/\d+#');
		$I->selectOption("Statut", "Inactif");
		$I->click("Enregistrer", '.form-actions');
		$I->seeCurrentUrlMatches('#/partenaire/view-admin/\d+$#');

		$I->click("Supprimer", '#sidebar');
		$I->seeCurrentUrlMatches('#/partenaire/admin#');
	}

	public function deleteRejected(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->expectThrowable(\CHttpException::class, function () use ($I) {
			$I->amOnPage('/partenaire/delete/1');
		});

		$I->amOnPage('/partenaire/view-admin/1');
		$I->startFollowingRedirects();
		$I->click("Supprimer", '#sidebar');
		$I->seeCurrentUrlMatches('#/partenaire/1#');
		$I->seeElement('.alert-error');
	}

	public function index(FunctionalTester $I)
	{
		$I->amOnPage('/partenaire');
		$I->see("Les 2 membres du réseau Mir@bel", 'h2');
		$I->see("Les 1 éditeurs partenaires de Mir@bel", 'h2');
		$I->see("Les 7 ressources partenaires qui sont suivies dans Mir@bel", 'h2');
	}

	public function possessionParCompletion(FunctionalTester $I)
	{
		$I->amLoggedInAs("frollo");
		$I->startFollowingRedirects();
		$I->amOnPage('/partenaire/possession/2');
		$I->see("Possessions du partenaire", 'h1');
		$I->see("Notre-Dame", 'h1');
		$I->see("Possessions déjà déclarées", 'h2');
		$I->seeNumberOfElements('#possessions-table tbody > tr', 15);

		//$I->wantTo("add a journal");
		$I->fillField("Possession[titreId]", 20);
		$I->fillField("Identifiant Local", "spiritum");
		$I->click("Ajouter ce titre");
		$I->seeNumberOfElements('#possessions-table tbody > tr', 16);
		$I->seeElement('#existing_t20');

		$I->submitForm('#possessions-table', [
			'existing[t20]' => 1,
		]);
		$I->seeNumberOfElements('#possessions-table tbody > tr', 15);
		$I->dontSeeElement('#existing_t20'); // titre.id
	}

	public function possessionSansCompletion(FunctionalTester $I)
	{
		$I->amLoggedInAs("frollo");
		$I->startFollowingRedirects();
		$I->amOnPage('/partenaire/possession/2');
		$I->seeNumberOfElements('#possessions-table tbody > tr', 15);
		$I->fillField("Possession[titreIdComplete]", "Langue française");
		$I->fillField("Identifiant Local", "froufrou");
		$I->click("Ajouter ce titre");
		$I->seeNumberOfElements('#possessions-table tbody > tr', 16);
		$I->seeElement('#existing_t736'); // titre.id
	}

	public function suiviEditeur(FunctionalTester $I)
	{
		$I->amLoggedInAs('milady');
		$I->amOnPage('/partenaire/suivi?id=1&target=Editeur');
		$I->see("1 suivis déjà déclarés", 'h2');
		$I->see("Gallimard", '#suivi-remove-form label');

		$I->wantTo("add 2 new Suivi/Editeur");
		$I->submitForm('#suivi-add-form', [
			'Suivi[cibleId][0]' => 926,
			'Suivi[cibleId][1]' => 1114,
		]);
		$I->see("3 suivis déjà déclarés", 'h2');
		$I->see("Larousse", '#suivi-remove-form label');

		$I->wantTo("remove 2 Suivi/Revue");
		$I->submitForm('#suivi-remove-form', [
			'existing[926]' => 0,
			'existing[1114]' => 0,
			'existing[10]' => 1,
		]);
		$I->see("1 suivis déjà déclarés", 'h2');
		$I->see("Gallimard", '#suivi-remove-form label');
		$I->dontSee("Larousse", '#suivi-remove-form label');
	}

	public function suiviRevue(FunctionalTester $I)
	{
		$I->amLoggedInAs('milady');
		$I->amOnPage('/partenaire/suivi?id=1&target=Revue');
		$I->see("Suivi pour le partenaire", 'h1');
		$I->see("trois mousquetaires", 'h1');
		$I->see("2 suivis déjà déclarés", 'h2');
		$I->see("Actes de la recherche en sciences sociales", '#suivi-remove-form label');
		$I->see("A contrario", '#suivi-remove-form label');

		$I->wantTo("add 2 new Suivi/Revue");
		\Yii::app()->db->createCommand("DELETE FROM IndexingRequiredTitre")->execute();
		$I->submitForm('#suivi-add-form', [
			'Suivi[cibleId][0]' => 6,
			'Suivi[cibleId][1]' => 20,
		]);
		$I->see("4 suivis déjà déclarés", 'h2');
		$I->see("Esprit", '#suivi-remove-form label');
		$I->comment("2 revues suivies => leurs 6 titres sont à réindexer");
		$I->assertSame(6, (int) \Yii::app()->db->createCommand("SELECT count(*) FROM IndexingRequiredTitre")->queryScalar());

		$I->wantTo("remove 2 Suivi/Revue");
		$I->submitForm('#suivi-remove-form', [
			'existing[6]' => 0,
			'existing[20]' => 0,
			'existing[444]' => 1,
			'existing[28]' => 1,
		]);
		$I->see("2 suivis déjà déclarés", 'h2');
		$I->see("Actes de la recherche en sciences sociales", '#suivi-remove-form label');
		$I->see("A contrario", '#suivi-remove-form label');
		$I->dontSee("Esprit", '#suivi-remove-form label');
		$I->assertSame(6, (int) \Yii::app()->db->createCommand("SELECT count(*) FROM IndexingRequiredTitre")->queryScalar());
	}

	public function tableauDeBord(FunctionalTester $I)
	{
		$I->amOnPage('/partenaire/1');
		$I->see("trois mousquetaires", 'h1');
		$I->amLoggedInAs('milady');
		$I->amOnPage('/partenaire/tableau-de-bord/1');
		$I->see("Liens morts dans ces revues", 'h4');
		$I->see("A contrario", '.revue-444');
		$I->see("02/09/2019", '.revue-444');
		$I->seeElement('.revue-444 a[href^="/revue/444"]');
		$I->dontSeeElement('.revue-444 .label');
	}

	public function tableauDeBordTri(FunctionalTester $I)
	{
		$I->amLoggedInAs('milady');
		$I->amOnPage('/partenaire/tableau-de-bord/1?sort=hdatemodif');
		$I->seeElement('.revue-444 a[href^="/revue/444"]');
	}

	public function update(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/partenaire/update/1');
		$I->seeInField("Nom court", "3Mousquetaires");
	}

	public function viewCustomized(FunctionalTester $I)
	{
		// simulate a cookie for removing the customization of display
		$I->setCookie('institute', 'set-0', ['path' => '/']);
		$I->amOnPage('/partenaire/1');
		$I->dontSeeElement('#institute-warning');

		// simulate a cookie for the customization of display
		$I->setCookie('institute', 'set-1', ['path' => '/']);

		$I->amOnPage('/revue/1');
		$I->see("Vous consultez Mir@bel avec la personnalisation trois mousquetaires", '#institute-warning');
		$I->seeLink("mousquetaires", '/partenaire/1');

		$I->amOnPage('/partenaire/1');
		$I->see("trois mousquetaires", 'h1');
		$I->seeInSource('<meta property="og:url" name="og:url" content="http://localhost/partenaire/1" />');
		// auth only
		$I->dontSee("https://possession.example.org/id=IDLOCAL", '.detail-view td');
		$I->dontSee("Utilisateurs associés", 'h2');
		$I->dontSee("Tableau de bord", '#sidebar .nav');
		$I->dontSee("Usage local", '.detail-view th');

		// Change the customization when a user logs in.
		$I->setCookie('institute', '', ['path' => '/']);
		$I->loginAs('frollo');
		$I->amOnPage('/revue/1');
		$I->see("Vous consultez Mir@bel avec la personnalisation Notre-Dame de Paris", '#institute-warning');
	}

	public function viewAdmin(FunctionalTester $I)
	{
		$I->amLoggedInAs('milady');
		$I->amOnPage('/');
		$I->seeElement('.nav ul.dropdown-menu a[href="/partenaire/view-admin/1"]');
		$I->amOnPage('/partenaire/view-admin/1');
		$I->see("trois mousquetaires", 'h1');
		// auth only
		$I->see("Utilisateurs associés", 'h2');
		$I->see("Tableau de bord", '#sidebar .nav');
		$I->see("Usage des données", '.detail-view th');

		$I->expectThrowable(\CHttpException::class, function () use ($I) {
			$I->amOnPage('/partenaire/view-admin/999999');
		});
	}
}
