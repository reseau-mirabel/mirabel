<?php

namespace tests\functional\partenaire;

use FunctionalTester;

class CustomizeCest
{
	public function customizeSearch(FunctionalTester $I)
	{
		$I->amLoggedInAs("frollo"); // not a global admin, but admin-member of Partenaire 2
		$I->amOnPage('/partenaire/view-admin/2');
		$I->click("Gérer les personnalisations", '#sidebar');
		$I->see("Administrer les personnalisations", 'h1');
		$I->dontSeeCheckboxIsChecked("Activer la personnalisation des liens");
		$I->checkOption("Activer la personnalisation des liens");
		$I->click("Enregistrer ces personnalisations", '.form-actions');
		$I->SeeCheckboxIsChecked("Activer la personnalisation des liens");

		$I->amOnPage('/partenaire/view-admin/1');
		$I->dontSee("Personnaliser la recherche", '#sidebar');
		$I->expectThrowable(\CHttpException::class, function () use ($I) {
			$I->amOnPage('/partenaire/customize-search/1');
		});
	}

	public function customizeProxy(FunctionalTester $I)
	{
		$url = 'https://proxy.example.com/q=URL';
		$I->amLoggedInAs("frollo"); // not a global admin, but admin-member of Partenaire 2
		$I->amOnPage('/partenaire/personnaliser/2');
		$I->fillField("URL du proxy", $url);

		$I->click("Enregistrer ces personnalisations", '.form-actions');
		$I->seeCurrentUrlMatches('#/partenaire/2$#');
		$I->see("Personnalisation enregistrée", '.alert-success');
		$I->amOnPage('/partenaire/view-admin/2');
		$I->see($url, '#partenaire-customized .detail-view td');

		$I->amOnPage('/partenaire/personnaliser/2');
		$I->seeInField("URL du proxy", $url);
		$I->fillField("URL du proxy", '');

		$I->click("Enregistrer", '.form-actions');
		$I->see("Personnalisation enregistrée", '.alert-success');
		$I->dontSee($url, '#partenaire-customized .detail-view td');
	}

	public function customizeRcr(FunctionalTester $I)
	{
		$I->amLoggedInAs("frollo"); // not a global admin, but admin-member of Partenaire 2
		$I->amOnPage('/partenaire/personnaliser/2');
		$I->fillField("RCR", "123456789, 987654321");
		$I->checkOption("Affichage public des données liées au RCR");
		$I->uncheckOption("Affichage des données liées au RCR pour les revues non possédées");
		$I->click("Enregistrer ces personnalisations", '.form-actions');

		$I->amOnPage('/partenaire/personnaliser/2');
		$I->seeInField("RCR", "123456789, 987654321");
		$I->seeCheckboxIsChecked("Affichage public des données liées au RCR");
		$I->dontSeeCheckboxIsChecked("Affichage des données liées au RCR pour les revues non possédées");
	}
}
