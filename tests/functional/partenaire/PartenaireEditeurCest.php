<?php

namespace tests\functional\partenaire;

use components\email\Mailer;
use FunctionalTester;

class PartenaireEditeurCest
{
	public function create(FunctionalTester $I)
	{
		$I->wantTo("Create a partenaire-éditeur");
		$this->createPartenaire($I, 2724, "Institut Maurice Thorez");
		$I->wantTo("Add an new user");
		$this->addUser($I, 2724);
		$I->wantTo("Delete users and partenaire-éditeur");
		$this->deletePartenaire($I, 2724);
		$I->comment("created then deleted a partenaire-éditeur");
	}

	private function createPartenaire(FunctionalTester $I, int $editeurId, string $editeurNom)
	{
		$I->amLoggedInAs("milady"); // suivi partenaires-éditeurs
		$I->amOnPage("/editeur/$editeurId");
		$I->click("Créer un partenariat", '#sidebar');
		$I->seeInField("Nom", $editeurNom);
		$I->fillField("Courriel", "thorez@example.org");

		$I->click("Créer", '.form-actions');
		$I->see($editeurNom, 'h1');
		$I->logout();
	}

	private function addUser(FunctionalTester $I, $editeurId)
	{
		$I->amLoggedInAs("milady"); // suivi partenaires-éditeurs
		$I->amOnPage("/editeur/$editeurId");
		$I->click("Partenaire", '#sidebar');

		$I->click("Créer un utilisateur", '#sidebar');
		$I->fillField("Nom", "Zorba");
		$I->fillField("Prénom", "alexis");
		$I->fillField("Nom complet", "Alexis Zorba");
		$I->fillField("Login", "alexis.zorba");
		$I->fillField("Notes privées", "Utilisateur temporaire");
		$I->fillField("Adresse électronique", "alexis.zorba@example.com");
		$I->dontSee("Partenaire", 'label.required');
		$I->dontSee("Authentification par LDAP");
		$I->dontSee("Mot de passe");
		$I->dontSee("Permissions", 'legend');
		$I->dontSee("Suivi", 'legend');

		$I->click("Créer", '.form-actions');
		$I->see("envoi d'un mot de passe", 'h1');

		$I->click("Consulter", '#sidebar');
		$I->see("Notes privées", 'table.detail-view');
		$I->see("Utilisateur temporaire", 'table.detail-view');
		$I->see("Envoyer un mot de passe", '#sidebar');

		$I->logout();
	}

	private function deletePartenaire(FunctionalTester $I, int $editeurId)
	{
		$I->amOnPage("/editeur/$editeurId");
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage("/editeur/$editeurId");
		$I->click("Partenaire", '#sidebar');
		$I->seeCurrentUrlMatches('#/partenaire/view-admin/\d+#');

		$I->wantTo("delete the Utilisateur 1");
		$I->click('.utilisateur a');
		$I->seeCurrentUrlMatches('#/utilisateur/\d+#');
		$I->click("Supprimer", '#sidebar');
		$I->seeResponseCodeIs(200);

		$I->wantTo("delete the Partenaire");
		$I->amOnPage("/editeur/$editeurId");
		$I->click("Partenaire", '#sidebar');
		$I->see("Aucun", '#partenaire-utilisateurs');
		$I->click("Supprimer", '#sidebar');
		$I->seeCurrentUrlMatches('#/partenaire/admin#');
		$I->amOnPage("/editeur/$editeurId");
		$I->dontSee("Partenaire", '#sidebar');

		$I->logout();
	}

	public function suiviAsPartenaireEditeur(FunctionalTester $I)
	{
		$I->amLoggedInAs('editeur-98');
		$I->expectThrowable(\CHttpException::class, function() use($I) {
			$I->amOnPage('/partenaire/suivi?id=1&target=Revue');
		});
		$I->expectThrowable(\CHttpException::class, function() use($I) {
			$I->amOnPage('/partenaire/suivi?id=98&target=Utilisateur');
		});
		$I->startFollowingRedirects();
		$I->amOnPage('/partenaire/suivi?id=98&target=Revue');
		$I->see("Suivi pour le partenaire", 'h1');
		$I->see("Julliard", 'h1');
		$I->see("2 suivis déjà déclarés", 'h2');
		$I->see("Annales historiques de la Révolution française", '#suivi-remove-form label');

		$I->wantTo("require 1 new Suivi/Revue");
		$transport = Mailer::getTransport();
		$transport->reset();
		$I->submitForm('#suivi-add-form', [
			'Suivi[cibleId][0]' => 20,
		]);
		$I->assertEquals(1, $transport->getSentCount());
		$I->see("2 suivis déjà déclarés", 'h2');
	}

	public function updateEditeur(FunctionalTester $I)
	{
		$I->amLoggedInAs('editeur-98'); // partenaire-editeur, partenaire 98, éditeur 5
		$I->amOnPage('/editeur/update/5');
		$I->see("Julliard", 'h1');
		$I->see("Les modifications seront appliquées immédiatement.", '.alert');
		$I->fillField("Sigle", "QQC");

		$I->startFollowingRedirects();
		$I->click("Enregistrer", '.form-actions');
		$I->see("Modification enregistrée", '.alert-success');

		$editeur = \Yii::app()->db->createCommand("SELECT * FROM Editeur WHERE id = 5")->queryRow();
		$I->assertSame('QQC', $editeur['sigle']);
	}

	public function viewPartenaireEditeur(FunctionalTester $I)
	{
		$I->startFollowingRedirects();

		$I->amOnPage('/partenaire/98');
		$I->see("Julliard", 'h1');
		$I->seeElement('a[href="/editeur/5/Julliard"]');

		$I->amLoggedInAs('milady'); // suiviPartenairesEditeurs
		$I->amOnPage('/partenaire/view-admin/98');
		$I->click("Créer un utilisateur", '#sidebar');
		$I->seeResponseCodeIs(200);
	}
}
