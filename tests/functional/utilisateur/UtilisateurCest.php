<?php

namespace tests\functional\utilisateur;

use components\email\SentMessage;
use FunctionalTester;

class UtilisateurCest
{
	public function admin(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/utilisateur/admin');
		$I->seeNumberOfElements("tbody tr", 6);
		$I->amOnPage('/utilisateur/admin?UtilisateurSearch[partenaireType]=normal');
		$I->seeNumberOfElements("tbody tr", 4);
		$I->amOnPage('/utilisateur/admin?UtilisateurSearch[login]=mila');
		$I->seeNumberOfElements("tbody tr", 1);
		$I->amOnPage('/partenaire/1');
	}

	public function suggestLogin(FunctionalTester $I)
	{
		$I->amLoggedInAs("milady");
		$I->amOnPage('/partenaire/1');
		$I->sendAjaxGetRequest('/utilisateur/suggest-login', ['nom' => "d'Alembert", 'prenom' => "N'golo"]);
		$I->seeInSource("m_d-alembert_n");
	}

	public function view(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/utilisateur/view/1');
		$I->see("Charles de Batz de Castelmore d'Artagnan", 'h1');
		$I->see("Administrateur", '.detail-view');
		$I->logout();

		$I->amOnPage('/partenaire/1');
		$I->amLoggedInAs('frollo');
		$I->amOnPage('/utilisateur/3');
		$I->dontSee("Créer", '#sidebar');

		$I->expectThrowable(\CHttpException::class, function() use($I) {
			$I->amOnPage('/utilisateur/999999');
		});
	}

	public function resetPassword(FunctionalTester $I)
	{
		$transport = \components\email\Mailer::getTransport();
		$transport->reset();

		$I->amOnPage("/site/login");
		$I->click('Demander un nouveau mot de passe');
		$I->seeResponseCodeIs(200);
		$I->fillField("Identifiant", "frollo@ndp.example.org");
		$I->click('.form-actions button');
		$I->assertEquals(1, $transport->getSentCount());
		$lastEmail = new SentMessage($transport->getLastSentMessage());
		$I->assertEquals("frollo@ndp.example.org", $lastEmail->getFirstRecipient());
	}
}
