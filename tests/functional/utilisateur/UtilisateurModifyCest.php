<?php

namespace tests\functional\utilisateur;

use components\email\SentMessage;
use FunctionalTester;

class UtilisateurModifyCest
{
	public function create(FunctionalTester $I)
	{
		$transport = \components\email\Mailer::getTransport();
		$transport->reset();

		$I->amLoggedInAs("d'artagnan");
		$I->startFollowingRedirects();
		$I->amOnPage('/partenaire/1');
		$I->click("Vue admin", '.btn');
		$I->click("Créer un utilisateur");
		$I->seeCurrentUrlEquals("/utilisateur/create?partenaireId=1");
		$I->see("Nouvel utilisateur", 'h1');
		$I->seeCheckboxIsChecked("Abonné à la liste [mirabel_contenu]");
		$I->fillField("Nom", "Bartol");
		$I->fillField("Prénom", "Vladimir");
		$I->fillField("Login", "vlad");
		$I->fillField("Adresse électronique", "vlad@example.com");

		$I->click("Créer", '.form-actions');
		$I->seeCurrentUrlMatches('#^/utilisateur/message-password/#');
		$I->see("envoi d'un mot de passe", 'h1');
		$I->seeInField("Sujet", "Bienvenue sur le réseau Mir@bel");
		$I->assertEquals(0, $transport->getSentCount());

		$I->click("Envoyer", '.form-actions');
		$I->seeCurrentUrlMatches('#^/utilisateur/\d+$#');
		$I->see("vlad@example.com");
		$I->assertEquals(1, $transport->getSentCount());
		$lastEmail = new SentMessage($transport->getLastSentMessage());
		$I->assertEquals("vlad@example.com", $lastEmail->getFirstRecipient());
	}


	public function createFails(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->startFollowingRedirects();
		$I->amOnPage('/partenaire/1');
		$I->click("Vue admin", '.btn');
		$I->click("Créer un utilisateur");
		$I->fillField("Nom", "Grossman");
		$I->fillField("Prénom", "Vassili");
		$I->fillField("Login", "vassia");
		$I->fillField("Adresse électronique", "anonyme@politique.localhost");

		$I->click("Créer", '.form-actions');
		$I->see("Cette adresse électronique est déjà présente dans Mir@bel.", '.help-inline.error');
	}

	public function createMulti(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->startFollowingRedirects();
		$I->amOnPage('/utilisateur/create-multi');
		$I->see("Création d'utilisateurs par lot");

		$I->fillField("Partenaire", "Volatil");
		$I->fillField("Nombre", "3");
		$I->fillField("Préfixe login", "vol");
		$I->fillField("Mot de passe", "mdp");
		$I->click("Créer", '.form-actions');

		$I->seeElement('.alert-success');
		$I->seeNumberOfElements('#utilisateur-grid tbody tr', 3);
		$I->see("Volatil", '#utilisateur-grid tbody tr');
		$I->click("vol01", '#utilisateur-grid tbody');
		$I->see("Utilisateur prénom01 nom01 (vol01)", 'h1');
	}

	public function deleteFails(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->stopFollowingRedirects();
		$I->amOnPage("/utilisateur/3");
		$I->dontSeeFlashMessage('error');

		$I->wantToTest("An user who authored an Intervention can't be deleted");
		$I->click("Supprimer", '#sidebar');
		$I->seeResponseCodeIsRedirection();
		$I->seeFlashMessage('error', "il est déjà intervenu au moins pour /intervention/");

		$I->wantToTest("GET deletion fails");
		$I->expectThrowable(\CHttpException::class, function() use($I) {
			$I->amOnPage('/utilisateur/delete-totally/3');
		});
	}

	public function disable(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");

		$I->stopFollowingRedirects();
		$I->sendAjaxPostRequest('/utilisateur/delete/219');
		$I->seeFlashMessage('warning', "déclarer des politiques d'éditeurs");
		$I->seeFlashMessage('warning', "Julliard");
		$I->startFollowingRedirects();

		$I->amOnPage('/utilisateur/219');
		$I->dontSeeElement('#utilisateur-editeurs');
	}

	public function removeEditeur(FunctionalTester $I)
	{
		$I->amLoggedInAs("milady");
		$I->stopFollowingRedirects();
		$I->sendAjaxPostRequest(
			'/utilisateur/remove-editeur/100', // login: u-pol
			[
				'editeurId' => '3370',
			]
		);
		$I->seeResponseCodeIsRedirection();
		$I->seeFlashMessage('success');

		$I->amOnPage('/utilisateur/politiques-editeurs/100');
		$I->dontSee("Société des études robespierristes");
	}

	public function updateAsAdmin(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/utilisateur/update/2');
		$I->fillField('Login', "milady@3mousquetaires.example.org");
		$I->click("Enregistrer", '.form-actions');
		$I->seeCurrentUrlEquals('/utilisateur/2');

		// As an admin, the login field can still be modified, even when it equals the email.
		$I->amOnPage('/utilisateur/update/2');
		$I->fillField('Login', "ceciesttemporaire");
		$I->click("Enregistrer", '.form-actions');
		$I->seeCurrentUrlEquals('/utilisateur/2');
		$I->see("ceciesttemporaire");
	}

	public function updateSelf(FunctionalTester $I)
	{
		$I->amLoggedInAs("milady");
		$I->amOnPage('/utilisateur/view/2'); // updateown
		$I->assertTrue((bool) \Yii::app()->user->getState('listeDiffusion'));
		$I->see("Liste de diffusion", '.detail-view tr:last-child th');
		$I->see("Oui", '.detail-view tr:last-child td');
		$I->click("Modifier", '#sidebar a');
		$I->seeCheckboxIsChecked("Abonné à la liste [mirabel_contenu]");
		$I->uncheckOption("Abonné à la liste [mirabel_contenu]");
		$I->submitForm('#utilisateur-form', ['User[listeDiffusion]' => '0']);
		$I->see("Liste de diffusion", 'tr.liste-contenus > th');
		$I->see("Non", '.detail-view tr.liste-contenus > td');

		$I->expectThrowable(\CHttpException::class, function() use($I) {
			$I->amOnPage('/utilisateur/update/1');
		});
	}

	public function updateSelfLdap(FunctionalTester $I)
	{
		$I->amLoggedInAs("aramis");
		$I->amOnPage('/utilisateur/view/4');
		$I->click("Modifier", '#sidebar a');

		$I->see("Vous êtes authentifié par LDAP.", 'p.info');
		$I->dontSee("Authentification");
		$I->seeCheckboxIsChecked("Abonné à la liste [mirabel_contenu]");
		$I->uncheckOption("Abonné à la liste [mirabel_contenu]");
		$I->submitForm('#utilisateur-form', ['User[listeDiffusion]' => '0']);
		$I->see("Liste de diffusion", 'tr.liste-contenus > th');
		$I->see("Non", '.detail-view tr.liste-contenus > td');
	}

	public function updateDisable(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");

		$I->amOnPage('/utilisateur/219');
		$I->seeElement('#utilisateur-editeurs');
		$I->see("responsable", '#utilisateur-editeurs');
		$I->see("Julliard", '#utilisateur-editeurs li');

		$I->amOnPage('/utilisateur/update/219');
		$I->uncheckOption("Actif");
		$I->stopFollowingRedirects();
		$I->submitForm('#utilisateur-form', ['User[actif]' => '0']);
		$I->seeFlashMessage('success', "a été désactivé");
		$I->seeFlashMessage('warning', "déclarer des politiques d'éditeurs");
		$I->seeFlashMessage('warning', "Julliard");
		$I->startFollowingRedirects();

		$I->amOnPage('/utilisateur/219');
		$I->dontSeeElement('#utilisateur-editeurs');
	}
}
