<?php

namespace tests\functional\utilisateur;

use FunctionalTester;

class UtilisateurPolitiqueCest
{
	public function politiqueEditeursToSelf(FunctionalTester $I)
	{
		$I->amLoggedInAs("milady");
		$I->wantTo("Add publishers to myself");
		$I->stopFollowingRedirects();

		$I->amOnPage('/utilisateur/politiques-editeurs');
		$I->see("Responsabilités actuelles", 'h2');
		$I->see("Aucun éditeur.", 'p');
		$I->sendAjaxPostRequest(
			'/utilisateur/politiques-editeurs',
			[
				'UserPolitique[editeurIds]' => '2847,3370',
				'UserPolitique[roles][2847]' => 'owner',
				'UserPolitique[roles][3370]' => 'guest',
			]
		);
		$I->seeFlashMessage('success', "Rôles ajoutés.");

		$I->amOnPage('/utilisateur/politiques-editeurs');
		$I->dontSee("Aucun éditeur.", 'p');
		$I->see("Société des études robespierristes");
		$I->see("responsable");
		$I->see("délégué");
	}

	public function politiqueEditeursToSomeone(FunctionalTester $I)
	{

		$I->amLoggedInAs("milady");
		$I->wantTo("Add a publisher to an Utilisateur-politiques");
		$I->stopFollowingRedirects();

		$I->amOnPage('/utilisateur/politiques-editeurs/100');
		$I->dontSee("Société des études robespierristes");
		$I->dontSee("délégué");
		$I->sendAjaxPostRequest(
			'/utilisateur/politiques-editeurs?id=100', // login: u-pol
			[
				'UserPolitique[roles][3370]' => 'guest',
				'UserPolitique[editeurIds]' => '3370',
			]
		);
		$I->seeResponseCodeIsRedirection();
		$I->seeFlashMessage('success', "Rôle ajouté.");
		$I->amOnPage('/utilisateur/politiques-editeurs/100');
		$I->see("Société des études robespierristes");
		$I->see("délégué");
	}
}
