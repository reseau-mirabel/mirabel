<?php

class CollectionCest
{
	public function ajaxComplete(FunctionalTester $I)
	{
		$I->amLoggedInAs("frollo");
		$I->amOnPage('/collection/ajax-complete?term=stat');
		$json = $I->grabPageSource();
		$I->assertEquals('[{"id":53,"label":"Mathematics & Statistics","value":"Mathematics & Statistics"},{"id":73,"label":"Mathematics & Statistics Legacy","value":"Mathematics & Statistics Legacy"}]', $json);
	}

	public function ajaxList(FunctionalTester $I)
	{
		$I->amLoggedInAs("frollo");
		$I->amOnPage('/collection/ajax-list?ressourceId=144');
		$response = json_decode($I->grabPageSource());
		$I->assertCount(2, $response);
		$I->assertEquals("Cambridge Core   (courant)", $response[0]->label);
	}

	public function createFails(FunctionalTester $I)
	{
		$I->amLoggedInAs("frollo"); // member of Partenaire with id = 2

		// cannot mix Tmp with other types
		$I->stopFollowingRedirects();
		$I->amOnPage('/collection/create-tmp?ressourceId=4');
		$I->seeFlashMessage('error');
		$I->seeResponseCodeIsRedirection();

		// Suivi by another Partenaire (id 1)
		$I->expectThrowable(\CHttpException::class, function() use($I) {
			$I->amOnPage('/collection/create?ressourceId=296');
		});

		// Ressource does not exist
		$I->expectThrowable(\Exception::class, function() use($I) {
			$I->amOnPage('/collection/create-tmp?ressourceId=9999');
		});
	}

	public function create(FunctionalTester $I)
	{
		$I->amLoggedInAs("frollo"); // member of Part2
		$I->startFollowingRedirects();
		$I->amOnPage('/collection/create?ressourceId=143');
		$I->seeCurrentUrlEquals('/collection/create?ressourceId=143');
		$I->fillField("Nom", "Bidon");
		$I->selectOption("Type", "Archive");
		$I->fillField("Description", "Ben j'en <b>sais</b> rien !");
		$I->fillField("Identifiant d'import", "Didon");

		$I->stopFollowingRedirects();
		$I->click("Créer", '.form-actions');
		$I->seeResponseCodeIsRedirection();
		$I->seeFlashMessage('success', "créée");

		$I->startFollowingRedirects();
		$I->amOnPage("/ressource/143");
		$I->see("Bidon", '#ressource-collections');

		$id = (int) Yii::app()->db->createCommand("SELECT MAX(id) FROM Collection WHERE id > 102 AND nom = 'Bidon'")->queryScalar();
		$this->update($I, $id);
	}

	private function update(FunctionalTester $I, int $id)
	{
		$I->startFollowingRedirects();
		$I->amOnPage("/collection/update/$id");
		$I->fillField("Nom", "Falsification");
		$I->click("Enregistrer", '.form-actions');
		$I->amOnPage("/ressource/143");
		$I->see("Falsification", '#ressource-collections');
	}

	public function createTmp(FunctionalTester $I)
	{
		$I->amLoggedInAs("milady");
		$I->startFollowingRedirects();
		$I->amOnPage('/collection/create?ressourceId=402');
		$I->seeCurrentUrlEquals('/collection/create-tmp?ressourceId=402');
		$I->fillField("Nom", "Bidon");
		$I->selectOption("Type", "Temporaire");
		$I->fillField("Description", "Ben j'en <b>sais</b> rien !");
		$I->fillField("Identifiant d'import", "Didon");

		$I->stopFollowingRedirects();
		$I->click("Créer", '.form-actions');
		$I->seeFlashMessage('success');

		$I->startFollowingRedirects();
		$I->amOnPage("/ressource/402");
		$I->see("Bidon", '#ressource-collections');
		$I->see("Ben j'en <b>sais</b> rien !", '.collection-description');
	}

	public function delete(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/collection/delete/2');
		$I->see("Cette collection contient 17 accès sur des titres.", 'p');
		$I->dontSee("accès liés à cette seule collection seront supprimés.", '.alert-danger');
		$I->see("Il y a 2 abonnements.", '.alert-danger');
		$I->dontSee('button.btn-danger');

		$I->amOnPage('/collection/delete/68');
		$I->see("Les 1 accès liés à cette seule collection seront supprimés.", '.alert-danger');
		$I->stopFollowingRedirects();
		$I->click('button.btn-danger');
		$I->seeResponseCodeIsRedirection();
		$I->seeFlashMessage('success');

		$I->expectThrowable(\CHttpException::class, function() use($I) {
			$I->amOnPage('/collection/delete/68');
		});

		$I->wantTo("revert collection/delete");
		$I->amOnPage('/intervention/admin?q[statut]=accepté&sort=hdateVal.desc');
		$I->click("tbody tr:first-child a.view");
		$I->seeCurrentUrlMatches('#^/intervention/\d+$#');
		$I->stopFollowingRedirects();
		$I->click("Annuler cette intervention", '#sidebar'); // Collection record
		$I->seeResponseCodeIsRedirection();
		$I->seeFlashMessage('success');
		$I->amOnPage('/intervention/admin?q[statut]=accepté&sort=hdateVal.desc');
		$I->click("tbody tr:first-child a.view");
		$I->click("Annuler cette intervention", '#sidebar'); // Service records
		$I->seeResponseCodeIsRedirection();
		$I->seeFlashMessage('success');

		$I->amOnPage('/collection/delete/68');
		$I->seeResponseCodeIsSuccessful();
		$I->see("Les 1 accès liés à cette seule collection seront supprimés.", '.alert-danger');
	}

	public function index(FunctionalTester $I)
	{
		$I->amLoggedInAs("milady");
		$I->amOnPage('/collection/index?ressourceId=143');
		$I->seeNumberOfElements('tbody tr', 40);
		$I->expectThrowable(\CHttpException::class, function() use($I) {
			$I->amOnPage('/collection/index');
		});
		$I->expectThrowable(\CHttpException::class, function() use($I) {
			$I->amOnPage('/collection/index?ressourceId=A');
		});
	}

	public function view(FunctionalTester $I)
	{
		$I->amLoggedInAs("milady");
		$I->stopFollowingRedirects();
		$I->amOnPage('/collection/98');
		$I->seeResponseCodeIsRedirection();
	}
}
