<?php

class HomeCest
{
	public function homeAsGuest(FunctionalTester $I)
	{
		$I->amOnPage('/');
		$I->see("Rechercher une revue", 'h2');
		if (Yii::app()->params->itemAt('displayThemesOnHomePage')) {
			$I->see("Thématiques des revues", 'h2');
		} else {
			$I->dontSee("Thématiques des revues", 'h2');
		}
		$I->see("actualité", '#bloc-accueil-actu');

		$I->click("Connexion", 'header .nav');
		$I->see("Connexion", 'h1');
	}

	public function homeAsAuthenticated(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/');
		$I->see("Rechercher une revue", 'h2');
		$I->dontSeeLink("Connexion", '/site/login', 'footer');

		$I->see("Informations réservées aux partenaires", 'h2');
		$I->see("Modifications suivies", 'h3');
	}
}
