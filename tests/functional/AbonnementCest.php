<?php

class AbonnementCest
{
	public function proxy(FunctionalTester $I)
	{
		$I->amLoggedInAs('frollo');
		$I->setCookie('institute', '2');
		$I->amOnPage('/ressource/137/Dalloz');
		$I->seeNumberOfElements('#ressource-collections tbody tr', 1);
		$I->see("Actions/partenaire", '#ressource-collections thead th', 1);
		$I->see("proxy", '#ressource-collections .label-success span');
		$I->click('.label-success .btn-mini');
		$I->dontSee("proxy", '.label-success span');
		$I->see("proxy", '.label-warning span');

		$I->expectThrowable(\CHttpException::class, function() use($I) {
			$I->amOnPage('/abonnement/toggle-proxy/18'); // not POST => 400
		});
		$I->expectThrowable(\CHttpException::class, function() use($I) {
			$I->sendAjaxPostRequest('/abonnement/toggle-proxy/999999');
		});
	}

	public function proxyUrl(FunctionalTester $I)
	{
		$I->amLoggedInAs('frollo');
		$I->stopFollowingRedirects();
		$I->sendAjaxPostRequest(
			'/abonnement/update-proxyurl',
			['id' => 18, 'proxyurl' => '/myproxy']
		);
		$I->seeResponseCodeIsRedirection();
		$I->seeFlashMessage('error');

		Yii::app()->user->getFlashes();
		$I->sendAjaxPostRequest(
			'/abonnement/update-proxyurl',
			['id' => 18, 'proxyurl' => 'https://example.com/myproxy/URL']
		);
		$I->seeResponseCodeIsRedirection();
		$I->dontSeeFlashMessage('error');

		$I->expectThrowable(\CHttpException::class, function() use($I) {
			$I->amOnPage('/abonnement/update-proxyurl?id=18&urlproxy=something'); // not POST => 400
		});
	}

	public function ressources(FunctionalTester $I)
	{
		$I->amLoggedInAs('frollo');
		$I->amOnPage('/partenaire/abonnements/2');
		$I->see("Abonnements du partenaire Notre-Dame de Paris", 'h1');
		$I->see("Vous avez 15 abonnements actifs sur des ressources.", 'p');
		$I->seeNumberOfElements('.abonnement-delete', 15);

		$I->click("Se désabonner", '.ressource-137');
		$I->see("Vous avez 14 abonnements actifs sur des ressources.", 'p');
		$I->dontSee("Se désabonner", '.ressource-137');

		$I->amOnPage('/partenaire/abonnements/2?AbonnementSearch%5BressourceNom%5D=dalloz');

		$I->click("Masquer", '.ressource-137');
		$I->see("Vous avez 14 abonnements actifs sur des ressources et 1 ressources masquées", 'p');

		$I->click("Afficher", '.ressource-137');
		$I->dontSee("et 1 ressources masquées", 'p');

		$I->click("S'abonner", '.ressource-137');
		$I->see("Vous avez 15 abonnements actifs sur des ressources.", 'p');
	}
}
