<?php

namespace tests\functional\revue;

class RevueSearchCest
{
	public function byAbo(\FunctionalTester $I)
	{
		$I->amLoggedInAs('milady');
		$I->amOnPage('/revue/search?q%5Babonnement%5D=1&q%5BaccesLibre%5D=1');
		$I->see("total de 18", '#extended-search-results .summary');

		$I->amOnPage('/revue/search?q%5BaboCombine%5D=ET&q%5Babonnement%5D=1&q%5Bowned%5D=1&q%5Bpid%5D=1');
		$I->see("abonné en ligne ET disponible", '#search-summary');
		$I->see("total de 8", '#extended-search-results .summary');
		$I->seeNumberOfElements('script.navigation-storage-init', 1);
		$json = $I->grabTextFrom('script#navigation-storage-init-revue');
		$decoded = json_decode($json, false);
		$I->assertCount(8, $decoded->results);

		$I->amOnPage('/revue/search?q%5BaboCombine%5D=OU&q%5Babonnement%5D=1&q%5Bowned%5D=1&q%5Bpid%5D=1');
		$I->see("abonné en ligne OU disponible", '#search-summary');
		$I->see("total de 17", '#extended-search-results .summary');
	}

	public function byAccess(\FunctionalTester $I)
	{
		$I->amOnPage('/revue/search?q%5BaccesLibre%5D=1&q%5Bacces%5D=2');
		$I->seeNumberOfElements('#search-summary em', 2);
		$I->see("Accès libre", '#search-summary em');
		$I->see("Texte intégral", '#search-summary em');
		$I->see("total de 15", '#extended-search-results .summary');

		$I->amOnPage('/revue/search?q%5Bmonitored%5D=1&q%5BsansAcces%5D=1');
		$I->seeCurrentUrlEquals('/revue/5/Commentaire'); // single result => redirection
	}

	public function byCollection(\FunctionalTester $I)
	{
		$I->amOnPage('/revue/search?q%5BcollectionId%5D=6&q%5BpaysId%5D=62');
		$I->seeNumberOfElements('#search-summary em', 2);
		$I->see("Bouquet Travail social", '#search-summary em');
		$I->see("France", '#search-summary em');
		$I->see("total de 2", '#extended-search-results .summary');
	}

	public function byDate(\FunctionalTester $I)
	{
		$I->amOnPage('/revue/search?q%5BhdateModif%5D=2021-03');
		$I->see("total de 4", '#extended-search-results .summary');
	}

	public function byIssn(\FunctionalTester $I)
	{
		$I->amOnPage('/revue/search?q%5Bissn%5D=0002-0478');
		$I->seeCurrentUrlMatches('#revue/2/#'); // Single result => redirection
	}

	public function byLang(\FunctionalTester $I)
	{
		$I->amOnPage('/revue/search?q%5Blangues%5D=!');
		// Le seul titre sans langue est obsolète.
		// Or ce critère ne s'applique qu'au dernier titre de chaque revue.
		$I->see("Aucun résultat trouvé.");

		$I->amOnPage('/revue/search?q%5Blangues%5D=eng');
		// 4 titres, 3 revues
		$I->seeNumberOfElements('#extended-search-results .items > div', 3);

		$I->amOnPage('/revue/search?q%5Blangues%5D=eng,ger');
		$I->seeNumberOfElements('#extended-search-results .items > div', 3);

		$I->setCookie('searchres', (string) \widgets\CustomizePager::VIEWMODE_SIMPLE);
		$I->setCookie('pagesize', '25');
		$I->amOnPage('/revue/search?q%5Blangues%5D=eng,fre');
		$I->seeNumberOfElements('#extended-search-results .items > div', 20); // pagination
		$I->see("total de 20", '#extended-search-results .summary');
	}

	public function byLinks(\FunctionalTester $I)
	{
		$I->amOnPage('/revue/search?q%5Blien%5D=19%2C-8');
		$I->seeNumberOfElements('#search-summary em', 2);
		$I->see("Avec lien HAL", '#search-summary em');
		$I->see("Sans lien Twitter", '#search-summary em');
		$I->see("total de 13", '#extended-search-results .summary');
	}

	public function byMonitoring(\FunctionalTester $I)
	{
		$I->setCookie('institute', '1');
		$I->amOnPage('/revue/search?q%5BmonitoredByMe%5D=1&q%5Bpid%5D=1');
		$I->seeNumberOfElements('#search-summary em', 1);
		$I->see("3Mousquetaires", '#search-summary em');
		$I->see("total de 2", '#extended-search-results .summary');
		$I->dontSee("Disponible", '#criteres-mirabel label');

		$I->amOnPage('/revue/search?q%5Bmonitored%5D=2'); // sans suivi
		$I->see("total de 15", '#extended-search-results .summary');
	}

	public function byPublisher(\FunctionalTester $I)
	{
		$I->amOnPage('/revue/search?q%5BediteurId%5D=23');
		$I->seeCurrentUrlMatches('#^/revue/98/#'); // single result

		$I->amOnPage('/revue/search?q%5BediteurId%5D=23&q%5Btitre%5D=biribi');
		$I->see('Aucun résultat trouvé.'); // no result
	}

	public function bySuivi(\FunctionalTester $I)
	{
		// Partenaire.id = 1
		$I->amOnPage('/revue/search?q%5Bsuivi%5D=1');
		$I->see("total de 2", '#extended-search-results .summary');

		// Partenaire.id IN (1, 2)
		$I->amOnPage('/revue/search?q%5Bsuivi%5D=1,2');
		$I->see("total de 4", '#extended-search-results .summary');
	}

	public function byTextAndCountry(\FunctionalTester $I)
	{
		$I->amOnPage('/revue/search');
		$I->fillField("Titre", "cont");
		$I->click("Rechercher");
		$I->see("Résultats de 1 à 2");

		$I->selectOption("Pays de l'éditeur", "France");
		$I->click("Rechercher");
		$I->see("Afrique Contemporaine", 'h1');
	}
}
