<?php

namespace tests\functional\revue;

use CHttpException;
use Exception;
use FunctionalTester;

class RevueCest
{
	public function ajaxComplete(FunctionalTester $I)
	{
		$I->amOnPage('/revue/ajax-complete?term=juridique');
		$decoded = json_decode($I->grabPageSource());
		\PHPUnit\Framework\assertCount(1, $decoded);
		\PHPUnit\Framework\assertSame("Actualité Juridique Droit Administratif — AJDA", $decoded[0]->label);
	}

	public function carte(FunctionalTester $I)
	{
		$I->amOnPage('/revue/carte');
		$I->see("Carte des 20 revues", 'figcaption');
	}

	public function delete(FunctionalTester $I)
	{
		$revueId = 1954;
		$title = "E-santé";
		$publisherName = "Université de Mahajanga";

		$I->amOnPage("/revue/$revueId");
		$I->see("Suivi", 'h2');
		$I->dontSee("Supprimer cette revue");

		$I->amLoggedInAs('milady');
		$I->amOnPage("/revue/$revueId");
		$I->see($title);
		$I->see("Suivi", 'h2');
		$I->click("Supprimer…", '#sidebar a');
		$I->fillField('input[name="annoying"]', "Je confirme");
		$I->click("Supprimer", '.btn-danger');
		$I->seeElement('.alert-success');
		$I->see($title);
		$I->see("Éditeur orphelin", '.alert-danger');
		$I->see($publisherName, '.alert-danger');
	}

	public function export(FunctionalTester $I)
	{
		$header = 'titre;"ID de titre";"ID de revue";ISSN;ISSN-E;"préfixe du titre";sigle;"URL Mir@bel"';

		$I->comment("Revues trouvées par leurs éditeurs actuels");
		$I->amOnPage('/revue/export?q[editeurId]=19');
		$csv = $I->grabPageSource();
		$rows = array_filter(explode("\n", $csv));
		$I->assertCount(3, $rows);
		$I->assertEquals($header, $rows[0]);

		$I->comment("Revues trouvées par leurs éditeurs actuels ou anciens");
		$I->amOnPage('/revue/export?q[aediteurId]=19');
		$csv = $I->grabPageSource();
		$rows = array_filter(explode("\n", $csv));
		$I->assertCount(4, $rows);
		$I->assertEquals($header, $rows[0]);

		$I->amOnPage('/revue/export?extended=1&q[aediteurId]=19');
		$csv = $I->grabPageSource();
		$rows = array_filter(explode("\n", $csv));
		$I->assertCount(4, $rows);
		$I->assertEquals($header, $rows[0]);

		$I->amLoggedInAs("milady");
		$I->amOnPage('/revue/export?extended=1&q[aediteurId]=19');
		$csv = $I->grabPageSource();
		$rows = array_filter(explode("\n", $csv));
		$I->assertCount(4, $rows);
		$I->assertStringStartsWith('possession', $rows[0]);

		$I->expectThrowable(Exception::class, function() use($I) {
			$I->amOnPage('/revue/export?q[editeurId]=999999');
		});
	}

	public function retireCategorie(FunctionalTester $I)
	{
		$I->amLoggedInAs('frollo');
		$I->stopFollowingRedirects();

		$I->sendAjaxPostRequest("/revue/retire-categorie?id=444&categorieId=2");
		$I->seeResponseCodeIsRedirection();
		$I->seeFlashMessage('success');

		$I->expectThrowable(CHttpException::class, function() use($I) {
			$I->sendAjaxPostRequest("/revue/retire-categorie?id=444&categorieId=2");
		});
	}

	public function searchDetenu(FunctionalTester $I)
	{
		$I->amLoggedInAs('milady');
		$I->amOnPage('/revue/search?q%5Bdetenu%5D=2');
		$I->seeNumberOfElements('#search-summary em', 1);
		$I->see("Notre-Dame de Paris", '#search-summary em');
		$I->see("total de 13", '#extended-search-results .summary');
		$I->see("Disponible", '#criteres-mirabel label');

		$I->checkOption('#SearchTitre_acces_0[value=2]');
		//$I->checkOption("Texte intégral", '#extended-search-results');
		$I->click("Rechercher", '.form-actions');
		$I->seeNumberOfElements('#search-summary em', 2);
		$I->see("Notre-Dame de Paris", '#search-summary em');
		$I->see("total de 13", '#extended-search-results .summary');
	}

	public function verify(FunctionalTester $I)
	{
		$I->amOnPage('/revue/28/Actes-de-la-recherche-en-sciences-sociales-ARSS');
		$I->see("Dernière vérification : 19/01/2016", '#modif-verif');

		$I->wantToTest("anonymous verification is prohibited");
		$I->stopFollowingRedirects();
		$I->expectThrowable(CHttpException::class, function() use($I) {
			$I->amOnPage('/revue/verify?id=28');
		});

		$I->amLoggedInAs('milady');
		$I->wantToTest("verification without reponsability is prohibited");
		$I->expectThrowable(CHttpException::class, function() use($I) {
			$I->amOnPage('/revue/verify?id=6');
		});

		$I->wantToTest("verification with reponsability changes the date");
		$I->sendAjaxPostRequest('/revue/verify?id=28');
		$I->seeResponseCodeIsRedirection();
		$I->amOnPage('/revue/28/Actes-de-la-recherche-en-sciences-sociales-ARSS');
		$I->see("Dernière vérification : " . date('d/m/Y'), '#modif-verif');
	}

	public function view(FunctionalTester $I)
	{
		$I->setCookie('institute', 'set-0');
		$I->amOnPage('/revue/2');
		$I->seeCurrentUrlMatches('/Afrique-Contemporaine/');
		$I->see("français", '#revue-titre-actif .detail-view');
		$I->seeInSource('<meta property="og:description" name="og:description" content="Accès en ligne aux contenus de la revue Afrique Contemporaine" />');
		$I->dontSee("Regrouper les accès", "a");
		$I->dontSee("Afficher le détail", "a");

		// simulate a cookie for the customization of display
		$I->setCookie('institute', 'set-1');
		$I->amOnPage('/revue/2');
		$I->dontSee("Regrouper les accès", "a");
		$I->seeLink("Afficher le détail");

		$I->click("Afficher le détail");
		$I->dontSee("Afficher le détail", "a");
		$I->seeLink("Regrouper les accès");

		$I->setCookie('institute', '');

		$I->expectThrowable(Exception::class, function() use($I) {
			$I->amOnPage('/revue/999999999');
		});
	}

	public function viewBy(FunctionalTester $I)
	{
		$I->amOnPage('/revue/issn/0002-0478');
		$I->seeCurrentUrlMatches('/Afrique-Contemporaine/');

		$I->amOnPage('/revue/titre/esprit');
		$I->seeCurrentUrlEquals('/revue/6/Esprit');

		$I->amOnPage('/revue/titre-id/1564');
		$I->seeCurrentUrlMatches('#/revue/1174/#');

		$I->expectThrowable(Exception::class, function() use($I) {
			$I->amOnPage('/revue/issn/1234');
		});
		$I->expectThrowable(Exception::class, function() use($I) {
			$I->amOnPage('/revue-by?by=issnl&id=1234');
		});
	}

	public function viewMultipleLang(FunctionalTester $I)
	{
		$I->amOnPage('/revue/1265');
		$I->see("Biscuit chinois", 'h1');
		$I->see("français, anglais", '#revue-titre-actif .detail-view');
	}

	public function viewWithCanonicalUrl(FunctionalTester $I)
	{
		$I->amOnPage('/revue/29');
		$I->seeElement('link[rel="canonical"][href="http://localhost/revue/29/Annales-historiques-de-la-Revolution-francaise"]');
		$I->amOnPage('/revue/366');
		$I->seeElement('link[rel="canonical"][href="http://localhost/revue/366/Slence"]');
		$I->amOnPage('/revue/718');
		$I->seeElement('link[rel="canonical"][href="http://localhost/revue/718/Annales-Histoire-Sciences-Sociales"]');
		// seeElement() cannot find META tags.
		$I->seeInSource('<meta property="og:url" name="og:url" content="http://localhost/revue/718/Annales-Histoire-Sciences-Sociales" />');
	}
}
