<?php

namespace tests\functional\revue;

use FunctionalTester;

class EtatcollectionCest
{
	public function partenaireSansRcr(FunctionalTester $I)
	{
		$I->setCookie('institute', '1');
		$I->amOnPage('/revue/2'); // avec possession
		$I->see("Disponible dans votre bibliothèque", '#revue-titres td');

		$I->amOnPage('/revue/103'); // sans possession
		$I->dontSee("Disponible dans votre bibliothèque", '#revue-titres td');
	}

	public function partenaireAvecRcr(FunctionalTester $I)
	{
		$I->setCookie('institute', 'set-2');

		// possessions et etatcollection
		$I->amOnPage('/revue/2');
		$I->see("Disponible dans votre bibliothèque", '#revue-titres tr[data-titreid=2]');
		$I->seeElement('#revue-titres td details'); // étatcollection long => replié
		$I->see("titre par sudoc-rcr avec possession", '#revue-titres tr[data-titreid=2]');

		// etatcollection sans possession
		$I->amOnPage('/revue/20');
		$I->see("titre par sudoc-rcr sans possession", '#revue-titres tr[data-titreid=1399]');
		$I->see("Disponible dans votre bibliothèque", '#revue-titres');

		// possession sans etatcollection
		$I->amOnPage('/revue/718');
		$I->see("Disponible dans votre bibliothèque", '#revue-titres tr[data-titreid=716]');
		$I->dontSee("Disponible dans votre bibliothèque", '#revue-titres tr[data-titreid^=1]');

		// ni possession ni etatcollection
		$I->amOnPage('/revue/1954');
		$I->dontSee("Disponible dans votre bibliothèque", '#revue-titres');

		$I->setCookie('institute', '');
	}
}
