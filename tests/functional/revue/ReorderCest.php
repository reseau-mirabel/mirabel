<?php

namespace tests\functional\revue;

use FunctionalTester;

class ReorderCest
{
	public function alert(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");

		// No problem with the journal chain.
		$I->amOnPage('/revue/1060');
		$I->dontSeeElement('#sidebar .glyphicon-warning-sign');
		$I->amOnPage('/revue/trier-titre/1060');
		$I->dontSeeElement('.alert-warning');
		$I->dontSeeElement('.alert-error');

		// Duplicate element in the journal chain.
		$I->amOnPage('/revue/718');
		$I->seeElement('#sidebar .glyphicon-warning-sign');
	}

	public function sameOrder(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/revue/trier-titre/718');
		$I->seeNumberOfElements('form#reorder tbody tr', 6);
		$I->see("rend obsolète plusieurs titres", '.alert-warning');
		$I->dontSeeElement('.alert-error');

		$I->submitForm('form#reorder', ['order' => ["716", "1900", "1082", "1083", "1084", "725"]]);
		$I->seeElement('.alert-success');
		$I->dontSeeElement('.alert-error');
		$I->seeNumberOfElements('form#reorder tbody tr', 6);
	}

	public function wrongOrder(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/revue/trier-titre/718');
		$I->submitForm('form#reorder', ['order' => []]); // empty
		$I->see("L'ordre que vous avez fourni est invalide.", '.alert-error');

		$I->amOnPage('/revue/trier-titre/718');
		$I->submitForm('form#reorder', ['order' => ["716", "1900", "1082", "1083", "1084"]]); // missing one
		$I->see("L'ordre que vous avez fourni est invalide.", '.alert-error');
	}
}
