<?php

namespace tests\functional\revue;

use FunctionalTester;

class RevueIndexCest
{
	public function testDefault(FunctionalTester $I)
	{
		\controllers\revue\IndexAction::$pageSize = 6;

		$I->amOnPage('/revue/index');
		$I->see("Revues - A", 'h1');
		$I->seeNumberOfElements('#before-list-results .pagination-alpha li', 8, "Distinct letters");
		$I->seeElement('#before-list-results .pagination-alpha li:first-child[title="10 revues"]');
		$I->seeNumberOfElements('#before-list-results .subpagination li a', 4); // 10/6 + 2
		$I->seeNumberOfElements('.list-results > div', 6);

		$I->click("Suivant", '#before-list-results');
		$I->seeCurrentUrlMatches('/lettre=A\b/');
		$I->seeCurrentUrlMatches('/page=2\b/');
		$I->seeNumberOfElements('.list-results > div', 4);

		$I->click("V", '#before-list-results');
		$I->seeCurrentUrlMatches('/lettre=V/');
		$I->dontSeeCurrentUrlMatches('/page=/');
		$I->seeNumberOfElements('.list-results > div', 1);

		\controllers\revue\IndexAction::$pageSize = 50;
	}

	public function outOfBoundsLetter(FunctionalTester $I)
	{
		$I->amOnPage('/revue/index?lettre=|');
		$I->seeElement('#before-list-results .pagination-alpha li.active a[href="/revue/index?lettre=A"]'); // letter not valid => "A"

		$I->amOnPage('/revue/index?lettre=,');
		$I->seeElement('#before-list-results .pagination-alpha li.active a[href="/revue/index?lettre=A"]'); // letter not valid => "A"
	}
}
