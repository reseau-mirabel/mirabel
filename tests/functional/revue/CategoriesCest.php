<?php

namespace tests\functional\revue;

use FunctionalTester;
use Intervention;

class CategoriesCest
{
	public function categoriesDirect(FunctionalTester $I)
	{
		$I->amOnPage('/revue/29/Annales-historiques-de-la-Revolution-francaise');
		$I->see("Notre-Dame de Paris suit cette revue", '#revue-suivi');
		$I->see("Europe de l'Ouest", '#revue-categories a'); // will be removed
		$I->dontSee("Préhistoire", '#revue-categories a'); // will be added
		$I->dontSee("Définir la thématique", "a");

		$I->amLoggedInAs("frollo"); // not admin, owner
		$I->amOnPage('/revue/29/Annales-historiques-de-la-Revolution-francaise');
		$I->click("Définir la thématique");
		$I->see("Thématique pour la revue Annales historiques de la Révolution française", 'h1');
		$I->see("Europe de l'Ouest", '#categorierevue-grid');
		$I->see("Les modifications seront appliquées immédiatement", '.alert');
		$I->seeElement('button[type="submit"]');

		$I->startFollowingRedirects();
		$I->submitForm(
			"#revue-categories",
			[
				'revueId' => '29',
				'forceProposition' => '0',
				'remove' => ['120'], // Europe de l'Ouest
				'add' => ['84'], // Préhistoire
			]
		);
		$I->seeCurrentUrlMatches('#revue/29/#');
		$I->dontSee("Europe de l'Ouest", '#revue-categories a'); // removed
		$I->see("Préhistoire", '#revue-categories a'); // added
		//$I->see("Les affectations de thèmes ont bien été modifiées", '.alert.alert-success');

		// SQL rollback does not work in this case,
		// so we reset at least the Intervention.
		// TODO: fix the rollback of embedded transactions (probable cause)
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/intervention/admin?q[revueId]=29&q[hdateVal]=' . date('Y-m-d'));
		$I->seeNumberOfElements('#intervention-grid tbody tr td.intervention', 2);
		$I->click('#intervention-grid .button-column a');
		$iid = (int) $I->grabTextFrom('.breadcrumbs .active');
		$I->stopFollowingRedirects();
		$I->click("Annuler cette intervention");
		// $I->seeFlashMessage('success'); // Revert fails in tests
		Intervention::model()->deleteByPk($iid);
	}

	public function categoriesDirectReset(FunctionalTester $I)
	{
		$I->wantToTest("the DB has been reset after the 'CategoriesDirect' test");
		$I->amOnPage('/revue/29/Annales-historiques-de-la-Revolution-francaise');
		/* Reset is broken, see previous test
		$I->see("Europe de l'Ouest", '#revue-categories a'); // will be removed
		$I->dontSee("Préhistoire", '#revue-categories a'); // will be added
		 */

		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/intervention/admin?q[revueId]=29&q[hdateVal]=' . date('Y-m-d'));
		$I->seeNumberOfElements('#intervention-grid tbody tr td.intervention', 0);
	}

	public function categoriesDirectPerm(FunctionalTester $I)
	{
		$I->amOnPage('/revue/29/Annales-historiques-de-la-Revolution-francaise');
		$I->see("Julliard suit cette revue", '#revue-suivi');

		$I->amLoggedInAs("editeur-98"); // not admin, owner, partenaire-editeur
		$I->amOnPage('/revue/29/Annales-historiques-de-la-Revolution-francaise');
		$I->click("Définir la thématique");
		$I->see("Les modifications seront appliquées immédiatement", '.alert');
	}

	public function categoriesProposal(FunctionalTester $I)
	{
		$I->amLoggedInAs("milady"); // not admin, not owner
		$I->amOnPage('/revue/6/Esprit');
		$I->click("Définir la thématique");
		$I->see("Les modifications que vous proposez devront être validées", '.alert');
	}

	public function categoriesProposalPossible(FunctionalTester $I)
	{
		$I->amOnPage('/revue/6/Esprit');
		$I->see("Cultures et sociétés", '#revue-categories a');
		$I->dontSee("Définir la thématique", "a");
		$I->dontSee("Interventions en attente de validation", '.alert-block');

		$I->amLoggedInAs("d'artagnan"); // admin, Partenaire 1
		$I->amOnPage('/revue/6/Esprit');
		$I->click("Définir la thématique");
		$I->see("Thématique pour la revue Esprit", 'h1'); // suivie par Partenaire 2
		$I->see("Cultures et sociétés", '#categorierevue-grid');
		$I->see("Passer par l'état en attente", '.alert');
		$I->seeElement('button[type="submit"]');

		$I->stopFollowingRedirects();
		$I->submitForm(
			"#revue-categories",
			[
				'revueId' => '6',
				'forceProposition' => '1',
				'remove' => ['2'], // Cultures et sociétés
				'add' => ['84'], // Préhistoire
			]
		);
		$I->seeResponseCodeIsRedirection();
		$I->seeFlashMessage('warning', "Les affectations de thèmes ont bien été proposées");
		$I->seeFlashMessage('warning', "Cultures et sociétés"); // to be removed
		$I->seeFlashMessage('warning', "Préhistoire"); // to be added

		$I->startFollowingRedirects();
		$I->amOnPage('/revue/6/Esprit');
		$I->see("Interventions en attente de validation", '.alert-block');
	}
}