<?php

namespace tests\functional\revue;

class FacettesCest
{
	public function langFilteredByCountry(\FunctionalTester $I)
	{
		$I->amOnPage('/revue/search?q%5Blangues%5D=fre');
		$I->see("Langues : français", '#search-summary .search-base');
		$I->dontSeeElement('#search-summary .facets');
		$I->see("total de 18", '.pagination-block');
		$I->seeElement('#facets .facet.facet-paysid');
		$I->seeNumberOfElements('#facets .facet-paysid > label', 4);
		$I->seeNumberOfElements('#facets input[checked]', 0);

		$I->amOnPage('/revue/search?q%5Blangues%5D=fre&f%5BpaysId%5D=62'); // facette : pays=France
		$I->see("Langues : français", '#search-summary .search-base');
		$I->see("Pays : France", '#search-summary .facets');
		$I->see("total de 13", '.pagination-block');
		$I->seeElement('#facets .facet-paysid label.checkbox input[value="62"][checked]');
	}

	public function langFilteredByLang(\FunctionalTester $I)
	{
		$I->amOnPage('/revue/search?q%5Blangues%5D=fre&f%5Blangues%5D=eng');
		$I->dontSee("Biscuit chinois", 'h1'); // When facets are enabled, single result => no redirection
		$I->see("Biscuit chinois", '.revue-details');
	}

	public function countryFilteredByThemes(\FunctionalTester $I)
	{
		$I->amOnPage('/revue/search?q%5BpaysId%5D=62');
		$I->see('Pays : France', '#search-summary .search-base');
		$I->see("total de 13", '.pagination-block');

		$I->amOnPage('/revue/search?q%5BpaysId%5D=62&f%5Bcategorie%5D%5B%5D=44'); // facette thème : Histoire
		$I->see("Thématique : Histoire", '#search-summary .facets');
		$I->see("total de 4", '.pagination-block');

		$I->amOnPage('/revue/search?q%5BpaysId%5D=62&f%5Bcategorie%5D%5B%5D=44&f%5Bcategorie%5D%5B%5D=57');
		$I->see("Thématique : Histoire ET Droit, justice, criminologie", '#search-summary .facets');
		$I->see("Aucun résultat trouvé", '#extended-search-results');
		$I->dontSeeElement('.pagination-block select');

		$I->amOnPage('/revue/search?q%5BpaysId%5D=62&f%5Bcategorie%5D%5B%5D=44&f%5Bcategorie%5D%5B%5D=120');
		$I->dontSee("Annales historiques de la Révolution française", 'h1'); // When facets are enabled, single result => no redirection
		$I->see("Annales historiques de la Révolution française", '.revue-details'); // When facets are enabled, single result => no redirection
	}

	public function themeFilteredByLangs(\FunctionalTester $I)
	{
		$I->amOnPage('/revue/search?q%5Bcategorie%5D=44');
		$I->see("total de 4", '.pagination-block');

		$I->amOnPage('/revue/search?q%5Bcategorie%5D=44&f%5Blangues%5D%5B%5D=fre');
		$I->see("total de 4", '.pagination-block');

		$I->amOnPage('/revue/search?q%5Bcategorie%5D=44&f%5Blangues%5D%5B%5D=fre&f%5Blangues%5D%5B%5D=eng');
		$I->see("Aucun résultat trouvé.");
	}
}
