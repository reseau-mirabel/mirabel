<?php

namespace tests\functional\titre;

use FunctionalTester;

class TitreMiscCest
{
	public function ajaxComplete(FunctionalTester $I)
	{
		$I->amOnPage('/titre/ajax-complete?term=juridique');
		$I->seeInSource('Actualit\u00e9 Juridique Droit Administratif');
	}

	public function ajaxIssn(FunctionalTester $I)
	{
		$I->amOnPage('/titre/ajax-issn?position=0&titreId=1');
		$I->seeInField('input[name="Issn[0][titreId]"]', '1');
	}

	public function export(FunctionalTester $I)
	{
		$I->amOnPage('/titre/export?type=ppn');
		$I->seeInSource('PPN;"URL Mir@bel"');
	}

	public function identification(FunctionalTester $I)
	{
		$I->amLoggedInAs('milady');
		$I->amOnPage('/titre/identification/2');
		$I->seeNumberOfElements('fieldset', 6);
		$I->seeInField('.ressource-3 input[type="text"]', "revue-afrique-contemporaine");
		$I->fillField('.ressource-3 input[type="text"]', ""); // delete
		$I->fillField('.ressource-402 input[type="text"]', "tralalaX"); // insert
		$I->fillField('#Identification_0_idInterne', "tralalaY"); // insert, new ressource
		$I->fillField('#Identification_0_ressourceId', "294"); // insert, new ressource

		$I->click("Enregistrer", '.form-actions');
		$I->seeCurrentUrlMatches('#titre/2#');
		$I->amOnPage('/titre/identification/2');
		$I->dontSeeInField('.ressource-3 input[type="text"]', "revue-afrique-contemporaine");
		$I->seeInField('.ressource-402 input[type="text"]', "tralalaX");
		$I->seeInField('.ressource-294 input[type="text"]', "tralalaY");
	}

	public function changeJournal(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/titre/change-journal/1564');
		$I->see("Midwest Journal of Political Science", 'h1 em');
		$I->fillField('input[type="text"]', 0);
		$I->click("Enregistrer", '.form-actions');
		$I->seeCurrentUrlMatches('#/revue/\d+/Midwest-Journal-of-Political-Science$#');

		$I->amOnPage('/titre/change-journal/1564');
		$I->fillField('input[type="text"]', "1174");
		$I->click("Enregistrer", '.form-actions');
	}

	public function editeurs(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/titre/editeurs/6');
		$I->see("Rôles des éditeurs", 'h1');
		$I->seeNumberOfElements('fieldset', 1);
		$I->seeOptionIsSelected("TitreEditeur[6][role]", "indéterminée");
		$I->dontSeeCheckboxIsChecked("TitreEditeur[6][ancien]");

		$I->submitForm('#titre-editeurs-form', [
			"TitreEditeur[6][role]" => 'copyright_holder',
			"TitreEditeur[6][ancien]" => '1',
		]);
		$I->seeCurrentUrlMatches('#/revue/6#');

		$I->amOnPage('/titre/editeurs/6');
		$I->seeOptionIsSelected("TitreEditeur[6][role]", "détenteur des droits");
		$I->seeCheckboxIsChecked("TitreEditeur[6][ancien]");
	}

	public function publication(FunctionalTester $I)
	{
		$I->expectThrowable(
			new \CHttpException(404, "Ce titre n'a pas de données sur sa politique de publication (ni source Open policy finder, ni politique validée par Mir@bel)."),
			function() use($I) {
				$I->amOnPage('/titre/publication/6');
			}
		);

		// prepare data
		$t = \Titre::model()->findByPk(28);
		/** @var \Titre $t */
		$t->liensJson = '[
			{"sourceId":13,"src":"JournalBase","url":"http:\/\/journalbase.cnrs.fr\/index.php?op=9&id=115"},
			{"sourceId":17,"src":"Wikipedia","url":"https:\/\/fr.wikipedia.org\/wiki\/Actes_de_la_recherche_en_sciences_sociales"},
			{"sourceId":19,"src":"HAL","url":"https:\/\/hal.archives-ouvertes.fr\/search\/index\/q\/*\/journalId_i\/22968"},
			{"sourceId":31,"src":"Sherpa","url":"https:\/\/sherpa.com/x"}
		]';
		$t->save(false);

		$I->amOnPage('/titre/publication/28');
		$I->see("Actes de la recherche en sciences sociales", 'h1');
		$I->seeNumberOfElements('.sherpa-oa > section', 3);

		// reset data: DB
		$t->liensJson = '[
			{"sourceId":13,"src":"JournalBase","url":"http:\/\/journalbase.cnrs.fr\/index.php?op=9&id=115"},
			{"sourceId":17,"src":"Wikipedia","url":"https:\/\/fr.wikipedia.org\/wiki\/Actes_de_la_recherche_en_sciences_sociales"},
			{"sourceId":19,"src":"HAL","url":"https:\/\/hal.archives-ouvertes.fr\/search\/index\/q\/*\/journalId_i\/22968"}
		]';
		$t->save(false);

		$I->amOnPage('/revue/28');
		$I->expectThrowable(
			new \CHttpException(404, "Ce titre n'a pas de données sur sa politique de publication (ni source Open policy finder, ni politique validée par Mir@bel)."),
			function() use($I) {
				$I->amOnPage('/titre/publication/28');
			}
		);
	}

	public function view(FunctionalTester $I)
	{
		$I->amOnPage('/revue/20');
		$I->dontSeeLink('', '/titre/20');

		$I->amLoggedInAs('milady');
		$I->amOnPage('/revue/20');
		$I->seeLink('', '/titre/20');
		$I->click('#revue-titres tr:first-child a[title="Détails de ce titre"]');
		$I->see("Détail du Titre Recueil Dalloz", 'h1');
		$I->seeLink('https://biblioweb.hypotheses.org/16577');
	}
}
