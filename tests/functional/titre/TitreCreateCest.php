<?php

namespace tests\functional\titre;

use FunctionalTester;

class TitreCreateCest
{
	public function create(FunctionalTester $I)
	{
		$I->amLoggedInAs('milady');
		$I->amOnPage('/titre/create');
		$I->see("Créer une nouvelle revue", 'h1');
		$I->see("Les modifications seront appliquées immédiatement.", '.alert');
		$I->fillField("#Titre_titre", "Ma revue temporaire");
		$I->checkOption("Suivre cette revue (trois mousquetaires)");
		$I->fillField("Titre[langues]", 'que, jpn');
		$I->click("Enregistrer", '.form-actions');
		$I->see("Ma revue temporaire", 'h1');
		$I->seeCurrentUrlMatches('#/revue/\d+/Ma-revue-temporaire$#');
		$createdUrl = $I->grabFromCurrentUrl();
		$I->see("suit cette revue dans Mir@bel", '.objets-suivis');
		$I->logout();

		$I->amLoggedInAs('editeur-98'); // utilisateur-éditeur
		$I->amOnPage('/titre/create');
		$I->see("Créer une nouvelle revue", 'h1');
		$I->dontSee("Les modifications seront appliquées immédiatement.", '.alert');
		$I->see("Les modifications que vous proposez devront être validées", '.alert');

		$this->delete($I, $createdUrl);
	}

	public function createChecksLinks(FunctionalTester $I)
	{
		$I->amOnPage('/titre/create');
		$I->submitForm('#titre-form', [
			'Titre[titre]' => "Ma revue temporaire",
			'Titre[liens][3][src]' => "MaSource3",
			'Titre[liens][3][url]' => "Xhttps://une.fausse.url/",
		]);
		$I->seeElement('.alert-error');

		$I->amOnPage('/titre/create');
		$I->submitForm('#titre-form', [
			'Titre[titre]' => "Ma revue temporaire",
			'Titre[liens][3][src]' => "MaSource3 ", // espace
			'Titre[liens][3][url]' => "https://une.fausse.url/\t", // tabulation
		]);
		$I->dontSeeElement('.alert-error');
		$I->see("Proposition enregistrée", '.alert-success');
		$intervention = \Intervention::model()->findBySql("SELECT * FROM Intervention ORDER BY id DESC LIMIT 1");
		assert($intervention->contenuJson instanceof \InterventionDetail);
		$changes = $intervention->contenuJson->toArray();
		$I->assertEquals("Revue", $changes['0']['model']);
		$I->assertEquals("Titre", $changes['1']['model']);
		$I->assertEquals("Ma revue temporaire", $changes[1]['after']['titre']);
		$I->assertEquals('[{"src":"MaSource3","url":"https://une.fausse.url/"}]', $changes[1]['after']['liensJson']);
	}

	public function createNonSequentialLinks(FunctionalTester $I)
	{
		$I->amOnPage('/titre/create');
		$I->dontSee("Les modifications seront appliquées immédiatement.", '.alert');
		$I->submitForm('#titre-form', [
			'Titre[titre]' => "Ma revue temporaire",
			'Titre[liens][3][src]' => "MaSource3",
			'Titre[liens][3][url]' => "https://une.fausse.url/",
		]);
		$I->seeCurrentUrlEquals('/revue/index');
		$I->see("Proposition enregistrée", '.alert-success');
		$I->dontSeeElement('.alert-error');
		$intervention = \Intervention::model()->findBySql("SELECT * FROM Intervention ORDER BY id DESC LIMIT 1");
		assert($intervention->contenuJson instanceof \InterventionDetail);
		$changes = $intervention->contenuJson->toArray();
		codecept_debug($changes);
		$I->assertEquals("Revue", $changes['0']['model']);
		$I->assertEquals("Titre", $changes['1']['model']);
		$I->assertEquals("Ma revue temporaire", $changes[1]['after']['titre']);
		$I->assertEquals('[{"src":"MaSource3","url":"https://une.fausse.url/"}]', $changes[1]['after']['liensJson']);
	}

	public function createByIssn(FunctionalTester $I)
	{
		$I->amOnPage('/titre/create-by-issn');
		$I->seeElement('#create-without-issn');
		$I->click('#create-without-issn');
		$I->see("Créer une nouvelle revue", 'h1');
	}

	public function createIndirect(FunctionalTester $I)
	{
		$I->amOnPage('/titre/create');
		$I->see("Créer une nouvelle revue", 'h1');
		$I->dontSee("Les modifications seront appliquées immédiatement.", '.alert');

		$I->amLoggedInAs('u-pol');
		$I->amOnPage('/titre/create');
		$I->dontSee("Les modifications seront appliquées immédiatement.", '.alert');
	}

	public function createWithCanceledIssn(FunctionalTester $I)
	{
		$I->amLoggedInAs('milady');
		$I->amOnPage('/titre/create');
		$I->fillField("#Titre_titre", "Ma revue temporaire");
		$I->fillField('.issn-number', "0177-1094");
		$I->selectOption('#Issn_new0_statut', "Annulé");
		$I->selectOption('#Issn_new0_support', "Papier (ISSN)");

		/**
		 * @todo Disable the HTTP request during tests.
		 */
		$I->click("Enregistrer", '.form-actions');
		$I->see("Ma revue temporaire", 'h1');
		$I->see("0177-1094", '.issn-bad');
	}

	private function delete(FunctionalTester $I, string $createdUrl)
	{
		$I->amOnPage($createdUrl);
		$I->click('a[title="Détails de ce titre"]');

		$I->click("Supprimer…", '#sidebar');
		$I->seeCurrentUrlMatches('#^/titre/delete/\d+$#');
		$I->fillField('input[name="annoying"]', "Je confirme");
		$I->click('.btn-danger');
		$I->seeCurrentUrlEquals('/revue/index');

		$I->expectThrowable(\CHttpException::class, function () use ($I, $createdUrl) {
			$I->amOnPage($createdUrl);
		});
	}
}
