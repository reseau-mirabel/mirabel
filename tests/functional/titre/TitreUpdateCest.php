<?php

namespace tests\functional\titre;

use FunctionalTester;

class TitreUpdateCest
{
	public function proposeUpdate1(FunctionalTester $I)
	{
		$I->amLoggedInAs('milady');
		$I->amOnPage('/revue/20');
		$I->dontSeeElement('#revue-titres tr:first-child a[title="Proposer une modification"]');
	}

	public function proposeUpdate2(FunctionalTester $I)
	{
		$I->amLoggedInAs('editeur-98'); // utilisateur-éditeur
		$I->amOnPage('/titre/update/2'); // titre d'un autre éditeur
		$I->see("Les modifications que vous proposez devront être validées", '.alert');
		$I->amOnPage('/titre/update/5'); // titre de son éditeur
		$I->dontSee("Les modifications que vous proposez devront être validées", '.alert');
	}

	public function proposeUpdate3(FunctionalTester $I)
	{
		$I->amOnPage('/revue/20');
		$I->click('#revue-titres tr:first-child a[title="Proposer une modification"]');
		$I->see("Modifier le titre Recueil Dalloz", 'h1');
		$I->seeInField('Titre[liens][2][url]', 'http://www.jurisguide.fr/fiches-documentaires/recueil-dalloz/');
		$I->seeInField('Titre[liens][3][url]', 'https://biblioweb.hypotheses.org/16577');
		$I->see("Les modifications que vous proposez", '.alert');
		$I->fillField('Titre[liens][0][url]', 'https://www.jurisguide.fr/fiches-documentaires/recueil-dalloz/');
		$I->stopFollowingRedirects();
		$I->click("Proposer cette modification");
		$I->seeFlashMessage('success', "Proposition enregistrée");

		$I->amLoggedInAs('milady');
		$I->startFollowingRedirects();
		$I->amOnPage('/revue/20');
		$I->see("Interventions en attente de validation", '.alert h4');
		$I->click('a[href^="/intervention"]', '#content .alert li');
		$I->see("Intervention", 'h1');
		$I->stopFollowingRedirects();
		$I->click("Accepter", '#sidebar');
		$I->seeFlashMessage('success');
	}

	public function proposeUpdateRelation(FunctionalTester $I)
	{
		$I->amLoggedInAs('milady');
		$I->amOnPage('/titre/update/6');
		$I->see("Modifier le titre Esprit", 'h1');
		$I->see("Les modifications que vous proposez", '.alert');
		$I->seeCheckboxIsChecked('fieldset#editeurs input[type="checkbox"]');
		$I->uncheckOption('fieldset#editeurs input[type="checkbox"]');
		$I->fillField('#Intervention_commentaire', "Retire l'éditeur");
		$I->click("Proposer cette modification", '.form-actions');
		$I->seeCurrentUrlEquals('/revue/6/Esprit');
		/*
		 * Yii1 checkboxes cannot be tested with Codeception!
		 *
		 * Submiting this form only removes the dynamic fields (Autres liens).
		 *
		$I->amOnPage('/intervention/admin?q[titre_titre]=esprit');
		$I->click("Voir", '#intervention-grid tbody');
		$I->see("Modification du titre", 'h2');
		$I->see("Détache l'éditeur « Esprit »", '#intervention-detail');
		 */
	}

	public function updateWithCanceledIssn(FunctionalTester $I)
	{
		$I->amLoggedInAs("d'artagnan");
		$I->amOnPage('/titre/update/6');
		$data = [
			'Titre' => [
				'confirm' => 1,
				'titre' => 'Esprit',
				'prefixe' => '',
				'sigle' => '',
				'obsoletePar' => '',
				'url' => 'http://www.esprit.presse.fr',
				'urlCouverture' => 'http://www.cairn.info/static/includes/vign_rev/ESPRI/ESPRI_1811_L204.jpg',
				'urlCouvertureDld' => '0',
				'dateDebut' => '1940',
				'dateFin' => '',
				'periodicite' => 'mensuel',
				'langues' => 'fre',
				'liens' => [
					['src' => 'Twitter', 'url' => 'https://twitter.com/RevueEsprit'],
					['src' => 'Facebook','url' => 'https://www.facebook.com/pages/Revue-Esprit/123674940994027'],
					['src' => 'Wikipedia', 'url' => 'https://fr.wikipedia.org/wiki/Esprit_%28revue%29'],
					['src' => 'JournalBase', 'url' => 'http://journalbase.cnrs.fr/index.php?op=9&id=2569'],
					['src' => 'Youtube', 'url' => 'https://www.youtube.com/channel/UCO1Iw3dR5aj3jA6tabsGPOw?sub_confirmation=1'],
					['src' => 'Instagram', 'url' => 'https://www.instagram.com/revue.esprit/'],
					['src' => 'HAL', 'url' => 'https://hal.archives-ouvertes.fr/search/index/q/*/journalId_i/97924'],
					['src' => 'OpenAlex', 'url' => 'https://openalex.org/V79731123'],
					['src' => '', 'url' => ''],
				],
			],
			'Issn' => [
				5 => [
					'id' => '5',
					'titreId' => '6',
					'issn' => '0014-0759',
					'statut' => '0',
					'support' => 'papier',
					'issnl' => '0014-0759',
					'dateDebut' => '1940',
					'dateFin' => '',
					'titreReference' => 'Esprit  : revue internationale  / directeur Emmanuel Mounier',
					'pays' => '',
					'sudocPpn' => '03922340X',
					'sudocNoHolding' => '0',
					'bnfArk' => '',
					'worldcatOcn' => '478745532',
				],
				5770 => [
					'id' => '5770',
					'titreId' => '6',
					'issn' => '2111-4579',
					'statut' => '0',
					'support' => 'electronique',
					'issnl' => '0014-0759',
					'dateDebut' => '20XX',
					'dateFin' => '',
					'titreReference' => 'Esprit',
					'pays' => '',
					'sudocPpn' => '14879453X',
					'sudocNoHolding' => '0',
					'bnfArk' => '',
					'worldcatOcn' => '793459215',
				],
				'new0' => [
					'id' => '',
					'titreId' => '6',
					'issn' => '0177-1094',
					'statut' => '3',
					'support' => 'papier',
					'issnl' => '',
					'dateDebut' => '',
					'dateFin' => '',
					'titreReference' => '',
					'pays' => '',
					'sudocPpn' => '',
					'bnfArk' => '',
					'worldcatOcn' => '',
				],
				5771 => [
					'sudocNoHolding' => '0',
				],
			],
			'TitreEditeur' => [
				6 => [
					'editeurId' => '6',
					'ancien' => '0',
					'intellectuel' => '0',
					'commercial' => '0',
				],
			],
			'TitreEditeurNew' => [
				[
					'ancien' => '0',
					'intellectuel' => '0',
					'commercial' => '0',
				],
			],
			'TitreEditeurComplete' => '',
			'force_proposition' => '0',
			'Intervention' => [
				'email' => '',
				'commentaire' => '',
			],
		];
		$I->submitForm('#titre-form', $data);
		$I->see("Esprit", 'h1');
		$I->seeElement('.alert-success');
		$I->see("0177-1094", '.issn-bad');
	}
}
