<?php

class EditeursCest
{
	public function idref(\ApiTester $I)
	{
		\Yii::app()->db->createCommand("UPDATE Editeur SET `idref` = '033135304' WHERE id = 377")->execute();
		$I->sendGet('editeurs/idref/033135304');
		$I->seeResponseCodeIs(200);
		$I->seeResponseIsJson();
		$response = json_decode($I->grabResponse());
		$I->assertIsObject($response);
		$I->assertSame(377, $response->editeur->id);
		$I->assertSame("Armand Colin", $response->editeur->nom);
		$I->assertCount(5, $response->relations_titres);
		$I->assertSame(716, $response->relations_titres[1]->titre_id);
		\Yii::app()->db->createCommand("UPDATE Editeur SET `idref` = NULL WHERE id = 377")->execute();
	}

	public function view(\ApiTester $I)
	{
		$I->sendGet('editeurs/6');
		$I->seeResponseCodeIs(200);
		$I->seeResponseIsJson();
		$response = json_decode($I->grabResponse());
		$I->assertIsObject($response);
		$I->assertSame(6, $response->editeur->id);
		$I->assertSame("Esprit", $response->editeur->nom);
		$I->assertSame(6, $response->relations_titres[0]->titre_id);
	}
}
