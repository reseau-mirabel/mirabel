<?php

class RevuesCest
{
	public function issn(\ApiTester $I)
	{
		$I->sendGet('revues/718');
		$I->seeResponseCodeIs(200);
		$I->seeResponseIsJson();
		$response = json_decode($I->grabResponse());
		$I->assertCount(6, $response->titres);
		$I->assertNull($response->titres[0]->obsoletepar);
		$I->assertNotNull($response->titres[1]->obsoletepar);
		$I->assertCount(2, $response->titres[0]->issns);
	}
}