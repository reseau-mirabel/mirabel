<?php

class ThemesCest
{
	public function index(\ApiTester $I)
	{
		$I->sendGet('themes');
		$I->seeResponseCodeIs(200);
		$I->seeResponseIsJson();
		$response = json_decode($I->grabResponse());
		$I->assertCount(118, $response);
		$I->assertSame(1, $response[0]->id);
		$I->assertSame(0, $response[0]->parentid);
		$I->assertsame('racine', $response[0]->nom);
	}

	public function revue(\ApiTester $I)
	{
		$I->sendGet('themes/revue/3');
		$I->seeResponseCodeIs(200);
		$I->seeResponseIsJson();
		$response = json_decode($I->grabResponse());
		$I->assertCount(1, $response);
		$I->assertEquals(
			(object) [
				'id' => 11,
				'parentid' => 81,
				'nom' => "Économie, gestion",
			],
			$response[0]
		);
	}

	public function titre(\ApiTester $I)
	{
		$I->sendGet('themes/titre/3');
		$I->seeResponseCodeIs(200);
		$I->seeResponseIsJson();
		$response = json_decode($I->grabResponse());
		$I->assertCount(1, $response);
		$I->assertEquals(
			(object) [
				'id' => 11,
				'parentid' => 81,
				'nom' => "Économie, gestion",
			],
			$response[0]
		);
	}
}