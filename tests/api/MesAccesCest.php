<?php

class MesAccesCest
{
	private const HASH = '47f9a24f244a4aa664dc739157c89b6a';

	public function abonnement(\ApiTester $I)
	{
		$I->sendGet('mes/acces?abonnement=1&partenaire=' . self::HASH);
		$I->seeResponseCodeIs(200);
		$I->seeResponseIsJson();
		$response = json_decode($I->grabResponse());
		$I->assertCount(95, $response);
		\PHPUnit\Framework\assertObjectHasProperty("identifiantpartenaire", $response[0]);
	}

	public function cumul(\ApiTester $I)
	{
		$I->sendGet('mes/acces?abonnement=1&possession=1&partenaire=' . self::HASH);
		$I->seeResponseCodeIs(200);
		$I->seeResponseIsJson();
		$response = json_decode($I->grabResponse());
		$I->assertCount(105, $response);
		\PHPUnit\Framework\assertObjectHasProperty("identifiantpartenaire", $response[0]);
	}

	public function erreur(\ApiTester $I)
	{
		$I->sendGet('mes/acces?partenaire=' . self::HASH);
		$I->seeResponseIsJson();
		$response = json_decode($I->grabResponse());
		$I->assertEquals(400, $response->code);
	}

	public function possession(\ApiTester $I)
	{
		$I->sendGet('mes/acces?possession=1&partenaire=' . self::HASH);
		$I->seeResponseCodeIs(200);
		$I->seeResponseIsJson();
		$response = json_decode($I->grabResponse());
		$I->assertCount(60, $response);

		\PHPUnit\Framework\assertObjectHasProperty("identifiantpartenaire", $response[0]);
		$I->assertEquals(2, $response[0]->titreid);
	}
}
