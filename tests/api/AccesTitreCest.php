<?php

class AccesTitreCest
{
	public function issn(\ApiTester $I)
	{
		$I->sendGet('acces/titres?issn=1243-258X');
		$I->seeResponseCodeIs(200);
		$I->seeResponseIsJson();
		$response = json_decode($I->grabResponse());
		$I->assertCount(2, $response);

		$I->assertEquals(1082, $response[0]->titreid);
		$I->assertSame(["1243-258X", "2420-4641"], $response[0]->issn);
		$I->assertEquals("JSTOR", $response[0]->ressource);
	}
}
