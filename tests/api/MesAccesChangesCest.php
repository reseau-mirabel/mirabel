<?php

class MesAccesChangesCest
{
	private const HASH = '47f9a24f244a4aa664dc739157c89b6a';

	public function erreur(\ApiTester $I)
	{
		$I->sendGet('mes/acces/changes?partenaire=' . self::HASH);
		$I->seeResponseIsJson();
		$response = json_decode($I->grabResponse());
		$I->assertEquals(400, $response->code);
		$I->assertStringContainsString("depuis", $response->fields);

		$I->sendGet('mes/acces/changes?depuis=555&partenaire=' . self::HASH);
		$I->seeResponseIsJson();
		$response = json_decode($I->grabResponse());
		$I->assertEquals(400, $response->code);
		$I->assertStringContainsString("depuis", $response->fields);
	}

	public function abonnement(\ApiTester $I)
	{
		$I->sendGet('mes/acces/changes?abonnement=1&depuis=1546297200&partenaire=' . self::HASH);
		$I->seeResponseCodeIs(200);
		$I->seeResponseIsJson();
		$response = json_decode($I->grabResponse());
		\PHPUnit\Framework\assertObjectHasProperty("ajouts", $response);
		\PHPUnit\Framework\assertObjectHasProperty("modifications", $response);
		\PHPUnit\Framework\assertObjectHasProperty("suppressions", $response);
		$I->assertCount(8, $response->ajouts);
		$I->assertCount(64, $response->modifications);
		$I->assertCount(0, $response->suppressions);
	}
}
