<?php

// Tests store the application and framework files in a distinct directory, to avoid aside effects.
define('DATA_DIR', dirname(__DIR__) . '/data-tests');
foreach (['', '/cache/yii', '/feeds', '/filestore', '/logs', '/sessions'] as $d) {
	if (!is_dir(DATA_DIR . $d)) {
		mkdir(DATA_DIR . $d, 0700, true);
	}
}

// Composer autoloading of classes
defined('VENDOR_PATH') or define('VENDOR_PATH', dirname(__DIR__, 1) . '/vendor');

// Composer autoloader
require_once VENDOR_PATH . '/autoload.php';

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'test');
define('YII_TARGET', 'test');

defined('YII_ENABLE_ERROR_HANDLER') or define('YII_ENABLE_ERROR_HANDLER', false);
defined('YII_ENABLE_EXCEPTION_HANDLER') or define('YII_ENABLE_EXCEPTION_HANDLER', false);

// Yii1 autoloader
require_once VENDOR_PATH . '/yiisoft/yii/framework/YiiBase.php';
YiiBase::$autoloaderFilters = [
	fn ($f) => strpos('\\', $f) !== false, // skip Yii autoloader for namespaced classes
];
spl_autoload_register(['YiiBase','autoload'], prepend: false);

require_once dirname(__DIR__) . '/src/www/protected/components/Yii.php';
