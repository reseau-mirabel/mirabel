<?php

namespace tests\unitwithyii\kbart;

use processes\kbart\Import;
use processes\kbart\State;
use processes\kbart\noninteractive\Config;

class ParseTest extends \Codeception\Test\Unit
{
	public function testEmbargo()
	{
		$data = self::dataFixtures();
		$_SERVER['REQUEST_TIME'] = 1740394053; // Set a fixed date, since embargos are relative to present.

		$state = new State($data->ressource, []);
		$state->title = "test embargo";
		$import = new Import($state);
		\PHPUnit\Framework\assertTrue($import->readFile(codecept_data_dir('kbart/embargo.tsv')));
		$import->checkConsistency();
		$import->identifyTitles();
		$known = $state->crossedData->known;

		\PHPUnit\Framework\assertCount(1, $known);
		\PHPUnit\Framework\assertArrayHasKey($data->titre->id, $known);

		$this->importWithOriginalDates($import, $data->titre);
		$this->importWithComputedDates($import, $data->titre);

		$data->delete();
	}

	private function importWithOriginalDates(Import $import, \Titre $titre): void
	{
		$config = new Config();
		$config->absents = false;
		$config->lacunaire = false;
		$config->overrideDatesWithEmbargo = false;
		$config->selection = false;
		$config->suppressions = false;
		$config->setDefaultAccess('Libre');
		$config->setDefaultCoveragedepth('Intégral');
		$diffs = $import->diff($config)->getDiffs();

		$create = $diffs[$titre->id]->services->getCreate();
		\PHPUnit\Framework\assertCount(2, $create);
		\PHPUnit\Framework\assertInstanceOf('\Service', $create[0]);
		/** @var Service[] $create */
		\PHPUnit\Framework\assertSame('Libre', $create[0]->acces);
		\PHPUnit\Framework\assertSame('2010', $create[0]->dateBarrDebut);
		\PHPUnit\Framework\assertSame('2023-02', $create[0]->dateBarrFin);
		\PHPUnit\Framework\assertSame('Restreint', $create[1]->acces);
		\PHPUnit\Framework\assertSame('2010', $create[1]->dateBarrDebut);
		\PHPUnit\Framework\assertSame('', $create[1]->dateBarrFin);
	}

	private function importWithComputedDates(Import $import, \Titre $titre): void
	{
		$config = new Config();
		$config->absents = false;
		$config->lacunaire = false;
		$config->overrideDatesWithEmbargo = true;
		$config->selection = false;
		$config->suppressions = false;
		$config->setDefaultAccess('Libre');
		$config->setDefaultCoveragedepth('Intégral');
		$diffs = $import->diff($config)->getDiffs();

		$create = $diffs[$titre->id]->services->getCreate();
		\PHPUnit\Framework\assertCount(2, $create);
		\PHPUnit\Framework\assertInstanceOf('\Service', $create[0]);
		/** @var Service[] $create */
		\PHPUnit\Framework\assertSame('Libre', $create[0]->acces);
		\PHPUnit\Framework\assertSame('2010', $create[0]->dateBarrDebut);
		\PHPUnit\Framework\assertSame('2023-02', $create[0]->dateBarrFin);
		\PHPUnit\Framework\assertSame('Restreint', $create[1]->acces);
		\PHPUnit\Framework\assertSame('2023-03', $create[1]->dateBarrDebut);
		\PHPUnit\Framework\assertSame('', $create[1]->dateBarrFin);
	}

	private static function dataFixtures(): object
	{
		return new class {
			public readonly \Ressource $ressource;
			public readonly \Titre $titre;
			private \Revue $revue;
			private \Issn $issn;
			public function __construct() {
				$ressource = new \Ressource;
				foreach (array_keys($ressource->getAttributes()) as $k) {
					$ressource->$k = '';
				}
				$ressource->id = null;
				$ressource->nom = "Ressource KBART";
				$ressource->type = 'Archive';
				$ressource->acces = 'Mixte';
				$ressource->url = 'https://example.com/ressource-kbart';
				$ressource->confirm = 1;
				$ressource->insert();
				if (!$ressource->id) {
					codecept_debug($ressource->getAttributes());
					codecept_debug($ressource->getErrors());
					throw new \Exception("Ressource.save() failed");
				}
				$this->ressource = $ressource;

				$this->revue = new \Revue;
				$this->revue->statut = 'normal';
				$this->revue->save();

				$titre = new \Titre;
				foreach (array_keys($titre->getAttributes()) as $k) {
					$titre->$k = '';
				}
				$titre->id = null;
				$titre->revueId = $this->revue->id;
				$titre->titre = "Titre KBART";
				$titre->statut = 'normal';
				$titre->langues = '[]';
				$titre->save(false);
				$this->titre = $titre;

				$issn = new \Issn;
				$issn->titreId = $titre->id;
				$issn->support = 'papier';
				$issn->issn = '2105-0678';
				$issn->save(false);
				$this->issn = $issn;
			}
			public function delete() {
				$this->issn->delete();
				$this->titre->delete();
				$this->revue->delete();
				$this->ressource->delete();
			}
		};
	}
}
