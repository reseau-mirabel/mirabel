<?php

namespace tests\unitwithyii\kbart;

use processes\kbart\Import;
use processes\kbart\State;
use processes\kbart\noninteractive\Config;

class CollectionTest extends \Codeception\Test\Unit
{
	public function testEmbargo()
	{
		$data = self::dataFixtures();
		$_SERVER['REQUEST_TIME'] = 1740394053; // Set a fixed date, since embargos are relative to present.

		$state = new State($data->ressource, [$data->collection->id]);
		$state->title = "test embargo";
		$import = new Import($state);
		\PHPUnit\Framework\assertTrue($import->readFile(codecept_data_dir('kbart/embargo.tsv')));
		$import->checkConsistency();
		$import->identifyTitles();
		$known = $state->crossedData->known;

		\PHPUnit\Framework\assertCount(1, $known);
		\PHPUnit\Framework\assertArrayHasKey($data->titre->id, $known);
		// Accès 1 du KBART : Libre, de 2010 à 2023-02
		// Accès 2 du KBART : Restreint, de 2023-02 à maintenant

		// Modifie l'accès restreint.
		$this->importWithComputedDates($import, $data->titre);

		$data->delete();
	}

	private function importWithComputedDates(Import $import, \Titre $titre): void
	{
		$config = new Config();
		$config->absents = false;
		$config->lacunaire = false;
		$config->overrideDatesWithEmbargo = true;
		$config->selection = false;
		$config->suppressions = false;
		$config->setDefaultAccess('Libre');
		$config->setDefaultCoveragedepth('Intégral');
		$diffs = $import->diff($config)->getDiffs();

		$create = $diffs[$titre->id]->services->getCreate();
		if ($create) {
			codecept_debug($create[0]->attributes);
		}
		\PHPUnit\Framework\assertCount(0, $create);

		$update = $diffs[$titre->id]->services->getUpdate();
		$before = $update[0]['before']->attributes;
		ksort($before);
		codecept_debug("First updated record, BEFORE:");
		codecept_debug($before);
		codecept_debug("First updated record, AFTER:");
		$after = $update[0]['after']->attributes;
		ksort($after);
		codecept_debug($after);

		\PHPUnit\Framework\assertCount(1, $update);
		\PHPUnit\Framework\assertSame('Restreint', $before['acces']);
		\PHPUnit\Framework\assertSame('2023-03', $after['dateBarrDebut']);
		\PHPUnit\Framework\assertSame('', $after['dateBarrFin']);
	}

	private static function dataFixtures(): object
	{
		return new class {
			public readonly \Ressource $ressource;
			public readonly \Collection $collection;
			public readonly \Titre $titre;
			private \Revue $revue;
			private \Issn $issn;
			public function __construct() {
				$ressource = new \Ressource;
				foreach (array_keys($ressource->getAttributes()) as $k) {
					$ressource->$k = '';
				}
				$ressource->id = null;
				$ressource->nom = "Ressource KBART";
				$ressource->type = 'Archive';
				$ressource->acces = 'Mixte';
				$ressource->url = 'https://example.com/ressource-kbart';
				$ressource->confirm = 1;
				$ressource->insert();
				if (!$ressource->id) {
					codecept_debug($ressource->getAttributes());
					codecept_debug($ressource->getErrors());
					throw new \Exception("Ressource.save() failed");
				}
				$this->ressource = $ressource;

				$collection1 = new \Collection;
				$collection1->nom = "collection-1";
				$collection1->ressourceId = $ressource->id;
				$collection1->type = \Collection::TYPE_ARCHIVE;
				$collection1->url = "https://localhost.local/collection1";
				$collection1->save(false);
				$collection2 = new \Collection;
				$collection2->nom = "collection-2";
				$collection2->ressourceId = $ressource->id;
				$collection2->type = \Collection::TYPE_ARCHIVE;
				$collection2->url = "https://localhost.local/collection2";
				$collection2->save(false);
				$this->collection = $collection2;

				$this->revue = new \Revue;
				$this->revue->statut = 'normal';
				$this->revue->save();

				$titre = new \Titre;
				foreach (array_keys($titre->getAttributes()) as $k) {
					$titre->$k = '';
				}
				$titre->id = null;
				$titre->revueId = $this->revue->id;
				$titre->titre = "Titre KBART";
				$titre->statut = 'normal';
				$titre->langues = '[]';
				$titre->save(false);
				$this->titre = $titre;

				$issn = new \Issn;
				$issn->titreId = $titre->id;
				$issn->support = 'papier';
				$issn->issn = '2105-0678';
				$issn->save(false);
				$this->issn = $issn;

				// Unchanged.
				$service1 = $this->createService([
					'acces' => 'Libre',
					'type' => \Service::TYPE_INTEGRAL,
					'dateBarrDebut' => '2010',
					'dateBarrFin' => '2023-02',
					'embargoInfo' => 'P24M',
					'noDebut' => 1,
					'volDebut' => '204',
					'noFin' => 0,
					'volFin' => 0,
					"numeroDebut" => "Vol. 204, no 1",
				]);
					codecept_debug($service1->getAttributes());
				// To update. Shared by 2 collections
				$service2 = $this->createService([
					'acces' => 'Restreint',
					'type' => \Service::TYPE_INTEGRAL,
					'dateBarrDebut' => '2023-02',
					'dateBarrFin' => '',
					'embargoInfo' => 'P24M',
				]);
				// Should not be updated (different type).
				$service3 = $this->createService([
					'acces' => 'Libre',
					'type' => \Service::TYPE_RESUME,
					'dateBarrDebut' => '2020',
					'dateBarrFin' => '',
				]);
				if (!$service1->id) {
					codecept_debug($service1->getAttributes());
					throw new \Exception("Service.save() failed");
				}

				\Yii::app()->db
					->createCommand(<<<SQL
						INSERT INTO Service_Collection VALUES
						({$service1->id}, {$collection2->id}),
						({$service2->id}, {$collection1->id}), ({$service2->id}, {$collection2->id}),
						({$service3->id}, {$collection1->id}), ({$service3->id}, {$collection2->id})
						SQL
					)->execute();
			}

			public function delete() {
				$this->issn->delete();
				$this->titre->delete();
				$this->revue->delete();
				$this->ressource->delete();
			}

			private function createService($attrs): \Service
			{
				$s = new \Service; // Unchanged.
				$s->ressourceId = (int) $this->ressource->id;
				$s->titreId = (int) $this->titre->id;
				$s->url = "https://www.biologie-journal.org";
				$s->lacunaire = $s->selection = 0;
				$s->numeroDebut = $s->numeroFin = "";
				$s->dateBarrInfo = "";
				$s->notes = "";
				$s->import = \models\import\ImportType::getSourceId('KBART');
				$s->statut = "normal";
				foreach ($attrs as $k => $v) {
					$s->{$k} = $v;
				}
				$s->save(false);
				return $s;
			}
		};
	}
}
