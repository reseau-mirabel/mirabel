<?php

namespace tests\unitwithyii\kbart;

use processes\kbart\convert\FromErudit;

class FromEruditTest extends \Codeception\Test\Unit
{
	public function testConvert()
	{
		$converter = new FromErudit();
		$converter->load(codecept_data_dir('kbart/erudit1.tsv'));
		$stream = fopen('php://memory', 'w');
		$converter->writeIntoHandler($stream);
		$kbart = stream_get_contents($stream, -1, 0);

		$expected = <<<'EOTXT'
publication_title	print_identifier	online_identifier	date_first_issue_online	num_first_vol_online	num_first_issue_online	date_last_issue_online	num_last_vol_online	num_last_issue_online	title_url	first_author	title_id	embargo_info	coverage_depth	notes	publisher_name	publication_type	date_monograph_published_print	date_monograph_published_online	monograph_volume	monograph_edition	first_editor	parent_publication_title_id	preceding_publication_title_id	access_type
"Acadiensis : Journal of the History of the Atlantic Region / Acadiensis: Revue d’histoire de la région Atlantique"	0044-5851	1712-7432	1971	1	1	2022	51	1	https://www.erudit.org/revues/acadiensis/#back-issues		acadiensis		fulltext		"Department of History at the University of New Brunswick"	serial								F
"Acadiensis : Journal of the History of the Atlantic Region / Acadiensis: Revue d’histoire de la région Atlantique"	0044-5851	1712-7432	2022	51	2				https://www.erudit.org/revues/acadiensis/#back-issues		acadiensis		fulltext		"Department of History at the University of New Brunswick"	serial								P

EOTXT;
		$this->assertSame($expected, $kbart);
	}

	public function testConvertEmbargoInfo()
	{
		$converter = new FromErudit();
		$converter->load(codecept_data_dir('kbart/erudit2.tsv'));
		$stream = fopen('php://memory', 'w');
		$converter->writeIntoHandler($stream);
		$kbart = stream_get_contents($stream, -1, 0);

		$expected = <<<'EOTXT'
publication_title	print_identifier	online_identifier	date_first_issue_online	num_first_vol_online	num_first_issue_online	date_last_issue_online	num_last_vol_online	num_last_issue_online	title_url	first_author	title_id	embargo_info	coverage_depth	notes	publisher_name	publication_type	date_monograph_published_print	date_monograph_published_online	monograph_volume	monograph_edition	first_editor	parent_publication_title_id	preceding_publication_title_id	access_type
"À bâbord ! : Revue sociale et politique"	1710-209X	1710-2103	2020		83	2021		89	https://www.erudit.org/revues/babord/#back-issues		babord	P1095D	fulltext		"Revue À bâbord !"	serial								F
"À bâbord ! : Revue sociale et politique"	1710-209X	1710-2103	2021		90				https://www.erudit.org/revues/babord/#back-issues		babord	P1095D	fulltext		"Revue À bâbord !"	serial								P

EOTXT;
		$this->assertSame($expected, $kbart);
	}
}
