<?php

class DataProviderExporterTest extends \Codeception\Test\Unit
{
	public function testCsv()
	{
		$provider = new CSqlDataProvider("SELECT id, nom FROM Editeur WHERE id < 5 ORDER BY id ASC");
		$columns = [
			"id:text:identifiant",
			['name' => 'nom', 'header' => "Nom de l'éditeur"],
		];
		$exporter = new components\DataProviderExporter($provider, $columns);

		$out = fopen('php://temp', 'w');
		$exporter->writeCsv($out);
		$csv = stream_get_contents($out, -1, 0);
		$this->assertSame(
			<<<EOCSV
			identifiant;"Nom de l'éditeur"
			2;"De Boeck Université"
			4;"Direction des journaux officiels"
			
			EOCSV,
			$csv
		);
	}
}