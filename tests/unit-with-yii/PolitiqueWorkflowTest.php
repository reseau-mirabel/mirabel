<?php

use processes\politique\Workflow;

class PolitiqueWorkflowTest extends \Codeception\Test\Unit
{
	public function testWrongRole()
	{
		$p = new Politique();
		$this->expectException('Exception');
		new Workflow($p, 'boss');
	}

	public function testAsPendingUser()
	{
		$p = new Politique();
		$action = new Workflow($p, 'pending');

		$p->status = null;
		$this->assertTrue($action->create());
		$this->assertEquals(Politique::STATUS_PENDING, $p->status);

		$p->status = Politique::STATUS_PENDING;
		$this->assertTrue($action->delete());
		$this->assertEquals(Politique::STATUS_DELETED, $p->status);

		$p->status = Politique::STATUS_PENDING;
		$this->assertTrue($action->update());
		$this->assertEquals(Politique::STATUS_PENDING, $p->status);

		// The following actions are not allowed

		$p->status = Politique::STATUS_DRAFT;
		$this->assertFalse($action->delete());
		$this->assertEquals(Politique::STATUS_DRAFT, $p->status);

		$p->status = Politique::STATUS_DRAFT;
		$this->assertFalse($action->update());
		$this->assertEquals(Politique::STATUS_DRAFT, $p->status);

		$p->status = Politique::STATUS_PENDING;
		$this->assertFalse($action->confirmUser());

		$p->status = Politique::STATUS_PENDING;
		$this->assertFalse($action->publish());
	}

	public function testAsOwnerUser()
	{
		$p = new Politique();
		$action = new Workflow($p, 'owner');

		$p->status = null;
		$this->assertTrue($action->create());
		$this->assertEquals(Politique::STATUS_DRAFT, $p->status);

		$p->status = Politique::STATUS_PENDING;
		$this->assertTrue($action->delete());
		$this->assertEquals(Politique::STATUS_DELETED, $p->status);

		$p->status = Politique::STATUS_DRAFT;
		$this->assertTrue($action->delete());
		$this->assertEquals(Politique::STATUS_DELETED, $p->status);

		$p->status = Politique::STATUS_PUBLISHED;
		$this->assertTrue($action->delete());
		$this->assertEquals(Politique::STATUS_TODELETE, $p->status);

		$p->status = Politique::STATUS_PENDING;
		$this->assertTrue($action->update());
		$this->assertEquals(Politique::STATUS_PENDING, $p->status);

		$p->status = Politique::STATUS_DRAFT;
		$this->assertTrue($action->update());
		$this->assertEquals(Politique::STATUS_DRAFT, $p->status);

		$p->status = Politique::STATUS_TOPUBLISH;
		$this->assertTrue($action->update());
		$this->assertEquals(Politique::STATUS_UPDATED, $p->status);

		$p->status = Politique::STATUS_PUBLISHED;
		$this->assertTrue($action->update());
		$this->assertEquals(Politique::STATUS_UPDATED, $p->status);

		$p->status = Politique::STATUS_UPDATED;
		$this->assertTrue($action->update());
		$this->assertEquals(Politique::STATUS_UPDATED, $p->status);

		$p->status = Politique::STATUS_DRAFT;
		$this->assertTrue($action->publish());
		$this->assertEquals(Politique::STATUS_TOPUBLISH, $p->status);

		// The following actions are not allowed

		$p->status = Politique::STATUS_TOPUBLISH;
		$this->assertFalse($action->publish());
		$this->assertEquals(Politique::STATUS_TOPUBLISH, $p->status);

		$p->status = Politique::STATUS_PENDING;
		$this->assertFalse($action->confirmUser());

		$p->status = Politique::STATUS_PENDING;
		$this->assertFalse($action->publish());

		$p->status = Politique::STATUS_PUBLISHED;
		$this->assertFalse($action->publish());
	}

	public function testAsAdminUser()
	{
		$p = new Politique();
		$action = new Workflow($p, 'admin');

		// admin-only actions

		$p->status = Politique::STATUS_PENDING;
		$this->assertTrue($action->confirmUser());
		$this->assertEquals(Politique::STATUS_DRAFT, $p->status);

		$p->status = Politique::STATUS_TOPUBLISH;
		$this->assertTrue($action->publish());
		$this->assertEquals(Politique::STATUS_PUBLISHED, $p->status);

		$p->status = Politique::STATUS_TOPUBLISH;
		$this->assertTrue($action->update());
		$this->assertEquals(Politique::STATUS_TOPUBLISH, $p->status);

		// actions allowed to other users

		$p->status = null;
		$this->assertTrue($action->create());
		$this->assertEquals(Politique::STATUS_DRAFT, $p->status);

		$p->status = Politique::STATUS_PENDING;
		$this->assertTrue($action->delete());
		$this->assertEquals(Politique::STATUS_DELETED, $p->status);

		$p->status = Politique::STATUS_DRAFT;
		$this->assertTrue($action->delete());
		$this->assertEquals(Politique::STATUS_DELETED, $p->status);

		$p->status = Politique::STATUS_PENDING;
		$this->assertTrue($action->update());
		$this->assertEquals(Politique::STATUS_PENDING, $p->status);

		$p->status = Politique::STATUS_DRAFT;
		$this->assertTrue($action->update());
		$this->assertEquals(Politique::STATUS_DRAFT, $p->status);

		$p->status = Politique::STATUS_PUBLISHED;
		$this->assertTrue($action->update());
		$this->assertEquals(Politique::STATUS_UPDATED, $p->status);

		$p->status = Politique::STATUS_UPDATED;
		$this->assertTrue($action->update());
		$this->assertEquals(Politique::STATUS_UPDATED, $p->status);

		$p->status = Politique::STATUS_DRAFT;
		$this->assertTrue($action->publish());
		$this->assertEquals(Politique::STATUS_TOPUBLISH, $p->status);

		// The following actions are not allowed

		$p->status = Politique::STATUS_PUBLISHED;
		$this->assertFalse($action->publish());
	}
}
