<?php

namespace tests\unitwithyii\cron;

use components\email\SentMessage;
use processes\cron\ar\Task;
use processes\cron\Cron;
use Yii;

class ServiceKbartTest extends \Codeception\Test\Unit
{
	private $configJson;

	private $transport;

	public function testCairn()
	{
		$this->setUpData();

		// Run
		$cron = new Cron();
		$cron->run(60*24);

		// Test
		// email
		$this->assertSame(1, $this->transport->getSentCount(), "Expected: 1 email sent to 1 address");
		$message = new SentMessage($this->transport->getLastSentMessage());
		$title = "6 revues rencontrées, dont 0 nouveautés, 3 modifications, 0 erreurs, et 1 créations d'un premier accès.";
		$body = <<<'EOTXT'
  *   1. Actes de la recherche en sciences sociales : 
	* Collections ajoutées à l'accès 4917 : IDs 98
	* Modification des accès
         http://localhost/revue/28/Actes-de-la-recherche-en-sciences-sociales-ARSS
  *   2. Afrique Contemporaine : 
	* Collections ajoutées à l'accès 4495 : IDs 98
	* Modification des accès
         http://localhost/revue/2/Afrique-Contemporaine
  *   3. Afrique Contemporaine : 
	* Accès « Afrique Contemporaine » inchangés.
         http://localhost/revue/2/Afrique-Contemporaine
  *   4. Annales historiques de la Révolution française : 
	* Collections ajoutées à l'accès 4899 : IDs 98
	* Modification des accès
         http://localhost/revue/29/Annales-historiques-de-la-Revolution-francaise
  *   5. Annales : Économies, Sociétés, Civilisations : 
	* ISSN 2268-3763 peut être ajouté.
	* Collections ajoutées au nouvel accès : IDs 98
	* Création des accès
         http://localhost/revue/718/Annales-Economies-Societes-Civilisations

      6 revues rencontrées, dont 0 nouveautés, 3 modifications, 0 erreurs, et 1 créations d'un premier accès.
EOTXT;
		$this->assertSame("$title\n\n$body", $message->getBody());
		$this->assertSame("[Mir@bel TEST] Import KBART Cairn.info [Cairn International]", $message->getSubject());
		// history
		$this->assertSame(1, (int) Yii::app()->db->createCommand("SELECT count(*) FROM CronHistory")->queryScalar());
		$history = \processes\cron\ar\History::model()->findBySql("SELECT * FROM CronHistory ORDER BY id DESC LIMIT 1");
		assert($history instanceof \processes\cron\ar\History);
		$history->id = 2;
		$history->startTime = 0;
		$history->endTime = 0;
		$this->assertEquals(
			[
				'id' => 2,
				'crontaskId' => 1,
				'config' => $this->configJson,
				'reportEmpty' => '0',
				'reportTitle' => $title,
				'reportBody' => $body,
				'reportFormat' => 1,
				'reportEmailBody' => '',
				'emailTo' => "bibi@here.localhost",
				'error' => '',
				'startTime' => 0,
				'endTime' => 0,
			],
			$history->getAttributes()
		);
	}

	protected function setUpData(): void
	{
		// Setup (prepare the test data)
		Yii::app()->db->createCommand("DELETE FROM CronTask")->execute();
		$task = new Task();
		$path = codecept_data_dir('kbart/cairn.txt');
		$this->configJson = '{"url":"' . $path . '", "ressourceId":3, "collectionIds":"98", "ssiConnu":true, "contenu":"Intégral", "acces":"Libre", "verbose":1}';
		$task->setAttributes(
			[
				'id' => 1,
				'name' => "Import d'accès Cairn par KBART",
				'description' => '',
				'fqdn' => '\\' . \processes\cron\tasks\ModelServiceKbart::class,
				'config' => $this->configJson,
				'schedule' => '{"periodicity":"daily","hours":["12:07"],"days":[0,1,2,3,4,5,6]}',
				'active' => true,
				'emailTo' => "bibi@here.localhost",
				'emailSubject' => '',
				'timelimit' => 10,
				'lastUpdate' => time(),
			],
			false
		);
		$task->save();
		$this->transport = new \components\email\TestTransport();
		\components\email\Mailer::getMailer($this->transport);
	}
}
