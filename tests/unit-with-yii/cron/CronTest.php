<?php

namespace tests\unitwithyii\cron;

use components\email\SentMessage;
use processes\cron\ar\Task;
use processes\cron\Cron;
use Yii;

class CronTest extends \Codeception\Test\Unit
{
	public function testRun()
	{
		Yii::app()->db->createCommand("DELETE FROM CronTask")->execute();
		Yii::app()->db->createCommand("ALTER TABLE CronHistory AUTO_INCREMENT=1")->execute();
		$task = new Task();
		$task->setAttributes(
			[
				'id' => 1,
				'name' => "Test d'attente",
				'description' => 'à supprimer après usage',
				'fqdn' => '\\' . \processes\cron\tasks\ModelSleep::class,
				'config' => '{"sleep":0}',
				'schedule' => '{"periodicity":"daily","hours":["12:07"],"days":[0,1,2,3,4,5,6]}',
				'active' => true,
				'emailTo' => "bibi@here.localhost\nCe Clampin ce.clampin@truc.localhost",
				'emailSubject' => 'dormir',
				'timelimit' => 10,
				'lastUpdate' => time(),
			],
			false
		);
		$task->save();
		$transport = new \components\email\TestTransport();
		\components\email\Mailer::getMailer($transport);

		$this->assertSame(0, $transport->getSentCount());
		$this->assertSame(0, (int) Yii::app()->db->createCommand("SELECT count(*) FROM CronHistory")->queryScalar());

		$startTime = time();
		$cron = new Cron();
		$cron->run(60*24);

		$this->assertSame(2, $transport->getSentCount(), "Expected: 1 email sent to 2 addresses");
		$lastEmail = new SentMessage($transport->getLastSentMessage());
		$this->assertSame(['bibi@here.localhost', '"Ce Clampin" <ce.clampin@truc.localhost>'], $lastEmail->getRecipients());
		$this->assertSame("[Mir@bel TEST] dormir", $lastEmail->getSubject(), "[{instance name}] [{task subject|model subject|task name}]");

		$this->assertSame(1, (int) Yii::app()->db->createCommand("SELECT count(*) FROM CronHistory")->queryScalar());
		$cronHistory = \processes\cron\ar\History::model()->findBySql("SELECT * FROM CronHistory LIMIT 1");
		$this->assertGreaterThanOrEqual($startTime, $cronHistory->endTime);
		$this->assertLessThan($startTime + 10, $cronHistory->endTime);
		$cronHistory->endTime = 0;
		$this->assertEquals(
			[
				'id' => 1,
				'crontaskId' => 1,
				'config' => '{"sleep":0}',
				'endTime' => 0,
				'startTime' => $startTime,
				'reportEmpty' => '0',
				'reportTitle' => '',
				'reportBody' => '',
				'reportFormat' => '1',
				'reportEmailBody' => "Ce texte est fixé par la tâche 'sleep' dans Mir@bel.\nElle a pour but de tester le système de cron.\n",
				'emailTo' => "bibi@here.localhost\nCe Clampin ce.clampin@truc.localhost",
				'error' => '',
			],
			$cronHistory->getAttributes()
		);

		Yii::app()->db->createCommand("DELETE FROM CronTask")->execute();
	}
}
