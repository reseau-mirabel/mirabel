<?php

namespace tests\unitwithyii\cron;

use components\email\SentMessage;
use processes\cron\ar\Task;
use processes\cron\Cron;
use Yii;

class ServiceKbart2Test extends \Codeception\Test\Unit
{
	private $configJson;

	private $transport;

	public function testCairn()
	{
		$this->setUpData();

		// Run
		$cron = new Cron();
		$cron->run(60*24);

		// Test
		$this->assertSame(1, $this->transport->getSentCount(), "Expected: 1 email sent to 1 address");
		$message = new SentMessage($this->transport->getLastSentMessage());
		$this->assertSame("[Mir@bel TEST] Import KBART Cairn.info [Bouquet Général,Bouquet Humanités,Bouquet Humanities and Social Science]", $message->getSubject());
		$title = "5 titres rencontrés, dont 1 nouveautés ignorées, 3 modifications, 0 erreurs, et 0 créations d'un premier accès.";
		$body = <<<'EOTXT'
cairn.txt

## Interventions réussies
   1. Actes de la recherche en sciences sociales — ARSS : http://localhost/revue/28/Actes-de-la-recherche-en-sciences-sociales-ARSS  
    Modification d'accès en ligne, revue « Actes de la recherche en sciences sociales » / « Cairn.info »  http://localhost/intervention/XXX
   2. Afrique Contemporaine : http://localhost/revue/2/Afrique-Contemporaine  
    Création d'accès en ligne, revue « Afrique Contemporaine » / « Cairn.info »  http://localhost/intervention/XXX
   3. Annales historiques de la Révolution française : http://localhost/revue/29/Annales-historiques-de-la-Revolution-francaise  
    Modification d'accès en ligne, revue « Annales historiques de la Révolution française » / « Cairn.info »  http://localhost/intervention/XXX
   4. Annales : Économies, Sociétés, Civilisations : http://localhost/revue/718/Annales-Economies-Societes-Civilisations  
    Création d'accès en ligne, revue « Annales : Économies, Sociétés, Civilisations » / « Cairn.info »  http://localhost/intervention/XXX

EOTXT;
		$this->assertSame(trim("$title\n\n$body"), preg_replace('#/intervention/\d+\b#', '/intervention/XXX', trim($message->getBody())));
		$this->assertSame(1, (int) Yii::app()->db->createCommand("SELECT count(*) FROM CronHistory")->queryScalar());
		$history = \processes\cron\ar\History::model()->findBySql("SELECT * FROM CronHistory ORDER BY id DESC LIMIT 1");
		assert($history instanceof \processes\cron\ar\History);
		$history->id = 2;
		$history->startTime = 0;
		$history->endTime = 0;
		$history->reportBody = preg_replace('#/intervention/\d+\b#', '/intervention/XXX', $history->reportBody);
		$this->assertEquals(
			[
				'id' => 2,
				'crontaskId' => 1,
				'config' => $this->configJson,
				'reportEmpty' => '0',
				'reportTitle' => $title,
				'reportBody' => $body,
				'reportFormat' => \processes\cron\ar\History::REPORTFORMAT_MARKDOWN,
				'reportEmailBody' => '',
				'emailTo' => "bibi@here.localhost",
				'error' => '',
				'startTime' => 0,
				'endTime' => 0,
			],
			$history->getAttributes()
		);
	}

	protected function setUpData(): void
	{
		// Setup (prepare the test data)
		Yii::app()->db->createCommand("DELETE FROM CronTask")->execute();
		$task = new Task();
		$path = codecept_data_dir('kbart/cairn.txt');
		$this->configJson = '{"url":"' . $path . '", "ressourceId":3, "collectionIds":"2 7 10", "ssiConnu":true, "contenu":"Intégral", "acces":"Libre", "verbose":1}';
		$task->setAttributes(
			[
				'id' => 1,
				'name' => "Import d'accès Cairn par KBART",
				'description' => '',
				'fqdn' => '\\' . \processes\cron\tasks\ModelServiceKbart2::class,
				'config' => $this->configJson,
				'schedule' => '{"periodicity":"daily","hours":["12:07"],"days":[0,1,2,3,4,5,6]}',
				'active' => true,
				'emailTo' => "bibi@here.localhost",
				'emailSubject' => '',
				'timelimit' => 10,
				'lastUpdate' => time(),
			],
			false
		);
		$task->save();
		$this->transport = new \components\email\TestTransport();
		\components\email\Mailer::getMailer($this->transport);
	}
}
