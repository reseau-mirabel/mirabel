<?php

namespace tests\unitwithyii\cron;

use processes\cron\SystemConverter;
use processes\cron\tasks;

class SystemConverterTest extends \Codeception\Test\Unit
{
	/**
	 * @dataProvider provideCronLines
	 */
	public function testExtractAttributesFromLine(array $expected, string $line)
	{
		$converter = new SystemConverter();
		$converter->verbose = 0;
		$attributes = $converter->extractAttributesFromLine($line);
		$this->assertSame($expected, $attributes);
	}

	public function provideCronLines(): array
	{
		return [
			[
				[],
				'#03 06 * * * commentaire'
			],
			[
				[],
				'03 06 * * * indexer'
			],
			[
				[
					'name' => "KBART OpenEdition Journals [Journals with Embargo Period]",
					'fqdn' => '\\' . tasks\ModelServiceKbart::class,
					'config' => '{"ressourceId":4,"collectionIds":"102","url":"https://www.openedition.org/tralala","ssiConnu":false,"contenu":"","acces":"","ignoreUrl":false,"verbose":1}',
					'schedule' => '{"periodicity":"daily","hours":["06:03"],"days":[0,1,2,3,4,5,6]}',
					'emailTo' => "mirabel_donnees@listes.localhost\nMarie.MARIE@mom.localhost",
					'emailSubject' => "Import OpenEdition embargo cron",
				],
				'03 06 * * * (cd /srv/m && ./yii import kbart --collection=102 "https://www.openedition.org/tralala" 2>&1) | mail -E -s "Import OpenEdition embargo cron" mirabel_donnees@listes.localhost Marie.MARIE@mom.localhost'
			],
			[
				[
					'name' => "KBART Ritimo",
					'fqdn' => '\\' . tasks\ModelServiceKbart::class,
					'config' => '{"ressourceId":12,"collectionIds":"","url":"http://www.ritimo.org/kbart/export","ssiConnu":true,"contenu":"","acces":"","ignoreUrl":false,"verbose":1}',
					'schedule' => '{"periodicity":"daily","hours":["11:35"],"days":[1,2,3,4,5,6]}',
					'emailTo' => "md@sciencespo-lyon.localhost",
					'emailSubject' => "Import Ritimo",
				],
				'35 11 * * 1-6 ./yii import kbart --ressource=12 --ssiConnu=1 "http://www.ritimo.org/kbart/export" | mail -E -s "Import Ritimo" md@sciencespo-lyon.localhost'
			],
			[
				[
					'name' => "KBART Cairn.info [Bouquet Documentation hospitalière]",
					'fqdn' => '\\' . tasks\ModelServiceKbart::class,
					'config' => '{"ressourceId":3,"collectionIds":"11","url":"https://cairn.org/kbart","ssiConnu":false,"contenu":"","acces":"Libre","ignoreUrl":false,"verbose":1}',
					'schedule' => '{"periodicity":"daily","hours":["06:42"],"days":[3]}',
					'emailTo' => "a@x.localhost\nb@x.localhost\nc@x.localhost",
					'emailSubject' => "Import Cairn Documentation hospitalière",
				],
				'42 6 * * wed ./yii import kbart --collection=11 --acces=Libre https://cairn.org/kbart | mail -E -s "Import Cairn Documentation hospitalière" -- a@x.localhost b@x.localhost c@x.localhost'
			],
			[
				[
					'name' => "Accès importés de CairnMagazine",
					'fqdn' => '\\' . tasks\ModelServiceCairnMagazine::class,
					'config' => '{}',
					'schedule' => '{"periodicity":"daily","hours":["14:03"],"days":[0,1,2,3,4,5,6]}',
					'emailTo' => "a@x.localhost",
					'emailSubject' => "Import CairnMagazine cron",
				],
				'03 14 * * * ./yii import CairnMagazine | mail -E -s "Import CairnMagazine cron" a@x.localhost'
			],
			[
				[
					'name' => "Accès jamais importés ou disparus",
					'fqdn' => '\\' . tasks\ModelServiceObsolete::class,
					'config' => '{"since":"-1weeks"}',
					'schedule' => '{"periodicity":"daily","hours":["04:28"],"days":[0]}',
					'emailTo' => "",
					'emailSubject' => "",
				],
				'28 04 * * 7 cd ~/mirabel2-prod && ./yii import delete --since="-1weeks" --emails=1'
			],
			[
				[
					'name' => "Attribut APC",
					'fqdn' => '\\' . tasks\ModelAttributImport::class,
					'config' => json_encode([
						'url' => 'https://docs.google.com/spreadsheets/d/1K/export?format=csv',
						'config' => '{"sourceattributId":1,"colIdentifiers":{"issn":[1,3],"mirabel":[16],"ppn":[2,4]},"colTitre":0}',
					], JSON_UNESCAPED_SLASHES),
					'schedule' => '{"periodicity":"monthly","hours":["22:20"],"days":[1]}',
					'emailTo' => "a@x.localhost\nb@x.localhost",
					'emailSubject' => "mirabel attribut Revantiq",
				],
				'20 22 1 * * ./yii attribut import --uri=\'https://docs.google.com/spreadsheets/d/1K/export?format=csv\' --config=\'{"sourceattributId":1,"colIdentifiers":{"issn":[1,3],"mirabel":[16],"ppn":[2,4]},"colTitre":0}\' | mail -E -s "mirabel attribut Revantiq" a@x.localhost b@x.localhost'
			],
			[
				[
					'name' => "Attribut APC : titres manquants dans Mir@bel",
					'fqdn' => '\\' . tasks\ModelAttributTitresInconnus::class,
					'config' => json_encode([
						'identifiant' => 'apc',
					], JSON_UNESCAPED_SLASHES),
					'schedule' => '{"periodicity":"daily","hours":["04:26"],"days":[0]}',
					'emailTo' => "m@localhost",
					'emailSubject' => "dans Mir@bel",
				],
				'26 04 * * 7   ./yii attribut unknown --identifiant="apc"| mail -E -s "dans Mir@bel" m@localhost',
			],
			[
				[
					'name' => "Liens MIAR",
					'fqdn' => '\\' . tasks\ModelLinkImport::class,
					'config' => json_encode([
						'source' => 'miar',
						'obsoleteDelete' => false,
						'obsoleteSince' => "7 days ago",
						'url' => "https://miar.ub.edu/path/to/csv",
					], JSON_UNESCAPED_SLASHES),
					'schedule' => '{"periodicity":"daily","hours":["10:12"],"days":[0,3]}',
					'emailTo' => "m@localhost",
					'emailSubject' => "",
				],
				'12 10 * * 3,7 ./yii links miar --csv="https://miar.ub.edu/path/to/csv" --obsoleteSince="7 days ago" --email="m@localhost"',
			],
			[
				[
					'name' => "Liens DOAJ",
					'fqdn' => '\\' . tasks\ModelLinkImport::class,
					'config' => json_encode([
						'source' => 'doaj',
						'obsoleteDelete' => true,
						'obsoleteSince' => "7 days ago",
						'url' => "",
					], JSON_UNESCAPED_SLASHES),
					'schedule' => '{"periodicity":"daily","hours":["05:47"],"days":[1,4]}',
					'emailTo' => "a@localhost\nb@localhost",
					'emailSubject' => "",
				],
				'47 05 * * mon,thu ./yii links doaj --obsoleteSince="7 days ago" --obsoleteDelete=1 --email="a@localhost" --email="b@localhost"',
			],
		];
	}
}
