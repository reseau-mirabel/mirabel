<?php

namespace tests\unitwithyii\cron;

use components\email\SentMessage;
use processes\cron\ar\Task;
use processes\cron\Cron;
use Yii;

class AlertTest extends \Codeception\Test\Unit
{
	private ?string $configJson = null;

	private ?\components\email\TestTransport $transport = null;

	public function testSuiviCairn()
	{
		$this->setUpData();
		$this->assertSame(0, (int) Yii::app()->db->createCommand("SELECT count(*) FROM CronHistory")->queryScalar());

		// Run
		$cron = new Cron();
		$cron->run(60*24);

		// Test
		$this->assertSame(1, $this->transport->getSentCount(), "Expected: 1 email sent to 1 address");
		$message = new SentMessage($this->transport->getLastSentMessage());
		$yesterday = date('Y-m-d', time() - 86400); // The alert is about the past day
		$this->assertSame("[Mir@bel TEST] Propositions à traiter dans Mir@bel : 1 ($yesterday)", $message->getSubject());
		$this->assertSame(1, (int) Yii::app()->db->createCommand("SELECT count(*) FROM CronHistory")->queryScalar());
		$history = \processes\cron\ar\History::model()->findBySql("SELECT * FROM CronHistory ORDER BY id DESC LIMIT 1");
		assert($history instanceof \processes\cron\ar\History);
		$history->id = 2;
		$history->startTime = 0;
		$history->endTime = 0;
		$history->reportBody = preg_replace('#/intervention/\d+\b#', '/intervention/XXX', $history->reportBody);
		$this->assertEquals(
			[
				'id' => 2,
				'crontaskId' => 1,
				'config' => $this->configJson,
				'reportEmpty' => '0',
				'reportTitle' => '',
				'reportBody' => '',
				'reportFormat' => 1,
				'reportEmailBody' => '',
				'emailTo' => "bibi@here.localhost",
				'error' => '',
				'startTime' => 0,
				'endTime' => 0,
			],
			$history->getAttributes()
		);
	}

	/*
	 * Setup (prepare the test data)
	 */
	protected function setUpData(): void
	{
		// Task
		Yii::app()->db->createCommand("DELETE FROM CronTask")->execute();
		$task = new Task();
		$this->configJson = '{"intervention":"all", "since":"", "before":"", "verbose":false, "cc":"", "ccToPartVe":"", "ccToPartDe":""}';
		$task->setAttributes(
			[
				'id' => 1,
				'name' => "Alertes de suivi",
				'description' => '',
				'fqdn' => '\\' . \processes\cron\tasks\ModelAlert::class,
				'config' => $this->configJson,
				'schedule' => '{"periodicity":"daily","hours":["12:07"],"days":[0,1,2,3,4,5,6]}',
				'active' => true,
				'emailTo' => "bibi@here.localhost",
				'emailSubject' => '',
				'timelimit' => 10,
				'lastUpdate' => time(),
			],
			false
		);
		$task->save();

		// Intervention that will require an email
		$titre = \Titre::model()->findByPk(28);
		assert($titre instanceof \Titre);
		$i = $titre->buildIntervention(false);
		$issn = new \Issn();
		$issn->titreId = 28;
		$issn->bnfArk = 'cb344387394';
		$i->contenuJson->create($issn);
		$i->save();

		// Email
		$this->transport = new \components\email\TestTransport();
		\components\email\Mailer::getMailer($this->transport);
	}
}
