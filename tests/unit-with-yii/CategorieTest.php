<?php

class CategorieTest extends \Codeception\Test\Unit
{
	public function testCompleteTerm()
	{
		$result = \processes\completion\Completer::type('Categorie')->completeTerm('politique');
		$this->assertCount(18, $result);
		// "Mouvements politiques" is the first to match by name, but by description there is...
		$this->assertSame("Administration, gouvernement", $result[0]['value']);
	}

	/**
	 * @dataProvider provideNode
	 */
	public function testGetNode(int $rootId, string $role, array $childrenIds)
	{
		$rootNode = Categorie::model()->findByPk($rootId);
		$result = $rootNode->getNode(1);
		$this->assertArrayHasKey('id', $result);
		$this->assertSame($rootId, $result['id']);
		$this->assertSame($rootNode->categorie, $result['text']);
		$this->assertSame($role, $result['type']);
		$this->assertCount(count($childrenIds), $result['children']);
		foreach ($childrenIds as $rank => $id) {
			$this->assertEquals($id, $result['children'][$rank]['id'], print_r($result['children'], true));
		}
	}

	public function provideNode(): array
	{
		return [
			[83, 'root', [99, 100, 101, 102, 103, 104]], // 138 is a child with role 'refus'
			[99, 'default', [109, 108, 107, 106, 105, 149]],
			[149, 'candidat', []],
		];
	}
}
