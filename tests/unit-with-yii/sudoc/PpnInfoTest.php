<?php

namespace tests\unitwithyii\sudoc;

use commands\models\sudoc\PpnInfo;
use models\marcxml\MarcTitre;
use models\marcxml\Parser;
use models\sudoc\Notice;

class PpnInfoTest extends \Codeception\Test\Unit
{
	public function testApply()
	{
		$ppn = "136188982";
		$notice = self::buildNotice($ppn);

		$output = fopen("php://memory", 'w');
		$ppnInfo = new PpnInfo($output);
		$ppnInfo->csvColumns = 6;
		$ppnInfo->csvSeparator = ";";
		$ppnInfo->simulation = true;
		$ppnInfo->verbose = true;

		$row = [
			'titre' => "Caribbean studies",
			'revueId' => 5877,
			'titreId' => 7656,
			'bnfArk' => null,
			'dateDebut' => '',
			'dateFin' => '',
			'issn' => '1940-9095',
			'pays' => '',
			'sudocNoHolding' => '0',
			'sudocPpn' => $ppn,
			'support' => 'electronique',
			'worldcatOcn' => null,
		];
		$ppnInfo->apply($row, $notice);
		$result = stream_get_contents($output, null, 0);

		$this->assertSame('NOP;1940-9095;136188982;7656;"Caribbean studies";' . "\n", $result);
	}

	private static function buildNotice(string $ppn): Notice
	{
		$xmlFile = codecept_data_dir("sudoc-marcxml/$ppn.xml");
		$parser = new Parser();
		$marc = new MarcTitre();
		$parser->parse(file_get_contents($xmlFile), $marc);
		$notice = new Notice();
		$notice->fromMarcXml($marc);
		return $notice;
	}
}
