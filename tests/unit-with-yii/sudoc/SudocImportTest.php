<?php

namespace tests\unitwithyii\sudoc;

use models\sudoc\Import;

class SudocImportTest extends \Codeception\Test\Unit
{
	public function testFile()
	{
		$fileIn = codecept_data_dir('sudoc-import/MIRABEL_GLOBAL_LIBRESACCES.xlsx');
		$fileOut = tempnam(sys_get_temp_dir(), 'mirabel_tests_') . ".xlsx";

		$import = new Import();
		$import->simulation = true;
		$import->verbosity = 1;
		$this->expectOutputString(
			<<<EOTXT
			ERR Le titre censé être associé à l'issn 1760-611X est introuvable
			INFO Cet issn 0002-0478 est déjà associé à ce même titre
			ERR Le titre censé être associé à l'issn 1718-9977 est introuvable
			INFO L'issn 1234-5678 sera ajouté au titre 98, revue 98
			ERR Le titre censé être associé à l'issn 0181-1916 est introuvable

			EOTXT
		);
		$import->file($fileIn, $fileOut);

		if (is_file($fileOut)) {
			unlink($fileOut);
		}
	}
}