<?php

namespace tests\unitwithyii\sudoc;

use Issn;
use models\marcxml\MarcTitre;
use models\marcxml\Parser;
use models\sudoc\Notice;

// Codeception fails within provideMarcxml() when the Issn class is not already loaded.
// Maybe the provider runs _before_ the bootstrap that configures the autoloader?
require_once dirname(__DIR__, 3) . '/src/www/protected/models/Issn.php';

class SudocNoticeTest extends \Codeception\Test\Unit
{
	/**
	 * @dataProvider provideMarcxml
	 */
	public function testFromMarcXml(array $expected)
	{
		$record = self::parseFile($expected['ppn']);
		$notice = new Notice();
		$notice->fromMarcXml($record);
		foreach ($expected as $eK => $eV) {
			$this->assertSame($eV, $notice->$eK, "PPN {$expected['ppn']}: field '$eK' must have value " . json_encode($eV));
		}
	}

	public function provideMarcxml(): array
	{
		return [
			[[
				'ppn' => "039228460",
				'bnfArk' => 'cb343491837',
				'dateDebut' => "1951",
				'dateFin' => "",
				'issn' => "0035-2950",
				'issnl' => "0035-2950",
				'pays' => 'FR',
				'statut' => Issn::STATUT_VALIDE,
				'sudocNoHolding' => false,
				'support' => Issn::SUPPORT_PAPIER,
				'titre' => "Revue française de science politique",
				'titreIssn' => "Revue française de science politique",
				'titreVariantes' => [
					['variante' => "Revue française de science politique", 'lang' => 'fre', 'zone' => '200$a', 'zoneLang' => '101$a'],
					['variante' => "RFSP", 'lang' => 'fre', 'zone' => '517.|#$a', 'zoneLang' => '101$a'],
					['variante' => "Revue française de science politique", 'lang' => 'fre', 'zone' => '530.0#$a', 'zoneLang' => '101$a'],
					['variante' => "Rev. fr. sci. polit.", 'lang' => 'fre', 'zone' => '531.##$a', 'zoneLang' => '101$a'],
				],
				'url' => 'http://www.afsp.msh-paris.fr/publi/rfsp/rfsp.html',
				'worldcat' => '185444438',
			]],
			[[
				'ppn' => "090133803",
				'bnfArk' => null,
				'dateDebut' => "",
				'dateFin' => "",
				'issn' => "1661-1802",
				'issnl' => "1661-1802",
				'pays' => 'CH',
				'sudocNoHolding' => false,
				'support' => Issn::SUPPORT_ELECTRONIQUE,
				'titre' => "RESSI",
				'titreIssn' => "RESSI",
				'url' => 'http://www.ressi.ch',
				'worldcat' => '61762295',
			]],
			[[
				'ppn' => "119905655",
				'dateDebut' => "2005",
				'dateFin' => "",
				'bnfArk' => null,
				'issn' => null,
				'pays' => 'FR',
				'sudocNoHolding' => true,
				'support' => Issn::SUPPORT_PAPIER,
				'titre' => "Collection La France de Jules Verne",
				'titreIssn' => null,
				'url' => null,
				'worldcat' => null,
			]],
			[[
				'ppn' => "262821222",
				'issn' => "2948-040X",
				'pays' => 'IL',
				'support' => Issn::SUPPORT_ELECTRONIQUE,
				'titre' => "ʿAtiqot",
				'url' => "http://www.atiqot.org.il/past.aspx",
			]],
		];
	}

	private static function parseFile(string $ppn): MarcTitre
	{
		$dataDir = codecept_data_dir('/sudoc-marcxml');
		$parser = new Parser();
		$marc = new MarcTitre();
		$parser->parse(file_get_contents("$dataDir/$ppn.xml"), $marc);
		return $marc;
	}
}
