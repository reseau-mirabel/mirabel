<?php

class CmsTest extends \Codeception\Test\Unit
{
	public function testListLast()
	{
		$this->assertSame('<ul><li>Aucune modification</li></ul>', \processes\cms\HtmlRender::listLastInterventions(2));
	}

	public function testToHtml()
	{
		$cms = new Cms();
		$cms->type = 'markdown';
		$cms->content = <<<EOMD
			## Mon titre
			%NB_REVUES% revues
			<picture><img src="logo.png" alt="logo"></picture>
			<iframe title="vimeo-player" src="https://player.vimeo.com/video/531796342" width="400" height="225" frameborder="0" allowfullscreen></iframe>
			EOMD;

		$html = $cms->toHtml();
		$this->assertStringContainsString('<h2 id="h2-mon-titre">Mon titre</h2>', $html);
		$this->assertStringContainsString("20 revues", $html);
		$this->assertStringContainsString("<picture>", $html);
		$this->assertStringContainsString("<iframe ", $html);
	}

	public function testToHtmlWithComment()
	{
		$cms = new Cms();
		$cms->type = 'markdown';
		$cms->content = <<<EOMD
			Mon titre
			---------
			<!--
			<picture><img src="logo.png" alt="logo"></picture>
			EOMD;

		$html = $cms->toHtml(true, null);
		$this->assertStringContainsString('<h2 id="h2-mon-titre">Mon titre</h2>', $html);
		$this->assertStringNotContainsString("<picture>", $html);
		$this->assertStringNotContainsString("<!--", $html);
	}

	public function testToHtmlWithUrl()
	{
		$cms = new Cms();
		$cms->type = 'markdown';
		$cms->content = <<<EOMD
			- l'[**instance de démonstration** de Mir@bel](https://mirabel-demo.sciencespo-lyon.fr/).
			- [tableau de bord](/partenaire/tableau-de-bord/%PARTENAIRE.ID%) est un lien.
			
			Nous sommes %NB_COMPTES_TYPE_TOUT% veilleurs actifs
			EOMD;

		$html = $cms->toHtml(true, Partenaire::model()->findByPk(1));
		$this->assertStringContainsString(<<<HTML
			l'<a href="https://mirabel-demo.sciencespo-lyon.fr/"><strong>instance de démonstration</strong> de Mir@bel</a>.
			HTML,
			$html
		);
		$this->assertStringContainsString(<<<HTML
			<a href="/partenaire/tableau-de-bord/1">tableau de bord</a> est un lien
			HTML,
			$html
		);
		$this->assertStringContainsString("Nous sommes 5 veilleurs actifs", $html);
	}
}
