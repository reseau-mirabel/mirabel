<?php

class TitreSearchTest extends \Codeception\Test\Unit
{
	public function testSearchAboAcces()
	{
		$cs = new SearchTitre();
		$cs->abonnement = true;
		$cs->accesLibre = true;
		$cs->pid = 1;

		$this->assertTrue($cs->validate());
		$dataProvider = $cs->search();
		$this->assertSame(18, $dataProvider->getTotalItemCount());
		$this->assertSame("[<em>Accès libre ou abonné</em>]", $cs->htmlSummary());
	}

	public function testSearchAboAccesOwned()
	{
		$cs = new SearchTitre();
		$cs->abonnement = true;
		$cs->accesLibre = true;
		$cs->owned = true;
		$cs->pid = 1;

		$this->assertTrue($cs->validate());
		$dataProvider = $cs->search();
		$this->assertSame(19, $dataProvider->getTotalItemCount());
		$this->assertSame("[<em>Accès libre ou abonné ou disponible à “3Mousquetaires”</em>]", $cs->htmlSummary());
	}

	public function testSearchAboAndOwned()
	{
		$cs = new SearchTitre();
		$cs->abonnement = true;
		$cs->owned = true;
		$cs->pid = 1;
		$cs->aboCombine = 'ET';

		$this->assertTrue($cs->validate());
		$dataProvider = $cs->search();
		$this->assertSame(8, $dataProvider->getTotalItemCount());
		$this->assertSame("[<em>abonné en ligne ET disponible à “3Mousquetaires”</em>]", $cs->htmlSummary());
	}

	public function testSearchAboOrOwned()
	{
		$cs = new SearchTitre();
		$cs->abonnement = true;
		$cs->owned = true;
		$cs->pid = 1;
		$cs->aboCombine = 'OU';

		$this->assertTrue($cs->validate());
		$dataProvider = $cs->search();
		$this->assertSame(17, $dataProvider->getTotalItemCount());
		$this->assertSame("[<em>abonné en ligne OU disponible à “3Mousquetaires”</em>]", $cs->htmlSummary());
	}

	public function testSearchAcceslibre()
	{
		$cs = new SearchTitre();
		$cs->accesLibre = true;

		$this->assertTrue($cs->validate());
		$dataProvider = $cs->search();
		$this->assertSame(17, $dataProvider->getTotalItemCount());
		$this->assertSame("[<em>Accès libre</em>]", $cs->htmlSummary());
	}

	public function testSearchAttribut()
	{
		$cs = new SearchTitre();
		$cs->attribut = 1;
		$this->assertTrue($cs->validate());
		$dataProvider = $cs->search();
		$this->assertSame(0, $dataProvider->getTotalItemCount());
		$this->assertSame("[<em>Avec attribut APC</em>]", $cs->htmlSummary());

		$cs->attribut = 6;
		$this->assertTrue($cs->validate());
		$this->assertSame(1, $cs->search()->getTotalItemCount());
	}

	public function testSearchDates()
	{
		$cs = new SearchTitre();
		$cs->hdateVerif = '2020';
		$this->assertTrue($cs->validate());
		$this->assertSame(1, $cs->search()->getTotalItemCount());
		$this->assertSame("[Vérifié : <em>2020</em>]", $cs->htmlSummary());

		$cs->hdateVerif = '2019-';
		$this->assertTrue($cs->validate());
		$this->assertSame(16, $cs->search()->getTotalItemCount());

		$cs->hdateModif = '-2018';
		$cs->hdateVerif = '';
		$this->assertTrue($cs->validate());
		$this->assertSame(3, $cs->search()->getTotalItemCount());

		$cs->hdateModif = '';
		$cs->hdateVerif = '!';
		$this->assertTrue($cs->validate());
		$this->assertSame(0, $cs->search()->getTotalItemCount());
	}

	public function testSearchGrappe()
	{
		$cs = new SearchTitre();
		$cs->grappe = "2";
		$this->assertTrue($cs->validate());
		$this->assertSame("[Grappe  : <em><span>Atron<a", substr($cs->htmlSummary(), 0, 28));
		$dataProvider = $cs->search();
		$this->assertSame(13, $dataProvider->getTotalItemCount());

		$cs->grappe = "2";
		$cs->monitored = "1";
		$this->assertTrue($cs->validate());
		$this->assertSame('[<em>Suivi</em>] [Grappe  : <em><span>Atron', substr($cs->htmlSummary(), 0, 43));
		$this->assertSame(3, $cs->search()->getTotalItemCount());

		$cs->grappe = "1";
		$this->assertFalse($cs->validate());

		$cs->grappe = "2'<script>";
		$this->assertFalse($cs->validate());
		$this->assertSame(2, $cs->grappe);
	}

	public function testSearchLangues()
	{
		$cs = new SearchTitre();
		$cs->langues = "eng, spa";
		$this->assertTrue($cs->validate());
		$dataProvider = $cs->search();
		$this->assertSame(3, $dataProvider->getTotalItemCount());
		$this->assertSame("[Langues : <em>anglais OU espagnol</em>]", $cs->htmlSummary());

		$cs->langues = "aucune";
		$this->assertTrue($cs->validate());
		$this->assertSame(0, $cs->search()->getTotalItemCount());
		$this->assertSame("[Langues : <em>aucune</em>]", $cs->htmlSummary());
	}

	public function testSearchNoCategory()
	{
		$cs = new SearchTitre();
		$this->assertStringContainsString("aucun filtre", $cs->htmlSummary());

		$cs->sanscategorie = 1;
		$this->assertTrue($cs->validate());
		$dataProvider = $cs->search();
		$this->assertSame(1, $dataProvider->getTotalItemCount());
		$this->assertSame("[<em>Sans thématique</em>]", $cs->htmlSummary());
		$this->assertSame([], $cs->getCategories());
	}

	public function testSearchRessource()
	{
		$cs = new SearchTitre();
		$cs->ressourceId = "a488";
		$this->assertFalse($cs->validate(), print_r($cs->getErrors(), true));
		$this->assertArrayHasKey('ressourceId', $cs->getErrors());

		$cs->ressourceId = [488];
		$cs->vivant = 1;
		$this->assertTrue($cs->validate(), print_r($cs->getErrors(), true));
		$dataProvider = $cs->search();
		$this->assertSame(2, $dataProvider->getTotalItemCount());
		$this->assertStringContainsString("[Ressource : <em>Dalloz Revues</em>]", $cs->htmlSummary());
		$this->assertStringContainsString("[<em>Revue vivante</em>]", $cs->htmlSummary());
	}

	public function testSearchCombinedCategories()
	{
		$cs = new SearchTitre();
		$cs->categorie = "31_148";
		$cs->categoriesEt = true;
		$this->assertTrue($cs->validate(), print_r($cs->getErrors(), true));
		$dataProvider = $cs->search();
		$this->assertSame([31, 148], $cs->categorie);
		$this->assertSame(1, $dataProvider->getTotalItemCount());
		$this->assertStringContainsString("[Thématique : <em>Philosophie ET Actualité, presse</em>]", $cs->htmlSummary());

		$cs->categorie = "3'<script>";
		$this->assertFalse($cs->validate());
		$this->assertSame([], $cs->categorie);

		$cs->categorie = "2_3'<script>_4";
		$this->assertFalse($cs->validate());
		$this->assertSame([2, 4], $cs->categorie);
	}
}
