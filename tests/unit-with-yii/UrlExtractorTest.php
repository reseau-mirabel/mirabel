<?php

class UrlExtractorTest extends \Codeception\Test\Unit
{
	public function testExtractFromCmsHtml()
	{
		$cms = new Cms();
		$cms->id = 1;
		$cms->type = 'html';
		$cms->content = <<<EOHTML
			<p>Ceci est <a href="https://un.lien/vers/rien">un lien</a>
				et un <a href="http://un.lien/vers?rien[r]=%20a&amp;ok=2">deuxième</a>.</p>
			Cela est une URL : <a href="http://example.localhost/hop?q[x]=1">http://example.localhost/hop?q[x]=1</a>
			EOHTML;
		$this->assertSame(
			[
				'https://un.lien/vers/rien',
				'http://un.lien/vers?rien[r]=%20a&ok=2',
				'http://example.localhost/hop?q[x]=1',
			],
			processes\urlverif\UrlExtractor::extract($cms)['checked']
		);
	}

	public function testExtractFromCmsMarkdown()
	{
		$cms = new Cms();
		$cms->id = 1;
		$cms->type = 'markdown';
		$cms->content = <<<EOMD
			Ceci est [un lien](https://un.lien/vers/rien) et un [deuxième](http://un.lien/vers?rien[r]=%20a&ok=2).
			Cela est une URL : <http://example.localhost/hop?q[x]=1>
			Sans balisage Markdown http://example.localhost/hop?q[x]=2 mais sans texte accolé.
			Enfin un [lien local](/vers/rien)
			EOMD;
		$this->assertSame(
			[
				'https://un.lien/vers/rien',
				'http://un.lien/vers?rien%5Br%5D=%20a&ok=2',
				'http://example.localhost/hop?q%5Bx%5D=1',
				'http://example.localhost/hop?q[x]=2',
				'http://localhost/vers/rien',
			],
			processes\urlverif\UrlExtractor::extract($cms)['checked']
		);
	}
}
