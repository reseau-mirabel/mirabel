<?php

use components\sphinx\DataProvider;
use models\sphinx\Revues;

class SearchNavigationTest extends \Codeception\Test\Unit
{
	public function testRevueNav()
	{
		$dataProvider = self::createDataProvider();

		$sn = new SearchNavigation();
		$hash = $sn->save("revue/index", ['lettre' => 'A'], 'revueid', $dataProvider->getData());
		$firstHash = $sn->getHash(0);
		$results = [444, 28, 1, 103, 2]; // list of Revue.id for the first page

		$newSn = new SearchNavigation();
		$this->assertTrue($newSn->isEmpty());

		// Load the hash at the first position
		$newSn->loadByHash($firstHash);
		$this->assertFalse($newSn->isEmpty());
		$this->assertSame(null, $newSn->getPreviousResult());
		$this->assertSame($results[1], $newSn->getNextResult()->revueid); // Revue.id of the second result

		// Load the hash at the second position
		$newSn->loadByHash($newSn->getHash(1));
		$this->assertSame($results[0], $newSn->getPreviousResult()->revueid);
		$this->assertSame($results[2], $newSn->getNextResult()->revueid);

		// Load the hash at the third position
		$newSn->loadByHash($newSn->getHash(2));
		$this->assertSame($results[1], $newSn->getPreviousResult()->revueid);
		$this->assertSame($results[3], $newSn->getNextResult()->revueid);

		// Load the hash then set the position
		$newSn->loadByHash($hash);
		$this->assertSame($results[2], $newSn->getPreviousResult($results[3])->revueid, print_r($newSn, true));
		$this->assertSame($results[4], $newSn->getNextResult($results[3])->revueid);
	}

	private static function createDataProvider(): DataProvider
	{
		$criteria = new CDbCriteria;
		$criteria->addColumnCondition(['lettre1' => 1]); // A
		$criteria->addColumnCondition(['obsolete' => 0]);

		$sort = new CSort(Revues::class);
		$sort->defaultOrder = 'cletri ASC';

		return new DataProvider(
            Revues::model(),
            [
				'criteria' => $criteria,
                'pagination' => [
					'pageSize' => 5,
					'params' => ['lettre' => 'A'],
				],
				'sort' => $sort,
            ]
        );
	}
}
