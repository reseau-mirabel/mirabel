<?php

namespace tests\unitwithyii\idref;

use processes\idref\Notice;
use processes\idref\UpdateEditeur;

class UpdateEditeurTest extends \Codeception\Test\Unit
{
	public function testDates()
	{
		$notice = self::createNotice();
		$notice->dateDebut = '1945';
		$notice->dateFin = '1981-05-01';
		$writer = new UpdateEditeur();

		$editeur = \Editeur::model()->findByAttributes(['idref' => '026485842']);
		$writer->update($editeur, $notice);
		$this->assertSame(
			<<<CSV
				ID;opération;nom;type;debut;"ancien début";fin;"ancienne fin";pays;alertes-erreurs
				19;OK;Dalloz;coll.;1945;;1981-05-01;;FR;"modifications : dateDebut dateFin"

				CSV,
			$writer->getCsv()
		);

		$editeurUpdated = \Editeur::model()->findByPk($editeur->id);
		$this->assertSame($notice->dateDebut, $editeurUpdated->dateDebut);
		$this->assertSame($notice->dateFin, $editeurUpdated->dateFin);
	}

	public function testEmpty()
	{
		$notice = self::createNotice();
		$writer = new UpdateEditeur();

		$editeur = \Editeur::model()->findByAttributes(['idref' => '026485842']);
		$editeur->dateDebut = '1945';
		$editeur->dateFin = '2000';
		$editeur->paysId = 62; // FR

		$writer->update($editeur, $notice);
		$this->assertSame('', $writer->getCsv());
	}

	public function testPays()
	{
		$notice = self::createNotice();
		$writer = new UpdateEditeur();

		$editeur = \Editeur::model()->findByAttributes(['idref' => '026485842']);
		$editeur->dateDebut = '1945';
		$editeur->paysId = null;
		$editeur->save(false);

		$writer->update($editeur, $notice);
		$this->assertSame(
			<<<CSV
				ID;opération;nom;type;debut;"ancien début";fin;"ancienne fin";pays;alertes-erreurs
				19;OK;Dalloz;coll.;;1945;;;FR;"modifications : paysId"

				CSV,
			$writer->getCsv()
		);

		$editeurUpdated = \Editeur::model()->findByPk($editeur->id);
		$this->assertSame($editeur->dateDebut, $editeurUpdated->dateDebut);
		$this->assertSame($editeur->dateFin, $editeurUpdated->dateFin);
		$this->assertSame(62, (int) $editeurUpdated->paysId); // France => Pays.id = 62
	}

	public function testPaysOverwrite()
	{
		$notice = self::createNotice();
		$writer = new UpdateEditeur();

		$editeur = \Editeur::model()->findByAttributes(['idref' => '026485842']);
		$editeur->dateDebut = '1945';
		$editeur->paysId = 62; // pays FR
		$editeur->save(false);

		$notice->pays = '';
		$writer->update($editeur, $notice); // empty => ignored
		$notice->pays = 'RE';
		$writer->update($editeur, $notice); // RE = FR => ignored
		$notice->pays = 'BR';
		$writer->update($editeur, $notice); // distinct => conflict
		$this->assertSame(
			<<<CSV
				ID;opération;nom;type;debut;"ancien début";fin;"ancienne fin";pays;alertes-erreurs
				19;DIVERGENCE;Dalloz;coll.;;1945;;;BR;"les pays M et idref.fr diffèrent (FR / BR)"

				CSV,
			$writer->getCsv()
		);

		$editeurUpdated = \Editeur::model()->findByPk($editeur->id);
		$this->assertSame($editeur->dateDebut, $editeurUpdated->dateDebut);
		$this->assertSame($editeur->dateFin, $editeurUpdated->dateFin);
		$this->assertSame(62, (int) $editeurUpdated->paysId); // France => Pays.id = 62
	}

	public function testWarningDates()
	{
		$notice = self::createNotice();
		$notice->dateDebut = '1945';
		$writer = new UpdateEditeur();

		$editeur = \Editeur::model()->findByAttributes(['idref' => '026485842']);
		$editeur->dateDebut = '1954';
		$editeur->save(false);

		$writer->update($editeur, $notice);
		$this->assertSame(
			<<<CSV
				ID;opération;nom;type;debut;"ancien début";fin;"ancienne fin";pays;alertes-erreurs
				19;DIVERGENCE;Dalloz;coll.;1945;1954;;;FR;"les dates M et idref.fr diffèrent"

				CSV,
			$writer->getCsv()
		);
	}

	public function testMultiplePays()
	{
		$notice = self::createNotice();
		$notice->pays = 'ZZ';
		$writer = new UpdateEditeur();

		$editeur = \Editeur::model()->findByAttributes(['idref' => '026485842']);
		$editeur->save(false);

		$writer->update($editeur, $notice);
		$this->assertSame(
			<<<CSV
				ID;opération;nom;type;debut;"ancien début";fin;"ancienne fin";pays;alertes-erreurs
				19;DIVERGENCE;Dalloz;coll.;;;;;ZZ;"les pays M et idref.fr diffèrent (FR / ZZ)"

				CSV,
			$writer->getCsv()
		);
	}

	private static function createNotice(): Notice
	{
		$notice = new Notice();
		$notice->dateDebut = '';
		$notice->dateFin = '';
		$notice->pays = 'FR';
		$notice->type = Notice::TYPE_COLLECTIVITE;
		return $notice;
	}
}
