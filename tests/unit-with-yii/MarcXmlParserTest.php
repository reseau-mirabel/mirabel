<?php

use models\marcxml\GeneralInfo;
use models\marcxml\Issn;
use models\marcxml\OriginatingSource;
use models\marcxml\MarcTitre;
use models\marcxml\Parser;
use models\marcxml\Record;
use models\marcxml\Title;

class MarcXmlParserTest extends \Codeception\Test\Unit
{
	private $samples = [];

	protected function setUp(): void
	{
		parent::SetUp();
		$dataDir = dirname(__DIR__) . '/_data/sudoc-marcxml';
		$this->samples["090133803"] = file_get_contents("$dataDir/090133803.xml");
		$this->samples["039228460"] = file_get_contents("$dataDir/039228460.xml");
	}

	public function testParser1()
	{
		$parser = new Parser();
		$marc = new MarcTitre();

		$ppn = "090133803";
		$xml = $this->samples[$ppn];
		$parser->parse($xml, $marc);

		// simple reads
		$this->assertSame(["RESSI"], $marc->readValue('200$a'));
		$this->assertSame(["Abes", "ISSN"], $marc->readValue('801$b'));

		// Read by tag, ignoring indicators.
		$this->assertCount(1, $marc->readFields('200'));
		$this->assertCount(2, $marc->readFields('035'));
		$this->assertCount(3, $marc->readFields('856'));

		// general info
		$info = $marc->getGeneralInfo();
		//$this->assertEquals(GeneralInfo::PUBLICATIONTYPE_ONGOING, $info->publicationType);
		$this->assertEquals(GeneralInfo::RESOURCETYPE_ELECTRONIC, $info->resourceType);
		$this->assertEquals(GeneralInfo::STATUS_OK, $info->status);

		// leader
		$this->assertEquals("     cls0 22        450 ", $marc->getLeader());

		// controls & noholding
		$this->assertEquals($ppn, $marc->getControl(1));
		$isNoHolding = empty($marc->getControl(2));
		$this->assertFalse($isNoHolding);
		$this->assertEquals("http://www.sudoc.fr/$ppn", $marc->getControl(3));
		$this->assertNotEmpty($marc->getControl(7));
		$this->assertNull($marc->getControl(8));

		// issn
		$this->assertInstanceOf(Issn::class, $marc->getIssn());
		$this->assertEquals("1661-1802", $marc->getIssn()->issn);
		$this->assertEquals("1661-1802", $marc->getIssn()->issnl);
		$this->assertEquals("RESSI", $marc->getIssn()->title);

		// other identifiers (worldcat)
		$this->assertEquals(["OCoLC" => "61762295"], $marc->getOtherSystemsControlNumbers());
		$this->assertEquals("61762295", $marc->getOtherSystemsControlNumber("OCoLC"));

		// sources
		$this->assertCount(2, $marc->getDataField("801", [" "], "3"));
		$this->assertEquals("20160202", $marc->getDataField("801", ["1", " "], "3")[0]["c"]);
		$this->assertCount(2, $marc->getOriginatingSources(Record::ROLE_ISSUING));
		$this->assertInstanceOf(OriginatingSource::class, $marc->getOriginatingSource(Record::ROLE_ISSUING, "Abes"));
		$this->assertEquals("2016-02-02", $marc->getOriginatingSource(Record::ROLE_ISSUING, "Abes")->date);

		// title
		$this->assertInstanceOf(Title::class, $marc->getTitle());
		$this->assertEquals("RESSI", $marc->getTitle()->title);

		// URL
		$this->assertEquals(["http://www.ressi.ch"], $marc->getUrls());
	}


	public function testParser2()
	{
		$parser = new Parser();
		$marc = new MarcTitre();

		$ppn = "039228460";
		$xml = $this->samples[$ppn];
		$parser->parse($xml, $marc);

		// simple reads
		$this->assertCount(165, $marc->readValue('955##$i')); // 165 entries at datafield level
		$this->assertCount(80, array_filter($marc->readValue('955##$i'), 'is_array')); // 80 of them have repeated values

		// controls & noholding
		$this->assertEquals($ppn, $marc->getControl(1));
		$isNoHolding = empty($marc->getControl(2));
		$this->assertFalse($isNoHolding);

		// issn
		$this->assertInstanceOf(Issn::class, $marc->getIssn());
		$this->assertEquals("0035-2950", $marc->getIssn()->issn);

		// title
		$this->assertInstanceOf(Title::class, $marc->getTitle());
		$this->assertEquals("Revue française de science politique", $marc->getTitle()->title);

		// URL
		// L'URL est dans 999##$u (champ non-standard, avec un $5 indiquant une donnée locale).
		$this->assertEquals(["http://www.afsp.msh-paris.fr/publi/rfsp/rfsp.html"], $marc->getUrls());
	}
}
