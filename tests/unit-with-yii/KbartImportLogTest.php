<?php

use processes\kbart\logger\LogEvent;
use processes\kbart\logger\LogFamily;
use processes\kbart\Logger;

class KbartImportLogTest extends \Codeception\Test\Unit
{
	public function testGlobal()
	{
		$logger = new Logger();
		$logger->addGlobal(new LogEvent(LogFamily::info(), "Info 1"));
		$this->assertEquals('info', $logger->getGlobalLog()->getMaxlevel());

		$logger->addGlobal(new LogEvent(LogFamily::warning(), "Avertissement 1"));
		$logger->addGlobal(new LogEvent(LogFamily::info(), "Info 2"));
		$this->assertEquals('warning', $logger->getGlobalLog()->getMaxlevel());
		$this->assertCount(3, $logger->getGlobalLog()->getEvents());

		$logger->setFileContext("fichier1");
		$logger->setRowContext(3);
		$this->assertCount(0, $logger->getRowLog(3)->getEvents());
		$logger->addLocal(new LogEvent(LogFamily::info(), "Info locale"));
		$this->assertCount(3, $logger->getGlobalLog()->getEvents());
		$this->assertCount(1, $logger->getRowLog(3)->getEvents());
	}

	public function testLocal()
	{
		$logger = new Logger();
		$logger->setFileContext("fichier1");
		$logger->setRowContext(2);
		$logger->addLocal(new LogEvent(LogFamily::info(), "Info 1.1"));
		$logger->addLocal(new LogEvent(LogFamily::warning(), "Avertissement 1.2"));
		$this->assertEquals('', $logger->getGlobalLog()->getMaxlevel());
		$logger->setRowContext(3);
		$logger->addLocal(new LogEvent(LogFamily::info(), "Info 2.1"));
		$logger->addGlobal(new LogEvent(LogFamily::info(), "Info globale"));

		$this->assertEquals('info', $logger->getGlobalLog()->getMaxlevel());
		$this->assertEquals('', $logger->getRowLog(1)->getMaxlevel());
		$this->assertEquals('warning', $logger->getRowLog(2)->getMaxlevel());
		$this->assertEquals('info', $logger->getRowLog(3)->getMaxlevel());
		$this->assertCount(2, $logger->getRowLog(2)->getEvents());
		$this->assertCount(1, $logger->getRowLog(3)->getEvents());
	}
}
