<?php

class TitresPublishersTest extends \Codeception\Test\Unit
{
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	public function testChangeRelations()
	{
		$post = [
			'TitreEditeur' => array (
				6 => [
					'editeurId' => '0', // unchecked => editeurId=0 => removed
					'ancien' => '0', 'intellectuel' => '0', 'commercial' => '0',
				],
			),
			'TitreEditeurNew' => array (
				0 => [],
				469 => ['ancien' => '0', 'intellectuel' => '0', 'commercial' => '1',], // unchecked => no 'editeurId' => ignore
				23 => ['editeurId' => '23', 'ancien' => '0', 'intellectuel' => '1', 'commercial' => '0',],
			),
		];
		$interv = new Intervention();
		$interv->contenuJson = new \InterventionDetail();
		$process = new processes\titres\Publishers(6, $interv);
		$process->linkAndUnlink($post['TitreEditeur'], $post['TitreEditeurNew']);

		$this->assertEmpty($process->getError());
		$result = $interv->contenuJson->toArray();
		$this->assertCount(2, $result);
		unset($result[0]['before']['hdateModif']);
		$this->tester->assertSameArray(
			[
				'model' => 'TitreEditeur',
				'operation' => 'delete',
				'id' => [
					'titreId' => 6,
					'editeurId' => 6,
				],
				'before' => [
					'titreId' => 6,
					'editeurId' => 6,
					'ancien' => 0,
					'commercial' => 0,
					'intellectuel' => 0,
					'role' => null,
				],
				'msg' => "Détache l'éditeur « Esprit »",
			],
			$result[0]
		);
		$this->tester->assertSameArray(
			[
				'model' => 'TitreEditeur',
				'operation' => 'create',
				'after' => [
					'titreId' => 6,
					'editeurId' => 23,
					'ancien' => 0,
					'commercial' => 0,
					'intellectuel' => 1,
					'role' => null,
				],
				'msg' => "Attache l'éditeur « Presses de Sciences Po »",
			],
			$result[1],
			"Got: " . print_r($result[1], true)
		);
	}

	public function testUpdateExisting()
	{
		$post = [
			'TitreEditeur' => array (
				6 => [
					'editeurId' => '6',
					'ancien' => '0', 'intellectuel' => '1', 'commercial' => '1',
				],
			),
			'TitreEditeurNew' => array (
				0 => [],
			),
		];
		$interv = new Intervention();
		$interv->contenuJson = new \InterventionDetail();
		$process = new processes\titres\Publishers(6, $interv);
		$process->linkAndUnlink($post['TitreEditeur'], $post['TitreEditeurNew']);

		$this->assertEmpty($process->getError());
		$result = $interv->contenuJson->toArray();
		$this->assertCount(1, $result);
		unset($result[0]['before']['hdateModif']);
		unset($result[0]['after']['hdateModif']);
		$this->tester->assertSameArray(
			[
				'model' => 'TitreEditeur',
				'operation' => 'update',
				'id' => [
					'titreId' => 6,
					'editeurId' => 6,
				],
				'before' => [
					'commercial' => 0,
					'intellectuel' => 0,
				],
				'after' => [
					'commercial' => 1,
					'intellectuel' => 1,
				],
			],
			$result[0],
			"Got: " . print_r($result[0], true)
		);
	}

	public function testDetachFailsOnPolicy()
	{
		$post = [
			'TitreEditeur' => array (
				23 => [
					'editeurId' => '0',
					'ancien' => '0', 'intellectuel' => '0', 'commercial' => '0',
				],
			),
			'TitreEditeurNew' => [],
		];
		$interv = new Intervention();
		$interv->contenuJson = new \InterventionDetail();
		$process = new processes\titres\Publishers(98, $interv);
		$process->linkAndUnlink($post['TitreEditeur'], $post['TitreEditeurNew']);

		$this->assertStringContainsString("Une politique de publication existe pour l'éditeur", $process->getError());
	}
}
