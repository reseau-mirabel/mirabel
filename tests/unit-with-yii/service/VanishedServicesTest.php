<?php

namespace tests\unitwithyii\service;

use Ressource;
use processes\service\VanishedServices;

class VanishedServicesTest extends \Codeception\Test\Unit
{
	private const EMAIL_TO = 'default@nowhere.localhost';

	/**
	 * @dataProvider provideFind
	 */
	public function testFind(int $ressourceId, string $collectionIds, array $expected)
	{
		$finder = new VanishedServices();
		$finder->defaultTo = self::EMAIL_TO;
		$finder->setSince('2023-01-01 00:00:00');
		$finder->verbose = 2;
		$ressource = Ressource::model()->findByPk($ressourceId);

		$finder->find($ressource, $collectionIds);
		$emails = $finder->getEmails();
		codecept_debug($emails);
		$this->assertCount(count($expected), $emails);
		foreach ($expected as $to => $numTitres) {
			$this->assertArrayHasKey($to, $emails);
			$this->assertCount(1, $emails[$to]);
			$this->assertSame($numTitres, substr_count($emails[$to][0], 'http://localhost/titre/'));
		}
	}

	public function provideFind(): array
	{
		return [
			[3, "7", [self::EMAIL_TO => 11]], // mail to the default address about X Titre instances
			[3, "91", [self::EMAIL_TO => 4]],
			[3, "1", [self::EMAIL_TO => 1]],
			[3, "7,91", [self::EMAIL_TO => 15]],
			[3, "", [self::EMAIL_TO => 18]],
		];
	}
}
