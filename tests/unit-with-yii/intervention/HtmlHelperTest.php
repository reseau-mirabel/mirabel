<?php

namespace tests\unitwithyii\intervention;

use processes\intervention\HtmlHelper as HH;

class HtmlHelperTest extends \Codeception\Test\Unit
{
	public function testListInterventionsSuivi()
	{
		$interventions = HH::listInterventionsSuivi(1, 'accepté', 1356082800, 10);
		$this->assertCount(10, $interventions);
		$this->assertEquals("Modification d'accès en ligne, revue « Recueil Dalloz » / « Sign@l »", $interventions[0]->description);
	}

	/**
	 * @dataProvider provideLinks
	 */
	public function testLink(array $iattr, string $url)
	{
		$i = new \Intervention();
		$i->setAttributes($iattr, false);
		$renderer = new HH($i);

		// guest
		$linkGuest = $renderer->link(false);
		$this->assertStringNotContainsString("/intervention/{$i->id}", $linkGuest);
		$this->assertStringContainsString("href=\"$url", $linkGuest);

		// authentified
		$link = $renderer->link(true);
		$this->assertStringContainsString("/intervention/{$i->id}", $link);
		$this->assertStringNotContainsString("href=\"$url", $link);
	}

	public function provideLinks()
	{
		return [
			[
				['id' => 9, 'titreId' => 2520, 'revueId' => 6],
				'/revue/6',
			],
			[
				['id' => 9, 'titreId' => 2520, 'revueId' => 6, 'ressourceId' => 22],
				'/revue/6',
			],
			[
				['id' => 9, 'ressourceId' => 22, 'editeurId' => 331],
				'/ressource/22',
			],
			[
				['id' => 9, 'editeurId' => 331],
				'/editeur/331',
			],
		];
	}
}
