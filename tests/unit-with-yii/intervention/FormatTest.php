<?php

namespace tests\unitwithyii\intervention;

class FormatTest extends \Codeception\Test\Unit
{
	public function testFormatText()
	{
		$intervention = new \Intervention();
		$intervention->setAttributes(
			[
				'id' => 436254,
				'description' => "Modification de l'éditeur « Asia Institute »",
				'editeurId' => 4708,
				'contenuJson' => new \InterventionDetail([
					json_decode(<<<EOJSON
						{"model":"Editeur","operation":"update","id":4708,"before":{"liensJson":"[{\"src\":\"American Institute for Persian Art and Archaeology\",\"url\":\"\\/editeur\\/4707\"}]","role":null},"after":{"liensJson":"[{\"src\":\"American Institute for Persian Art and Archaeology\",\"url\":\"/editeur/4707\"}]","role":"commercial_publisher"}}
						EOJSON, true)
				]),
				'hdateProp' => 1694095032,
				'utilisateurIdProp' => 295,
				'suivi' => 0,
				'import' => 0,
				'action' => 'editeur-U',
			],
			false
		);

		$formatter = new \processes\intervention\Formatter();
		$formatter->showRestrictedInfo = true;
		$html = $formatter->format($intervention);
		$this->assertSame("<ul>\n<li><b>Rôle/type</b> : non défini &#8594; éditeur commercial.</li>\n</ul>", $html);

		$formatted = preg_replace('/^/m', '  ', strip_tags(html_entity_decode($html, ENT_COMPAT | ENT_HTML5)));
		$this->assertSame('Rôle/type : non défini → éditeur commercial.', trim($formatted));
	}
	/**
	 * @dataProvider provideContents
	 */
	public function testFormatContents($json, $action, $html)
	{
		$formatter = new \processes\intervention\Formatter();
		$formatter->showRestrictedInfo = true;
		$contents = json_decode($json, true);
		$this->assertEquals($html, $formatter->formatContents($contents['content'], $action));
	}

	public function provideContents()
	{
		return [
			[
				<<<EOJSON
				{"class":"InterventionDetail","content":[{"model":"Service","operation":"update","id":7330,"before":{"noFin":"9","numeroFin":"no 9","dateBarrFin":"2019-09"},"after":{"noFin":"10","numeroFin":"no 10","dateBarrFin":"2019-10"}}]}
				EOJSON,
				'service-U',
				<<<EOHTML
				<ul>
				<li><b>Date de fin</b> : &quot;2019-09&quot; &#8594; &quot;2019-10&quot;.</li>
				<li><b>Dernier numéro (numérique)</b> : &quot;9&quot; &#8594; &quot;10&quot;.</li>
				<li><b>Etat de collection (dernier numéro)</b> : &quot;no 9&quot; &#8594; &quot;no 10&quot;.</li>
				</ul>
				EOHTML,
			],
			[
				<<<EOJSON
				{"class":"InterventionDetail","content":[{"model":"Service","operation":"delete","id":"1321","before":{"id":"1321","ressourceId":"27","titreId":"2521","type":"Int\u00e9gral","acces":"Restreint","url":"https:\/\/esprit.presse.fr\/tous-les-numeros","alerteRssUrl":"","alerteMailUrl":"","derNumUrl":"","lacunaire":"0","selection":"0","volDebut":null,"noDebut":"1","numeroDebut":"no 1","volFin":null,"noFin":"84","numeroFin":"no 84","dateBarrDebut":"1932-10","dateBarrFin":"1939-09","dateBarrInfo":"","embargoInfo":null,"notes":null,"hdateModif":"1584694399","hdateCreation":"2013","hdateImport":null,"import":"0","statut":"normal"},"msg":"Suppression de l'acc\u00e8s en ligne"}]}
				EOJSON,
				'service-D',
				<<<EOHTML
				<ul>
				<li>Suppression de l'accès en ligne</li>
				</ul>
				EOHTML,
			],
			[
				<<<EOJSON
				{"class":"InterventionDetail","content":[{"model":"CategorieRevue","operation":"create","after":{"revueId":6,"categorieId":148,"modifPar":"2"}}]}
				EOJSON,
				'revue-I',
				<<<EOHTML
				<ul>
				<li>Ajout du thème <em>Actualité, presse</em>.</li>
				</ul>
				EOHTML,
			],
			[
				<<<EOJSON
				{"class":"InterventionDetail","content":[{"model":"CategorieRevue","operation":"delete","id":{"categorieId":"94","revueId":"29"},"before":{"categorieId":"94","revueId":"29","hdateModif":"2016-02-10 14:45:27","modifPar":"15"}}]}
				EOJSON,
				'revue-I',
				<<<EOHTML
				<ul>
				<li>Retrait d'un thème</li>
				</ul>
				EOHTML,
			],
			[
				<<<EOJSON
				{"class":"InterventionDetail","content":[{"model":"CategorieRevue","operation":"delete","id":{"categorieId":9,"revueId":29},"before":{"categorieId":9,"revueId":29,"hdateModif":"2016-02-10 14:45:27","modifPar":15}}]}
				EOJSON,
				'revue-I',
				<<<EOHTML
				<ul>
				<li>Retrait du thème <em>Patrimoine, musées, bibliothèques</em>.</li>
				</ul>
				EOHTML,
			],
			[
				<<<EOJSON
				{"class":"InterventionDetail","content":[{"model":"Titre","operation":"update","id":6,"before":{"liensJson":"[{\"src\":\"Twitter\",\"url\":\"https://twitter.com/RevueEsprit\"},{\"src\":\"Facebook\",\"url\":\"https://www.facebook.com/pages/Revue-Esprit/123674940994027\"}]"},"after":{"liensJson":"[{\"sourceId\":\"8\",\"src\":\"Twitter\",\"url\":\"https://twitter.com/RevueEsprit\"},{\"sourceId\":\"2\",\"src\":\"Facebook\",\"url\":\"https://www.facebook.com/pages/Revue-Esprit/123674940994027\"},{\"sourceId\":\"17\",\"src\":\"Wikipedia\",\"url\":\"https://fr.wikipedia.org/wiki/Esprit_%28revue%29\"}]"}}]}
				EOJSON,
				'',
				<<<EOHTML
				<ul>
				<li><b>Autres liens</b> : <ul>
				    <li><b>+</b> Wikipedia : <a href="https://fr.wikipedia.org/wiki/Esprit_%28revue%29">https://fr.wikipedia.org/wiki/Esprit_%28revue%29</a></li>
				    </ul></li>
				</ul>
				EOHTML,
			],
			[
				<<<EOJSON
				{"class":"InterventionDetail","content":[{"model":"Titre","operation":"update","id":6,"before":{"liensJson":"[{\"sourceId\":8,\"src\":\"Twitter\",\"url\":\"https://twitter.com/RevueEsprit\"},{\"sourceId\":2,\"src\":\"Facebook\",\"url\":\"https://www.facebook.com/pages/Revue-Esprit/123674940994027\"},{\"sourceId\":17,\"src\":\"Wikipedia (français)\",\"url\":\"https://fr.wikipedia.org/wiki/Esprit_(revue)\"},{\"sourceId\":13,\"src\":\"JournalBase\",\"url\":\"http://journalbase.cnrs.fr/index.php?op=9&id=2569\"},{\"sourceId\":10,\"src\":\"Youtube\",\"url\":\"https://www.youtube.com/channel/UCO1Iw3dR5aj3jA6tabsGPOw?sub_confirmation=1\"},{\"sourceId\":4,\"src\":\"Instagram\",\"url\":\"https://www.instagram.com/revue.esprit/\"},{\"sourceId\":19,\"src\":\"HAL\",\"url\":\"https://hal.archives-ouvertes.fr/search/index/q/*/journalId_i/97924%20OR%20161537\"},{\"sourceId\":27,\"src\":\"Cairn international\",\"url\":\"https://www.cairn-int.info/journal-esprit.htm\"}]"},"after":{"liensJson":"[{\"sourceId\":8,\"src\":\"Twitter\",\"url\":\"https://twitter.com/RevueEsprit\"},{\"sourceId\":2,\"src\":\"Facebook\",\"url\":\"https://www.facebook.com/pages/Revue-Esprit/123674940994027\"},{\"sourceId\":17,\"src\":\"Wikipedia (français)\",\"url\":\"https://fr.wikipedia.org/wiki/Esprit_(revue)\"},{\"sourceId\":13,\"src\":\"JournalBase\",\"url\":\"http://journalbase.cnrs.fr/index.php?op=9&id=2569\"},{\"sourceId\":10,\"src\":\"Youtube\",\"url\":\"https://www.youtube.com/channel/UCO1Iw3dR5aj3jA6tabsGPOw?sub_confirmation=1\"},{\"sourceId\":4,\"src\":\"Instagram\",\"url\":\"https://www.instagram.com/revue.esprit/\"},{\"sourceId\":19,\"src\":\"HAL\",\"url\":\"https://hal.archives-ouvertes.fr/search/index/q/*/journalId_i/97924\"},{\"sourceId\":27,\"src\":\"Cairn international\",\"url\":\"https://www.cairn-int.info/journal-esprit.htm\"}]"}}]}
				EOJSON,
				'',
				<<<EOHTML
				<ul>
				<li><b>Autres liens</b> : <ul>
				    <li>HAL : <a href="https://hal.archives-ouvertes.fr/search/index/q/*/journalId_i/97924%20OR%20161537">https://hal.archives-ouvertes.fr/search/index/q/*/journalId_i/97924%20OR%20161537</a> → <a href="https://hal.archives-ouvertes.fr/search/index/q/*/journalId_i/97924">https://hal.archives-ouvertes.fr/search/index/q/*/journalId_i/97924</a></li>
				    </ul></li>
				</ul>
				EOHTML,
			],
		];
	}
}
