<?php

use processes\intervention\ARTools;

class InterventionARToolsTest extends \Codeception\Test\Unit
{
	/**
	 * @dataProvider provideAR
	 */
	public function testFixFieldsTypes(string $tableName, array $attrBefore, array $attrExpected)
	{
		$attrAfter = ARTools::fixFieldsTypes($tableName, $attrBefore);
		$this->assertSame($attrExpected, $attrAfter);
	}

	public function provideAR(): array
	{
		return [
			[
				"Titre_Editeur",
				[
					'titreId' => '22',
					'editeurId' => '33',
					'ancien' => '1',
					'commercial' => '0',
					'intellectuel' => '',
					'role' => null,
					'hdateModif' => "2020-01-22",
				],
				[
					'titreId' => 22,
					'editeurId' => 33,
					'ancien' => 1,
					'commercial' => 0,
					'intellectuel' => 0,
					'role' => null,
					'hdateModif' => "2020-01-22",
				],
			],
		];
	}
}
