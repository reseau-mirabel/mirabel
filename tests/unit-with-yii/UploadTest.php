<?php

class UploadTest extends \Codeception\Test\Unit
{
	/**
	 * @dataProvider provideSizes
	 */
	public function testGetReadableFileSize(string $expected, int $size): void
	{
		$this->assertEquals($expected, Upload::getReadableFileSize($size));
	}

	public function provideSizes(): array
	{
		return [
			["200 B", 200],
			["50 kB", 51200],
			["50 kB", 51400],
			["1 MB", 1024*1024+1000],
			["1,5 MB", 1500*1024],
		];
	}
}
