<?php

use processes\kbart\work\Dedup;
use processes\kbart\read\RowData;

class KbartDedupTest extends \Codeception\Test\Unit
{
	public function testDedup()
	{
		$row1 = new RowData();
		$row1->assign([
			'position' => 1,
			'title' => "Premier titre",
			'accessType' => 'F',
		]);

		$row2 = new RowData();
		$row2->assign([
			'position' => 3,
			'title' => "Second titre",
			'accessType' => 'F',
		]);

		$row3 = new RowData();
		$row3->assign([
			'position' => 4,
			'title' => "Premier titre",
			'accessType' => 'R',
		]);

		$rows = [$row1, $row2, $row3];

		$dedup = new Dedup();
		$this->assertEquals(
			[[1,4]],
			$dedup->find($rows, ["title"])
		);
		$this->assertEquals(
			[],
			$dedup->find($rows, ["title", "accessType"])
		);
	}
}
