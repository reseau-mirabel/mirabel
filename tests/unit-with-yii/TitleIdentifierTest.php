<?php

use processes\kbart\identify\Batch;
use processes\kbart\identify\Single;
use processes\kbart\Logger;
use processes\kbart\read\RowData;

class TitleIdentifierTest extends \Codeception\Test\Unit
{
	public function testIdentifyByIssne()
	{
		$rowData = new RowData();
		$rowData->position = 2;
		$rowData->assign([
			'title' => "Actualité Juridique Droit Administratif",
			'issne' => '1760-7558',
		]);
		$logger = new Logger();
		$logger->setRowContext($rowData->position);
		$identifier = new Single($logger);
		$identifier->setRessource(\Ressource::model()->findByPk(3));
		$title = $identifier->identify($rowData);
		$this->assertInstanceOf('Titre', $title);
	}

	public function testIdentifyAll()
	{
		$rowData1 = new RowData();
		$rowData1->position = 2;
		$rowData1->assign([
			'title' => "Absent des tests",
			'issn' => '2747-9099',
			'id' => 'id_inconnu',
		]);
		$rowData2 = new RowData();
		$rowData2->position = 2;
		$rowData2->assign([
			'title' => "A Contrario",
			'id' => 'revue-a-contrario',
		]);

		$identifier = new Batch(\Ressource::model()->findByPk(3));
		$titles = $identifier->identifyAll([$rowData1, $rowData2]);
		$this->assertCount(1, $titles);
		$this->assertInstanceOf('Titre', $titles[$rowData2->position]);
		$this->assertEquals("A contrario. Revue interdisciplinaire de sciences sociales", $titles[$rowData2->position]->titre);
	}

	public function testIdentifyAllByIssne()
	{
		$rowData = new RowData();
		$rowData->position = 2;
		$rowData->assign([
			'title' => "Actualité Juridique Droit Administratif",
			'issne' => '1760-7558',
		]);

		$identifier = new Batch(\Ressource::model()->findByPk(3));
		$titles = $identifier->identifyAll([$rowData]);
		$this->assertCount(1, $titles);
		$this->assertInstanceOf('Titre', $titles[$rowData->position]);
		$this->assertEquals("AJDA", $titles[$rowData->position]->sigle);
	}

	public function testIdentifyAllByPublisher()
	{
		$rowData1 = new RowData();
		$rowData1->position = 1;
		$rowData1->assign([
			'title' => "A contrario", // will not match
			'publisherName' => 'Editions Antipodes',
		]);
		$rowData2 = new RowData();
		$rowData2->position = 2;
		$rowData2->assign([
			'title' => "Actualité Juridique Droit Administratif",
			'publisherName' => 'Dalloz',
		]);
		$data = [$rowData1, $rowData2];
		$ressource = \Ressource::model()->findByPk(3);

		$identifier0 = new Batch($ressource);
		$this->assertCount(0, $identifier0->identifyAll($data));

		$ressource->importIdentifications = 'editeur';
		$identifier = new Batch($ressource);
		$titles = $identifier->identifyAll($data);
		$this->assertCount(1, $titles);
		$this->assertInstanceOf('Titre', $titles[$rowData2->position]);
		$this->assertEquals("AJDA", $titles[$rowData2->position]->sigle);
	}
}
