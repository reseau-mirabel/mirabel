<?php

namespace tests\unitwithyii\linkimporters;

use commands\models\linkimport\Hal;
use components\email\SentMessage;
use Yii;

class HalTest extends \Codeception\Test\Unit
{
	public function testHal()
	{

		// Email capture
		$transport = new \components\email\TestTransport();
		\components\email\Mailer::getMailer($transport);

		$importer = new Hal([
			'email' => 'contact@silecs.info',
			'verbose' => 0,
		]);
		$importer->setCurl($this->createCurlStub());
		$importer->import();

		self::assertSame(1, $transport->getSentCount());
		$message = new SentMessage($transport->getLastSentMessage());
		self::assertStringContainsString("Mises à jour HAL", $message->getSubject());
		self::assertSame(<<<EOTXT
			Modifications 20

			## Import des liens (HAL)

			20;20;"Recueil Dalloz";1298-728X;https://hal.science/search/index/q/*/journalId_i/107760;"Lien modifié";OK;http://localhost/revue/20
			1;1;"Actualité Juridique Droit Administratif";0001-7728;https://hal.science/search/index/q/*/journalId_i/28533;"Lien modifié";OK;http://localhost/revue/1
			6;6;Esprit;0014-0759;https://hal.science/search/index/q/*/journalId_i/97924;"Lien modifié";OK;http://localhost/revue/6
			718;716;"Annales : Histoire, Sciences Sociales";0395-2649;https://hal.science/search/index/q/*/journalId_i/102624%20OR%20145085%20OR%20160497;"Lien modifié";OK;http://localhost/revue/718
			98;98;"Vingtième siècle, revue d'histoire";0294-1759;https://hal.science/search/index/q/*/journalId_i/25293;"Lien modifié";OK;http://localhost/revue/98
			738;736;"Langue française";0023-8368;https://hal.science/search/index/q/*/journalId_i/39823;"Lien modifié";OK;http://localhost/revue/738
			28;28;"Actes de la recherche en sciences sociales";0335-5322;https://hal.science/search/index/q/*/journalId_i/22968;"Lien modifié";OK;http://localhost/revue/28
			29;29;"Annales historiques de la Révolution française";0003-4436;https://hal.science/search/index/q/*/journalId_i/2991;"Lien modifié";OK;http://localhost/revue/29
			2;2;"Afrique Contemporaine";0002-0478;https://hal.science/search/index/q/*/journalId_i/23060;"Lien modifié";OK;http://localhost/revue/2
			1060;1105;"Cahiers d'histoire. Revue d'histoire critique";1271-6669;https://hal.science/search/index/q/*/journalId_i/41568%20OR%20124183;"Lien modifié";OK;http://localhost/revue/1060
			3;3;"Alternatives économiques";0247-3739;https://hal.science/search/index/q/*/journalId_i/103789%20OR%20318460;"Lien modifié";OK;http://localhost/revue/3
			20;1399;"Recueil Dalloz Sirey de doctrine de jurisprudence et de législation";0034-1835;https://hal.science/search/index/q/*/journalId_i/144759;"Lien modifié";OK;http://localhost/revue/20
			444;444;"A contrario. Revue interdisciplinaire de sciences sociales";1660-7880;https://hal.science/search/index/q/*/journalId_i/26287;"Lien modifié";OK;http://localhost/revue/444
			1184;1248;"Cahiers de l'Orient";0767-6468;https://hal.science/search/index/q/*/journalId_i/127270%20OR%20145931;"Lien modifié";OK;http://localhost/revue/1184
			114;114;"American Political Science Review";0003-0554;https://hal.science/search/index/q/*/journalId_i/2924;"Lien modifié";OK;http://localhost/revue/114
			1174;1229;"American journal of political science";0092-5853;https://hal.science/search/index/q/*/journalId_i/2874%20OR%20161543;"Lien modifié";OK;http://localhost/revue/1174
			366;366;S!lence;0756-2640;https://hal.science/search/index/q/*/journalId_i/106862%20OR%20137025;"Lien modifié";OK;http://localhost/revue/366
			1060;2268;"Cahiers d'histoire de l'Institut de recherches marxistes";0246-9731;https://hal.science/search/index/q/*/journalId_i/110664;"Lien modifié";OK;http://localhost/revue/1060
			20;1400;"Recueil Dalloz de doctrine de jurisprudence et de législation";1624-0553;https://hal.science/search/index/q/*/journalId_i/150637;"Lien modifié";OK;http://localhost/revue/20
			103;103;"Actualité Législative Dalloz";0753-874X;https://hal.science/search/index/q/*/journalId_i/414649;"Lien modifié";OK;http://localhost/revue/103

			EOTXT,
			$message->getBody()
		);

		// Applying a second time is silent
		$transport->reset();
		$importerBis = new Hal([
			'email' => 'contact@silecs.info',
			'verbose' => 0
		]);
		$importerBis->setCurl($this->createCurlStub());
		$importerBis->import();
		self::assertSame(0, $transport->getSentCount());
	}

	private function createCurlStub(): \components\Curl
	{
		$curl = $this->createStub(\components\Curl::class);
		$curl->method('get')
			->will($this->returnSelf());
		$curl->method('getContent')
			->will($this->onConsecutiveCalls(
				// eissn_s (electronique) for findJournalsByIssn()
				// curl "https://api.archives-ouvertes.fr/ref/journal/?rows=200&fl=docid,issn_s,eissn_s&q=eissn_s:(%221955-2564%22%20OR%20%221953-8146%22%20OR%20%221952-403X%22%20OR%20%221957-7982%22%20OR%20%221950-6678%22%20OR%20%222102-5916%22%20OR%20%221920-7840%22%20OR%20%221662-8667%22%20OR%20%221782-138X%22%20OR%20%222111-4579%22%20OR%20%221963-1707%22%20OR%20%221953-8146%22%20OR%20%222420-0018%22%20OR%20%221540-5907%22%20OR%20%222552-0016%22%20OR%20%221537-5943%22%20OR%20%222419-7157%22%20OR%20%222513-6216%22%20OR%20%221760-7558%22%20OR%20%221760-785X%22%20OR%20%222420-465X%22%20OR%20%222437-2315%22%20OR%20%222420-4641%22%20OR%20%222419-8595%22%20OR%20%221540-5907%22)" > tests/_data/links/hal_electronique.json
				file_get_contents(codecept_data_dir("/links/hal_electronique.json")),
				// issn_s (papier) for findJournalsByIssn()
				// curl "https://api.archives-ouvertes.fr/ref/journal/?rows=200&fl=docid,issn_s,eissn_s&q=issn_s:(%220001-7728%22%20OR%20%220002-0478%22%20OR%20%220247-3739%22%20OR%20%220014-0759%22%20OR%20%221298-728X%22%20OR%20%220335-5322%22%20OR%20%220003-4436%22%20OR%20%220294-1759%22%20OR%20%220753-874X%22%20OR%20%220003-0554%22%20OR%20%220756-2640%22%20OR%20%221660-7880%22%20OR%20%220003-441X%22%20OR%20%220023-8368%22%20OR%20%221243-258X%22%20OR%20%221243-2571%22%20OR%20%221243-2563%22%20OR%20%221271-6669%22%20OR%20%220092-5853%22%20OR%20%220767-6468%22%20OR%20%221718-9578%22%20OR%20%220034-1835%22%20OR%20%221624-0553%22%20OR%20%220026-3397%22%20OR%20%220246-9731%22%20OR%20%220184-7805%22%20OR%20%220184-7791%22%20OR%20%221145-7376%22%20OR%20%221145-7368%22%20OR%20%220221-5047%22%20OR%20%220020-2363%22%20OR%20%220755-2424%22%20OR%20%220395-2649%22)" > tests/_data/links/hal_papier.json
				file_get_contents(codecept_data_dir("/links/hal_papier.json")),
				// notices for filterJournalsByNotices()
				// curl "https://api.archives-ouvertes.fr/search/?wt=json&rows=0&facet=true&facet.field=journalId_i&facet.mincount=1&facet.limit=100&q=journalId_i%3A%28161543%20OR%202874%20OR%202991%20OR%202924%20OR%2026287%20OR%2022968%20OR%2023060%20OR%2039823%20OR%2097924%20OR%20107760%20OR%20127270%20OR%2041568%20OR%2025293%20OR%20124183%20OR%2028533%20OR%20110664%20OR%2017107%20OR%20414649%20OR%20106862%20OR%20137025%20OR%20145931%20OR%20103789%20OR%20318460%20OR%20144759%20OR%20150637%20OR%20160497%20OR%20145085%20OR%20102624%20OR%20544921%29" > tests/_data/links/hal_notices.json
				file_get_contents(codecept_data_dir("/links/hal_notices.json")),
			));
		return $curl;
	}
}


