<?php

namespace tests\unitwithyii\linkimporters;

use commands\models\linkimport\Scopus;
use components\email\SentMessage;

class ScopusTest extends \Codeception\Test\Unit
{
	public function testScopus()
	{
		// Email capture
		$transport = new \components\email\TestTransport();
		\components\email\Mailer::getMailer($transport);

		$importer = new Scopus([
			'email' => 'contact@silecs.info',
			'verbose' => 0,
		]);
		$importer->xlsx = codecept_data_dir('/links/scopus.xlsx');
		$importer->import();

		$message = new SentMessage($transport->getLastSentMessage());
		self::assertStringContainsString("Mises à jour Scopus", $message->getSubject());
		self::assertSame(<<<EOTXT
			Créations 3

			## Import des liens (Scopus)

			444;444;"A contrario. Revue interdisciplinaire de sciences sociales";5700161051;https://www.scopus.com/sourceid/5700161051;"Lien ajouté";OK;http://localhost/revue/444
			1174;1229;"American journal of political science";15555;https://www.scopus.com/sourceid/15555;"Lien ajouté";OK;http://localhost/revue/1174
			1174;1564;"Midwest Journal of Political Science";15555;https://www.scopus.com/sourceid/15555;"Lien ajouté";OK;http://localhost/revue/1174

			EOTXT,
			$message->getBody()
		);
		$transport->reset();
	}
}
