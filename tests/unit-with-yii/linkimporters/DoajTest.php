<?php

namespace tests\unitwithyii\linkimporters;

use commands\models\linkimport\Doaj;
use components\email\SentMessage;

class DoajTest extends \Codeception\Test\Unit
{
	public function testDoaj()
	{
		$localFile = self::copyMockupFile();

		// Email capture
		$transport = new \components\email\TestTransport();
		\components\email\Mailer::getMailer($transport);

		$importer = new Doaj([
			'email' => 'contact@silecs.info',
			'obsoleteSince' => '5 minute ago',
			'verbose' => 0,
		]);
		$importer->import();

		self::assertSame(1, $transport->getSentCount());
		$message = new SentMessage($transport->getLastSentMessage());
		self::assertStringContainsString("Mises à jour DOAJ", $message->getSubject());
		self::assertSame(<<<EOTXT
			Créations 1

			## Import des liens (DOAJ)

			718;1084;"Annales d'histoire sociale (1939)";1678-2690;https://doaj.org/toc/1678-2690;"Lien ajouté";OK;http://localhost/revue/718

			EOTXT,
			$message->getBody()
		);

		// Applying a second time is silent
		$transport->reset();
		$importerBis = new Doaj([
			'email' => 'contact@silecs.info',
			'verbose' => 0
		]);
		$importerBis->import();
		self::assertSame(0, $transport->getSentCount());

		// Remove the mockup CSV
		unlink($localFile);
	}

	/**
	 * Copy data mockup into the file this import will read.
	 *
	 * @return string Path to the destination file
	 */
	private static function copyMockupFile(): string
	{
		$mockupCsv = codecept_data_dir('/links/doaj.csv');
		$dir = DATA_DIR . '/filestore/liens_DOAJ';
		if (!is_dir($dir)) {
			mkdir($dir, 0774, true);
		}
		$localFile = "$dir/toimport";
		if (!file_put_contents($localFile, file_get_contents($mockupCsv))) {
			throw new \Exception("Mock CSV failure");
		}
		return $localFile;
	}
}
