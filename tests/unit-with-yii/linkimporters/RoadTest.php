<?php

namespace tests\unitwithyii\linkimporters;

use commands\models\linkimport\Road;
use components\email\SentMessage;

class RoadTest extends \Codeception\Test\Unit
{
	public function testRoad()
	{
		$localFile = self::copyMockupFile();

		// Email capture
		$transport = new \components\email\TestTransport();
		\components\email\Mailer::getMailer($transport);

		$importer = new Road([
			'email' => 'contact@silecs.info',
			'obsoleteSince' => '5 minute ago',
			'verbose' => 0,
		]);
		$importer->import();

		self::assertSame(1, $transport->getSentCount());
		$message = new SentMessage($transport->getLastSentMessage());
		self::assertStringContainsString("Mises à jour ROAD", $message->getSubject());
		self::assertSame(<<<EOTXT
			Créations 1

			## Import des liens (ROAD)

			1265;1352;"Biscuit chinois : littérature pop";1920-7840;http://road.issn.org/issn/1920-7840;"Lien ajouté";OK;http://localhost/revue/1265

			EOTXT,
			$message->getBody()
		);

		// Applying a second time is silent
		$transport->reset();
		$importerBis = new Road([
			'email' => 'contact@silecs.info',
			'verbose' => 0
		]);
		$importerBis->import();
		self::assertSame(0, $transport->getSentCount());

		// Remove the mockup CSV
		unlink($localFile);
	}

	/**
	 * Copy data mockup into the file this import will read.
	 *
	 * @return string Path to the destination file
	 */
	private static function copyMockupFile(): string
	{
		$mockupXml = codecept_data_dir('/links/road.xml');
		// The following path would break if anything changes in FileStore or Road...
		$dir = DATA_DIR . '/filestore/liens_ROAD';
		if (!is_dir($dir)) {
			mkdir($dir, 0774, true);
		}
		$localFile = "$dir/ROAD.xml.gz";
		if (!file_put_contents($localFile, gzencode(file_get_contents($mockupXml)))) {
			throw new \Exception("Mock XML failure");
		}
		return $localFile;
	}
}
