<?php

namespace tests\unitwithyii\linkimporters;

use commands\models\linkimport\Ezb;
use components\email\SentMessage;

class EzbTest extends \Codeception\Test\Unit
{
	public function testEzb()
	{
		// Prepare a mockup CSV.
		$mockupCsv = codecept_data_dir('/links/ezb.csv');

		// Email capture
		$transport = new \components\email\TestTransport();
		\components\email\Mailer::getMailer($transport);

		$importer = new Ezb([
			'email' => 'contact@silecs.info',
			'obsoleteSince' => '5 minute ago',
			'verbose' => 0,
		]);
		$importer->csvFile = $mockupCsv;
		$importer->import();

		self::assertSame(1, $transport->getSentCount());
		$message = new SentMessage($transport->getLastSentMessage());
		self::assertStringContainsString("Mises à jour EZB", $message->getSubject());
		self::assertSame(<<<EOTXT
			Créations 2

			## Import des liens (EZB)

			2;2;"Afrique Contemporaine";0002-0478,1782-138X;https://ezb.ur.de/searchres.phtml?lang=en&jq_type1=IS&jq_term1=0002-0478&jq_bool2=OR&jq_type2=IS&jq_term2=1782-138X;"Lien ajouté";OK;http://localhost/revue/2
			1265;1352;"Biscuit chinois : littérature pop";1718-9578,1920-7840;https://ezb.ur.de/searchres.phtml?lang=en&jq_type1=IS&jq_term1=1718-9578&jq_bool2=OR&jq_type2=IS&jq_term2=1920-7840;"Lien ajouté";OK;http://localhost/revue/1265

			EOTXT,
			$message->getBody()
		);

		// Applying a second time is silent
		$transport->reset();
		$importerBis = new Ezb([
			'email' => 'contact@silecs.info',
			'verbose' => 0
		]);
		$importerBis->csvFile = $mockupCsv;
		$importerBis->import();
		self::assertSame(0, $transport->getSentCount());
	}
}
