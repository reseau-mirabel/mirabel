<?php

namespace tests\unitwithyii\linkimporters;

use commands\models\linkimport\Openalex;
use components\email\SentMessage;

class OpenalexTest extends \Codeception\Test\Unit
{
	public function testOpenalex()
	{
		// Email capture
		$transport = new \components\email\TestTransport();
		\components\email\Mailer::getMailer($transport);

		$importer = new Openalex([
			'email' => 'contact@silecs.info',
			'obsoleteSince' => '5 minute ago',
			'verbose' => 0,
		]);
		$importer->setFetcher(function(array $issns): string {
			// Mockup JSON response for the first response, then an error for the other batches.
			if (in_array('0001-7728', $issns)) {
				// Un fichier JSON légitime, modifié pour introduire un doublon d'identifiants sur le premier titre.
				return file_get_contents(codecept_data_dir('/links/openalex-6.json'));
			}
			throw new \Exception("pinpon pinpon");
		});
		$importer->import();

		self::assertSame(1, $transport->getSentCount());
		$message = new SentMessage($transport->getLastSentMessage());
		self::assertStringContainsString("Mises à jour OpenAlex", $message->getSubject());
		self::assertSame(<<<EOTXT
			ERREURS :
			pinpon pinpon

			Créations 6

			## Import des liens (OpenAlex)

			2;2;"Afrique Contemporaine";S127917940,S4210172030;https://explore.openalex.org/works?sort=cited_by_count:desc&filter=primary_location.source.id:S127917940%7CS4210172030;"Lien ajouté";OK;http://localhost/revue/2
			6;6;Esprit;S79731123;https://explore.openalex.org/sources/S79731123;"Lien ajouté";OK;http://localhost/revue/6
			29;29;"Annales historiques de la Révolution française";S198035802;https://explore.openalex.org/sources/S198035802;"Lien ajouté";OK;http://localhost/revue/29
			114;114;"American Political Science Review";S176007004;https://explore.openalex.org/sources/S176007004;"Lien ajouté";OK;http://localhost/revue/114
			718;725;"Annales d'histoire économique et sociale";S4210172030;https://explore.openalex.org/sources/S4210172030;"Lien ajouté";OK;http://localhost/revue/718
			738;736;"Langue française";S169927069;https://explore.openalex.org/sources/S169927069;"Lien ajouté";OK;http://localhost/revue/738

			EOTXT,
			$message->getBody()
		);
	}
}
