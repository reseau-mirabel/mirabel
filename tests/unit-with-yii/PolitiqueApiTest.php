<?php

use processes\politique\ApiServer;

class PolitiqueApiTest extends \Codeception\Test\Unit
{
	public function testChanges()
	{
		$api = new ApiServer([], []);
		$this->assertEquals(
			[
				'since' => '',
				'new' => [
					[
						'publication_id' => 98,
						'policy_id' => 2,
						'publisher_id' => 23,
						'last_update' => '2022-01-10T11:39:38+01:00',
					],
				],
				'deleted' => [],
			],
			$api->actionChanges(0)
		);
	}

	public function testMissingPolicy()
	{
		$api = new ApiServer([], []);
		$this->expectException('CHttpException');
		$api->actionPolicy(1); // not published
	}

	public function testMissingPublication()
	{
		$api = new ApiServer([], []);
		$this->expectException('CHttpException');
		$api->actionPublication(9999999);
	}

	public function testMissingPublisher()
	{
		$api = new ApiServer([], []);
		$this->expectException('CHttpException');
		$api->actionPublisher(9999999);
	}

	public function testPolicy(): void
	{
		$id = 2;
		$api = new ApiServer([$id], []);
		$this->assertEquals(
			[
				'id' => $id,
				'last_update' => 1641811178,
				'content' => (object) [
					'permitted_oa' => [],
					'urls' => [
						(object) [
							'description' => 'vingtième',
							'url' => 'https://vingtieme.tralala',
						],
					],
					'internal_moniker' => 'politique de Presses de Sciences Po',
				],
			],
			$api->actionPolicy($id)
		);
	}

	/**
	 * @dataProvider providePublications
	 */
	public function testPublication(array $expected, int $id): void
	{
		$api = new ApiServer([], []);
		$this->assertEquals($expected, $api->actionPublication($id));
	}

	public function providePublications(): array
	{
		return [
			[
				[
					'id' => 98,
					'group_id' => 98,
					'successor_publication_id' => 0,
					'fulltitle' => "Vingtième siècle, revue d'histoire",
					'title' => [
						[
							'title' => "Vingtième siècle, revue d'histoire",
							'language' => '',
						],
					],
					'prefix' => '',
					'shorttitle' => '',
					'start_date' => '1984',
					'end_date' => '',
					'url' => 'http://www.pressesdesciencespo.fr/revues/vingtimesiclerevuedhist/',
					'external_urls' => [
						[
							'source' => 'Wikipedia',
							'url' => "https://fr.wikipedia.org/wiki/Vingti%C3%A8me_si%C3%A8cle_:_Revue_d'histoire",
						],
						[
							'source' => 'JournalBase',
							'url' => 'http://journalbase.cnrs.fr/index.php?op=9&id=9255',
						],
						[
							'source' => 'HAL',
							'url' => 'https://hal.archives-ouvertes.fr/search/index/q/*/journalId_i/25293',
						],
					],
					'languages' => ['fre'],
					'identifiers' => [
						[
							'medium' => 'print',
							'issn' => '0294-1759',
							'issnl' => '0294-1759',
							'sudoc_ppn' => '001020196',
							'worldcat_ocn' => '224545103',
						],
						[
							'medium' => 'online',
							'issn' => '1950-6678',
							'issnl' => '0294-1759',
							'sudoc_ppn' => '099236346',
							'worldcat_ocn' => '488516303',
						],
					],
					'publishers' => [
						[
							'publisher_id' => 23,
							'former' => false,
							'role' => '',
						]
					],
					'last_update' => '2019-07-04T12:05:01+02:00',
					'last_check' => '2019-03-15T09:00:06+01:00',
				],
				98,
			],
		];
	}

	/**
	 * @dataProvider providePublishers
	 */
	public function testPublisher(array $expected, int $id): void
	{
		$api = new ApiServer([], []);
		$this->assertEquals($expected, $api->actionPublisher($id));
	}

	public function providePublishers(): array
	{
		return [
			[
				[
					'id' => 23,
					'fullname' => 'Presses de Sciences Po',
					'name' => [
						[
							'name' => 'Presses de Sciences Po',
							'language' => '',
						],
					],
					'prefix' => '',
					'shortname' => '',
					'description' => "Maison d'édition universitaire dépendant de la Fondation nationale des sciences politiques (FNSP)",
					'location' => [
						'country2' => 'FR',
						'country3' => 'FRA',
						'place' => 'Paris',
					],
					'url' => 'http://www.pressesdesciencespo.fr/',
					'external_urls' => [
						[
							'source' => 'Facebook',
							'url' => 'https://fr-fr.facebook.com/Presses.de.Sciences.po',
						],
						[
							'source' => 'Twitter',
							'url' => 'https://x.com/EditionsScpo',
						],
						[
							'source' => 'Contacts',
							'url' => 'http://www.pressesdesciencespo.fr/fr/contacts/',
						],
					],
					'identifiers' => [],
					'publications' => [
						[
							'publication_id' => 98,
							'former' => false,
							'role' => '',
						],
					],
					'last_update' => '2017-04-21T14:33:54+02:00',
					'last_check' => '2017-02-24T14:35:23+01:00',
				],
				23,
			],
			[
				[
					'id' => 2724,
					'fullname' => 'Institut Maurice Thorez',
					'name' => [
						[
							'name' => 'Institut Maurice Thorez',
							'language' => '',
						],
					],
					'prefix' => '',
					'shortname' => '',
					'description' => '',
					'location' => [
						'country2' => 'FR',
						'country3' => 'FRA',
						'place' => 'Paris',
					],
					'url' => '',
					'external_urls' => [],
					'identifiers' => [],
					'publications' => [
						[
							'publication_id' => 4629,
							'former' => true,
							'role' => '',
						],
						[
							'publication_id' => 4630,
							'former' => true,
							'role' => '',
						],
					],
					'last_update' => '2016-11-08T13:11:00+01:00',
					'last_check' => '',
				],
				2724,
			]
		];
	}

	public function testRunMissingId()
	{
		$this->expectExceptionObject(new CHttpException(400, "No ID was given in the path."));
		$api = new ApiServer([], []);
		$api->run("policy");
	}

	public function testRunWrongId()
	{
		$this->expectExceptionObject(new CHttpException(400, "The ID must be a positive integer pointing to an existing record."));
		$api = new ApiServer(["ARSS"], []);
		$api->run("publisher");
	}

	public function testRunWrongDate()
	{
		$this->expectExceptionObject(new CHttpException(400, "Wrong 'since' parameter. ISO8601 or UNIX timestamp expected"));
		$api = new ApiServer([], ['since' => '2022-01-11T13']);
		$api->run("changes");
	}

	public function testRun()
	{
		$id = 6;
		$api = new ApiServer([$id], ['since' => '2022-01-11T13:11:00 01:00']);

		$this->assertEquals($id, $api->run("publication")['id']);
		$this->assertEquals($id, $api->run("publisher")['id']);

		$this->assertEquals(
			['since' => '2022-01-11T13:11:00+01:00', 'new' => [], 'deleted' => []],
			$api->run("changes")
		);
	}

	public function testRunWithTimestamp()
	{
		$api = new ApiServer([6], ['since' => 1641903060]);
		$this->assertEquals(
			['since' => '2022-01-11T13:11:00+01:00', 'new' => [], 'deleted' => []],
			$api->run("changes")
		);
		unset($_GET['since']);

		$this->expectException('CHttpException');
		$api->run("noway");
	}
}
