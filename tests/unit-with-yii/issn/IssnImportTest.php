<?php

namespace tests\unitwithyii\issn;

use Issn;
use processes\issn\Importpe;
use Yii;

class IssnImportTest extends \Codeception\Test\Unit
{
	public function testProcess()
	{
		$t = Yii::app()->db->beginTransaction();

		$import = new Importpe(false);
		$import->querySudocApi = false;
		$import->loadFile(codecept_data_dir('/issn-import.csv'));

		$this->assertCount(0, $import->listExisting());
		$this->assertCount(5, $import->listToInsert()); // including 3 future errors
		$this->assertCount(22, $import->listUnknown());

		$issne = '2822-8499';
		$this->assertNull(Issn::model()->findByAttributes(['issn' => $issne]));
		$import->insertMissing();

		$this->assertCount(3, $import->listErrors(), json_encode($import->listErrors(), JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
		$this->assertCount(2, $import->listInserts());
		$this->assertSame($issne, $import->listInserts()[0]['issne']);

		$issnRecord = Issn::model()->findByAttributes(['issn' => $issne]);
		$this->assertInstanceof('Issn', $issnRecord);
		$this->assertSame(Issn::SUPPORT_ELECTRONIQUE, $issnRecord->support);

		$this->assertCount(2, $import->listExisting(), "before insert: 0, after insert: 2");
		$this->assertCount(3, $import->listToInsert(), "after inserts, only errors are kept");

		$t->rollback();
	}
}
