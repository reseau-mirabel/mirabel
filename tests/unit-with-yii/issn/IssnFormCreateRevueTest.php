<?php

namespace tests\unitwithyii\issn;

use Issn;
use IssnForm;
use models\sudoc\ApiClient;
use models\sudoc\Notice;

class IssnFormCreateRevueTest extends \Codeception\Test\Unit
{
	public function testMultipleIssn()
	{
		$api = $this->createStubApi([
			"2321-4600" => [
				'ppn' => "090133803",
				'sudocNoHolding' => false,
				'titre' => "RESSI",
				'titreIssn' => "RESSI",
				'dateDebut' => "",
				'dateFin' => "",
				'issn' => "2198-7823",
				'issnl' => "2095-8293",
				'statut' => Issn::STATUT_VALIDE,
				'support' => Issn::SUPPORT_ELECTRONIQUE,
			],
			"2321-1407" => [
				'ppn' => "090133804",
				'sudocNoHolding' => true,
			],
		]);

		$issn = new IssnForm($api);
		$issn->setAttributes([
			'issn' => "par exemple 2321-4600 et '2321-1407' dans le texte",
		]);
		$this->assertTrue($issn->validate());
		$notices = $issn->getSudocNotices();
		$this->assertCount(2, $notices);
	}

	/**
	 * @param array<string, string[]> $content
	 */
	private function createStubApi(array $content): ApiClient
	{
		$notices = [];
		foreach ($content as $issn => $attributes) {
			$notice = new Notice();
			foreach ($attributes as $k => $v) {
				$notice->$k = $v;
			}
			$notices[$issn] = $notice;
		}

		return $this->make(
			ApiClient::class,
			[
				'getNoticeByIssn' => function (string $issn) use ($notices) {
					return $notices[$issn] ?? null;
				},
			]
		);
	}
}
