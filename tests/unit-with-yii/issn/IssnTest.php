<?php

namespace tests\unitwithyii\issn;

use models\sudoc\ApiClient;

class IssnTest extends \Codeception\Test\Unit
{
	public function testFillBySudoc()
	{
		// create an Issn record
		$issn = new \Issn();
		$issn->setAttributes(
			[
				'titreId' => 1564,
				'issn' => "1661-1802",
				'dateDebut' => '2010',
				'statut' => \Issn::STATUT_ENCOURS,
				'sudocNoHolding' => true,
			],
			false
		);
		$this->assertEquals("2010", $issn->dateDebut);

		// update it with a stub Sudoc API
		$api = $this->createStubApi("090133803", [
				'ppn' => "090133803",
				'sudocNoHolding' => false,
				'titre' => "RESSI",
				'titreIssn' => "RESSI",
				'dateDebut' => "",
				'dateFin' => "",
				'issn' => "2198-7823",
				'issnl' => "2095-8293",
				'statut' => \Issn::STATUT_VALIDE,
				'support' => \Issn::SUPPORT_ELECTRONIQUE,
		]);
		$issn->fillBySudoc($api);

		// check the result
		$this->assertEquals(1564, $issn->titreId);
		$this->assertEquals("2010", $issn->dateDebut);
		$this->assertEquals("", $issn->dateFin);
		$this->assertEquals('2198-7823', $issn->issn);
		$this->assertEquals('2095-8293', $issn->issnl);
		$this->assertEquals(\Issn::STATUT_VALIDE, $issn->statut);
		$this->assertEquals('090133803', $issn->sudocPpn);
		$this->assertFalse($issn->sudocNoHolding);
		$this->assertEquals("RESSI", $issn->titreReference);
		$this->assertEquals(\Issn::SUPPORT_ELECTRONIQUE, $issn->support);
	}

	public function testFillBySudocNoholding()
	{
		// create an Issn record
		$issn = new \Issn();
		$issn->setAttributes(
			[
				'titreId' => 1564,
				'issn' => "1661-1802",
				'dateDebut' => '2010',
				'statut' => \Issn::STATUT_ENCOURS,
				'sudocNoHolding' => false,
			],
			false
		);

		// update it with a stub Sudoc API
		$api = $this->createStubApi("090133803", []);
		$issn->fillBySudoc($api);

		// check the result
		$this->assertEquals(1564, $issn->titreId);
		$this->assertEquals("2010", $issn->dateDebut);
		$this->assertEquals("", $issn->dateFin);
		$this->assertEquals('1661-1802', $issn->issn);
		$this->assertEquals(\Issn::STATUT_ENCOURS, $issn->statut);
		$this->assertEquals(\Issn::SUPPORT_INCONNU, $issn->support);
		$this->assertEquals("", $issn->titreReference);
		$this->assertEquals('090133803', $issn->sudocPpn);
		$this->assertTrue($issn->sudocNoHolding);
	}

	public function testValidateUnicity()
	{
		$issn = new \Issn();
		$issn->setAttributes(
			[
				'titreId' => 1564,
				'issn' => "1661-1802",
				'statut' => \Issn::STATUT_VALIDE,
				'support' => \Issn::SUPPORT_ELECTRONIQUE,
			],
			false
		);
		$this->assertTrue($issn->validateUnicity($issn->titre));
		$issn->issn = '1952-403X'; // not unique, used as issn-e
		$this->assertFalse($issn->validateUnicity($issn->titre));
	}

	private function createStubApi(string $ppn, array $content)
	{
		if ($content) {
			$notice = new \models\sudoc\Notice();
			foreach ($content as $k => $v) {
				$notice->$k = $v;
			}
		} else {
			$notice = null;
		}
		$ppnRecord = new \models\sudoc\Ppn($ppn, false);

		return $this->make(
			ApiClient::class,
			[
				'getNotice' => $notice,
				'getPpn' => [$ppnRecord],
			]
		);
	}
}
