<?php

namespace tests\unitwithyii\issn;

class IssnOrgPortalTest extends \Codeception\Test\Unit
{
	public function testUpdateRecordAll()
	{
		$issn = "0335-5322";
		$record = $this->initRecord($issn);
		$updater = new IssnPortal(['issnl', 'pays', 'support']);
		$updater->htmls = [$issn => file_get_contents(codecept_data_dir("issn-portal/$issn.html"))];

		$changes = $updater->updateRecord($record);
		$this->assertSame(['pays' => 'pays:→FR', 'support' => 'support:inconnu→papier'], $changes);
		$this->assertSame("FR", $record->pays);
		$this->assertSame(\Issn::SUPPORT_PAPIER, $record->support);

		// A second pass changes nothing
		$this->assertSame([], $updater->updateRecord($record));

		$record->issnl = '0335-000X';
		$this->assertSame(['issnl' => 'issnl:0335-000X→0335-5322'], $updater->updateRecord($record));
	}

	public function testUpdateRecordIssnl()
	{
		$issn = "0335-5322";
		$record = $this->initRecord($issn);
		$issnlUpdater = new IssnPortal(['issnl']);
		$issnlUpdater->htmls = [$issn => file_get_contents(codecept_data_dir("issn-portal/$issn.html"))];

		// Update only issnl: no change
		$result = $issnlUpdater->updateRecords([$record]);
		$this->assertCount(1, $result);
		$this->assertArrayHasKey($issn, $result);
		$this->assertSame([], $result[$issn]['changes']);
		$this->assertEquals($record, $result[$issn]['record']);
	}

	public function testUpdateRecordMultiCountries()
	{
		$issn = "2517-9837";
		$record = $this->initRecord($issn);
		$record->pays = "FR";
		$updater = new IssnPortal(['issnl', 'pays']);
		$updater->htmls = [$issn => file_get_contents(codecept_data_dir("issn-portal/$issn.html"))];

		$this->assertSame(['pays' => 'pays:FR→ZZ'], $updater->updateRecord($record));
		$this->assertSame("ZZ", $record->pays);
	}

	private function initRecord(string $issn): \Issn
	{
		$record = \Issn::model();
		$record->issn = $issn;
		$record->issnl = $issn;
		$record->pays = "";
		$record->support = \Issn::SUPPORT_INCONNU;
		return $record;
	}
}

/**
 * Derived class, only for tests, that removes network downloads.
 */
class IssnPortal extends \processes\issn\IssnOrgPortal
{
	public array $htmls = [];

	protected function downloadHtml(array $issns): array
	{
		return $this->htmls;
	}
}
