<?php

namespace tests\unitwithyii\issn;

use Issn;

/**
 * Tests the class "IssnValidator".
 */
class IssnValidatorTest extends \Codeception\Test\Unit
{
	/**
	 * @dataProvider providerIssn
	 */
	public function testValidateIssn($expect, $issn)
	{
		$issnRecord = new Issn();
		$issnRecord->setAttributes(['titreId' => 1, 'statut' => Issn::STATUT_VALIDE, 'support' => Issn::SUPPORT_INCONNU], false);
		$issnRecord->issn = $issn;

		if ($expect) {
			$this->assertTrue($issnRecord->validate(), "ISSN = '$issn'");
		} else {
			$this->assertFalse($issnRecord->validate(), "ISSN = '$issn'");
		}
	}

	public function providerIssn()
	{
		return array(
			[false, ''],
			[false, 'EN COURS'],  // statut VALIDE, not ENCOURS
			[false, 'en cours'],
			[false, '1234-5678'], // bad key
			[false, '03952037'],  // reject ISSN without '-'
			[true, '1769-101X'],
			[false, '0730–479X'], // long dash
			[false, 'sans'],
		);
	}
}
