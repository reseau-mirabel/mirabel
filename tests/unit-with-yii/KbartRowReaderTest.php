<?php

use processes\kbart\Logger;
use processes\kbart\read\Header;
use processes\kbart\read\RowReader;

class KbartRowReaderTest extends \Codeception\Test\Unit
{
	public function testParseGbm()
	{
		$firstRow = explode(
			"\t",
			"publication_title	print_identifier	online_identifier	date_first_issue_online	num_first_vol_online	num_first_issue_online	date_last_issue_online	num_last_vol_online	num_last_issue_online	title_url	first_author	title_id	embargo_info	coverage_depth	notes	publisher_name	publication_type	date_monograph_published_print	date_monograph_published_online	monograph_volume	monograph_edition	first_editor	parent_publication_title_id	preceding_publication_title_id	access_type"
		);
		$logger = new Logger();
		$header = new Header();
		$header->setLogger($logger);
		$header->read($firstRow);

		// Global Mental Health
		$input = "Global Mental Health		2054-4251	01/07/1993	1					http://www.cambridge.org/core/product/0ED89DDEA3AE758E19BA3F5FA74ECDE4		0ED89DDEA3AE758E19BA3F5FA74ECDE4		fulltext	New open access journal (2014-2015)		serial";
		$expected = [
			'title' => "Global Mental Health",
			'prefix' => "",
			'issn' => null,
			'issne' => "2054-4251",
			'id' => "0ED89DDEA3AE758E19BA3F5FA74ECDE4",
			'url' => "http://www.cambridge.org/core/product/0ED89DDEA3AE758E19BA3F5FA74ECDE4",
			'accessType' => null,
			'embargoInfo' => "",
			'coverageDepth' => "fulltext",
			'notes' => "New open access journal (2014-2015)",
			'publisherName' => "",
		];

		$reader = new RowReader($header, $logger);
		$reader->setTitlePrefixes(["La", "Le"]);
		$result = $reader->parse(explode("\t", $input), 1);
		foreach ($expected as $name => $value) {
			$this->assertEquals($value, $result->$name, $name);
		}
		$this->assertEquals("1993", $result->accessBegin->date, "Issue.accessBegin.date");
		$this->assertEquals(1, $result->accessBegin->vol, "Issue.accessBegin.vol");
		$this->assertTrue($result->accessEnd->isEmpty(), "Issue.accessEnd");
	}

	public function testParseSoc()
	{
		$firstRow = explode(
			"\t",
			"PUBLICATION_TITLE	PRINT_IDENTIFIER	ONLINE_IDENTIFIER	DATE_FIRST_ISSUE_ONLINE	NUM_FIRST_VOL_ONLINE	NUM_FIRST_ISSUE_ONLINE	DATE_LAST_ISSUE_ONLINE	NUM_LAST_VOL_ONLINE	NUM_LAST_ISSUE_ONLINE	TITLE_URL	FIRST_AUTHOR	TITLE_ID	EMBARGO_INFO	COVERAGE_DEPTH	NOTES	PUBLISHER_NAME	PUBLICATION_TYPE	DATE_MONOGRAPH_PUBLISHED_PRINT	DATE_MONOGRAPH_PUBLISHED_ONLINE	MONOGRAPH_VOLUME	MONOGRAPH_EDITION	FIRST_EDITOR	PARENT_PUBLICATION_TITLE_ID	PRECEDING_PUBLICATION_TITLE_ID	ACCESS_TYPE"
		);
		$logger = new Logger();
		$header = new Header();
		$header->setLogger($logger);
		$header->read($firstRow);

		$input = "Archiv für Papyrusforschung und verwandte Gebiete	0066-6459	1867-1551	1901-01-01	1	1	2018-12-20	LXIV	2	https://www.degruyter.com/openurl?genre=journal&issn=1867-1551		8208		fulltext		De Gruyter	serial								P";
		$expected = [
			'title' => "Archiv für Papyrusforschung und verwandte Gebiete",
			'prefix' => "",
			'issn' => "0066-6459",
			'issne' => "1867-1551",
			'id' => "8208",
			'url' => "https://www.degruyter.com/openurl?genre=journal&issn=1867-1551",
			'accessType' => 'P',
			'embargoInfo' => "",
			'coverageDepth' => "fulltext",
			'notes' => "",
			'publisherName' => "De Gruyter",
		];

		$reader = new RowReader($header, $logger);
		$result = $reader->parse(explode("\t", $input), 1);
		foreach ($expected as $name => $value) {
			$this->assertEquals($value, $result->$name, $name);
		}
		$this->assertEquals("1901-01-01", $result->accessBegin->date, "Issue.accessBegin.date");
		$this->assertEquals("2018-12-20", $result->accessEnd->date, "Issue.accessEnd.date");
		$this->assertEquals(64, $result->accessEnd->vol, "Issue.accessEnd.vol");
		$this->assertEquals("Vol. LXIV, no 2", $result->accessEnd->numbering);
	}
}
