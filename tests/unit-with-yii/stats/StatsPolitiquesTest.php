<?php

namespace tests\unitwithyii\stats;

use processes\stats\PolitiqueEditeurs;
use processes\stats\PolitiqueUtilisateurs;

class StatsPolitiquesTest extends \Codeception\Test\Unit
{
	public function testEditeurs()
	{
		$stats = new PolitiqueEditeurs();
		$this->assertEquals([[1, 2]], $stats->byJournal());
		$this->assertEquals(
			[
				['Presses de Sciences Po', 1, 1, 0],
				['Courant alternatif', 1, 1, 0],
			],
			self::stripHtmlTags($stats->byPublisher())
		);
		$this->assertEquals(
			[
				["brouillon", 1],
				["publié", 1],
			],
			$stats->byStatus()
		);
	}

	public function testUtilisateurs()
	{
		$stats = new PolitiqueUtilisateurs();
		$this->assertEquals([
			["Partenaire-éditeur", 1]],
			$stats->byInstitute()
		);
		$this->assertEquals(
			[
				["Institut québécois des hautes études internationales", 1],
				['Julliard', 1],
			],
			self::stripHtmlTags($stats->byPublisher())
		);
		$this->assertEquals(
			[
				"Comptes validés" => 0,
				"Comptes en attente de validation" => '2',
			],
			self::stripHtmlTags($stats->counts())
		);
	}

	private static function stripHtmlTags(array $a): array
	{
		array_walk_recursive(
			$a,
			function (&$x) {
				if (is_string($x)) {
					$x = strip_tags($x);
				}
			}
		);
		return $a;
	}
}
