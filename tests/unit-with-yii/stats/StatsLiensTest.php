<?php

namespace tests\unitwithyii\stats;

use processes\stats\Liens;
use Yii;

class StatsLiensTest extends \Codeception\Test\Unit
{
	public function testAll()
	{
		$stats = new Liens(Yii::app()->db);
		$stats->run();
		$this->assertSame(
			[
				[
					'domain' => 'hal.archives-ouvertes.fr',
				    'num' => '22',
					'numRevues' => '17',
					'numIdentif' => '22',
				],
				[
					'domain' => 'fr.wikipedia.org',
					'num' => '8',
					'numRevues' => '8',
					'numIdentif' => '8',
				],
				[
					'domain' => 'journalbase.cnrs.fr',
					'num' => '7',
					'numRevues' => '7',
					'numIdentif' => '7',
				],
				[
					'domain' => 'facebook.com',
					'num' => '6',
					'numRevues' => '6',
					'numIdentif' => '6',
				],
				[
					'domain' => 'x.com',
					'num' => '6',
					'numRevues' => '6',
					'numIdentif' => '6',
				],
				[
					'domain' => 'LIEN INTERNE',
					'num' => '5',
					'numRevues' => '3',
					'numIdentif' => '0',
				],
				[
					'domain' => 'jurisguide.fr',
					'num' => '2',
					'numRevues' => '2',
					'numIdentif' => '2',
				],
				[
					'domain' => 'en.wikipedia.org',
					'num' => '2',
					'numRevues' => '2',
					'numIdentif' => '2',
				],
			],
			$stats->domains
		);
		$this->assertSame(
			[
				[
					'name' => 'HAL',
					'num' => '22',
					'numRevues' => '17',
				],
				[
					'name' => 'Wikipedia',
					'num' => '10',
					'numRevues' => '10',
				],
				[
					'name' => 'Facebook',
					'num' => '7',
					'numRevues' => '7',
				],
				[
					'name' => 'JournalBase',
					'num' => '7',
					'numRevues' => '7',
				],
				[
					'name' => 'Twitter',
					'num' => '6',
					'numRevues' => '6',
				],
				[
					'name' => 'Jurisguide',
					'num' => '2',
					'numRevues' => '2',
				],
			],
			$stats->names
		);
	}
}