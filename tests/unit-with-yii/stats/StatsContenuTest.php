<?php

namespace tests\unitwithyii\stats;

use processes\stats\Contenu;
use Yii;

class StatsContenuTest extends \Codeception\Test\Unit
{
	public function testRessourcesParType()
	{
		$stats = new Contenu(Yii::app()->db);
		$this->assertSame(
			[
				[
					'Type' => 'Bouquet de revues',
				    'Ressources' => 12,
				],
				[
					'Type' => 'Site web',
				    'Ressources' => 7,
				],
				[
					'Type' => 'Archives de revues',
				    'Ressources' => 3,
				],
				[
					'Type' => 'Base de sommaires',
				    'Ressources' => 3,
				],
				[
					'Type' => 'Base de bibliographie',
				    'Ressources' => 3,
				],
				[
					'Type' => 'Catalogue éditeur',
				    'Ressources' => 3,
				],
			],
			$stats->getRessourcesParType()
		);
	}

	public function testRessourcesImportees()
	{
		$stats = new Contenu(Yii::app()->db);
		$this->assertCount(6, $stats->getRessourcesImportees());
		$this->assertSame(
			[
				'nom' => "Cairn.info",
				'type' => "Bouquet",
			],
			$stats->getRessourcesImportees()[0]
		);
	}

	public function testAccesParType()
	{
		$stats = new Contenu(Yii::app()->db);
		$result = $stats->getAccesParType();
		$this->assertCount(5, $result);
		$this->assertArrayHasKey('Intégral', $result);
		$this->assertSame(
			[
				'type' => 'Intégral',
				'Libre' => 33,
				'Restreint' => 40,
				'Total' => 73,
				'Taux' => 45.0,
			],
			$result['Intégral']
		);
		$this->assertArrayHasKey('Tout type', $result);
		$this->assertSame(
			77,
			$result['Tout type']['Libre']
		);
	}

	public function testRevuesParAcces()
	{
		$stats = new Contenu(Yii::app()->db);
		$this->assertSame(
			[
				'Intégral libre' => 15,
				'Intégral restreint' => 14,
				'Sommaire' => 15,
				'Sommaire sans accès intégral' => 0,
				'Indexation' => 6,
				'Sans accès' => 2,
				'Total' => 20,
			],
			$stats->getRevuesParAcces()
		);
	}

	public function testCountLangGroups()
	{
		$stats = new Contenu(Yii::app()->db);
		$this->assertSame(4, $stats->countLangGroups());
	}

	public function testCountLangGroupsUnordered()
	{
		$stats = new Contenu(Yii::app()->db);
		$this->assertSame(4, $stats->countLangGroupsUnordered());
	}

	public function testGetLangInfo()
	{
		$stats = new Contenu(Yii::app()->db);
		$this->assertSame(
			[
			['Français', 18, 17],
			['Anglais', 3, 2],
			['Espagnol', 0, 0],
			['Allemand', 0, 0],
			['Bilingue français-anglais', 1, 1],
			],
			$stats->getLangInfo()->data
		);
	}

	public function testGetLangNb()
	{
		$stats = new Contenu(Yii::app()->db);
		$this->assertSame(
			[
				['Sans langue précisée', 0],
				['Une langue', 19],
				['Deux langues', 1],
				['Trois langues', 0],
				['Plus de langues', 0],
			],
			$stats->getLangNb()->data
		);
	}

	public function testGetEditeursNb()
	{
		$stats = new Contenu(Yii::app()->db);
		$this->assertSame(
			[
				["Un éditeur", 15],
				["Deux éditeurs", 5],
				["Trois éditeurs", 0],
				["Plus de 3 éditeurs", 0],
			],
			$stats->getEditeursNb()->data
		);
	}

	public function testGetMainInfo()
	{
		Yii::app()->getCache()->flush();
		$stats = new Contenu(Yii::app()->db);
		$this->assertSame(
			[
				[2],
				[1],
				[30],
				[121],
				[394],
				[4],
				[1]
			],
			$stats->getMainInfo()->data
		);
	}

	public function testGetMainInfoRessources()
	{
		$stats = new Contenu(Yii::app()->db);
		$this->assertSame(
			[
				[31],
				[0],
				[9],
				[6],
				[64],
				[4],
			],
			$stats->getMainInfoRessources()->data
		);
	}

	public function testGetMainInfoRevues()
	{
		$stats = new Contenu(Yii::app()->db);
		$this->assertSame(
			[
				[20],
				[19],
				[5],
				[18],
				[18],
				[36],
				[56],
			],
			$stats->getMainInfoRevues()->data
		);
	}
}
