<?php

namespace tests\unitwithyii\commands;

use commands\models\sudoc\CheckByIssn;
use models\sudoc\ApiClient;
use models\sudoc\Ppn;
use Yii;

class SudocCheckTest extends \Codeception\Test\Unit
{
	public function testCheck()
	{
		Yii::app()->db->createCommand("UPDATE Issn SET sudocPpn = '' WHERE issn = '1540-5907'")->execute();

		$check = new CheckByIssn($this->getApiClientMockup());
		$check->setConfig([
			'limit' => 5,
			'verbose' => 0,
		]);
		$output = fopen('php://memory', 'w');
		$check->run($output);

		rewind($output);
		self::assertSame(<<<EOCSV
			1540-5907		ADD	"Le PPN 153829591 a été ajouté à cet ISSN (intervention 999)"
			2437-2315	183838378	ERR	"Sudoc: erreur HTTP, code 500: timeout"
			2420-465X	197509789	WARN	"Multiples PPN pour cet ISSN dans le Sudoc"

			EOCSV,
			preg_replace('/intervention \d+/', 'intervention 999', stream_get_contents($output))
		);

		Yii::app()->db->createCommand("UPDATE Issn SET sudocPpn = '153829591' WHERE issn = '1540-5907'")->execute();
	}

	private function getApiClientMockup()
	{
		return $this->make(ApiClient::class, [
			'getPpn' => function(string $issn): array {
				if ($issn === '1540-5907') {
					return [new Ppn('153829591', false)];
				}
				if ($issn === '2419-8595') {
					return [new Ppn('197501036', false)];
				}
				if ($issn === '2420-4641') {
					return [new Ppn('197509770', false)];
				}
				if ($issn === '2437-2315') {
					throw new \Exception("Sudoc: erreur HTTP, code 500: timeout");
				}
				if ($issn === '2420-465X') {
					return [new Ppn('197509789', false), new Ppn('254879918', true)];
				}
				return [];
			},
		]);
	}
}
