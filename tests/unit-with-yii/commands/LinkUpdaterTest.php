<?php

namespace tests\unitwithyii\commands;

use commands\models\LinkUpdater;
use Sourcelien;
use Titre;
use Yii;

class LinkUpdaterTest extends \Codeception\Test\Unit
{
	public function testValidation()
	{
		$transaction = Yii::app()->db->beginTransaction();

		$source = Sourcelien::model()->findByAttributes(['nomcourt' => "openpolicyfinder"]);
		$this->assertInstanceOf(Sourcelien::class, $source);

		$titre = Titre::model()->findByPk(725);
		$titre->urlCouverture = "https://www-degruyter-com/doc/cover/s0722480X.jpg";
		$titre->save(false);
		$updater = new LinkUpdater($source);
		$csv = $updater->updateLink($titre, "0003-441X", "https://example.com/do-not-existtt");
		$this->assertEquals($csv, <<<EOCSV
			718;725;"Annales d'histoire économique et sociale";0003-441X;https://example.com/do-not-existtt;"Lien ajouté";OK;http://localhost/revue/718

			EOCSV
		);

		$transaction->rollback();
	}

	public function testMultiple()
	{
		$transaction = Yii::app()->db->beginTransaction();

		$sourceWen = Sourcelien::model()->findByAttributes(['nomcourt' => "wikipedia-en"]);
		$sourceWfr = Sourcelien::model()->findByAttributes(['nomcourt' => "wikipedia-fr"]);
		$titre = Titre::model()->findByPk(1229);

		$updater = new LinkUpdater();
		$updater->verbose = 2;
		$updater->addSource($sourceWen);
		$updater->addSource($sourceWfr);
		$csv = $updater->updateLinks(
			$titre,
			"XXX",
			[
				[$sourceWen->id, "https://en.wikipedia.org/tralala"],
				[$sourceWfr->id, "https://fr.wikipedia.org/tralala"],
			]
		);
		$this->assertEquals(
			$csv,
			<<<EOCSV
			1174;1229;"American journal of political science";XXX;https://en.wikipedia.org/tralala;"Lien modifié";OK;http://localhost/revue/1174
			1174;1229;"American journal of political science";XXX;https://fr.wikipedia.org/tralala;"Lien ajouté";OK;http://localhost/revue/1174

			EOCSV
		);

		$transaction->rollback();
	}
}
