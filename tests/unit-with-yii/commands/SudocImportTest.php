<?php

namespace tests\unitwithyii\commands;

use commands\models\sudoc\ImportByPpn;
use models\sudoc\ApiClient;
use models\sudoc\Notice;

class SudocImportTest extends \Codeception\Test\Unit
{
	public function testCheck()
	{
		$import = new ImportByPpn($this->getApiClientMockup());
		$import->setConfig([
			'limit' => 1,
			'verbose' => 0,
		]);
		$output = fopen('php://memory', 'w');
		$import->run($output);

		rewind($output);
		self::assertSame(<<<EOCSV
			Erreur	issn	ppn	Titre.id	titre	Modifications
			WARNING	0001-7728	039219208	1	"Actualité Juridique Droit Administratif"	"Champs ignorés car conflits : [dateDebut : SUDOC=1954 M=1955]"
			OK	0001-7728	039219208	1	"Actualité Juridique Droit Administratif"	[bnfArk=cb34348119w]

			EOCSV,
			stream_get_contents($output)
		);
	}

	private function getApiClientMockup()
	{
		return $this->make(ApiClient::class, [
			'getNotice' => function(string $ppn): ?Notice {
				if ($ppn === '039219208') {
					$n = new Notice;
					$n->bnfArk = 'cb34348119w';
					$n->dateDebut = '1954';
					$n->dateFin = '';
					$n->issn = '0001-7728';
					$n->issnl = '0001-7728';
					$n->langues = 'fre';
					$n->pays = 'FR';
					$n->ppn = '039219208';
					$n->statut = 0;
					$n->sudocNoHolding = false;
					$n->support = 'papier';
					$n->titre = "L'Actualité juridique";
					$n->titreIssn = "L'Actualité juridique. Droit administratif";
					$n->titreVariantes = [];
					return $n;
				}
				return null;
			},
		]);
	}
}
