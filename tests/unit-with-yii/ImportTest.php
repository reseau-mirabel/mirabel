<?php

use processes\kbart\Import;
use processes\kbart\State;

class ImportTest extends \Codeception\Test\Unit
{
	private static $ressource;
	private static $saveDir;
	private static $hash;
	private static $savedStateFile;

    /**
     * @Override
     */
	protected function _before(): void
	{
		self::$saveDir = DATA_DIR . "/cache/kbart";
		if (!is_dir(self::$saveDir)) {
			@mkdir(self::$saveDir, 0777);
		}

		if (self::$ressource === null) {
			self::$ressource = \Ressource::model()->findByPk(3);
		}
	}

    /**
     * @Override
     */
	protected function _after(): void
	{
		foreach (glob(self::$saveDir . "/test-*") as $file) {
			@unlink($file);
		}
		@rmdir(self::$saveDir);
	}

	private static function getKbartFilename(string $kbartFile): string
	{
		$uploadKbartFile = dirname(__DIR__) . "/_data/$kbartFile";
		self::$hash = md5_file($uploadKbartFile);
		self::$savedStateFile = sprintf("%s/%s.serialized", self::$saveDir, self::$hash);
		if (is_file(self::$savedStateFile)) {
			unlink(self::$savedStateFile);
		}
		return $uploadKbartFile;
	}

	/**
	 * Import d'un fichier KBART d'une seule ligne :
	 * - titre reconnu par son identifiiant interne à la ressource,
	 * - aucun accès en ligne n'existe avant l'import
	 * - création d'un accès en ligne unique.
	 */
	public function testCreateSingle()
	{
		$state = new State(self::$ressource);
		$import = new Import($state);
		$this->assertTrue(
			$import->readFile(self::getKbartFilename("kbart_cairn_create-coverage.txt")),
			"Erreur. Log = " . json_encode($import->getLogger()->getGlobalLog(), JSON_PRETTY_PRINT)
		);
		$import->checkConsistency();
		$import->identifyTitles();
		$eventsRow2 = $import->getLogger()->getRowLog(2)->getEvents();
		$this->assertCount(0, $eventsRow2);
		$titreId = 1900;
		$crossed = $state->crossedData;
		$this->assertEquals($titreId, $crossed->known[$titreId]->title->id);
		$this->assertCount(0, $crossed->known[$titreId]->services, "No Service exists prior to import.");

		$config = new processes\kbart\noninteractive\Config();
		$config->lacunaire = true;
		$config->selection = true;
		$config->diffusion = 'Restreint';
		$diffs = $import
			->diff($config)
			->getDiffs();
		$this->assertCount(1, $diffs, "A single Titre must appear in changes.");
		$this->assertArrayHasKey($titreId, $diffs, print_r($diffs, true));
		$diff = $diffs[$titreId];
		$this->assertInstanceof(\processes\kbart\data\Diff::class, $diff);

		$this->assertCount(1, $diff->services->getCreate());
		$this->assertCount(0, $diff->services->getUpdate(), "The change must have no update");

		$created = array_filter($diff->services->getCreate()[0]->getAttributes());
		unset($created['hdateImport']);
		ksort($created);
		codecept_debug($created);
		$this->assertEquals(
			[
				'acces' => 'Restreint',
				'dateBarrDebut' => '2003',
				'import' => \models\import\ImportType::getSourceId('KBART'),
				'lacunaire' => 1,
				'noDebut' => 1,
				'numeroDebut' => 'no 1',
				'ressourceId' => 3,
				'selection' => 1,
				'statut' => 'normal',
				'titreId' => 1900,
				'type' => 'Intégral',
				'url' => 'https://www.cairn.info/revue-annales.htm',
			],
			$created
		);
	}

	/**
	 * Import d'un fichier KBART d'une seule ligne :
	 * - titre reconnu par son identifiiant interne à la ressource,
	 * - deux accès en ligne existent avant l'import
	 * - l'un des deux accès est fusionnable (diffusion "Libre")
	 * - modification de cet unique accès en ligne.
	 */
	public function testUpdateSingle()
	{
		$state = new State(self::$ressource);
		$import = new Import($state);
		$import->readFile(self::getKbartFilename("kbart_cairn_update-coverage.txt"));
		$import->checkConsistency();
		$import->identifyTitles();
		$crossed = $state->crossedData;
		$this->assertCount(1, $crossed->known, "A single Titre must get identified.");
		$titreId = 29;
		$this->assertArrayHasKey($titreId, $crossed->known);
		$this->assertCount(2, $crossed->known[$titreId]->services, "Two instances of Service exist prior to import.");

		$config = new processes\kbart\noninteractive\Config();
		$config->lacunaire = false;
		$config->selection = false;
		$config->diffusion = 'Libre';
		$diffs = $import
			->diff($config)
			->getDiffs();
		$this->assertCount(1, $diffs, "A single Titre must appear in changes.");
		$this->assertArrayHasKey($titreId, $diffs);
		$diffServices = $diffs[$titreId]->services;
		$this->assertInstanceof(\processes\kbart\work\DiffServices::class, $diffServices);
		assert($diffServices instanceof \processes\kbart\work\DiffServices);

		$this->assertCount(0, $diffServices->getCreate(), "The change must have no creation");
		$this->assertCount(1, $diffServices->getUpdate(), "A single Service must appear in changes");
		$this->assertCount(1, $diffServices->getDelete()); // Includes 1 unmatched
		$this->assertCount(1, $diffServices->getUnmatched());
		$updated = $diffServices->getUpdate()[0];

		$this->assertSame(["revue-annales-historiques-de-la-revolution-francaise", "revue-annales-de-la-revolution", 718], $diffs[$titreId]->identification);

		$this->assertSame(4899, (int) $updated['before']->id); // Service.id
		$newValues = array_filter(
			array_diff_assoc($updated['after']->getAttributes(), $updated['before']->getAttributes()),
			function($x) { return $x !== null; }
		);
		unset($newValues['hdateModif'], $newValues['hdateImport']);
		$oldValues = array_diff_assoc($updated['before']->getAttributes(), $updated['after']->getAttributes());
		unset($oldValues['hdateModif'], $oldValues['hdateImport']);
		ksort($newValues);
		$this->assertEquals(
			[
				'dateBarrDebut' => '2013',
				'dateBarrFin' => '',
				'dateBarrInfo' => "", // l'import vide le champ
				'import' => \models\import\ImportType::getSourceId('KBART'),
				'noDebut' => 1,
				'numeroDebut' => 'Vol. 371, no 1',
				'numeroFin' => '',
				'url' => 'https://www.cairn.info/revue-annales-historiques-de-la-revolution-francaise.htm',
				'volDebut' => 371,
			],
			$newValues
		);
		ksort($oldValues);
		$this->assertEquals(
			[
				'dateBarrDebut' => '2017',
				'dateBarrFin' => '2019',
				'dateBarrInfo' => "Barrière mobile de 2 ans",
				'import' => '1', // cairn
				'noDebut' => null,
				'noFin' => '396',
				'numeroDebut' => '',
				'numeroFin' => 'no 396',
				'url' => 'http://www.cairn.info/revue-annales-historiques-de-la-revolution-francaise.htm',
				'volDebut' => null,
			],
			$oldValues
		);
	}

	public function testUpdatePairIntoPair()
	{
		$state = new State(self::$ressource);
		$import = new Import($state);
		$import->readFile(self::getKbartFilename("kbart_cairn_update-pair-into-pair.txt"));

		$import->checkConsistency();
		$import->identifyTitles();
		$titreId = 29;
		$crossed = $state->crossedData;
		$this->assertCount(2, $crossed->known[$titreId]->services, "Two instances of Service exist prior to import.");

		// Save the current state.
		$this->assertFalse(is_file(self::$savedStateFile), "File should not be found: " . self::$savedStateFile);
		$this->assertTrue($import->save(self::$hash));
		$this->assertTrue(is_file(self::$savedStateFile), "File not found: " . self::$savedStateFile);

		$config = new processes\kbart\noninteractive\Config();
		$config->lacunaire = false;
		$config->selection = false;
		$config->diffusion = 'Libre';
		$diffs = $import
			->diff($config)
			->getDiffs();
		$this->assertCount(1, $diffs, "A single Titre must appear in changes.");
		$this->assertArrayHasKey($titreId, $diffs);
		$this->assertInstanceof(\processes\kbart\data\Diff::class, $diffs[$titreId]);
		$diffServices = $diffs[$titreId]->services;
		$this->assertInstanceof(\processes\kbart\work\DiffServices::class, $diffServices);

		$this->assertCount(0, $diffServices->getCreate(), "The change must have no creation");
		$this->assertCount(2, $diffServices->getUpdate());
		$this->assertCount(0, $diffServices->getDelete());
		$this->assertCount(0, $diffServices->getUnmatched());

		$this->assertSame($diffServices->getUpdate()[0]['before']->acces, $diffServices->getUpdate()[0]['after']->acces);
		$this->assertSame($diffServices->getUpdate()[1]['before']->acces, $diffServices->getUpdate()[1]['after']->acces);
		$this->assertSame($diffServices->getUpdate()[0]['before']->type, $diffServices->getUpdate()[0]['after']->type);
		$this->assertSame($diffServices->getUpdate()[1]['before']->type, $diffServices->getUpdate()[1]['after']->type);

		// Check the saved state.
		$this->subtestRestoreState();
	}

	private function subtestRestoreState()
	{
		// restore import from serialization
		$this->assertTrue(is_file(self::$savedStateFile), "File not found: " . self::$savedStateFile);
		$importRestored = Import::load(self::$hash);
		$this->assertInstanceof(Import::class, $importRestored);
		$importRestored->checkConsistency();
		$this->assertInstanceof(\processes\kbart\Logger::class, $importRestored->state->logs);
	}
}
