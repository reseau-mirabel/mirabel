<?php

namespace tests\unitwithyii\attribut;

use processes\attribut\ImportConfig;
use processes\attribut\Importeattribut;

class ImporteattributTest extends \Codeception\Test\Unit
{
	/**
	 * @dataProvider provideData
	 */
	public function testLoad(string $filePath, array $config, array $expected)
	{
		$importer = new Importeattribut(new ImportConfig($config));
		$importer->load($filePath, substr($filePath, -3));

		$this->assertEquals($expected['rows'], $importer->getNumRows(), "Wrong count of CSV rows (containing ISSN and attribute)");
		// TMP disabled, because empty values are inserted, updates are doubled, and removals are missing
		//$this->assertEquals($expected['changes'], $importer->saveAttribute(), print_r($importer, true));
	}

	public function provideData()
	{
		return [
			// First sample
			[
				codecept_data_dir('/doaj.csv'),
				[
					'sourceattributId' => 1,
					'colIdentifiers' => ['issn' => [3]],
					'colAttribut' => 31,
				],
				['rows' => 4, 'changes' => 1], // expected results
			],
			// Second sample
			[
				codecept_data_dir('/doaj.csv'),
				[
					'sourceattributId' => 2,
					'colIdentifiers' => ['issn' => [3]],
					'colAttribut' => 40,
				],
				['rows' => 4, 'changes' => 0],
			],
			// Third sample: no value, only 1 if the title is in the imported source.
			[
				codecept_data_dir('/doaj.csv'),
				[
					'sourceattributId' => 1,
					'colIdentifiers' => ['issn' => [3]],
				],
				['rows' => 4, 'changes' => 1],
			],
		];
	}

	public function testLoadFiltered()
	{
		$config = new ImportConfig([
			'sourceattributId' => 1,
			'colIdentifiers' => ['issn' => [3, 4]],
			'colAttribut' => 29,
			'filtre' => 'Yes',
		]);
		$importer = new Importeattribut($config);
		$filePath = codecept_data_dir('/doaj.csv');
		$importer->load($filePath, 'csv', ',');

		$this->assertSame(1, $importer->getNumRows(), "Wrong count of CSV rows (containing ISSN and attribute with value 'Yes')");
		//$this->assertSame(["0001-3765" => "Yes", "1678-2690" => "Yes"], $valuesByIssn); // ISSNs of the row having the value "Yes"
	}
}
