<?php

namespace tests\unitwithyii\attribut;

use AttributImportLog;
use processes\attribut\FileImport;
use processes\attribut\ImportConfig;
use Yii;

class FileImportTest extends \Codeception\Test\Unit
{
	public function testPermissions()
	{
		$importedFile = sys_get_temp_dir() . '/M-issn-tmp.ods';
		copy(codecept_data_dir('issn-compare.ods'), $importedFile);
		$key = FileImport::storeFile($importedFile); // will delete the source
		$this->assertStringEndsWith('.ods', $key);

		$log = new AttributImportLog();
		$log->fileKey = $key;
		$log->fileName = 'issn-compare.ods';
		$this->assertSame(
			["2307-4302", "", "Rien du tout"],
			FileImport::readSourceHeader($log->getFilePath())
		);

		$config = new ImportConfig([
			'colIdentifiers' => ['issn' => [0, 1]],
			'sourceattributId' => 1,
		]);
		$process = new FileImport($log, $config);
		$this->assertEmpty($process->getLastError());

		$path = DATA_DIR . '/filestore/' . FileImport::FILESTORE_ID;
		$this->assertSame(0775, 0775 & fileperms($path), "Wrong file permissions for directory '$path'"); // at least rwX|rwX|rX
		foreach (glob("$path/*") as $file) {
			$this->assertSame(0664, 0664 & fileperms($file), "Wrong file permissions for '$file'"); // // rw|rw|r
			unlink($file);
		}
	}
}
