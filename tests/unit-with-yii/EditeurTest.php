<?php

class EditeurTest extends \Codeception\Test\Unit
{
	/**
	 * @dataProvider provideNames
	 */
	public function testSelfUrl(string $encoded, string $name, string $acronym)
	{
		$e = new Editeur();
		$e->nom = $name;
		$e->sigle = $acronym;
		$url = $e->getSelfUrl();
		$this->assertSame($encoded, $url['nom']);
	}

	public function provideNames(): array
	{
		return [
			['Mirabel', 'Mir@bel', ''],
			['', 'Византолошки институт', 'САНУ'],
			['C-est-l-ete-ET', "C'est l'été", 'ÉT'],
		];
	}
}
