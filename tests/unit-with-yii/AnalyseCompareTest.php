<?php

use processes\analyse\Compare;

class AnalyseCompareTest extends \Codeception\Test\Unit
{
	function tearDown(): void
	{
		// delete tmp files
		foreach (glob(sys_get_temp_dir() . "/RevueCompare_*") as $tmpFile) {
			unlink($tmpFile);
		}
	}

	/**
	 * @dataProvider provideData
	 */
	function testLoad(string $filePath, int $numRows, int $numIssns, array $comparison)
	{
		$comparer = new Compare();
		$comparer->load($filePath);

		$this->assertEquals($numRows, $comparer->getNumRows(), "Wrong count of CSV rows (only rows containing ISSNs)");

		$this->assertEquals($numIssns, $comparer->getNumIssns(), "Wrong number of ISSNs found in the source");

		$i = 0;
		$got = $comparer->getMatches(0); // 0 => without possession data
		$this->assertCount(count($comparison), $got, "Comparison has wrong count of results");
		foreach ($comparison as $i => $expect) {
			foreach ($expect as $k => $v) {
				$this->assertEquals($v, $got[$i]->{$k}, "Result #$i differs on field $k");
			}
		}
	}

	public function provideData()
	{
		return [
			[
				$this->createTmpFile(
					<<<EOCSV
					issne;issnp;titre
					2437-2315;0575-089X;Les annales de l'histoire SOCIALE
					;;Sans ISSN
					;1056-8190;pas dans M
					1662-8667;1660-7880;1662-8667;A contrario
					EOCSV
				),
				3, // rows (journals)
				5, // issns
				[
					[
						'num' => 2,
						'issnInput' => '0575-089X 2437-2315',
						'issnMatching' => '2437-2315',
						'titre' => "Annales d'histoire sociale (1939)",
						'titreId' => 1084,
						'revueId' => 718,
						'url' => 'http://localhost/revue/718',
						'suivi' => false,
					],
					[
						'num' => 4,
						'issnInput' => '1056-8190',
						'issnMatching' => '',
						'titre' => "",
						'titreId' => 0,
						'revueId' => 0,
						'url' => '',
						'suivi' => false,
					],
					[
						'num' => 5,
						'issnInput' => '1660-7880 1662-8667',
						'issnMatching' => '1660-7880 1662-8667',
						'titre' => "A contrario. Revue interdisciplinaire de sciences sociales",
						'titreId' => 444,
						'revueId' => 444,
						'url' => 'http://localhost/revue/444',
						'suivi' => true,
					],
				],
			],
			[
				dirname(__DIR__) . '/_data/issn-compare.ods',
				2, // rows (journals)
				4, // issns
				[
					[
						'num' => 1,
						'issnInput' => '2307-4302',
						'issnMatching' => '',
						'titre' => "",
						'titreId' => 0,
						'revueId' => 0,
						'url' => '',
						'suivi' => false,
					],
					[
						'num' => 2,
						'issnInput' => '0575-089X 1660-7880 1920-437X',
						'issnMatching' => '1660-7880',
						'titre' => "A contrario. Revue interdisciplinaire de sciences sociales",
						'titreId' => 444,
						'revueId' => 444,
						'url' => 'http://localhost/revue/444',
						'suivi' => true,
					],
				],
			],
		];
	}
	private function createTmpFile(string $content): string
	{
		$fileName = tempnam(sys_get_temp_dir(), 'RevueCompare_') . ".csv";
		file_put_contents($fileName, $content);
		return $fileName;
	}
}
