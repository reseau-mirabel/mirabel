<?php

use models\sherpa\Item;
use models\sherpa\Publication;

class SherpaItemTest extends \Codeception\Test\Unit
{
	/**
	 * @var \models\sherpa\Item[]
	 */
	private $sample = [];

	protected function setUp(): void
	{
		parent::SetUp();
		$dataDir = dirname(__DIR__) . '/_data';
		foreach (["1179-3163", "0002-7294", "0335-5322", "1278-3366"] as $issn) {
			$this->sample[$issn] = Item::loadFromFile("$dataDir/sherpa_$issn.json");
		}
	}

	/**
	 * @dataProvider policyProvider
	 */
	public function testGetPoliciesSummary(string $issn, array $expected)
	{
		$item = $this->sample[$issn];
		$this->assertSame($expected, $item->getPoliciesSummary());
	}

	public function policyProvider()
	{
		return [
			[
				"0002-7294",
				["submitted" => 1, "accepted" => 1, "published" => 2],
			],
			[
				"0335-5322",
				["submitted" => 1, "accepted" => 1, "published" => 0],
			],
			[
				"1278-3366",
				["submitted" => 1, "accepted" => 1, "published" => 1],
			],
		];
	}

	/**
	 * @dataProvider publicationProvider
	 */
	public function testPublication(string $issn, array $attributes, int $openAccess, array $resourceUrls)
	{
		$publication = $this->sample[$issn]->getPublication();
		foreach ($attributes as $k => $v) {
			$this->assertSame($v, $publication->$k);
		}
		$this->assertSame($openAccess, $publication->getOpenAccess());
		$this->assertSame($resourceUrls, $publication->getResourceUrls());
	}

	public function publicationProvider()
	{
		return [
			[
				"0002-7294",
				[
					// attribute => expected value
					'preferredTitle' => [
						"Journal of the American Anthropological Association",
					],
					'romeoUrl' => "http://v2.sherpa.ac.uk/id/publication/1149",
					'url' => "http://onlinelibrary.wiley.com/journal/10.1111/%28ISSN%291548-1433",
				],
				Publication::ACCESS_OPEN,
				[
					"AAA Author Agreement Form - Example" => "http://americanethnologist.org/submit/aaa-author-agreement-form",
					"Publish open access with Wiley OnlineOpen" => "https://authorservices.wiley.com/author-resources/Journal-Authors/open-access/onlineopen.html",
				],
			],
			/*
			[
				"0008-4239",
				[
					// attribute => expected value
					'preferredTitle' => [
						"Canadian Journal of Political Science",
						"Revue canadienne de science politique",
					],
					'romeoUrl' => "http://v2.sherpa.ac.uk/id/publication/1541",
					'url' => "http://journals.cambridge.org/action/displayJournal?jid=CJP",
				],
				Publication::ACCESS_OPEN,
				[
					"Services for open access policies" => "https://www.cambridge.org/core/services/open-access-policies",
					"Creative commons licenses" => "https://www.cambridge.org/core/services/open-access-policies/open-access-resources/creative-commons-licenses",
					"Hybrid Open Access FAQs" => "https://www.cambridge.org/core/services/open-access-policies/open-access-journals/hybrid-open-access-faqs",
				],
			],
			 */
		];
	}

	/**
	 * @dataProvider publicationOaProvider
	 */
	public function testPublicationOa(string $issn, int $policyRank, string $version, int $oaRank, array $expected)
	{
		$publication = $this->sample[$issn]->getPublication();
		$policy = $publication->policy[$policyRank];
		$this->assertInstanceOf(\models\sherpa\Policy::class, $policy);
		$oa = $policy->oa[$version][$oaRank];

		if ($expected['additionalOaFee']) {
			$this->assertTrue($oa->additionalOaFee);
		} else {
			$this->assertFalse($oa->additionalOaFee);
		}
		$this->assertSame($expected['conditions'], $oa->getPrintableConditions());
		$this->assertSame($expected['copyrightOwner'], $oa->getPrintableCopyrightOwner());
		$this->assertSame($expected['embargo'], $oa->getPrintableEmbargo());
		$this->assertSame($expected['licenses'], $oa->licenses);
		$this->assertSame($expected['locations'], $oa->getPrintableLocations(''));
	}

	public function publicationOaProvider()
	{
		return [
			[
				"0002-7294",
				0, // policy rank
				'submitted',
			 	0, // OA rank
				[
					'additionalOaFee' => false,
					'conditions' => [
						"La version publiée doit être citée avec la source originale",
						"Copyright must be acknowledged",
					],
					'copyrightOwner' => "",
					'embargo' => "Pas d'embargo",
					'licenses' => [],
					'locations' => [
						"Page personnelle de l'auteur",
						"Site web institutionnel",
						"Archive thématique non commerciale",
						"Archive d'articles prépubliés",
					],
				],
			],
			[
				"0002-7294",
				0, // policy rank
				'published',
			 	0,
				[
					'additionalOaFee' => true,
					'conditions' => [
						"La version publiée doit être citée avec la source originale",
					],
					'copyrightOwner' => "Auteur",
					'embargo' => "Pas d'embargo",
					'licenses' => ['cc_by' => '<a href="http://creativecommons.org/licenses/by/3.0/fr/" title="attribution">Creative Commons BY</a>'],
					'locations' => [
						"Tout site web",
						"Archive institutionnelle",
						"Archive spécifique : PubMed Central",
						"Archive thématique",
						// "Accès ouvert sur le site de l'éditeur", // not displayed when the URL is empty
					],
				],
			],
			/*
			[
				"0008-4239",
				0, // policy rank
				'submitted',
			 	0,
				[
					'additionalOaFee' => false,
					'conditions' => [
						"Upon acceptance for publication",
						"Publisher copyright and source must be acknowledged",
						"Must link to publisher version with DOI",
					],
					'copyrightOwner' => "",
					'embargo' => "",
					'licenses' => ['cc_by_nc_nd' => "CC BY-NC-ND"],
					'locations' => [
						"Page personnelle de l'auteur",
						"Archive institutionnelle",
						"Site web institutionnel",
						"Réseau social académique spécifique : ResearchGate ; Academia.edu",
						"Archive spécifique : PubMed Central ; Europe PMC ; arXiv",
						"Archive thématique non commerciale",
					],
				],
			],
			[
				"0008-4239",
				1, // policy rank
				'published',
			 	0,
				[
					'additionalOaFee' => true,
					'conditions' => [],
					'copyrightOwner' => "",
					'embargo' => "",
					'licenses' => [
						'cc_by' => "CC BY",
						'cc_by_nc' => "CC BY-NC",
						'cc_by_nc_nd' => "CC BY-NC-ND",
						'cc_by_nc_sa' => "CC BY-NC-SA",
					],
					'locations' => [
						"Archive spécifique : PubMed Central",
						"Accès ouvert sur le site de l'éditeur",
					],
				],
			],
			 */
		];
	}
}
