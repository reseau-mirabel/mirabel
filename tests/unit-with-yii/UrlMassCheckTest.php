<?php

use processes\urlverif\UrlMassCheck;

class UrlMassCheckTest extends \Codeception\Test\Unit
{
	public function testCheckLinks()
	{
		\Yii::app()->db->createCommand("DELETE FROM VerifUrl")->execute();

		// 17 URLs (15 distinct) related to the AJDA journal.
		$linksData = UrlMassCheck::extractLinks(Revue::model()->findAll('id = 1'));
		$this->assertCount(17, $linksData[0]['urls'], print_r($linksData, true));
		$this->assertCount(15, array_unique($linksData[0]['urls']));

		// 15 URLs are checked
		$checker = new UrlMassCheck(false);
		$checker->setCsvOutput(null);
		$checker->verbose = 0;
		$checker->linkChecker = $this->mockupLinkChecker();
		$checker->checkAllLinks("Revue", $linksData);
		$this->assertSame(
			15,
			(int) \Yii::app()->db
				->createCommand("SELECT count(*) FROM VerifUrl WHERE source = " . UrlMassCheck::SOURCES['Revue'])
				->queryScalar()
		);
		$this->assertSame(
			14,
			(int) \Yii::app()->db
				->createCommand("SELECT count(*) FROM VerifUrl WHERE success = 1 AND source = " . UrlMassCheck::SOURCES['Revue'])
				->queryScalar()
		);

		// Check those that were in error
		$this->assertSame(
			1,
			$checker->checkBrokenLinks("Revue")
		);
	}

	private function mockupLinkChecker(): \LinkChecker
	{
		return $this->make(\LinkChecker::class, [
			'checkLinks' => function (array $urls): array
				{
					$r = [];
					$first = true;
					foreach ($urls as $u) {
						if ($first) {
							$r[$u] = [0, 'not found'];
							$first = false;
						} elseif (!isset($r[$u])) {
							$r[$u] = [1, '']; // SUCCESS
						}
					}
					return $r;
				}
			]
		);
	}
}
