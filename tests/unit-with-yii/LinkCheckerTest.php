<?php

// When Yii is loaded, its autoloading completes composer's.
// Without Yii, we have to require the file in the global namespace.
require_once dirname(VENDOR_PATH) . '/src/www/protected/components/LinkChecker.php';

class LinkCheckerTest extends \Codeception\Test\Unit
{
	/**
	 * @dataProvider provideUrls
	 */
	public function testValidateUrlFormat($url, $expect)
	{
		$lc = new LinkChecker();
		LinkChecker::$skipDomains = ['linkedin.com'];
		LinkChecker::$forbidDomains = ['pas.ca'];
		LinkChecker::$forbidParams = ['jid', 'sessionid'];
		$result = $lc->validateUrlFormat($url);
		$this->assertEquals(
			$expect,
			$result,
			sprintf("`%s` should result in '%s', got '%s'.", $url, LinkChecker::DESCRIPTION[$expect], LinkChecker::DESCRIPTION[$result])
		);
	}

	public function provideUrls()
	{
		return [
			["", LinkChecker::RESULT_EMPTY],
			["https://tout.va.bien/ici", LinkChecker::RESULT_SUCCESS],
			["https://ok.com/linkedin.com/", LinkChecker::RESULT_SUCCESS],
			["https://ok.com/pas.ca/", LinkChecker::RESULT_SUCCESS],
			["www.sans.protocole.org", LinkChecker::RESULT_MALFORMED],
			["ftp://mauvais.protocole.org", LinkChecker::RESULT_MALFORMED],
			["https://example.org/\x00", LinkChecker::RESULT_MALFORMED],
			["https:///linkedin.com", LinkChecker::RESULT_MALFORMED],
			["https://linkedin.com/beurk", LinkChecker::RESULT_IGNORED],
			["https://avec.protocole.org.mais.via.un.proxy", LinkChecker::RESULT_FORBIDDEN],
			["https://avec.protocole.org.mais.via.un.proxy/aussi", LinkChecker::RESULT_FORBIDDEN],
			["https://pas.ca", LinkChecker::RESULT_FORBIDDEN],
			["https://pas.ca/non/plus", LinkChecker::RESULT_FORBIDDEN],
			["https://domaine.ok/page?jid", LinkChecker::RESULT_FORBIDDEN],
			["https://domaine.ok/?param=ok;sessionid=ABABA", LinkChecker::RESULT_FORBIDDEN],
			["https://domaine.ok/?param=ok&jid=123", LinkChecker::RESULT_FORBIDDEN],
			["https://domaine.ok/?param=ok&ok%20jid=123", LinkChecker::RESULT_SUCCESS],
			["https://domaine.ok/?search=jidisais", LinkChecker::RESULT_SUCCESS],
			["https://domaine.jido/tout/va/bien", LinkChecker::RESULT_SUCCESS],
		];
	}

	public function testCheckLink()
	{
		$lc = new LinkChecker();
		$lc->setTimeout(1);
		$this->assertEquals([LinkChecker::RESULT_MALFORMED, "format d'URL non valide"], $lc->checkLink("www.sans.protocole.org"));
		$this->assertEquals([LinkChecker::RESULT_EMPTY, "URL vide"], $lc->checkLink(""));
	}

	public function testGetStats()
	{
		$lc = new LinkChecker();
		$lc->setTimeout(1);
		LinkChecker::resetStats();
		$this->assertEquals([], $lc->checkLinks([]));
		$this->assertEquals(
			[
				"ftp://no.way" => [LinkChecker::RESULT_MALFORMED, "format d'URL non valide"],
				"bonjour" => [LinkChecker::RESULT_MALFORMED, "format d'URL non valide"],
			],
			$lc->checkLinks(["ftp://no.way", "bonjour"])
		);
		$this->assertEquals(
			[
				'urlsNum' => 0,
				'batches' => 0,
				'answersNum' => 0,
				'overallTime' => 0,
				'slowChecks' => 0,
				'errorsNum' => 0,
			],
			LinkChecker::getStats(),
			"No network request should be sent."
		);
	}
}
