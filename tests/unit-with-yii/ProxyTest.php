<?php

class ProxyTest extends \Codeception\Test\Unit
{
	public function testProxyfyUrl()
	{
		$url = 'https://www.cairn.info/revue-actes-de-la-recherche-en-sciences-sociales.htm';
		$this->assertSame(
			'https://portail.marseille.archi.fr/https%3A%2F%2Fwww.cairn.info%2Frevue-actes-de-la-recherche-en-sciences-sociales.htm',
			Proxy::proxifyUrl($url, 'https://portail.marseille.archi.fr/URL')
		);
		$this->assertSame(
			'https://portail.marseille.archi.fr/https/www.cairn.info/revue-actes-de-la-recherche-en-sciences-sociales.htm',
			Proxy::proxifyUrl($url, 'https://portail.marseille.archi.fr/GPURL')
		);
	}
}
