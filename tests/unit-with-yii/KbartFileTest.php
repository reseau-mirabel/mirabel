<?php

use processes\kbart\Logger;
use processes\kbart\read\File;

class KbartFileTest extends \Codeception\Test\Unit
{
	public function testreadAll()
	{
		$dir = dirname(__DIR__) . '/_data';
		$filename = "kbart_2rows.txt";
		$filepath = "$dir/$filename";

		$logger = new Logger();

		$reader = new File($logger);
		$reader->load($filepath);
		$data = $reader->readAll(new processes\kbart\data\Filter());

		$rows = $data->getRows();
		$this->assertCount(2, $rows);
		$this->assertEquals("Journal of Nutritional Science", $rows[3]->title);

		$log = $logger->getRowLog(3);
		$this->assertEquals("warning", $log->getMaxlevel());

		$events = $log->getEvents();
		$this->assertCount(1, $events);
		$this->assertSame(processes\kbart\logger\LogLevel::Warning->value, $events[0]->level);
		$this->assertStringEndsWith("'2012' sera utilisé.", $events[0]->message);
	}
}
