<?php

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class FunctionalTester extends \Codeception\Actor
{
	use _generated\FunctionalTesterActions;

	public function loginAs($username, $password = "mdp")
	{
		$this->amOnPage('/site/login');
		$this->fillField("Identifiant", $username);
		$this->fillField("Mot de passe", $password);
		$this->click("Se connecter");
		$this->assertEquals($username, \Yii::app()->user->getState('login'), "Login with '$username' failed.");
		$this->dontSeeElement('.alert-error');
	}

	public function dontSeeFlashMessage(string $status)
	{
		$this->assertFalse(\Yii::app()->user->hasFlash($status), "A flash message of type '$status' was NOT expected.\n");
	}

	public function seeFlashMessage(string $status, string $message = '')
	{
		$this->assertTrue(
			\Yii::app()->user->hasFlash($status),
			"A flash message of type '$status' was expected among flash messages:\n"
				. print_r(\Yii::app()->user->getFlashes(false), true)
				. "\n"
		);
		if ($message) {
			$this->assertStringContainsString($message, \Yii::app()->user->getFlash($status, '', false));
		}
	}
}
