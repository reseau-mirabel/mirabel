<?php

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class UnitTester extends \Codeception\Actor
{
    use _generated\UnitTesterActions;

	/**
	 * Compare arrays
	 * - recursively,
	 * - on values and their types,
	 * - ignoring the order of keys.
	 */
	public function assertSameArray($expected, $actual, string $msg = ''): void
	{
		$copyExpected = $expected;
		ksort_recursive($copyExpected);
		$copyActual = $actual;
		ksort_recursive($copyActual);
		$this->assertSame($copyExpected, $copyActual, $msg);
	}
}

function ksort_recursive(&$array)
{
    if (is_array($array) && !isset($array[0])) {
        ksort($array);
        array_walk($array, 'ksort_recursive');
    }
}
