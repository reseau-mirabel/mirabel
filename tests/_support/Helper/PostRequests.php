<?php

namespace Helper;

/**
 * There is no default way to emulate a post form when some fields are dynamic (JS).
 * sendAjaxPostRequest() does not load the response into the current page.
 * This Helper provides a way to send an arbitrary POST request and read the response.
 *
 * Enable this in "functional.suite.yml" by adding `- \Helper\PostRequests` under "modules / enabled".
 */
class PostRequests extends \Codeception\Module
{
	public function sendPostRequest(string $url, array $data)
	{
		$this->getModule('Yii1')->_loadPage('POST', $url, $data);
	}
}
