<?php

namespace tests\unit\kbart;

use processes\kbart\Normalize;

class NormalizeTest extends \Codeception\Test\Unit
{
	private const TIME = "2024-05-06 12:00:00";

	public function testConvKbartToDateEmpty()
	{
		$time = strtotime(self::TIME);
		$this->expectException(\Exception::class);
		Normalize::convKbartToDate("P1C", $time);
	}

	/**
	 * @dataProvider provideEmbargos
	 */
	public function testConvKbartToDate(array $expected, string $input)
	{
		$time = strtotime(self::TIME);
		$result = Normalize::convKbartToDate($input, $time);
		$this->assertIsArray($result);
		$this->assertArrayHasKey('info', $result);
		$this->assertSame($expected['date'], $result['date']);
		$this->assertSame($expected['fulldate'], $result['fulldate']);
		$this->assertSame($expected['type'], $result['type']);
		$this->assertCount(4, $result);
	}

	public function provideEmbargos(): array
	{
		return [
			[
				['date' => '', 'fulldate' => '', 'type' => ''],
				'input' => '',
			],
			// année calendaire
			[
				['date' => '2023', 'fulldate' => '2023-12-31', 'type' => 'P'],
				'input' => 'P1Y',
			],
			[
				['date' => '2024', 'fulldate' => '2024-01-01', 'type' => 'R'],
				'input' => 'R1Y',
			],
			// 4 mois calendaires
			[
				['date' => '2024-01', 'fulldate' => '2024-01-30', 'type' => 'P'],
				'input' => 'P4M',
			],
			[
				['date' => '2024-02', 'fulldate' => '2024-02-01', 'type' => 'R'],
				'input' => 'R4M',
			],
			// année glissante
			[
				['date' => '2023-05-06', 'fulldate' => '2023-05-06', 'type' => 'P'],
				'input' => 'P365D',
			],
			[
				['date' => '2023-05-06', 'fulldate' => '2023-05-06', 'type' => 'R'],
				'input' => 'R365D',
			],
			// 4 mois glissants
			[
				['date' => '2024-01-06', 'fulldate' => '2024-01-06', 'type' => 'P'],
				'input' => 'P120D',
			],
		];
	}

	/**
	 * @dataProvider provideRomanNumbers
	 */
	public function testArabizeRomandNumbers(?int $expected, string $input)
	{
		$this->assertSame($expected, Normalize::arabizeRomanNumber($input));
	}

	public function provideRomanNumbers(): array
	{
		return [
			[1949, 'MCMXLIX'],
			[1949, 'MDCCCCXXXXVIIII'],
			[null, 'MDR'],
		];
	}
}
