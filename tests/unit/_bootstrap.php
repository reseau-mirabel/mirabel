<?php

/**
 * This is the bootstrap file for unit tests.
 */

ini_set('date.timezone', 'Europe/Paris');

defined('YII_ENV') or define('YII_ENV', 'test');
defined('YII_TEST') or define('YII_TEST', 1);
defined('VENDOR_PATH') or define('VENDOR_PATH', dirname(__DIR__, 2) . '/vendor');
defined('WEBROOT') or define('WEBROOT', dirname(__DIR__, 2) . '/src/www');

// Composer autoload
require_once VENDOR_PATH . '/autoload.php';

// Yii autoload with lowest priority
spl_autoload_register(array('YiiBase','autoload'));

// Load the Yii class (when not loaded from the framework with YiiBase)
require_once WEBROOT . '/protected/components/Yii.php';

$mainConfig = require WEBROOT . '/protected/config/main.php';
$localConfig = require WEBROOT . '/protected/config/local.php';
$testConfig = require WEBROOT . '/protected/config/test.php';
$config = array_merge_recursive($mainConfig, $localConfig, $testConfig);
unset($mainConfig, $localConfig, $testConfig);
$config['basePath'] = WEBROOT . '/protected';
$config['components']['request'] = [
	'class' => \Codeception\Lib\HttpRequest::class,
];

$_SERVER['SERVER_NAME'] = 'localhost';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

Yii::createWebApplication($config);

