<?php

class ImageResizerTest extends \Codeception\Test\Unit
{
	public function testResizeJpeg()
	{
		$filename = "logo-mirabel-gd";
		$basePath = dirname(__DIR__); // .../tests/
		$input = dirname($basePath) . "/src/www/images/$filename.jpg";
		$output = "$basePath/_output/$filename.png";
		if (is_file($output)) {
			unlink($output);
		}

		$this->assertFalse(is_file($output));
		$process = new processes\images\Resizer("$basePath/_output", "$basePath/_output/raw");
		$process->boundingbox = "64x64";
		$this->assertSame("vignette créée", $process->resizeFile($input));
		$this->assertTrue(is_file($output));
		if (function_exists('getimagesize')) { // requires 'gd' extension
			[$width, $height, ] = getimagesize($output);
			$this->assertSame([64, 30], [$width, $height]);
		}
		$this->assertSame("vignette à jour", $process->resizeFile($input));
	}

	public function testResizePng()
	{
		$filename = "blue.png";
		$basePath = dirname(__DIR__);
		$input = "$basePath/_data/images/$filename";
		$output = "$basePath/_output/$filename";
		if (is_file($output)) {
			unlink($output);
		}

		$this->assertFalse(is_file($output));
		$process = new processes\images\Resizer("$basePath/_output", "$basePath/_output/raw");
		$process->boundingbox = "64x64";
		$this->assertSame("vignette créée", $process->resizeFile($input));
		$this->assertTrue(is_file($output));
		if (function_exists('getimagesize')) { // requires 'gd' extension
			[$width, $height, ] = getimagesize($output);
			$this->assertSame([64, 64], [$width, $height]);
		}
		$this->assertSame("vignette à jour", $process->resizeFile($input));
	}
}
