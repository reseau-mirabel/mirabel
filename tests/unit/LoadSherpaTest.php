<?php

use processes\politique\LoadSherpa;

class LoadSherpaTest extends \Codeception\Test\Unit
{
	/**
	 * @dataProvider policyProvider
	 */
	public function testCleanupSherpaPolicy(string $json, \stdClass $expected)
	{
		$policy = json_decode($json, false);
		$result = LoadSherpa::cleanupSherpaPolicy($policy);
		$this->assertEquals($expected, $result);
	}

	public function policyProvider()
	{
		return [
			[
				// json input
				<<<EOJSON
{
    "id": 3284,
    "internal_moniker": "Default Policy",
    "open_access_prohibited": "no",
    "permitted_oa": [
        {
            "additional_oa_fee": "no",
            "article_version": [
                "submitted"
            ],
            "location": {
				"location_phrases": [
                    {
                        "value": "this_journal",
                        "language": "en",
                        "phrase": "Journal Website"
                    }
                ],
                "location": [
                    "named_repository",
                    "preprint_repository"
                ],
                "named_repository": [
                    "arXiv",
                    "bioRxiv",
                    "Peer J PrePrints"
                ]
            }
        },
        {
            "additional_oa_fee": "no",
            "article_version": [
                "accepted",
                "published"
            ],
            "conditions": [
                "Published source must be acknowledged",
                "Copy of License must accompany any deposit.",
                "Must link to publisher version with DOI"
            ],
            "copyright_owner": "authors",
			"copyright_owner_phrases": [
				{
					"phrase": "Authors",
					"language": "en",
					"value": "authors"
				}
			],
            "license": [
                {
                    "license": "cc_by"
                }
            ],
            "location": {
                "location": [
                    "any_website",
                    "named_repository",
                    "this_journal"
                ],
                "named_repository": [
                    "PubMed Central"
                ]
            },
            "publisher_deposit": [
                {
                    "repository_metadata": {
                        "description": "A subject-based repository of biomedical and life sciences journal literature developed and managed by the National Center for Biotechnology Information (NCBI) at the US National Library of Medicine (NLM). Content includes articles deposited by participating journals that have applied to and been selected for the archive by NLM, as well as individual author manuscripts that have been submitted in compliance with the NIH Public Access Policy and similar policies of other research funding agencies. More than 2000 journals currently use PMC as a repository. Digitization projects have also added content from the 18th, 19th, and 20th centuries to the archive.",
                        "name": [
                            {
                                "language": "en",
                                "name": "PubMed Central",
                                "preferred": "name"
                            }
                        ],
                        "type": "disciplinary",
                        "url": "http://www.ncbi.nlm.nih.gov/pmc/"
                    },
                    "system_metadata": {
                        "id": 267,
                        "uri": "https://v2.sherpa.ac.uk/id/repository/267"
                    }
                },
                {
                    "repository_metadata": {
                        "description": "This is a subject based repository, that mirrors the data held on the PubMed Central site. Since 2010, the service diversified from PMC and introduced additional content including PubMed abstracts and biological patents. In addition this site acts as an Open Access repository for peer-reviewed research from researchers who have been funded by the Europe PMC Funders Group - submitted via the Europe PMC Manuscript Submission System A range of supporting guidance and background information are available on this site.",
                        "name": [
                            {
                                "language": "en",
                                "name": "Europe PubMed Central",
                                "preferred": "name"
                            }
                        ],
                        "notes": "Launched as UK PubMed Central (UKPMC) in January 2007, changed to Europe PubMed Central in November 2012.Special item types include: Links",
                        "type": "disciplinary",
                        "url": "http://europepmc.org/"
                    },
                    "system_metadata": {
                        "id": 908,
                        "uri": "https://v2.sherpa.ac.uk/id/repository/908"
                    }
                }
            ]
        }
    ],
    "publication_count": 232,
    "uri": "https://v2.sherpa.ac.uk/id/publisher_policy/3284",
    "urls": [
        {
            "description": "Duplicate publication",
            "url": "https://www.springeropen.com/get-published/editorial-policies#duplicate+publication"
        },
        {
            "description": "Journal Policies",
            "url": "http://www.springernature.com/gp/open-research/policies/journal-policies"
        }
    ]
}
EOJSON,
				// expected output
				(object) [
					"internal_moniker" => "Default Policy",
				    "open_access_prohibited" => "no",
					"permitted_oa" => [
						(object) [
				            "additional_oa_fee" => "no",
							"article_version" => [
								"submitted"
							],
							"location" => (object) [
								"location" => [
									"named_repository",
									"preprint_repository"
								],
								"named_repository" => [
									"arXiv",
									"bioRxiv",
									"Peer J PrePrints"
								],
							],
						],
						(object) [
							"additional_oa_fee" => "no",
							"article_version" => [
								"accepted",
								"published"
							],
							"conditions" => [
								"Published source must be acknowledged",
								"Copy of License must accompany any deposit.",
								"Must link to publisher version with DOI"
							],
							"copyright_owner" => "authors",
							"license" => [
								(object) [
									"license" => "cc_by"
								]
							],
							"location" => (object) [
								"location" => [
									"any_website",
									"named_repository",
									"this_journal"
								],
								"named_repository" => [
									"PubMed Central"
								]
							],
						]
					],
					"urls" => [
						(object) [
							"description" => "Duplicate publication",
							"url" => "https://www.springeropen.com/get-published/editorial-policies#duplicate+publication"
						],
						(object) [
							"description" => "Journal Policies",
							"url" => "http://www.springernature.com/gp/open-research/policies/journal-policies"
						],
					],
				],
			], // end of first sample
		];
	}
}
