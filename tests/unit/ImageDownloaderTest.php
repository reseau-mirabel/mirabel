<?php

class ImageDownloaderTest extends \Codeception\Test\Unit
{
	public function testResizeJpeg()
	{
		$basePath = dirname(__DIR__); // .../tests/
		$process = new \processes\images\Downloader("$basePath/_output", $basePath);
		$this->assertInstanceof(\processes\images\Downloader::class, $process);
	}
}
