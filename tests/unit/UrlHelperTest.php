<?php

use components\UrlHelper;

class UrlHelperTest extends \Codeception\Test\Unit
{
	/**
	 * @dataProvider provideDomainData
	 */
	public function testDomain($url, $expected)
	{
		$this->assertSame($expected, UrlHelper::extractDomain($url));
	}

	public function provideDomainData()
	{
		return [
			['/revue/3', ''],
			['https://reseau-mirabel.info/revue/3', 'reseau-mirabel.info'],
			['https://en.wikipedia.org/wiki/Top-level_domain', 'wikipedia.org'],
			['https://www.php.net/manual/', 'php.net'],
			['http://sure.i.like.it', 'like.it'],
			['//going.down/…', ''],
			['wrong.name', ''],
		];
	}

	/**
	 * @dataProvider provideAllDomainsData
	 */
	public function testAllDomains($url, $expected)
	{
		$this->assertSame($expected, UrlHelper::extractAllDomains($url));
	}

	public function provideAllDomainsData()
	{
		return [
			['/revue/3', []],
			['https://reseau-mirabel.info/revue/3', ['reseau-mirabel.info']],
			['https://en.wikipedia.org/wiki/Top-level_domain', ['en.wikipedia.org', 'wikipedia.org']],
			['https://www.php.net/manual/', ['www.php.net', 'php.net']],
			['http://sure.i.like.it', ['sure.i.like.it', 'i.like.it', 'like.it']],
			['//going.down/…', []],
		];
	}
}
