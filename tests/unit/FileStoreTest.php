<?php

use components\FileStore;

class FileStoreTest extends \Codeception\Test\Unit
{
	public function testProcess()
	{
		$tmpPath = tempnam(sys_get_temp_dir(), 'Mirabel_FileStoreTest_');
		$contents = "Some junk";
		file_put_contents($tmpPath, $contents);

		$store = new FileStore("unit-test");
		$rootDir = sys_get_temp_dir() . '/' . uniqid('MirabelFS_');
		if (!is_dir($rootDir)) {
			mkdir($rootDir);
		}
		$store->setRootPath($rootDir);

		$key = "mon nom à moi";
		$this->assertFalse($store->exists($key));
		$this->assertTrue($store->set($key, $tmpPath));
		$this->asserttrue($store->exists($key));
		$this->assertFalse(file_exists($tmpPath));

		$storedPath = $store->get($key);
		$this->assertSame($contents, file_get_contents($storedPath));

		$store->flush();
		$this->assertFalse($store->exists($key));
	}
}
