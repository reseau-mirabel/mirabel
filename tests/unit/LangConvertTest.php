<?php

use models\lang\Convert;

class LangConvertTest extends \Codeception\Test\Unit
{
	/**
	 * @dataProvider providesLangs
	 */
	public function testLists($expect, $from)
	{
		$a = preg_split('/\s*[ ,]\s*/', $from);
		$this->assertEquals($expect, Convert::codesToFullNames($a));
	}

	public function providesLangs()
	{
		return [
			["français", "fre"],
			["français", "fra "],
			["français", " fra , "],
			["anglais, français, espagnol", "eng,fra,  spa "],
			["anglais, français, espagnol", "en fr, esp, nawak"],
		];
	}
}
