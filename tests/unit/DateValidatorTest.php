<?php

use models\validators\DateValidator;

class DateVersatileTest extends \Codeception\Test\Unit
{
	/**
	 * @dataProvider providesDates
	 */
	public function testConvertdateToTs($date, $endDate, $timestamp)
	{
		$this->assertEquals($timestamp, DateValidator::convertDateToTs($date, $endDate));
	}

	public function providesDates()
	{
		return array(
			array('2001', false, mktime(0, 0, 0, 1, 1, 2001)),
			array('2001', true, mktime(24, 0, 0, 12, 31, 2001)),
			array('2001-06', false, mktime(0, 0, 0, 6, 1, 2001)),
			array('2001-06', true, mktime(24, 0, 0, 6, 30, 2001)),
			array('2001-05', true, mktime(24, 0, 0, 5, 31, 2001)),
			array('2011-02', true, mktime(24, 0, 0, 2, 28, 2011)),
			array('2012-02', true, mktime(24, 0, 0, 2, 29, 2012)),
			array('02/2012', true, mktime(24, 0, 0, 2, 29, 2012)),
			array('2001-07-20', false, mktime(0, 0, 0, 7, 20, 2001)),
			array('2001-07-20', true, mktime(24, 0, 0, 7, 20, 2001)),
			array('20/07/2001', true, mktime(24, 0, 0, 7, 20, 2001)),
		);
	}

	/**
	 * @dataProvider providesConditions
	 */
	public function testBuildTsCondition(string $expectedCondition, string $colname, string $input)
	{
		$this->assertSame($expectedCondition, DateValidator::buildTsConditionFromDateValue($colname, $input));
	}

	public function providesConditions(): array
	{
		return [
			["", "whatever", ""],
			["mydate BETWEEN 1585692000 AND 1588284000", "mydate", "2020-04"],
			["mydate BETWEEN 1588024800 AND 1588111200", "mydate", "2020-04-28"],
			["mydate >= 1585692000", "mydate", ">= 2020-04"],
			["mydate > 1588284000", "mydate", "> 2020-04"],
			["mydate <= 1588284000", "mydate", "<= 2020-04"],
			["mydate NOT BETWEEN 1588024800 AND 1588111200", "mydate", "!=2020-04-28"],
			["0", "mydate", "tralala"],
		];
	}
}
