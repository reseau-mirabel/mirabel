<?php

namespace tests\unit\idref;

use models\marcxml\MarcEditeur;
use models\marcxml\Parser;
use processes\idref\Notice;

class NoticeTest extends \Codeception\Test\Unit
{
	public function testFromMarcxml()
	{
		$parser = new Parser();
		$xml = file_get_contents(codecept_data_dir('idref/026485842.xml'));
		$marc = $parser->parse($xml, new MarcEditeur());

		$notice = new Notice();
		$notice->fromMarcXml($marc);

		$this->assertSame("1845", $notice->dateDebut);
		$this->assertSame("", $notice->dateFin);
		$this->assertSame("FR", $notice->pays);
		$this->assertSame(Notice::TYPE_COLLECTIVITE, $notice->type);
		$this->assertCount(0, $notice->getErrors());
	}
}
