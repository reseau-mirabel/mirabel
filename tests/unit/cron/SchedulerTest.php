<?php

namespace tests\unit\cron;

use DateTimeImmutable;
use processes\cron\Schedule;
use processes\cron\Scheduler;

class SchedulerTest extends \Codeception\Test\Unit
{
	const STARTING_DATE = '2020-04-18 12:06:44'; // saturday

	/**
	 * Check that the $num next runs (starting at a fixed date) of the schedule in $json are $datetimes.
	 *
	 * @dataProvider provideSchedules
	 */
	public function testListNextRuns(array $datetimes, string $json, int $num)
	{
		$scheduler = new Scheduler();
		$scheduler->start = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', self::STARTING_DATE);
		$schedule = Schedule::fromJson($json);

		$expect = [];
		foreach ($datetimes as $dt) {
			$expect[] = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $dt);
		}

		$this->assertEquals($expect, $scheduler->listNextRuns($schedule, $num));
	}

	public function provideSchedules(): array
	{
		return [
			[
				['2020-04-18 17:01:30', '2020-04-19 17:01:30'],
				'{"periodicity": "daily", "hours": ["17:01"], "days": [0,1,2,3,4,5,6]}',
				2,
			],
			[
				['2020-04-21 07:01:30', '2020-04-25 07:01:30', '2020-04-28 07:01:30', '2020-05-02 07:01:30'],
				'{"periodicity": "daily", "hours": ["07:01"], "days": [2,6]}',
				4,
			],
			[
				['2020-04-18 17:01:30', '2020-04-19 07:01:30', '2020-04-19 17:01:30'],
				'{"periodicity": "daily", "hours": ["07:01","17:01"], "days": [0,1,2,3,4,5,6]}',
				3,
			],
			[
				['2020-05-01 07:01:30'],
				'{"periodicity": "monthly", "hours": ["07:01", "19:01"], "days": [1]}',
				1,
			],
			[
				['2020-05-01 07:01:30', '2020-05-15 07:01:30', '2020-06-01 07:01:30'],
				'{"periodicity": "monthly", "hours": ["07:01"], "days": [1, 15]}',
				3,
			],
			[
				['2020-04-18 12:13:30', '2020-04-18 13:13:30'],
				'{"periodicity": "hourly", "hours": ["13"], "days": []}',
				2
			],
			[
				['2020-04-18 13:05:30', '2020-04-18 14:05:30'],
				'{"periodicity": "hourly", "hours": ["05"], "days": []}',
				2
			]
		];
	}

	/**
	 * @dataProvider provideSchedulesUntil
	 */
	public function testListNextRunsUntil(array $datetimes, string $json, string $endTime)
	{
		$scheduler = new Scheduler();
		$scheduler->start = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', self::STARTING_DATE);
		$schedule = Schedule::fromJson($json);

		$expect = [];
		foreach ($datetimes as $dt) {
			$expect[] = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $dt);
		}
		$end = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $endTime);

		$this->assertEquals($expect, $scheduler->listNextRunsUntil($schedule, $end));
	}

	public function provideSchedulesUntil(): array
	{
		return [
			[
				['2020-04-18 17:01:30', '2020-04-19 17:01:30'],
				'{"periodicity": "daily", "hours": ["17:01"], "days": [0,1,2,3,4,5,6]}',
				'2020-04-20 00:00:00',
			],
			[
				['2020-04-21 07:01:30', '2020-04-25 07:01:30', '2020-04-28 07:01:30', '2020-05-02 07:01:30'],
				'{"periodicity": "daily", "hours": ["07:01"], "days": [2,6]}',
				'2020-05-04 00:00:00',
			],
			[
				['2020-05-01 07:01:30', '2020-06-01 07:01:30'],
				'{"periodicity": "monthly", "hours": ["07:01"], "days": [1]}',
				'2020-07-01 00:00:00',
			],
			[
				['2020-04-18 13:03:30', '2020-04-18 14:03:30'],
				'{"periodicity": "hourly", "hours": ["03"], "days": []}',
				'2020-04-18 15:00:00'
			],
		];
	}

	/**
	 * @dataProvider provideSchedulesToRun
	 */
	public function testIsScheduledToRunNow(bool $expected, string $json)
	{
		$scheduler = new Scheduler();
		$scheduler->start = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', '2020-04-18 12:06:44'); // saturday
		$schedule = Schedule::fromJson($json);
		$this->assertSame($expected, $scheduler->isScheduledToRunNow($schedule, 300));
	}

	public function provideSchedulesToRun(): array
	{
		return [
			[false, '{"periodicity": "daily", "hours": ["13:01"], "days": [0,1,2,3,4,5,6]}'],
			[true, '{"periodicity": "daily", "hours": ["12:10", "19:10"], "days": [6]}']
		];
	}
}
