<?php

namespace tests\unit\cron;

use processes\cron\Schedule;

class ScheduleTest extends \Codeception\Test\Unit
{
	/**
	 * @dataProvider provideScheduleErrors
	 */
	public function testGetErrors(string $expected, string $json)
	{
		$schedule = Schedule::fromJson($json);
		$this->assertSame($expected, $schedule->getError(), $json);
	}

	public function provideScheduleErrors(): array
	{
		return [
			[
				"",
				'{"periodicity": "daily", "hours": ["17:01"], "days": [0,1,2,3,4,5,6]}',
			],
			[
				"",
				'{"periodicity": "monthly", "hours": ["07:01"], "days": [1, 15]}',
			],
			[
				"La périodicité choisie n'est pas une des valeurs prévues.",
				'{"periodicity": "weekly", "hours": ["17:01"], "days": [1,5]}',
			],
			[
				"Il faut déclarer au moins un horaire.",
				'{"periodicity": "daily", "hours": [], "days": [1,5]}',
			],
			[
				"Chaque horaire doit être entre 00:00 et 23:59 (avec 4 chiffres en tout).",
				'{"periodicity": "daily", "hours": ["7:01"], "days": [1, 15]}',
			],
			[
				"Valeur incorrecte pour un jour de la semaine.",
				'{"periodicity": "daily", "hours": ["17:01"], "days": [1, 15]}',
			],
			[
				"Choisir au moins un jour de la semaine.",
				'{"periodicity": "daily", "hours": ["17:01"], "days": []}',
			],
			[
				"Chaque horaire doit être entre 00:00 et 23:59 (avec 4 chiffres en tout).",
				'{"periodicity": "monthly", "hours": ["27:01"], "days": [1, 15]}',
			],
			[
				"Valeur incorrecte pour un jour du mois.",
				'{"periodicity": "monthly", "hours": ["07:01"], "days": [-1]}',
			],
		];
	}

	/**
	 * @dataProvider provideScheduleStrings
	 */
	public function testToString(string $expected, string $json)
	{
		$schedule = Schedule::fromJson($json);
		$this->assertSame($expected, $schedule->toString(), $json);
	}

	public function provideScheduleStrings(): array
	{
		return [
			[
				"Chaque jour, à 17:01",
				'{"periodicity": "daily", "hours": ["17:01"], "days": [0,1,2,3,4,5,6]}',
			],
			[
				"Chaque mois, les jours 1, 15, à 07:01",
				'{"periodicity": "monthly", "hours": ["07:01"], "days": [1, 15]}',
			],
			[
				"Chaque mardi, jeudi, à 07:01, 14:44",
				'{"periodicity": "daily", "hours": ["07:01", "14:44"], "days": [2,4]}',
			],
			[
				"Chaque heure, à la minute 24",
				'{"periodicity": "hourly", "hours": ["24"], "days": [2,4]}',
			],
		];
	}
}
