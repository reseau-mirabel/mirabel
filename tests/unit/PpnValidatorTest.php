<?php

use models\validators\PpnValidator as V;

class PpnValidatorTest extends \Codeception\Test\Unit
{
	public function testCheckKey()
	{
		$this->assertTrue(V::check("029883091"));
		$this->assertTrue(V::check("035206896"));
		$this->assertTrue(V::check("194736725"));
		$this->assertTrue(V::check("194737675"));
		$this->assertTrue(V::check("056568983"));
		$this->assertTrue(V::check("02662673X"));
		$this->assertTrue(V::check("254427677"));
		$this->assertTrue(V::check("20400215X"));
		$this->assertFalse(V::check("20400215"));
		$this->assertFalse(V::check("204002159"));
		$this->assertFalse(V::check("086040921"));
		$this->assertFalse(V::check("0860409212"));
		$this->assertFalse(V::check("05656-8983"));
		$this->assertFalse(V::check("/056568983"));
	}
}
