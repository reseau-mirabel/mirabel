<?php

use components\DateTimeHelper;

class DateTimeHelperTest extends \Codeception\Test\Unit
{
	/**
	 * @dataProvider provideDateTime
	 */
	public function testToRelativeFr($expected, $input)
	{
		$from = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', '2020-01-02 06:00:00');
		$to = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $input);
		$this->assertSame($expected, DateTimeHelper::diffToRelativeFr($from, $to));
	}

	public function provideDateTime(): array
	{
		return [
			["dans 3 minutes", "2020-01-02 06:03:30"],
			["dans 3 heures", "2020-01-02 09:40:30"],
			["aujourd'hui à 22:44", "2020-01-02 22:44:30"],
			["demain à 22:44", "2020-01-03 22:44:30"],
			["dans 2 jours", "2020-01-04 02:10:30"],
			["dans 14 jours", "2020-01-16 06:00:00"],
			["hier à 02:44", "2020-01-01 02:44:30"],
			["il y a 2 jours", "2019-12-31 22:44:30"],
			["il y a 32 jours", "2019-12-01 06:00:00"],
		];
	}

	/**
	 * @dataProvider provideIntervals
	 */
	public function testParseDateInterval($interval, $expected)
	{
		$this->assertEquals($expected, DateTimeHelper::parseDateInterval($interval), "Parsed '$interval'");
	}

	public function provideIntervals(): array
	{
		return array(
			// year
			array('2001', [mktime(0, 0, 0, 1, 1, 2001), mktime(0, 0, 0, 1, 1, 2002)]),
			array('20012', null),
			array('< 2001', [0, mktime(0, 0, 0, 1, 1, 2002)]),
			array('> 2001', [mktime(0, 0, 0, 1, 1, 2001), 0]),
			array('< 20012', null),
			// month
			array('2001-06', [mktime(0, 0, 0, 6, 1, 2001), mktime(0, 0, 0, 7, 1, 2001)]),
			array('2001-060', null),
			array('<2001-06', [0, mktime(0, 0, 0, 7, 1, 2001)]),
			array('>2001-06', [mktime(0, 0, 0, 6, 1, 2001), 0]),
			array('02/2012', [mktime(0, 0, 0, 2, 1, 2012), mktime(0, 0, 0, 3, 1, 2012)]),
			array('>02/2012', [mktime(0, 0, 0, 2, 1, 2012), 0]),
			// day
			array('2001-07-20', [mktime(0, 0, 0, 7, 20, 2001), mktime(24, 0, 0, 7, 20, 2001)]),
			array('20/07/2001', [mktime(0, 0, 0, 7, 20, 2001), mktime(24, 0, 0, 7, 20, 2001)]),
			array('20/07/2001 2005-05-17', [mktime(0, 0, 0, 7, 20, 2001), mktime(24, 0, 0, 5, 17, 2005)]),
		);
	}
}
