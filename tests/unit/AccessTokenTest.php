<?php

use components\AccessToken;

class AccessTokenTest extends \Codeception\Test\Unit
{
	public function testExpiration()
	{
		$path = 'some/path';
		$now = time();
		$tokenizers = [
			new AccessToken($path, $now),
			new AccessToken($path, $now + 1000),
			new AccessToken($path, $now + 5000),
		];
		$tokens = [];
		foreach ($tokenizers as $t) {
			$tokens[] = $t->createToken();
		}

		// Tokens in the future are valid
		$ok = AccessToken::RES_OK;
		$this->assertSame($ok, $tokenizers[0]->checkToken($tokens[1]));
		$this->assertSame($ok, $tokenizers[0]->checkToken($tokens[2]));
		$this->assertSame($ok, $tokenizers[1]->checkToken($tokens[2]));

		$expired = AccessToken::RES_EXPIRED;
		$this->assertSame($expired, $tokenizers[1]->checkToken($tokens[0]));
		$this->assertSame($expired, $tokenizers[2]->checkToken($tokens[0]));
		$this->assertSame($expired, $tokenizers[2]->checkToken($tokens[1]));

		$this->assertSame($now + 5000, AccessToken::getExpiration($tokens[2]));
	}

	public function testPath()
	{
		$path = 'some/path$';
		$now = time() + 1000;
		$token = (new AccessToken($path, $now))->createToken();

		$this->assertSame(AccessToken::RES_OK, (new AccessToken('some/path'))->checkToken($token));
		$this->assertSame(AccessToken::RES_NONE, (new AccessToken('another/path'))->checkToken($token));
		$this->assertSame(AccessToken::RES_NONE, (new AccessToken('some/path-to-somewhere'))->checkToken($token));
	}
}
