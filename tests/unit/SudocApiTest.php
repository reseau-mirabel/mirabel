<?php

use models\sudoc\ApiClient;
use models\sudoc\Notice;
use models\sudoc\Ppn;

class SudocApiTest extends \Codeception\Test\Unit
{
	public function testGetNoticeByIssn()
	{
		// stub-mockup for Curl
		$curl = $this->createStub(\components\Curl::class);
		$curl->method('get')
			->will($this->returnSelf());
		$curl->method('getContent')
			->will($this->onConsecutiveCalls(
				// issn2ppn
				file_get_contents(codecept_data_dir("/sudoc-issn2ppn/2948-040X.xml")),
				// marcxml
				file_get_contents(codecept_data_dir("/sudoc-marcxml/262821222.xml")),
			));

		// Yii1 models are bad at autoloading outside of the full framework
		require_once __DIR__ . '/../../src/www/protected/models/Issn.php';

		$api = new ApiClient($curl);
		$issn = '2948-040X';
		$notice = $api->getNoticeByIssn($issn);
		$this->assertInstanceof(Notice::class, $notice);
		$this->assertSame($issn, $notice->issn);
		$this->assertSame('262821222', $notice->ppn);
		$this->assertSame("ʿAtiqot", $notice->titre);
		$this->assertSame(\Issn::SUPPORT_ELECTRONIQUE, $notice->support);
	}

	/**
	 * Test ApiClient::extractPpn().
	 *
	 * @dataProvider provideXmlResponses
	 */
	public function testExtractPpn($ppn, $xml)
	{
		$api = new ApiClient();
		$expected = [new Ppn($ppn[0], $ppn[1])];
		$this->assertEquals($expected, self::callProtectedMethod($api, "extractPpn", [$xml]));
	}

	public function provideXmlResponses(): array
	{
		$dataDir = dirname(__DIR__) . '/_data/sudoc-issn2ppn';
		return [
			[["03615377X", false], file_get_contents("$dataDir/0001-026X.xml")],
			[["195826744", true], file_get_contents("$dataDir/0001-2068.xml")],
			[["058481648", false], file_get_contents("$dataDir/1361-374X.xml")],
		];
	}

	private static function callProtectedMethod($obj, string $methodName, array $args)
	{
		$reflection = new \ReflectionClass($obj);
		$method = $reflection->getMethod($methodName);
		$method->setAccessible(true);
		return $method->invokeArgs($obj, $args);
	}
}
