Installation de Mir@bel
=======================

* Installer les dépendances avec `composer install`, éventuellement `--no-dev`.

* Créer une base de données et un utilisateur MySQL.

* Initialiser la base avec un dump (par ex. celui de `tests/_data/mirabel2_tests.sql.gz`).

* Installer [Manticore Search](https://manticoresearch.com/).  
  Auparavant, Mir@bel utilisait Sphinx Search, dont Manticore s'est séparé.

* Configurer le fichier `src/www/protected/config/local.php`
  à partir du fichier `local.dist.php` dans ce même répertoire.
	* prod ou debug
	* config MySQL
	* config Sphinx

* Générer le fichier de config de Manticore avec
  `./yii sphinx conf > /chemin/de/manticore.conf`
	* La commande `./yii` suppose que `php` est accessible en ligne de commande.
	* S'il y a plusieurs instances sur un même démon searchd,
	  remplacer `conf` par `confLocal` pour chaque instance,
	  puis compléter par un `confCommon`.

* Démarrer Manticore (`searchd`) avec cette config.

* Exécuter `./yii migrate --interactive=0`.
  Si c'est une nouvelle installation ou que le code a évolué,
  une mise à jour de la base de données sera lancée.

* Installer la documentation interne, qui est disponible dans un dépôt git séparé.
    * `./bin/install-doc.sh`

En cas de problème
------------------

### Continuous Integration (Gitlab CI)

Le fichier `.gitlab-ci` configure un "pipeline" de Gitlab pour exécuter
les tests automatiques dans gitlab-runner.
Il contient donc toutes les commandes pour créer une instance de Mir@bel
qui fonctionne en ligne de commande.
Il peut donc aider à comprendre ou adapter le processus de déploiement.


### Manticore (fork de Sphinx)

Voir <doc/manticore.md> pour des instructions détaillées sur l'installation,
et notamment la création de services systemd.


### Configuration de Mir@bel

Dans `/src/www/protected/config/local.php`, modifier les champs du tableau PHP :

- base de données (connectionString, username, password)
- baseUrl : l'URL de la racine du site,
- ldap
- sphinx (i.e. Manticore)
- autres paramètres facultatifs (commentés dans le fichier de config).

D'autres réglages (email, etc) se font par la page web de configuration.


### Fonctions PHP manquantes

`composer` vérifie que les extensions nécessaires sont installées.
Avec Debian, les dépendances compilées s'installent par :
```
sudo apt install -y --no-install-recommends php-cli php-curl php-gd php-intl php-json php-ldap php-mbstring php-mysql php-sqlite3 php-xml php-zip
```

Pour une instance de développement, `xdebug` peut être utile en web,
et `pcov` en ligne de commande.


### Permissions de fichiers

Vérifier les permissions d'accès (par ex. `namei -l`) pour les répertoires
accessibles *récursivement* en lecture et en écriture par web et ligne de commande :

- src/www/assets
- src/www/images/ (7 sous-répertoires)
- src/www/protected/data
- src/www/protected/runtime

Concrètement, si l'utilisateur "mirabel" est dans le groupe "www-data" du serveur web :
```sh
chown -R www-data: src/www/assets src/www/images/ src/www/protected/data src/www/protected/runtime
chmod gu+rwX -R src/www/assets src/www/images/ src/www/protected/data src/www/protected/runtime
```

Annexes
-------

### Initialisation MySQL

Par exemple :
```sh
echo "
CREATE DATABASE mirabel_devel
GRANT ALL PRIVILEGES ON mirabel_devel.* TO mirabel IDENTIFIED BY 'thepassword';
" | mysql -u root -p
zcat tests/_data/mirabel2_tests.sql.gz | mysql -u mirabel -pthepassword mirabel_devel
```

### Configuration Nginx

Le VirtualHost doit au minimum orienter les requêtes vers `index.php` pour permettre les *pretty URLs*.

D'autres règles sont appliquées à ce niveau pour des raisons de sécurité ou de performance
(contrôle d'accès par IP, interdiction de /protected, cache navigateur, etc).

Un exemple de configuration sous Debian avec un socket php-fpm est dans
<doc/exemples/nginx-mirabel.conf>.

### Configuration Apache

Un exemple de configuration sous Debian avec un socket php-fpm est dans
<doc/exemples/apache-vh-mirabel.conf>.
