#!/bin/bash

declare -a instances
instances=("mirabel-prod" "mirabel-demo" "mirabel-devel" "mirabel-tmp")
outputfile="$(pwd)/manticore_NEW.conf"

echoerr() { echo "$@" 1>&2; }

echoerr "Outputs the manticore config for all the instances configured:"
echoerr "    ${instances[*]}"

echoerr "Parties spécifiques"
for instance in "${instances[@]}" ; do
	echoerr "$instance"
	echo "# INSTANCE $instance" >> "$outputfile"
	(cd "$instance" && ./yii sphinx confLocal >> "$outputfile")
done

echoerr "Partie commune"
(cd "${instances[0]}" && ./yii sphinx confCommon >> "$outputfile")

printf "\ndiff -u ~/manticore/manticore.conf %s\n" "$outputfile"
echo "mv ~/manticore/manticore.conf ~/manticore/manticore.conf.OLD && mv $outputfile ~/manticore/manticore.conf"
echo "(cd ~/manticore && /usr/bin/indexer --config manticore.conf --rotate --all)"
