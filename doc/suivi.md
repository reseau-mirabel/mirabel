# Suivi dans Mir@bel2

## Questions

### À confirmer auprès des IEP/ENS

* S'il n'est pas sur un objet qu'il suit, un utilisateur authentifié
  n'a pas plus de droit de suivi qu'un visiteur.  
  Par exemple, ses modifications sur un service seront soumises à validation.

* Parmi les propositions possibles pour un visiteur, la seule création est
  pour "Editeur", dans le cadre d'une modification de titre.  
  En particulier, pas de création de "Service".

* Seul un administrateur peut directement créer un titre.  
  Exception pour le titre suivant celui qu'on suit ?

* En dehors des éditeurs, quelles *propositions de créations* sont possibles
  pour les partenaires ? (cf CdC 2.3 p.20)  
  Un service (non-suivi) ? Un titre ? Une ressource ?  
  A moins que les créations soient toujours directes ?

### Questions internes et remarques douteuses

* En cas de proposition de création d'un éditeur lié à un titre,
  il faudra l'enregistrer réellement dans la table (statut "attente"),
  sinon le stockage de la relation devra être "sur mesure".

* Intervention.editeurId n'est rempli qu'en cas de modification dans la table
  Editeur. Les champs ressourceId et titreId sont alors vides.
  Le champ sert au suivi global des éditeurs.  
  Pour ceux qui suivent un éditeur, cela se fera par jointure à la volée
  (pas d'info sur les pertes de relation "Editeur-Titre").


## Table "Intervention"

### Relations avec les autres tables

Les champs "ressourceId" et "titreId" doivent être remplis quand c'est possible.
Pour un "Service", les 2 doivent être remplis.
Pour un "Titre_Editeur", "titreId" et "editeurId" doivent être remplis.

Le champ "editeurId" doit être rempli uniquement pour les interventions directes
dans la table "Editeur" ou dans "Titre_Editeur".

### Format d'enregistrement

Les modifications proposées ou effectués sont saisies dans un formulaire,
et sauvegardées dans **un seul enregistrement** de "Intervention".

Les données sont sérialisées dans le champ "contenu".

### Sérialisation dans "Intervention"."contenu"

Pour la création et suppression ("create", "delete"), le champ "attributes" contient
un tableau associatif de clé et valeurs scalaires.
Pour la modification ("update"), les valeurs ne sont plus scalaires, ce sont
des tableaux associatifs de clés "before" et "after".

Exemple de modification d'un titre, y compris l'ajout de son éditeur,
en JSON (sérialisé dans la base) :

~~~~JSON
[
	{
		table: "Titre",
		id: 24,
		operation: "update",
		before: {
			titre: "mots",
			sigle: ""
		after: {
			titre: "mots. Le langage du politique.",
			sigle: "Les mots"
			}
		}
	},
	{
		table: "Titre_Editeur",
		id: null,
		operation: "create",
		after: {
			titreId: 24,
			editeurId: 5
		}
	}
]
~~~~

Autre exemple, en PHP :

~~~~PHP
<?php
array(
	array(
		"table" => "Service",
		"id" => 24,
		"operation" => "update",
		"attributes" => array(
			"acces" => array(
				"before" => "texte intégral",
				"after" => "sommaire"
			),
			"langues" => array(
				"before" => "fr",
				"after" => "fr-en"
			),
		)
	)
)
~~~~

### Remarques

* On liste dans "before" et "after" uniquement les champs modifiés.
* Pour une suppression, le champ "id" est aussi au niveau 1, comme pour une modification.
* Il faut imposer un champ "id" dans chaque table relationnelle concernée par la veille
  ("Titre_Editeur", "Titre_Collection" et "Identification").


## Notifications

Les notifications sont l'affichage des dernières interventions concernant
l'utilisateur, en fonction de ses droits de suivis.

### Notifications pour un suivi classique (Ressource|Titre)

Le partenaire associé à l'utilisateur possède des droits de suivi via la table
"Suivi", avec le champ "cible" qui contient le nom de la table.
Il suffit donc de faire un jointure (ou 2 requêtes SQL) pour obtenir les
enregistrements de "Intervention" concernés.

### Notifications pour un suivi par Editeur

Identique au précédent, avec un passage par la table "Titre_Editeur".

### Notifications pour le suivi général des éditeurs

Si un utilisateur a l'attribut "suiviEditeurs", alors il suit ce qui se passe
dans la table "Editeur" (mais pas dans les titres associés).
Les interventions se trouvent avec un `WHERE editeurID IS NOT NULL AND titreId IS NULL`
(il faut ignorer ce qui concerne la table "titre_Editeur").

### Notification pour les objets non-suivis

Si un utilisateur a l'attribut "suiviNonSuivi", alors il suit les interventions
pour lesquelles ni "ressourceId" ni "titreId" ne figurent parmi les enregistrements
de la table "Suivi".

#### Solution par changements d'états de l'intervention

Au moment de l'enregistrement dans la table "Intervention", il est facile de regarder
si celle-ci est suivie (requête simple dans la table "Suivi").
On fixe donc un booléen "suivi" lors de l'enregistrement initial.
Puis toute modification dans les droits de suivi doit mettre à jour la table "Intervention".

Par exemple, si un utilisateur se met à suivre le titre 24 :

~~~~SQL
UPDATE Intervention SET suivi = 1 WHERE suivi = 0 AND titreId = 24 ;
~~~~

Le principal avantage est que l'interrogation des objets suivis devient simple.
Comme l'affichage des notifications est une opération courante,
alors que la modification des droits de suivi est rare,
il est appréciable que la première opération soit facile et la seconde complexe.


## Modifications

* Seuls les admin ont un droit global de modifier ou valider tout objet.

* Prévoir un filtre anti-spam.
  Par exemple, captcha à la seconde proposition de modification anonyme de même IP.

### Modification de services

Pour création ou la modification d'un service
lié à un *titre* de *revue* et à une *ressource*,
le droit de modification par un utilisateur découle du tableau suivant :

| | | global ? | modification |
|--|--|:--:|:--:|
|0| La ressource liée est verrouillée (import auto) | local | non |
|1| L'utilisateur a le rôle *admin* | global | directe |
|2| Le partenaire suit le titre lié | local | directe |
|3| Le partenaire suit la ressource liée | local | directe |
|4| Le partenaire suit l'éditeur lié | local | directe |
|5| Personne ne suit la ressource et le titre liés, et l'utilisateur a l'attribut *suiviNonSuivi* | global | directe |
|6| Autre | . | suggestion à valider |

### Autres suivis (titre, ressource, éditeur, revue)

Pour la création, les utilisateurs authentifiés peuvent agir directement (**à confirmer**),
les visiteurs ne peuvent que suggérer.

Pour la modification et la suppression, cela dépend de la table ciblée.

#### Ressource

| | **Ressource** | global ? | modification |
|--|--|:--:|:--:|
|0| La ressource est verrouillée (import auto) | local | non |
|1| L'utilisateur a le rôle *admin* | global | directe |
|3| Le partenaire suit la ressource | local | directe |
|5| Personne ne suit la ressource, et l'utilisateur a l'attribut *suiviNonSuivi* | global | directe |
|6| Autre | . | suggestion à valider |

#### Titre

| | **Titre** | global ? | modification |
|--|--|:--:|:--:|
|1| L'utilisateur a le rôle *admin* | global | directe |
|2| Le partenaire suit le titre | local | directe |
|4| Le partenaire suit l'éditeur lié | local | directe |
|5| Personne ne suit le titre, et l'utilisateur a l'attribut *suiviNonSuivi* | global | directe |
|6| Autre | . | suggestion à valider |

#### Editeur

**à confirmer**

| | **Editeur** | global ? | modification |
|--|--|:--:|:--:|
|| Toute personne authentifiée | global | directe |
|6| Autre | . | suggestion à valider |

#### Remarques

* Les partenaires de type "éditeur" ne peuvent que suivre
  des éditeurs et ne peuvent rien détenir (table "Partenaire_Titre").

* Un titre est dit *suivi* uniquement s'il y a un suivi directement dessus.

