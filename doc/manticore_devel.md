Développement avec Manticore
============================

En alternative à l'installation globale décrite dans la [doc d'utilisation](manticore.md),
on peut préférer soit lancer le démon Manticore `searchd` manuellement,
soit via un service systemd de l'utilisateur (et non un service du système).

Démarrer le démon Manticore (manuel)
---------------------------

La procédure est identique au déploiement en production,
en replaçant le service systemd `manticore-mirabel.unit`
par la commande :
```sh
(cd "$MIRABEL_MANTICORE_DIR" && searchd --config "$MIRABEL_MANTICORE_CONFIG")
```

Éventuellement :
```sh
# Vérifier le fonctionnement
searchd --config "$MIRABEL_MANTICORE_CONFIG" --status
# Arrêter le démon
searchd --config "$MIRABEL_MANTICORE_CONFIG" --stopwait
```

Démarrer le démon Manticore (systemd --user)
---------------------------

Le démon Sphinx Search peut être lancé par `systemctl --user` sans être root.

Le plus simple est de déclarer quelques variables :
```
# manticore.env
MIRABEL_MANTICORE_DIR=/chemin/vers/manticore/mirabel
MIRABEL_MANTICORE_CONFIG=/chemin/vers/manticore/mirabel/mirabel.conf
# At least one of socket or port must be configured.
MIRABEL_MANTICORE_SOCKET=/chemin/vers/manticore/mirabel/run/mimamy.socket
MIRABEL_MANTICORE_PORT=9703
```

Le service devient alors :
```ini
# ~/.config/systemd/user/manticore-mirabel.service
[Unit]
Description=Sphinx Search for Mir@bel, as a user service
After=local-fs.target network.target

[Service]
EnvironmentFile=/chemin/vers/mirabel/manticore.env
# WorkingDirectory cannot use environment variables
WorkingDirectory=/chemin/vers/mirabel/manticore

# Read config under %E = $XDG_CONFIG_HOME (defaults to ~/.config/)
ExecStart=/usr/bin/searchd --config $MIRABEL_MANTICORE_CONFIG --nodetach
ExecStop=/usr/bin/searchd --config $MIRABEL_MANTICORE_CONFIG --stopwait

Restart=on-failure
KillMode=process
KillSignal=SIGTERM
SendSIGKILL=no

[Install]
WantedBy=multi-user.target
```

Activer à la demande avec `systemctl --user start manticore-mirabel`,
ou à chaque session avec `... enable ...`.

Tests automatiques
------------------

Si le fichier `manticore.env` est présent à la racine (cd ci-dessus),
alors il est lu par `./bin/tests-php.sh`.
Si le script reçoit bien les variables d'environnement nécessaire :
- Il regénère le fichier de configuration de Manticore (base normale + base de tests).
- Il réindexe les données (lecture dans MariaDB).

`bin/tests-php.sh` ne lancera les tests que si `searchd` est accessible.


Changer la structure (tables, champs)
--------------------

Pour changer la structure des données, il faut
modifier les vues des commandes `sphinx`,
dans `src/www/protected/commands/views`.

Si nécessaire, modifier les modèles ActiveRecord associés aux tables,
par exemple `src/www/protected/models/sphinx/Editeurs.php`.

Regénérer la configuration
--------------------------

C'est nécessaire après un changement de structure.
La commande `./yii sphinx conf` ne traite que les tables de l'instance principale.
Il faut utiliser `./yii sphinx confLocal` pour traiter plusieurs instances,
et `src/www/protected/yiic_test.php sphinx confLocal` pour les tables des tests automatiques.

Par exemple, que Manticore contienne la base de test et la base standard :
```sh
export MIRABEL_MANTICORE_CONFIG="$HOME/.config/manticore/mirabel.conf"
# uniquement les données, pour la config de test
php src/www/protected/yiic_test.php sphinx confLocal > "$MIRABEL_MANTICORE_CONFIG"
# données et service, pour la config normale
./yii sphinx conf >> "$MIRABEL_MANTICORE_CONFIG"
```

Puis réindexer :
- Si `searchd` tourne : `(cd "$MIRABEL_MANTICORE_DIR" && indexer --config "$MIRABEL_MANTICORE_CONFIG" --all --rotate)`
- Sinon : `(cd "$MIRABEL_MANTICORE_DIR" && indexer --config "$MIRABEL_MANTICORE_CONFIG" --all )`

Mode SQL interactif
-------------------

On peut se connecter au service Manticore avec n'importe quel client MySQL.
Par exemple avec `mysql` ou `mycli`.
```sh
# Afficher le chemin vers le socket
grep mysql4 "$MIRABEL_MANTICORE_CONFIG"
	listen = /tmp/sphinx-mirabel-mysql.socket:mysql41
# Se connecter
mysql --socket=/tmp/sphinx-mirabel-mysql.socket
```

Ensuite les commandes usuelles de MySQL fonctionnent :
```sql
SHOW TABLES;
DESC devel_titres;
-- Attention, LIMIT est à 20 par défaut
SELECT * FROM devel_titres;
SELECT revueid, count(*) FROM devel_titres WHERE paysid = 62 GROUP BY revueid;
```
