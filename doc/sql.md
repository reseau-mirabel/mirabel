Mise à jour de l'application
============================

Appliquer des migrations SQL
----------------------------

En ligne de commande :

    ./yii migrate

On peut connaître les migrations en attente avec `./yii migrate new`,
et l'historique des migrations appliquées avec `./yii migrate history`.

Après une synchronisation des données ou des opérations sur la base,
on peut avoir besoin de forcer l'état des migrations avec
`./yii migrate mark version_voulue`
Avec cette commande, aucune migration n'est appliquée.


Créer des migration SQL
-----------------------

En ligne de commande :

    ./yii migrate create nom_du_patch

Puis modifier le nouveau fichier `www/protected/migrations/m{date}_{heure}_nom_du_patch`.
La complétion de `$this->` les les commandes spécifiques à une migration.

Idéalement, une migration doit être *idempotente* : une seconde application ne change rien.
C'est surtout important pour les migrations qui ne sont pas effacées par les synchros
(chargements de sauvegardes SQL), comme les créations de tables.
Pour cela, on a souvent besoin de vérifier l'existence d'une table ou d'un champ.
```
if (!$this->getDbConnection()->getSchema()->getTable("MaTable")) {
if (!$this->getDbConnection()->getSchema()->getTable("X")->getColumn("y")) {
```

### Données de test

S'il faut introduire des **données de test**, utiliser un bloc
```
if (YII_ENV === 'test') {
	$this->insert(...);
	$this->execute("...sql...");
}
```

### up() ou safeUp() ?

**Si la migration ne modifie aucune structure SQL**, alors on peut utiliser
les méthodes `safeUp()` et `safeDown()` qui garantissent la cohérence :
la migration est emballée dans une transaction SQL.

Si une commande modifie la structure d'une table, alors MySQL interrompt
les migrations en cours, donc les méthodes `up()` et `down()` sont nécessaires.


Conventions
-----------

- Le nommage SQL suit généralement les mêmes conventions qu'en PHP,
  soit `MaTable` et `monChamp`.
- Les tables de relations utilisent parfois un séparateur : `Titre_Editeur`.
- La clé primaire, si elle est explicite, est nommée `id`.
- Les champs à clés étrangères sont nommés `maTableId`.

Historiquement (reprise d'une base existante), les noms sont en français.
Localement, certaines tables sont en anglais.

