Doc technique interne à Mir@bel
===============================

Cette documentation techinque est dans un dépôt privé :
https://gitlab.com/reseau-mirabel/mirabel-doc-interne
Elle est destinée aux utilisateurs experts,
et peut être utile aux développeurs.

Pour l'installer ou la mettre à jour :
```
./bin/install-doc.sh
```

Pour modifier la doc technique :
```
cd doc/doc-interne
git …
git push
```

Le chemin d'installation par défaut est "doc/doc-interne".
Ce chemin est déclaré par "main.php" dans "src/www/protected/config/"
On peut le changer en modifiant "local.php" dans ce même répertoire.

Miroir pour le déploiement
--------------------------

Un compte gratuit sur gitlab.com ne permet pas de créer
une clé de déploiement pour un dépôt privé.
Donc git.silecs.info héberge un miroir, synchronisé automatiquement
avec celui de gitlab.com.

Dans `./bin/install-doc.sh`, l'URL d'installation est implicitement
`git@gitlab.com:reseau-mirabel/mirabel-doc-interne.git`.
Ceci permet ensuite de modifier le sous-dépôt.
C'est donc une commande pout le déploiement d'une instance de développement de M.

Mais pour un déploiement en lecture seule, sans compte de développeur :
```
./bin/install-doc.sh git@git.silecs.info:silecs/mirabel-doc-interne
```
Il faut bien sûr que la clé SSH ait été ajoutée aux clés de déploiement
de ce dépôt Git.

La mise à jour du contenu se fait par la même commande :
```
./bin/install-doc.sh
```
