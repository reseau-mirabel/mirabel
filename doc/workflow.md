Workflow dans Mir@bel
=====================

En bref
-------

- Si on crée une branche locale `M12345_qq_chose`, ne la publier que si nécessaire.
  Si publiée, supprimer après usage : `git push --delete origin M12345_qq_chose`.

- Pour faire valider ses développements, commiter ou fusionner dans "master".
  Si la branche "devel" existe, fusionner "master" dedans.

- Déployer sur le serveur dans l'instance "devel".

- Passer le ticket en "résolu" et rédiger une note.

- Vérifier que le ticket impacté est dans la liste "dev pas en prod".


En détail
---------

## 1. Ticket

Choisir un ticket dans Mantis. Par exemple #5678.

## 2. Branche ?

Si nécessaire (ticket long à traiter, ou à intégrer bien plus tard),
créer une branche.
En général, le nom de la branche contient le numéro de ticket et un titre,
par ex. "m5747_obsoletepar_unique" ou "5414_marcxml".

Sinon, travailler directement dans la branche "master".

## 3. commits avec git

Les messages de commit doivent commencer par une référence au ticket :
"M#5678 ...".

Si le message a plusieurs lignes, il faut une ligne vide après le titre.
Cette convention n'est pas juste pour Mir@bel, elle est quasi-universelle.

Pour une petite branche,
relire le diff global avec `git diff master...`,
puis fusionner depuis master par `git merge --squash branche`.

Si la branche est assez complexe pour que l'historique soit utile,
on la fusionne avec `git merge branche`.
Si son historique n'est pas propre,
on peut auparavant la nettoyer localement (`git rebase -i `),

## 4. Tests

Exécuter les tests :
```sh
./bin/tests-php.sh -f
./bin/tests-static-analysis.sh
```
Éventuellement ajouter les tests JS `./bin/tests-js.sh`.

Les tests sont de toutes façons lancés par Gitlab
qui enverra un message à l'auteur du commit, mais il est plus simple
de traiter les erreurs avant qu'elles ne soient publiées.

## 5. Publier le code

`git push` publie sur Gitlab.

Si une branche "devel" existe, il faut fusionner dedans.
```sh
git switch devel
git pull
git merge master (ou la branche à long terme hors de master)
git push
git switch -
```
C'est cette branche "devel" qui est utilisée par l'instance commune "mirabel-devel",
ou à défaut la branche "master".

Pour déployer sur l'instance de test, il faut se connecter en ssh au serveur de test,
puis effectuer un `git pull` (étape à automatiser par chaque auteur).

La branche "devel" peut être réinitialisée, notamment après une montée de version.
Si `git pull` échoue sur cette branche, vérifier qu'on n'a pas de modification locale,
puis `git reset --hard "{u}"` pour passer à l'état tel que sur le serveur Git.

## 6. Compléter le ticket

1. Ajouter une note dans le ticket.
   Éventuellement cela passe par un changement d'état du ticket, cf plus bas.

2. Ajouter ensuite le ticket à la liste des développements pas encore en production,
   sur la liste "dev pas en prod" de https://tickets.silecs.info//plugin.php?page=TicketList/index

Les **états dans Mantis** (tickets.silecs.info) ont pour Mir@bel un sens précis :
- *résolu* si le ticket est implémenté et qu'on attend la validation
- *validé* si le demandeur confirme que l'implémentation lui convient
   (en général, on ne passe pas nous même à cet état).
- *en attente* s'il manque des informations pour développer
- *fermé* si le ticket est résolu et le code en production (ou qu'il n'y a pas de code)
- Tout autre état si le travail de développement est en cours ou à faire plus tard.

## 7. Montée de version ?

Le script `./bin/prod.sh` liste les tickets concernés par une montée de version
et détaille le processus à suivre.

Il faut croiser ces infos avec la liste "dev pas en prod" de Mantis
pour vérifier que la liste est à jour,
et que ce qui passera en production est validé.
