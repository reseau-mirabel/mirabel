Développement de Mir@bel
========================

Cette documentation complète le guide officiel du Framework Yii 1.1
avec les points spécifiques à Mir@bel.

Le **processus de développement** est décrit dans [workflow](workflow.md).
D'autres aspects sont dans des fichiers distincts.

Beaucoup d'explications du fonctionnement sont dans la doc technique
à destination des administrateurs et pilotes :
cf `src/www/protected/views/doc/pages/`
ou par l'URL `/doc` dans M (authentification nécessaire).
Le *schéma de la base de données* est dans cette doc technique.

Ressources extérieures
----------------------

- Le [guide de Yii 1.1](https://www.yiiframework.com/doc/guide/1.1/en)
  et la [liste des classes de Yii](https://www.yiiframework.com/doc/api/1.1)

- [Bootstrap v2](https://getbootstrap.com/2.3.2/)

- [Manticore Search](https://manticoresearch.com/)
  et surtout sa [documentation](https://manual.manticoresearch.com/) (JS requis)

https (ssl-tls)
---------------

Par défaut, M est configuré dans `src/www/protected/config/main.php` pour ne
gérer les cookies de sessions que si la connexion est en https.
Pour une instance locale, il faut soit activer un certificat (par exemple avec `mkcert`
de mkcert.dev), soit désactiver ces réglages dans `src/www/protected/config/local.php`.
```php
<?php
return [
  'components' => [
    'session' => [
      'cookieParams' => [
        'secure' => false,
        'httponly' => true,
      ],
?>
```

Tester
------

Les principaux tests automatiques sont configurés dans `.gitlab-ci.yml`.
Les instructions de ce fichier peuvent se transposer à un environnement local.

### PHP

Prérequis :

- Une base de données `mirabel2_tests` existe.
- Cette base est accessible à l'utilisateur `mirabel@localhost`,
  cf `src/www/protected/config/test.php`.
- Pour certains tests, le service Manticore doit être accessible.
- Pour le *code coverage*, l'extension PHP *pcov* (à défaut, *Xdebug*) doit être installée.

Lancer tous les tests :
```sh
./bin/tests-php.sh
```

Un test ciblé, avec infos de débogage :
```sh
./bin/tests-php.sh -f -d -- functional RessourceCest
```
Ici `-f` indique qu'il faut arrêter les tests à la première erreur.
Une autre option utile est `-d` pour afficher des infos de débogage,
en particulier ce qui est passé à `codecept_debug()`.

Écrire les données HTML sur la couverture du code PHP par les tests (*code coverage*),
dans `tests/_output/coverage/` :
```
./bin/tests-php.sh --coverage
```


### JavaScript

Lancer tous les tests :
```
pnpm test
```

Écrire les données HTML sur la couverture du code JS par les tests (*code coverage*),
dans `tests/_output/coverage-js/` :
```
pnpm coverage
```


Outils de qualité de code
-------------------------

### Analyse statique

Afficher un rapport d'analyse statique du code :
```sh
./bin/tests-static-analysis.sh
```
Le cache est désactivé car `psalm` plante souvent lorsque qu'il utilise un cache
et que des fichiers PHP ont été modifiés.


### Formatage du code source

Si nécessaire, [installer php-cs-fixer](https://github.com/FriendsOfPHP/PHP-CS-Fixer/blob/3.0/doc/installation.rst),
par exemple :
```
wget -O php-cs-fixer https://cs.symfony.com/download/php-cs-fixer-v3.phar
```

Formatter le code PHP récursivement :
```
php php-cs-fixer fix --allow-risky=yes [répertoire]
```
Attention de *ne pas appliquer aux vues* car le mélange HTML et PHP déconcerte l'outil.
Relire avant de commiter (par exemple `git add -p`).

