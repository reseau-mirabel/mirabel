Manticore Search et Mir@bel
===========================

Manticore est un fork de Sphinx.

Installation
------------

Suivre la [documentation](https://manual.manticoresearch.com/).

Pour une installation sous Debian,
les paquets "manticore" et "manticore-tools" sont nécessaires.

Attention, le paquet "manticore-repo.noarch.deb" autorise la signature
de Manticore globalement.
Pour ajouter manuellement le dépôt de Manticore à Debian bookworm,
et restreindre sa signature :
```
curl -fsSL "https://repo.manticoresearch.com/GPG-KEY-manticore" \
  | gpg --dearmor > /usr/local/share/keyrings/deb.manticoresearch.gpg
```
Puis créer `/etc/apt/sources.list.d/manticoresearch.sources` avec le contenu:
```
Types: deb
URIs: http://repo.manticoresearch.com/repository/manticoresearch_bookworm
Suites: bookworm
Components: main
Architectures: amd64
Signed-By: /usr/local/share/keyrings/deb.manticoresearch.gpg
```
On peut alors installer normalement.
```sh
apt update && apt install manticore manticore-tools
```

Fonctionnement général
----------------------

La recherche dans Mir@bel utilise le démon `searchd` de "manticore-server-core".
La communication se fait en SQL avec le protocole MySQL, mais indépendamment de MySQL.
Plus précisément, il faut :
1. une indexation des données SQL vers Manticore avec `indexer`,
2. un démon `searchd` actif
3. un *socket* ou un *port TCP* permettant d'accéder au démon.

La configuration par défaut des paquets Manticore est inadaptée.
Le plus simple est de la désactiver :
```sh
systemctl stop manticore.service
```

Il est plus simple que l'utilisateur unix
qui indexe les données soit aussi le propriétaire du démon.
Sans cela, il est difficile de mettre à jour automatiquement la structure Sphinx
quand on lance les tests.

Configurer et charger les données
---------------------------------

Cette documentation est celle de la production.
Pour un déploiement de développement, consulter <manticore_devel.md>.

Pour la suite on suppose que toutes les données de Manticore sont
dans un répertoire `/srv/mirabel/manticore/` appartenant à l'utilisateur "mirabel".
Cf <manticore_devel.md> pour d'autre modes de déploiement.

1. Configurer Mir@bel via `src/www/protected/config/local.php` pour que le bloc sphinx
  déclare un préfixe de tables et un socket.
  Par exemple,
  ```
  'sphinx' => [
      'connectionString' => 'mysql:unix_socket=/srv/mirabel/manticore/run/manticore-mirabel.socket',
      'tablePrefix' => 'prod_',
  ],
  ```

2. Créer les répertoires nécessaires.
```sh
export MIRABEL_MANTICORE_DIR="/srv/mirabel/manticore/"
mkdir -p "$MIRABEL_MANTICORE_DIR"/{indexes,log,run}
```

3. Générer un fichier de configuration de Manticore.
```sh
export MIRABEL_MANTICORE_CONFIG="$MIRABEL_MANTICORE_DIR/mirabel.conf"
./yii sphinx conf > "$MIRABEL_MANTICORE_CONFIG"
cp  src/www/protected/stopwords.txt "$MIRABEL_MANTICORE_DIR"
```

4. Charger les données.
```sh
(cd "$MIRABEL_MANTICORE_DIR" && indexer --config "$MIRABEL_MANTICORE_CONFIG" --all)
```

5. Créer le service systemd global (utilisateur "mirabel").
```ini
# /etc/systemd/system/manticore-mirabel.service
[Unit]
Description=Manticore Search Engine for Mir@bel
After=network.target
Documentation=https://manual.manticoresearch.com/, man:searchd(1)

[Service]
User=mirabel
Group=mirabel

# files (indexes, etc)
WorkingDirectory=/srv/mirabel/manticore
PIDFile=/srv/mirabel/manticore/run/searchd.pid

# Before a first start, please ensure the indexes exist:
# /usr/bin/indexer --config mirabel.conf --all

ExecStart=/usr/bin/searchd --config mirabel.conf --nodetach
ExecStop=/usr/bin/searchd --config mirabel.conf --stopwait

KillMode=process
KillSignal=SIGTERM
SendSIGKILL=no
LimitNOFILE=65536
LimitCORE=infinity
LimitMEMLOCK=infinity
Restart=on-failure
TimeoutStartSec=infinity

[Install]
WantedBy=multi-user.target
```

6. Démarrer le service.
```sh
sudo systemctl enable --now manticore-mirabel
```
