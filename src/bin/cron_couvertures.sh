#!/bin/bash

# go to the app's directory (script is in src/bin/)
cd "$( dirname "${BASH_SOURCE[0]}" )"
cd ..
cd ..

php src/www/protected/yiic.php couverture cairnWeb --verbose=0 --interventions=1 --majMemeSource=1
php src/www/protected/yiic.php couverture download --verbose=0 --urlPattern='https://www.cairn.info/%'
php src/www/protected/yiic.php couverture resize

