<?php

define('DATA_DIR', /** @var string */ 'data-tests');
define('APP_PRODUCTION_MODE', /** @var bool */ false);
define('VENDOR_PATH', /** @var string */ dirname(__DIR__, 2) . '/vendor');
define('YII_DEBUG', /** @var bool */ true);
define('YII_ENV', /** @var string */ 'test');
define('YII_PATH', /** @var string */ VENDOR_PATH . '/yiisoft/yii/framework');
define('SPH_GROUPBY_ATTR', 'attr');
define('SPH_SORT_ATTR_ASC', 'attr');
define('SPH_SORT_EXTENDED', 'ext');
