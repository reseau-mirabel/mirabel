<?php

namespace silecs\psalm;

use PhpParser;
use Psalm;
use Psalm\Internal\Analyzer\ClassAnalyzer;
use Psalm\Internal\Analyzer\MethodAnalyzer;
use Psalm\Internal\Analyzer\StatementsAnalyzer;
use Psalm\Context;
use Psalm\DocComment;
use Psalm\Node\Stmt\VirtualClass;
use Psalm\Node\Stmt\VirtualClassMethod;
use Psalm\Storage\MethodStorage;
use Psalm\Type;

/**
 * Load PHP templates into vimeo/psalm.
 *
 * Usage: add to `psalm.xml`:
 * ```
 * <fileExtensions>
 *     <extension name=".php" checker="path/to/Yii1TemplateChecker.php" />
 * </fileExtensions>
 *
 * Then each file placed (recursively) under a directory `views`
 * will be parsed in the context of a controller object.
 * Use PHPdoc liens `@var ClassName $var` in the view file
 * in order to declare the type `$this` and the other variables.
 * E.g. in a file `protected/views/site/index.php`
 * ```
 * / ** @var SiteController $this * /
 * / ** @var int $viewVariable * /
 * ```
 */
class YiiTemplateChecker extends Psalm\Internal\Analyzer\FileAnalyzer
{
	/**
	 * Apply this special process iff the file full path contains this pattern.
	 */
	const VIEWS_PATH_PATTERN = '/views';

	/**
	 * FQDN (Fully Qualified Domain Name) of the default controller.
	 */
	const DEFAULTCONTROLLER_CLASSNAME = 'CController';

	public function analyze(?Context $file_context = null, ?Context $global_context = null): void
	{
		if (strpos($this->file_path, self::VIEWS_PATH_PATTERN) === false) {
			parent::analyze($file_context, $global_context);
			return;
		}

		$codebase = $this->project_analyzer->getCodebase();
		$stmts = $codebase->getStatementsForFile($this->file_path);
		if (!$stmts) {
			return;
		}

		$definitions = self::getCommentBlock($stmts[0]);
		if (!$definitions) {
			return;
		}

        $viewClass = trim($definitions['$this'] ?? self::DEFAULTCONTROLLER_CLASSNAME, ' \\');
		$viewContext = new Context();
		$viewContext->check_variables = true;
		$viewContext->self = strtolower($viewClass);
        foreach ($definitions as $name => $type) {
            $viewContext->vars_in_scope[$name] = Type::parseString($type);
        }
		$this->wrapStmtsInMethod($viewClass, $viewContext, $stmts);
	}

	private static function getCommentBlock(PhpParser\Node\Stmt $stmt): array
	{
        $definitions = [];
        foreach ($stmt->getComments() as $comment) {
            if (!($comment instanceof PhpParser\Comment\Doc)) {
                continue;
            }
            $commentBlock = DocComment::parsePreservingLength($comment);
            $var = $commentBlock->tags['var'] ?? '';
            if (!$var) {
                continue;
            }
            $matches = [];
            foreach ($var as $value) {
                if (preg_match('/^(.+?) (\$\w+)\b/', $value, $matches)) {
                    $definitions[$matches[2]] = $matches[1];
                }
            }
        }
        return $definitions;
	}

	private function wrapStmtsInMethod(string $viewClassName, Context $context, array $stmts): void
	{
		$pseudoMethodName = strtolower(preg_replace('/\W+/', '_', $this->file_name) . "_" . bin2hex(random_bytes(4)));
		$viewMethodAnalyzer = $this->createMethodAnalyzer($viewClassName, $pseudoMethodName);

		if (!$context->check_variables) {
			$viewMethodAnalyzer->addSuppressedIssue('UndefinedVariable');
		}

		$pseudoMethodStmts = $this->filterOutUse($stmts);
        try {
            $statementsSource = new StatementsAnalyzer(
                $viewMethodAnalyzer,
                new \Psalm\Internal\Provider\NodeDataProvider()
            );
            $statementsSource->analyze($pseudoMethodStmts, $context);
        } catch (\Throwable $e) {
            // This plugin warns about exceptions and stops checking the file.
            error_log(sprintf("Exception:\n\t'%s'\n\twhile parsing '%s'\n", $e->getMessage(), $this->file_path));
        }
	}

	private function filterOutUse(array $stmts): array
	{
		$remains = [];
		foreach ($stmts as $stmt) {
			if ($stmt instanceof PhpParser\Node\Stmt\Use_) {
				$this->visitUse($stmt);
			} else {
				$remains[] = $stmt;
			}
		}
		return $remains;
	}

	private function createMethodAnalyzer(string $className, string $methodName): MethodAnalyzer
	{
		$returnType = null;
		$method = new VirtualClassMethod(
            $methodName,
            [
                'flags' => PhpParser\Node\Stmt\Class_::MODIFIER_PROTECTED,
                'params' => [],
                'returnType' => $returnType,
                'stmts' => [],
            ]
        );

		$storage = new MethodStorage();
		$storage->signature_return_type = $returnType;

		// Add this pseudo method to the class
		$codebase = $this->project_analyzer->getCodebase();
		$codebase->classlike_storage_provider->get($className)->methods[$methodName] = $storage;

		return new MethodAnalyzer($method, $this->getClassAnalyzer($className), $storage);
	}

	private function getClassAnalyzer(string $className): ClassAnalyzer
	{
		$class = new VirtualClass($className);
		return new ClassAnalyzer($class, $this, $className);
	}
}
