<?php
/**
 * This is the bootstrap file for test application.
 */

defined('VENDOR_PATH') or define('VENDOR_PATH', dirname(__DIR__, 2) . '/vendor');
require_once VENDOR_PATH . '/autoload.php';

spl_autoload_register(array('YiiBase','autoload'));
require_once __DIR__ . '/protected/components/Yii.php';

defined('YII_ENV') or define('YII_ENV', 'test');

$localConfig = require __DIR__ . '/protected/config/local.php';
$mainConfig = require __DIR__ . '/protected/config/main.php';
$testConfig = require __DIR__ . '/protected/config/test.php';
$config = \CMap::mergeArray(
	\CMap::mergeArray($mainConfig, $localConfig),
	$testConfig
);
unset($mainConfig, $localConfig, $testConfig);
if (isset($config['components']['session']['savePath'])) {
	$sessionPath = $config['components']['session']['savePath'];
	if (!is_dir($sessionPath)) {
		@mkdir($sessionPath);
	}
}

$config['basePath'] = __DIR__ . '/protected';

$_SERVER['SERVER_NAME'] = 'localhost';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

//Yii::createWebApplication($config)->run();
return [
	'class' => 'WebApplication',
	'config' => $config,
];
