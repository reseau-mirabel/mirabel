<?php

$localConfig = require __DIR__ . '/protected/config/local.php';
defined('YII_ENV') or define('YII_ENV', APP_PRODUCTION_MODE ? 'prod' : 'dev');
$mainConfig = require __DIR__ . '/protected/config/main.php';

define('VENDOR_PATH', dirname(__DIR__, 2) . '/vendor');
require_once VENDOR_PATH . '/autoload.php';
spl_autoload_register(array('YiiBase','autoload'));
require_once __DIR__ . '/protected/components/Yii.php';

$config = CMap::mergeArray($mainConfig, $localConfig);
unset($mainConfig, $localConfig);
Yii::createWebApplication($config);

$type = 'atom';
if (isset($_GET['type']) && in_array($_GET['type'], ['rss1', 'rss2', 'atom'])) {
	$type = $_GET['type'];
}

$gen = new \components\FeedGenerator($type);
$gen->privateFeed = true;
if (isset($_GET['valide'])) {
	$gen->showValidatedOnly((boolean) $_GET['valide']);
} else {
	$gen->showValidatedOnly(null);
}
if (isset($_GET['editeurs'])) {
	$gen->addCriteria('editeurId', "IS NOT NULL");
	$gen->feedTitle[] = 'Éditeurs';
}
foreach (array('revue', 'ressource', 'editeur') as $o) {
	if (!empty($_GET[$o])) {
		$gen->addCriteria($o . 'Id', (int) $_GET[$o]);
	}
}
if (!empty($_GET['partenaire'])) {
	$gen->addFilterMonitored((int) $_GET['partenaire']);
}
$gen->run();
