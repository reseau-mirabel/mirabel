<?php

$localConfig = require __DIR__ . '/protected/config/local.php';
defined('YII_ENV') or define('YII_ENV', APP_PRODUCTION_MODE ? 'prod' : 'dev');
$mainConfig = require __DIR__ . '/protected/config/main.php';

define('VENDOR_PATH', dirname(__DIR__, 2) . '/vendor');
require_once VENDOR_PATH . '/autoload.php';
spl_autoload_register(array('YiiBase','autoload'));
require_once dirname(__DIR__) . '/www/protected/components/Yii.php';

$config = CMap::mergeArray($mainConfig, $localConfig);
unset($mainConfig, $localConfig);

Yii::createWebApplication($config);

ini_set('date.timezone', 'Europe/Paris');

$type = 'atom';
if (isset($_GET['type'])) {
	$type = $_GET['type'];
}

$gen = new \components\FeedGenerator($type);
$gen->showValidatedOnly(true);
foreach (array('revue', 'ressource', 'editeur') as $o) {
	if (!empty($_GET[$o])) {
		$id = (int) $_GET[$o];
		$gen->findFeedInFiles($o, $id);
		$gen->addCriteria($o . 'Id', $id);
	}
}
$gen->run();
