<?php

return [
	'Home' => 'Accueil',
	'Previous' => 'Précédent',
	'Next' => 'Suivant',
	'First' => 'Début',
	'Last' => 'Fin',
];
