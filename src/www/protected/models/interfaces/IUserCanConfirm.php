<?php

interface IUserCanConfirm
{
	public function getConfirm(): bool;

	public function setConfirm(bool $confirm): void;
}
