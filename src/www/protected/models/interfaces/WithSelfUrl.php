<?php

interface WithSelfUrl
{
	public function getSelfUrl(): array;
}
