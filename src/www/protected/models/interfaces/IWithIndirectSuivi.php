<?php

/**
 * Interface IWithIndirectSuivi for objects that anyone can suggest to modify.
 */
interface IWithIndirectSuivi
{
	public function getPrimaryKey();

	/**
	 * Returns an array of parents that have a direct "Suivi": [ [table => ,  id => ], ... ]
	 *
	 * @return array
	 */
	public function listParentsForSuivi();
}
