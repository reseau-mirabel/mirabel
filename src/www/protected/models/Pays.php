<?php

/**
 * This is the model class for table "Pays".
 *
 * The followings are the available columns in table 'Pays':
 * @property int $id
 * @property string $nom
 * @property string $code 3-letters ISO code
 * @property string $code2 2-letters ISO code
 *
 * @method Pays sorted() Cf scopes
 */
class Pays extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Pays';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['nom, code', 'required'],
			['nom', 'length', 'max' => 200],
			['code', 'length', 'max' => 6],
			// The following rule is used by search().
			['nom, code', 'safe', 'on' => 'search'],
		];
	}

	/**
	 * Each array key defines a function that can be chained before a find*() method.
	 */
	public function scopes()
	{
		return [
			'sorted' => [
				// Start with "Organisation internationale"
				'order' => "code2 = 'ZZ' DESC, nom ASC",
			],
		];
	}
}
