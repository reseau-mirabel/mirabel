<?php

namespace models\cms;

use Cms;

/**
 * Extrait du ticket #2533 :
 *
 * On doit pouvoir lier chaque brève à :
 * 1. un ou plusieurs partenaires
 * 2. une ou plusieurs ressources
 * 3. des catégories textuelles (saisie par champ libre, découpé par ";" en une liste de catégories)
 * Une même brève peut contenir des références de chacun de ces 3 types. La saisie se fait par autocomplétion multivaluée.
 */
class BreveForm extends \CFormModel
{
	public string $content;

	public string $date = '';

	/**
	 * @var string E.g. "congrès ; communauté"
	 */
	public string $categories;

	/**
	 * @var string E.g. "2, 4"
	 */
	public string $partenaireIds;

	/**
	 * @var string E.g. "2, 4"
	 */
	public string $ressourceIds;

	public string $sourcelienIds;

	private Cms $cms;

	private int $timestamp = 0;

	public function __construct(Cms $cms)
	{
		parent::__construct();
		$this->cms = $cms;
		$this->content = (string) $cms->content;
		$this->date = ($cms->isNewRecord ? "" : date('Y-m-d', $cms->hdateCreat));
		$this->timestamp = ($cms->isNewRecord ? time() : (int) $cms->hdateCreat);
		$struct = json_decode($cms->categorisation);
		$this->categories = (isset($struct->categories) ? join(" ; ", $struct->categories) : "");
		$this->partenaireIds = (isset($struct->partenaires) ? join(", ", $struct->partenaires) : "");
		$this->ressourceIds = (isset($struct->ressources) ? join(", ", $struct->ressources) : "");
		$this->sourcelienIds = (isset($struct->sourcesliens) ? join(", ", $struct->sourcesliens) : "");
	}

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return [
			['content', 'length', 'max' => 65000],
			['date', 'date', 'format' => 'yyyy-MM-dd'],
			[['categories', 'partenaireIds', 'ressourceIds', 'sourcelienIds'], 'length', 'max' => 250],
		];
	}

	public function attributeLabels(): array
	{
		return [
			'content' => "Contenu",
			'date' => "Date de publication",
			'categories' => "Catégories",
			'partenaireIds' => "Partenaires liés",
			'ressourceIds' => "Ressources liées",
			'sourcelienIds' => "Sources d'autres liens",
		];
	}

	public function afterValidate()
	{
		parent::afterValidate();
		if ($this->date) {
			try {
				$date = new \DateTime($this->date);
				$this->timestamp = $date->getTimestamp();
			} catch (\Throwable $e) {
				$this->addError('date', "Cette date n'est pas valide : {$e->getMessage()}");
			}
		}
	}

	public function getDateFr(): string
	{
		return (string) \Yii::app()->dateFormatter->format('d MMMM yyyy', $this->timestamp);
	}

	public function getDateIso(): string
	{
		return (string) date('Y-m-d H:i', $this->timestamp);
	}

	public function save(): bool
	{
		$this->cms->name = 'brève';
		$this->cms->singlePage = false;
		$this->cms->pageTitle = '';
		$this->cms->type = 'markdown';
		$this->cms->private = 0;

		$this->cms->content = $this->content;
		$this->cms->hdateCreat = $this->timestamp;
		$this->cms->categorisation = json_encode($this->buildClassification(), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

		return $this->cms->save();
	}

	private function buildClassification(): array
	{
		return array_filter([
			'categories' => array_filter(preg_split('/\s*;\s*/', $this->categories)),
			'partenaires' => array_filter(array_map('intval', preg_split('/\s*,\s*/', $this->partenaireIds))),
			'ressources' => array_filter(array_map('intval', preg_split('/\s*,\s*/', $this->ressourceIds))),
			'sourcesliens' => array_filter(array_map('intval', preg_split('/\s*,\s*/', $this->sourcelienIds))),
		]);
	}
}
