<?php

namespace models\cms;

use components\SqlHelper;
use Partenaire;
use Ressource;
use Sourcelien;

class BreveIndicateurs
{
	public function countNoMetadata(): int
	{
		return (int) \Yii::app()->db
			->createCommand("SELECT count(*) FROM Cms WHERE name = 'brève' AND categorisation = '{}'")
			->queryScalar();
	}

	/**
	 * @return array<string, int>
	 */
	public function listeCategorie(): array
	{
		$categories = self::listUniqueIds('categories');
		ksort($categories, SORT_STRING | SORT_FLAG_CASE);
		return $categories;
	}

	/**
	 * @return array{id: int, partenaire: ?Partenaire, count: int}[]
	 */
	public function listePartenaire(): array
	{
		$ids = self::listUniqueIds('partenaires');
		if (!$ids) {
			return [];
		}
		$partenaires = SqlHelper::sqlToPairsObject(
			"SELECT * FROM Partenaire WHERE id IN (" . join(",", array_keys($ids)) . ")",
			Partenaire::class
		);
		/** @var Partenaire[] $partenaires */
		$result = [];
		foreach ($ids as $id => $count) {
			$result[] = [
				'id' => (int) $id,
				'partenaire' => $partenaires[$id] ?? null,
				'count' => (int) $count,
			];
		}
		usort(
			$result,
			function ($a, $b) {
				return strcasecmp(str_replace('É', 'E', $a['partenaire']->nom ?? ""), str_replace('É', 'E', $b['partenaire']->nom ?? ""));
			}
		);
		return $result;
	}

	/**
	 * @return array{id: int, ressource: ?Ressource, count: int}[]
	 */
	public function listeRessource(): array
	{
		$ids = self::listUniqueIds('ressources');
		if (!$ids) {
			return [];
		}
		$ressources = SqlHelper::sqlToPairsObject(
			"SELECT * FROM Ressource WHERE id IN (" . join(",", array_keys($ids)) . ")",
			Ressource::class
		);
		/** @var Ressource[] $ressources */
		$result = [];
		foreach ($ids as $id => $count) {
			$result[] = [
				'id' => (int) $id,
				'ressource' => $ressources[$id] ?? null,
				'count' => (int) $count,
			];
		}
		return $result;
	}

	/**
	 * @return array{id: int, sourcelien: ?Sourcelien, count: int}[]
	 */
	public function listeSourcelien(): array
	{
		$ids = self::listUniqueIds('sourcesliens');
		if (!$ids) {
			return [];
		}
		$sourceliens = SqlHelper::sqlToPairsObject(
			"SELECT * FROM Sourcelien WHERE id IN (" . join(",", array_keys($ids)) . ")",
			Sourcelien::class
		);
		/** @var Sourcelien[] $sourceliens */
		$result = [];
		foreach ($ids as $id => $count) {
			$result[] = [
				'id' => (int) $id,
				'sourcelien' => $sourceliens[$id] ?? null,
				'count' => (int) $count,
			];
		}
		return $result;
	}

	/**
	 * @param "categories"|"partenaires"|"ressources"|"sourcesliens" $name
	 * @return array<string, int>
	 */
	private static function listUniqueIds(string $name): array
	{
		$values = \Yii::app()->db
			->createCommand("SELECT JSON_EXTRACT(categorisation, '$.{$name}') FROM Cms WHERE name = 'brève' AND JSON_EXISTS(categorisation, '$.{$name}')")
			->queryColumn();
		$map = [];
		foreach ($values as $json) {
			$names = json_decode($json);
			foreach ($names as $v) {
				if ($v) {
					if (!isset($map[$v])) {
						$map[$v] = 0;
					}
					$map[$v]++;
				}
			}
		}
		return $map;
	}
}
