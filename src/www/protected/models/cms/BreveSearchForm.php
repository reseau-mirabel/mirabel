<?php

namespace models\cms;

use CActiveDataProvider;
use CDbCriteria;
use Cms;
use CSort;

class BreveSearchForm extends \CFormModel
{
	public string $categorie = "";

	public $partenaireId = "";

	public $ressourceId = "";

	public $sourcelienId = "";

	public string $texte = "";

	public bool $rien = false;

	public $startingId = 0;

	public function rules()
	{
		return [
			[['categorie', 'texte'], 'length', 'max' => 250],
			[['partenaireId', 'ressourceId', 'sourcelienId', 'startingId'], 'numerical', 'integerOnly' => true],
			[['rien'], 'boolean'],
		];
	}

	public function attributeLabels(): array
	{
		return [
			'categorie' => "Catégorie",
			'partenaireId' => "Partenaire lié",
			'ressourceId' => "Ressource liée",
			'rien' => "Aucune propriété",
			'sourcelienId' => "Source d'autres liens",
			'texte' => "Le texte contient…",
		];
	}

	public function afterValidate()
	{
		parent::afterValidate();
		$this->startingId = (int) $this->startingId;

		$this->partenaireId = (int) $this->partenaireId;
		if ($this->partenaireId && \Partenaire::model()->findByPk($this->partenaireId) === null) {
			$this->addError('partenaireId', "Ce numéro de partenaire n'est pas dans les données de Mir@bel");
		}

		$this->ressourceId = (int) $this->ressourceId;
		if ($this->ressourceId && \Ressource::model()->findByPk($this->ressourceId) === null) {
			$this->addError('ressourceId', "Ce numéro de ressource n'est pas dans les données de Mir@bel");
		}

		$this->sourcelienId = (int) $this->sourcelienId;
		if ($this->sourcelienId && \Sourcelien::model()->findByPk($this->sourcelienId) === null) {
			$this->addError('sourcelienId', "Ce numéro de source de liens n'est pas dans les données de Mir@bel");
		}

		if ($this->categorie) {
			$exists = \Yii::app()->db
				->createCommand(<<<'EOSQL'
					SELECT 1 FROM Cms WHERE JSON_CONTAINS(categorisation, :categorie, '$.categories') = 1 LIMIT 1
					EOSQL
				)
				->queryScalar([':categorie' => json_encode($this->categorie, JSON_UNESCAPED_UNICODE)]);
			if (!$exists) {
				$this->addError('categorie', "Cette catégorie n'est pas dans les données de Mir@bel");
			}
		}
	}

	public function search($pagination = false): CActiveDataProvider
	{
		$criteria = new CDbCriteria;
		$criteria->addCondition("name = 'brève'");

		if (!$this->validate()) {
			$criteria->condition = "0=1";
			return new CActiveDataProvider(
				new Cms(),
				[
					'criteria' => $criteria,
				]
			);
		}

		if ($this->texte) {
			$criteria->addSearchCondition('content', $this->texte);
		}
		if ($this->startingId > 0) {
			$latest = Cms::model()->findByPk($this->startingId);
			if ($latest) {
				$criteria->addCondition("hdateCreat <= {$latest->hdateCreat}");
			}
		}
		if ($this->rien) {
			$criteria->addCondition("categorisation = '{}'");
		} else {
			if ($this->categorie) {
				$criteria->addCondition("JSON_CONTAINS(categorisation, :categorie, '$.categories')");
				$criteria->params[':categorie'] = json_encode($this->categorie, JSON_UNESCAPED_UNICODE);
			}
			if ($this->partenaireId) {
				$criteria->addCondition("JSON_CONTAINS(categorisation, :pid, '$.partenaires')");
				$criteria->params[':pid'] = (string) $this->partenaireId;
			}
			if ($this->ressourceId) {
				$criteria->addCondition("JSON_CONTAINS(categorisation, :rid, '$.ressources')");
				$criteria->params[':rid'] = (string) $this->ressourceId;
			}
			if ($this->sourcelienId) {
				$criteria->addCondition("JSON_CONTAINS(categorisation, :sid, '$.sourcesliens')");
				$criteria->params[':sid'] = (string) $this->sourcelienId;
			}
		}

		$sort = new CSort();
		$sort->attributes = ['hdateCreat', 'hdateModif'];
		$sort->defaultOrder = 'hdateCreat DESC';

		return new CActiveDataProvider(
			new Cms(),
			[
				'criteria' => $criteria,
				'pagination' => $pagination,
				'sort' => $sort,
			]
		);
	}

	public static function listCategories(): array
	{
		$c = self::listUniqueValues('categories');
		asort($c, SORT_STRING | SORT_FLAG_CASE);
		return $c;
	}

	public static function listPartenaires(): array
	{
		$values = self::listUniqueValues('partenaires');
		if (!$values) {
			return [];
		}
		$ids = join(",", array_map('intval', $values));
		return \components\SqlHelper::sqlToPairs(
			"SELECT id, nom FROM Partenaire WHERE id IN ($ids) ORDER BY nom"
		);
	}

	public static function listRessources(): array
	{
		$values = self::listUniqueValues('ressources');
		if (!$values) {
			return [];
		}
		$ids = join(",", array_map('intval', $values));
		return \components\SqlHelper::sqlToPairs(
			"SELECT id, nom FROM Ressource WHERE id IN ($ids) ORDER BY nom"
		);
	}

	public static function listSources(): array
	{
		$values = self::listUniqueValues('sourcesliens');
		if (!$values) {
			return [];
		}
		$ids = join(",", array_map('intval', $values));
		return \components\SqlHelper::sqlToPairs(
			"SELECT id, nom FROM Sourcelien WHERE id IN ($ids) ORDER BY nom"
		);
	}

	public function exportAttributes(): ?array
	{
		if (!$this->validate()) {
			return null;
		}
		return array_filter($this->getAttributes());
	}

	private static function listUniqueValues(string $name): array
	{
		$values = \Yii::app()->db
			->createCommand("SELECT JSON_EXTRACT(categorisation, '$.{$name}') FROM Cms WHERE name = 'brève' AND JSON_EXISTS(categorisation, '$.{$name}')")
			->queryColumn();
		$set = [];
		foreach ($values as $json) {
			$names = json_decode($json);
			if (!is_array($names)) {
				\Yii::log("Cms.categorisation contains a wrong value '$json'", \CLogger::LEVEL_WARNING);
			}
			foreach ($names as $v) {
				if ($v) {
					$set[$v] = $v;
				}
			}
		}
		return $set;
	}
}
