<?php

use processes\intervention\HtmlHelper;

/**
 * This is the model class for table "Suivi".
 *
 * The followings are the available columns in table 'Suivi':
 * @property int $id
 * @property int $partenaireId
 * @property string $cible
 * @property int $cibleId
 */
class Suivi extends CActiveRecord
{
	public const MAX_INTERVENTIONS = 21; // 1 more than diplayed

	public static $enumCible = ['Revue', 'Ressource', 'Editeur'];

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Suivi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['partenaireId, cible, cibleId', 'required'],
			['partenaireId, cibleId', 'numerical', 'integerOnly' => true],
			['cible', 'in', 'range' => self::$enumCible], // enum
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			['partenaireId, cible, cibleId', 'safe', 'on' => 'search'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'partenaireId' => 'Partenaire',
			'cible' => 'Cible',
			'cibleId' => 'Cible',
		];
	}

	/**
	 * Return a Partenaire record if the object is tracked by someone.
	 *
	 * @param IWithIndirectSuivi $target
	 * @return ?Partenaire
	 */
	public static function isTracked(IWithIndirectSuivi $target): ?Partenaire
	{
		if (!$target->getPrimaryKey()) {
			return null;
		}
		$parents = $target->listParentsForSuivi();
		if (empty($parents)) {
			return null;
		}
		$condition = [];
		foreach ($parents as $parent) {
			$condition[] = sprintf("(s.cible = '%s' AND s.cibleId = %d)", $parent['table'], $parent['id']);
		}
		return Partenaire::model()->findBySql(
			"SELECT p.* FROM Suivi s JOIN Partenaire p ON p.id = s.partenaireId "
			. " WHERE " . join(' OR ', $condition)
			. " LIMIT 1"
		);
	}

	/**
	 * Check if an authentified user has direct access to an object (through Suivi).
	 *
	 * @param IWithIndirectSuivi $target
	 * @return bool direct access?
	 */
	public static function checkDirectAccess(IWithIndirectSuivi $target): bool
	{
		$webuser = Yii::app()->user;
		if ($webuser->isGuest || !$webuser->checkAccess('avec-partenaire')) {
			return false;
		}
		$user = Utilisateur::model()->findByPk(Yii::app()->user->id);
		// modifier un Editeur de type "partenaire-editeur", seulement par l'éditeur lui-même ou si suiviPartenairesEditeurs
		if ($target instanceof Editeur && $target->partenaire) {
			return ($user->partenaireId == $target->partenaire->id) || $webuser->checkAccess('suiviPartenairesEditeurs');
		}
		$suivi = $webuser->suivi;
		$parents = $target->listParentsForSuivi();
		$condition = [];
		// Check if the object has no access restriction => OK
		if (empty($parents)) {
			return ($user->partenaire->type !== 'editeur');
		}
		// Check if the object "belongs to" the user     => OK
		foreach ($parents as $parent) {
			if (
				isset($suivi[$parent['table']]) && !empty($parent['id'])
				&& in_array($parent['id'], $suivi[$parent['table']])
			) {
				return true;
			}
			if (!empty($parent['id'])) {
				$condition[] = "(cible = '{$parent['table']}' AND cibleId = {$parent['id']})";
			}
		}
		if ($user->partenaire && $user->partenaire->type === 'editeur') {
			// Un partenaire-éditeur ne peut modifier un objet qu'il ne suit pas
			return false;
		}
		if (empty($condition)) {
			// creation
			return true;
		}
		// Check if the object "belongs to" someone else => DENIED
		if (self::model()->exists(join(' OR ', $condition))) {
			return false;
		}
		// No one has any right on this object           => OK
		return true;
	}

	/**
	 * Check if an authentified user has direct access to an object defined by 2 IDs.
	 *
	 * @param ?int $revueId Id or null.
	 * @param ?int $ressourceId Id or null.
	 * @return bool direct access?
	 */
	public static function checkDirectAccessByIds(?int $revueId, ?int $ressourceId): bool
	{
		$webuser = Yii::app()->user;
		if ($webuser->isGuest) {
			return false;
		}

		// void context
		if (empty($revueId) && empty($ressourceId)) {
			return ($webuser->partenaireType !== 'editeur');
		}

		// Ressource or Revue context
		$suivi = $webuser->suivi;
		$condition = [];
		// Check if the object "belongs to" the user     => OK
		if (!empty($revueId)) {
			if (isset($suivi['Revue']) && in_array($revueId, $suivi['Revue'])) {
				return true;
			}
			$condition[] = "(cible = 'Revue' AND cibleId = {$revueId})";
		}
		if (!empty($ressourceId)) {
			if (isset($suivi['Ressource']) && in_array($ressourceId, $suivi['Ressource'])) {
				return true;
			}
			$condition[] = "(cible = 'Ressource' AND cibleId = {$ressourceId})";
		}
		// => The current user has no Suivi record on this object.

		// Check if the object "belongs to" someone else => DENIED
		if (self::model()->exists(join(' OR ', $condition))) {
			return false;
		}

		// No one has any right on this object           => OK unless current Partenaire is 'éditeur'
		return ($webuser->partenaireType !== 'editeur');
	}

	public static function listInterventionsByInstitute(int $since, ?int $before = 0, bool $excludeImport = true): array
	{
		$cond = "statut = 'attente' AND hdateProp > " . (int) $since
			. ($before ? ' AND hdateProp < ' . (int) $before : '')
			. ($excludeImport ? " AND i.import = 0 " : '');
		$sql = <<<EOSQL
			SELECT s.partenaireId, i.*
			FROM Intervention i
			JOIN Suivi s ON s.cible = 'Revue' AND i.revueId = s.cibleId
			WHERE $cond

			UNION

			SELECT s.partenaireId, i.*
			FROM Intervention i
			JOIN Suivi s ON s.cible = 'Ressource' AND i.ressourceId = s.cibleId
			WHERE $cond

			UNION

			SELECT s.partenaireId, i.*
			FROM Intervention i
			JOIN Suivi s ON s.cible = 'Editeur' AND i.editeurId = s.cibleId
			WHERE $cond

			ORDER BY partenaireId, hdateProp ASC
			EOSQL;

		$cmd = Yii::app()->db->createCommand($sql);
		$list = [];
		foreach ($cmd->query() as $row) {
			if (!isset($list[$row['partenaireId']])) {
				$list[$row['partenaireId']] = [];
			}
			$list[$row['partenaireId']][] = Intervention::model()->populateRecord($row);
		}
		return $list;
	}

	public static function listInterventionsTrackedByUser(WebUser $user): array
	{
		$intvByCategory = [
			'En attente' => HtmlHelper::listInterventionsSuivi($user->partenaireId, 'attente', 0, 0),
			'Validées' => HtmlHelper::listInterventionsSuivi($user->partenaireId, 'accepté', (int) strtotime("1 month ago"), self::MAX_INTERVENTIONS),
		];
		if ($user->getState('suiviEditeurs')) {
			$intvByCategory['Éditeurs A'] = HtmlHelper::listInterventionsEditeurs('attente', self::MAX_INTERVENTIONS);
			$intvByCategory['Éditeurs V'] = HtmlHelper::listInterventionsEditeurs('accepté', self::MAX_INTERVENTIONS);
		}

		if ($user->getState('suiviNonSuivi')) {
			$intvByCategory['Non-suivi A'] = Suivi::listInterventionsNotTracked('attente', (int) strtotime("1 year ago"), 0, true, self::MAX_INTERVENTIONS);
			$intvByCategory['Non-suivi V'] = Suivi::listInterventionsNotTracked('accepté', (int) strtotime("3 month ago"), 0, true, self::MAX_INTERVENTIONS);
		}

		if ($user->getState('suiviPartenairesEditeurs')) {
			$intvByCategory['Partenaires-Éditeurs A'] = HtmlHelper::listInterventionsPartenairesEditeurs('attente', self::MAX_INTERVENTIONS);
			$intvByCategory['Partenaires-Éditeurs V'] = HtmlHelper::listInterventionsPartenairesEditeurs('accepté', self::MAX_INTERVENTIONS);
		}
		return $intvByCategory;
	}

	public static function listInterventionsNotTracked(string $statut, int $since, int $before = 0, bool $excludeImport = true, int $limit = 0): array
	{
		// exclude any Intervention PartenairesEditeurs
		$excludeIds = Yii::app()->db->createCommand(
			"SELECT i.id "
			. " FROM Intervention i JOIN Utilisateur u ON utilisateurIdProp = u.id JOIN Partenaire p ON p.id = u.partenaireId"
			. " WHERE p.type = 'editeur' AND i.statut = :statut AND i.hdateProp > :since "
			. ($before ? ' AND hdateProp < ' . (int) $before : '')
		)->queryColumn([':statut' => $statut, ':since' => (int) $since]);
		if (!isset(Intervention::STATUTS[$statut])) {
			throw new Exception("statut non valide");
		}
		$cond = "i.statut = '$statut' AND i.hdateProp > $since"
			. ($before ? ' AND hdateProp < ' . (int) $before : '')
			. ($excludeImport ? " AND i.import = 0 " : '')
			. ($excludeIds ? " AND i.id NOT IN (" . join(",", $excludeIds) . ") " : "");
		$sqlLimit = $limit > 0 ? "LIMIT " . (int) $limit : "";
		$sql = <<<EOSQL
			SELECT *
			FROM Intervention i
			WHERE $cond
			    AND (editeurId IS NULL OR editeurId NOT IN (SELECT cibleId FROM Suivi WHERE cible = 'Editeur'))
			    AND (ressourceId IS NULL OR ressourceId NOT IN (SELECT cibleId FROM Suivi WHERE cible = 'Ressource'))
			    AND (revueId IS NULL OR revueId NOT IN (SELECT cibleId FROM Suivi WHERE cible = 'Revue'))
			ORDER BY hdateProp ASC
			$sqlLimit
			EOSQL;
		return Intervention::model()->findAllBySql($sql);
	}

	public static function listInterventionsPartenairesEditeurs(int $since, int $before): array
	{
		$uIds = Yii::app()->db
			->createCommand("SELECT u.id FROM Partenaire p JOIN Utilisateur u ON u.partenaireId = p.id WHERE p.type = 'editeur'")
			->queryColumn();
		if (!$uIds) {
			return [];
		}
		$sql = "SELECT i.* "
			. " FROM Intervention i "
			. " WHERE i.statut = 'attente' AND i.hdateProp > " . (int) $since
			. ($before ? ' AND hdateProp < ' . (int) $before : '')
			. " AND utilisateurIdProp IN (" . join(',', $uIds) . ")"
			. " ORDER BY i.hdateProp ASC";
		return Intervention::model()->findAllBySql($sql);
	}

	/**
	 * Liste [id, partenaireId] pour les cibles suivies par d'autres partenaires.
	 *
	 * @param int $partenaireId
	 * @param string $target
	 * @return array
	 */
	public static function findDuplicates(int $partenaireId, string $target): array
	{
		$sql = <<<EOSQL
			SELECT s.cibleId, p.id, p.nom, p.sigle, p.prefixe, p.type
			FROM Suivi s
			    JOIN Partenaire p ON s.partenaireId = p.id
			    JOIN Suivi s2 ON s.cible = s2.cible AND s.cibleId = s2.cibleId
			WHERE s2.cible = :target AND s2.partenaireId = :id AND s.partenaireId <> s2.partenaireId
			EOSQL;
		$query = Yii::app()->db
			->createCommand($sql)
			->queryAll(true, [':target' => $target, ':id' => $partenaireId]);
		$r = [];
		foreach ($query as $row) {
			$id = (int) $row['cibleId'];
			unset($row['cibleId']);
			$r[$id][] = $row;
		}
		return $r;
	}
}
