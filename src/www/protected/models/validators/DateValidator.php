<?php

namespace models\validators;

class DateValidator extends \CValidator
{
	public $onlyFullDate = false;

	public $nothingAfter = false;

	public $padding = false;

	/**
	 * @var int Reject date older than this year.
	 */
	public $modernDate = 1050;

	/**
	 * Called automatically before the validation of the owner's attributes.
	 *
	 * @param \CModel $object
	 * @param string $attribute
	 */
	public function validateAttribute($object, $attribute)
	{
		if ($object->{$attribute} === null || trim($object->{$attribute}) === '') {
			return;
		}
		$value = str_replace(
			['/', "\xFE\xFF", "\xEF\xBB\xBF"],
			['-', '', ''],
			trim($object->{$attribute}) . ' '
		);
		$match = [];
		// ISO
		if (preg_match('/^(\d{4})(-\d\d)?(-\d\d)?\s+(.*?)$/', $value, $match)) {
			[, $year, $month, $day, $comment] = $match;
		} elseif (preg_match('/^(\d\d-)?(\d\d-)?(\d{4})\s+(.*?)$/', $value, $match)) {
			[, $day, $month, $year, $comment] = $match;
			if (empty($month)) {
				$month = $day;
				$day = '';
			}
		} else {
			$object->addError($attribute, "Format de date non valide (YYYY-mm-dd ou dd/mm/YYYY).");
			return;
		}
		$month = trim($month, '-');
		$day = trim($day, '-');
		if (!checkdate((int) ($month ?: 1), (int) ($day ?: 1), (int) $year)) {
			$object->addError($attribute, 'Date non valide mais de format correct.');
			return;
		}
		if ($this->onlyFullDate && (!$day || !$month)) {
			$object->addError($attribute, 'La date est incomplète (YYYY-mm-dd ou dd/mm/YYYY).');
			return;
		}
		if ($this->modernDate && $year < $this->modernDate) {
			$object->addError($attribute, "La date est trop ancienne, elle doit être postérieure à {$this->modernDate}.");
			return;
		}
		if ($this->nothingAfter && $comment) {
			$object->addError($attribute, 'La date est suivie de texte excédentaire.');
			return;
		}
		if ($this->padding) {
			$object->{$attribute} = trim(sprintf('%4d-%02d-%02d %s', $year, $month, $day, $comment));
		} else {
			if (empty($month)) {
				$object->{$attribute} = trim(sprintf('%4d %s', $year, $comment));
			} elseif (empty($day)) {
				$object->{$attribute} = trim(sprintf('%4d-%02d %s', $year, $month, $comment));
			} else {
				$object->{$attribute} = trim(sprintf('%4d-%02d-%02d %s', $year, $month, $day, $comment));
			}
		}
	}

	/**
	 * Converts a (possibly incomplete) date to a timestamp.
	 *
	 * @param string $datePart Date to convert.
	 * @param bool $endDate If true, complete the date at the end of the period it covers.
	 * @throws \Exception if the date is invalid
	 * @return int Timestamp.
	 */
	public static function convertdateToTs($datePart, bool $endDate): int
	{
		if (!$datePart) {
			return ($endDate ? $_SERVER['REQUEST_TIME'] : 0);
		}
		if (preg_match('/^\d{5,}$/', $datePart) && $datePart <= time()) {
			return (int) $datePart; // Already a timestamp
		}
		if (preg_match('#^(\d\d)/(\d\d)/(\d{4})\b#', $datePart, $m)) {
			$datePart = "{$m[3]}-{$m[2]}-{$m[1]}";
		}
		if (preg_match('#^(\d\d)/(\d{4})\b#', $datePart, $m)) {
			$datePart = "{$m[2]}-{$m[1]}";
		}
		if (!preg_match('/^(\d{4})(-\d\d)?(-\d\d)?/', $datePart, $m)) {
			throw new \Exception('Invalid date format.');
		}
		if (empty($m[2])) {
			$m[2] = $endDate ? 12 : 1;
		} else {
			$m[2] = ltrim($m[2], '-');
		}
		if ($endDate) {
			// end date, hard work
			if (empty($m[3])) {
				if ($m[2] == 12) {
					return mktime(24, 0, 0, 12, 31, (int) $m[1]);
				}
				return (int) strtotime(
					"-1 day",
					strtotime(sprintf("%d-%02d-01 24:00:00", $m[1], $m[2]+1))
				);
			}
			$m[3] = ltrim((string) $m[3], '-');
			return (int) mktime(24, 0, 0, (int) $m[2], (int) $m[3], (int) $m[1]);
		}
		// start date, easy
		if (empty($m[3])) {
			$m[3] = 1;
		} else {
			$m[3] = ltrim((string) $m[3], '-');
		}
		return (int) mktime(0, 0, 0, (int) $m[2], (int) $m[3], (int) $m[1]);
	}

	/**
	 * Returns a SQL condition (to use with CDbCriteria) from a date input.
	 *
	 * Example:
	 * ("hdateModif", "= 2001") ---> "hdateModif BETWEEN TS<2001-01-01> AND TS<2001-12-32>"
	 *
	 * @param string $column
	 * @param string $input
	 * @return string SQL condition
	 */
	public static function buildTsConditionFromDateValue(string $column, string $input)
	{
		if (empty($input)) {
			return '';
		}
		$operation = "=";
		$m = [];
		if (preg_match('/^(\d{4,}.+)\s*-\s*(\d{4,}.+)$/', trim($input), $m)) {
			return "$column BETWEEN " . self::convertdateToTs($m[1], false)
				. " AND " . self::convertdateToTs($m[2], true);
		}
		if (preg_match('/^\s*(=|<|>|<=|>=|!=|<>)\s*(\d.+)$/', $input, $m)) {
			$operation = $m[1];
			$input = $m[2];
		}
		$date = trim($input, " \t <>=!");
		try {
			switch ($operation) {
				case '!=':
				case '<>':
					return "$column NOT BETWEEN " . self::convertdateToTs($date, false)
						. " AND " . self::convertdateToTs($date, true);
				case '<':
				case '<=':
					return "$column $operation " . self::convertdateToTs($date, $operation != '<');
				case '>':
				case '>=':
					return "$column $operation " . self::convertdateToTs($date, $operation == '>');
				default:
					return "$column BETWEEN " . self::convertdateToTs($date, false)
						. " AND " . self::convertdateToTs($date, true);
			}
		} catch (\Throwable $e) {
			if (class_exists('Yii')) {
				\Yii::log("Invalid date format '$date' for column '$column': {$e->getMessage()}", 'info');
			}
			return '0';
		}
	}
}
