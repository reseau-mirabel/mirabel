<?php

namespace models\validators;

class PpnValidator extends \CValidator
{
	public $enableClientValidation = false;

	public static function check(string $n): bool
	{
		if (!preg_match('/^\d{8}[\dX]$/', $n)) {
			return false;
		}
		$parts = str_split($n);
		$key = array_pop($parts);
		if ($key === 'X' || $key === 'x') {
			$key = 10;
		} else {
			$key = (int) $key;
		}

		$sum = 0;
		foreach ($parts as $i => $p) {
			$sum += ((int) $p * (count($parts) - $i + 1)) % 11;
		}
		$sum = (11 - ($sum % 11)) % 11;
		return $sum === $key;
	}

	/**
	 * Validates the attribute of the object.
	 *
	 * If there is any error, the error message is added to the object.
	 *
	 * @param \CModel $object the object being validated.
	 * @param string $attribute the attribute being validated.
	 */
	protected function validateAttribute($object, $attribute): void
	{
		if ($this->isEmpty($object->{$attribute})) {
			return;
		}
		$ppn = strtoupper(trim($object->{$attribute}));
		$object->{$attribute} = $ppn;
		if (!preg_match('/^\d{8}[\dX]$/', $ppn)) {
			$object->addError($attribute, "Cet identifiant est mal-formé.");
			return;
		}
		if (!self::check($ppn)) {
			$object->addError($attribute, "Cet identifiant est mal-formé (clé incorrecte).");
		}
	}
}
