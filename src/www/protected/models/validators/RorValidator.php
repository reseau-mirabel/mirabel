<?php

namespace models\validators;

class RorValidator extends \CValidator
{
	/**
	 * Convertit un caractère en une valeur numérique en base 10 selon l'encodage Crockford 32.
	 *
	 * @param string $char
	 * @throws \InvalidArgumentException Si le caractère n'est pas valide pour l'encodage Crockford 32.
	 * @return int
	 */
	protected static function charToValue($char): int
	{
		$chars = "0123456789abcdefghjkmnpqrstvwxyz";
		$value = strpos($chars, $char);
		if ($value === false) {
			throw new \InvalidArgumentException(
				"Le caractère '{$char}' n'est pas valide pour l'encodage de crockford."
			);
		}
		return $value;

	}

	/**
	 * Calcule le checksum d'un identifiant ROR donné. 98 - (base10IdValue * 100) % 97
	 *
	 * @param string $id les charactères [1:7] de l'id ROR à calculer.
	 * @return int
	 */
	protected static function computeRorChecksum($id)
	{
		$base10IdValue = 0;
		for ($i = 0; $i < strlen($id); $i++) {
			$value = self::charToValue($id[$i]);
			$base10IdValue = $base10IdValue * 32 + $value;
		}
		return 98 - ($base10IdValue * 100 % 97);
	}

	/**
	 * Vérifie si le checksum est valide.
	 * @param $ror
	 * @return bool
	 */
	protected static function check($ror): bool
	{
		$id = substr($ror, 1, 6);
		$checksum = substr($ror, 7, 2);
		$calculatedChecksum = self::computeRorChecksum($id);
		if ((int) $checksum !== $calculatedChecksum) {
			return false;
		}
		return true;
	}

	/**
	 * Validate the attribute on the object
	 *
	 * If there is any error, the message is added to the object
	 *
	 * @param \CModel $object the object being validated.
	 * @param string $attribute the attribute being validated.
	 */
	protected function validateAttribute($object, $attribute): void
	{
		if ($this->isEmpty($object->{$attribute})) {
			return;
		}
		$ror = (trim($object->{$attribute}));
		$object->{$attribute} = $ror;
		if (!preg_match('/^0[a-hj-km-np-tv-z|0-9]{6}[0-9]{2}$/', $ror)) {
			$object->addError($attribute, "Cet identifiant est mal-formé.");
			return;
		}
		if (!self::check($ror)) {
			$object->addError($attribute, "Cet identifiant est mal-formé (clé incorrecte).");
		}
	}
}
