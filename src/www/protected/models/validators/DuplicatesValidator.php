<?php

namespace models\validators;

use CActiveRecord;
use IUserCanConfirm;
use processes\completion\Completer;
use Yii;

class DuplicatesValidator extends \CValidator
{
	public const PATTERN = "Avertissement : il s'agit peut-être d'un doublon";

	/**
	 * @param \CModel $object
	 * @param string $attribute
	 * @return void
	 */
	public function validateAttribute($object, $attribute)
	{
		$text = (string) $object->{$attribute};
		if (empty($text)) {
			return;
		}
		if (!($object instanceof CActiveRecord) || !($object instanceof IUserCanConfirm) || !Completer::existsFor(get_class($object))) {
			Yii::log("Cannot find duplicates on this class: " . get_class($object), 'warning');
			return;
		}
		if ($object->getScenario() == 'reprise') {
			return;
		}
		if ($object->getConfirm()) {
			// Already confirmed.
			return;
		}
		$sphinx = Yii::app()->getComponent('sphinx');
		if (!($sphinx instanceof \CDbConnection) || (!$sphinx->setActive(true) && !$sphinx->active)) {
			// Cannot find duplicates without a search engine.
			return;
		}

		$pk = (int) $object->getPrimaryKey();
		if (($object instanceof \CActiveRecord) && !$object->getIsNewRecord()) {
			$oldField = CActiveRecord::model(get_class($object))->findByPk($pk)->getAttribute($attribute);
			if ($oldField === $text) {
				// The record is already in the DB with this same value.
				return;
			}
		}

		$dupl = self::findDuplicates($object, $text, $pk);
		if ($dupl) {
			$object->addError($attribute, self::getErrorMessage($dupl));
			$object->setConfirm(false);
		}
	}

	private static function findDuplicates(object $object, string $text, int $pk): array
	{
		return array_filter(
			Completer::type(get_class($object))->completeTerm($text),
			function ($x) use ($pk) {
				return empty($x['id']) || ((int) $x['id']) !== $pk;
			}
		);
	}

	private static function getErrorMessage(array $duplicates): string
	{
		$list = '<ol>';
		foreach ($duplicates as $d) {
			if ($d['id']) {
				$list .= '<li>' . htmlspecialchars($d['label']) . '</li>';
			} else {
				$list .= '<li>' . $d['label'] . '</li>';
			}
		}
		$list .= '</ol>';
		return self::PATTERN
			. ", veuillez vérifier avec les éléments comparés."
			. " S'il ne s'agit pas d'un doublon, il vous suffit de cocher la case de confirmation ci-bas. $list";
	}
}
