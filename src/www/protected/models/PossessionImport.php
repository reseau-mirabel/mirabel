<?php

/**
 * PossessionImport class.
 */
class PossessionImport extends CFormModel
{
	public $partenaireId;

	public $importfile;

	public $separator = ";";

	public $simulation = true;

	public $overwrite = false;

	public static $columns = [
		'possession', 'bouquet', 'titre',
		'titre ID', 'issn', 'issne', 'issnl', 'revue ID', 'identifiant local',
	];

	public array $actions = []; // [#ligne, titre, action, "class" => ...]

	public array $importedRows = [];

	public array $currentRow = [];

	protected int $rowPos;

	/**
	 * Declares the validation rules.
	 *
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return [
			['importfile', 'file', 'types' => 'csv'],
			['separator', 'length', 'max' => 1],
			['simulation, overwrite', 'boolean'],
		];
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return [
			'importfile' => 'Fichier CSV',
			'separator' => 'Séparateur de champs',
			'simulation' => 'Simulation',
			'overwrite' => "Modification de l'existant",
		];
	}

	/**
	 * Imports a CSV file for the current Partenaire.
	 */
	public function import(string $file): bool
	{
		if ($this->hasErrors()) {
			return false;
		}
		$this->actions = [];
		$this->importedRows = [];
		$handle = fopen($file, "r");
		if ($handle === false) {
			$this->addError('importfile', "Erreur en lisant le fichier déposé.");
			return false;
		}
		fgetcsv($handle, 0, $this->separator); // skip first line
		$this->rowPos = 1;
		while (($data = fgetcsv($handle, 0, $this->separator)) !== false) {
			if (count($data) <= 1) {
				$this->rowPos++;
				continue; // skip empty lines
			}
			if (count($data) === 8) {
				$data[] = '';
			}
			if (count($data) !== 9) {
				throw new Exception(
					"Import : erreur ligne {$this->rowPos}, 8 colonnes attendues, "
					. count($data) . " obtenues. Titre = " . ($data[2] ?? '')
				);
			}
			$row = array_combine(self::$columns, array_map('trim', $data));
			$row['issn'] = preg_replace('/[^\dXx-]/', '', $row['issn']);
			$row['issnl'] = preg_replace('/[^\dXx-]/', '', $row['issnl']);
			$this->importRow($row);
			$this->rowPos++;
		}
		return true;
	}

	/**
	 * Import a line from the CSV file.
	 */
	protected function importRow(array $row): bool
	{
		$this->currentRow = $row;
		$titres = $this->findTitles($row);
		if (count($titres) > 1) {
			$this->addAction(Possession::IMPORT_ERROR, "Plusieurs titres correspondent", "Critères : " . join(" ; ", $row));
			return false;
		}
		if (!$titres) {
			$this->addAction(Possession::IMPORT_ERROR, "Aucun titre ne correspond", "Critères : " . join(" ; ", $row));
			return false;
		}

		$this->currentRow['titre ID'] = (int) $titres[0]->id;
		$this->currentRow['revue ID'] = (int) $titres[0]->revueId;

		$possession = new Possession;
		$possession->partenaireId = $this->partenaireId;
		$possession->titreId = $titres[0]->id;
		$possession->identifiantLocal = $row['identifiant local'];
		$possession->bouquet = $row['bouquet'];

		if ($row['possession']) {
			$possession->overwrite = $this->overwrite;
			$returnCode = $possession->importAdd($this->simulation);
		} else {
			$returnCode = $possession->importDelete($this->simulation);
		}
		$this->addAction($returnCode, $titres[0]->getFullTitle(), '');
		return true;
	}

	/**
	 * @return Titre[]
	 */
	protected function findTitles(array $criteria): array
	{
		$sql = "SELECT t.* FROM Titre t ";
		$joins = [];
		$where = [];
		$params = [];
		if ($criteria['titre ID'] > 0) {
			$where[] = "t.id = :titreId";
			$params[':titreId'] = (int) $criteria['titre ID'];
		} else {
			if ($criteria['revue ID'] > 0) {
				$where[] = "t.revueId = :revueId";
				$params[':revueId'] = (int) $criteria['revue ID'];
			}
			if (!empty($criteria['issn'])) {
				$joins[] = "LEFT JOIN Issn i ON t.id = i.titreId";
				$where[] = "i.issn = :issn";
				$params[':issn'] = strlen($criteria['issn']) === 9 ?
					$criteria['issn']
					: substr($criteria['issn'], 0, 4) . "-" . substr($criteria['issn'], 4);
			} elseif (!empty($criteria['issne'])) {
				$joins[] = "LEFT JOIN Issn i ON t.id = i.titreId";
				$where[] = "i.issn = :issne";
				$params[':issne'] = strlen($criteria['issne']) === 9 ?
					$criteria['issne']
					: substr($criteria['issne'], 0, 4) . "-" . substr($criteria['issne'], 4);
			}
			if (!empty($criteria['issnl'])) {
				$joins[] = "LEFT JOIN Issn i ON t.id = i.titreId";
				$where[] = "i.issnl = :issnl";
				$params[':issnl'] = strlen($criteria['issnl']) === 9 ?
					$criteria['issnl']
					: substr($criteria['issnl'], 0, 4) . "-" . substr($criteria['issnl'], 4);
			}
		}
		if (!$where) {
			return [];
		}
		if ($joins) {
			$sql .= join(' ', array_unique($joins));
		}
		$sql .= " WHERE " . join(' AND ', $where);
		if ($joins) {
			$sql .= " GROUP BY t.id";
		}
		return Titre::model()->findAllBySql($sql, $params);
	}

	/**
	 * Adds an action log for the current line according to the return code.
	 *
	 * @param string $extra info about the error
	 * @throws Exception
	 */
	protected function addAction(int $returnCode, string $title, string $extra): void
	{
		$msg = Possession::getMsgFromCode($returnCode);
		if (!$msg && $returnCode === Possession::IMPORT_ERROR) {
			$msg = "Erreur d'écriture dans la base de données";
		}
		$this->currentRow = array_values($this->currentRow);
		$this->currentRow['titre M'] = $title;
		$this->currentRow['message'] = $msg;
		switch ($returnCode) {
			case Possession::IMPORT_NOTFOUND:
				$this->addError(
					'importfile',
					"Impossible de supprimer une possession inexistante : ligne {$this->rowPos}"
				);
				$this->currentRow['status'] = 'error';
				break;
			case Possession::IMPORT_ERROR:
				$this->currentRow['titre M'] = '';
				$this->currentRow['message'] .= ". $title";
				$this->addError(
					'importfile',
					"En ligne {$this->rowPos}, $msg" . ($title ? " : $title. $extra" : "")
				);
				$this->currentRow['status'] = 'error';
				break;
			case Possession::IMPORT_NOCHANGE:
				$this->currentRow['status'] = 'success unchanged';
				break;
			case Possession::IMPORT_UPDATE_NOTALLOWED:
			case Possession::IMPORT_INSERT_NOTALLOWED:
				$this->currentRow['status'] = 'warning';
				break;
			case Possession::IMPORT_ADDED:
			case Possession::IMPORT_UPDATED:
			case Possession::IMPORT_DELETED:
				$this->currentRow['status'] = 'success modified';
				break;
			default:
				throw new Exception("Unknown return code from Possession.");
		}
		$this->importedRows[] = $this->currentRow;
	}
}
