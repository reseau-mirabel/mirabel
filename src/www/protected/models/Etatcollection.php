<?php

namespace models;

/**
 * The followings are the available columns in table 'Partenaire':
 * @property int $id
 * @property int $issnId
 * @property string $rcr
 * @property string $etats json in db
 * @property string $lastImport
 */
class Etatcollection extends \CActiveRecord
{
	public function tableName()
	{
		return 'Etatcollection';
	}

	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'issn' => 'ISSN',
			'etats' => 'États de collection',
			'lastImport' => 'Date du dernier import',
		];
	}

	/**
	 * Remplace en base de donnée une liste d'état de collection pour un couple ISSN, RCR donné, met à jour le timestamp lastImport en même temps.
	 * @param array<string, string[]> $rcrEtats
	 * @throws \CHttpException
	 */
	public static function remplaceEtatcollection(int $issnId, array $rcrEtats): void
	{
		if (!$rcrEtats) {
			return;
		}
		$values = [];
		foreach ($rcrEtats as $rcr => $etats) {
			$etatsJson = \Yii::app()->db->quoteValue(json_encode($etats, JSON_UNESCAPED_UNICODE));
			$quotedRcr = \Yii::app()->db->quoteValue($rcr);
			$values[] = "({$issnId}, {$quotedRcr}, {$etatsJson}, CURRENT_TIMESTAMP)";
		}
		$valuesString = implode(", ", $values);
		$command = \Yii::app()->db->createCommand(<<<EOSQL
			REPLACE INTO Etatcollection (issnId, rcr, etats, lastImport)
			VALUES {$valuesString};
			EOSQL
		)->execute();
	}
}
