<?php

use models\wikidata\Cache;
use models\wikidata\Compare;

/**
 * @todo Vue matrice de synthèse : en ligne les propriétes, en colonnes les différences-type, compte dans chaque cellule
 */

/**
 * This is the model class for table "Wikidata".
 *
 * @property int $id
 * @property int $revueId
 * @property int $titreId
 * @property string  $qId       eg. Q3428731
 * @property string  $property eg. "P2733" (identifiant Persée)
 * @property int $propertyType  eg. 1 (voir WikidataComp)
 * @property string  $value     eg. "rfsp"
 * @property string  $url       eg. "https://www.persee.fr/collection/rfsp"
 * @property string  $object    eg. "Q12345"
 * @property string  $objectLabel
 * @property int $hdate timestamp
 * @property ?int $comparison See constants COMP_*, with NULL more or less the same as 0?
 * @property string  $compUrl Mirabel value to compare
 * @property string  $compDetails url components (json serialization)
 * @property int $compDate comparison timestamp
 *
 * @property Revue $revue
 * @property Titre $titre
 * @property Suivi[] $suivis
 */

class Wikidata extends CActiveRecord
{
	public const WD_BASE_URL = 'https://www.wikidata.org/wiki';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Wikidata';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['property, propertyType', 'safe', 'on' => 'search'],
			['comparison', 'safe', 'on' => 'search'],
			['value', 'safe', 'on' => 'search'],
		];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'revue' => [self::BELONGS_TO, 'Revue', 'revueId'],
			'titre' => [self::BELONGS_TO, 'Titre', 'titreId'],
			'suivis' => [
				self::HAS_MANY,
				'Suivi', // in the SQL, aliased to the relation's name
				['cibleId' => 'revueId'], // foreign column => our column (witn a string instead, our PK would be implicit)
				'on' => "suivis.cible = 'Revue'", // additional condition added to the ON
				'select' => "suivis.partenaireId", // fetch less data than *
			],
		];
	}

	public function getRevueLink(): string
	{
		if (isset($this->revue)) {
			return $this->revue->getSelfLink();
		}
		return sprintf('revue-%d disparue', $this->revueId);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'revueId' => 'Revue',
			'titreId' => 'Titre',
			'qId' => 'QID',
			'property' => 'Propriété',
			'propertyType' => 'Type propriété',
			'value' => 'Valeur', //valeur brute
			'url' => 'URL',
			'object' => 'Objet',
			'objectLabel' => 'Intitulé objet',
			'hdate' => "Dernier import",
			'comparison' => "Comparaison",
			'compUrl' => "Url Mir@bel",
			'compDetails' => "Détails",
			'compDate' => "Date comparaison",
		];
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @param int|bool $pageSize Number of items for pagination (false to disable pagination).
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($pageSize=100)
	{
		$criteria = new CDbCriteria;
		$criteria->with = ['suivis'];

		$criteria->compare('id', $this->id);
		$criteria->compare('revueId', $this->revueId);
		$criteria->compare('titreId', $this->titreId);
		$criteria->compare('qId', $this->qId);
		if (empty($this->property)) {
			$criteria->addCondition('0=1');
		} else {
			$criteria->compare('property', $this->property);
		}
		$criteria->compare('propertyType', $this->propertyType);
		$criteria->compare('LOWER(value)', strtolower((string) $this->value), true);
		$criteria->compare('url', $this->url, true);
		$criteria->compare('hdate', $this->hdate);
		$criteria->compare('comparison', $this->comparison);
		$criteria->compare('compUrl', $this->compUrl, true);
		$criteria->compare('compDate', $this->compDate, true);
		$criteria->compare('propertyType', '> 0');

		$sort = new CSort();
		$sort->attributes = [
			'id', 'revueId', 'titreId', 'qId', 'property', 'propertyType',  'value', 'url', 'hdate',
			'comparison', 'compUrl', 'compDate',
		];
		$sort->defaultOrder = sprintf(
			"propertyType ASC, property ASC, FIND_IN_SET(comparison, '%s'), revueId ASC",
			Compare::COMP_DEFAULT_ORDER
		);
		return new CActiveDataProvider(
			$this,
			[
				'criteria' => $criteria,
				'pagination' => ['pageSize' => $pageSize],
				'sort' => $sort,
			]
		);
	}

	/**
	 * Liste les incohérences détectées entre Wikidata et Mir@bel
	 * @param int $pageSize
	 * @return \CActiveDataProvider
	 */
	public function searchInconsistencies($pageSize = 100)
	{
		$criteria = new CDbCriteria;

		$criteria->compare('property', 'P236'); // issn, choix arbitraire de la propriété support des erreurs graves
		$criteria->compare('comparison', $this->comparison);
		$criteria->compare('comparison', '> 1');
		$criteria->compare('LOWER(value)', strtolower((string) $this->value), true);
		$sort = new CSort();
		$sort->attributes = [
			'id', 'revueId', 'titreId', 'qId', 'property', 'propertyType',  'value', 'url', 'hdate',
			'comparison', 'compUrl', 'compDate',
		];
		$sort->defaultOrder = 'comparison DESC, revueId ASC';

		return new CActiveDataProvider(
			$this,
			[
				'criteria' => $criteria,
				'pagination' => ['pageSize' => $pageSize],
				'sort' => $sort,
			]
		);
	}

	/**
	 * return property code and name
	 * @return string
	 */
	public function getExplicitProperty(): string
	{
		$properties = Cache::getWdProperties();
		if (isset($properties[$this->property])) {
			return sprintf('%s %s', $this->property, $properties[$this->property]);
		}
		return $this->property ?: "";
	}

	/**
	 * return a complete link from the url
	 * @return string HTML
	 */
	public function getUrlLink(): string
	{
		if (isset($this->url)) {
			return CHtml::link($this->url, $this->url);
		}
		return '';
	}

	/**
	 * @param bool $link  true=>link, false=>url only
	 * @return string
	 */
	public function getQidLink($link = true): string
	{
		$url = sprintf('%s/%s', self::WD_BASE_URL, $this->qId);
		if ($link) {
			return CHtml::link($this->qId, $url);
		}
		return $url;
	}

	/**
	 * Est-ce que cette entrée de cache Wikidata est comparable avec la donnée Mir@bel ?
	 */
	public function isComparable(): bool
	{
		return (bool) $this->comparison;
	}

	/**
	 * Est-ce que cette entrée de cache devrait proposer l'édition du lien extérieur ? (absent ou différent)
	 */
	public function isEditable(): bool
	{
		return (
			(bool) $this->comparison
			&& $this->comparison < Compare::COMP_ISSN_INCONSISTENT
			&& Compare::getCompLevel($this->comparison) !== 'success'
			&& $this->titreId > 0
		);
	}

	/**
	 * @param int $revueId
	 * @return bool  (false si la table Wikidata n'existe pas)
	 */
	public static function isRevuePresent(int $revueId): bool
	{
		try {
			return (bool) Yii::app()->db
				->createCommand("SELECT 1 FROM Wikidata WHERE revueId = :rid LIMIT 1")
				->queryScalar([':rid' => $revueId]);
		} catch (\Exception $_) {
			return false;
		}
	}

	public function getComparisonText(): string
	{
		return Compare::getCompCode((int) $this->comparison);
	}

	/**
	 * comparison level used as html class
	 * @return string ('success', 'warning', 'error', 'info')
	 */
	public function getComparisonLevel(): string
	{
		return Compare::getCompLevel((int) $this->comparison);
	}

	/**
	 * get a count of records by comparison status for a given property
	 * @param string $property
	 * @return array
	 */
	public static function getPropertyComparisonStats(string $property): array
	{
		$sql = <<<EOSQL
			SELECT comparison, COUNT(id) AS cnt
			FROM Wikidata
			WHERE property = :p
			GROUP BY comparison
			ORDER BY comparison DESC
			EOSQL;
		$rows = Yii::app()->db->createCommand($sql)->queryAll(true, [':p' => $property]);
		$res = [];
		foreach ($rows as $row) {
			$res[Compare::getCompCode((int) $row['comparison'])] = $row['cnt'];
		}
		return $res;
	}

	/**
	 * provide a structured array of Wikidata info
	 * each row is a qid + value + url
	 * @param int $revueId
	 */
	public static function getRevueWdInfo(int $revueId): array
	{
		if (!self::isRevuePresent($revueId)) {
			return [];
		}

		$res = [];
		$propname = Cache::getIncomparableProperties();
		$sqlProp = <<<EOSQL
			SELECT qId, property, value, url, objectLabel
			FROM Wikidata
			WHERE property LIKE 'P%' AND revueId = :rid
			ORDER BY qId
			EOSQL;
		$rows = Yii::app()->db->createCommand($sqlProp)->queryAll(true, [':rid' => $revueId]);
		foreach ($rows as $row) {
			if (!isset($propname[$row['property']])) {
				continue;
			}
			$key = strtolower($propname[$row['property']]) . $row['value'] . $row['objectLabel']; //tri ultérieur sans écraser les valeurs
			$res[$key] = [
				'rowhead' => sprintf('%s (%s)', ucfirst($propname[$row['property']]), $row['property']), // eg. 'Twitter'
				'qidLink' => CHtml::link($row['qId'], sprintf('%s/%s', self::WD_BASE_URL, $row['qId'])),
				'value' => $row['value'],
				'url' => $row['url'],
				'objectLabel' => $row['objectLabel'],
			];
			ksort($res, SORT_REGULAR);
		}
		return $res;
	}

	/**
	 * Returns a list of Partenaire linked to this Wikidata entry (through Suivi).
	 *
	 * @return Partenaire[]
	 */
	public function getPartenairesSuivant(): array
	{
		return Partenaire::model()->findAllBySql(<<<EOSQL
			SELECT p.*
			FROM Partenaire p JOIN Suivi s ON p.id = s.partenaireId
			WHERE s.cible = 'Revue' AND s.cibleId = {$this->revueId}
			ORDER BY p.nom ASC
			EOSQL
		);
	}

	public function getSuiviLevel(): string
	{
		$suivis = array_map(
			function ($x) {
				return (int) $x->partenaireId;
			},
			$this->suivis
		);
		if (empty($suivis)) {
			return '-';
		}
		if (in_array((int) Yii::app()->user->partenaireId, $suivis, true)) {
			return 'moi';
		}
		return 'autre';
	}

	/**
	 * @return array dropdownlist of properties, as an associative array, eg. [ 'P268' => 'bnfArk (42)' ...]
	 */
	public static function getWdPropertyDDL(): array
	{
		$sql = "SELECT propertyType, property, COUNT(DISTINCT revueId) AS cnt "
			. "FROM Wikidata WHERE propertyType > 0 "
			. "GROUP BY property ORDER BY propertyType, (0 + SUBSTRING(property, 2))";
		$res = [];
		$previousType = 1;
		foreach (Yii::app()->db->createCommand($sql)->queryAll() as $record) {
			if ($record['propertyType'] != $previousType) { // intercalaire quand on change de type de propriété
				$res['inter' . $previousType] = '-----';
				$previousType = $record['propertyType'];
			}
			$prop = $record['property'];
			$res[$prop] = sprintf("%s (%d revues)", Cache::getPropName($prop), $record['cnt']);
		}
		return $res;
	}

	/**
	 * @param $inconsistent : seulement les incohérences (>= seuil) ou seulement les comparaisons standard
	 * @return array dropdownlist of comparison results as an associative array, eg. [11 => 'Seulement Wikidata (52)']
	 */
	public static function getComparisonDDL(bool $inconsistent): array
	{
		$sql = "SELECT comparison, COUNT(DISTINCT revueId) AS cnt FROM Wikidata "
			. "WHERE property LIKE 'P%' AND comparison IS NOT NULL "
			. sprintf("AND comparison %s %d ", ($inconsistent ? '>=' : '<'), Compare::COMP_INCONSISTENCIES)
			. "GROUP BY comparison";
		$res = [];
		foreach (Yii::app()->db->createCommand($sql)->queryAll() as $record) {
			$comp = $record['comparison'];
			$res[$comp] = sprintf("%s", Compare::getCompCode($comp));
		}
		return $res;
	}

	/**
	 * prépare le tableau des statistiques de comparaison :
	 * nombre d'enregistrements, revues, titres et éléments-WD par code comparaison
	 */
	public static function getComparisonStatistics(): array
	{
		$res = [];
		$sql = <<<SQL
			SELECT
				comparison,
				COUNT(DISTINCT id) AS cid,
				COUNT(DISTINCT revueId) AS crevue,
				COUNT(DISTINCT titreId) AS ctitre,
				COUNT(DISTINCT qId) AS cqid
			FROM Wikidata
			WHERE comparison BETWEEN ? AND ?
			GROUP BY comparison
			SQL;
		$comprange = [Compare::COMP_INCONSISTENCIES, Compare::COMP_UNKNOWN];
		$rows = Yii::app()->db->createCommand($sql)->queryAll(true, $comprange);
		foreach ($rows as $row) {
			$nrow = array_values($row);
			$nrow[0] = Compare::getCompCode($row['comparison']);
			$nrow[5] = Compare::COMP_LEVELS[$row['comparison']];
			$res[] = $nrow;
		}
		return $res;
	}

	/**
	 * provide a structured array of comparison between Mirabel and Wikidata
	 * @return array of array ; each row is an array with the following keys: ['text', 'wdurl', 'murl', 'comp', ...]
	 */
	public static function getRevueComparison(int $revueId): array
	{
		$res = [];

		// Properties
		$propname = Cache::LIEN_PROPERTIES;
		$sqlProp = "SELECT qId, property, value AS wdval, url AS wdUrl, comparison, compUrl "
			. "FROM Wikidata "
			. "WHERE property LIKE 'P%' AND revueId = :rid";
		$rows = Yii::app()->db->createCommand($sqlProp)->queryAll(true, [':rid' => $revueId]);
		foreach ($rows as $row) {
			if (!isset($propname[$row['property']])) {
				continue;
			}
			$comp = ($row['comparison'] ?? Compare::COMP_UNKNOWN);
			$res[] = [
				'text' => $propname[$row['property']],
				'qidLink' => \CHtml::link($row['qId'], sprintf('%s/%s', \Wikidata::WD_BASE_URL, $row['qId'])),
				'wdVal' => $row['wdval'],
				'wdUrl' => $row['wdUrl'],
				'mUrl' => $row['compUrl'],
				'compLevel' => Compare::COMP_LEVELS[$comp],
				'compMsg' => Compare::COMP_CODES[$comp],
			];
		}

		return $res;
	}

}
