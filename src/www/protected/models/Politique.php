<?php

/**
 * This is the model class for table "Politique".
 *
 * The followings are the available columns in table 'Politique':
 * @property int $id
 * @property int $editeurId
 * @property ?int $utilisateurId
 * @property string $status
 * @property ?int $initialTitreId
 * @property string $name
 * @property ?int $lastUpdate timestamp
 * @property ?int $model 0..4
 * @property string $publisher_policy JSON object, cf Sherpa API
 * @property string $notesAdmin
 *
 * @property ?Editeur $editeur
 * @property ?Utilisateur $createur
 * @property Titre[] $titres
 */
class Politique extends CActiveRecord
{
	public const ROLE_OWNER = 'owner';

	public const ROLE_GUEST = 'guest';

	public const STATUS_PENDING = 'pending'; // authored by unvalidated user

	public const STATUS_DRAFT = 'draft';

	public const STATUS_TOPUBLISH = 'topublish'; // an owner confirmed the draft should be published

	public const STATUS_PUBLISHED = 'published'; // an admin confirmed the "to publish" policy

	public const STATUS_UPDATED = 'updated'; // updated post publication

	public const STATUS_TODELETE = 'todelete';

	public const STATUS_DELETED = 'deleted';

	public const ENUM_STATUS = [
		self::STATUS_PENDING,
		self::STATUS_DRAFT,
		self::STATUS_TOPUBLISH,
		self::STATUS_PUBLISHED,
		self::STATUS_UPDATED,
		self::STATUS_TODELETE,
		self::STATUS_DELETED,
	];

	public function tableName(): string
	{
		return 'Politique';
	}

	public function rules(): array
	{
		return [];
	}

	public function attributeLabels(): array
	{
		return [
			'id' => 'ID',
			'editeurId' => 'Éditeur',
			'utilisateurId' => 'Créateur',
			'name' => 'Nom',
			'status' => 'Statut',
			'lastUpdate' => 'Dernière modification',
		];
	}

	public function relations(): array
	{
		return [
			'editeur' => [self::BELONGS_TO, 'Editeur', 'editeurId'],
			'createur' => [self::BELONGS_TO, 'Utilisateur', 'utilisateurId'],
			'titres' => [self::MANY_MANY, 'Titre', 'Politique_Titre(politiqueId, titreId)', 'order' => 'titres.titre'],
		];
	}

	public function beforeSave()
	{
		$this->lastUpdate = time();
		return parent::beforeSave();
	}

	public function getPublisherPolicy(): ?\stdClass
	{
		if ($this->publisher_policy === '') {
			return null;
		}
		return json_decode($this->publisher_policy, false);
	}

	/**
	 * @return Titre[]
	 */
	public function getTitresAssigned(): array
	{
		return Titre::model()->findAllBySql(
			<<<EOSQL
			SELECT t.*
			FROM Titre t
				JOIN Politique_Titre pt ON pt.titreId = t.id
				JOIN Politique p ON p.id = pt.politiqueId
			WHERE pt.politiqueId = :id AND p.status <> :s
			ORDER BY t.titre
			EOSQL,
			[':id' => $this->id, ':s' => self::STATUS_PENDING]
		);
	}

	/**
	 * @return Titre[]
	 */
	public function getTitresPending(): array
	{
		return Titre::model()->findAllBySql(
			<<<EOSQL
			SELECT t.*
			FROM Titre t
				JOIN Politique p ON p.initialTitreId = t.id
			WHERE p.id = :id AND p.status = :s
			ORDER BY t.titre
			EOSQL,
			[':id' => $this->id, ':s' => self::STATUS_PENDING]
		);
	}

	public static function translateStatus(string $status): string
	{
		$fr = [
			self::STATUS_PENDING => 'provisoire',
			self::STATUS_DRAFT => 'brouillon',
			self::STATUS_TOPUBLISH => 'à publier',
			self::STATUS_PUBLISHED => 'publié',
			self::STATUS_UPDATED => 'modifié',
			self::STATUS_TODELETE => 'à supprimer',
			self::STATUS_DELETED => 'supprimé',
		];
		return $fr[$status] ?? "";
	}
}
