<?php

/**
 * AMonitored is an abstract class for Titre, Editeur, Service and Ressource.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
abstract class AMonitored extends CActiveRecord
{
	/**
	 * Builds an Intervention object that can be completed later.
	 */
	public function buildIntervention(bool $direct): Intervention
	{
		$i = new Intervention();
		$i->setAttributes(
			[
				'ressourceId' => null,
				'revueId' => null,
				'titreId' => null,
				'editeurId' => null,
				'statut' => 'attente',
				'import' => 0,
				'ip' => $_SERVER['REMOTE_ADDR'] ?? '',
				'contenuJson' => new InterventionDetail,
			],
			false
		);
		if (PHP_SAPI !== 'cli') {
			$i->utilisateurIdProp = (isset(Yii::app()->user) && !Yii::app()->user->isGuest ? (int) Yii::app()->user->id : null);
		}
		return $i;
	}

	abstract public function getSelfLink();
}
