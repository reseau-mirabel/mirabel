<?php

use components\Tools;

/**
 * This is the model class for table "Grappe".
 *
 * The followings are the available columns in table 'Grappe':
 * @property int $id
 * @property string $nom
 * @property string $description (html)
 * @property string $note (note interne)
 * @property int $diffusion
 * @property int $niveau
 * @property string $recherche : critères de recherche
 * @property ?int $partenaireId
 * @property int $hdateCreation
 * @property ?int $hdateModif
 *
 * @property Partenaire $partenaire
 * @property array $titres Titre[]
 */
class Grappe extends CActiveRecord
{
	public const DIFFUSION_PARTAGE = 1;

	public const DIFFUSION_PUBLIC = 2;

	public const DIFFUSION_AUTHENTIFIE = 3;

	public const DIFFUSION_PARTENAIRE = 4;

	public const DIFFUSION_TOUT = 255;

	public const ENUM_DIFFUSION = [
		self::DIFFUSION_PARTAGE => 'partagée',
		self::DIFFUSION_PUBLIC => 'publique',
		self::DIFFUSION_AUTHENTIFIE => 'authentifié',
		self::DIFFUSION_PARTENAIRE => 'mon partenaire',
	];

	public const DIFFUSION_COMMENT = [
		0 => "",
		Grappe::DIFFUSION_AUTHENTIFIE => "Visible uniquement par les utilisateurs authentifiés dans Mir@bel, si le lien leur est transmis.",
		Grappe::DIFFUSION_PARTAGE => "Visible par tous. D'autres partenaires de Mir@bel peuvent l'ajouter à leurs listes de grappes. <strong>Attention</strong>, dès que d'autres partenaires référencent cette grappe, vous ne pouvez plus changer sa diffusion ni la supprimer.",
		Grappe::DIFFUSION_PARTENAIRE => "Visible uniquement par les utilisateurs authentifiés qui sont membres de votre établissement-partenaire de Mir@bel.",
		Grappe::DIFFUSION_PUBLIC => "Visible par tous. Vous pouvez l'ajouter à vos listes de grappes ou transmettre son lien public.",
	];

	public const NIVEAU_TITRE = 1;

	public const NIVEAU_REVUE = 2;

	public const NIVEAU = [
		self::NIVEAU_TITRE => "titres",
		self::NIVEAU_REVUE => "revues",
	];

	/**
	 * Grappe_Titre.source = -1 pour un titre ajouté directement.
	 */
	public const SOURCE_NOTSEARCH = -1;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Grappe';
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'partenaire' => [self::BELONGS_TO, 'Partenaire', 'partenaireId'],
			'titres' => [self::HAS_MANY, 'Grappe_Titre', 'titreId'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'nom' => 'Nom',
			'description' => 'Description',
			'note' => 'Note interne',
			'diffusion' => "Diffusion",
			'partenaireId' => 'Partenaire',
		];
	}

	public function afterFind()
	{
		parent::afterFind();
		$this->id = $this->id ? (int) $this->id : null;
		$this->diffusion = (int) $this->diffusion;
		$this->niveau = (int) $this->niveau;
		$this->partenaireId = $this->partenaireId ? (int) $this->partenaireId : null;
		$this->hdateCreation = (int) $this->hdateCreation;
		$this->hdateModif = $this->hdateModif ? (int) $this->hdateModif : null;
	}

	/**
	 * @return Titre[]
	 */
	public function findTitres(string $sqlFilter, bool $groupByRevue, int $limit = 0): array
	{
		$and = $sqlFilter ? "AND $sqlFilter" : "";
		$group = $groupByRevue ? "GROUP BY t.revueId" : "GROUP BY t.id";
		$limitsql = $limit > 0 ? " LIMIT $limit" : "";
		return \Titre::model()
			->findAllBySql(<<<EOSQL
				SELECT t.*
				FROM Titre t
					JOIN Grappe_Titre gt ON gt.titreId = t.id $and
				WHERE gt.grappeId = :id
				$group
				ORDER BY t.titre ASC $limitsql
				EOSQL,
				[':id' => $this->id]
			);
	}

	/**
	 * Returns a HTML link.
	 *
	 * @return string HTML link.
	 */
	public function getSelfLink(): string
	{
		return CHtml::link(
			CHtml::encode($this->nom),
			$this->getSelfUrl()
		);
	}

	public function getSelfUrl(): array
	{
		return ['/grappe/view', 'id' => $this->id, 'nom' => Norm::urlParam($this->nom)];
	}

	/**
	 * @return SearchTitre[]
	 */
	public function getSearchCriteria(): array
	{
		$criteria = [];
		foreach (json_decode($this->recherche, true, 16, JSON_THROW_ON_ERROR) as $r) {
			$search = new SearchTitre();
			$search->setAttributes($r, false);
			$search->validate();
			$criteria[] = $search;
		}
		return $criteria;
	}

	/**
	 * Returns nombre de critères de recherche dans la grappe
	 *
	 * @return int
	 */
	public function countCriteria(): int
	{
		$criteria = json_decode($this->recherche, true, 16, JSON_THROW_ON_ERROR);
		return (int) count($criteria);
	}

	/**
	 * Called automatically before Save().
	 *
	 * @return bool
	 */
	protected function beforeSave()
	{
		$this->nom = Tools::normalizeText($this->nom);
		$this->description = Tools::normalizeText($this->description);
		$this->note = Tools::normalizeText($this->note);
		if (!$this->recherche) {
			$this->recherche = '[]';
		}
		if (!$this->partenaireId) {
			$this->partenaireId = null;
		}
		if ($this->isNewRecord) {
			$this->hdateCreation = (int) $_SERVER['REQUEST_TIME'];
		} else {
			$this->hdateModif = (int) $_SERVER['REQUEST_TIME'];
		}
		return parent::beforeSave();
	}
}
