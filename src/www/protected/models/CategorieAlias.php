<?php

/**
 * This is the model class for table "CategorieAlias".
 *
 * The followings are the available columns in table 'CategorieAlias':
 * @property int $id
 * @property int $categorieId
 * @property int $vocabulaireId
 * @property string $alias
 * @property string $uri
 * @property string $hdateModif
 * @property int $modifPar
 *
 * @property Categorie $categorie
 * @property Vocabulaire $vocabulaire
 */
class CategorieAlias extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'CategorieAlias';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['categorieId, vocabulaireId, alias', 'required'],
			['categorieId, vocabulaireId', 'numerical', 'integerOnly' => true, 'allowEmpty' => true],
			['alias, uri', 'length', 'max' => 255],
			['hdateModif',
				'default', 'value' => date("Y-m-d H:i:s"),
				'setOnEmpty' => false, 'on' => 'insert, update', ],
			// The following rule is used by search().
			['id, categorieId, vocabulaireId, alias, uri, hdateModif, modifPar', 'safe', 'on' => 'search'],
		];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'categorie' => [self::BELONGS_TO, 'Categorie', 'categorieId'],
			'vocabulaire' => [self::BELONGS_TO, 'Vocabulaire', 'vocabulaireId'],
			'modifiePar' => [self::HAS_ONE, 'Utilisateur', 'modifiePar'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'categorieId' => 'Thème',
			'vocabulaireId' => 'Vocabulaire',
			'alias' => 'Alias',
			'uri' => 'UAR',
			'hdateModif' => 'Date modif.',
			'modifPar' => 'Modif. par',
		];
	}

	public function beforeDelete()
	{
		CategorieAliasTitre::model()->deleteAllByAttributes(['categorieAliasId' => $this->id]);
		return true;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @param int|bool $pageSize Number of items for pagination (false to disable pagination).
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($pageSize = 25)
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('categorieId', $this->categorieId);
		$criteria->compare('vocabulaireId', $this->vocabulaireId);
		$criteria->compare('alias', $this->alias, true);
		$criteria->compare('uri', $this->uri, true);
		$criteria->compare('hdateModif', $this->hdateModif, true);
		$criteria->compare('modifPar', $this->modifPar, true);

		$sort = new CSort();
		$sort->attributes = ['id', 'categorieId', 'vocabulaireId', 'alias', 'hdateModif', 'modifPar'];
		$sort->defaultOrder = 'hdateModif DESC';

		return new CActiveDataProvider(
			$this,
			[
				'criteria' => $criteria,
				'pagination' => ($pageSize ? ['pageSize' => $pageSize] : false),
				'sort' => $sort,
			]
		);
	}
}
