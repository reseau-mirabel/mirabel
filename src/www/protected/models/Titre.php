<?php

use components\Tools;
use models\validators\DateValidator;
use processes\titres\PolitiquePublication;

require_once __DIR__ . '/traits/LiensJson.php';

/**
 * This is the model class for table "Titre".
 *
 * The followings are the available columns in table 'Titre':
 * @property int $id
 * @property int $revueId
 * @property string $titre
 * @property string $prefixe
 * @property string $sigle
 * @property ?int $obsoletePar
 * @property string $dateDebut
 * @property string $dateFin
 * @property string $url
 * @property string $urlCouverture
 * @property string $liensJson
 * @property string $periodicite
 * @property string $langues JSON array
 * @property string $statut
 * @property int $hdateModif timestamp
 *
 * @property bool $electronique
 * @property string $fullTitle
 * @property Liens $liens
 * @property Identification[] $identifications
 * @property Intervention[] $interventions
 * @property Service[] $services
 * @property Revue $revue
 * @property ?Titre $obsoleteParTitre
 * @property Titre[] $titres
 * @property Collection[] $collections
 * @property TitreEditeur[] $titreEditeurs
 * @property Editeur[] $editeurs
 * @property Issn[] $issns
 */
class Titre extends AMonitored implements IUserCanConfirm, IWithIndirectSuivi, TitledObject, WithSelfUrl
{
	use LiensJson;
	use TitreHtml;
	use models\traits\UserCanConfirm;

	public const PERIODICITY_NORM = [
		['value' => 'annuel', 'label' => 'annuel (tous les ans)'],
		['value' => 'bisannuel', 'label' => 'bisannuel (tous les 2 ans)'],
		['value' => 'bi-hebdomadaire', 'label' => 'bi-hebdomadaire (2 fois par semaine)'],
		['value' => 'bimensuel', 'label' => 'bimensuel (2 fois par mois)'],
		['value' => 'bimestriel', 'label' => 'bimestriel (tous les 2 mois)'],
		['value' => 'hebdomadaire', 'label' => 'hebdomadaire (toutes les semaines)'],
		//'inconnue' => 'inconnue',
		['value' => 'irrégulier', 'label' => 'irrégulier  (périodicité sans motif reconnaissable)'],
		['value' => 'mensuel', 'label' => 'mensuel (tous les mois)'],
		['value' => 'parution continue', 'label' => 'parution continue'],
		['value' => 'quadrimestriel', 'label' => 'quadrimestriel (tous les 4 mois)'],
		['value' => 'quinquennal', 'label' => 'quinquennal (tous les 5 ans)'],
		['value' => 'quotidien', 'label' => 'quotidien (tous les jours)'],
		['value' => 'semestriel', 'label' => 'semestriel (tous les 6 mois)'],
		['value' => 'triannuel', 'label' => 'triannuel (tous les 3 ans)'],
		['value' => 'tri-hebdomadaire', 'label' => 'tri-hebdomadaire (3 fois par semaine)'],
		['value' => 'trimensuel', 'label' => 'trimensuel (3 fois par mois)'],
		['value' => 'trimestriel', 'label' => 'trimestriel (tous les 3 mois)'],
		['value' => '.. numéros par an', 'label' => '.. numéros par an'],
	];

	public const STATUTS = ['normal', 'suppr', 'attente'];

	public const URL_BNF = 'https://catalogue.bnf.fr/ark:/12148/%s';

	public const URL_WORLDCAT = 'https://www.worldcat.org/oclc/%u';

	public const URL_SUDOC = 'https://www.sudoc.fr/%s';

	public const URL_SUDOC_ISSN = 'https://www.sudoc.abes.fr/DB=2.1/CMD?ACT=SRCHA&IKT=8&SRT=RLV&TRM=%s';

	public $urlCouvertureDld;

	public $suivre = false;

	public $posseder = false;

	public $hasUrlError = false;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Titre';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['titre', 'required'],
			['revueId, obsoletePar', 'numerical', 'integerOnly' => true],
			['titre', 'length', 'max' => 1024],
			['titre', '\models\validators\DuplicatesValidator'],
			['sigle, periodicite', 'length', 'max' => 255],
			['dateDebut, dateFin', DateValidator::class, 'nothingAfter' => true], // rejects '2005-05 with an extra'
			['url, urlCouverture', 'url', 'on' => 'insert update'],
			['url, urlCouverture', 'length', 'max' => 512],
			['url, urlCouverture', 'ext.validators.UrlFetchableValidator', 'on' => 'insert update'],
			['prefixe', 'length', 'max' => 25],
			['langues', \models\validators\LangIso639Validator::class, 'on' => 'insert'],
			['langues', 'length', 'max' => 60],
			['statut', 'in', 'range' => self::STATUTS], // enum
			['urlCouvertureDld', 'boolean'],
			['liensJson', 'safe', 'on' => 'import'], // an Intervention uses this
			['liens', 'safe'], // will use setLiens()
			['confirm, suivre, posseder', 'boolean'],
			// local validator methods
			['liensJson', 'validateLiensJson'],
		];
	}

	public function validateLiensJson($attribute, $params)
	{
		if ($attribute === 'liensJson' && !empty($this->liensJson)) {
			$liens = $this->getLiens();
			$urlValidation = !$this->confirm;
			if (($this->scenario === 'insert' || $this->scenario === 'update')) {
				if ($liens->validate($urlValidation)) {
					$this->liensJson = json_encode($liens, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
				} else {
					$this->hasUrlError = $liens->hasUrlError();
					$this->addError('liensJson', join('<br />', $liens->getErrors()));
				}
			}
		}
	}

	public function getElectronique(): bool
	{
		return (boolean) $this->dbConnection
			->createCommand("SELECT MIN(support = :support) FROM Issn WHERE titreId = :id")
			->queryScalar([':support' => Issn::SUPPORT_ELECTRONIQUE, ':id' => $this->id]);
	}

	/**
	 * Called automatically before delete().
	 *
	 * @return bool
	 */
	public function beforeDelete()
	{
		if ($this->isNewRecord) {
			return false;
		}
		$errors = (new processes\titres\Delete($this))->getConstraints();
		if (count($errors) === 0) {
			return true;
		}
		$this->addError('id', join(" - ", $errors));
		return false;
	}

	public function relations()
	{
		return [
			'identifications' => [self::HAS_MANY, 'Identification', 'titreId'],
			'interventions' => [self::HAS_MANY, 'Intervention', 'titreId'],
			//'services' => array(self::HAS_MANY, 'Service', 'titreId'),
			'revue' => [self::BELONGS_TO, 'Revue', 'revueId'],
			'obsoleteParTitre' => [self::BELONGS_TO, 'Titre', 'obsoletePar'],
			'titres' => [self::HAS_MANY, 'Titre', 'obsoletePar'],
			'titreEditeurs' => [
				self::HAS_MANY, 'TitreEditeur', 'titreId',
				'index' => 'editeurId',
				'order' => "ancien ASC",
			],
			'editeurs' => [
				self::MANY_MANY, 'Editeur', 'Titre_Editeur(titreId, editeurId)',
				'index' => 'id',
				'order' => "editeurs_editeurs.ancien ASC, editeurs.nom ASC",
			],
			'issns' => [
				self::HAS_MANY, 'Issn', 'titreId',
				'index' => 'id',
				'order' => "(issns.support = '" . Issn::SUPPORT_PAPIER . "') DESC, (dateFin = '') DESC, dateDebut DESC, id ASC",
			],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'revueId' => 'Revue',
			'titre' => 'Titre',
			'prefixedTitle' => 'Titre actuel',
			'prefixe' => 'Préfixe',
			'sigle' => 'Sigle',
			'obsoletePar' => 'Obsolète Par',
			'dateDebut' => 'Date de début',
			'dateFin' => 'Date de fin',
			'url' => 'Site web',
			'urlCouverture' => "URL de la couverture",
			'liensJson' => 'Autres liens',
			'periodicite' => 'Périodicité',
			'electronique' => 'Électronique',
			'langues' => 'Langues',
			'statut' => 'Statut',
			'hdateModif' => 'Dernière modification',
			'confirm' => "Confirmer malgré l'avertissement",
			'suivre' => 'Suivre cette revue',
			'posseder' => 'Ajouter ce titre à mes possessions',
			'urlCouvertureDld' => "Télécharger, modifier l'image de couverture",
			'liensInternes' => "Titres liés",
		];
	}

	/**
	 * @inheritdoc
	 */
	public function getAttributeLabel($attribute)
	{
		if (($attribute === 'suivre' || $attribute === 'posseder') && Yii::app()->user->checkAccess("avec-partenaire")) {
			$p = Partenaire::model()->findByPk(Yii::app()->user->partenaireId);
			return parent::getAttributeLabel($attribute) . ' (' . $p->nom . ')';
		}
		return parent::getAttributeLabel($attribute);
	}

	public function load(array $post): bool
	{
		if (!isset($post[get_class($this)])) {
			return false;
		}
		$this->setAttributes($post[get_class($this)]);
		return true;
	}

	/**
	 * Returns an array of parents that have a direct "Suivi": [ [table => ,  id => ], ... ]
	 */
	public function listParentsForSuivi(): array
	{
		return [
			['table' => 'Revue', 'id' => $this->revueId],
		];
	}

	/**
	 * Returns the list of the titles from the same Revue.
	 *
	 * @param bool $assoc (opt) array (id => fullTitle).
	 * @return array of Title, or assoc array (id => fullTitle).
	 */
	public function getOtherTitles(bool $assoc = false): array
	{
		if (empty($this->revueId)) {
			return [];
		}
		if (empty($this->id)) {
			$condition = '';
		} else {
			$condition = "id != {$this->id}";
		}
		$titles = $this->findAllByAttributes(['revueId' => $this->revueId], $condition);
		if (empty($titles) || !$assoc) {
			return $titles;
		}
		$pairs = [];
		/** @var Titre $title */
		foreach ($titles as $title) {
			$pairs[$title->id] = $title->getFullTitleWithPerio();
		}
		return $pairs;
	}

	/**
	 * Returns a list of Partenaire linked to this (through Suivi).
	 *
	 * @return Partenaire[]
	 */
	public function getPartenairesSuivant(): array
	{
		if (!$this->revueId) {
			return [];
		}
		return Partenaire::model()->findAllBySql(
			"SELECT p.* FROM Partenaire p "
			. "JOIN Suivi s ON (s.partenaireId = p.id AND s.cible = 'Revue' AND s.cibleId={$this->revueId})"
		);
	}

	/**
	 * Returns a list of Service (with its Ressource) linked to this Titre.
	 *
	 * @return Service[]
	 */
	public function getServices(): array
	{
		$criteria = new CDbCriteria;
		$criteria->with = [
			'ressource' => ['joinType' => 'INNER JOIN'],
		];
		$criteria->order = 't.type, t.acces, t.dateBarrDebut DESC';
		$criteria->condition = 't.titreId = ' . $this->id;
		return Service::model()->findAll($criteria);
	}

	public function getPreferedIssn(): ?Issn
	{
		return Issn::model()->findBySql(
			"SELECT * FROM Issn WHERE titreId = :tid
				ORDER BY (dateFin = '') DESC, (support = '" . Issn::SUPPORT_PAPIER . "') DESC, dateDebut DESC, id ASC",
			[':tid' => $this->id]
		);
	}

	/**
	 * Return the first ISSN among ISSN, ISSN-L, ISSN-Es, or null.
	 */
	public function getEitherIssn(): ?string
	{
		$issns = $this->issns;
		if ($issns) {
			return reset($issns)->issn;
		}
		return null;
	}

	/**
	 * @return string HTML
	 */
	public function getLinksOther(): string
	{
		$html = [];
		if ($this->liensJson) {
			$html[] = $this->getLiensNormaux();
		}
		$html[] = $this->getLinkWorldcat();
		return join(" ", array_filter($html));
	}

	/**
	 * @return string HTML
	 */
	public function getLinksEditorial(): string
	{
		$polPub = new PolitiquePublication($this);
		$source = $polPub->getSource();
		$local = null;
		if ($source === PolitiquePublication::SOURCE_MIRABEL) {
			$local = new Lien();
			$local->src = "enregistrée par Mir@bel";
			$local->setTitle("Politique déclarée par l'éditeur via le service Mir@bel dédié (validée par Mir@bel)");
			// On conserve le lien vers Open policy finder s'il existe, cf #5866.
		} elseif ($source === PolitiquePublication::SOURCE_SHERPA) {
			$local = new Lien();
			$local->src = "voir en français";
			$local->setTitle("Adaptation en français des informations de “Open policy finder”");
		}

		$links = $this->getLiensEditoriaux();
		if ($local !== null) {
			$local->url = \Yii::app()->controller->createUrl('/titre/publication', ['id' => $this->id]);
			if ($links == null) {
				$links = new Liens();
			}
			$links->add($local);
		}

		if ($links === null) {
			return '';
		}
		return (string) $links;
	}

	/**
	 * Returns true if the (currently selected) Partenaire has this title in its belongings.
	 *
	 * @param ?int $partenaireId (opt) Defaults to current user's partenaireId.
	 */
	public function belongsTo(?int $partenaireId = null): bool
	{
		if (empty($partenaireId)) {
			if (empty(Yii::app()->user->getState('partenaireId'))) {
				return false;
			}
			$partenaireId = Yii::app()->user->getState('partenaireId');
		}
		return (bool) Yii::app()->db
			->createCommand("SELECT 1 FROM Partenaire_Titre WHERE partenaireId = :pid AND titreId = :tid")
			->queryScalar(['pid' => $partenaireId, 'tid' => $this->id]);
	}

	/**
	 * Returns true if the (current) Partenaire has some Suivi on this title.
	 *
	 * @param ?int $partenaireId (opt) Defaults to current user's partenaireId.
	 */
	public function monitoredBy(?int $partenaireId = null): bool
	{
		if (empty($partenaireId)) {
			if (!Yii::app()->user->checkAccess("avec-partenaire")) {
				return false;
			}
			$partenaireId = Yii::app()->user->partenaireId;
		}
		return (bool) Yii::app()->db
			->createCommand("SELECT 1 FROM Suivi WHERE partenaireId=:pid AND cible='Revue' AND cibleId=:rid")
			->queryScalar(['pid' => $partenaireId, 'rid' => $this->revueId]);
	}

	/**
	 * Builds an Intervention object that can be completed later.
	 */
	public function buildIntervention(bool $direct): Intervention
	{
		$i = parent::buildIntervention($direct);
		if (isset($this->id)) {
			$i->titreId = $this->id;
		}
		if (isset($this->revueId)) {
			$i->revueId = $this->revueId;
		}
		$i->suivi = null !== Suivi::isTracked($this);
		return $i;
	}

	/**
	 * Returns the list of Partenaire owning this.
	 * @return Partenaire[]
	 */
	public function getOwners(): array
	{
		if (empty($this->id)) {
			return [];
		}
		return Partenaire::model()->findAllBySql(
			"SELECT p.* FROM Partenaire p JOIN Partenaire_Titre pt ON p.id=pt.partenaireId "
			. "WHERE pt.titreId = {$this->id} ORDER BY p.nom ASC"
		);
	}

	/**
	 * Returns true if this title is, or could be, part of a collection.
	 */
	public function hasPotentialCollections(): bool
	{
		if (!$this->id) {
			return false;
		}
		$sqls = [
			"SELECT 1 FROM Service s JOIN Service_Collection sc ON sc.serviceId = s.id WHERE s.titreId = {$this->id} LIMIT 1",
			"SELECT 1 FROM Service s JOIN Collection USING (ressourceId) WHERE s.titreId = {$this->id} LIMIT 1",
		];
		foreach ($sqls as $sql) {
			if (Yii::app()->db->createCommand($sql)->queryScalar()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Return an array of ISSN groups, each group indexed by an optional keys "issnp", "issne", "issnl".
	 *
	 * @return array{dates?: string, issnp?: Issn[], issne?: Issn[], issnl?: Issn[]}
	 */
	public function getIssnGroups(): array
	{
		if (!$this->id) {
			return [];
		}
		$displayDates = false;
		$datesInfo = [];
		$issns = ['issnp' => [], 'issne' => [], 'issnl' => []];
		foreach ($this->issns as $issn) {
			if (in_array($issn->support, [Issn::SUPPORT_PAPIER, Issn::SUPPORT_ELECTRONIQUE])) {
				$displayDates = $displayDates || $issn->dateDebut !== $this->dateDebut || $issn->dateFin !== $this->dateFin;
				$datesInfo[] = sprintf("%s : %s - %s", $issn->issn, $issn->dateDebut, $issn->dateFin);
				if ($issn->support === Issn::SUPPORT_PAPIER) {
					$issns['issnp'][] = $issn;
				} elseif ($issn->support === Issn::SUPPORT_ELECTRONIQUE) {
					$issns['issne'][] = $issn;
				}
			}
			if ($issn->issnl) {
				$issns['issnl'][] = $issn;
			}
		}
		if ($displayDates) {
			$issns['dates'] = join("\n", $datesInfo);
		}
		return $issns;
	}

	public static function findByIssn($issn): ?Titre
	{
		return self::model()->findBySql(
			"SELECT t.* FROM Titre t JOIN Issn i ON i.titreId = t.id WHERE i.issn = :issn OR i.issnl = :issnl LIMIT 1",
			[':issn' => $issn, ':issnl' => $issn]
		);
	}

	public function getCollections()
	{
		return Collection::model()->findAllBySql(
			"SELECT c.* "
			. " FROM Service s JOIN Service_Collection sc ON sc.serviceId = s.id JOIN Collection c ON c.id = sc.collectionId"
			. " WHERE s.titreId = {$this->id} ORDER BY c.nom"
		);
	}

	public function onUnsafeAttribute($name, $value)
	{
		// do nothing
	}

	/**
	 * Called automatically before validate().
	 *
	 * @return bool
	 */
	protected function beforeValidate()
	{
		if ($this->scenario === 'search') {
			return parent::beforeValidate();
		}
		if ($this->prefixe) {
			$this->prefixe = str_replace(["’", "´"], "'", $this->prefixe);
			if (preg_match('/[\'-]\s*$/', $this->prefixe)) {
				$this->prefixe = trim($this->prefixe);
			} elseif (!preg_match('/[\s ]$/', $this->prefixe)) {
				$this->prefixe = trim($this->prefixe) . " ";
			}
		}
		if ($this->titre) {
			$this->titre = str_replace(["’", "´"], "'", trim($this->titre));
			if (!$this->confirm && ($this->scenario === 'insert' || $this->scenario == 'update')) {
				$prefixes = array_filter(explode('/', (string) \Config::read('import.prefixes')));
				foreach ($prefixes as $prefixe) {
					if (strncasecmp($this->titre, $prefixe, strlen($prefixe)) === 0) {
						$this->addError('titre', "Ce titre débute par « {$prefixe} ». Ne faudrait-il pas utiliser le champ <em>Préfixe</em> pour permettre un meilleur tri ?");
						$this->confirm = false; // Will display the field to the end user
						break;
					}
				}
			}
		}
		if ($this->langues === '') {
			$this->langues = '[]';
		}
		if (!empty($this->periodicite)) {
			$this->validatePeriodicity($this->periodicite);
		}
		return parent::beforeValidate();
	}

	/**
	 * Called automatically after validate().
	 */
	protected function afterValidate()
	{
		// ordered dates
		if (!$this->errors) {
			if ($this->dateDebut && $this->dateFin && $this->dateDebut > $this->dateFin) {
				$this->addError('dateDebut', "La date de début est supérieure à celle de fin.");
			}
		}
		// cleanup languages
		if (empty($this->langues)) {
			$this->langues = '[]';
		} elseif ($this->langues[0] !== '[') {
			$this->langues = json_encode(preg_split('/\s*[ ,]\s*/', trim($this->langues, ', ')));
		}
		// no loop in obsolescence
		if (!$this->isNewRecord && !empty($this->obsoletePar)) {
			$titles = $this->findAllByAttributes(['revueId' => $this->revueId, 'obsoletePar' => null]);
			if (count($titles) == 1 && $titles[0]->id == $this->id) {
				$this->addError(
					'obsoletePar',
					"Ce titre est actif, vous ne pouvez pas le rendre osolète sans création."
				);
			}
		}
		return parent::afterValidate();
	}

	/**
	 * Add an error to the model instance if the field "periodicite" does not validate.
	 */
	protected function validatePeriodicity(string $p): bool
	{
		// starts with a known value?
		foreach (self::PERIODICITY_NORM as $norm) {
			if ($norm['value'][0] === '.') {
				continue;
			}
			$len = strlen($norm['value']);
			if (strncmp($p, $norm['value'], $len) === 0) {
				if (strlen($p) === $len || $p[$len-1] === ' ') {
					return true;
				}
			}
		}
		// .. numéros par ans
		if (preg_match('/^(\S+) numéros par an\b/', $p, $matches)) {
			$num = (int) $matches[1];
			if ($num > 0 && !in_array($num, [1, 2, 3, 4, 6, 12, 24, 56])) {
				return true;
			}
			$this->addError('periodicite', "Le nombre annuel de numéros n'est pas valide.");
			return false;
		}
		// not recognized
		$this->addError('periodicite', "La périodicité n'est pas d'une forme admise.");
		return false;
	}

	/**
	 * Called automatically before save().
	 *
	 * @return bool
	 */
	protected function beforeSave()
	{
		$this->hdateModif = $_SERVER['REQUEST_TIME'];
		if ($this->prefixe === null || trim($this->prefixe) === '') {
			$this->prefixe = '';
		}
		if (empty($this->obsoletePar)) {
			$this->obsoletePar = null;
		}
		if (empty($this->dateDebut)) {
			$this->dateDebut = '';
		}
		if (empty($this->liensJson)) {
			$this->liensJson = '[]';
		}
		$this->titre = Tools::normalizeText($this->titre);
		return parent::beforeSave();
	}

	/**
	 * Called automatically after save().
	 */
	protected function afterSave()
	{
		if (empty($this->obsoletePar) && $this->revueId > 0 && $this->id > 0) {
			Yii::app()->db->createCommand(
				"UPDATE Titre SET obsoletePar = {$this->id} "
				. "WHERE revueId = {$this->revueId} AND obsoletePar IS NULL AND id <> {$this->id}"
			)->execute();
		}
		$this->getLiens()->save("Titre", (int) $this->id);
		return parent::afterSave();
	}
}
