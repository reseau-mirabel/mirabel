<?php

use components\email\Mailer;
use components\Tools;
use import\Log;
use processes\intervention\HtmlHelper;

/**
 * This is the model class for table "Intervention".
 *
 * The followings are the available columns in table 'Intervention':
 * @property int $id
 * @property string  $description
 * @property ?int $ressourceId
 * @property ?int $revueId
 * @property ?int $titreId
 * @property ?int $editeurId
 * @property string $statut
 * @property string|InterventionDetail $contenuJson
 * @property int $hdateProp timestamp
 * @property int $hdateVal timestamp
 * @property int $utilisateurIdProp
 * @property bool $suivi
 * @property int $import -1 désactive le contrôle des valeurs modifiées avant écriture
 * @property string $ip IPv4 or IPv6
 * @property string $email
 * @property string $commentaire
 * @property int $utilisateurIdVal
 * @property string $action
 *
 * @property ?Editeur $editeur
 * @property ?Ressource $ressource
 * @property ?Titre $titre
 * @property ?Titre $titreRevue Titre actif de la revue
 * @property ?Revue $revue
 * @property ?Utilisateur $utilisateurProp
 */
class Intervention extends CActiveRecord implements IWithIndirectSuivi
{
	use \models\traits\AntispamSum;

	public const STATUTS = [
		'attente' => 'Attente',
		'accepté' => 'Accepté',
		'refusé' => 'refusé',
	];

	public const ACTIONS = [
		'service-C' => 'accès/création',
		'service-D' => 'accès/suppression',
		'service-U' => 'accès/mise à jour',
		'editeur-C' => 'éditeur/création',
		'editeur-D' => 'éditeur/suppression',
		'editeur-U' => 'éditeur/mise à jour',
		'ressource-C' => 'ressource/création',
		'ressource-D' => 'ressource/suppression',
		'ressource-U' => 'ressource/mise à jour',
		'collection-C' => 'collection/création',
		'collection-D' => 'collection/suppression',
		'collection-U' => 'collection/mise à jour',
		'revue-C' => 'revue/création',
		'revue-D' => 'revue/suppression',
		'revue-I' => 'revue/indexation',
		'revue-U' => 'revue/mise à jour',
	];

	/**
	 * Imports external methods into this class.
	 */
	public function behaviors()
	{
		return [
			'json' => [
				'class' => 'application.models.behaviors.JsonFields',
				'fields' => ['contenuJson'],
			],
		];
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Intervention';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['statut, contenuJson', 'required'],
			[
				'ressourceId, revueId, titreId, editeurId, import',
				'numerical', 'integerOnly' => true,
			],
			['suivi', 'boolean'],
			['commentaire', 'length', 'max' => 4095],
			['email', 'length', 'max' => 255],
			['email', 'email'],
			//array('contenuJson, ip', 'safe'),
			['statut', 'in', 'range' => array_keys(self::STATUTS), 'on' => 'update'], // enum
			['spam1, spam2', 'length', 'max' => 255],
		];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'editeur' => [self::BELONGS_TO, 'Editeur', 'editeurId'],
			'ressource' => [self::BELONGS_TO, 'Ressource', 'ressourceId'],
			'revue' => [self::BELONGS_TO, 'Revue', 'revueId'],
			'titre' => [self::BELONGS_TO, 'Titre', 'titreId'],
			'titreRevue' => [self::BELONGS_TO, 'Titre', ['revueId' => 'revueId'],
				'on' => 'titreRevue.obsoletePar IS NULL', ],
			'utilisateurProp' => [self::BELONGS_TO, 'Utilisateur', 'utilisateurIdProp', 'with' => 'partenaire'],
			'utilisateurVal' => [self::BELONGS_TO, 'Utilisateur', 'utilisateurIdVal', 'with' => 'partenaire'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'description' => 'Description',
			'ressourceId' => 'Ressource',
			'revueId' => 'Revue',
			'titreId' => 'Titre',
			'editeurId' => 'Editeur',
			'statut' => 'Statut',
			'contenuJson' => 'Contenu',
			'hdateProp' => 'Date de proposition',
			'hdateVal' => 'Date de validation',
			'utilisateurIdProp' => 'Utilisateur proposant',
			'import' => 'Import',
			'ip' => 'IP',
			'email' => 'Courriel',
			'commentaire' => 'Commentaire',
			'utilisateurIdVal' => 'Utilisateur validant',
			'action' => 'Action',
			'suivi' => 'Limiter aux objets que je suis',
			'spam2' => $this->getRandomAntispam(),
		];
	}

	public function getContent(): InterventionDetail
	{
		if (is_string($this->contenuJson)) {
			$this->contenuJson = new InterventionDetail(json_decode($this->contenuJson, true));
		}
		assert($this->contenuJson instanceof InterventionDetail);
		return $this->contenuJson;
	}

	/**
	 * Marks the intervention as "accepted" (by the current user), and applies its changes.
	 *
	 * @param bool $save (opt, defaults to true) Save this after the operation?
	 * @return bool Success?
	 */
	public function accept(bool $save = true): bool
	{
		if ($this->statut === 'accepté') {
			throw new Exception("Impossible d'accepter une intervention déjà acceptée.");
		}
		if (!$this->validate()) {
			return false;
		}
		if ($this->scenario === 'import') {
			$this->contenuJson->isImport = true;
		}
		if (!$this->contenuJson->apply()) {
			$this->addError('contenuJson', $this->formatContentErrors());
			return false;
		}
		$this->hdateVal = time();
		$this->utilisateurIdVal = (PHP_SAPI === 'cli' ? null : (int) Yii::app()->user->id);
		$this->statut = 'accepté';

		if ($this->contenuJson->hasOnlySpecialFields()) {
			// Apply but do not save the Intervention if it changes only "hidden" fields
			return true;
		}
		$this->updateDates();
		$this->updateFk();
		if ($this->action === 'revue-I') {
			CategorieStats::fill();
		}
		if ($save) {
			return $this->save(false);
		}
		return true;
	}

	/**
	 * Marks the intervention as "rejected" (by the current user).
	 */
	public function reject(): bool
	{
		if ($this->statut !== 'attente') {
			throw new Exception("Impossible de refuser une intervention déjà acceptée/refusée.");
		}
		$this->hdateVal = time();
		$this->utilisateurIdVal = Yii::app()->user->id;
		$this->statut = 'refusé';
		return $this->save(false);
	}

	/**
	 * Marks as "rejected" an accepted intervention and reverts the changes.
	 */
	public function revert(): bool
	{
		if ($this->statut !== 'accepté') {
			throw new Exception("Impossible d'annuler une intervention non acceptée.");
		}
		if (!$this->validate()) {
			return false;
		}
		if ($this->scenario === 'import') {
			$this->contenuJson->isImport = true;
		}
		{   // delete the FK for a creation
			$backupAttributes = $this->attributes;
			$changed = false;
			$tables = ["Revue", "Titre", "Editeur", "Ressource"];
			foreach ($tables as $table) {
				$attr = lcfirst($table) . "Id";
				if ($this->hasAttribute($attr)) {
					$lastInsertId = (int) ($this->contenuJson->lastInsertId[$table] ?? 0);
					if ($lastInsertId > 0 && (int) $this->{$attr} === $lastInsertId) {
						$this->{$attr} = null;
						$changed = true;
					}
				}
			}
			if ($changed) {
				$this->save(false);
			}
		}
		if (!$this->contenuJson->revert()) {
			$this->addError('contenuJson', $this->formatContentErrors());
			if ($changed) { // restore the initial values
				$this->attributes = $backupAttributes;
				$this->save(false);
			}
			return false;
		}
		$this->hdateVal = time();
		$this->utilisateurIdVal = (PHP_SAPI === 'cli' ? null : (int) Yii::app()->user->id);
		$this->statut = 'refusé';
		return $this->save(false);
	}

	/**
	 * Returns an array of parents that have a direct "Suivi": [ [table => ,  id => ], ... ]
	 *
	 * @return array[]
	 */
	public function listParentsForSuivi(): array
	{
		$parents = [];
		if ($this->revueId) {
			$parents[] = ['table' => 'Revue', 'id' => $this->revueId];
		}
		if ($this->ressourceId) {
			$parents[] = ['table' => 'Ressource', 'id' => $this->ressourceId];
		}
		if ($this->editeurId) {
			$parents[] = ['table' => 'Editeur', 'id' => $this->editeurId];
		}
		if ($this->titreId) {
			foreach ($this->titre->editeurs as $e) {
				$parents[] = ['table' => 'Editeur', 'id' => $e->id];
			}
		}
		return $parents;
	}

	public function renderAsText()
	{
		static $formatter = null;

		if ($formatter === null) {
			$formatter = new \processes\intervention\Formatter();
			$formatter->showRestrictedInfo = true;
		}
		if (isset($_SERVER['SERVER_NAME'])) {
			$url = Yii::app()->createAbsoluteUrl('intervention/view', ['id' => $this->id]);
		} elseif (Yii::app()->params->contains('baseUrl')) {
			$url = Yii::app()->params->itemAt('baseUrl') . '/intervention/view?id=' . $this->id;
		} else {
			$url = 'Intervention : ' . $this->id;
		}
		return $this->description . ($this->import ? ' (import)' : '')
			. "\n$url\n"
			. preg_replace('/^/m', '  ', strip_tags(html_entity_decode($formatter->format($this))))
			. "\n" . Yii::app()->getDateFormatter()->formatDateTime($this->hdateProp) . "\n\n";
	}

	/**
	 * Sends an email describing the intervention to the configured 'email-alerte-editeurs'.
	 */
	public function emailForEditorChange(string $name = '', string $subject = 'Modification directe'): bool
	{
		Yii::log("emailForEditorChange() \nname/id : " . print_r($name, true) . " " . print_r($this->editeurId, true), "trace", "suivi");
		if (!$this->editeurId && !$name) {
			return false;
		}
		if (!$name) {
			$name = $this->editeur->nom;
		}
		$email = Config::read('suivi.editeurs.email');
		if (empty($email)) {
			return false;
		}
		$emails = array_filter(array_map('trim', explode("\n", $email)));

		if (isset($_SERVER['SERVER_NAME'])) {
			$url = Yii::app()->createAbsoluteUrl('intervention/view', ['id' => $this->id]);
		} elseif (Yii::app()->params->contains('baseUrl')) {
			$url = Yii::app()->params->itemAt('baseUrl') . '/intervention/view?id=' . $this->id;
		} else {
			$url = 'Intervention : ' . $this->id;
		}
		$formatter = new \processes\intervention\Formatter();
		$formatter->showRestrictedInfo = true;
		$html = html_entity_decode($formatter->format($this), ENT_COMPAT | ENT_HTML5);
		$formatted = preg_replace('/^/m', '  ', strip_tags($html));
		$comment = ($this->commentaire ? 'Commentaire : ' . $this->commentaire : '');
		$body = <<<EOTXT
			$url
			$this->description
			Statut : **{$this->statut}**
			$comment
			$formatted
			EOTXT;
		$message = Mailer::newMail()
			->subject(Yii::app()->name . " : {$subject} « {$name} »")
			->from(Config::read('email.from'))
			->setTo($emails)
			->text($body);

		return Mailer::sendMail($message);
	}

	/**
	 * -1 désactive les vérifications de différences au moment de l'application de l'intervention.
	 */
	public function setImport(int $importType): void
	{
		$this->import = $importType;
		if ($importType > 0) {
			$this->contenuJson->isImport = true;
		} elseif ($this->import === -1) {
			$this->contenuJson->disableDifferenceCheck = true;
		}
	}

	/**
	 * Update an intervention with the links (Service_Collection) with the collections.
	 *
	 * @param array $existingCollectionIds List of present collectionId.
	 * @param array $collectionIds List of wanted collectionId.
	 */
	public function updateCollections(?Service $service, array $existingCollectionIds, array $collectionIds, ?Log $log = null): void
	{
		if (empty($collectionIds)) {
			if ($service && $service->ressourceId && $service->ressource->hasCollections()) {
				$this->addError('ressourceId', "Un accès d'une ressource à collections doit avoir une collection.");
				return;
			}
			if (empty($existingCollectionIds)) {
				return;
			}
		} else {
			$allowedCids = Yii::app()->db
				->createCommand("SELECT id FROM Collection WHERE ressourceId = :rid")
				->queryColumn([':rid' => $service->ressourceId]);
			foreach ($collectionIds as $cid) {
				if (!in_array($cid, $allowedCids)) {
					$this->addError('ressourceId', "Cet accès doit avoir une collection de sa ressource.");
				}
			}
		}
		$serviceId = $service && $service->id ? (int) $service->id : 0;
		if (array_values($existingCollectionIds) != array_values($collectionIds)) {
			$toAdd = array_diff($collectionIds, $existingCollectionIds);
			if ($toAdd) {
				foreach ($toAdd as $cid) {
					$x = new ServiceCollection();
					$x->serviceId = $serviceId;
					$x->collectionId = (int) $cid;
					$this->contenuJson->create($x);
				}
				if ($log) {
					$log->addLocal(
						'info',
						"Collections ajoutées "
						. ($serviceId ? "à l'accès $serviceId" : "au nouvel accès")
						. " : IDs " . join(', ', $toAdd)
					);
				}
			}

			$toRemove = array_diff($existingCollectionIds, $collectionIds);
			if ($toRemove) {
				foreach ($toRemove as $cid) {
					$x = new ServiceCollection();
					$x->serviceId = $serviceId;
					$x->collectionId = (int) $cid;
					$x->isNewRecord = false;
					$this->contenuJson->delete($x);
				}
				if ($log) {
					$log->addLocal('info', "Collections retirées à l'accès $serviceId : " . join(', ', $toRemove));
				}
			}
		}
	}

	/**
	 * Apply the changes "insert", "update" et "delete", as produced by ::loadMultiple().
	 */
	public function addChanges(array $changes): void
	{
		if (isset($changes['insert'])) {
			foreach ($changes['insert'] as $data) {
				$this->contenuJson->create($data['after']);
			}
		}
		if (isset($changes['update'])) {
			foreach ($changes['update'] as $data) {
				$this->contenuJson->update($data['before'], $data['after']);
			}
		}
		if (isset($changes['delete'])) {
			foreach ($changes['delete'] as $data) {
				$this->contenuJson->delete($data['before']);
			}
		}
	}

	public function render(): HtmlHelper
	{
		return new HtmlHelper($this);
	}

	/**
	 * Called automatically before validate().
	 */
	protected function beforeValidate()
	{
		if ($this->scenario !== 'search' && $this->getIsNewRecord()) {
			if (PHP_SAPI !== 'cli' && $this->scenario === 'insert' && Yii::app()->user->isGuest) {
				$this->validateAntispam();
			}
		}
		return parent::beforeValidate();
	}

	/**
	 * Called automatically after validate().
	 */
	protected function afterValidate()
	{
		parent::afterValidate();
		if ($this->scenario !== 'search' && $this->getIsNewRecord()) {
			if (empty($this->hdateProp)) {
				$this->hdateProp = time();
			}
			if (empty($this->ip)) {
				$this->ip = $_SERVER['REMOTE_ADDR'] ?? '';
			}
		}
		if ($this->scenario === 'search' || $this->scenario === 'import') {
			return;
		}
		if (empty($this->errors) && $this->getIsNewRecord() && !$this->commentaire) {
			if ($this->contenuJson instanceof InterventionDetail) {
				$detail = $this->contenuJson;
			} else {
				$detail = new InterventionDetail(json_decode((string) $this->contenuJson, true));
			}
			if ($detail->isEmpty()) {
				$this->addError(
					'commentaire',
					"Aucune modification des données. Veuillez au moins fournir un commentaire."
				);
			}
		}
	}

	/**
	 * @inheritdoc
	 */
	protected function afterSave()
	{
		if ($this->titreId > 0) {
			\Yii::app()->db
				->createCommand("INSERT IGNORE INTO IndexingRequiredTitre (id, updated, deleted) VALUES (:id, :time, 0)")
				->execute([':id' => $this->titreId, ':time' => time()]);
		} elseif ($this->revueId > 0) {
			\Yii::app()->db
				->createCommand("INSERT IGNORE INTO IndexingRequiredTitre (id, updated, deleted) SELECT id, :time, 0 FROM Titre WHERE revueId = :id")
				->execute([':id' => $this->revueId, ':time' => time()]);
		}
		return parent::afterSave();
	}

	/**
	 * Called automatically before save().
	 *
	 * @return bool
	 */
	protected function beforeSave()
	{
		if ($this->commentaire && $this->scenario === 'insert') {
			$this->commentaire = CHtml::encode(strip_tags(Tools::normalizeText($this->commentaire)));
		}
		if ($this->isNewRecord) {
			if (empty($this->email)) {
				$this->email = '';
			}
			if (empty($this->commentaire)) {
				$this->commentaire = '';
			}
		}
		foreach (['revue', 'titre', 'ressource', 'editeur'] as $k) {
			if ((int) $this->{"{$k}Id"} === 0) {
				$this->{"{$k}Id"} = null;
			}
		}
		return parent::beforeSave();
	}

	/**
	 * Update the 'hdateModif' field of each modified object.
	 */
	protected function updateDates(): void
	{
		foreach (['Titre', 'Editeur', 'Ressource'] as $attr) {
			$attrId = strtolower($attr) . 'Id';
			if (!empty($this->{$attrId})) {
				call_user_func([$attr, 'model'])->updateByPk(
					$this->{$attrId},
					['hdateModif' => time()]
				);
			}
		}
	}

	/**
	 * Fills the Fk of the Intervention if it is a creation.
	 */
	protected function updateFk(): void
	{
		if (empty($this->contenuJson->lastInsertId)) {
			return;
		}
		$tables = ["Revue", "Titre", "Editeur", "Ressource"];
		foreach ($tables as $table) {
			$attr = lcfirst($table) . "Id";
			if (property_exists($this, $attr) || $this->hasAttribute($attr)) {
				if (!empty($this->contenuJson->lastInsertId[$table]) && empty($this->{$attr})) {
					$this->{$attr} = $this->contenuJson->lastInsertId[$table];
				}
			}
		}
	}

	private function formatContentErrors(): string
	{
		$errors = [];
		foreach ($this->contenuJson->getErrors() as $attr => $perAttr) {
			$prefix = (is_int($attr) || ctype_digit($attr) ? "" : "**$attr** : ");
			$errors[] = $prefix . (is_array($perAttr) ? join(" // ", $perAttr) : (string) $perAttr);
		}
		return join("\n////\n", $errors);
	}
}
