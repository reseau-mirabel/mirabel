<?php

use components\SqlHelper;

/**
 * This is the model class for table "Revue".
 *
 * The followings are the available columns in table 'Revue':
 * @property int $id
 * @property string $statut
 * @property int $hdateVerif timestamp
 *
 * @property Titre[] $titres
 * @property Categorie[] $categories
 * @property CategorieRevue[] $categorieLinks
 * @property Titre $activeTitle
 */
class Revue extends CActiveRecord implements IWithIndirectSuivi, TitledObject, WithSelfUrl
{
	public static $enumStatut = ['normal', 'suppr', 'attente'];

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Revue';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['statut', 'required'],
			['statut', 'in', 'range' => self::$enumStatut], // enum
		];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'titres' => [self::HAS_MANY, 'Titre', 'revueId', 'order' => "(titres.obsoletePar IS NULL) DESC, (titres.dateFin = '') DESC, titres.dateFin DESC, titres.dateDebut DESC, titres.titre DESC, titres.id DESC"],
			'categories' => [self::MANY_MANY, 'Categorie', 'Categorie_Revue(revueId, categorieId)', 'order' => 'categories.chemin'],
			'categorieLinks' => [self::HAS_MANY, 'CategorieRevue', 'revueId', 'with' => ['categorie', 'modifiePar'], 'order' => 'categorie.categorie'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'statut' => 'Statut',
			'hdateVerif' => 'Dernière vérification',
		];
	}

	/**
	 * Each array key defines a function that can be chained before a find*() method.
	 */
	public function scopes()
	{
		return [
			'sorted' => [
				'join' => 'JOIN Titre ON (Titre.revueId = t.id AND Titre.obsoletePar IS NULL)',
				'order' => 'Titre.titre ASC',
			],
		];
	}

	/**
	 * Delete related data in "Suivi".
	 */
	public function afterDelete()
	{
		if ($this->id > 0) {
			Yii::app()->db->createCommand(
				"DELETE FROM Suivi WHERE cible = '" . $this->tableName() . "' AND cibleId = " . (int) $this->id
			)->execute();
		}
		return parent::afterDelete();
	}

	/**
	 * Returns an array of parents that have a direct "Suivi": [ [table => ,  id => ], ... ]
	 */
	public function listParentsForSuivi(): array
	{
		return [
			['table' => 'Revue', 'id' => $this->id],
		];
	}

	/**
	 * Returns an assoc array(titreId => Titre) linked to this object.
	 *
	 * @param bool $assoc (opt, true)
	 * @return Titre[] assoc array(titreId => Titre).
	 */
	public function getTitres(bool $assoc = true): array
	{
		if ($this->isNewRecord) {
			return [];
		}
		if ($assoc) {
			return SqlHelper::sqlToPairsObject(
				"SELECT * FROM Titre WHERE revueId = {$this->id} "
				. "ORDER BY (obsoletePar IS NULL) DESC, dateDebut DESC",
				"Titre"
			);
		}
		$criteria = new CDbCriteria();
		$criteria->condition = 'revueId = ' . (int) $this->id;
		$criteria->order = "(obsoletePar IS NULL) DESC, dateDebut DESC";
		return Titre::model()->findAll($criteria);
	}

	/**
	 * Returns titles ordered by the obsolescence chain.
	 *
	 * @return Titre[]
	 */
	public function getTitresOrderedByObsolete(): array
	{
		if (!$this->id) {
			return [];
		}

		return Titre::model()->findAllBySql(<<<EOSQL
			WITH RECURSIVE TitreChaines AS (
				SELECT id, 1 as depth
				FROM Titre
				WHERE obsoletePar IS NULL AND revueId = :revueId

				UNION ALL

				SELECT t2.id, tc.depth + 1
				FROM Titre t2
					JOIN TitreChaines tc ON tc.id = t2.obsoletePar
				WHERE tc.depth < 100
			)
			SELECT t.*
			FROM Titre t
				LEFT JOIN TitreChaines tc ON t.id = tc.id
			WHERE revueId = :revueId
			ORDER BY
				CASE
					WHEN tc.depth IS NULL THEN 1
					ELSE 0
				END,
				tc.depth ASC;
			EOSQL,
			[':revueId' => $this->id]
		);
	}

	/**
	 * Returns the active title.
	 */
	public function getActiveTitle(): ?Titre
	{
		return Titre::model()->findByAttributes(
			['revueId' => $this->id, 'statut' => 'normal', 'obsoletePar' => null]
		);
	}

	/**
	 * Returns the full active title, prefix included, 'sigle' appended.
	 */
	public function getFullTitle(): string
	{
		$active = $this->getActiveTitle();
		if ($active) {
			return $active->getFullTitle();
		}
		Yii::log("Revue {$this->id} sans titre", "error");
		return "TITRE SUPPRIMÉ";
	}

	/**
	 * Alias nom=titre.
	 */
	public function getNom(): string
	{
		$active = $this->getActiveTitle();
		if ($active) {
			return $active->titre;
		}
		return "TITRE SUPPRIMÉ";
	}

	/**
	 * Returns the HTML link to one-self.
	 *
	 * @codeCoverageIgnore
	 * @return string HTML link.
	 */
	public function getSelfLink(): string
	{
		$htmlOptions = [];
		if (Yii::app()->user->hasState('suivi')) {
			$suivi = Yii::app()->user->getState('suivi');
			if (!empty($suivi['Revue']) && in_array($this->id, $suivi['Revue'])) {
				$htmlOptions["class"] = 'suivi-self';
			}
		}
		$params = $this->getSelfUrl();
		$path = array_shift($params);
		return CHtml::link(
			$this->getFullTitle(),
			Yii::app()->createUrl($path, $params),
			$htmlOptions
		);
	}

	public function getSelfUrl(): array
	{
		$titres = $this->titres;
		if (empty($titres)) {
			return ['/revue/view', 'id' => $this->id];
		}
		return [
			'/revue/view',
			'id' => $this->id,
			'nom' => Norm::urlParam($titres[0]->getFullTitle()),
		];
	}

	/**
	 * Returns a list of Service (with its Ressource) linked to this Revue (through a Titre).
	 *
	 * @return Service[]
	 */
	public function getServices(): array
	{
		if (!$this->id) {
			throw new \Exception("Need an ID.");
		}
		$criteria = new CDbCriteria;
		$criteria->with = [
			'ressource' => ['joinType' => 'INNER JOIN'],
			'titre' => ['joinType' => 'INNER JOIN', 'select' => 'titre.revueId, titre.titre, titre.sigle'],
		];
		$criteria->order = 't.type, t.acces, t.dateBarrDebut DESC';
		$criteria->condition = 'titre.revueId = ' . $this->id;
		return Service::model()->findAll($criteria);
	}

	/**
	 * Returns a list of Partenaire linked to this Revue (through Suivi).
	 *
	 * @return Partenaire[]
	 */
	public function getPartenairesSuivant(): array
	{
		if (empty($this->id)) {
			return [];
		}
		return Partenaire::model()->findAllBySql(
			"SELECT p.* "
			. "FROM Partenaire p JOIN Suivi s ON p.id = s.partenaireId "
			. "WHERE s.cible = 'Revue' AND s.cibleId = {$this->id} "
			. "ORDER BY p.nom ASC"
		);
	}

	/**
	 * Returns an array of collections linked to this Revue through Titre.
	 */
	public function getCollections(bool $ressources = true, bool $auth = false): array
	{
		if (empty($this->id)) {
			return [];
		}
		$results = [];
		if ($ressources) {
			$visibleCond = ($auth ? "" : "AND c.visible = 1");
			$sql = <<<EOSQL
				SELECT c.*, r.id AS r_id, r.nom AS r_nom, t.titre AS t_titre
				FROM Ressource r
					JOIN Service s ON s.ressourceId = r.id
					JOIN Titre t ON s.titreId = t.id
					LEFT JOIN Service_Collection sc ON sc.serviceId = s.id
					LEFT JOIN Collection c ON r.id = c.ressourceId AND sc.collectionId = c.id $visibleCond
				WHERE t.revueId = {$this->id}
				GROUP BY r.id, c.id
				ORDER BY r.nom, c.nom
				EOSQL;
		} else {
			$sql = <<<EOSQL
				SELECT c.*, r.id AS r_id, r.nom AS r_nom, t.titre AS t_titre
				FROM Collection c
					JOIN Service_Collection sc ON sc.collectionId = c.id
					JOIN Service s ON s.id = sc.serviceId
					JOIN Titre t ON t.id = s.titreId
					JOIN Ressource r ON r.id = c.ressourceId
				WHERE t.revueId = {$this->id}
				GROUP BY c.id
				ORDER BY r.nom, c.nom
				EOSQL;
		}
		$stmt = Yii::app()->db->createCommand($sql)->query();
		foreach ($stmt as $row) {
			$results[] = [
				'collection' => Collection::model()->populateRecord($row, false),
				'ressourceId' => $row['r_id'],
				'ressourceNom' => $row['r_nom'],
				'titreTitre' => $row['t_titre'],
			];
		}
		return $results;
	}

	/**
	 * Return true if the Revue has at least one ISSN-L.
	 */
	public function hasIssnl(): bool
	{
		return (boolean) $this->dbConnection
			->createCommand(
				"SELECT 1 FROM Issn i JOIN Titre t ON t.id = i.titreId"
				. " WHERE t.revueId = :rid AND i.issnl IS NOT NULL LIMIT 1"
			)
			->queryScalar([':rid' => $this->id]);
	}

	/**
	 * Return an array of ISSN groups.
	 *
	 * E.g.:
	 * {
	 *   issnp: {44: ["", "1234-5678", "1122-3344"]},
	 *   issne: {45: ["8765-4321"]},
	 *   issnl: {},
	 *   dates: {44: "2001-2002"}
	 * }
	 *
	 * @return array with keys "issnp", "issne", "issnl", "dates"
	 */
	public function getIssnGroups(): array
	{
		if (!$this->id) {
			return [];
		}
		$issns = ['issnp' => [], 'issne' => [], 'issnl' => [], 'dates' => []];
		$fromDb = Yii::app()->db
			->createCommand(<<<EOSQL
				SELECT i.*, NOT (i.dateDebut = t.dateDebut AND i.dateFin = t.dateFin) AS datesDiffer
				FROM Issn i
					JOIN Titre t ON t.id = i.titreId
				WHERE t.revueId = :rid AND (issn IS NOT NULL OR sudocPpn IS NOT NULL)
				EOSQL
			)->queryAll(true, [':rid' => $this->id]);
		if ($fromDb) {
			$displayDates = [];
			$datesInfo = [];
			foreach ($fromDb as $issnData) {
				$issn = Issn::model()->populateRecord($issnData);
				$displayDates[$issn->titreId] = ($displayDates[$issn->titreId] ?? false) || (bool) $issnData['datesDiffer'];
				$datesInfo[$issn->titreId][] = sprintf("%s : %s - %s", $issn->issn, $issn->dateDebut, $issn->dateFin);
				if ($issn->support === Issn::SUPPORT_PAPIER || $issn->support === Issn::SUPPORT_INCONNU) {
					if (isset($issns['issnp'][$issn->titreId])) {
						$issns['issnp'][$issn->titreId][] = $issn;
					} else {
						$issns['issnp'][$issn->titreId] = [$issn];
					}
				} elseif ($issn->support === Issn::SUPPORT_ELECTRONIQUE) {
					if (isset($issns['issne'][$issn->titreId])) {
						$issns['issne'][$issn->titreId][] = $issn;
					} else {
						$issns['issne'][$issn->titreId] = [$issn];
					}
				}
				if ($issn->issnl) {
					if (isset($issns['issnl'][$issn->titreId])) {
						$issns['issnl'][$issn->titreId][] = $issn;
					} else {
						$issns['issnl'][$issn->titreId] = [$issn];
					}
				}
			}
			foreach ($displayDates as $id => $status) {
				if ($status) {
					$issns['dates'][$id] = join("\n", $datesInfo[$id]);
				}
			}
		}
		return $issns;
	}

	/**
	 * Does it have a Service with automated imports through collections?
	 *
	 * @return string[] Collection.nom
	 */
	public function getCollectionsImport(): array
	{
		if (!$this->id) {
			return [];
		}
		return Yii::app()->db
			->createCommand(<<<EOSQL
				SELECT CONCAT(c.nom, ' de ', r.nom)
				FROM Titre t
					JOIN Service s ON s.titreId = t.id JOIN Service_Collection sc ON sc.serviceId = s.id
					JOIN Collection c ON c.id = sc.collectionId AND importee > 0 JOIN Ressource r ON r.id = c.ressourceId
				WHERE t.revueId = {$this->id}
				GROUP BY c.id, c.nom, r.nom
				ORDER BY c.nom
				EOSQL
			)->queryColumn();
	}

	/**
	 * Does it have a Service with automated imports?
	 */
	public function hasRessourceImport(): bool
	{
		if (!$this->id) {
			return false;
		}
		$r = Yii::app()->db
			->createCommand(<<<EOSQL
				SELECT r.id
				FROM Titre t
					JOIN Service s ON s.titreId = t.id
					JOIN Ressource r ON r.id = s.ressourceId
				WHERE r.autoImport = 1 AND t.revueId = {$this->id}
				EOSQL
			)->query();
		return (count($r) > 0);
	}

	/**
	 * Return the list of Ressource.nom.
	 *
	 * @return string[]
	 */
	public function getRessourcesNamesWithImport(): array
	{
		if (!$this->id) {
			return [];
		}
		return Yii::app()->db->createCommand(<<<EOSQL
			SELECT DISTINCT r.nom
			FROM Titre t
				JOIN Service s ON s.titreId = t.id
				JOIN Ressource r ON r.id = s.ressourceId
			WHERE r.autoImport = 1 AND t.revueId = {$this->id}
			ORDER BY r.nom ASC
			EOSQL
		)->queryColumn();
	}

	/**
	 * Return [categorieId => Categorie] mixing direct associations and those through a Vocabulaire.
	 *
	 * @return array<int, Categorie>
	 */
	public function getThemes(): array
	{
		$byRevue = SqlHelper::sqlToPairsObject(<<<EOSQL
			SELECT c.*
			FROM Categorie_Revue cr
				JOIN Categorie c ON cr.categorieId = c.id
			WHERE cr.revueId = {$this->id}
			ORDER BY c.categorie
			EOSQL,
			"Categorie"
		);
		if ($byRevue) {
			return $byRevue;
		}
		return SqlHelper::sqlToPairsObject(<<<EOSQL
			SELECT c.*
			FROM Titre t
				JOIN CategorieAlias_Titre cat ON t.id = cat.titreId
				JOIN CategorieAlias ca ON cat.categorieAliasId = ca.id
				JOIN Categorie c ON ca.categorieId = c.id
			WHERE t.revueId = {$this->id}
			GROUP BY c.id
			ORDER BY c.categorie
			EOSQL,
			"Categorie"
		);
	}

	/**
	 * Return array without direct associations (only those through a Vocabulaire).
	 */
	public function getThemesTitres(): array
	{
		return Yii::app()->db
			->createCommand(<<<EOSQL
				SELECT c.*, t.id AS titreId, t.titre AS titre, v.titre AS vocabulaire, ca.alias
				FROM Categorie c
					JOIN CategorieAlias ca ON ca.categorieId = c.id
					JOIN CategorieAlias_Titre cat ON cat.categorieAliasId = ca.id
					JOIN Titre t ON t.id = cat.titreId
					JOIN Vocabulaire v ON v.id = ca.vocabulaireId
				WHERE t.revueId = {$this->id}
				GROUP BY c.id, t.id
				ORDER BY c.categorie, t.titre
				EOSQL
			)->queryAll();
	}
}
