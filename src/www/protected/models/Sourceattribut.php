<?php


/**
 * This is the model class for table "Sourceattribut".
 *
 * The followings are the available columns
 * @property int $id
 * @property string $nom
 * @property string $identifiant
 * @property string $description
 * @property int $sourcelienId
 * @property int $visibilite
 *
 * @method self sorted() See scopes().
 */
class Sourceattribut extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Sourceattribut';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['nom, identifiant', 'required'],
			['nom, description', 'length', 'max' => 255],
			['identifiant', 'length', 'max' => 32],
			['sourcelienId, visibilite', 'numerical', 'integerOnly' => true],
			// The following rule is used by search().
			['nom, identifiant', 'safe', 'on' => 'search'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'nom' => 'Nom',
			'identifiant' => 'Identifiant',
			'description' => 'Description',
			'visibilite' => 'Visibilité',
		];
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @param int|bool $pageSize Number of items for pagination (false to disable pagination).
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($pageSize = 25)
	{
		$criteria = new CDbCriteria;

		$criteria->compare('nom', $this->nom, true);
		$criteria->compare('identifiant', $this->identifiant, true);

		$sort = new CSort();
		$sort->attributes = ['nom', 'identifiant'];
		$sort->defaultOrder = 'nom ASC';

		return new CActiveDataProvider(
			$this,
			[
				'criteria' => $criteria,
				'pagination' => ['pageSize' => $pageSize],
				'sort' => $sort,
			]
		);
	}

	/**
	 * Each array key defines a function that can be chained before a find*() method.
	 */
	public function scopes()
	{
		return [
			'sorted' => [
				'order' => 'identifiant ASC',
			],
		];
	}

	/**
	 * @param Titre $titre
	 * @param bool $all Si faux (défaut), seulement les visibles. Si vrai, visibilite ignorée.
	 * @return array [$identifiant => ['nom' => , 'valeur' => , 'visibilite' => ]]
	 */
	public static function getAttributesForTitre(Titre $titre, bool $all = false): array
	{
		$sql = <<<EOSQL
			SELECT sa.identifiant, at.valeur, sa.nom, sa.visibilite
			FROM Sourceattribut sa
				JOIN AttributTitre at ON sa.id = at.sourceattributId
			WHERE at.titreId = :tid
			EOSQL;
		if (!$all) {
			$sql .= " AND visibilite = 1";
		}
		$result = [];
		$query = Yii::app()->db->createCommand($sql)->query([':tid' => $titre->id]);
		foreach ($query as $row) {
			$result[$row['identifiant']] = [
				'nom' => $row['nom'],
				'valeur' => $row['valeur'],
				'visibilite' => (int) $row['visibilite'],
			];
		}
		return $result;
	}

	public function isProtected(): bool
	{
		foreach (processes\attribut\DisplayedAttributes::NAMES as $protected) {
			if ($this->identifiant === null) {
				return false;
			}
			if ($this->identifiant === $protected) {
				return true;
			}
			if ($protected[strlen($protected) - 1] === '*') {
				if (strncmp((string) $this->identifiant, $protected, strlen($protected) - 1) === 0) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Called automatically before Save().
	 *
	 * @return bool
	 */
	protected function beforeSave()
	{
		$this->nom = Norm::text($this->nom);
		if (!$this->identifiant) {
			$this->identifiant = strtolower(preg_replace('/[\[(].+|[^a-zA-Z0-9]+/', '', Norm::unaccent($this->nom)));
		} else {
			$this->identifiant= strtolower(Norm::unaccent($this->identifiant));
		}

		if (empty($this->sourcelienId)) {
			$this->setAttribute('sourcelienId', null);
		}
		return parent::beforeSave();
	}
}
