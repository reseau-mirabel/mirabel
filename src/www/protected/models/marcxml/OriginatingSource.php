<?php

namespace models\marcxml;

/**
 * 801 ORIGINATING SOURCE
 *
 * This field contains an indication of the origin of the record, including one of the following: the agency
 * that created the data, the agency that transcribed the data into machine-readable form, any agency that
 * has modified the original record/data, and the agency issuing the present record.
 *
 * See https://www.ifla.org/files/assets/uca/publications/unimarc-holdings-format.pdf
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class OriginatingSource
{
	public $country;

	public $agency;

	public $date;

	public function __construct(array $datafield)
	{
		if (empty($datafield["a"]) || strlen($datafield["a"]) !== 2) {
			throw new \Exception("Invalid country code");
		}
		$this->country = $datafield["a"];

		if (empty($datafield["b"])) {
			throw new \Exception("Missing agency subfield b");
		}
		$this->agency = $datafield["b"];

		if (empty($datafield["c"])) {
			throw new \Exception("Missing date subfield c");
		}
		$this->date = self::formatDate($datafield["c"]);
	}

	private static function formatDate(string $date): string
	{
		$firstChar = substr($date, 0, 1);
		if ($firstChar === '9' || $firstChar === 'X') {
			return "";
		}
		$m = [];
		if (preg_match('/^(\d{4})(\d\d)(\d\d)$/', $date, $m)) {
			return sprintf("%d-%02d-%02d", $m[1], $m[2], $m[3]);
		}
		if (preg_match('/^(\d{4})(\d\d)/', $date, $m)) {
			return sprintf("%d-%02d", $m[1], $m[2]);
		}
		if (preg_match('/^(\d{4})/', $date, $m)) {
			return $m[1];
		}
		throw new \Exception("Invalid date");
	}
}
