<?php

namespace models\marcxml;

/**
 * A Record stores the data that the Parser class reads in UNIMARC MARCXML (not MARC21).
 *
 * To access this data,
 * call a specific (unimarc) method like `getLangCodes()` or `getTitle()`,
 * or one of the generic methods:
 * - readValue('200$a') ==> list of values
 * - readFields('801') ==> list of fields
 * - getDataField('100', ["0", "1", " "], " ") ==> single field
 */
class Record
{
	//const ROLE_CATALOGING = 0;

	//const ROLE_TRANSCRIBING = 1;

	public const ROLE_MODIFYING = 2;

	public const ROLE_ISSUING = 3;

	/**
	 * @var array Store the whole XML as $this->data["200"][" 2"][0]["g"] = ["some value", "another"] (or "Single value")
	 */
	private array $data = [];

	private $leader = '';

	private $control = [];

	/**
	 * @internal Called by Parser
	 */
	public function setLeader(string $leader): void
	{
		$this->leader = $leader;
	}

	/**
	 * @internal Called by Parser
	 */
	public function addControl(string $tag, string $value): void
	{
		$this->control[$tag] = $value;
	}

	/**
	 * @internal Called by Parser
	 */
	public function addDataField(string $tag, string $ind1, string $ind2, array $values): void
	{
		$ind = "$ind1$ind2";
		if (!isset($this->data[$tag])) {
			$this->data[$tag] = [$ind => [$values]];
			return;
		}
		if (!isset($this->data[$tag][$ind])) {
			$this->data[$tag][$ind] = [$values];
			return;
		}
		$this->data[$tag][$ind][] = $values;
	}

	/**
	 * This is a low-level method when getGeneralInfo() is not enough.
	 *
	 * @return string 24 characters
	 */
	public function getLeader(): string
	{
		return $this->leader;
	}

	/**
	 * Return a control field value.
	 *
	 * This is a low-level method.
	 * A control field is a field/zone in the 001-009 range with no indicator, no subfield and no repetition).
	 *
	 * @param int $num With 1 <= $num <=9
	 */
	public function getControl(int $num): ?string
	{
		$key = sprintf("%03d", $num);
		return $this->control[$key] ?? null;
	}

	/**
	 * Return a list of the values in all the matching fields.
	 *
	 * The parameter applies the traditional MARC interpretation.
	 * For instance "210#2$g" is understood as:
	 * 1. zone "210"
	 * 2. indicator 1 " " (or "#" or "|", because of the content received from the Sudoc)
	 * 3. indicator 2 "2"
	 * 4. field "g"
	 *
	 * There may be several XML tags that match the level 1-3,
	 * and each can have several tags matching the level 4.
	 *
	 * @param string $selector E.g. "200$a"="200**$a", "500#1$b"="500 1$b", etc.
	 * @return string[]
	 */
	public function readValue(string $selector): array
	{
		// Parse the selector
		$m = [];
		if (preg_match('/^(\d\d\d)(.)(.)\$([0-9a-z])$/', $selector, $m)) {
			$field = $m[1];
			$ind1 = $m[2];
			$ind2 = $m[3];
			$subfield = $m[4];
			if ($ind1 === ' ' || $ind1 === '#') {
				$ind1 = '[ #\|]';
			} elseif ($ind1 === '*') {
				$ind1 = '.';
			}
			if ($ind2 === ' ' || $ind2 === '#') {
				$ind2 = '[ #\|]';
			} elseif ($ind2 === '*') {
				$ind2 = '.';
			}
		} elseif (preg_match('/^(\d\d\d)\$([0-9a-z])$/', $selector, $m)) {
			$field = $m[1];
			$ind1 = ".";
			$ind2 = ".";
			$subfield = $m[2];
		} else {
			throw new \Exception("The MARC request could not be parsed.");
		}
		$ind = $ind1 . $ind2;

		// Find the data matching the selector
		$result = [];
		if (!isset($this->data[$field])) {
			return $result;
		}
		if (isset($this->data[$field][$ind])) {
			foreach ($this->data[$field][$ind] as $datafields) {
				foreach ($datafields as $datafield) {
					if (isset($datafield[$subfield])) {
						$result[] = $datafield[$subfield];
					}
				}
			}
		} else {
			$pattern = "/^{$ind}\$/";
			foreach ($this->data[$field] as $indicators => $datafields) {
				if (!preg_match($pattern, $indicators)) {
					continue;
				}
				foreach ($datafields as $datafield) {
					if (isset($datafield[$subfield])) {
						$result[] = $datafield[$subfield];
					}
				}
			}
		}
		return $result;
	}

	/**
	 * Renvoie une liste des zones ("fields" in English) ayant le tag demandé.
	 *
	 * Chaque zone est représenté par un tableau associatif.
	 * Les indicateurs sont ignorés.
	 *
	 * Exemple d'appel :
	 * getDataField('181'); // Tous les champs avec tag="801", quelques soient les indicateurs
	 *
	 * Exemple de réponse (2 zones) :
	 * [ ["5" => "z01", "c" => "txt"], ["a" => "i#"] ]
	 *
	 * @return array<string|int, string>[]
	 */
	public function readFields(string $tag): array
	{
		if (isset($this->data[$tag])) {
			return array_merge(...array_values($this->data[$tag]));
		}
		return [];
	}

	/**
	 * Renvoie le premier bloc (tableaux associatifs) correspondant aux critères donnés.
	 *
	 * Exemples d'appels :
	 * getDataField("801", ["1", "2"]) // tag="801" && (ind1="1" || ind1="2") && ind2=" "
	 * getDataField("801", ["1"], "3") // tag="801" && ind1="1" && ind2="3"
	 *
	 * @param string[] $ind1 List of indicators, where the first found will be returned.
	 * @return array[] E.g. $result[0]["g"] --> "text" | ["text1", "text2"]
	 */
	public function getDataField(string $tag, array $ind1, string $ind2 = ' '): array
	{
		$keys = [];
		if (in_array(" ", $ind1, true) && !in_array("#", $ind1, true)) {
			$ind1[] = '#';
		}
		foreach ($ind1 as $ind1K) {
			$keys[] = $ind1K . $ind2;
		}
		foreach ($keys as $key) {
			if (isset($this->data[$tag][$key])) {
				return $this->data[$tag][$key];
			}
		}
		if ($ind2 === " ") {
			return $this->getDataField($tag, $ind1, "#");
		}
		return [];
	}

	public function getGeneralInfo(): GeneralInfo
	{
		$c100 = $this->getDataField("100", ["0", "1", " "], " ");
		return new GeneralInfo($this->leader, empty($c100) ? null : $c100[0]["a"]);
	}

	/**
	 * @return array of ISO 639-2 codes
	 */
	public function getLangCodes(): array
	{
		$data = $this->getDataField("101", ["0", "1", "2", " "], " ");
		if (!$data) {
			return [];
		}
		$langs = [];
		foreach ($data as $d) {
			if (!isset($d['a'])) {
				continue;
			}
			if (is_string($d['a'])) {
				$langs[] = $d['a'];
			} else {
				$langs = array_merge($langs, $d['a']);
			}
		}
		return $langs;
	}

	/**
	 * @param int $role See class constants ROLE_*
	 * @param string $agency
	 * @return OriginatingSource|null
	 */
	public function getOriginatingSource(int $role, string $agency): ?OriginatingSource
	{
		$sources = $this->getOriginatingSources($role);
		return $sources[$agency] ?? null;
	}

	/**
	 * Return an array of OriginatingSource instances indexed by agency name.
	 *
	 * @param int $role See class constants ROLE_*
	 * @return OriginatingSource[] indexed by agency name
	 */
	public function getOriginatingSources(int $role): array
	{
		if ($role < 0 || $role > 3) {
			throw new \Exception("Invalid role");
		}
		$sources = [];
		foreach ($this->getDataField("801", ["0", "1", "2", "3", " "], (string) $role) as $s) {
			$source = new OriginatingSource($s);
			$sources[$source->agency] = $source;
		}
		return $sources;
	}

	public function getOtherSystemsControlNumber(string $name): ?string
	{
		$numbers = $this->getOtherSystemsControlNumbers();
		return $numbers[$name] ?? null;
	}

	/**
	 * @return array E.g. ["OCoLC" => "61762295"]
	 */
	public function getOtherSystemsControlNumbers(): array
	{
		$codes = [];
		$m = [];
		foreach ($this->getDataField("034", [" "], " ") as $datafield) {
			if (!empty($datafield["a"]) && !empty($datafield["0"])) {
				$codes[$datafield["a"]] = $datafield["0"];
			}
		}
		foreach ($this->getDataField("035", [" "], " ") as $datafield) {
			if (!empty($datafield["a"]) && preg_match('/^\((.+)\)(.+)$/', $datafield["a"], $m)) {
				$codes[$m[1]] = $m[2];
			}
		}
		return $codes;
	}

	public function getTitle(): ?Title
	{
		$data = $this->getDataField("200", ["1", "0"], " ");
		return ($data ? new Title($data[0]) : null);
	}

	/**
	 * Return a list of URLs, ignoring those which are localized ($5).
	 *
	 * If the standard 856 4# is empty, the non-standard 999 ## is used instead.
	 *
	 * @return string[]
	 */
	public function getUrls(): array
	{
		$data = $this->getDataField("856", ["4"], " ");
		$urls = [];
		foreach ($data as $datafield) {
			if (!empty($datafield["5"]) || empty($datafield["u"])) {
				continue;
			}
			if (is_string($datafield["u"])) {
				$candidates = [$datafield["u"]];
			} else {
				$candidates = $datafield["u"];
			}
			foreach ($candidates as $c) {
				if (strncmp($c, "http", 4) === 0) {
					$urls[] = $c;
				}
			}
		}
		if (!$urls) {
			$data = $this->getDataField("999", [" "], " ");
			foreach ($data as $datafield) {
				if (empty($datafield["u"])) {
					continue;
				}
				if (is_string($datafield["u"])) {
					$candidates = [$datafield["u"]];
				} else {
					$candidates = $datafield["u"];
				}
				foreach ($candidates as $c) {
					if (strncmp($c, "http", 4) === 0) {
						$urls[] = $c;
					}
				}
			}
		}
		return $urls;
	}
}
