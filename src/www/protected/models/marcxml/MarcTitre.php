<?php

namespace models\marcxml;

class MarcTitre extends Record
{
	/**
	 * Return the ISSN info (only the first ISSN block) as a marcxml\Issn instance.
	 *
	 * @return Issn|null
	 */
	public function getIssn(): ?Issn
	{
		$data = $this->getDataField("011", ["0", "1", " "], " ");
		if (!$data) {
			return null;
		}
		$titleData = $this->getDataField("530", ["0", "1", " "], " ");
		foreach ($data as $d) {
			$issn = new Issn($d, $titleData);
			if ($issn->issn) {
				return $issn;
			}
		}
		return null;
	}
}
