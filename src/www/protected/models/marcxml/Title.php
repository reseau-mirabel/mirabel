<?php

namespace models\marcxml;

/**
 * 200 Title and statement of responsibility
 *
 * This field contains the title along with any other title information and statements of responsibility relating
 * to the title including any of the preceding repeated in other languages(parallel titles, parallel statements
 * of responsibility, etc.) generally in the form and sequence inwhich they appear on the item being catalogued.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class Title
{
	/**
	 * @var string Title Proper (a)
	 */
	public $title;

	/**
	 * @var string General Material Designation (b)
	 */
	public $material;

	/**
	 * @var string[]
	 */
	public $parallels = [];

	public function __construct(array $datafield)
	{
		if (empty($datafield["a"])) {
			throw new \Exception("Missing proper title in a");
		}
		$this->title = $datafield["a"];

		if (!empty($datafield["b"])) {
			if (is_array($datafield["b"])) {
				$this->material = join(" ; ", $datafield["b"]);
			} else {
				$this->material = $datafield["b"];
			}
		}

		if (!empty($datafield["d"])) {
			if (is_array($datafield["d"])) {
				$this->parallels = $datafield["d"];
			} else {
				$this->parallels = [$datafield["d"]];
			}
		}
	}
}
