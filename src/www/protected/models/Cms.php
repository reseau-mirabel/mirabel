<?php

/**
 * This is the model class for table "Cms".
 *
 * The followings are the available columns in table 'Cms':
 * @property int $id
 * @property string $name
 * @property bool $singlePage
 * @property string $pageTitle
 * @property string $type
 * @property string $content
 * @property string $categorisation { categories: ["a"], partenaires: [1], ressources: [2], sourcesliens: [] }
 * @property string $dcdescription
 * @property int $private
 * @property int $hdateCreat
 * @property int $hdateModif
 */
class Cms extends CActiveRecord
{
	private const CACHE_DURATION = 7200; // 2h

	public static $enumType = ['text', 'markdown', 'html'];

	public $newdate;

	private static ?\processes\cms\ExpandVariables $processor = null;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Cms';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['name', 'required'],
			['name, pageTitle', 'length', 'max' => 255],
			['private', 'boolean'],
			['type', 'length', 'max' => 8],
			['content, dcdescription', 'safe'],
			['categorisation', 'safe'],
			['newdate', 'length', 'max' => 16],
			['type', 'in', 'range' => self::$enumType], // enum
			// The following rule is used by search().
			['name, pageTitle, hdateCreat', 'safe', 'on' => 'search'],
			['singlePage', 'boolean', 'on' => 'search'],
		];
	}

	public function afterValidate()
	{
		if ($this->name && $this->singlePage) {
			$this->name = trim($this->name);
			$exists = Cms::model()->find('name = :name AND singlePage = :s', [':name' => $this->name, ':s' => (int) $this->singlePage]);
			if ($exists && $exists->id != $this->id) {
				$this->addError('name', "Ce nom de page n'est pas unique.");
			}
		}
		return parent::afterValidate();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => 'Nom',
			'singlePage' => 'Bloc-page',
			'pageTitle' => 'Titre de la page',
			'type' => 'Type',
			'content' => 'Contenu',
			'categorisation' => 'Catégorisation',
			'dcdescription' => "Méta description",
			'private' => "Privée",
			'hdateCreat' => 'Date de publication',
			'newdate' => 'Changer cette date',
			'hdateModif' => 'Dernière modification',
		];
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 */
	public function search(): \CActiveDataProvider
	{
		$criteria = new CDbCriteria;

		if ($this->name === 'pas de brèves') {
			$criteria->condition = "name != 'brève'";
			$this->name = '';
		} else {
			$criteria->compare('name', $this->name, true);
		}
		if (isset($this->singlePage)) {
			$criteria->compare('singlePage', (int) $this->singlePage);
		}
		if ($this->content) {
			$criteria->addSearchCondition('content', $this->content);
		}

		$sort = new CSort();
		$sort->attributes = ['name', 'type', 'content', 'hdateCreat'];
		$sort->defaultOrder = 'name ASC, hdateCreat DESC';

		return new CActiveDataProvider(
			$this,
			[
				'criteria' => $criteria,
				'pagination' => false,
				'sort' => $sort,
			]
		);
	}

	/**
	 * Returns block matching a CMS name.
	 *
	 * @param string $name Unique block identifier.
	 * @return ?Cms Block matching the criteria.
	 */
	public static function getBlock(string $name)
	{
		return self::model()->findByAttributes(['name' => $name]);
	}

	/**
	 * Prints the HTML block matching a CMS name.
	 *
	 * @param string $name Unique block identifier.
	 */
	public static function getBlockAsHtml(string $name): string
	{
		$block = self::getBlock($name);
		if (!$block) {
			Yii::log("Le bloc '$name' n'a pas été trouvé.", 'warning', 'cms');
			return '';
		}
		return "<div id=\"{$block->getHtmlId()}\">{$block->toHtml()}</div>";
	}

	public function isBloc(): bool
	{
		return ($this->name !== 'brève' && !$this->singlePage);
	}

	public function isBreve(): bool
	{
		return ($this->name === 'brève');
	}

	public function isPage(): bool
	{
		return (bool) $this->singlePage;
	}

	/**
	 * Returns the HTML block built from the type and content attributes.
	 *
	 * @return string HTML output
	 */
	public function toHtml(bool $expandVariables = true, ?Partenaire $partenaire = null): string
	{
		if ($partenaire === null && self::$processor === null) {
			$withPartenaire = (bool) Yii::app()->user->checkAccess("avec-partenaire");
			if ($withPartenaire) {
				$partenaireId = (int) Yii::app()->user->getState('partenaireId');
				if ($partenaireId > 0) {
					$partenaire = Partenaire::model()->findByPk($partenaireId);
				}
			}
		}

		switch ($this->type) {
			case 'text':
				$output = CHtml::encode($this->content);
				if ($expandVariables) {
					$output = self::expandVariables($output, $partenaire);
				}
				break;
			case 'html':
				$output = $this->content;
				if ($expandVariables) {
					$output = self::expandVariables($output, $partenaire);
				}
				break;
			case 'markdown':
				$md = Yii::app()->getComponent('markdown');
				assert($md instanceof \League\CommonMark\ConverterInterface);
				$rawMarkdown = $expandVariables ?
					self::getVariablesExpander($partenaire)->expandNumbers($this->content)
					: $this->content;
				$unsafeOutput = str_replace(
					['<table>', ' align="right">'],
					['<table class="table table-striped table-hover">', ' style="text-align:right;">'],
					$md->convert($rawMarkdown)->getContent()
				);

				$config = HTMLPurifier_HTML5Config::createDefault();
				$config->set('HTML.SafeIframe', true);
				$config->set('HTML.IframeAllowFullscreen', true);
				$config->set('URI.SafeIframeRegexp', '#^(?:/|https://player\.vimeo\.com/)#');
				$config->set('Attr.EnableID', true);
				$htmlPurifier = new CHtmlPurifier();
				$htmlPurifier->setOptions($config);
				$output = $htmlPurifier->purify($unsafeOutput);

				if ($expandVariables) {
					$output = self::getVariablesExpander($partenaire)->expandHtml($output);
				}
				break;
			default:
				$output = "";
		}
		if ($this->isBreve()) {
			$output = '<div class="cms-breve">' . $output . '</div>';
		} elseif ($this->isPage()) {
			$output = '<div class="cms-page">' . $output . '</div>';
		}
		if ($expandVariables) {
			return $this->filterOutput($output);
		}
		return $output;
	}

	public static function countLinks(): int
	{
		$value = Yii::app()->getCache()->get("CMS_countLinks");
		if ($value === false) {
			$db = Yii::app()->db;
			$numInTitles = $db
				->createCommand(<<<EOSQL
					SELECT
						count(distinct t.url) - 1
						+ count(distinct i.sudocPpn) - 1
						+ count(distinct i.worldcatOcn) - 1
					FROM Titre t
						LEFT JOIN Issn i ON i.titreId = t.id
					EOSQL
				)->queryScalar()
				+ (int) $db->createCommand("SELECT count(*) FROM LienTitre WHERE url <> ''")->queryScalar();
			$numInEd = (int) $db->createCommand("SELECT count(*) FROM Editeur WHERE url <> ''")->queryScalar()
				+ (int) $db->createCommand("SELECT count(*) FROM LienEditeur WHERE url <> ''")->queryScalar();
			$numInRess = (int) $db->createCommand("SELECT count(*) FROM Ressource WHERE url <> ''")->queryScalar();
			$numInServ = (int) $db->createCommand("SELECT count(DISTINCT url) FROM Service")->queryScalar();
			$value = $numInTitles + $numInEd + $numInRess + $numInServ;
			Yii::app()->getCache()->set("CMS_countLinks", $value, self::CACHE_DURATION);
		}
		return $value;
	}

	/**
	 * Called automatically before save().
	 *
	 * @return bool
	 */
	protected function beforeSave()
	{
		$this->hdateModif = $_SERVER['REQUEST_TIME'];
		if ($this->newdate) {
			try {
				$date = new DateTime($this->newdate);
				$this->hdateCreat = $date->getTimestamp();
			} catch (Throwable $e) {
				Yii::log("Wrong Cms date {$this->newdate}: {$e->getMessage()}");
				unset($this->hdateCreat);
			}
		} elseif ($this->isNewRecord && empty($this->hdateCreat)) {
			$this->hdateCreat = $_SERVER['REQUEST_TIME'];
		}
		return parent::beforeSave();
	}

	/**
	 * Replaces the special variables %MYVAR% by their values.
	 * @todo list the vars, get their values, use APC, replace.
	 *
	 * @param string $text Text where to replace.
	 * @param ?Partenaire $partenaire If the user is authenticated and has a Partenaire, use it for interpolating.
	 * @return string Text with variables replaced.
	 */
	protected static function expandVariables(string $text, ?Partenaire $partenaire): string
	{
		return self::getVariablesExpander($partenaire)->expand($text);
	}

	protected static function getVariablesExpander(?Partenaire $partenaire): \processes\cms\ExpandVariables
	{
		if (self::$processor === null || YII_ENV === 'test') {
			self::$processor = new \processes\cms\ExpandVariables($partenaire);
		}
		return self::$processor;
	}

	/**
	 * Filters each HTML output.
	 *
	 * @param string $html
	 * @return string
	 */
	protected function filterOutput(string $html): string
	{
		if ($this->name !== 'partenaires') {
			return $html;
		}
		$isAdmin = Yii::app()->user->access()->toUtilisateur()->admin();
		$append = "<p>Récapitulatif des ABOCOUNT abonnés à la liste de diffusion mirabel_partenaires (cette liste ne comprend pas les partenaires éditeurs).</p>"
			. '<ul class="partenaires">'
		;
		/** @var Partenaire[] */
		$partenaires = Partenaire::model()->ordered()->findAllByAttributes(['type' => 'normal']);
		$count = 0;
		foreach ($partenaires as $partenaire) {
			$append .= '<li><h3 class="statut-' . $partenaire->statut . '" title="' . $partenaire->statut . '">'
				. $partenaire->getSelfLink()
				. '</h3>'
				. '<div><table class="table table-condensed table-bordered"><tbody>' . "\n";
			/** @var Utilisateur[] */
			$utilisateurs = $partenaire->utilisateurs;
			if ($utilisateurs) {
				foreach ($utilisateurs as $u) {
					if ($u->actif) {
						$count++;
						$dateDiff = $u->derConnexion
							? trim((new DateTime())->setTimestamp($u->derConnexion)->diff(new DateTime)->format('%R%a jours'), '+')
							: '';
						$dateExact = date('d/m/Y', $u->derConnexion);
						$append .= "<tr>"
							. '<td width="35%">' . CHtml::encode($u->nomComplet) . "</td>"
							. '<td>' . $u->email . "</td>"
							. '<td style="width:20ex">' . ($isAdmin ? $u->getSelfLink(false) : CHtml::encode($u->login)) . "</td>"
							. (
								$isAdmin
								? '<td style="width:12ex">' . ($u->derConnexion ? "<span title=\"$dateExact\">$dateDiff</span>" : '-') . "</td>"
								: ''
							)
							. "</tr>\n";
					}
				}
				$append .= "</tbody></table></div></li>\n";
			}
		}
		$append .= "</ul>\n";
		$html .= str_replace('ABOCOUNT', (string) $count, $append);
		return $html;
	}

	/**
	 * Returns a valid HTML ID in ASCII, derived from he name.
	 */
	private function getHtmlId(): string
	{
		return "bloc-" . preg_replace(
			'/[^\w-]+/',
			'',
			strtr(strtolower(iconv('UTF-8', 'ASCII//TRANSLIT', $this->name)), ' "', '-_')
		);
	}
}
