<?php

/**
 * This is the model class for table "Titre_Editeur".
 *
 * The followings are the available columns in table 'Titre_Editeur':
 * @property int $titreId
 * @property int $editeurId
 * @property mixed $ancien bool "0"|"1"
 * @property mixed $commercial bool "0"|"1"
 * @property mixed $intellectuel bool "0"|"1"
 * @property ?string $role
 * @property string $hdateModif timestamp MySQL
 *
 * @property Titre $titre
 * @property Editeur $editeur
 */
class TitreEditeur extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Titre_Editeur';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['titreId, editeurId', 'required'],
			['titreId, editeurId', 'numerical', 'integerOnly' => true, 'allowEmpty' => false],
			['ancien, commercial, intellectuel', 'boolean'],
			['role', 'validateRole'],
		];
	}

	public function validateRole(string $attribute)
	{
		$value = $this->{$attribute};
		if ($value) {
			$allowed = self::getPossibleRoles();
			if (!isset($allowed[$value])) {
				$this->addError($attribute, "rôle inconnu");
			}
		} else {
			$value = null;
		}
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'titre' => [self::BELONGS_TO, 'Titre', 'titreId'],
			'editeur' => [self::BELONGS_TO, 'Editeur', 'editeurId'],
		];
	}

	public function attributeLabels()
	{
		return [
			'ancien' => "Éditeur précédent",
			'commercial' => "rôle de publication & diffusion",
			'intellectuel' => "rôle de direction & rédaction",
			'role' => "Fonction éditoriale",
			'hdateModif' => "Dernière modification",
		];
	}

	public function afterFind()
	{
		if ($this->ancien === null) {
			$this->ancien = '';
		}
		return parent::afterFind();
	}

	public function beforeSave()
	{
		$this->ancien = (int) $this->ancien;
		$this->commercial = (int) $this->commercial;
		$this->intellectuel = (int) $this->intellectuel;
		if ($this->role === '') {
			$this->role = null;
		}
		return parent::beforeSave();
	}

	public static function getPossibleRoles(): array
	{
		$config = Config::read('sherpa.publisher_role');
		$roles = [];
		foreach ($config as $row) {
			$roles[$row[1]] = $row[2];
		}
		return $roles;
	}

	public function isComplete(): bool
	{
		return $this->ancien !== null
			&& $this->role !== null;
	}
}
