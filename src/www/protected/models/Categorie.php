<?php

/**
 * This is the model class for table "Categorie".
 *
 * The followings are the available columns in table 'Categorie':
 * @property int $id
 * @property int $parentId
 * @property string $categorie
 * @property string $description
 * @property string $role
 * @property string $chemin
 * @property int $profondeur
 * @property string $hdateCreation
 * @property string $hdateModif
 * @property int $modifPar
 *
 * @property Categorie $parent
 * @property Categorie[] $children
 * @property CategorieAlias[] $categorieAliases
 * @property CategorieRevue[] $categorieRevues
 * @property ?CategorieStats $categorieStats
 * @property Utilisateur $modifiePar
 */
class Categorie extends CActiveRecord implements TitledObject
{
	public const MAX_DEPTH = 3;

	public const ID_PADDING = 5;

	public const ROLES_ENUM = [
		'public' => 'public',
		'candidat' => 'candidat',
		'refus' => 'refusé',
	];

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Categorie';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['categorie', 'required'],
			['role', 'required', 'on' => 'insert'],
			['categorie', 'unique', 'caseSensitive' => false, 'on' => 'update'],
			['parentId', 'numerical', 'integerOnly' => true, 'allowEmpty' => true],
			['parentId', 'exist', 'className' => 'Categorie', 'attributeName' => 'id', 'allowEmpty' => true],
			['categorie', 'length', 'max' => 255],
			['description', 'length', 'max' => 65535],
			['role', 'default', 'value' => 'candidat', 'on' => 'insert', 'setOnEmpty' => true],
			['role', 'default', 'value' => 'candidat', 'on' => 'insert-sansperm', 'setOnEmpty' => false],
			['role', 'in', 'range' => array_keys(self::ROLES_ENUM), 'on' => 'insert, update'], // enum
			// The following rule is used by search().
			['id, parentId, categorie, role, chemin, profondeur, hdateCreation, hdateModif, modifPar', 'safe', 'on' => 'search'],
		];
	}

	public function beforeValidate()
	{
		if ($this->parentId) {
			if ($this->parent->profondeur >= self::MAX_DEPTH) {
				$this->addError('parentId', "On ne peut créer de thème de niveau supérieur à " . self::MAX_DEPTH);
			}
			if ($this->id && (int) $this->parentId === (int) $this->id) {
				$this->addError('parentId', "Un thème ne peut être son propre parent.");
			}
		}
		if ($this->isNewRecord && $this->scenario === 'insert') {
			$exists = Categorie::model()->findByAttributes(['categorie' => $this->categorie]);
			if ($exists) {
				$this->addError(
					'categorie',
					sprintf(
						"Un thème de même nom existe déjà : [ Thème : <i>%s</i>, Rôle : <i>%s</i>, Création <i>%s</i>]",
						CHtml::link($exists->categorie, ['categorie/view', 'id' => $exists->id]),
						$exists->role,
						$exists->hdateModif
					)
				);
			}
		} elseif ($this->role === 'refus' && $this->scenario === 'update') {
			$nbrevues = CategorieRevue::model()->countByAttributes(['categorieId' => $this->id]);
			if ($nbrevues) {
				$this->addError(
					'role',
					sprintf(
						"Ce thème est associé à <a href=\"%s\">%d revue%s</a>, il ne peut passer au rôle 'refusé'.",
						Yii::app()->createAbsoluteUrl('revue/search', ['SearchTitre[categorie][]' => $this->id]),
						$nbrevues,
						($nbrevues > 1 ? 's' : '')
					)
				);
			}
		}
		return parent::beforeValidate();
	}

	public function beforeDelete()
	{
		if ($this->children) {
			return false;
		}
		if ($this->categorieRevues) {
			return false;
		}
		return parent::beforeValidate();
	}

	/**
	 * @param array $values
	 * @param bool $safeOnly
	 */
	public function setAttributes($values, $safeOnly = true)
	{
		parent::setAttributes($values, $safeOnly);
		if (isset($values['parentIdComplete'])) {
			if (empty($values['parentIdComplete'])) {
				$this->parentId = 1;
			} elseif (empty($values['parentId'])) {
				$parents = Categorie::model()->findAllByAttributes(['categorie' => $values['parentIdComplete']]);
				if ($parents && count($parents) === 1) {
					$this->parentId = $parents[0]->id;
				} else {
					$this->parentId = 1;
				}
			}
		}
	}

	/**
	 * @return bool
	 */
	public function beforeSave()
	{
		if (!$this->parentId) {
			$this->parentId = 1; // hardcoded root
			$this->profondeur = 1;
			$this->chemin = '/1';
		} elseif ($this->parentId > 0 && is_object($this->parent)) {
			$parent = $this->parent;
			$this->profondeur = $parent->profondeur + 1;
			$this->chemin = $parent->chemin . '/' . sprintf('%0' . self::ID_PADDING . 'd', $parent->id);
		}
		if (!(Yii::app() instanceof CConsoleApplication) && Yii::app()->user->id) {
			$this->modifPar = Yii::app()->user->id;
		}
		if ($this->isNewRecord) {
			$this->hdateCreation = date("Y-m-d H:i:s");
		}
		$this->hdateModif = date("Y-m-d H:i:s");
		return parent::beforeSave();
	}

	public function afterSave()
	{
		CategorieStats::fill();
		return parent::afterSave();
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'parent' => [self::BELONGS_TO, 'Categorie', 'parentId'],
			'children' => [self::HAS_MANY, 'Categorie', 'parentId', 'order' => 'children.id ASC'], // default sort at level 1
			'categorieAliases' => [self::HAS_MANY, 'CategorieAlias', 'categorieId'],
			'categorieRevues' => [self::HAS_MANY, 'CategorieRevue', 'categorieId'],
			'categorieStats' => [self::HAS_ONE, 'CategorieStats', 'id'],
			'modifiePar' => [self::BELONGS_TO, 'Utilisateur', 'modifPar'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'parentId' => 'Parent',
			'categorie' => 'Thème',
			'description' => 'Description',
			'role' => 'Rôle',
			'chemin' => 'Chemin',
			'profondeur' => 'Profondeur',
			'hdateCreation' => 'Date création',
			'hdateModif' => 'Date modif.',
			'modifPar' => 'Modifié par',
		];
	}

	/**
	 * @return array<int, Categorie>
	 */
	public function getAncestors(): array
	{
		$ancestors = [];
		if ($this->profondeur > 1) {
			foreach (array_filter(explode('/', (string) $this->chemin)) as $ancestorId) {
				if ($ancestorId > 0) {
					$c = Categorie::model()->findByPk($ancestorId);
					if ($c instanceof Categorie) {
						$ancestors[(int) $ancestorId] = $c;
					}
				}
			}
		}
		return $ancestors;
	}

	/**
	 * @return Categorie[]
	 */
	public function getChildrenList(bool $onlyPublic): array
	{
		$criteria = new CDbCriteria();
		$criteria->with = 'categorieStats';
		$criteria->condition = "t.parentId = {$this->id}";
		$criteria->order = "t.id ASC";
		if ($onlyPublic) {
			$criteria->addCondition("t.role = 'public'");
		}
		return self::model()->findAll($criteria);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @param int|bool $pageSize Number of items for pagination (false to disable pagination).
	 */
	public function search($pageSize = 25): CActiveDataProvider
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('parentId', $this->parentId);
		$criteria->compare('categorie', $this->categorie, true);
		$criteria->compare('role', $this->role);
		$criteria->compare('chemin', $this->chemin, true);
		$criteria->compare('profondeur', $this->profondeur);
		$criteria->compare('hdateCreation', $this->hdateCreation, true);
		$criteria->compare('hdateModif', $this->hdateModif, true);
		$criteria->compare('modifPar', $this->modifPar);

		$sort = new CSort();
		$sort->attributes = ['id', 'categorie', 'role', 'chemin', 'hdateCreation', 'hdateModif', 'modifPar'];
		$sort->defaultOrder = 'chemin ASC, categorie ASC';

		return new CActiveDataProvider(
			$this,
			[
				'criteria' => $criteria,
				'pagination' => ($pageSize ? ['pageSize' => $pageSize] : false),
				'sort' => $sort,
			]
		);
	}

	/**
	 * @param int $depth (opt, 0) How many levels to list
	 * @param bool $extra (opt, false) Add extra info to nodes
	 * @param string $role (opt, '')
	 * @param int $filterByUserId (opt, null) If set to an Utilisateur.id, only candidates of this user will be updatable.
	 * @return array
	 */
	public function getNode(int $depth = 0, bool $extra = false, string $role = '', int $filterByUserId = 0): array
	{
		if ($depth) {
			$children = array_values(array_filter(array_map(
				function (Categorie $x) use ($depth, $extra, $role, $filterByUserId) {
					if (!$role || $x->role == $role) {
						return $x->getNode($depth - 1, $extra, $role, $filterByUserId);
					}
					return null;
				},
				Categorie::model()->with('categorieStats')->findAll([
					'condition' => "parentId = {$this->id} AND role <> 'refus'",
					'order' => "categorie ASC",
				])
			)));
			if ($this->categorie === 'Chronologique') {
				usort(
					$children,
					function ($a, $b) {
						$specialOrder = [
							'Préhistoire' => 1,
							'Préhistoire et Protohistoire' => 1,
							'Antiquité' => 2,
							'Antiquité tardive' => 3,
							'Moyen Âge' => 4,
							'Époque moderne' => 5,
							'Époque contemporaine' => 6,
						];
						return ($specialOrder[$a['text']] ?? 99) <=> ($specialOrder[$b['text']] ?? 99);
					}
				);
			}
		} else {
			$children = Categorie::model()->exists("parentId = {$this->id}");
		}
		$node = [
			'id' => (int) $this->id,
			'text' => $this->categorie,
			'type' => ($this->role === 'public' ? ($this->profondeur == 1 ? 'root' : 'default') : $this->role),
			'children' => $children,
		];
		if ($extra) {
			$node['data'] = [
				'description' => nl2br($this->description),
				'searchableText' => strtolower(\Norm::unaccent("{$this->categorie} {$this->description}")),
				'updatable' => ($filterByUserId ? ($this->role === 'candidat' && $this->modifPar > 0 && $this->modifPar == $filterByUserId) : true),
				'url' => $this->getSelfAbsoluteUrl(),
				'search' => ($children ?
					Yii::app()->createUrl('revue/search', ['q[categorie][]' => $this->id, 'q[cNonRec][]' => $this->id])
					: $this->getSelfAbsoluteUrl()),
				'numRevues' => ($this->categorieStats ? $this->categorieStats->numRevuesInd : null),
				'numRevuesRec' => ($this->categorieStats ? $this->categorieStats->numRevuesIndRec : null),
				'depth' => $this->profondeur,
				'hasChildren' => !empty($children),
			];
		}
		return $node;
	}

	/**
	 * List each 'candidat' Categorie created by an unauthorized user.
	 *
	 * @return self[]
	 */
	public static function listProposals(): array
	{
		return self::model()->findAllBySql(<<<EOSQL
			SELECT c.*
			FROM Categorie c JOIN Utilisateur u ON c.modifPar = u.id
			WHERE c.role = 'candidat' AND u.permIndexation = 0 AND permAdmin = 0
			ORDER BY c.hdateCreation DESC
			EOSQL
		);
	}

	public function onUnsafeAttribute($name, $value)
	{
		// do nothing
	}

	public function getSelfLink(): string
	{
		if ($this->id) {
			return CHtml::link(CHtml::encode($this->categorie), ['categorie/view', 'id' => $this->id]);
		}
		return CHtml::encode($this->categorie);
	}

	/**
	 * Set a flash message in the user session.
	 */
	public static function buildFlashForCandidates(): void
	{
		$list = [];
		foreach (Categorie::listProposals() as $c) {
			/** @var Categorie $c */
			$list[] = '<tr>'
				. "<td>" . CHtml::link(CHtml::encode($c->categorie), ['categorie/update', 'id' => $c->id]) . "</td>"
				. "<td>" . $c->hdateCreation . "</td>"
				. "<td>" . $c->modifiePar->nomComplet . "</td>"
				. '</tr>';
		}
		if ($list) {
			$html = "<div>Les thèmes suivants ont été proposés par des utilisateurs sans permission d'indexation."
				. '<table border="1" id="categorie-candidats">'
				. '<thead><tr><th>Nom</th><th>création</th><th>Dernière modif.</th></tr></thead><tbody>'
				. join("", $list) . "</tbody></table>";
			Yii::app()->user->setFlash('info', $html);
		}
	}

	/**
	 * Return the descendants (recursive children) IDs.
	 *
	 * @return int[]
	 */
	public function getDescendantsIds(): array
	{
		if ($this->profondeur == 3) {
			return [];
		}
		$ids = Yii::app()->db
			->createCommand("SELECT id FROM Categorie WHERE chemin LIKE :c")
			->queryColumn([':c' => sprintf("%s/%0" . self::ID_PADDING . "d%%", $this->chemin, $this->id)]);
		return array_map('intval', $ids);
	}

	public function getSelfAbsoluteUrl(): string
	{
		$url = $this->getSelfUrl();
		$path = array_shift($url);
		return Yii::app()->createAbsoluteUrl($path, $url);
	}

	public function getSelfUrl(): array
	{
		return ['/categorie/revues', 'id' => $this->id, 'name' => Norm::urlParam($this->categorie)];
	}

	/**
	 * Return the descendants (recursive children) IDs of each ID given in the list.
	 *
	 * @param array $roots array of Category
	 * @return int[]
	 */
	public static function listDescendantsIds(array $roots): array
	{
		if (!$roots) {
			return [];
		}
		$conditions = array_map(
			function ($c) {
				return sprintf("(chemin LIKE '%s/%0" . self::ID_PADDING . "d%%')", $c->chemin, $c->id);
			},
			$roots
		);
		$ids = Yii::app()->db
			->createCommand("SELECT id FROM Categorie WHERE " . join(" OR ", $conditions))
			->queryColumn();
		return array_map('intval', $ids);
	}
}
