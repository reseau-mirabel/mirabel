<?php

namespace models\forms;

class PolitiqueForm extends \CFormModel
{
	/**
	 * @var string
	 */
	public $name = '';

	public $oa;

	/**
	 * @var bool
	 */
	public $open = false;

	/**
	 * @var array [ {description: X, url: Y} ]
	 */
	public $urls = [];

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return [
			['name', 'required'],
			['name', 'length', 'max' => 250],
			['open', 'boolean'],
			['urls', 'validateUrls'],
		];
	}

	public function loadPolitique(\Politique $p)
	{
		$this->name = $p->name;
		$policy = $p->getPublisherPolicy();
		$this->open = (empty($policy->open_access_prohibited) || $policy->open_access_prohibited === 'no');
		$this->urls = $policy->urls ?? [];
		$this->oa = $policy->permitted_oa ?? [];
	}

	public function validateUrls($attrName)
	{
		if (!is_array($this->{$attrName})) {
			$this->addError($attrName, "Invalid structure, array expected");
		}
		$validator = new \CUrlValidator();
		$validator->validateIDN = true;
		$filtered = [];
		foreach ($this->{$attrName} as $u) {
			if (empty($u['url'])) {
				continue;
			}
			if ($validator->validateValue($u['url'])) {
				$this->addError($attrName, "L'URL {$u['url']} est mal formée.");
			}
			$record = (object) ['url' => (string) $u['url']];
			if (!empty($u['description'])) {
				$record->description = (string) $u['description'];
			}
			$filtered[] = $record;
		}
		$this->urls = $filtered;
	}
}
