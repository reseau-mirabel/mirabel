<?php

namespace models\forms;

use Utilisateur;

class User extends \CFormModel
{
	public $id;

	public $partenaireId;

	public $login;

	public $email;

	public $nom;

	public $prenom;

	public $nomComplet;

	public $notes = "";

	public $actif = 1;

	public $permAdmin;

	public $permImport;

	public $permPartenaire;

	public $permIndexation;

	public $permRedaction;

	public $permPolitiques;

	public $suiviEditeurs;

	public $suiviNonSuivi;

	public $suiviPartenairesEditeurs;

	public $motdepasse;

	public $passwordDuplicate;

	public $authmethod;

	public $listeDiffusion;

	public $listeLyonnais;

	public $listeFranciliens;

	/**
	 * @var ?Utilisateur
	 */
	public $record;

	public function __construct(?Utilisateur $utilisateur)
	{
		if ($utilisateur === null) {
			$scenario =  'insert';
		} elseif (\Yii::app()->user->access()->toUtilisateur()->admin()) {
			$scenario = 'update';
		} else {
			$scenario = (int) \Yii::app()->user->id === (int) $utilisateur->id ? 'updateown' : 'update';
		}
		parent::__construct($scenario);
		if ($utilisateur) {
			$this->record = $utilisateur;
			foreach ($utilisateur->getAttributes() as $k => $v) {
				if (property_exists($this, $k) && $k !== 'motdepasse') {
					$this->{$k} = $v;
				}
			}
		} else {
			$this->record = new Utilisateur();
		}
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		if ($this->scenario === 'updateown') {
			if ($this->partenaireId) {
				return [
					['email', 'email'],
					['email', 'length', 'max' => 100],
					['email', 'validateEmailUnicity'],
					['listeDiffusion, listeLyonnais, listeFranciliens', 'boolean'],
					['motdepasse', 'length', 'max' => 50],
					['motdepasse', 'validatePasswordStrength'],
					['nom, prenom', 'length', 'max' => 100],
					['nomComplet', 'length', 'max' => 255],
					['passwordDuplicate', 'compare', 'compareAttribute' => 'motdepasse'],
				];
			}
			return [
				['email, nom, prenom', 'length', 'max' => 100],
				['nomComplet', 'length', 'max' => 255],
				['email', 'email'],
				['email', 'validateEmailUnicity'],
				['motdepasse', 'length', 'max' => 50],
				['motdepasse', 'validatePasswordStrength'],
				['passwordDuplicate', 'compare', 'compareAttribute' => 'motdepasse'],
			];
		}
		$updateUPolitiques = $this->id > 0 && $this->partenaireId === null;
		$rules = [
			['authmethod, partenaireId', 'numerical', 'integerOnly' => true],
			['login', 'length', 'max' => 50],
			['email', 'email'],
			['email, nom, prenom', 'length', 'max' => 100],
			['email', 'validateEmailUnicity'],
			['nomComplet', 'length', 'max' => 255],
			['notes', 'length', 'max' => 65535],
			['motdepasse', 'length', 'max' => 50],
			['motdepasse', 'validatePasswordStrength'],
			['passwordDuplicate', 'compare', 'compareAttribute' => 'motdepasse'],
			[
				'actif, listeDiffusion, listeLyonnais, listeFranciliens, '
				. 'permAdmin, permImport, permPartenaire, permIndexation, permRedaction, permPolitiques, '
				. 'suiviEditeurs, suiviNonSuivi, suiviPartenairesEditeurs',
				'boolean',
			],
		];
		if (!$updateUPolitiques) {
			$rules[] = ['partenaireId, login', 'required'];
			if ($this->hasIndependantLogin()) {
				$rules[] = ['login', 'match', 'pattern' => '/^[\w\d.@-]+$/']; // alphanum + "._-"
			}
		}
		return $rules;
	}

	/**
	 * Called automatically before validate().
	 *
	 * @return bool
	 */
	public function beforeValidate()
	{
		if ($this->authmethod == \Utilisateur::AUTHMETHOD_LDAP) {
			if (!empty($this->motdepasse) && $this->scenario !== 'insert') {
				$this->addError('motdepasse', "Un utilisateur LDAP ne peut pas avoir de mot de passe.");
			}
		} else {
			$this->authmethod = null;
		}
		if (strncmp($this->login, '_deleted_', 9) === 0 && $this->actif) {
			$this->addError('login', "Pour réactiver ce compte, corrigez le login et l'adresse électronique.");
		}
		return parent::beforeValidate();
	}

	/**
	 * Called automatically at the end of validate().
	 */
	public function afterValidate()
	{
		parent::afterValidate();
		if ($this->scenario === 'insert' && !$this->hasErrors('login')) {
			$sql = "SELECT 1 FROM Utilisateur WHERE login = :l";
			if ($this->record && $this->record->id > 0) {
				$sql .= " AND id = " . (int) $this->record->id;
			}
			if (\Yii::app()->db->createCommand($sql)->queryScalar([':l' => $this->login])) {
				$this->addError('login', "Ce login est déjà attribué.");
			}
		}
	}

	public function validateEmailUnicity(): void
	{
		if (!$this->email) {
			return;
		}
		if ($this->partenaireId > 0) {
			// This email must not in use by an utilisateur-politique.
			// But it can be shared with another utilisateur-partenaire.
			$excludeSelf = ($this->record->id > 0 ? "id <> {$this->record->id} AND" : "");
			$exists = \Yii::app()->db
				->createCommand("SELECT 1 FROM Utilisateur WHERE $excludeSelf email = :e AND partenaireId IS NULL LIMIT 1")
				->queryScalar([':e' => $this->email]);
		} else {
			// Updating an utilisateur-politique => strict unicity.
			$exists = \Yii::app()->db
				->createCommand("SELECT 1 FROM Utilisateur WHERE (email = :e1 OR login = :e2) AND id <> :id LIMIT 1")
				->queryScalar([':e1' => $this->email, ':e2' => $this->email, ':id' => $this->id]);
		}
		if ($exists) {
			$this->addError('email', "Cette adresse électronique est déjà présente dans Mir@bel.");
		}
	}

	public function validatePasswordStrength(string $attr, array $params): void
	{
		if (empty($this->{$attr})) {
			return;
		}
		$zxcvbn = new \ZxcvbnPhp\Zxcvbn();
		$userData = [$this->prenom, $this->nom, $this->email, $this->login];
		$eval = $zxcvbn->passwordStrength($this->{$attr}, $userData);
		if ($eval['score'] < 3) {
			$this->addError($attr, "La sécurité du mot de passe est insuffisante.");
		}
	}

	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'partenaireId' => 'Partenaire',
			'login' => 'Login',
			'email' => 'Adresse électronique',
			'nom' => 'Nom',
			'prenom' => 'Prénom',
			'nomComplet' => 'Nom complet',
			'notes' => "Notes privées",
			'actif' => 'Actif',
			'permAdmin' => 'Administrateur',
			'permImport' => "Permission d'import",
			'permPartenaire' => 'Permission sur son partenaire',
			'permIndexation' => 'Permission d\'indexation',
			'permRedaction' => 'Permission éditoriale globale',
			'permPolitiques' => "Validation des politiques",
			'suiviEditeurs' => 'Suivi des éditeurs',
			'suiviNonSuivi' => 'Suivi des objets non suivis',
			'suiviPartenairesEditeurs' => "Suivi des partenaires-éditeurs",
			'motdepasse' => 'Mot de passe',
			'passwordDuplicate' => 'Mot de passe (bis)',
			'authmethod' => 'Authentification par LDAP',
			'listeDiffusion' => "Liste de diffusion",
			'listeLyonnais' => "Liste de diffusion Lyonnais",
			'listeFranciliens' => "Liste de diffusion Franciliens",
		];
	}

	public function getAttributes($names = null)
	{
		$attributes = parent::getAttributes($names);
		unset($attributes['motdepasse']);
		if (empty($attributes['authmethod'])) {
			unset($attributes['authmethod']);
		}
		return $attributes;
	}

	public function getPartenaire(): ?\Partenaire
	{
		if ($this->partenaireId > 0) {
			return \Partenaire::model()->findByPk($this->partenaireId);
		}
		return null;
	}

	public function hasIndependantLogin(): bool
	{
		return \Yii::app()->user->access()->toUtilisateur()->admin()
			|| empty($this->record->login) || ($this->record->login !== $this->record->email);
	}

	public function setPartenaireId(?int $id): void
	{
		$this->partenaireId = $id;
		$this->record->partenaireId = $id;
	}

	public function save()
	{
		if (!$this->validate()) {
			return false;
		}
		if (!$this->partenaireId) {
			$this->partenaireId = null;
			$this->login = $this->email;
		} elseif (!$this->hasIndependantLogin()) {
			$this->login = $this->email;
		}
		$this->record->setAttributes($this->getAttributes(), false);
		if ($this->motdepasse) {
			$this->record->storePassword($this->motdepasse);
		}
		$success = $this->record->save(false);
		$this->id = (int) $this->record->id;
		return $success;
	}
}
