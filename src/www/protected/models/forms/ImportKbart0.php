<?php

namespace models\forms;

/**
 * First step of a KBART import.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class ImportKbart0 extends \CFormModel
{
	public const ENUM_FILTER_ACCESSTYPE = [
		'' => "Tout",
		'F' => "Libre (F)",
		'P' => "Restreint (P)",
	];

	public $kbartfile;

	public $kbarturl;

	public $ressourceId;

	public $collectionIds;

	public $filterAccessType;

	private string $kbartLocalPath = '';

	private ?\Ressource $ressource = null;

	public function rules()
	{
		return [
			['ressourceId', 'required'],
			['kbartfile', 'file', 'types' => 'csv,tsv,txt', 'allowEmpty' => true],
			['kbarturl', 'url'],
			['ressourceId', 'numerical', 'integerOnly' => true],
			['collectionIds', 'ext.validators.ArrayOfIntValidator'],
			['filterAccessType', 'in', 'range' => array_keys(self::ENUM_FILTER_ACCESSTYPE)],
		];
	}

	public function afterValidate()
	{
		parent::afterValidate();

		// Valid Ressource.id?
		if ($this->ressourceId > 0) {
			$this->ressource = \Ressource::model()->findByPk($this->ressourceId);
		} else {
			$this->ressource = null;
			$this->addError('ressourceId', "Ressource introuvable");
			return;
		}

		// Valid Collection.id list?
		if (empty($this->collectionIds)) {
			$this->collectionIds = [];
			$hasCollections = (bool) \Yii::app()->db
				->createCommand("SELECT 1 FROM Collection WHERE ressourceId = {$this->ressourceId} LIMIT 1")
				->queryScalar();
			if ($hasCollections) {
				$this->addError('collectionIds', "Impossible d'importer hors-collection dans une ressource à collections.");
			}
		} elseif (!$this->hasErrors('collectionIds')) {
			sort($this->collectionIds, SORT_NUMERIC);
			$joined = join(",", $this->collectionIds);
			$ids = array_map(
				'intval',
				\Yii::app()->db
					->createCommand("SELECT id FROM Collection WHERE ressourceId = {$this->ressourceId} AND id IN ($joined) ORDER BY id")
					->queryColumn()
			);
			if ($ids !== $this->collectionIds) {
				$this->addError('collectionIds', "Certaines des collections ne sont pas dans la ressource choisie.");
			}
		}

		// File or URL
		if (empty($this->kbartfile) && empty($this->kbarturl)) {
			$this->addError('kbartfile', "Fournir une URL ou déposer un fichier KBART.");
		}
	}

	public function attributeLabels()
	{
		return [
			'kbartfile' => "Fichier KBART local",
			'kbarturl' => "URL du KBART",
			'ressourceId' => "Ressource",
			'collectionIds' => "Collections",
			'filterAccessType' => "Type d'accès (diffusion)",
		];
	}

	/**
	 * @return string[]|null
	 */
	public function getPotentialCollections(): ?array
	{
		if (empty($this->ressourceId)) {
			return null;
		}
		$query = \Collection::model()->findAllBySql(
			"SELECT * FROM Collection WHERE ressourceId = :ressourceId ORDER BY nom ASC",
			[':ressourceId' => (int) $this->ressourceId]
		);
		$collections = [];
		foreach ($query as $c) {
			/* @var $c Collection */
			$collections[$c->id] = "{$c->nom}   ({$c->type})";
		}
		return $collections;
	}

	public function getRessource(): ?\Ressource
	{
		return $this->ressource;
	}

	public function getKbartFileHash($path = ''): string
	{
		/** @var array<string, string> */
		static $hashes = [];
		if ($path === '') {
			$path = $this->kbartLocalPath;
		}
		if (!$path) {
			throw new \Exception("Cannot compute a hash for a missing file");
		}
		if (!isset($hashes[$path])) {
			$hashes[$path] = (string) md5_file($path);
		}
		return $hashes[$path];
	}

	public function getKbartLocalPath(): string
	{
		return $this->kbartLocalPath;
	}

	public function getUploadFilename(): string
	{
		if ($this->kbarturl) {
			return $this->kbarturl;
		}
		return $this->kbartfile->name;
	}

	public function saveKbartFile(): bool
	{
		if ($this->kbarturl) {
			$dlder = new \components\curl\Downloader('kbart');
			$dlder->download($this->kbarturl);
			$tmpPath = $dlder->getLocalFilepath();
		} else {
			if (!($this->kbartfile instanceof \CUploadedFile)) {
				$this->addError('kbartfile', "Fichier mal déposé. Recommencez l'envoi.");
				return false;
			}
			$tmpPath = $this->kbartfile->tempName;
		}
		$convertedPath = (new \processes\kbart\convert\Convert($tmpPath))->apply();
		$this->kbartLocalPath = $this->buildKbartFilepath($convertedPath);

		$dir = dirname($this->kbartLocalPath);
		if (!is_dir($dir)) {
			mkdir($dir, 0774, true);
		} else {
			@chmod($dir, 0774);
		}

		if (is_readable($this->kbartLocalPath)) {
			touch($this->kbartLocalPath);
			return true;
		}

		$success = rename($tmpPath, $this->kbartLocalPath);
		if ($success) {
			@chmod($this->kbartLocalPath, 0774);
		} else {
			$this->addError('kbartfile', "Erreur en enregistrant le fichier KBART. Disque saturé ?");
		}
		return $success;
	}

	/**
	 * @param string $directory (opt) If not set, {DATA_DIR}/cache/kbart/
	 * @return string
	 */
	private function buildKbartFilepath(string $tmpPath, string $directory = ""): string
	{
		static $filenames = [];
		$filehash = $this->getKbartFileHash($tmpPath);
		if (!isset($filenames[$filehash])) {
			if ($directory === "") {
				$directory = DATA_DIR . '/cache';
			}
			if (!is_dir("$directory/kbart")) {
				mkdir("$directory/kbart", 0774, true);
			}
			$filenames[$filehash] = realpath("$directory/kbart") . "/$filehash.txt";
		}
		return $filenames[$filehash];
	}
}
