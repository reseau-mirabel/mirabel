<?php

namespace models\forms;

use PolitiqueLog;

class PolitiqueTitresForm extends \CFormModel
{
	/**
	 * @var int[]
	 */
	public $ids = [];

	/**
	 * @var \Politique
	 */
	private $politique;

	public function __construct(\Politique $p, int $userId)
	{
		parent::__construct();
		$this->politique = $p;
		if (!$this->isAllowed($userId)) {
			throw new \Exception("Permission non-accordée sur les politiques de cet éditeur");
		}
		$this->ids = array_map(
			'intval',
			\Yii::app()->db
				->createCommand("SELECT titreId FROM Politique_Titre WHERE politiqueId = :id")
				->queryColumn([':id' => $p->id])
		);
	}

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return [
			['ids', 'ext.validators.ArrayOfIntValidator'],
		];
	}

	public function attributeLabels()
	{
		return ['ids' => "Titres de cet éditeur"];
	}

	public function load($data): void
	{
		$this->ids = [];
		if ($data['ids']) {
			foreach ($data['ids'] as $id) {
				if ($id > 0) {
					$this->ids[] = (int) $id;
				}
			}
		}
	}

	public function getTitres(): array
	{
		$titres = [
			'vivants' => [],
			'morts' => [],
		];
		$rows = \Yii::app()->db
			->createCommand(
				<<<EOSQL
				SELECT t.*, p.id AS politiqueId, p.name AS politiqueName, te.ancien
				FROM Titre_Editeur te
					JOIN Titre t ON t.id = te.titreId
					LEFT JOIN Politique_Titre pt ON pt.titreId = te.titreId
					LEFT JOIN Politique p ON p.id = pt.politiqueId
				WHERE te.editeurId = :id
				ORDER BY t.titre
				EOSQL
			)->queryAll(true, [':id' => $this->politique->editeurId]);
		foreach ($rows as $row) {
			$titre = \Titre::model()->populateRecord($row);
			/** @var \Titre $titre */
			$label = ($titre->dateFin ? '&dagger; ' : '') . $titre->getSelfLink();
			$politiqueId = (int) $row['politiqueId'];
			if ($politiqueId && $politiqueId !== (int) $this->politique->id) {
				$label .= sprintf(' <span class="label">%s</span>', \CHtml::encode($row['politiqueName']));
			}
			if ($titre->obsoletePar > 0 || $titre->dateFin > 0 || $row['ancien']) {
				$titres['morts'][$titre->id] = $label;
			} else {
				$titres['vivants'][$titre->id] = $label;
			}
		}
		return $titres;
	}

	public function save(): bool
	{
		$ids = join(',', $this->ids);
		$values = [];
		$now = time();
		if ($this->politique->status === \Politique::STATUS_PUBLISHED || $this->politique->status === \Politique::STATUS_UPDATED) {
			$this->writeLog();
		}
		try {
			\Yii::app()->db
				->createCommand("DELETE FROM Politique_Titre WHERE politiqueId <> :pid AND titreId IN ($ids)")
				->execute([':pid' => $this->politique->id]);
			\Yii::app()->db
				->createCommand("DELETE FROM Politique_Titre WHERE politiqueId = :pid AND titreId NOT IN ($ids)")
				->execute([':pid' => $this->politique->id]);
			foreach ($this->ids as $id) {
				$values[] = "({$this->politique->id}, $id, 0, $now)";
			}
			\Yii::app()->db
				->createCommand("INSERT IGNORE INTO Politique_Titre (politiqueId, titreId, confirmed, createdOn) VALUES " . join(", ", $values))
				->execute();
		} catch (\Throwable $e) {
			\Yii::log($e->getMessage() . $e->getTraceAsString(), 'error');
			return false;
		}
		return true;
	}

	private function isAllowed(int $userId): bool
	{
		if (\Yii::app()->user->checkAccess('admin')) {
			return true;
		}
		$isConfirmed = \Yii::app()->db
			->createCommand("SELECT confirmed FROM Utilisateur_Editeur WHERE utilisateurId = :uid AND editeurId = :eid")
			->queryScalar([':uid' => $userId, ':eid' => $this->politique->editeurId]);
		return (bool) $isConfirmed;
	}

	private function writeLog(): void
	{
		$ids = join(',', $this->ids);
		$now = time();

		// Relations removed from another Politique
		$oldRelations = \Yii::app()->db
			->createCommand(
				<<<EOSQL
				SELECT pt.titreId, pt.politiqueId, p.editeurId
				FROM Politique_Titre pt
				  JOIN Politique p ON pt.politiqueId = p.id
				WHERE politiqueId <> :pid AND pt.titreId IN ($ids)
				EOSQL
			)
			->queryAll(true, [':pid' => $this->politique->id]);
		foreach ($oldRelations as $row) {
			// The policy on this journal has changed, so we log the removal of the old relation.
			$log = new PolitiqueLog();
			$log->action = PolitiqueLog::ACTION_DELETE;
			$log->actionTime = $now;
			$log->politiqueId = $row['politiqueId'];
			$log->editeurId = $row['editeurId'];
			$log->titreId = $row['titreId'];
			$log->save(false);
		}

		// Relations added to this Politique
		$existingIds = \Yii::app()->db
			->createCommand(
				<<<EOSQL
				SELECT pt.titreId
				FROM Politique_Titre pt
				  JOIN Politique p ON pt.politiqueId = p.id
				WHERE politiqueId = :pid AND pt.titreId IN ($ids)
				EOSQL
			)
			->queryColumn([':pid' => $this->politique->id]);
		foreach ($this->ids as $titreId) {
			if (in_array($titreId, $existingIds)) {
				continue;
			}
			// The policy on this journal has changed, so we log the addition.
			$log = new PolitiqueLog();
			$log->action = ($this->politique->status === \Politique::STATUS_PUBLISHED ? PolitiqueLog::ACTION_PUBLISH : PolitiqueLog::ACTION_UPDATE);
			$log->actionTime = $now;
			$log->politiqueId = $this->politique->id;
			$log->editeurId = $this->politique->editeurId;
			$log->titreId = $titreId;
			$log->save(false);
		}
	}
}
