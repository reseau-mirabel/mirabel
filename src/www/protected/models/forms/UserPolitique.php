<?php

namespace models\forms;

use Config;
use Utilisateur;
use Yii;
use components\email\Mailer;

class UserPolitique extends \CFormModel
{
	use \models\traits\AntispamSum;

	public $email;

	public $nom;

	public $prenom;

	public $editeurIds = [0, 0, 0];

	public $roles = [];

	public $message = '';

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		$rules = [
			['email, nom, prenom', 'required'],
			['email', 'email'],
			['email, nom, prenom', 'length', 'max' => 100],
			['message', 'length', 'max' => 1000],
			['editeurIds', 'ext.validators.ArrayOfIntValidator', 'allowEmpty' => false],
			['editeurIds', 'validateEditeurs'],
			['roles', 'safe'],
			['email', 'validateEmailUnicity'],
		];
		if (!defined('YII_ENV') || YII_ENV !== 'test') {
			array_push(
				$rules,
				['spam1, spam2', 'length', 'max' => 255],
				['spam2', 'validateAntispam'],
			);
		}
		return $rules;
	}

	public function validateEditeurs(): void
	{
		if ($this->editeurIds && !$this->hasErrors('editeurIds')) {
			$this->editeurIds = array_unique(array_filter($this->editeurIds));
			if (!$this->editeurIds) {
				$this->addError('editeurIds', "Au moins un éditeur doit être choisi.");
				return;
			}
			if (count($this->editeurIds) > 3) {
				$this->addError('editeurIds', "Un maximum de 3 éditeurs est autorisé.");
				return;
			}
			$ids = join(',', array_map('intval', $this->editeurIds));
			$count = (int) Yii::app()->db
				->createCommand("SELECT count(*) FROM Editeur WHERE id IN ($ids)")
				->queryScalar();
			if ($count !== count($this->editeurIds)) {
				$this->addError('editeurIds', "Certains de ces éditeurs ne sont pas valides.");
			}
		}
	}

	public function validateEmailUnicity(): void
	{
		if (!$this->email) {
			return;
		}
		$exists = \Yii::app()->db
			->createCommand("SELECT 1 FROM Utilisateur WHERE email = :e LIMIT 1")
			->queryScalar([':e' => $this->email]);
		if ($exists) {
			$this->addError(
				'email',
				"Un compte existe déjà avec cette adresse électronique,"
				. " vous pouvez <a href=\"/site/login\">vous connecter directement</a>."
				. " Si vous avez perdu votre mot de passe, vous pouvez en <a href=\"/utilisateur/reset-password\">demander un nouveau</a>."
			);
		}
	}

	public function afterValidate()
	{
		parent::afterValidate();
		if ($this->email && !$this->hasErrors('email')) {
			$exists = Yii::app()->db
				->createCommand("SELECT 1 FROM Utilisateur WHERE email = :e1 OR login = :e2")
				->queryScalar([':e1' => $this->email, ':e2' => $this->email]);
			if ($exists) {
				$this->addError('email', "Cette adresse électronique est déjà attribuée.");
			}
		}
	}

	public function attributeLabels()
	{
		return [
			'email' => 'Adresse électronique',
			'nom' => 'Nom',
			'prenom' => 'Prénom',
			'editeurIds' => "Éditeur",
			'message' => "Commentaire",
			'spam2' => $this->getRandomAntispam(),
		];
	}

	public function getEditeur(int $rank): string
	{
		if (empty($this->editeurIds[$rank])) {
			return '';
		}
		return Yii::app()->db
			->createCommand("SELECT nom FROM Editeur WHERE id = :eid")
			->queryScalar([':eid' => (int) $this->editeurIds[$rank]]);
	}

	public function save(): bool
	{
		if (!$this->validate()) {
			return false;
		}
		$u = new Utilisateur();
		$u->setAttributes($this->getAttributes(), false);
		$u->actif = true;
		$u->authmethod = Utilisateur::AUTHMETHOD_PASSWORD;
		$u->listeDiffusion = false;
		$u->listeLyonnais = false;
		$u->listeFranciliens = false;
		$u->login = $u->email;
		$password = Utilisateur::generateRandomPassword();
		$u->storePassword($password);
		$u->message = $this->message ?? "";
		if (!$u->save(false)) {
			$this->addError('nom', json_encode($u->getErrors()));
			return false;
		}
		$transaction = Yii::app()->db->beginTransaction();
		Yii::app()->db->createCommand("DELETE FROM Utilisateur_Editeur WHERE utilisateurId = {$u->id}")->execute();
		$insert = Yii::app()->db->createCommand(
			"INSERT INTO Utilisateur_Editeur (utilisateurId, editeurId, role, confirmed) VALUES ({$u->id}, :eid, :role, 0)"
		);
		foreach ($this->editeurIds as $eid) {
			$role = $this->roles[$eid] ?? \Politique::ROLE_OWNER;
			if ($role !== \Politique::ROLE_OWNER && $role !== \Politique::ROLE_GUEST) {
				$role = \Politique::ROLE_OWNER;
			}
			if (!$insert->execute([':eid' => $eid, ':role' => $role])) {
				$transaction->rollback();
				return false;
			}
		}
		$transaction->commit();

		if (\Yii::app()->params->itemAt('auto-inscription-affiche-mdp')) {
			\Yii::app()->user->setFlash('info', nl2br($this->getEmailBody($u, $password)));
			return true;
		}
		if (!$this->emailPassword($u, $password)) {
			$u->delete();
			$this->addError('nom', "Erreur lors de l'envoi du courriel contenant le mot de passe. Réessayez plus tard.");
			return false;
		}
		return true;
	}

	private function emailPassword(Utilisateur $u, string $password): bool
	{
		$message = Mailer::newMail()
			->subject(Config::read('password.politique.mailOnCreate.subject') ?: "Bienvenue sur Mir@bel")
			->from(Config::read('email.from'))
			->to($u->email)
			->text($this->getEmailBody($u, $password));
		$replyTo = Config::read('email.replyTo');
		if ($replyTo) {
			$message->replyTo($replyTo);
		}
		return Mailer::sendMail($message);
	}

	private function getEmailBody(Utilisateur $u, string $password): string
	{
		return str_replace(
			['%LOGIN%', '%NOMCOMPLET%', '%PASSWORD%', '%URL%'],
			[$u->login, $u->nomComplet ?: $u->nom, $password, Yii::app()->getBaseUrl(true)],
			Config::read('password.politique.mailOnCreate.body') ?? ''
		);
	}
}
