<?php

namespace models\forms;

use Partenaire;

class CustomizePartenaireForm extends CustomizeLinksForm
{
	public readonly ?int $editeurId;

	/**
	 * @var string|bool Save custom links (from parent class) in the profile?
	 */
	public $customLinks;

	public $fusionAcces;

	public string $ipAutorisees;

	public string $phrasePerso;

	public string $possessionUrl;

	public string $proxyUrl;

	public string $rcr;

	public $rcrHorsPossession;

	public $rcrPublic;

	public function __construct(Partenaire $partenaire)
	{
		parent::__construct('update');
		$this->editeurId = $partenaire->editeurId > 0 ? (int) $partenaire->editeurId : null;
		$this->fusionAcces = (bool) $partenaire->fusionAcces;
		$this->ipAutorisees = (string) $partenaire->ipAutorisees;
		$this->phrasePerso = (string) $partenaire->phrasePerso;
		$this->possessionUrl = (string) $partenaire->possessionUrl;
		$this->proxyUrl = (string) $partenaire->proxyUrl;
		$this->rcr = implode(', ', $partenaire->getRcrList());
		$this->rcrHorsPossession = (bool) $partenaire->rcrHorsPossession;
		$this->rcrPublic = (bool) $partenaire->rcrPublic;
		if ($partenaire->persoRecherche === null) {
			$this->customLinks = false;
			$this->selection = \Sourcelien::DEFAULT_SEARCH_DISPLAY;
		} else {
			$this->customLinks = true;
			$this->selection = json_decode($partenaire->persoRecherche, true, 2);
		}

	}

	public function rules()
	{
		return array_merge(
			parent::rules(),
			[
				['customLinks, fusionAcces, rcrPublic, rcrHorsPossession', 'boolean'],
				['ipAutorisees, phrasePerso', 'length', 'max' => 255],
				['possessionUrl, proxyUrl', 'url'],
				['possessionUrl', 'validateUrlPossession'],
				['proxyUrl', 'validateUrlProxy'],
				['rcr', 'validateRcr'],
			]
		);
	}

	public function attributeLabels(): array
	{
		return parent::attributeLabels() + [
			'customLinks' => "Activer la personnalisation des liens",
			'fusionAcces' => 'Fusion des accès',
			'ipAutorisees' => 'IP locales',
			'phrasePerso' => "Phrase de personnalisation",
			'possessionUrl' => 'URL des possessions',
			'proxyUrl' => 'URL du proxy',
			'rcr' => "RCR",
			'rcrPublic' => "Affichage public des données liées au RCR",
			'rcrHorsPossession' => "Affichage des données liées au RCR pour les revues non possédées",
		];
	}

	public function save(Partenaire $partenaire): bool
	{
		$partenaire->fusionAcces = (bool) $this->fusionAcces;
		$partenaire->ipAutorisees = trim((string) $this->ipAutorisees);
		$partenaire->phrasePerso = (string) $this->phrasePerso;
		$partenaire->possessionUrl = (string) $this->possessionUrl;
		$partenaire->proxyUrl = (string) $this->proxyUrl;
		$partenaire->rcr = (string) $this->rcr;
		$partenaire->rcrHorsPossession = (bool) $this->rcrHorsPossession;
		$partenaire->rcrPublic = (bool) $this->rcrPublic;
		if ($this->customLinks) {
			$partenaire->persoRecherche = json_encode($this->selection, JSON_UNESCAPED_UNICODE);
		} else {
			$partenaire->persoRecherche = '';
		}
		return $partenaire->save(false);
	}

	/**
	 * Validates the attribute "possessionUrl".
	 */
	public function validateUrlPossession(string $attr): void
	{
		if (!$this->hasErrors() && $this->{$attr} && strpos($this->{$attr}, Partenaire::PATTERN_IDLOCAL) === false) {
			$this->addError(
				$attr,
				"L'URL doit contenir le texte \"" . Partenaire::PATTERN_IDLOCAL . "\" que Mir@bel remplacera ensuite par l'identifiant local de chaque titre possédé."
			);
		}
	}

	/**
	 * Validates the attribute "proxyUrl".
	 */
	public function validateUrlProxy(string $attr): void
	{
		if (!$this->hasErrors() && $this->{$attr} && strpos($this->{$attr}, \Proxy::PATTERN_EZ) === false) {
			$this->addError(
				$attr,
				sprintf("L'URL doit contenir %s ou %s qui sera remplacé à chaque usage du proxy.", \Proxy::PATTERN_EZ, \Proxy::PATTERN_GP)
			);
		}
	}

	public function validateRcr(string $attr): void
	{
		if (!preg_match('/^(\d{9}(\s*,\s*\d{9})*)?\s*$/', $this->{$attr})) {
			$this->addError($attr, 'Les RCR doivent être séparé par des "," : 123456789, 123456789,... ');
		}
	}

	public function validateSelection(string $attrName): void
	{
		if ($this->customLinks) {
			parent::validateSelection($attrName);
		}
	}
}
