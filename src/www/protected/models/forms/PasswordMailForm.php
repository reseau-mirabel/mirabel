<?php

namespace models\forms;

use Utilisateur;
use Yii;

class PasswordMailForm extends \CFormModel
{
	public $subject;

	public $body;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return [
			['subject, body', 'required'],
			['subject', 'length', 'max' => 50],
			['body', 'length', 'max' => 4096],
			['body', 'containsPassword'],
		];
	}

	/**
	 * Add an error to the instance if its $attribute does not contain a %PASSWORD% tag.
	 *
	 * @param string $attribute
	 */
	public function containsPassword(string $attribute): void
	{
		if (!$this->hasErrors()) {
			if (strpos($this->{$attribute}, '%PASSWORD%') === false) {
				$this->addError('body', 'Le texte doit contenir "%PASSWORD%" qui sera remplacé par le mot de passe généré.');
			}
		}
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return [
			'subject' => 'Sujet',
			'body' => 'Contenu',
		];
	}

	public function fillBody(Utilisateur $user, string $clearPassword): string
	{
		return str_replace(
			['%LOGIN%', '%PASSWORD%', '%URL%'],
			[$user->login, $clearPassword, Yii::app()->getBaseUrl(true)],
			(string) $this->body
		);
	}
}
