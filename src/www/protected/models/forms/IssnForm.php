<?php

use models\sudoc\ApiClient;
use models\validators\IssnValidator;

class IssnForm extends CModel
{
	public $issn = '';

	public $revueId;

	/**
	 * @var string[]
	 */
	private $issns = [];

	/**
	 * @var \models\sudoc\Notice[]
	 */
	private $sudocNotices = [];

	private ApiClient $api;

	public function __construct(?ApiClient $api = null)
	{
		$this->api = $api ?? new ApiClient();
	}

	public function rules()
	{
		return [
			['issn', 'length', 'max' => '80'],
			['revueId', 'numerical', 'integerOnly' => true, 'allowEmpty' => true],
		];
	}

	public function afterValidate()
	{
		if ($this->issn) {
			$matches = [];
			if (preg_match_all('/\b(\d{4}-\d{3}[\dxX])\b/', $this->issn, $matches)) {
				$this->issns = $matches[1];
				$validator = new IssnValidator();
				foreach ($this->issns as $issn) {
					if (!$validator->validateString($issn)) {
						$this->addError('issn', sprintf("%s n'est pas au format ISSN.", htmlspecialchars($issn)));
					}
				}
				$this->issn = join(" ", $this->issns);
			}
		}
		if ($this->issns && !$this->hasErrors()) {
			foreach ($this->issns as $issn) {
				$titre = Titre::model()->findBySql(
					"SELECT t.* FROM Titre t JOIN Issn i ON t.id = i.titreId WHERE i.issn = :issn",
					[':issn' => $issn]
				);
				if ($titre) {
					$this->addError('issn', "L'ISSN '$issn' est déjà attribué au titre « {$titre->titre} » dans Mir@bel.");
				}
			}
		}
		if (!$this->hasErrors()) {
			$this->sudocNotices = [];
			//$lastIssnl = 'start';
			try {
				foreach ($this->issns as $issn) {
					$notice = $this->api->getNoticeByIssn($issn);
					/*
					if ($lastIssnl !== 'start' && $lastIssnl !== $notice->issnl) {
						$this->addError('issn', "Les ISSN-L de ces ISSN ne sont pas identiques : {$lastIssnl} et {$notice->issnl}.");
					}
					$lastIssnl = $notice->issnl;
					 */
					if ($notice !== null) {
						$this->sudocNotices[] = $notice;
					}
				}
			} catch (Exception $e) {
				$this->addError('issn', $e->getMessage());
			}
			// Update notices with data fetched from ISSN portal.
			try {
				$updater = new \processes\issn\IssnOrgPortal(['issnl', 'name', 'pays', 'support']);
				$updater->updateRecords($this->sudocNotices);
			} catch (\Throwable $_) {
				// NOP
			}
		}
		return parent::afterValidate();
	}

	public function attributeNames()
	{
		return ['issn'];
	}

	public function attributeLabels()
	{
		return ['issn' => 'ISSN'];
	}

	/**
	 * @return array
	 */
	public function getSudocNotices()
	{
		return array_filter($this->sudocNotices);
	}
}
