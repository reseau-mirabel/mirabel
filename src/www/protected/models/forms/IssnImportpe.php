<?php

namespace models\forms;

class IssnImportpe extends \CFormModel
{
	public $simulation = true;

	/**
	 * @var \CUploadedFile
	 */
	public $csv;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return [
			['simulation', 'boolean'],
			['csv', 'required'],
			['csv', 'file', 'types' => 'csv'],
		];
	}

	/**
	 * Declares customized attribute labels.
	 */
	public function attributeLabels()
	{
		return [
			'csv' => 'Fichier CSV à importer',
		];
	}
}
