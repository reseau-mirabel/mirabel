<?php

namespace models\forms;

use Partenaire;
use Utilisateur;

class UtilisateurMultiForm extends \CFormModel
{
	public $partenaire;

	public $nombre;

	public $loginPrefix;

	public $nomPrefix = "nom";

	public $prenomPrefix = "prénom";

	public $motdepasse;

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['partenaire, nombre, loginPrefix, nomPrefix, prenomPrefix, motdepasse', 'required'],
			['nombre', 'numerical', 'integerOnly' => true, 'min' => 1, 'max' => 99],
			['partenaire', 'length', 'max' => 254],
			['loginPrefix', 'length', 'max' => 48],
			['nomPrefix, prenomPrefix', 'length', 'max' => 98],
			['motdepasse', 'length', 'max' => 50],
		];
	}

	public function attributeLabels()
	{
		return [
			'partenaire' => 'Partenaire',
			'loginPrefix' => 'Préfixe login',
			'nomPrefix' => 'Préfixe nom',
			'prenomPrefix' => 'Préfixe prénom',
			'motdepasse' => 'Mot de passe',
		];
	}

	/**
	 * @return Utilisateur[]
	 */
	public function save(): array
	{
		$p = new Partenaire();
		$p->nom = $this->partenaire;
		$p->prefixe = '';
		$p->sigle = '';
		$p->description = 'Créé à des fins de test ou de démonstration.';
		$p->save();

		$created = [];
		for ($i = 1; $i <= $this->nombre; $i++) {
			$u = $this->getUtilisateur($i, $p->id);
			if ($u->save()) {
				$created[] = $u;
			}
		}
		return $created;
	}

	private function getUtilisateur(int $increment, int $partenaireId): Utilisateur
	{
		$u = new Utilisateur();
		$suffix = sprintf("%02d", $increment);
		$u->nom = $this->nomPrefix . $suffix;
		$u->prenom = $this->prenomPrefix . $suffix;
		$u->login = $this->loginPrefix . $suffix;
		$u->storePassword($this->motdepasse);
		$u->partenaireId = $partenaireId;
		$u->permPartenaire = true;
		return $u;
	}
}
