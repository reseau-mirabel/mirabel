<?php

namespace models\forms;

class CustomizeLinksForm extends \CFormModel
{
	/**
	 * @var string[] Liste de Sourcelien.nomcourt
	 */
	public $selection = [];

	public function rules()
	{
		return [
			['selection', 'validateSelection'],
		];
	}

	public function validateSelection(string $attrName): void
	{
		if (empty($this->selection)) {
			$this->selection = [];
			return;
		}
		if (!is_array($this->selection)) {
			$this->addError($attrName, "Liste attendue, texte reçu");
			return;
		}
		$filtered = array_intersect(
			array_keys($this->listCandidates()),
			array_filter($this->selection)
		);
		if (count($filtered) !== count($this->selection)) {
			$this->addError($attrName, "Certains noms de liens ne sont pas parmi les sources de liens autorisées." . join(",", $this->selection));
		}
	}

	public function attributeLabels(): array
	{
		return [
			'selection' => "Liens affichés dans les résultats de recherche",
		];
	}

	/**
	 * Return the list of link source candidates as an assoc array with key=Identifier and value=HtmlDisplay.
	 *
	 * @return array<string, string>
	 */
	public function listCandidates(): array
	{
		$cmd = \Yii::app()->db
			->createCommand("SELECT nomcourt, nom, nomlong FROM Sourcelien WHERE import > 0 ORDER BY nom")
			->queryAll(false);
		$candidates = [];
		foreach ($cmd as [$nomcourt, $nom, $nomlong]) {
			$candidates[$nomcourt] = \CHtml::tag('span', ['title' => $nomlong], \CHtml::encode($nom));
		}
		return $candidates;
	}
}
