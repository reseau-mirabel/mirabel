<?php

/**
 * Trait that add honey pot fields to a form model.
 */
trait Honeypot
{
	public $email; // fake

	public $subject = "Contact"; // fake

	public function validateFakeEmail($attrName)
	{
		if ($this->{$attrName}) {
			$this->addError('email', "Filtre anti-spam. Rechargez la page à vide et remplissez le formulaire sans la complétion du navigateur.");
		}
	}

	public function validateFakeSubject($attrName)
	{
		if ($this->{$attrName} !== self::FAKE_DATA) {
			$this->addError('email', "Filtre anti-spam. Rechargez la page à vide et remplissez le formulaire sans la complétion du navigateur.");
		}
	}
}
