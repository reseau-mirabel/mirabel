<?php

use components\SqlHelper;

/**
 * String (plain text or HTML) methods of Titre.
 */
trait TitreHtml
{
	/**
	 * Alias nom=titre.
	 */
	public function getNom(): string
	{
		return $this->titre;
	}

	/**
	 * Returns the full title, prefix included, but without its 'sigle'.
	 */
	public function getPrefixedTitle(): string
	{
		return $this->prefixe . $this->titre;
	}

	/**
	 * Returns the full title, prefix included, 'sigle' appended.
	 */
	public function getFullTitle(): string
	{
		return $this->prefixe . $this->titre . ($this->sigle ? ' — ' . $this->sigle : '');
	}

	/**
	 * Returns the full title, prefix included, 'sigle' appended, and periodicity (if defined).
	 */
	public function getFullTitleWithPerio(): string
	{
		$p = $this->getPeriode();
		return $this->getFullTitle() . ($p ? " ($p)" : '');
	}

	/**
	 * Returns a short title ('sigle' if it exists).
	 */
	public function getShortTitle(): string
	{
		if ($this->sigle) {
			return $this->sigle;
		}
		return $this->titre;
	}

	public function getUrlSudoc(): string
	{
		$issn = $this->getPreferedIssn();
		if ($issn && $issn->sudocPpn && !$issn->sudocNoHolding) {
			return sprintf(self::URL_SUDOC, $issn->sudocPpn);
		}
		return '';
	}

	public function getUrlWorldcat(): string
	{
		$issn = $this->getPreferedIssn();
		if ($issn && $issn->worldcatOcn) {
			return sprintf(self::URL_WORLDCAT, $issn->worldcatOcn);
		}
		return '';
	}

	/**
	 * Returns the *public* HTML link to one-self (/revue/X).
	 *
	 * @codeCoverageIgnore
	 * @return string HTML link.
	 */
	public function getSelfLink(): string
	{
		$htmlOptions = ['class' => $this->getSuiviType()];
		if (!$htmlOptions['class']) {
			$htmlOptions = [];
		}
		return CHtml::link(
			\CHtml::encode($this->getFullTitle()),
			$this->getRevueUrl(),
			$htmlOptions
		);
	}

	/**
	 * Returns the *private* URL, as a route array.
	 */
	public function getSelfUrl(): array
	{
		return [
			'/titre/view',
			'id' => $this->id,
		];
	}

	public function getSuiviType(): string
	{
		static $suiviOther = null;
		if (Yii::app()->user->hasState('suivi')) {
			if ($suiviOther === null) {
				$suiviOther = SqlHelper::sqlToPairs("SELECT cibleId, partenaireId FROM Suivi WHERE cible = 'Revue'");
			}
			$suivi = Yii::app()->user->getState('suivi');
			if (!empty($suivi['Revue']) && in_array($this->revueId, $suivi['Revue'])) {
				return 'suivi-self';
			}
			if (isset($suiviOther[$this->revueId])) {
				return 'suivi-other';
			}
			return 'suivi-none';
		}
		return '';
	}

	public function getRevueUrl(): string
	{
		return Yii::app()->createUrl('/revue/view', ['id' => $this->revueId, 'nom' => Norm::urlParam($this->getFullTitle())]);
	}

	/**
	 * Returns a date interval built from dateDebut, dateFin.
	 *
	 * @return string HTML text.
	 */
	public function getPeriode(): string
	{
		$periode = '';
		if ($this->dateDebut || $this->dateFin) {
			$formatter = Yii::app()->format;
			if ($this->dateDebut) {
				$periode = $formatter->formatStrDate($this->dateDebut);
			} else {
				$periode = "…";
			}
			$periode .= " – ";
			if ($this->dateFin) {
				$periode .= $formatter->formatStrDate($this->dateFin);
			} else {
				$periode .= "…";
			}
		}
		return $periode;
	}

	/**
	 * @param string $text (opt) Link texte
	 * @return string HTML
	 */
	public function getLinkWorldcat(string $text = ''): string
	{
		if (!$text) {
			$text = CHtml::image(Yii::app()->baseUrl . '/images/icons/worldcat.png', 'WorldCat') . " Worldcat";
		}
		$url = $this->getUrlWorldcat();
		if (!$url) {
			return '';
		}
		return CHtml::link(
			$text,
			$url,
			[
				'class' => 'nb worldcat',
				'title' => 'WorldCat : Catalogue mondial - Online Computer Library Center - OCLC',
			]
		);
	}
}
