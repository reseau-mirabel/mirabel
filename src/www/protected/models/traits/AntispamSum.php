<?php

namespace models\traits;

/**
 * 1. `use` this trait in a class
 * 2. Add a rule: ['spam1, spam2', 'length', 'max' => 255]
 * 3. Add a line in attributeLabels(): 'spam2' => $this->getRandomAntispam(),
 * 4. Add a rule: ['spam2', 'validateAntispam']
 */
trait AntispamSum
{
	/**
	 * @var string
	 */
	public $spam1;

	/**
	 * @var string
	 */
	public $spam2;

	public function validateAntispam()
	{
		if (!$this->spam2 || $this->computeSpamKey($this->spam2) !== $this->spam1) {
			$this->addError('spam2', "Réponse anti-spam incorrecte.");
		}
	}

	protected function computeSpamKey(string $str): string
	{
		return md5("$str verbatim &tchaa%");
	}

	protected function getRandomAntispam($new = false): string
	{
		static $answer = null;
		if (isset($answer) && !$new) {
			return $answer;
		}
		$a = rand(10, 99);
		$b = rand(1, min([$a, 10]));
		if (rand(1, 2) == 1) {
			$this->spam1 = $this->computeSpamKey((string) ($a + $b));
			$answer = "Combien font <code>$a + $b</code> ?";
		} else {
			$this->spam1 = $this->computeSpamKey((string) ($a - $b));
			$answer =  "Combien font <code>$a - $b</code> ?";
		}
		return $answer;
	}
}
