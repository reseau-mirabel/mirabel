<?php

namespace models\traits;

trait UserCanConfirm
{
	/**
	 * @var ?bool
	 */
	public $confirm;

	public function getConfirm(): bool
	{
		return (bool) $this->confirm;
	}

	public function setConfirm(bool $confirm): void
	{
		$this->confirm = $confirm;
	}
}
