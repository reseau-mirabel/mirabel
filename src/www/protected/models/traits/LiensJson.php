<?php

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
trait LiensJson
{
	protected $cache = [];

	/**
	 * @return \Liens
	 */
	public function getLiens(): \Liens
	{
		if (!isset($this->cache['liens'])) {
			$liens = new Liens();
			$liens->setContent($this->liensJson);
			$this->cache['liens'] = $liens;
		}
		return $this->cache['liens'];
	}

	/**
	 * @param array|Liens $data
	 */
	public function setLiens($data)
	{
		if ($data instanceof Liens) {
			$liens = $data;
		} else {
			$liens = new Liens();
			$liens->setContent($data);
		}
		$this->liensJson = json_encode($liens, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
		return $this;
	}

	public function getLiensInternes(): ?Liens
	{
		if (!array_key_exists('liensInternes', $this->cache)) {
			$internal = [];
			if ($this->liensJson) {
				$internal = array_filter(
					$this->getLiens()->getContent(),
					function ($x) {
						return $x->isInternal();
					}
				);
			}
			$this->cache['liensInternes'] = (new Liens)->setContent($internal);
		}
		return $this->cache['liensInternes'];
	}

	public function getLiensEditoriaux(): ?Liens
	{
		if (!array_key_exists('liensEditoriaux', $this->cache)) {
			$editorial = [];
			if ($this->liensJson) {
				$editorial = array_filter(
					$this->getLiens()->getContent(),
					function ($x) {
						return $x->isEditorial();
					}
				);
			}
			if ($editorial) {
				$this->cache['liensEditoriaux'] = (new Liens)->setContent($editorial);
				$this->cache['liensEditoriaux']->sort();
			} else {
				$this->cache['liensEditoriaux'] = null;
			}
		}
		return $this->cache['liensEditoriaux'];
	}

	public function getLiensNormaux(): ?Liens
	{
		if (!array_key_exists('liensNormaux', $this->cache)) {
			$external = [];
			if ($this->liensJson) {
				$external = array_filter(
					$this->getLiens()->getContent(),
					function ($x) {
						return !($x->isInternal() || $x->isEditorial());
					}
				);
			}
			$this->cache['liensNormaux'] = (new Liens)->setContent($external);
			$this->cache['liensNormaux']->sort();
		}
		return $this->cache['liensNormaux'];
	}

	public function getLiensExternes(): ?Liens
	{
		if (!array_key_exists('liensExternes', $this->cache)) {
			$external = [];
			if ($this->liensJson) {
				$external = array_filter(
					$this->getLiens()->getContent(),
					function ($x) {
						return !$x->isInternal();
					}
				);
				usort($external, fn (\Lien $a, \Lien $b) => strcmp($a->src, $b->src));
			}
			$this->cache['liensExternes'] = (new Liens)->setContent($external);
		}
		return $this->cache['liensExternes'];
	}

	/**
	 * @return string HTML
	 */
	public function getDetailedLinksOther(): string
	{
		if (empty($this->liensJson)) {
			return '';
		}
		$output = '<ol>';
		foreach ($this->getLiens() as $link) {
			if ($link->url[0] === '/') {
				// Pas de liens internes
				continue;
			}
			$output .= '<li><strong>' . CHtml::encode($link->src) . '</strong> : '
				. '<span>' . CHtml::link(CHtml::encode($link->url), $link->url) . '</span></li>';
		}
		$output .= '</ol>';
		return $output;
	}
}
