<?php

namespace models\traits;

trait VerbosityEcho
{
	/**
	 * conditional echo, according to verbosity level
	 */
	protected function vecho(int $verbmin, string $text): void
	{
		if ($this->verbose >= $verbmin) {
			echo $text;
		}
	}
}
