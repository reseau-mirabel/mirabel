<?php

use components\SqlHelper;
use models\import\ImportType;
use models\import\Normalize;

require_once __DIR__ . '/Import.php';
require_once __DIR__ . '/ReaderCsv.php';

/**
 * Import from Cairn (custom format).
 */
class ImportCairn extends Import
{
	protected $encoding = 'UTF-8';

	/**
	 * Constructor.
	 */
	public function __construct($parameters = [])
	{
		$this->name = 'Cairn';
		if (empty($parameters['url'])) {
			$parameters['url'] = 'https://www.cairn.info/static/includes/CairnCsv/revues_cairn_csv_2015.php';
		}
		foreach (['url', 'verbose', 'simulation'] as $k) {
			if (isset($parameters[$k])) {
				$this->{$k} = $parameters[$k];
			}
		}
		$this->header = explode(
			';',
			"REVUE;EDITEUR;ISSN;ISSN VERSION EN LIGNE;PERIODICITE;URL DE LA REVUE;PREMIER NUMERO DISPO;DERNIER NUMERO DISPO;BARRIERE MOBILE"
		);
		$this->reader = new import\ReaderCsv(';', $this->header, $this->encoding);
		$this->reader->readAllColumns = true;
		$this->init();
	}

	/**
	 * Returns a integer that is unique to this import type.
	 *
	 * @return int
	 */
	public function getId()
	{
		return ImportType::getSourceId('Cairn');
	}

	/**
	 * Returns the title attributes: titre, issn... and some extra: key, issne...
	 *
	 * @param array $line Splitted CSV line.
	 * @return array Assoc array with keys: "titre", "issn", "issne"...
	 */
	public function extractTitleData($line)
	{
		if (preg_match('#https?://www\.cairn\.info/(.+)\.htm#', $this->getCol($line, 'URL DE LA REVUE'), $m)) {
			$idInterne = $m[1];
		} else {
			$idInterne = null;
		}
		return Normalize::cleanTitleData(
			[
				'titre' => $this->getCol($line, $this->header[0]),
				'editeur' => $this->getCol($line, 'EDITEUR'),
				'issn' => $this->getCol($line, 'ISSN'),
				'issne' => $this->getCol($line, 'ISSN VERSION EN LIGNE'),
				'idInterne' => $idInterne,
				'urlLocal' => $this->getCol($line, 'URL DE LA REVUE'),
			]
		);
	}

	/**
	 * Returns a list of Collection objects.
	 *
	 * @param array $line Splitted CSV line.
	 * @return array
	 */
	public function extractCollections($line)
	{
		$collections = [];
		foreach ($this->collections as $colnum => $collectionId) {
			if (isset($line[$colnum]) && $line[$colnum] === 'X') {
				$collections[] = $collectionId;
			}
		}
		return $collections;
	}

	/**
	 * Returns a list of Service objects.
	 *
	 * @param array $line Splitted CSV line.
	 * @return array
	 */
	public function extractServicesData($line)
	{
		$begin = $this->parseVolNum($this->getCol($line, 'PREMIER NUMERO DISPO'));
		$end = $this->parseVolNum($this->getCol($line, 'DERNIER NUMERO DISPO'));
		$barrMob = strtolower($this->getCol($line, 'BARRIERE MOBILE'));
		if ($barrMob === 'accès libre intégral') {
			$services = [
				[
					'type' => 'Intégral',
					'acces' => 'Libre',
					'lacunaire' => 0,
					'selection' => 0,
					'derNumUrl' => '',
					'url' => $this->getCol($line, 'URL DE LA REVUE'),
					'volDebut' => Normalize::toNum($begin['vol']),
					'noDebut' => Normalize::toNum($begin['num']),
					'numeroDebut' => Normalize::buildVolNum($begin['vol'], $begin['num'], $begin['type']),
					'volFin' => Normalize::toNum($end['vol']),
					'noFin' => Normalize::toNum($end['num']),
					'numeroFin' => Normalize::buildVolNum($end['vol'], $end['num'], $end['type']),
					'dateBarrDebut' => $begin['date'],
					'dateBarrFin' => $end['date'],
					'dateBarrInfo' => '',
					'alerteRssUrl' => "https://shs.cairn.info/rss/revue/" . $this->getCol($line, 'ID'),
				],
			];
		} elseif (preg_match('/^\s*(\d+) +ans?/', $barrMob, $m)) {
			$year = (int) $m[1];
			$date = (string) ((int) date('Y', $this->time) - $year);
			$dateInfo = "Barrière mobile de $m[1] an" . ($year > 1 ? 's' : '');
			$services = [
				[
					'type' => 'Intégral',
					'acces' => 'Libre',
					'lacunaire' => 0,
					'selection' => 0,
					'derNumUrl' => '',
					'url' => $this->getCol($line, 'URL DE LA REVUE'),
					'volDebut' => Normalize::toNum($begin['vol']),
					'noDebut' => Normalize::toNum($begin['num']),
					'numeroDebut' => Normalize::buildVolNum($begin['vol'], $begin['num'], $begin['type']),
					'volFin' => null,
					'noFin' => null,
					'numeroFin' => '',
					'dateBarrDebut' => $begin['date'],
					'dateBarrFin' => min([$date - 1, $end['date']]),
					'dateBarrInfo' => $dateInfo,
					'alerteRssUrl' => "https://shs.cairn.info/rss/revue/" . $this->getCol($line, 'ID'),
				],
				[
					'type' => 'Intégral',
					'acces' => 'Restreint',
					'lacunaire' => 0,
					'selection' => 0,
					'derNumUrl' => '',
					'url' => $this->getCol($line, 'URL DE LA REVUE'),
					'volDebut' => null,
					'noDebut' => null,
					'numeroDebut' => '',
					'volFin' => Normalize::toNum($end['vol']),
					'noFin' => Normalize::toNum($end['num']),
					'numeroFin' => Normalize::buildVolNum($end['vol'], $end['num'], $end['type']),
					'dateBarrDebut' => max([$date, $begin['date']]),
					'dateBarrFin' => $end['date'],
					'dateBarrInfo' => $dateInfo,
					'alerteRssUrl' => "https://shs.cairn.info/rss/revue/" . $this->getCol($line, 'ID'),
				],
			];
		} elseif ($barrMob === 'non' || $barrMob === '') {
			$services = [
				[
					'type' => 'Intégral',
					'acces' => 'Libre',
					'lacunaire' => 0,
					'selection' => 0,
					'derNumUrl' => '',
					'url' => $this->getCol($line, 'URL DE LA REVUE'),
					'volDebut' => Normalize::toNum($begin['vol']),
					'noDebut' => Normalize::toNum($begin['num']),
					'numeroDebut' => Normalize::buildVolNum($begin['vol'], $begin['num'], $begin['type']),
					'volFin' => Normalize::toNum($end['vol']),
					'noFin' => Normalize::toNum($end['num']),
					'numeroFin' => Normalize::buildVolNum($end['vol'], $end['num'], $end['type']),
					'dateBarrDebut' => $begin['date'],
					'dateBarrFin' => $end['date'],
					'dateBarrInfo' => '',
					'alerteRssUrl' => "https://shs.cairn.info/rss/revue/" . $this->getCol($line, 'ID'),
				],
			];
		} else {
			$this->log->addLocal('warning', "Format de date barrière incompris : " . $this->getCol($line, 'BARRIERE MOBILE'));
			return [];
		}
		return Normalize::cleanServicesData($services);
	}

	/**
	 * Initializes the list of collections.
	 *
	 * Ignores the prefix "Revues - ": it will detect the DB Collection.name with or without it.
	 *
	 * @throws Exception
	 */
	protected function initCollections()
	{
		$collections = SqlHelper::sqlToPairs(
			"SELECT identifiant, id FROM Collection WHERE identifiant <> '' AND ressourceId = " . $this->ressource->id
		);
		foreach ($collections as $colName => $cid) {
			$colName = strtolower($colName);
			$colnum = $this->reader->getHeaderRank($colName);
			if ($colnum) {
				$this->collections[$colnum] = (int) $cid;
				continue;
			}

			$shortName = str_replace("revues - ", '', $colName);
			$num = $this->reader->getHeaderRank($shortName);
			if ($num) {
				$this->collections[$num] = (int) $cid;
				continue;
			}
		}
		foreach (array_keys($this->reader->colMap) as $colName) {
			$colName = strtolower($colName);
			echo "COLL $colName\n";
			if (!str_starts_with($colName, "revues - ")) {
				continue;
			}
			$shortName = str_replace("revues - ", '', $colName);
			if (!isset($collections[$colName]) && !isset($collections[$shortName])) {
				$this->log->addGlobal(
					"warning",
					"Erreur, collection inconnue",
					"Attention, la collection « {$shortName} » du CSV (en minuscules) est introuvable dans la base de données."
				);
			}
		}
	}

	/**
	 * Parse a Cairn field and extract: date, vol num.
	 *
	 * @return array  assoc array: vol, num, date (YYYY-MM or YYYY)
	 */
	protected function parseVolNum($text)
	{
		$numero = [];
		$text = trim($text);
		if (preg_match('/^(\d{4})\/(\d+),[ \s]+(Vol\.?|Volume|Tome)[ \s]+(\d+),[ \s]+(?:n°|numéro)[ \s]*(\d+)/i', $text, $m)) {
			$numero = [
				'type'   => $m[3],
				'vol'   => $m[4],
				'num'   => $m[5],
			];
		} elseif (preg_match('/^(\d{4})\/(\d+),[ \s]+(?:n°|numéro)[ \s]*(\d+)/i', $text, $m)) {
			$numero = [
				'type'   => '',
				'vol'   => null,
				'num'   => $m[3],
			];
		} elseif (preg_match('/^(\d{4})\/(\d+),[ \s]+(Vol\.?|Volume|Tome)[ \s]+(\d+|[IVXLCDM]+)/', $text, $m)) {
			$numero = [
				'type'   => $m[3],
				'vol'   => $m[4],
				'num'   => null,
			];
		} elseif (preg_match('/^(\d{4})[^\d]/', $text, $m)) {
			$m[2] = 0;
			$numero = [
				'type'   => '',
				'vol'   => null,
				'num'   => null,
			];
		} else {
			$this->log->addLocal('error', "Format du numéro incompréhensible, champ='$text'");
			return [
				'type'   => '',
				'date' => '',
				'vol'   => null,
				'num'   => null,
			];
		}
		$numero['date'] = $m[1];
		return $numero;
	}

	protected function validateColMap($colMap, $header)
	{
		return true;
	}
}
