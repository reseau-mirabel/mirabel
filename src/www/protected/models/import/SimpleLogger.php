<?php

namespace import;

class SimpleLogger
{
	/**
	 * Add a local log entry.
	 *
	 * @param string $level
	 * @param string $msg
	 */
	public function addLocal($level, $msg)
	{
	}

	/**
	 * Add a global log entry.
	 *
	 * @param string $level
	 * @param string $title
	 * @param string $msg
	 * @param string $url
	 */
	public function addGlobal($level, $title, $msg, $url='')
	{
		\Yii::log($title . " : " . $msg, $level);
	}
}
