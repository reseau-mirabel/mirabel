<?php

namespace import;

/**
 * Ancestor for an import input source.
 */
abstract class Reader
{
	/**
	 * @var ?array Maps a colum name to its position in the CSV.
	 */
	public $colMap = null;

	public bool $ignoreHeaderCase = true;

	/** @var SimpleLogger */
	protected $log;

	protected array $header = [];

	public function setLog(SimpleLogger $log): void
	{
		$this->log = $log;
	}

	public function getHeaderRank(string $name): ?int
	{
		if ($this->ignoreHeaderCase) {
			$name = strtolower($name);
		}
		return ($this->colMap[$name] ?? null);
	}

	/**
	 * @param string[] $line
	 */
	public function getCol(array $line, string $name, string $default = ''): string
	{
		if (!isset($this->colMap[$name])) {
			return $default;
		}
		$position = $this->colMap[$name];
		if (!isset($line[$position])) {
			throw new \Exception("Pas de colonne '$name' ($position) dans les données lues");
		}
		// trim all kinds of spaces
		$val = (string) preg_replace('/^[\pZ\pC]+|[\pZ\pC]+$/u', '', $line[$position]);
		return ($val === '' ? $default : $val);
	}

	public function getHeader(): array
	{
		return $this->header;
	}

	abstract public function readUrl(string $url): bool;

	abstract public function readLines(): iterable;

	abstract public function splitRawLine(string $line): array;

	protected function flattenColumns($cols)
	{
		foreach ($cols as $k => $c) {
			if (is_scalar($c) && strpos((string) $c, '|') !== false) {
				$c = array_filter(explode('|', (string) $c));
			}
			if (is_array($c)) {
				unset($cols[$k]);
				$cols = array_merge($cols, $c);
			}
		}
		return $cols;
	}
}
