<?php

/**
 * This is the model class for table "CategorieAlias_Titre".
 *
 * The followings are the available columns in table 'CategorieAlias_Titre':
 * @property int $categorieAliasId
 * @property int $titreId
 * @property string $hdateModif
 * @property int $modifPar
 */
class CategorieAliasTitre extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'CategorieAlias_Titre';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['categorieAliasId, titreId', 'required'],
			['categorieAliasId, titreId, modifPar', 'numerical', 'integerOnly' => true, 'allowEmpty' => true],
			['hdateModif',
				'default', 'value' => date("Y-m-d H:i:s"),
				'setOnEmpty' => false, 'on' => 'insert, update', ],
			// The following rule is used by search().
			['categorieAliasId, titreId, hdateModif, modifPar', 'safe', 'on' => 'search'],
		];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'alias' => [self::BELONGS_TO, 'CategorieAlias', 'categorieAliasId'],
			'titre' => [self::BELONGS_TO, 'Titre', 'titreId'],
			'modifiePar' => [self::HAS_ONE, 'Utilisateur', 'modifiePar'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'categorieAliasId' => 'Alias',
			'titreId' => 'Titre',
			'hdateModif' => 'Date modif.',
			'modifPar' => 'Modif. par',
		];
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @param int|bool $pageSize Number of items for pagination (false to disable pagination).
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($pageSize = 25)
	{
		$criteria = new CDbCriteria;

		$criteria->compare('categorieAliasId', $this->categorieAliasId);
		$criteria->compare('titreId', $this->titreId);
		$criteria->compare('hdateModif', $this->hdateModif, true);
		$criteria->compare('modifPar', $this->modifPar);

		$sort = new CSort();
		$sort->attributes = ['categorieAliasId', 'titreId', 'hdateModif', 'modifPar'];
		$sort->defaultOrder = 'hdateModif DESC';

		return new CActiveDataProvider(
			$this,
			[
				'criteria' => $criteria,
				'pagination' => ($pageSize ? ['pageSize' => $pageSize] : false),
				'sort' => $sort,
			]
		);
	}
}
