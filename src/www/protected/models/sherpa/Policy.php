<?php

namespace models\sherpa;

/**
 * See publisher_policy in https://v2.sherpa.ac.uk/api/metadata-schema.html
 */
class Policy
{
	/**
	 * @var array{published: Oa[], accepted: Oa[], submitted: Oa[]}
	 */
	public $oa = [
		'published' => [],
		'accepted' => [],
		'submitted' => [],
	];

	/**
	 * @var bool
	 */
	public $openAccess;

	/**
	 * @var string Sherpa URL for this policy
	 */
	public $policyUrl = "";

	/**
	 * @var array "Ressources sur les politiques éditoriales" : { "urls" : [...]}
	 */
	public $resourceUrls = [];

	public function __construct(\stdClass $p)
	{
		$this->openAccess = empty($p->open_access_prohibited) || ($p->open_access_prohibited === "no");
		$this->policyUrl = $p->uri;

		// resourceUrls
		if (!empty($p->urls)) {
			$this->resourceUrls = $p->urls;
		}

		// oa
		if (isset($p->permitted_oa)) {
			foreach ($p->permitted_oa as $poa) {
				if (empty($poa)) {
					continue;
				}
				$oa = new Oa($poa);
				if (!empty($poa->article_version)) {
					foreach ($poa->article_version as $version) {
						$this->oa[$version][] = $oa;
					}
				}
			}
		}
	}

	public function isHybrid(): bool
	{
		foreach ($this->oa['published'] as $oa) {
			if ($oa->additionalOaFee) {
				return true;
			}
		}
		return false;
	}
}
