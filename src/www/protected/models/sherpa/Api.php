<?php

namespace models\sherpa;

use components\Curl;

/**
 * This class models the API of Sherpa for Object Retrieval.
 * See https://v2.sherpa.ac.uk/api/
 */
class Api
{
	public const ENDPOINT = 'https://v2.sherpa.ac.uk/cgi/retrieve_by_id';

	protected Curl $curl;

	protected string $apiKey;

	public function __construct(string $apiKey)
	{
		if (!$apiKey) {
			throw new \Exception("Sherpa API needs a key. Please configure Mir@bel.");
		}
		$this->apiKey = $apiKey;

		$this->initCurl();
	}

	/**
	 * Fetch a Publication by the first matching ISSN.
	 *
	 * The Sherpa responses will be stored into the Sherpa* tables.
	 * May throw a NetworkException (network error) or an Exception (HTTP code <> 200).
	 *
	 * @param int $titreId Mirabel ID
	 * @param string[] $issns
	 * @return ?Item
	 */
	public function fetchPublication(int $titreId, array $issns): ?Item
	{
		foreach ($issns as $issn) {
			$json = $this->fetchPublicationByIssn($issn);
			$response = json_decode($json, false); // into \stdClass
			if (empty($response->items)) {
				continue;
			}
			$publication = $response->items[0];
			$dateModified = $this->saveSherpaPublication($titreId, $publication);
			return new Item($publication, $dateModified);
		}
		return null;
	}

	/**
	 * Return the raw response (JSON encoded).
	 *
	 * May throw a NetworkException (network error) or an Exception (HTTP code <> 200).
	 *
	 * @param string $issn
	 * @return string JSON-encoded
	 */
	public function fetchPublicationByIssn(string $issn): string
	{
		$url = self::ENDPOINT . "?"
			. http_build_query([
				'api-key' => $this->apiKey,
				'item-type' => "publication",
				'format' => 'Json',
				'identifier' => $issn,
			]);
		return $this->fetch($url);
	}

	/**
	 * @return string JSON-encoded
	 */
	public function fetchPublisher(string $sherpaId): string
	{
		$url = self::ENDPOINT . "?"
			. http_build_query([
				'api-key' => $this->apiKey,
				'item-type' => "publisher",
				'format' => 'Json',
				'identifier' => $sherpaId,
			]);
		return $this->fetch($url);
	}

	private function fetch(string $url): string
	{
		$result = $this->curl->get($url)->getContent();
		$code = $this->curl->getHttpCode();
		if ($code < 200 || $code >= 300) {
			throw new \Exception("Erreur HTTP, code $code.", $code);
		}
		return $result;
	}

	private function initCurl(): void
	{
		$this->curl = new Curl();
		$this->curl->setopt(CURLOPT_MAXREDIRS, 1);
		$this->curl->setopt(CURLOPT_TIMEOUT, 3);
		$this->curl->setopt(CURLOPT_COOKIEFILE, '');
		// same connection for multiple queries
		$this->curl->setopt(CURLOPT_FRESH_CONNECT, 0);
		$this->curl->setopt(CURLOPT_FORBID_REUSE, 0);
	}

	private function saveSherpaPublication(int $titreId, \stdClass $publication): int
	{
		$dateModified = '';
		if (!empty($publication->system_metadata->date_modified)) {
			$dateModified = $publication->system_metadata->date_modified;
		} elseif (!empty($publication->system_metadata->date_created)) {
			$dateModified = $publication->system_metadata->date_created;
		}
		$ts = $dateModified ? \DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $dateModified)->getTimestamp() : 0;

		if (isset($publication->publishers)) {
			$ids = [];
			foreach ($publication->publishers as $publisher) {
				$ids[] = $publisher->publisher->id;
			}
			if ($ids) {
				$publisherTs = $this->updatePublishers($ids);
				if ($ts < $publisherTs) {
					$ts = $publisherTs;
				}
			}
		}

		if ($titreId > 0) {
			\Yii::app()->db
				->createCommand(
					"REPLACE INTO SherpaPublication (id, titreId, content, lastUpdate, lastFetch) VALUES (:id, :tid, :json, :u, :now)"
				)->execute([
					':id' => $publication->id,
					':tid' => $titreId,
					':json' => json_encode($publication, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE),
					':u' => $ts,
					':now' => time(),
				]);
		}
		return $ts;
	}

	private function updatePublishers(array $ids): int
	{
		$strIds = join(",", array_map('intval', $ids));
		$row = \Yii::app()->db
			->createCommand(<<<EOSQL
				SELECT
					count(*) AS num,
					MIN(lastFetch) AS lastFetch,
					MAX(lastUpdate) AS lastUpdate
				FROM SherpaPublisher
				WHERE id IN ($strIds)
				EOSQL
			)
			->queryRow();
		if ((int) $row['num'] === count($ids) && $row['lastFetch'] >= time() - 86400) {
			return (int) $row['lastUpdate'];
		}

		$lastUpdate = 0;
		$replace = \Yii::app()->db
			->createCommand(<<<EOSQL
				REPLACE INTO SherpaPublisher (id, content, lastUpdate, lastFetch)
					VALUES (:id, :json, :u, :now)
				EOSQL
			);
		foreach ($ids as $id) {
			$json = $this->fetchPublisher($id);
			if (!$json) {
				continue;
			}
			$response = json_decode($json, false);
			if (empty($response->items)) {
				continue;
			}
			$publisher = $response->items[0];
			$dateCreated = $publisher->system_metadata->date_modified ?? $publisher->system_metadata->date_created;
			$ts =  \DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $dateCreated)->getTimestamp();
			$replace->execute([
				':id' => $id,
				':json' => json_encode($publisher, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE),
				':u' => $ts,
				':now' => time(),
			]);
			if ($lastUpdate < $ts) {
				$lastUpdate = $ts;
			}
		}
		return $lastUpdate;
	}
}
