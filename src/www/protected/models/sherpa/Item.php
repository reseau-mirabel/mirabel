<?php

namespace models\sherpa;

/**
 * Contains a raw item from the response of the Sherpa API (usually the first item).
 *
 * This should match each entry of the JSON returned by the API,
 * e.g. curl "https://v2.sherpa.ac.uk/cgi/retrieve?api-key=$KEY&format=Json&item-type=publication&filter=%5B%5B%22issn%22%2C%22equals%22%2C%22$ISSN%22%5D%5D"
 * (where $KEY and $ISSN must be set).
 * Mirabel will then use this to fill in a sherpa\Publication instance.
 *
 * Cf https://v2.sherpa.ac.uk/api/metadata-schema.html with the blocks
 * "common to all types" and "publication".
 */
class Item
{
	private const IGNORED_ATTRIBUTES = ['tj_status', 'tj_status_phrases'];

	/**
	 * @var int the internal id of the item
	 */
	public $id;

	/**
	 * @var array
	 */
	public $issns;

	/**
	 * @var string yes|no
	 */
	public $listed_in_doaj;

	/**
	 * @var array
	 */
	public $listed_in_doaj_phrases;

	/**
	 * @var string
	 */
	public $notes;

	/**
	 * @var array
	 */
	public $publisher_policy = [];

	/**
	 * @var array
	 */
	public $publishers;

	/**
	 * @var object E.g. {id: 4, publicly_visible: "yes", uri: "http...", publicly_visible_phrases: [...]}
	 */
	public $system_metadata;

	/**
	 * @var array
	 */
	public $title = [];

	/**
	 * @var string
	 */
	public $type;

	/**
	 * @var array
	 */
	public $type_phrases;

	/**
	 * @var string
	 */
	public $url;

	public $lastUpdate = 0;

	public function __construct(\stdClass $raw, int $lastUpdate = 0)
	{
		foreach ((array) $raw as $k => $v) {
			if (!property_exists(self::class, $k)) {
				if (!in_array($k, self::IGNORED_ATTRIBUTES, true)) {
					\Yii::log("SherpaPublication, attribute '$k' is unknown.", \CLogger::LEVEL_WARNING);
				}
				continue;
			}
			$this->{$k} = $v;
		}
		if ($lastUpdate > 0) {
			$this->lastUpdate = $lastUpdate;
		}
	}

	public static function loadFromCache(int $titreId): ?Item
	{
		$row = \Yii::app()->db
			->createCommand("SELECT * FROM SherpaPublication WHERE titreId = $titreId LIMIT 1")
			->queryRow();
		if (!$row) {
			return null;
		}
		return self::loadFromRow($row);
	}

	/**
	 * @param int $titreId
	 * @param string[] $issns
	 * @return Item|null
	 */
	public static function loadFromCacheByIssn(int $titreId, array $issns): ?Item
	{
		foreach ($issns as $issn) {
			$filePath = self::getFilePath((string) $issn);
			if ($filePath && is_file($filePath)) {
				$newFilePath = self::getFilePath((string) $titreId);
				rename($filePath, $newFilePath);
				return self::loadFromFile($newFilePath);
			}
		}
		return null;
	}

	public static function loadFromFile(string $path): ?Item
	{
		$contents = file_get_contents($path);
		if (!$contents) {
			return null;
		}
		$data = json_decode($contents, false);
		if (!$data || empty($data->items)) {
			return null;
		}
		/*
		 * A json file contains the raw response, with usually a single item of publication.
		 */
		return new self($data->items[0]);
	}

	public static function loadFromRow(array $row): ?Item
	{
		$data = json_decode($row['content'], false);
		if (!$data || empty($data->id)) {
			\Yii::log("JSON non valide dans SherpaPublication : {$row['content']}", 'warning', 'sherpa');
			return null;
		}
		/*
		 * The json inside a DB row matches a single Sherpa publication, it's not a list of items.
		 */
		return new self($data, (int) $row['lastUpdate']);
	}

	public function getRomeoUrl(): string
	{
		if ($this->system_metadata->publicly_visible !== 'yes' || empty($this->system_metadata->uri)) {
			return "";
		}
		return str_replace(
			'https://v2.sherpa.ac.uk/id/publication/',
			'https://openpolicyfinder.jisc.ac.uk/id/publication/',
			(string) $this->system_metadata->uri
		);
	}

	public function getPoliciesSummary(): array
	{
		$policies = [
			"submitted" => 0,
			"accepted" => 0,
			"published" => 0,
		];
		if (empty($this->publisher_policy)) {
			return [];
		}
		foreach ($this->publisher_policy as $pp) {
			if (empty($pp->permitted_oa)) {
				continue;
			}
			foreach ($pp->permitted_oa as $oa) {
				if (empty($oa->article_version)) {
					continue;
				}
				foreach ($oa->article_version as $p) {
					$policies[$p]++;
				}
			}
		}
		return $policies;
	}

	public function getPublication(): Publication
	{
		$p = new Publication();
		$p->fill($this);
		return $p;
	}

	private static function getFilePath(string $id): string
	{
		if ($id === '') {
			return "";
		}
		$basePath = \Yii::app()->runtimePath . '/sherpa';
		return sprintf("%s/%s.json", $basePath, $id);
	}
}
