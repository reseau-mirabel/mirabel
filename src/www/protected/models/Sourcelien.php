<?php

/**
 * This is the model class for table "Sourcelien".
 *
 * The followings are the available columns in table 'Sourcelien':
 * @property int $id
 * @property string $nom
 * @property string $nomcourt
 * @property string $nomlong
 * @property string $url Domaine prévu, généralement la terminaison de l'hôte dans les URLs (par ex. "wikipedia.org").
 * @property string $urlRegex Regexp de validation des URLs
 * @property bool $import
 * @property int $nbimport
 * @property int $hdateCrea timestamp
 * @property int $hdateModif timestamp
 *
 * @method self sorted() See scopes().
 */
class Sourcelien extends CActiveRecord
{
	public const CACHE_DURATION = 3600;

	public const DEFAULT_SEARCH_DISPLAY = ['doaj', 'hal', 'openalex'];

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Sourcelien';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['nom', 'required'],
			['nom, nomlong', 'length', 'max' => 255],
			['url', 'length', 'max' => 512],
			['nomcourt', 'length', 'max' => 50],
			['import', 'boolean'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'nom' => 'Nom',
			'nomlong' => 'Nom long',
			'nomcourt' => 'Nom court',
			'url' => "Domaine d'URL",
			'urlRegex' => "Validateur d'URL",
			'import' => 'Données importées',
			'nbimport' => 'Nombre de lignes',
			'hdateCrea' => 'Date de création',
			'hdateModif' => 'Dernière modification',
		];
	}

	/**
	 * Each array key defines a function that can be chained before a find*() method.
	 */
	public function scopes()
	{
		return [
			'sorted' => [
				'order' => 'nom ASC',
			],
		];
	}

	/**
	 * @return bool
	 */
	public function hasLogo()
	{
		$path = dirname(Yii::app()->getBasePath()) . '/images/liens';
		return glob(sprintf('%s/%06d.*', $path, $this->id))
			|| glob(sprintf('%s/%s.*', $path, $this->nomcourt));
	}

	public static function listNames(): array
	{
		$values = Yii::app()->db
			->createCommand(<<<EOSQL
				SELECT
					IF(nomlong != '' AND nomlong != nom, CONCAT(nom, ' — ', nomlong), nom) AS label,
					nom AS value,
					(import > 0) AS forbid,
					url AS domain,
					urlRegex
				FROM Sourcelien
				ORDER BY nom
				EOSQL
			)
			->queryAll(true);
		return array_map(
			function ($x) {
				if ($x['forbid'] === '0') {
					unset($x['forbid']);
				} else {
					$x['forbid'] = 1;
				}
				return $x;
			},
			$values
		);
	}

	/**
	 * @param string $table
	 * @param string $domain
	 * @return array Each row is an array with keys revueId, name, domain, url
	 */
	public static function listByDomain($table, $domain)
	{
		$cacheName = "Sourcelien::listByDomain_$table$domain";
		$value = Yii::app()->cache->get($cacheName);
		if ($value === false) {
			if ($domain === 'LIEN INTERNE') {
				$cond = "l.url LIKE '/%'";
				$params = [];
			} else {
				$cond = "l.domain LIKE :domain";
				$params = [':domain' => "%$domain"];
			}
			if ($table === 'Titre') {
				$sql = "SELECT l.name, l.url, t.titre, t.revueId FROM LienTitre l JOIN Titre t ON l.titreId = t.id WHERE $cond ORDER BY t.titre, l.name";
			} else {
				$sql = "SELECT l.name, l.url, e.nom, e.id AS editeurId FROM LienEditeur l JOIN Editeur e ON l.editeurId = e.id WHERE $cond ORDER BY e.nom, l.name";
			}
			$value = Yii::app()->db
				->createCommand($sql)
				->queryAll(true, $params);
			Yii::app()->cache->set($cacheName, $value, self::CACHE_DURATION);
		}
		return $value;
	}

	/**
	 * @param string $table
	 * @param string $name
	 * @return array Each row is an array with keys revueId, name, domain, url
	 */
	public static function listByName($table, $name)
	{
		$cacheName = "Sourcelien::listByName_$table$name";
		$value = Yii::app()->cache->get($cacheName);
		if ($value === false) {
			if ($table === 'Titre') {
				$sql = "SELECT l.name, l.url, t.titre, t.revueId FROM Lien$table l JOIN Titre t ON l.titreId = t.id WHERE name = :n ORDER BY t.titre, l.name";
			} else {
				$sql = "SELECT l.name, l.url, e.nom, e.id AS editeurId FROM Lien$table l JOIN Editeur e ON l.editeurId = e.id WHERE name LIKE :n ORDER BY e.nom, l.name";
			}
			$value = Yii::app()->db
				->createCommand($sql)
				->queryAll(true, [':n' => $name]);
			Yii::app()->cache->set($cacheName, $value, self::CACHE_DURATION);
		}
		return $value;
	}

	public static function identifyUrl(string $url): ?Sourcelien
	{
		$m = [];
		if (preg_match('#^(?:https?://)?([^/]+)(?:/|$)#', $url, $m)) {
			$host = $m[1];
			$domain = join('.', array_slice(explode('.', $host), -2));
			return Sourcelien::model()->find("url = :h OR url = :d", [':h' => $host, ':d' => $domain]);
		}
		return null;
	}

	public function getDomain(): string
	{
		if (strpos($this->url, '/') === false) {
			return $this->url;
		}
		return \components\UrlHelper::extractHostname($this->url);
	}

	/**
	 * @return string HTML img
	 */
	public function getLogo()
	{
		$image = null;
		$path = dirname(Yii::app()->getBasePath()) . '/images/liens';
		$url = Yii::app()->getBaseUrl() . '/images/liens';

		$byId = glob(sprintf('%s/%06d.*', $path, $this->id));
		if ($byId) {
			$image = "$url/{$byId[0]}";
		} else {
			$byName = glob(sprintf('%s/%s.*', $path, $this->id));
			if ($byName) {
				$image = "$url/{$byName[0]}";
			}
		}
		return ($image ? CHtml::image($image, $this->nom) : '');
	}

	/**
	 * Called automatically before Save().
	 *
	 * @return bool
	 */
	protected function beforeSave()
	{
		if ($this->isNewRecord) {
			$this->hdateCrea = $_SERVER['REQUEST_TIME'];
		}
		$this->hdateModif = $_SERVER['REQUEST_TIME'];
		$this->nom = Norm::text($this->nom);
		if (!$this->nomcourt) {
			$this->nomcourt = strtolower(preg_replace('/[\[(].+|[^a-zA-Z0-9]+/', '', Norm::unaccent($this->nom)));
		} else {
			$this->nomcourt = strtolower(Norm::unaccent($this->nomcourt));
		}
		if (strpos($this->url, "://") !== false && preg_match('#://(.+?)(?:/|$)#', $this->url, $m)) {
			$this->url = $m[1];
		}
		return parent::beforeSave();
	}
}
