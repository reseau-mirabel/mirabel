<?php

use components\Tools;

require_once __DIR__ . '/traits/LiensJson.php';

/**
 * This is the model class for table "Editeur".
 *
 * The followings are the available columns in table 'Editeur':
 * @property int $id
 * @property string $nom
 * @property string $prefixe
 * @property string $sigle
 * @property string $dateDebut
 * @property string $dateFin
 * @property string $description
 * @property string $presentation
 * @property ?string $idref
 * @property ?int $sherpa
 * @property ?string $ror
 * @property string $url
 * @property string $logoUrl
 * @property ?int $paysId
 * @property string $geo
 * @property string $liensJson
 * @property string $statut
 * @property string $role
 * @property int $hdateModif timestamp
 * @property int $hdateVerif timestamp
 *
 * @property Liens $liens
 * @property Intervention[] $interventions
 * @property TitreEditeur[] $titreEditeurs
 * @property ?Pays $pays
 * @property ?Partenaire $partenaire
 * @property Politique[] $politiques
 */
class Editeur extends AMonitored implements IUserCanConfirm, IWithIndirectSuivi, TitledObject, WithSelfUrl, JsonSerializable
{
	use LiensJson;
	use models\traits\UserCanConfirm;

	public const NAME_SHORT = 0;

	public const NAME_LONG = 1;

	public const NAME_DATES = 2;

	private const STATUT = [
		'normal' => 'Normal',
		'suppr' => 'Supprimé',
		'attente' => 'En attente',
	];

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Editeur';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['nom', 'required'],
			['nom, sigle, geo', 'length', 'max' => 255],
			['nom', \models\validators\DuplicatesValidator::class],
			['idref', \models\validators\PpnValidator::class],
			['ror', \models\validators\RorValidator::class],
			['idref', 'validateIdrefUnicity'],
			['url, logoUrl', 'url'],
			['url, logoUrl', 'length', 'max' => 512],
			['url', 'ext.validators.UrlFetchableValidator', 'on' => 'insert update'],
			['description, presentation', 'length', 'max' => 65535],
			['dateDebut, dateFin', 'validateDate'],
			['prefixe', 'length', 'max' => 25],
			['statut', 'in', 'range' => array_keys(self::STATUT)], // enum
			['confirm', 'boolean'],
			['paysId, sherpa', 'numerical', 'integerOnly' => true],
			['liensJson', 'safe', 'on' => 'import'], // an Intervention uses this
			['liens', 'safe'], // will use setLiens()
			// local validator methods
			['liensJson', 'validateLiensJson'],
			['role', 'validateRole'],
			// The following rule is used by search().
			[
				'nom, description, paysId, geo, statut, hdateModif, hdateVerif',
				'safe', 'on' => 'search',
			],
		];
	}

	public function validateDate($attribute, $params): void
	{
		if ($this->hasErrors($attribute) || $this->{$attribute} === null || $this->{$attribute} === '') {
			return;
		}
		$v = trim($this->{$attribute});
		$m = [];
		if (preg_match('#^(\d\d)/(\d\d)/(\d\d\d\d)$#', $v, $m)) {
			$v = "{$m[3]}-{$m[2]}-{$m[1]}";
		} elseif (preg_match('#^(\d\d)/(\d\d\d\d)$#', $v, $m)) {
			$v = "{$m[2]}-{$m[1]}";
		}
		$fullDate = str_pad($v, 10, '-01');
		$date = DateTimeImmutable::createFromFormat('Y-m-d', $fullDate);
		if ($date === false || $date->getTimestamp() > time()) {
			$this->addError($attribute, "La date est incorrecte ou est dans le futur.");
			return;
		}
		$this->{$attribute} = $v;
	}

	public function validateIdrefUnicity($attribute, $params): void
	{
		if ($this->hasErrors($attribute) || !$this->{$attribute}) {
			return;
		}
		$e = Editeur::model()->findBySql(
			"SELECT * FROM Editeur WHERE id <> :id AND idref = :idref",
			[':id' => (int) $this->id, ':idref' => strtoupper(trim($this->idref))]
		);
		if ($e !== null) {
			/** @var \Editeur $e */
			$this->addError('idref', "Cet IdRef est déjà attribué à l'éditeur {$e->getSelfLink()}.");
		}
	}

	public function validateLiensJson($attribute, $params): void
	{
		if ($attribute === 'liensJson' && !empty($this->liensJson)) {
			$urlValidation = !$this->confirm;
			if (($this->scenario === 'insert' || $this->scenario === 'update')) {
				$liens = $this->getLiens();
				if ($liens->validate($urlValidation)) {
					$this->liensJson = json_encode($liens, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
				} else {
					$this->addError('liensJson', join('<br />', $liens->getErrors()));
				}
			}
		}
	}

	public function validateRole(string $attribute): void
	{
		$value = $this->{$attribute};
		if ($value) {
			$allowed = self::getPossibleRoles();
			if (!isset($allowed[$value])) {
				$this->addError($attribute, "rôle inconnu");
			}
		} else {
			$value = null;
		}
	}

	/**
	 * Called automatically before delete().
	 *
	 * @return bool
	 */
	public function beforeDelete()
	{
		$countRevues = $this->countRevues(false);
		if ($countRevues) {
			$this->addError('id', $countRevues . " revues sont liées à cet éditeur. Suppression impossible.");
			return false;
		}
		return parent::beforeDelete();
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'interventions' => [self::HAS_MANY, 'Intervention', 'editeurId'],
			'titreEditeurs' => [self::HAS_MANY, 'TitreEditeur', 'editeurId'],
			'pays' => [self::BELONGS_TO, 'Pays', 'paysId'],
			'partenaire' => [self::HAS_ONE, 'Partenaire', 'editeurId'],
			'politiques' => [self::HAS_MANY, 'Politique', 'editeurId'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'nom' => 'Nom',
			'prefixe' => 'Préfixe',
			'sigle' => 'Sigle',
			'description' => 'Description',
			'presentation' => "Présentation",
			'dateDebut' => "Date de début",
			'dateFin' => "Date de fin",
			'idref' => 'IdRef',
			'sherpa' => 'ID Open policy finder',
			'ror' => 'ROR',
			'url' => 'Site web',
			'logoUrl' => "URL du logo",
			'paysId' => "Pays",
			'geo' => 'Repère géo',
			'statut' => 'Statut',
			'role' => "Type",
			'partenaireId' => "Partenaire associé",
			'hdateVerif' => 'Dernière vérification',
			'hdateModif' => 'Dernière modification',
			'confirm' => "Confirmer malgré l'avertissement",
		];
	}

	/**
	 * Each array key defines a function that can be chained before a find*() method.
	 */
	public function scopes()
	{
		return [
			'sorted' => [
				'order' => 'nom ASC',
			],
		];
	}

	/**
	 * Delete related data in "Suivi".
	 */
	public function afterDelete()
	{
		if ($this->id > 0) {
			Yii::app()->db->createCommand(
				"DELETE FROM Suivi WHERE cible = '" . $this->tableName() . "' AND cibleId = " . (int) $this->id
			)->execute();
		}
		parent::afterDelete();
	}

	/**
	 * Called automatically after save().
	 */
	public function afterSave()
	{
		$this->getLiens()->save("Editeur", (int) $this->id);
		parent::afterSave();
	}

	/**
	 * Returns an array of parents that have a direct "Suivi": [ [table => ,  id => ], ... ]
	 *
	 * @return array
	 */
	public function listParentsForSuivi()
	{
		return [
			['table' => 'Editeur', 'id' => $this->id],
		];
	}

	/**
	 * Returns the prefix+name.
	 *
	 * @return string prefix+name.
	 */
	public function getLongName(): string
	{
		return $this->prefixe . $this->nom;
	}

	/**
	 * Returns the full name, including prefix and sigle.
	 *
	 * @return string Full name.
	 */
	public function getFullName(int $long = self::NAME_LONG): string
	{
		return $this->prefixe . $this->nom
			. ($long >= self::NAME_LONG && $this->sigle ? ' — ' . $this->sigle : '')
			. ($long >= self::NAME_DATES && ($this->dateDebut || $this->dateFin) ? " ({$this->getPeriode()})" : "");
	}

	public function getPeriode(): string
	{
		if (!$this->dateDebut && !$this->dateFin) {
			return "";
		}
		if ($this->dateDebut && $this->dateFin) {
			return substr($this->dateDebut, 0, 4) . " à " . substr($this->dateFin, 0, 4);
		}
		if ($this->dateDebut) {
			return substr($this->dateDebut, 0, 4) . " à …";
		}
		return "… à " . substr($this->dateFin, 0, 4);
	}

	/**
	 * Returns a link toward editeur/view/id/...
	 *
	 * @param int $long 0, 1, 2
	 * @return string HTML link.
	 */
	public function getSelfLink(int $long = self::NAME_LONG): string
	{
		$options = ['itemprop' => "publisher"];
		if ($long === self::NAME_SHORT && $this->sigle) {
			$name = $this->sigle;
			$options['title'] = $this->getFullName();
		} else {
			$name = $this->getFullName($long);
		}
		$url = $this->getSelfUrl();
		return CHtml::link(
			CHtml::encode($name),
			Yii::app()->createAbsoluteUrl($url[0], array_splice($url, 1)),
			$options
		);
	}

	public function getSelfUrl(): array
	{
		return [
			'/editeur/view',
			'id' => $this->id,
			'nom' => Norm::urlParam($this->nom . ($this->sigle ? ' — ' . $this->sigle : '')),
		];
	}

	/**
	 * Returns the number of 'Revue' linked to this through 'Titre'.
	 *
	 * @param bool $active If true, only the active role on living journals.
	 *   If false, count those were the publisher is no longer active.
	 **/
	public function countRevues(bool $active): int
	{
		$condition = $active ? "AND te.ancien = 0 AND t.dateFin = ''" : "";
		return (int) Yii::app()->db
			->createCommand(<<<EOSQL
				SELECT count(DISTINCT t.revueId)
				FROM Titre_Editeur te
					JOIN Titre t ON te.titreId = t.id
				WHERE te.editeurId = {$this->id} $condition
				EOSQL
			)
			->queryScalar();
	}

	/**
	 * Builds an Intervention object that can be completed later.
	 */
	public function buildIntervention(bool $direct): Intervention
	{
		$i = parent::buildIntervention($direct);
		$i->suivi = false;
		if (isset($this->id)) {
			$i->editeurId = $this->id;
			$i->description = "Modification de l'éditeur « {$this->nom} »";
			$i->action = 'editeur-U';
			$i->suivi = (Suivi::isTracked($this) !== null);
		}
		return $i;
	}

	public static function getPossibleRoles(): array
	{
		$config = Config::read('sherpa.publisher_role');
		$roles = [];
		foreach ($config as $row) {
			if ($row[0]) {
				$roles[$row[0]] = $row[2];
			}
		}
		return $roles;
	}

	#[\ReturnTypeWillChange]
	public function jsonSerialize(): array
	{
		$a = $this->getAttributes();
		foreach (['id', 'paysId', 'hdateModif', 'hdateVerif'] as $k) {
			if (isset($a[$k])) {
				if ($a[$k]) {
					$a[$k] = (int) $a[$k];
				} else {
					$a[$k] = null;
				}
			}
		}
		foreach (['liensJson'] as $k) {
			if (isset($a[$k])) {
				unset($a[$k]);
			}
		}
		return $a;
	}

	/**
	 * Called automatically before validate().
	 *
	 * @return bool
	 */
	protected function beforeValidate()
	{
		if (strlen((string) $this->prefixe)) {
			$this->prefixe = str_replace(["’", "´"], "'", $this->prefixe);
			if (preg_match('/[\'-]\s*$/', $this->prefixe)) {
				$this->prefixe = trim($this->prefixe);
			} elseif (!preg_match('/[\s ]$/', $this->prefixe)) {
				$this->prefixe = trim($this->prefixe) . " ";
			}
		}
		if ($this->nom) {
			$this->nom = str_replace(["’", "´"], "'", trim($this->nom));
		}
		if ($this->geo) {
			$this->geo = trim(str_replace(["\t", "’", "´"], [" ", "'", "'"], $this->geo));
		}
		$m = [];
		if ($this->idref) {
			if (preg_match('#^https?://www\.idref\.fr/(\d+X?)$#', trim($this->idref), $m)) {
				$this->idref = $m[1];
			}
		} else {
			$this->idref = null;
		}
		if (preg_match('#^https?://v2\.sherpa\.ac\.uk/id/publisher/(\d+)$#', trim((string) $this->sherpa), $m)) {
			$this->sherpa = (int) $m[1];
		} elseif (ctype_digit(trim((string) $this->sherpa))) {
			$this->sherpa = (int) $this->sherpa;
		} else {
			$this->sherpa = null;
		}
		if ($this->ror) {
			if (preg_match('#^https?://ror\.org/(.*)$#', trim((string) $this->ror), $m)) {
				$this->ror = $m[1];
			}
		} else {
			$this->ror = null;
		}
		return parent::beforeValidate();
	}

	/**
	 * Called automatically before Save().
	 *
	 * @return bool
	 */
	protected function beforeSave()
	{
		$this->hdateModif = $_SERVER['REQUEST_TIME'];
		$this->nom = Tools::normalizeText($this->nom);
		$this->description = Tools::normalizeText($this->description);
		$this->presentation = ''; // Champ désactivé par #4740 ~24901
		if (empty($this->liensJson)) {
			$this->liensJson = '[]';
		}
		if (empty($this->idref)) {
			$this->idref = null;
		}
		if (empty($this->sherpa)) {
			$this->sherpa = null;
		}
		if (empty($this->ror)) {
			$this->ror = null;
		}
		if (empty($this->logoUrl)) {
			$this->logoUrl = '';
		}
		if (empty($this->paysId)) {
			$this->paysId = null;
		}
		return parent::beforeSave();
	}
}
