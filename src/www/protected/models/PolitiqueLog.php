<?php

/**
 * This is the model class for table "Politique".
 *
 * The followings are the available columns in table 'PolitiqueLog':
 * @property int $id
 * @property string $action
 * @property int $actionTime On save, current time if not set.
 * @property int $politiqueId May be 0.
 * @property int $editeurId
 * @property int $titreId May be 0.
 * @property int $utilisateurId
 * @property string $publisher_policy
 *
 * @property Editeur $editeur
 * @property Politique $politique
 * @property ?Titre $titre
 * @property Utilisateur $createur
 */
class PolitiqueLog extends CActiveRecord
{
	public const ACTION_ASK_FOR_ROLE = 'ask-for-role';

	public const ACTION_DELETE = 'delete';

	public const ACTION_PUBLISH = 'publish';

	public const ACTION_TOPUBLISH = 'topublish';

	public const ACTION_UPDATE = 'update';

	public function tableName(): string
	{
		return 'PolitiqueLog';
	}

	public function attributeLabels(): array
	{
		return [
			'id' => 'ID',
			'politiqueId' => 'Politique',
			'editeurId' => 'Éditeur',
			'titreId' => 'Titre',
			'utilisateurId' => 'Opérateur',
		];
	}

	public function relations(): array
	{
		return [
			'editeur' => [self::BELONGS_TO, 'Editeur', 'editeurId'],
			'politique' => [self::BELONGS_TO, 'Politique', 'politique.id'],
			'titre' => [self::BELONGS_TO, 'Titre', 'titre.id'],
		];
	}

	public function beforeSave()
	{
		if (!$this->actionTime) {
			$this->actionTime = time();
		}
		if (!$this->utilisateurId) {
			$this->utilisateurId = (int) Yii::app()->user->id;
		}
		if (empty($this->publisher_policy)) {
			$this->publisher_policy = '';
		}
		return parent::beforeSave();
	}
}
