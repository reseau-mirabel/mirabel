<?php

/**
 * ImportSelecForm class for Cairn, etc.
 *
 */
class ImportSpecForm extends CFormModel
{
	public $className;

	public $verbose = 1;

	public $simulation = false;

	public $checkDeletion = true;

	public static $enumClassName = [
		'Cairn' => 'Cairn',
		'CairnMagazine' => 'Cairn Magazine',
	];

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return [
			['className', 'required'],
			['className', 'in', 'range' => array_keys(self::$enumClassName)], // enum
			['verbose', 'numerical', 'integerOnly' => true],
			['simulation, checkDeletion', 'boolean'],
		];
	}

	/**
	 * Declares customized attribute labels.
	 */
	public function attributeLabels()
	{
		return [
			'className' => 'Source de l\'import',
			'verbose' => 'Verbosité',
			'simulation' => 'Simulation',
			'checkDeletion' => "Vérifier si des titres ont été retirés",
		];
	}

	/**
	 * Returns the parameters that will feed the Import's constructor.
	 *
	 * @return array
	 */
	public function getParameters()
	{
		return [
			'verbose' => $this->verbose,
			'simulation' => $this->simulation,
		];
	}
}
