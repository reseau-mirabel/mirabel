<?php

use components\Tools;
use models\validators\DateValidator;

/**
 * This is the model class for table "Partenaire".
 *
 * @todo auto-fill 'hash' on insert
 * @todo declare relations 'utilisateurs' and 'suivis' (or getSuivis() to be more versatile)
 * @todo create function getTitres($bouquet=null)
 *
 * The followings are the available columns in table 'Partenaire':
 * @property int $id
 * @property string $nom
 * @property string $sigle
 * @property string $type
 * @property string $description
 * @property string $presentation
 * @property string $prefixe
 * @property string $notes
 * @property string $url
 * @property string $email
 * @property ?string $importEmail
 * @property string $logoUrl
 * @property string $phrasePerso
 * @property mixed $persoRecherche JSON, liste de Sourcelien.nomcourt
 * @property ?int   $paysId
 * @property string $geo
 * @property ?string $latitude decimal in DB
 * @property ?string $longitude decimal in DB
 * @property string $hash
 * @property string $ipAutorisees
 * @property string $possessionUrl
 * @property string $proxyUrl
 * @property ?int   $editeurId null unless partenaire-editeur
 * @property bool   $fusionAcces
 * @property int $hdateCreation timestamp
 * @property int $hdateModif timestamp
 * @property string $statut
 * @property string $implication
 * @property string $sigb
 * @property string|string[] $usageLocal JSON in DB, array in form
 * @property string $rcr
 * @property bool $rcrPublic
 * @property bool $rcrHorsPossession
 *
 *
 * @property string $fullName
 * @property Utilisateur[] $utilisateurs
 * @property Possession[] $possessions
 * @property ?Editeur $editeur
 * @property ?Pays $pays
 *
 * @method Partenaire ordered() Cf scopes
 */
class Partenaire extends CActiveRecord
{
	public const PATTERN_IDLOCAL = 'IDLOCAL';

	public const STATUT_ACTIF = 'actif';

	public const STATUT_INACTIF = 'inactif';

	public const STATUT_PROVISOIRE = 'provisoire';

	public const STATUT_DEMO = 'démo';

	public const TYPE_EDITEUR = 'editeur';

	public const TYPE_IMPORT = 'import';

	public const TYPE_NORMAL = 'normal';

	public const EXTENSION = ['C3RB' => 'C3RB', 'Koha' => 'Koha', 'PMB' => 'PMB', 'Syracuse' => 'Syracuse'];

	public const DECOUVERTE = ['Ebsco' => 'Ebsco', 'Primo' => 'Primo'];

	public static $enumType = [
		'normal' => 'Normal', 'import' => 'Import', 'editeur' => 'Éditeur',
	];

	public static $enumStatut = [
		self::STATUT_ACTIF => 'Actif',
		self::STATUT_INACTIF => 'Inactif',
		self::STATUT_PROVISOIRE => 'Provisoire',
		self::STATUT_DEMO => 'Démonstration',
	];

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Partenaire';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		$rules = [
			['nom', 'required'],
			['email', 'required', 'on' => 'partenaire-editeur'],
			['nom, sigle, prefixe, email, geo, sigb', 'length', 'max' => 255],
			['url, logoUrl', 'url'],
			['url, logoUrl', 'length', 'max' => 512],
			['email, importEmail', 'length', 'max' => 255],
			['email, importEmail', 'ext.validators.EmailsValidator'],
			['description, presentation, implication, usageLocal', 'length', 'max' => 65535],
			['type', 'in', 'range' => array_keys(self::$enumType)], // enum
			['paysId', 'numerical', 'integerOnly' => true],
			['latitude, longitude', 'numerical', 'integerOnly' => false],
			['hash, url, logoUrl', 'default', 'value' => ""],
			['email', 'filter', 'filter' => function ($x) {
				return trim((string) $x);
			}],
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			['nom, type, description, email, hdateCreation, hdateModif, statut', 'safe', 'on' => 'search'],
		];
		if (Yii::app()->user->access()->toPartenaire()->admin()) {
			array_push(
				$rules,
				['notes', 'length', 'max' => 65535],
				['statut', 'in', 'range' => array_keys(self::$enumStatut)], // enum
			);
		}
		return $rules;
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'utilisateurs' => [
				self::HAS_MANY,
				'Utilisateur',
				'partenaireId',
				'order' => 'utilisateurs.nom, utilisateurs.prenom, utilisateurs.login',
			],
			'possessions' => [self::HAS_MANY, 'Possession', 'partenaireId'],
			'editeur' => [self::BELONGS_TO, 'Editeur', 'editeurId'],
			'pays' => [self::BELONGS_TO, 'Pays', 'paysId'],
		];
	}

	/**
	 * Cf <http://www.yiiframework.com/doc/guide/1.1/en/database.ar#named-scopes>
	 */
	public function scopes(): array
	{
		return [
			'ordered' => [
				'order' => 'nom ASC',
			],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'nom' => 'Nom',
			'sigle' => 'Nom court',
			'type' => 'Type',
			'description' => 'Détail',
			'presentation' => "Présentation",
			'prefixe' => 'Préfixe',
			'notes' => "Notes privées",
			'url' => 'Site web',
			'email' => 'Courriel',
			'importEmail' => "Courriel des imports",
			'logoUrl' => 'URL du logo',
			'phrasePerso' => "Phrase de personnalisation",
			'paysId' => "Pays",
			'geo' => "Ville ou lieu",
			'latitude' => "Latitude",
			'longitude' => "Longitude",
			'hash' => 'Clé API',
			'ipAutorisees' => 'IP locales',
			'possessionUrl' => 'URL des possessions',
			'proxyUrl' => 'URL du proxy',
			'editeurId' => "Éditeur associé",
			'fusionAcces' => 'Fusion des accès',
			'statut' => 'Statut',
			'implication' => 'Implication dans Mir@bel',
			'sigb' => 'SIGB utilisé',
			'hdateCreation' => 'Création',
			'hdateModif' => 'Dernière modification',
			'usageLocal' => 'Usage des données',
			'rcr' => "RCR",
			'rcrPublic' => "Affichage public des données liées au RCR",
			'rcrHorsPossession' => "Affichage des données liées au RCR pour toutes les revues (non limité aux possessions)",
		];
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @param int|bool $pageSize Number of items for pagination (false to disable pagination).
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($pageSize = 25): CActiveDataProvider
	{
		$criteria = new CDbCriteria;

		$criteria->compare('nom', $this->nom, true);
		$criteria->compare('type', $this->type);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('email', $this->email, true);
		$criteria->addCondition(DateValidator::buildTsConditionFromDateValue('hdateCreation', (string) $this->hdateCreation));
		$criteria->addCondition(DateValidator::buildTsConditionFromDateValue('hdateModif', (string) $this->hdateModif));
		$criteria->compare('statut', $this->statut);

		$sort = new CSort();
		$sort->attributes = ['nom', 'type', 'hdateCreation', 'hdateModif', 'statut'];
		$sort->defaultOrder = 'nom ASC';

		return new CActiveDataProvider(
			$this,
			[
				'criteria' => $criteria,
				'pagination' => ['pageSize' => $pageSize],
				'sort' => $sort,
			]
		);
	}

	/**
	 * Returns the full name, including the prefix with correct spacing.
	 */
	public function getFullName(): string
	{
		return $this->prefixe . $this->nom;
	}

	public function getShortName(): string
	{
		return empty($this->sigle) ? $this->nom : $this->sigle;
	}

	/**
	 * Return an array with all the "usageLocal" wich are not "Extension" or "Decouverte" and should be displayed in the textInput
	 */
	public function getAutreUsage(): array
	{
		$extDecouvKey = array_merge(array_keys(Partenaire::EXTENSION), array_keys(Partenaire::DECOUVERTE));
		return array_diff(json_decode($this->usageLocal, true), $extDecouvKey);
	}

	/**
	 * Returns a HTML link.
	 *
	 * @codeCoverageIgnore
	 * @return string HTML link.
	 */
	public function getSelfLink(bool $short = false): string
	{
		return CHtml::link(
			CHtml::encode($short ? $this->getShortName() : $this->nom),
			['/partenaire/view', 'id' => $this->id]
		);
	}

	/**
	 * Returns an assoc array('Suivi' => array(1, 2, 5), 'Ressource' => , 'Editeur' => ).
	 *
	 * @param bool $includeEditeurs If true (suiviPartenairesEditeurs), shows 'Editeur' data.
	 */
	public function getRightsSuivi(bool $includeEditeurs = true): array
	{
		if (empty($this->id)) {
			return [];
		}
		$rows = $this->getDbConnection()->createCommand(
			"SELECT cible, cibleId FROM Suivi WHERE partenaireId = {$this->id} "
				. ($includeEditeurs ? '' : " AND cible <> 'Editeur'")
				. " ORDER BY cible, cibleId"
		)->queryAll();
		$suivi = [];
		foreach ($rows as $row) {
			if (!isset($suivi[$row['cible']])) {
				$suivi[$row['cible']] = [];
			}
			$suivi[$row['cible']][] = (int) $row['cibleId'];
		}
		return $suivi;
	}

	/**
	 * Checks if this Partenaire can have records in Partenaire_Titre.
	 */
	public function canOwnTitles(): bool
	{
		if ($this->type === 'normal') {
			return true;
		}
		return false;
	}

	/**
	 * Returns nombre de revues suivi par le partenaire
	 */
	public function countRevuesSuivies(): int
	{
		if (empty($this->id)) {
			return 0;
		}
		$sql = "SELECT count(*) AS nbRevues "
			. "FROM Suivi s "
			. "WHERE s.cible = 'Revue' AND s.partenaireId = {$this->id}";
		return (int) $this->getDbConnection()->createCommand($sql)->queryScalar();
	}

	/**
	 * Returns a assoc array(<type> => array(partenaire1, partenaire2...)).
	 *
	 * @return array assoc array(<type> => array(partenaire1, partenaire2...)).
	 */
	public static function getListByType(): array
	{
		$criteria = [
			'condition' => "statut = 'actif'",
			'order' => 'type, nom',
		];
		$partenaires = self::model()->findAll($criteria);
		$ordered = [];
		foreach ($partenaires as $p) {
			if (!isset($ordered[$p->type])) {
				$ordered[$p->type] = [];
			}
			$ordered[$p->type][] = $p;
		}
		return $ordered;
	}

	/**
	 * Returns the ID of the first Partenaire whose IP ranges matches the given one.
	 *
	 * @return int partenaireId or 0.
	 */
	public static function findInstituteByIp(string $ip): int
	{
		$masks = Yii::app()->db
			->createCommand("SELECT id, ipAutorisees FROM Partenaire WHERE statut <> 'inactif' AND ipAutorisees <> ''")
			->query();
		foreach ($masks as $mask) {
			if (Tools::matchIpFilter($ip, $mask['ipAutorisees'])) {
				return (int) $mask['id'];
			}
		}
		return 0;
	}

	/**
	 * Returns a link for the current Partenaire and this Titre.
	 *
	 * @return string URL
	 */
	public function buildPossessionUrl(Titre $titre, ?string $idLocal = null): string
	{
		if (empty($this->id) || empty($titre->id)) {
			return "";
		}
		if (!empty($this->possessionUrl) && $titre->belongsTo($this->id)) {
			if (empty($idLocal)) {
				$idLocal = $this->dbConnection->createCommand(
					"SELECT identifiantLocal FROM Partenaire_Titre WHERE partenaireId = {$this->id}"
						. " AND titreId = {$titre->id} LIMIT 1"
				)->queryScalar();
			}
			if ($idLocal) {
				return str_replace(self::PATTERN_IDLOCAL, $idLocal, $this->possessionUrl);
			}
		}
		return "";
	}

	/**
	 * Helper function that returns an IMG if possible (checks if the file exists).
	 *
	 * @codeCoverageIgnore
	 * @return string HTML img or plain text.
	 */
	public function getLogoImg(bool $reduced = true, array $htmlOptions = []): string
	{
		if (isset($htmlOptions['class'])) {
			$htmlOptions['class'] .= " partenaire-logo";
		} else {
			$htmlOptions['class'] = "partenaire-logo";
		}
		$url = $this->getLogoUrl($reduced);
		if ($url) {
			return CHtml::image($url, $this->nom, $htmlOptions);
		}
		if ($this->logoUrl) {
			return CHtml::image($this->logoUrl, $this->nom, $htmlOptions);
		}
		return '';
	}

	/**
	 * Helper function that returns the URL or an empty string.
	 *
	 * @codeCoverageIgnore
	 * @return string URL
	 */
	public function getLogoUrl(bool $reduced = true): string
	{
		$relativePath = ($reduced ? '/public/images/partenaires/' : '/images/partenaires/');
		$path = DATA_DIR . $relativePath;
		$baseUrl = ($reduced ? Yii::app()->getBaseUrl(true) : Yii::app()->createAbsoluteUrl('/upload/view'));
		$url = '';
		$pngPath = $path . sprintf('%03d.png', $this->id);
		if (file_exists($pngPath)) {
			$lastUpdate = substr(md5((string) stat($pngPath)['ctime']), 0, 4);
			$url = $baseUrl . sprintf('%s%03d.png?h=%s', $relativePath, $this->id, $lastUpdate);
		} else {
			$jpgPath = $path . sprintf('%03d.jpg', $this->id);
			if (file_exists($jpgPath)) {
				$lastUpdate = substr(md5((string) stat($jpgPath)['ctime']), 0, 4);
				$url = $baseUrl . sprintf('%s%03d.jpg?h=%s', $relativePath, $this->id, $lastUpdate);
			}
		}
		return $url;
	}

	public function hasAbonnements(): bool
	{
		return Abonnement::model()->exists('partenaireId = ' . (int) $this->id);
	}

	public function listConventionFiles(): array
	{
		$uploadDir = DATA_DIR . '/upload';
		if (!is_dir($uploadDir) || !is_readable($uploadDir)) {
			return [];
		}
		$cwd = getcwd();
		chdir($uploadDir);
		$files = glob(sprintf("conventions/%03d*", $this->id));
		chdir($cwd);
		return $files ?: [];
	}

	public static function getRandomHash(): string
	{
		return md5(str_shuffle(md5(microtime())));
	}

	public function onUnsafeAttribute($name, $value)
	{
		// do nothing
	}

	public function getRcrList(): array
	{
		if ($this->rcr === null) {
			return [];
		}

		$decodedJson = json_decode($this->rcr, true);
		if (json_last_error() === JSON_ERROR_NONE) {
			if (is_array($decodedJson)) {
				$rcrList = $decodedJson;
			} else {
				$rcrList = [$decodedJson];
			}
		} else {
			$rcrList = explode(',', $this->rcr);
			$rcrList = array_map('trim', $rcrList);
		}
		return array_filter($rcrList);
	}

	/**
	 * Called automatically before validate().
	 *
	 * @return bool
	 */
	protected function beforeValidate()
	{
		$this->rcr = implode(', ', $this->getRcrList());
		if (is_array($this->usageLocal)) {
			$this->usageLocal = json_encode($this->usageLocal);
		}
		if (strlen($this->prefixe)) {
			$this->prefixe = str_replace(["’", "´"], "'", $this->prefixe);
			if (preg_match('/[\'-]\s*$/', $this->prefixe)) {
				$this->prefixe = trim($this->prefixe);
			} elseif (!preg_match('/[\s ]$/', $this->prefixe)) {
				$this->prefixe = trim($this->prefixe) . " ";
			}
		}
		if ($this->statut === 'inactif') {
			$users = $this->utilisateurs;
			if ($users) {
				foreach ($users as $u) {
					/** @var Utilisateur $u */
					if ($u->actif) {
						$this->addError('statut', "Un partenaire ayant des utilisateurs actifs ne peut être inactif.");
						break;
					}
				}
			}
			$suivi = $this->getRightsSuivi();
			if ($suivi) {
				$this->addError('statut', "Un partenaire ayant des suivis ne peut être inactif.");
			}
		}
		if ($this->nom) {
			$this->nom = str_replace(["’", "´"], "'", trim($this->nom));
		}
		if ($this->editeurId && $this->statut !== 'inactif' && empty($this->email)) {
			$this->addError('email', "Un éditeur-partenaire doit avoir une adresse électronique de contact.");
		}
		if ($this->latitude) {
			$this->latitude = str_replace(',', '.', trim($this->latitude));
			if ($this->latitude < -90 || $this->latitude > 90) {
				$this->addError('latitude', "La latitude doit être comprise entre -90 et 90°.");
			}
		} else {
			$this->latitude = null;
		}
		if ($this->longitude) {
			$this->longitude = str_replace(',', '.', trim($this->longitude));
			if ($this->longitude < -180 || $this->longitude > 180) {
				$this->addError('longitude', "La longitude doit être comprise entre -180 et 180°.");
			}
		} else {
			$this->longitude = null;
		}
		return parent::beforeValidate();
	}

	/**
	 * Called automatically before Save().
	 *
	 * @return bool
	 */
	protected function beforeSave()
	{
		$this->rcr = json_encode($this->getRcrList());
		if ($this->getIsNewRecord()) {
			$this->hdateCreation = $_SERVER['REQUEST_TIME'];
		}
		$this->hdateModif = $_SERVER['REQUEST_TIME'];
		$this->nom = Tools::normalizeText($this->nom);
		$this->description = Tools::normalizeText($this->description);
		if ($this->possessionUrl === null) {
			$this->possessionUrl = '';
		}
		if ($this->proxyUrl === null) {
			$this->proxyUrl = '';
		}
		if (preg_match('#^\s*<p>\s*</p>\s*$#s', (string) $this->presentation)) {
			$this->presentation = '';
		} else {
			$purifier = Yii::app()->getComponent('htmlpurifier');
			assert($purifier instanceof \CHtmlPurifier);
			$this->presentation = $purifier->purify($this->presentation);
		}
		if (empty($this->paysId)) {
			$this->paysId = null;
		}
		if ($this->importEmail === '') {
			$this->importEmail = null;
		}
		if (!$this->persoRecherche) {
			$this->persoRecherche = new \CDbExpression('NULL');
		}
		return parent::beforeSave();
	}

	protected function afterSave()
	{
		if ($this->persoRecherche instanceof \CDbExpression) {
			$this->persoRecherche = null;
		}
	}
}
