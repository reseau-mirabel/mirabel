<?php

use components\FileStore;
use processes\attribut\FileImport;

/**
 * @property ?int $id
 * @property int $sourceattributId
 * @property int $logtime Timestamp 0 if file loaded but not imported.
 * @property string $fileName Remote file name.
 * @property string $url
 * @property string $fileKey In FileStore.
 * @property string $reportKey In FileStore.
 * @property ?int $userId
 */
class AttributImportLog extends CActiveRecord
{
	public function tableName(): string
	{
		return 'AttributImportLog';
	}

	public function attributeLabels(): array
	{
		return [
			'id' => 'ID',
			'logtime' => 'Date',
			'fileName' => 'Fichier tableur',
			'url' => 'URL',
			'userId' => 'Opérateur',
		];
	}

	public function getFilePath(): string
	{
		if (empty($this->fileKey)) {
			return '';
		}
		$store = new FileStore(FileImport::FILESTORE_ID);
		return $store->get($this->fileKey);
	}

	public function needNewLog(): self
	{
		// logtime is > 0 iff this log is already complete.
		if ((int) $this->logtime === 0) {
			return $this;
		}

		$log = new self();
		$log->fileKey = $this->fileKey;
		$log->fileName = $this->fileName;
		$log->logtime = 0;
		$log->sourceattributId = $this->sourceattributId;
		$log->url = $this->url;
		$log->userId = empty(\Yii::app()->user) ? null : \Yii::app()->user->id;
		$log->save();
		return $log;
	}
}
