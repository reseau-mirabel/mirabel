<?php

/**
 * Description of PartenaireDecorator
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class PartenaireDecorator extends CComponent
{
	/**
	 * @var ?Partenaire
	 */
	private $partenaire;

	/**
	 * Custom constructor.
	 *
	 * @param int $partenaireId
	 * @return \PartenaireDecorator
	 */
	public static function fromId($partenaireId)
	{
		$new = new self();
		$new->partenaire = Partenaire::model()->findByPk($partenaireId);
		return $new;
	}

	/**
	 * Custom constructor.
	 *
	 * @param \Partenaire $partenaire
	 * @return \PartenaireDecorator
	 */
	public static function fromInstance($partenaire)
	{
		$new = new self();
		$new->partenaire = Partenaire::model()->findByPk($partenaire);
		return $new;
	}

	/**
	 * @param bool $long (opt, true) Long name?
	 * @param string $default (opt, "-") Text to display when the model is not set.
	 * @param bool $sigle
	 * @return string HTML
	 */
	public function getName($long = true, $default = "-", $sigle = false)
	{
		if ($this->partenaire) {
			if ($long) {
				return $this->partenaire->prefixe . $this->partenaire->nom
					. ($sigle && $this->partenaire->sigle ? " ({$this->partenaire->sigle})" : "");
			}
			return ($this->partenaire->sigle ?: $this->partenaire->nom);
		}
		return $default;
	}

	/**
	 *
	 * @param bool $long (opt, true) Long name?
	 * @param string $default (opt, "-") Text to display when the model is not set.
	 * @return string HTML
	 */
	public function getHtmlLink($long = true, $default = "-")
	{
		if ($this->partenaire) {
			return CHtml::link(
				CHtml::encode($this->getName($long, $default)),
				['/partenaire/view', 'id' => $this->partenaire->id]
			);
		}
		return "-";
	}
}
