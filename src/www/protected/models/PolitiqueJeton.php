<?php

/**
 * This is the model class for table "PolitiqueJeton".
 *
 * @property int $id
 * @property string $name
 * @property string $token
 * @property string $lastUpdate
 * @method self sorted() See scopes().
 */
class PolitiqueJeton extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'PolitiqueJeton';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['name, token', 'required'],
			['token', 'match', 'pattern' => '/^[a-zA-Z0-9]+$/', 'message' => "N'utiliser que des chiffres et des lettres non accentuées"],
			['name', 'length', 'max' => 255],
			['token', 'length', 'max' => 32],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => 'Nom',
			'token' => 'Jeton',
			'lastUpdate' => 'Dernière modification',
		];
	}

	/**
	 * Each array key defines a function that can be chained before a find*() method.
	 */
	public function scopes()
	{
		return [
			'sorted' => [
				'order' => 'name ASC',
			],
		];
	}
}
