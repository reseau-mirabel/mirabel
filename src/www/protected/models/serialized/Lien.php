<?php

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class Lien extends CModel implements \JsonSerializable
{
	/**
	 * @var ?int ID in Sourcelien
	 */
	public $sourceId;

	/**
	 * @var string Free text (copy of source.nom if sourceId is defined).
	 */
	public $src;

	/**
	 * @var string
	 */
	public $url;

	/**
	 * @var string Unsaved property.
	 */
	private $title = '';

	/**
	 * @var bool
	 */
	private $urlValidation = true;

	/**
	 * @return string HTML link
	 */
	public function __toString()
	{
		$source = $this->getSource();
		$options = ['class' => "nb"];
		if ($source && $source->nomlong) {
			$options['title'] = $source->nomlong;
		}
		if ($this->title) {
			$options['title'] = $this->title;
		}
		$name = $source ? $source->nom : $this->src;
		return CHtml::link($this->getLogo() . $name, $this->url, $options);
	}

	/**
	 * @return array attributes names
	 */
	public function attributeNames()
	{
		return ['sourceId', 'src', 'url'];
	}

	/**
	 * @return array customized attributes labels (name => label)
	 */
	public function attributeLabels()
	{
		return [
			'sourceId' => "Source",
			'src' => "Autre source",
			'url' => 'URL',
		];
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['src, url', 'required'],
			['url', 'validateUrl'],
			['src', 'length', 'max' => 255],
			['url', 'length', 'max' => 512],
			[['src', 'url'], 'filter', 'filter' => function ($x) {
				return trim((string) $x);
			}],
			['sourceId', 'numerical', 'integerOnly' => true, 'min' => 1],
		];
	}

	public function validateUrl(): void
	{
		if (!$this->url || !$this->urlValidation) {
			return;
		}
		$urlValidator = new UrlFetchableValidator();
		if (strpos($this->url, Yii::app()->getBaseUrl(true)) !== false) {
			// full URL when a local path is expected
			$this->addError(
				'url',
				"L'URL " . CHtml::encode($this->url) . " semble interne. Utiliser une URL sans domaine (\"/…\") ?"
			);
		} elseif ($this->url[0] == '/') {
			// URL without domain, check it as a local URL
			if (!$urlValidator->checkLink(Yii::app()->getBaseUrl(true) . $this->url)) {
				$this->addError(
					'url',
					"L'URL <code>" . CHtml::encode($this->url) . "</code> n'a pu être téléchargée."
				);
			}
		} elseif (!(new CUrlValidator)->validateValue($this->url)) {
			$this->addError(
				'url',
				"L'URL <code>" . CHtml::encode($this->url) . "</code> est de format incorrect."
			);
		} elseif (!$urlValidator->checkLink($this->url)) {
			$this->addError(
				'url',
				"L'URL <code>" . CHtml::encode($this->url) . "</code> n'a pu être téléchargée."
			);
		}
	}

	public function afterValidate()
	{
		if (!$this->errors) {
			$this->fillin();
		}
		return parent::afterValidate();
	}

	/**
	 * @return \Lien
	 */
	public function disableUrlValidation()
	{
		$this->urlValidation = false;
		return $this;
	}

	/**
	 * @param array $values
	 * @param bool $safeOnly
	 */
	public function setAttributes($values, $safeOnly = true)
	{
		parent::setAttributes($values, $safeOnly);
		$this->fillin();
	}

	public function setTitle(string $title): void
	{
		$this->title = $title;
	}

	public function isEmpty(): bool
	{
		return !trim($this->url) && !trim($this->src) && empty($this->sourceId);
	}

	public function isEditorial(): bool
	{
		static $editorialSourceIds = null;
		if ($editorialSourceIds === null) {
			$editorialSourceIds = \Yii::app()->db
				->createCommand("SELECT id FROM Sourcelien WHERE nomcourt IN('openpolicyfinder')")
				->queryColumn();
		}
		return in_array($this->sourceId, $editorialSourceIds);
	}

	public function isInternal(): bool
	{
		return !empty($this->url) && strncmp($this->url, '/', 1) === 0;
	}

	public function getSource(): ?Sourcelien
	{
		if ($this->sourceId) {
			$source = Sourcelien::model()->findByPk($this->sourceId);
			if (!$source) {
				$this->sourceId = null;
			}
			return $source;
		}
		return null;
	}

	/**
	 * Called automatically on json_encode() and such.
	 */
	#[\ReturnTypeWillChange]
	public function jsonSerialize(): array
	{
		return array_filter($this->getAttributes());
	}

	/**
	 * Fill in 'src' or 'sourceId' with the other parameter.
	 */
	private function fillin(): void
	{
		if (empty($this->url)) {
			return;
		}
		$source = $this->getSource();
		if ($source && !$this->src) {
			$this->src = $source->nom;
		} elseif ($this->src && !$this->sourceId) {
			$source = Sourcelien::model()->findByAttributes(['nom' => $this->src]);
			if ($source) {
				$this->sourceId = (int) $source->id;
			}
		}
		if (!$this->sourceId) {
			$this->fillinByUrl();
		}
		if (!$this->sourceId) {
			$this->sourceId = null;
		} else {
			$this->sourceId = (int) $this->sourceId;
		}
	}

	/**
	 * Fill in 'src' or 'sourceId' with the other parameter.
	 */
	private function fillinByUrl(): void
	{
		$source = Sourcelien::identifyUrl($this->url);
		if ($source) {
			$this->sourceId = (int) $source->id;
			if (!$this->src) {
				$this->src = $source->nom;
			}
		}
	}

	/**
	 * @return string HTML img or empty string
	 */
	private function getLogo(): string
	{
		if ($this->url[0] === '/') {
			// local link
			return CHtml::image(Yii::app()->getBaseUrl() . '/images/logo-mirabel-16px.png', $this->src);
		}
		$imageName = null;
		if ($this->sourceId) {
			$path = dirname(Yii::app()->getBasePath()) . '/images/liens';
			$imageById = glob(sprintf('%s/%06d.*', $path, $this->sourceId));
			if ($imageById) {
				$imageName = basename($imageById[0]);
			} else {
				$source = $this->getSource();
				if ($source) {
					$imageBySource = glob((sprintf('%s/%s.*', $path, $source->nomcourt)));
					if ($imageBySource) {
						$imageName = basename($imageBySource[0]);
					}
				}
			}
		}
		$url = Yii::app()->getBaseUrl() . '/images/liens';
		return ($imageName ? CHtml::image("$url/$imageName", $this->src) : '');
	}
}
