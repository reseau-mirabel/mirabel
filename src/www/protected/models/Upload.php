<?php

use processes\upload\UploadDestination;

class Upload extends CFormModel
{
	/** @var ?CUploadedFile */
	public $file;

	/** @var string */
	public $destType = 'public';

	/** @var string */
	public $destName;

	/** @var bool */
	public $overwrite = false;

	/** @var UploadDestination[] */
	public static $destinations = [];

	/** @var string Relative path and name of the destination file */
	protected $destRelFile;

	/** @var string Absolute path and name of the destination file */
	protected $destAbsFile;

	protected $overrideFileName = false;

	/**
	 * Constructor
	 *
	 * @param string|UploadDestination $destType
	 * @throws Exception
	 */
	public function __construct($destType, bool $overrideFileName = false)
	{
		parent::__construct();
		if ($destType) {
			if (is_string($destType)) {
				if (!isset(self::$destinations[$this->destType])) {
					throw new Exception("Erreur de code : le type d'upload est inconnu.");
				}
			} elseif ($destType instanceof UploadDestination) {
				self::$destinations[''] = $destType;
				$destType = '';
			} else {
				throw new \Exception("Wrong value for parameter destType in `new Upload()`");
			}
			$this->destType = $destType;
			$forceDestName = $this->getDestination()->forceDestName;
			if ($forceDestName) {
				$this->destName = $forceDestName;
			}
			$this->overrideFileName = $overrideFileName;
		}
	}

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		$rules = [
			'file' => ['file', 'file', 'types' => 'pdf, png, jpg, jpeg, csv, mkv, mp4, avi, webm, md, txt, doc, docx, odt, ppt'],
			['destType', 'in', 'range' => array_keys(self::$destinations)],
			['overwrite', 'boolean'],
		];
		if ($this->canOverrideFileName()) {
			$rules[] = ['destName', 'match', 'pattern' => '/^[\w()_.-]+$/'];
		}
		if (!empty($this->getDestination()->types)) {
			$rules['file']['types'] = $this->getDestination()->types;
		}
		return $rules;
	}

	/**
	 * Called automatically before validate().
	 */
	public function beforeValidate()
	{
		// file saved in {destinationPath}/{destName}.{srcExt}
		$this->destRelFile = $this->file->getName();
		if ($this->destName) {
			$this->destRelFile = $this->destName . substr($this->destRelFile, strrpos($this->destRelFile, '.'));
		}
		$d = $this->getDestination();
		$this->destAbsFile = rtrim($d->path, '/') . '/' . $this->destRelFile;

		if ($this->file && !$this->destName) {
			if (!preg_match('/^[\w().-]+$/', $this->file->name)) {
				$this->addError('file', "Le nom de fichier doit être composé de lettres non accentuées et de \"()_.-\"");
				return false;
			}
		}
		if ($this->destType === 'logos-p') {
			if (!$this->removeDuplicates()) {
				$this->addError('overwrite', "Ce dépôt écraserait le fichier existant.");
				return false;
			}
		}
		return parent::beforeValidate();
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return [
			'file' => 'Fichier',
			'overwrite' => "Remplacer le fichier déjà présent",
			'destName' => "Destination",
		];
	}

	public function canOverrideFileName(): bool
	{
		return empty($this->getDestination()->forceDestName) || $this->overrideFileName;
	}

	/**
	 * Remove duplicates if the "overwrite" flag is set.
	 */
	public function removeDuplicates(): bool
	{
		$pattern = preg_replace('/\.\w+$/', '.*', $this->destAbsFile);
		$duplicates = glob($pattern);
		if (glob($pattern)) {
			if ($this->overwrite) {
				foreach ($duplicates as $dup) {
					unlink($dup);
				}
			} else {
				return false;
			}
		}
		return true;
	}

	/**
	 * Move the uploaded file from its temporary place to its final destination.
	 */
	public function moveFile(): bool
	{
		if (file_exists($this->destAbsFile) && !$this->overwrite) {
			return false;
		}
		if (YII_ENV === 'test') {
			copy($this->file->tempName, $this->destAbsFile);
		} elseif (!$this->file->saveAs($this->destAbsFile)) {
			return false;
		}
		if ($this->destType === 'logos-p') {
			return $this->resizeLogo($this->destAbsFile);
		}
		return true;
	}

	/**
	 * Return various info on the destination of this upload.
	 */
	public function getDestination(): UploadDestination
	{
		return self::$destinations[$this->destType];
	}

	/**
	 * Return the URL for this file name and this destination.
	 */
	public function getViewUrl(string $filename = ''): string
	{
		if (!$filename) {
			$filename = $this->destRelFile;
		}
		$subpath = $this->getDestination()->subpath;
		if ($subpath && substr($subpath, -1) !== '/') {
			$subpath .= '/';
		}
		return $this->getDestination()->url . $subpath . rawurlencode($filename);
	}

	/**
	 * If the requested path is in one of the allowed destinations, return its path, else "".
	 *
	 * It compares the absolute path of the requested file to avoid publishing unwanted files.
	 * There must be no symbolic link in DATA_DIR, i.e. `realpath(DATA_DIR) === DATA_DIR`.
	 *
	 * @param string $webPath path extracted from the URL
	 * @return string An absolute full path on the server)
	 */
	public static function toServerPath(string $webPath): string
	{
		// Compatibility: before 2025-03, the web path had this prefix.
		if (str_starts_with($webPath, 'protected/data/')) {
			$webPath = substr($webPath, 15);
		}
		// Compatibility: the old storage had files directly under upload/
		if (strpos($webPath, '/') === false) {
			$webPath = "prive/$webPath";
		}

		$serverPath = realpath(DATA_DIR . "/upload/$webPath");
		if (!$serverPath) {
			$serverPath = realpath(DATA_DIR . "/$webPath");
		}
		if (!$serverPath) {
			// File was not found since realpath() returned false or "".
			return "";
		}

		// Security: ban dir traversal.
		// Check if the requested path is still under the upload/ or images/ directory.
		if (!str_starts_with($serverPath, DATA_DIR . "/upload/") && !str_starts_with($serverPath, DATA_DIR . "/images/")) {
			return "";
		}
		if (!is_file($serverPath)) {
			// Cannot send a directory, etc.
			return "";
		}

		// For a convention, check access rights on the Partenaire.
		$m = [];
		if (preg_match('#/conventions/(\d+)_#', $serverPath, $m)) {
			$partenaire = Partenaire::model()->findByPk((int) $m[1]);
			if (!Yii::app()->user->access()->toPartenaire()->update($partenaire)) {
				return "";
			}
		}

		return $serverPath;
	}

	/**
	 * Returns a human-readable size.
	 *
	 * @param string $format (opt) printf format.
	 */
	public static function getReadableFileSize(int $size, string $format = ""): string
	{
		$sizes = ['B', 'kB', 'MB', 'GB'];
		$sizeUnit = '';

		if ($size < 1024) {
			$format = '%01d %s';
			$sizeUnit = $sizes[0];
		} else {
			foreach ($sizes as $i => $u) {
				$sizeUnit = $u;
				if ($size < 1024 || $i === (count($sizes) - 1)) {
					break;
				}
				$size = $size / 1024;
			}
		}
		if ($format === "") {
			$format = $size < 10 && abs($size - round($size)) > .1 ? '%01.1f %s' : '%d %s';
		}
		return sprintf($format, $size, $sizeUnit);
	}

	public function resizeLogo(string $file, int $maxWidth = 450, int $maxHeight = 110): bool
	{
		[$width, $height] = getimagesize($file);
		[$newWidth, $newHeight] = processes\images\Resizer::computeSize($width, $height, $maxWidth, $maxHeight);
		if ($newWidth === 0) {
			$this->addError('file', "Dimensions d'image illisibles. Erreur de format ?");
			return false;
		}

		if (strtolower(substr($file, -4)) === '.png') {
			$oldImage = imagecreatefrompng($file);
		} else {
			$oldImage = imagecreatefromjpeg($file);
		}
		if (!$oldImage) {
			$this->addError('file', "Erreur de format d'image (jpeg/png).");
			return false;
		}
		$newImage = imagecreatetruecolor($newWidth, $newHeight);
		imagealphablending($newImage, false);
		imagesavealpha($newImage, true);
		imagecopyresampled($newImage, $oldImage, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

		$destDir = DATA_DIR . "/public/images/" . basename(dirname($file));
		if (!is_dir($destDir)) {
			@mkdir($destDir, 0777, true);
		}
		$destFile = preg_replace('/\..+?$/', '', basename($file));
		if (file_exists("$destDir/$destFile.jpg")) {
			unlink("$destDir/$destFile.jpg");
		}
		if (!imagepng($newImage, "$destDir/$destFile.png")) {
			$this->addError('file', "Erreur d'enregistrement de l'image redimensionnée.");
			return false;
		}
		return true;
	}

	public function onUnsafeAttribute($name, $value)
	{
		// do nothing
	}
}
