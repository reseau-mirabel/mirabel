<?php

declare(strict_types=1);

class AbonnementInfo
{
	public int $count = 0;

	public int $countRessources = 0;

	public int $countCollections = 0;

	public int $countRevues = 0;

	public function __construct(int $partenaireId)
	{
		$db = Yii::app()->db;
		$this->countRessources = (int) $db
			->createCommand("SELECT count(*) FROM Abonnement WHERE partenaireId = :pid AND collectionId IS NULL AND mask = " . Abonnement::ABONNE)
			->queryScalar([':pid' => $partenaireId]);
		$this->countCollections = (int) $db
			->createCommand("SELECT count(*) FROM Abonnement WHERE partenaireId = :pid AND collectionId IS NOT NULL AND mask = " . Abonnement::ABONNE)
			->queryScalar([':pid' => $partenaireId]);
		$this->count = $this->countRessources + $this->countCollections;
		if ($this->count) {
			$this->countRevues = (int) $db
				->createCommand(<<<EOSQL
					SELECT count(DISTINCT t.revueId)
					FROM
						Titre t
						JOIN Service s ON s.titreId = t.id
						JOIN Abonnement a ON s.ressourceId = a.ressourceId AND a.partenaireId = :pid AND a.mask = :a
						LEFT JOIN Service_Collection sc ON s.id = sc.serviceId
					WHERE
						(a.collectionId IS NULL) OR (sc.collectionId = a.collectionId)
					EOSQL
				)
				->queryScalar([':pid' => $partenaireId, ':a' => Abonnement::ABONNE]);
		}
	}
}
