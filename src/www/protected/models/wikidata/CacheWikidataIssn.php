<?php

namespace models\wikidata;

use Yii;

class CacheWikidataIssn
{
	use \models\traits\VerbosityEcho;

	/**
	 * @var int 0 to 2
	 */
	private int $verbose;

	/**
	 * @var int 0 to 2
	 */
	private int $simulation;

	/**
	 * @var int for Sparql and SQL queries
	 */
	private int $limit;

	public function __construct(int $verbose, int $simulation, int $limit = 0)
	{
		$this->verbose = $verbose;
		$this->simulation = $simulation;
		$this->limit = $limit;
	}

	/**
	 * Remplit la table cache WikidataIssn.
	 *
	 * @return int nombre d'entrées Wikidata récupérées
	 */
	public function fillWikidataIssn(): int
	{
		$zero = microtime(true);
		$rows = $this->fetchWikidataIssn();
		$timestamp = time();
		Yii::app()->db->createCommand('TRUNCATE TABLE WikidataIssn')->execute();
		$tr = Yii::app()->db->beginTransaction();
		$cmd = Yii::app()->db->createCommand("INSERT INTO WikidataIssn (id, qId, issn, revueId, titre, hdate) VALUES (null, ?, ?, ?, ?, ?)");
		foreach ($rows as $row) {
			$cmd->execute(array_merge($row, [$timestamp]));
		}
		$tr->commit();

		$this->vecho(1, sprintf("fill WikidataIssn  %f s\n", (microtime(true) - $zero)));
		return (int) Yii::app()->db->createCommand("SELECT COUNT(*) FROM WikidataIssn")->queryScalar();
	}


	/**
	 * Récupère dans Wikidata toutes les données ISSN
	 * et optionnellement l'identifiant Mirabel revueId s'il est renseigné
	 */
	private function fetchWikidataIssn(): array
	{
		$sparql=
			<<< 'SPARQL'
			SELECT ?item ?issn ?revueId ?titre
			WHERE
			{
			  ?item wdt:P236 ?issn .
			  OPTIONAL { ?item wdt:P4730 ?revueId . }
			  OPTIONAL { ?item wdt:P1476 ?titre . }
			}
			SPARQL;
		$sparql .= ($this->limit > 0 ? "LIMIT {$this->limit}" : "");

		$start1 = microtime(true);
		$WQuery = new Query('en', 'text/csv');
		$wdrows = explode("\n", $WQuery->querySparql($sparql));
		$this->vecho(1, sprintf("Sparql query  %f s\n", (microtime(true) - $start1)));

		$start2 = microtime(true);
		array_shift($wdrows);  // première ligne = en-tête
		array_pop($wdrows); // la dernière ligne reçue est vide
		$res = [];
		// prétraitement, reformatage et nettoyage des données récupérées
		foreach ($wdrows as $wdrow) {
			$row = str_getcsv($wdrow, ",", '"', '\\');
			$res[] = [
				$WQuery->getEntity($row[0]),
				mb_substr(iconv('UTF-8', 'ASCII//TRANSLIT', $row[1]), 0, 9), /** @todo diagnostic ? **/
				(empty($row[2]) ? null : (int) $row[2]),
				$row[3], // titre
			];
		}
		$this->vecho(1, sprintf("Prétraitement %d enregistrements. %f s\n", count($wdrows), (microtime(true) - $start2)));
		return $res;
	}

}