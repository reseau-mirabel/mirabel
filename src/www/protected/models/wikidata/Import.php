<?php

namespace models\wikidata;

use Titre;
use Yii;
use commands\models\LinkUpdater;

/**
 * Toutes les méthodes pour importer des données Wikidata et des liens Wikipédia dans Mir@bel
 */
class Import
{
	/**
	 * @var LinkUpdater global au processus
	 */
	public LinkUpdater $linkUpdater;

	private int $limit;

	public function __construct(int $verbose, int $simulation, int $limit)
	{
		$this->limit = $limit;

		$this->linkUpdater = new LinkUpdater();
		$this->linkUpdater->simulation = $simulation;
		$this->linkUpdater->verbose = $verbose;
		$sources = \Sourcelien::model()->findAllBySql("SELECT * FROM Sourcelien WHERE nomcourt LIKE 'wikipedia-__'");
		foreach ($sources as $s) {
			$this->linkUpdater->addSource($s);
		}
	}

	public function getLinkUpdater(): LinkUpdater
	{
		return $this->linkUpdater;
	}

	public function importWplinks(): string
	{
		$cmd = Yii::app()->db->createCommand(
			"SELECT DISTINCT titreId FROM Wikidata WHERE property LIKE 'WP:%' AND titreId > 0"
			. ($this->limit ? " LIMIT {$this->limit}" : "")
		);

		$output = LinkUpdater::getCsvlogHeader();
		foreach ($cmd->queryColumn() as $titreId) {
			$titre = Titre::model()->findByPk($titreId);
			$output .= $this->importWplinksTitre($titre);
		}
		return $output;
	}

	/**
	 * Méthode à usage unique (ou rare) pour remplacer les liens de name "Wikipedia" par "Wikipedia (langue)"
	 * conformément à la table Sourcelien
	 */
	public function updateWplinksChangeName(): void
	{
		$sql = "SELECT titreId, url, SUBSTR(domain,1,2) AS lang2  FROM LienTitre WHERE name='Wikipedia' AND domain LIKE '%.wikipedia.org'";
		$rows = Yii::app()->db->createCommand($sql)->queryAll(true);
		$count = ['ok' => 0, 'error' => 0];
		echo count($rows) . " enregistrements LienTitre à transformer... \n";
		echo LinkUpdater::getCsvlogHeader();

		foreach ($rows as $row) {
			$titre = \Titre::model()->findByPk($row['titreId']);
			$sourcename = sprintf("wikipedia-%s", $row['lang2']);
			$updater = $this->getLinkUpdater();
			echo $updater->updateLinks($titre, '', [ [$sourcename, $row['url']] ]);
			$count['ok']++;
		}
		print_r($count);
	}

	private function importWplinksTitre(Titre $titre): string
	{
		$sql = "SELECT id, qId, SUBSTRING(property,4,2) AS lang, url FROM Wikidata WHERE property LIKE 'WP:%' AND titreId = :tid";
		$rows = Yii::app()->db->createCommand($sql)->queryAll(true, [':tid' => $titre->id]);

		$urls = [];
		$logqids = [];
		foreach ($rows as $row) {
			$urls[] = ["wikipedia-" . $row['lang'], $row['url']];
			$logqids[] = $row['id'] . ':' . $row['qId'];
		}

		try {
			$csvRow = $this->getLinkUpdater()->updateLinks(
				$titre,
				implode(',', $logqids), // arbitrairement la 1ère ligne
				$urls
			);
		} catch (\Exception $e) {
			Yii::log($e->getMessage() . "\n--\nTRACE:\n" . $e->getTraceAsString(), \CLogger::LEVEL_WARNING, 'wikidata');
			$csvRow = join(';', [$titre->revueId, $titre->id, $titre->titre, "", "", $e->getMessage(), "ERREUR"]) . "\n";
		}
		return $csvRow;
	}
}
