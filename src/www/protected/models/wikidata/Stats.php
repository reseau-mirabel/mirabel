<?php

namespace models\wikidata;

use Yii;

class Stats
{
	public static function getPropertyStats(): array
	{
		$sql = "SELECT propertyType, property, COUNT(id) AS Nb, FROM_UNIXTIME(hdate) as Date "
			. "FROM Wikidata WHERE propertyType > 0 "
			. "GROUP BY propertyType, property ORDER BY propertyType, COUNT(id) DESC";
		return Yii::app()->db->createCommand($sql)->queryAll();
	}

	public static function getWikipediaLanguageStats(): array
	{
		$sql = "SELECT property, COUNT(id) AS Nb FROM Wikidata WHERE property LIKE 'WP:%' GROUP BY property ORDER BY Nb DESC";
		$rows = Yii::app()->db->createCommand($sql)->queryAll();

		$result = [];
		foreach ($rows as $row) {
			$result[$row['property']] = $row['Nb'];
		}
		return $result;
	}

	public static function getWikipediaLinksStats(): array
	{
		$sql = "SELECT COUNT(id) AS Nb, domain FROM LienTitre WHERE url LIKE '%.wikipedia.org%' "
				. "GROUP BY domain ORDER BY Nb DESC";
		$rows = Yii::app()->db->createCommand($sql)->queryAll();

		$result = [];
		foreach ($rows as $row) {
			$result[$row['domain']] = $row['Nb'];
		}
		return $result;
	}
}
