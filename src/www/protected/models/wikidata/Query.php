<?php

namespace models\wikidata;

class Query
{
	private const SPARQL_URL = 'https://query.wikidata.org/sparql';

	private const ENTITY_URL = 'http://www.wikidata.org/entity';

	private const PROPERTY_URL = 'https://www.wikidata.org/wiki/Property';

	private const ENTITYDATA_URL = "https://www.wikidata.org/wiki/Special:EntityData";

	private string $lang = '';

	private string $mimeFormat = '';

	public function __construct(string $lang, string $format)
	{
		$this->lang = $lang;
		$this->mimeFormat = $format;
	}

	/**
	 * return the result of Wikidata records
	 */
	public function querySparql(string $sparql): string
	{
		$qurl = self::SPARQL_URL . '?query=' . rawurlencode($sparql);
		return $this->queryRaw($qurl);
	}

	/**
	 * return the elaborate array of Wikidata records
	 */
	public function queryRefined(string $sparql): array
	{
		$qurl = self::SPARQL_URL . '?query=' . rawurlencode($sparql);
		$raw = json_decode($this->queryRaw($qurl), false);
		$res = [];
		$header = $raw->head->vars;
		foreach ($raw->results->bindings as $item) {
			$element = [];
			foreach ($header as $key) {
				if (isset($item->{$key})) {
					$element[$key] = $item->{$key}->value;
				}
			}
			$res[] = $element;
		}
		return $res;
	}

	/**
	 * return the entity extracted from an url
	 * @param string $url Wikidata entity url  eg. "http://www.wikidata.org/entity/Q42"
	 * @return string|null eg. "Q42", "P31"
	 */
	public function getEntity(string $url): ?string
	{
		if (preg_match('@' . self::ENTITY_URL . '/([A-Z]\d+)$@', $url, $matches)) {
			return $matches[1];
		}
		return null;
	}

	public function queryRaw(string $qurl): string
	{
		$request = new \components\Curl;
		$request->setopt(CURLOPT_HTTPHEADER, ["Accept: {$this->mimeFormat}"]);
		$request->setopt(CURLOPT_TIMEOUT, 30); // seconds
		return $request->get($qurl)->getContent();
	}

	public function getPropertyDetails(string $prop): array
	{
		$qurl = sprintf("%s/%s.json", self::ENTITYDATA_URL, $prop);
		$raw = $this->queryRaw($qurl);
		$details = json_decode($raw, false)->entities->{$prop};
		return [
			'wikidata' => sprintf("%s:%s", self::PROPERTY_URL, $prop),
			'name' => $details->labels->{$this->lang}->value,
			'desc' => $details->descriptions->{$this->lang}->value,
			'modified' => $details->modified,
			'datatype' => $details->datatype,
			'urlformat' => (isset($details->claims->P1630) ? $details->claims->P1630[0]->mainsnak->datavalue->value : ''),
		];
	}

	/**
	 * @todo la résolution peut se faire localement (sans requête http) pour le schéma wikidata-externalid-url.toolforge.org
	 */
	public static function resolveRedirection(string $url): string
	{
		$request = new \components\Curl;
		$request->setopt(CURLOPT_FOLLOWLOCATION, false);
		$request->setopt(CURLOPT_HEADER, true);
		$header = $request->get($url)->getContent();
		$m = [];
		if (preg_match('/^location: (.+)$/im', $header, $m)) {
			return trim($m[1]);
		}
		return $url;
	}
}
