<?php

namespace models\wikidata;

use CLogger;
use models\wikidata\Cache;
use models\wikidata\CacheWikidataIssn;
use models\wikidata\CacheWikipedia;
use models\wikidata\Compare;
use models\wikidata\CompareContent;
use Yii;

class Synchro
{
	use \models\traits\VerbosityEcho;

	public int $verbose = 0;

	private int $startTime;

	public function __construct()
	{
		$this->startTime = time();
	}

	public function run(bool $simulation, int $limit): bool
	{
		$this->vecho(1, "\nRécupération des liens Wikipédia...\n");
		$WpCache = new CacheWikipedia($this->verbose, (int) $simulation, $limit);
		$success1 = $this->wrapInTransaction(
			function () use ($WpCache) {
				$WpCache->fetchWikipediaLinks();
			},
			"property LIKE 'WP:%'"
		);
		if (!$success1) {
			echo "Annulation de la synchro des liens de Wikidata.\n";
			return false;
		}

		$this->vecho(1, "\nRécupération des propriétés Wikidata...\n");
		$WdCache = new Cache($this->verbose, (int) $simulation, $limit);
		$success2 = $this->wrapInTransaction(
			function () use ($WdCache) {
				$WdCache->fetchPropertyValues();
				$WdCache->fetchPropertyElements();
			},
			"property LIKE 'P%'"
		);
		if (!$success2) {
			echo "Annulation de la synchro des propriétés de Wikidata.\n";
			return false;
		}
		$this->vecho(1, "\nRésolution des redirections de liens (Mastodon, Bluesky, etc.)...\n");
		$WdCache->resolveRedirections();

		$this->vecho(1, "\nRemplissage de la table WikidataIssn...\n");
		$CacheWdI = new CacheWikidataIssn($this->verbose, (int) $simulation, $limit);
		$CacheWdI->fillWikidataIssn();

		$WComp = new Compare($this->verbose);
		$this->vecho(1, "\nDétermination des identifiants de titres à partir des ISSN...\n");
		$WComp->updateTitres();
		$this->vecho(1, "\nDétection des incohérences globales Wikidata / Mir@bel...\n");
		$WComp->compareInexistent();
		$WComp->compareComplement();
		$WComp->cleanupProperties();
		$compContent = new CompareContent($this->verbose);
		$this->vecho(1, "\nVérifie la cohérence du contenu Wikidata... \n");
		$compContent->checkWikidataConsistency();

		$this->vecho(1, "\nComparaisons Wikidata / Mir@bel...\n");
		$compContent->compareProperties(false);
		return true;
	}

	private function wrapInTransaction(callable $function, string $sqlCondition): bool
	{
		$transaction = Yii::app()->db->beginTransaction();
		try {
			$function();

			if (!$this->isComplete($sqlCondition)) {
				$transaction->rollback();
				// Just in case the transaction is not enough
				Yii::app()->db
					->createCommand("DELETE FROM Wikidata WHERE {$sqlCondition} AND hdate >= {$this->startTime}")
					->execute();
				return false;
			}
		} catch (\Exception $e) {
			$transaction->rollback();
			echo "Une erreur est survenue : " . $e->getMessage();
			Yii::log($e->getMessage() . "\n" . $e->getTraceAsString(), CLogger::LEVEL_ERROR, 'wikidata');
			return false;
		}
		Yii::app()->db
			->createCommand("DELETE FROM Wikidata WHERE {$sqlCondition} AND hdate < {$this->startTime}")
			->execute();
		$transaction->commit();
		return true;
	}

	private function isComplete(string $where): bool
	{
		$seuil = 90; // si le nouveau cache a moins de seuil % d'entrées par rapport au cache Backup on annule la mise à jour.

		$newrecords = (int) Yii::app()->db
			->createCommand("SELECT COUNT(*) FROM Wikidata WHERE $where AND hdate >= {$this->startTime}")
			->queryScalar();
		$oldrecords = (int) Yii::app()->db
			->createCommand("SELECT COUNT(*) FROM Wikidata WHERE $where AND hdate < {$this->startTime}")
			->queryScalar();
		if ($newrecords < $seuil / 100 * $oldrecords) {
			printf("ERREUR Cache : %d enregistrements vs %d anciens.\n", $newrecords, $oldrecords);
			return false;
		}
		return true;
	}
}
