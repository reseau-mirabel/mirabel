<?php

namespace models\wikidata;

use models\wikidata\Compare;
use Yii;

/**
 * Comparaison des données, phase 2/2
 * Comparaison effective des propriétés "comparables" (liens) entre Wikidata et Mirabel
 */
class CompareContent
{
	use \models\traits\VerbosityEcho;

	protected int $verbose = 0; // 0 to 2

	private int $time = 0; // timestamp constant for the whole process

	public function __construct(int $verbose)
	{
		$this->verbose = $verbose;
		$this->time = time();
	}

	public function checkWikidataConsistency(): void
	{
		$nb = $this->checkLinkResolver(Compare::COMP_WIKIDATA_INCOHERENT);
		$this->vecho(1, "$nb.\n");
	}

	private function checkLinkResolver($code): int
	{
		foreach (Cache::REDIRECTION_SERVERS as $server) {
			$sql = "UPDATE Wikidata SET comparison=:c  WHERE url LIKE :s";
		}
		$nb = Yii::app()->db->createCommand($sql)->execute([':c' => $code, ':s' => '%'.$server.'%']);
		return $nb;
	}

	/**
	 * Met à jour les enregistrement de la table Wikidata avec toutes les comparaisons de propriétés.
	 *
	 * @param bool $refresh seulement si la dernière comparaison est plus vieille que la modification du titre
	 */
	public function compareProperties(bool $refresh, ?int $revueId = null): void
	{
		$ids = Yii::app()->db->createCommand()
			->from("Sourcelien")
			->select('nomcourt, id')
			->where(['in', 'nomcourt', array_values(Cache::getComparableProperties())])
			->setFetchMode(\PDO::FETCH_KEY_PAIR)
			->queryAll();
		$this->vecho(1, "equal + different + missing.\n");
		try {
			foreach (Cache::getComparableProperties() as $property => $name) {
				$sourceId = (int) ($ids[$name] ?? 0);
				$this->vecho(1, "\n$property ($name)... ");
				$this->compareProperty($property, $sourceId, $revueId, $refresh);
			}
		} catch (\Exception $_) {
			return; // si la table Wikidata n'existe pas
		}
	}

	/**
	 * Update the Wikidata table with some comparisons.
	 *
	 * @param string $property Wikidata.property
	 * @param bool  $refresh seulement si la dernière comparaison est plus vieille que la modification du titre
	 */
	private function compareProperty(string $property, int $sourceId, ?int $revueId, bool $refresh): void
	{
		if (!$sourceId) {
			return;
		}
		$sqlbase = <<<EOSQL
			SELECT w.id AS wid, w.qId as qid, w.url AS wurl, l.id AS lid, l.url AS lurl
			FROM Wikidata w
			EOSQL;
		if ($refresh) {
			$sqlbase .= " JOIN Titre t ON t.id = w.titreId AND w.compDate < t.hdateModif ";
		}
		$sqlbase .= <<<EOSQL
				LEFT JOIN LienTitre l ON l.titreId = w.titreId AND l.sourceId = $sourceId
			WHERE w.property = :prop AND (comparison <> :error)
			EOSQL;
		if (isset($revueId)) {
			$sqlbase .= " AND w.revueId = $revueId ";
		}
		$params = [':prop' => $property, ':error' => Compare::COMP_WIKIDATA_INCOHERENT];

		$rows = Yii::app()->db->createCommand("$sqlbase AND w.url = l.url")->queryAll(true, $params);
		$this->saveComparison($rows, Compare::COMP_EQUAL);

		$rows = Yii::app()->db->createCommand("$sqlbase AND w.url <> l.url")->queryAll(true, $params);
		$this->saveComparison($rows, Compare::COMP_DIFFERENT);

		$rows = Yii::app()->db->createCommand("$sqlbase AND l.id IS NULL")->queryAll(true, $params);
		$this->saveComparison($rows, Compare::COMP_ONLY_WIKIDATA);
	}

	private function saveComparison(array $rows, int $compcode): int
	{
		$cnt = 0;
		foreach ($rows as $row) {
			$wdar = \Wikidata::model()->findByPk($row['wid']);
			$wdar->comparison = $compcode;
			$wdar->compUrl = ($row['lurl'] ?? '');
			$wdar->compDate = $this->time;
			if ($wdar->save()) {
				$cnt++;
			}
		}
		return $cnt;
	}
}
