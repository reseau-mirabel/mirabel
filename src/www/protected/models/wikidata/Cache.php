<?php

namespace models\wikidata;

use Yii;

class Cache
{
	use \models\traits\VerbosityEcho;

	public const PROPTYPE_WIKIPEDIA = 0;	// lien Wikipédia

	public const PROPTYPE_LIEN = 1;		 // comparable à la table LienTitre

	public const PROPTYPE_ISSN = 2;		 // comparable à la table Issn

	public const PROPTYPE_AUTRESOURCE = 3;  // source imports, eg. Cairn (fr), DOAJ

	public const PROPTYPE_TITRE = 4;		// propriété chaîne de caractères non comparable

	public const PROPTYPE_ELEMENT = 5;  // propriété dont l'objet est un autre élément Wikidata (Qnnnn)

	public const PROPERTIES_TREE = [
		self::PROPTYPE_LIEN => self::LIEN_PROPERTIES,
		self::PROPTYPE_ISSN => self::ISSN_PROPERTIES,
		self::PROPTYPE_AUTRESOURCE => self::AUTRESOURCE_PROPERTIES,
		self::PROPTYPE_TITRE => self::TITRE_PROPERTIES,
	];

	/**
	 * Propriétés d'identifiants externes / liens comparables aux lignes de la table LienTitre via Sourcelien.nomcourt
	 * Les seules comparées en pratique
	 */
	public const LIEN_PROPERTIES = [
		'P2013' => 'facebook', //+ P4003
		'P2002' => 'x', //+P6552
		'P7368' => 'journalbase',
		'P2397' => 'youtube',
		'P4264' => 'linkedin', //+ P6634
		'P2003' => 'instagram',
		'P4033' => 'mastodon',
		'P12361' => 'bluesky',
		'P1581' => 'Blog',
		'P8100' => 'JournalTOCs',
	];

	/**
	 * Propriétés d'identifiants externes / liens comparables aux colonnes de la table Issn
	 * issn sert pour l'alignement des données, les autres sont purement indicatives
	 */
	public const ISSN_PROPERTIES = [
		'P236' => 'issn',
		'P7363' => 'issnl',
		'P269' => 'sudocPpn',
		'P243' => 'worldcatOcn',
		'P268' => 'bnfArk',
	];

	/**
	 * Propriétés d'identifiants externes / liens comparables aux autres liens de la table LienTitre
	 * Ne sont pas comparées en pratique car Mirabel interroge les bases de référence
	 */
	public const AUTRESOURCE_PROPERTIES = [
		'P4700' => 'Cairn', // Cairn francophone, pas Cairn International
		'P6773' => 'HAL',
		'P5115' => 'DOAJ',
		'P3434' => 'ERIH PLUS',
		'P3127' => 'Latindex',
	];

	/**
	 * Propriétés de titres et dérivés, chaînes de caractères
	 */
	public const TITRE_PROPERTIES = [
		'P1476' => 'Titre',
		'P1680' => 'Sous-titre',
	];

	/**
	 * Propriétés dont l'objet est un autre élément Wikidata Qnnnnn
	 */
	public const ELEMENT_PROPERTIES = [
		'P31' => 'nature de l’élément',
		'P136' => 'genre artistique',
		'P921' => 'sujet ou thème principal',
		'P2360' => 'public cible',
	];

	/**
	 * @var int 0 to 2
	 */
	private int $verbose;

	/**
	 * @var int 0 to 2
	 */
	private int $simulation;

	/**
	 * @var int Timestamp constant for writing in WikidataIssn.
	 */
	private int $time;

	public const REDIRECTION_SERVERS = [
		'wikidata-externalid-url.toolforge.org',
		'fedirect.toolforge.org'
	];

	/**
	 * @var int for Sparql and SQL queries
	 */
	private int $limit;

	public function __construct(int $verbose, int $simulation, int $limit = 0)
	{
		$this->verbose = $verbose;
		$this->simulation = $simulation;
		$this->limit = $limit;
		$this->time = (int) time();
	}


	/**
	 * toutes les propriétés
	 */
	public static function getWdProperties(): array
	{
		return array_merge(
			self::ISSN_PROPERTIES, self::AUTRESOURCE_PROPERTIES, self::LIEN_PROPERTIES,
			self::TITRE_PROPERTIES,
			self::ELEMENT_PROPERTIES
		);
	}

	public static function getComparableProperties(): array
	{
		return self::LIEN_PROPERTIES;
	}

	/**
	 * les propriétés non soumises à la comparaison Wikidata / Mirabel
	 */
	public static function getIncomparableProperties(): array
	{
		return array_merge(
			self::ISSN_PROPERTIES, self::AUTRESOURCE_PROPERTIES,
			self::TITRE_PROPERTIES,
			self::ELEMENT_PROPERTIES
			);
	}

	/**
	 * @param string $code  Wikidata.propcode
	 */
	public static function getPropName(string $code): string
	{
		$properties = self::getWdProperties();
		if (isset($properties[$code])) {
			return sprintf("%s-%s", $code, $properties[$code]);
		}
		return sprintf("%s", $code);
	}

	/**
	 * Récupère les valeurs et les liens puis les enregistre pour toutes les propriétés dont l'objet
	 * est un identifiant externe ou une chaîne de caractères (titres)
	 * @return int nombre de propriétés enregistrées
	 */
	public function fetchPropertyValues(): int
	{
		$total = 0;
		foreach (self::PROPERTIES_TREE as $proptype => $properties) {
			$this->vecho(1, "\nType $proptype. \n");
			foreach ($properties as $prop => $name) {
				$timestart = microtime(true);
				$this->vecho(1, "$prop ($name)... ");
				$records = $this->getPropertyValues($prop, $proptype);
				$count = $this->savePropertyRecords($records);
				$dtime = microtime(true) - $timestart;
				$this->vecho(1, sprintf("    %5d  %6.3f s.\n", $count, $dtime));
				$total += $count;
			}
		}
		return $total;
	}

	/**
	 * Récupère les valeurs et les liens puis les enregistre pour les propriétés dont l'objet est un élément Qnnnnn
	 * @return int nombre de propriétés enregistrées
	 */
	public function fetchPropertyElements(): int
	{
		$total = 0;
		$this->vecho(1, sprintf("\nType %d. \n", self::PROPTYPE_ELEMENT));
		foreach (self::ELEMENT_PROPERTIES as $prop => $name) {
			$timestart = microtime(true);
			$this->vecho(1, "$prop ($name)... ");
			$records = $this->getPropertyElements($prop, self::PROPTYPE_ELEMENT);
			$count = $this->savePropertyRecords($records);
			$dtime = microtime(true) - $timestart;
			$this->vecho(1, sprintf("    %5d  %6.3f s.\n", $count, $dtime));
			$total += $count;
		}
		return $total;
	}

	/**
	 * Résoud les redirections du champ Wikidata.url
	 */
	public function resolveRedirections(): int
	{
		$n = 0;
		$sql = "SELECT id, url FROM Wikidata WHERE url LIKE :d";
		foreach (self::REDIRECTION_SERVERS as $domain) {
			$this->vecho(1, "Serveur $domain\n");
			$records = Yii::app()->db->createCommand($sql)->queryAll(true, [':d' => '%' . $domain . '%']);
			foreach ($records as $record) {
				$n++;
				$redirect = Query::resolveRedirection($record['url']);
				$wdrecord = \Wikidata::model()->findByPk($record['id']);
				$this->vecho(2, sprintf("%4d. %s -> %s \n", $n, $wdrecord->value, $redirect));
				$wdrecord->setAttribute('url', $redirect);
				$wdrecord->save();
				if ($this->limit > 0 && $n >= $this->limit) {
					return $n;
				}
			}
		}
		return $n;
	}

	/**
	 * Pour les propriétés qui ont pour objet un *identifiant externe* (Wikidata) résoluble en lien-url
	 * prépare les champs "value" (identifiant externe) et "url" (lien-url)
	 * @param string $prop property eg. "P2002"
	 * @param int $proptype property type
	 */
	private function getPropertyValues(string $prop, int $proptype): array
	{
		$this->checkProperty($prop);
		$sparql =
			<<< SPARQLPROP
			SELECT ?item ?idMirabel ?extId ?extUrl
			WHERE
			{
				OPTIONAL { wd:%s wdt:P1630 ?formatterUrl . }
				?item wdt:P4730 ?idMirabel .
				?item wdt:%s ?extId .
				BIND(IRI(REPLACE(?extId, '^(.+)$', ?formatterUrl)) AS ?extUrl).
			}
			SPARQLPROP
			. ($this->limit > 0 ? "LIMIT {$this->limit}" : "");
		$req = sprintf($sparql, $prop, $prop);
		$WQuery = new Query('en', 'application/sparql-results+json');
		$wdresult = $WQuery->queryRefined($req);
		$timestamp = time();

		$result = [];
		foreach ($wdresult as $wdrow) {
			$result[] = [
				'revueId' => (int) $wdrow['idMirabel'],
				'titreId' => 0,
				'qId' => $WQuery->getEntity($wdrow['item']),
				'property' => $prop,
				'propertyType' => $proptype,
				'value' => ($wdrow['extId'] ?? ''),
				'url' => ($wdrow['extUrl'] ?? ''),
				'hdate' => $timestamp,
			];
		}
		return $result;
	}

	/**
	 * Pour les propriétés qui ont pour objet un element Wikidata Qnnnnn
	 * prépare les champs "object" (Q....) et "objectLabel" 
	 * @param string $prop property eg. "P136"
	 * @param int $proptype property type
	 */
	private function getPropertyElements(string $prop, int $proptype): array
	{
		$this->checkProperty($prop);
		$sparql =
			<<< SPARQLPROP
			SELECT ?revue ?idMirabel ?object ?objectLabel
			WHERE {
			  ?revue wdt:P4730 ?idMirabel ;
					 wdt:%s ?object .
			  SERVICE wikibase:label { bd:serviceParam wikibase:language "fr,en". }
			}
			SPARQLPROP
			. ($this->limit > 0 ? "LIMIT {$this->limit}" : "");
		$req = sprintf($sparql, $prop);
		$WQuery = new Query('en', 'application/sparql-results+json');
		$wdresult = $WQuery->queryRefined($req);
		$timestamp = time();

		$result = [];
		foreach ($wdresult as $wdrow) {
			$result[] = [
				'revueId' => (int) $wdrow['idMirabel'],
				'titreId' => 0,
				'qId' => $WQuery->getEntity($wdrow['revue']),
				'property' => $prop,
				'propertyType' => $proptype,
				'object' => $WQuery->getEntity($wdrow['object']),
				'objectLabel' => ($wdrow['objectLabel'] ?? ''),
				'hdate' => $timestamp,
			];
		}
		return $result;
	}

	/**
	 * Throws an exception is $prop is not valid, and NOP is the property is valid.
	 */
	private function checkProperty(string $prop): void
	{
		$properties = self::getWdProperties();
		if (!isset($properties[$prop])) {
			throw new \Exception("Unknown property: $prop");
		}
	}

	/**
	 * Remplit la table Wikidata pour les propriétés préparées
	 *
	 * @param array $records
	 */
	private function savePropertyRecords(array $records): int
	{
		foreach ($records as $record) {
			$wdrecord = new \Wikidata();
			$wdrecord->setAttributes($record, false);
			if ($this->simulation === 0) {
				$wdrecord->save(false);
			} elseif ($this->simulation === 1) {
				$wdrecord->validate();
			}
		}
		return count($records);
	}

}
