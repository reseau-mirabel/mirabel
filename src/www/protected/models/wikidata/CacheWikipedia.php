<?php

namespace models\wikidata;

use Yii;

class CacheWikipedia
{
	use \models\traits\VerbosityEcho;

	/**
	 * @var ?array [$alpha3 => $alpha2]
	 */
	private $MirabelLanguages = null;

	/**
	 * @var int 0 to 2
	 */
	private int $verbose;

	/**
	 * @var int 0 to 2
	 */
	private int $simulation;

	/**
	 * @var int for Sparql and SQL queries
	 */
	private int $limit;

	public function __construct(int $verbose, int $simulation, int $limit = 0)
	{
		$this->verbose = $verbose;
		$this->simulation = $simulation;
		$this->limit = $limit;
	}

	/**
	 * Retrieve Wikipedia links and save them for all languages
	 * @return int nombre de liens Wikipedia récupérés
	 */
	public function fetchWikipediaLinks(): int
	{
		if ($this->MirabelLanguages === null) {
			$this->MirabelLanguages = self::findMirabelLanguages();
		}
		$n = 0;
		foreach (array_keys($this->MirabelLanguages) as $alpha3) {
			$n++;
			$timestart = microtime(true);
			$this->vecho(1, sprintf("%3d. %s...", $n, $alpha3));
			$count = $this->saveWikipediaLinks($alpha3);
			$dtime = microtime(true) - $timestart;
			$this->vecho(1, sprintf("%4d %6.3f s. \n", $count, $dtime));
			if ($this->limit > 0 && $n >= $this->limit) {
				break;
			}
		}
		return (int) Yii::app()->db->createCommand("SELECT COUNT(*) FROM Wikidata WHERE property LIKE 'WP:%'")->queryScalar();
	}

	/**
	 * @param string $alpha3 language  eg. "eng", "fre"
	 */
	private function getWikipediaLinks(string $alpha3): array
	{
		if ($this->MirabelLanguages === null) {
			$this->MirabelLanguages = self::findMirabelLanguages();
		}
		$alpha2 = $this->MirabelLanguages[$alpha3];
		$sparql =
			<<<SPARQL
			SELECT ?item ?idMirabel ?linkwp ?itemLabel
				WHERE
				{
					?item wdt:P4730 ?idMirabel .
					?linkwp schema:about ?item .
					?linkwp schema:isPartOf <https://%s.wikipedia.org/> .
					OPTIONAL { SERVICE wikibase:label { bd:serviceParam wikibase:language "%s". } }
				}
			SPARQL
			. ($this->limit > 0 ? " LIMIT {$this->limit}" : "");
		$req = sprintf($sparql, $alpha2, $alpha2);
		$WQuery = new Query('', 'application/sparql-results+json');
		$wdresult = $WQuery->queryRefined($req);
		$timestamp = time();

		$result = [];
		foreach ($wdresult as $wdrow) {
			if (!$this->isLanguageInRevue($alpha3, (int) $wdrow['idMirabel'])) {
				// uniquement les langues officielles de la revue
				continue;
			}
			$result[] = [
				'revueId' => (int) $wdrow['idMirabel'],
				'titreId' => 0,
				'qId' => $WQuery->getEntity($wdrow['item']),
				'property' => sprintf('WP:%s', $alpha2),
				'propertyType' => Cache::PROPTYPE_WIKIPEDIA,
				'value' => '', //not applicable
				'url' => $wdrow['linkwp'],
				'hdate' => $timestamp,
			];
		}

		return $result;
	}

	/**
	 * Return true if $alpha3 is an official language for this Revue.
	 */
	private function isLanguageInRevue(string $alpha3, int $revId): bool
	{
		$sql = "SELECT MAX(JSON_CONTAINS(langues, :lang, '$')) FROM Titre WHERE revueId = :rid";
		return (bool) Yii::app()->db
			->createCommand($sql)
			->queryScalar([':lang' => json_encode($alpha3), ':rid' => $revId]);
	}

	/** Get all languages linked to Mirabel revues.
	 *
	 * alpha3 is used by Mirabel, whereas alpha2 is used for Wikipedia links.
	 *
	 * @return array [ $alpha3 => $alpha2 ]
	 */
	private static function findMirabelLanguages(): array
	{
		$mirabelLanguages = [];
		$codes3to2 = self::getCodesLanguages();
		$sql = "SELECT DISTINCT langues FROM Titre WHERE langues <> '[]'";
		$titreslangues = Yii::app()->db->createCommand($sql)->queryColumn();
		foreach ($titreslangues as $strlangues) {
			foreach (json_decode($strlangues) as $langue) {
				if (isset($codes3to2[$langue]) && !isset($mirabelLanguages[$langue])) {
					$mirabelLanguages[$langue] = $codes3to2[$langue];
				}
			}
		}
		asort($mirabelLanguages);
		return $mirabelLanguages;
	}

	/**
	 * Fetch languages codes from Wikidata (independant from Mirabel data).
	 *
	 * @return array [ $alpha3 => $alpha2 ]
	 */
	private static function getCodesLanguages(): array
	{
		/*
		 * note P218 = ISO 639-1 apha2
		 * note P219 = ISO 639-2 apha3 multiple
		 * note P220 = ISO 639-3 apha3 unique
		 * @see \models\validators\LangIso639Validator
		 */
		$sparql =
			<<<SPARQL
			SELECT ?alpha2 ?alpha3
			WHERE {
			   ?item wdt:P218 ?alpha2 ;
					 wdt:P219 ?alpha3 .
			}
			SPARQL;
		$WQuery = new Query('', 'application/sparql-results+json');
		$codeslangues = $WQuery->queryRefined($sparql);

		$res = [];
		foreach ($codeslangues as $codelangue) {
			$res[$codelangue['alpha3']] = $codelangue['alpha2'];
		}
		return $res;
	}

	/**
	 * Fill the Wikidata table with Wikipedia links in one language.
	 */
	private function saveWikipediaLinks(string $alpha3): int
	{
		$records = $this->getWikipediaLinks($alpha3);

		foreach ($records as $record) {
			$wdrecord = new \Wikidata();
			$wdrecord->setAttributes($record, false);
			if ($this->simulation == 0) {
				$wdrecord->save(false);
			} elseif ($this->simulation == 1) {
				$wdrecord->validate();
			}
		}
		return count($records);
	}
}
