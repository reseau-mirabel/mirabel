<?php

namespace models\wikidata;

use Yii;

/**
 * Comparaison des données, phase 1/2
 * Toutes les méthodes d'alignement des titres Mirabel avec les données Wikidata, basées sur l'ISSN (P236)
 */
class Compare
{
	use \models\traits\VerbosityEcho;

	/*
	 * codes for comparisons between the Mirabel value and the Wikidata cache
	 */
	public const COMP_BOTH_EMPTY = 0;

	public const COMP_EQUAL = 1;

	public const COMP_ONLY_MIRABEL = 10;

	public const COMP_ONLY_WIKIDATA = 11;

	public const COMP_DIFFERENT = 20;

	public const COMP_MULTIVALUED = 30; // Multivalué dans Wikidata, valeur simple dans Mirabel

	public const COMP_WIKIDATA_INCOHERENT = 70; // La donnée Wikidata est incohérente/fausse/obsolète

	// À partir d'ici, avertissements/erreurs de matching entre les titres Mirabel et les items Wikidata
	public const COMP_TITRE_OLD = 80;		  // le titre porteur des propriétés n'est pas le dernier de la revue

	public const COMP_MULTIPLE_TITLES = 81;	// le rapprochement avec les ISSN renvoie plusieurs titres

	public const COMP_MULTIPLE_QIDS = 82;	  // une même revue Mir@abel est retournée par plusieurs éléments Wikidata

	public const COMP_MULTIPLE_REVUES = 83;	// un même élément Wikidata est associé à plusieurs revues Mir@bel

	public const COMP_ISSN_INCONSISTENT = 98;  // aucun ISSN commun entre Mir@bel et Wikidata

	public const COMP_INEXISTENT = 99;		 // Mir@belId dans Wikidata n'existe pas dans Mir@bel  (revue supprimée)

	public const COMP_UNKNOWN = 100;		   // catchall => probablement le calcul des comparaisons n'a pas été lancé


	public const COMP_DEFAULT_ORDER = '11,20,30,10,1,0'; // Affichage de vérification

	public const COMP_INCONSISTENCIES = 80;	  // seuil des données incohérentes (inclus)

	public const COMP_INCONSISTENCIES_CRIT = 95; // seuil des données incohérentes graves (inclus)

	public const COMP_CODES = [
		self::COMP_BOTH_EMPTY => 'Vides',
		self::COMP_EQUAL => 'Égales',
		self::COMP_ONLY_MIRABEL => 'Seulement Mirabel',
		self::COMP_ONLY_WIKIDATA => 'Seulement Wikidata',
		self::COMP_DIFFERENT => 'Différentes',
		self::COMP_MULTIVALUED => 'Multivaluée',
		self::COMP_WIKIDATA_INCOHERENT => 'Donnée Wikidata fausse/obsolète',
		self::COMP_TITRE_OLD => 'Titre porteur obsolète',
		self::COMP_MULTIPLE_TITLES => 'Plusieurs titres par Qid',
		self::COMP_MULTIPLE_QIDS => 'Plusieurs Qids par revue',
		self::COMP_MULTIPLE_REVUES => 'Plusieurs revues par Qid',
		self::COMP_ISSN_INCONSISTENT => 'ISSN introuvable',
		self::COMP_INEXISTENT => 'Disparue',
		self::COMP_UNKNOWN => 'Inconnue (erreur)',
	];

	public const COMP_LEVELS = [
		self::COMP_BOTH_EMPTY => 'success',
		self::COMP_EQUAL => 'success',
		self::COMP_ONLY_MIRABEL => 'info',
		self::COMP_ONLY_WIKIDATA => 'warning',
		self::COMP_DIFFERENT => 'error',
		self::COMP_MULTIVALUED => 'warning',
		self::COMP_WIKIDATA_INCOHERENT => 'error',
		self::COMP_TITRE_OLD => 'warning',
		self::COMP_MULTIPLE_TITLES => 'info',
		self::COMP_MULTIPLE_QIDS => 'info',
		self::COMP_MULTIPLE_REVUES => 'warning',
		self::COMP_ISSN_INCONSISTENT => 'error',
		self::COMP_INEXISTENT => 'error',
		self::COMP_UNKNOWN => 'error',
	];

	protected int $verbose = 0; // 0 to 2

	private int $time = 0; // timestamp constant for the whole process

	public function __construct(int $verbose)
	{
		$this->verbose = $verbose;
		$this->time = time();
	}

	/**
	 * @param int $numeric  Wikidata.comparison
	 */
	public static function getCompCode(int $numeric): string
	{
		if (isset(self::COMP_CODES[$numeric])) {
			return sprintf("%s", self::COMP_CODES[$numeric]);
		}
		return sprintf("%s", $numeric);
	}

	/**
	 *
	 * @param int $numeric  Wikidata.comparison
	 * @return "success"|"warning"|"error"|"info"
	 */
	public static function getCompLevel(int $numeric): string
	{
		if (isset(self::COMP_LEVELS[$numeric])) {
			return self::COMP_LEVELS[$numeric];
		}
		return 'error';
	}

	public function compareInexistent(): void
	{
		$this->vecho(1, "Revue inexistante...\n");
		$timestart = microtime(true);
		$tr = Yii::app()->db->beginTransaction();
		$sql = "SELECT W.id, qId, revueId, MAX(I.id) AS interventionId "
			. "FROM Wikidata W LEFT JOIN Intervention I USING (revueId) "
			. "WHERE revueId NOT IN (SELECT id FROM Revue) GROUP BY W.id";
		$rows = Yii::app()->db->createCommand($sql)->queryAll();

		foreach ($rows as $row) {
			$this->setConsistencyErrorByQid($row['qId'], self::COMP_INEXISTENT, "revueId=" . $row['revueId']);
		}
		$tr->commit();
		$dtime = microtime(true) - $timestart;
		$this->vecho(1, sprintf("    %5d  %6.3f s.\n", count($rows), $dtime));
	}

	/**
	 * Comparaisons complémentaires
	 */
	public function compareComplement(): void
	{
		$this->vecho(1, "Revues multiples...\n");
		$timestart = microtime(true);
		$sql1 = "SELECT qId, COUNT(DISTINCT revueId) AS cnt, GROUP_CONCAT(DISTINCT revueId) AS gc "
			. "FROM Wikidata WHERE comparison IS NULL GROUP BY qId HAVING cnt>1";
		$rows = Yii::app()->db->createCommand($sql1)->queryAll();
		foreach ($rows as $row) {
			$this->setConsistencyErrorByQid($row['qId'], self::COMP_MULTIPLE_REVUES, "revueId=" . $row['gc']);
		}
		$dtime = microtime(true) - $timestart;
		$this->vecho(1, sprintf("    %5d  %6.3f s.\n", count($rows), $dtime));
		
		$this->vecho(1, "Éléments multiples...\n");
		$timestart = microtime(true);
		$sql2 = "SELECT revueId, COUNT(DISTINCT qId) AS cnt, GROUP_CONCAT(DISTINCT qId) AS gc "
			. "FROM Wikidata WHERE comparison IS NULL GROUP BY revueId HAVING cnt>1";
		$rows = Yii::app()->db->createCommand($sql2)->queryAll();
		foreach ($rows as $row) {
			$this->setConsistencyErrorByRevue($row['revueId'], self::COMP_MULTIPLE_QIDS, "qId=" . $row['gc']);
		}
		$dtime = microtime(true) - $timestart;
		$this->vecho(1, sprintf("    %5d  %6.3f s.\n", count($rows), $dtime));

		$this->vecho(1, "Titre porteur obsolète...\n");
		$timestart = microtime(true);
		$sql3 = "UPDATE Wikidata W JOIN Titre T ON (W.titreId=T.id) SET comparison = :c, compQid = :qid"
			. " WHERE W.comparison IS NULL AND T.obsoletePar IS NOT NULL";
		Yii::app()->db->createCommand($sql3)->execute([':c' => self::COMP_TITRE_OLD, ':qid' => self::COMP_TITRE_OLD]);
		$dtime = microtime(true) - $timestart;
		$this->vecho(1, sprintf("    %6.3f s.\n", $dtime));
	}

	/**
	 * Dans la table Wikidata, met à jour les champs titreId devinables à partir d'un ISSN
	 * et repère deux erreurs de cohérence possibles
	 */
	public function updateTitres(): void
	{
		$this->vecho(1, "Met à jour Wikidata.titreId à partir d'un ISSN...\n");
		$timestart = microtime(true);
		Yii::app()->db->createCommand("CREATE INDEX IF NOT EXISTS tmp_prop_value ON Wikidata (`property`, `value` (10));")->execute();
		$this->detectTitreIds();
		$this->detectMultipleTitres();

		$sql = "SELECT qId, COUNT(DISTINCT I.titreId) AS nbtitres FROM Wikidata W "
			. "LEFT JOIN Issn I ON (I.issn = W.value AND W.property='P236') WHERE W.titreId=0 "
			. "GROUP BY qId HAVING nbtitres=0";
		$rows = Yii::app()->db->createCommand($sql)->queryAll();

		foreach ($rows as $row) {
			$this->setConsistencyErrorByQid($row['qId'], self::COMP_ISSN_INCONSISTENT, '');
			$this->vecho(2, "0");
		}

		Yii::app()->db->createCommand("DROP INDEX IF EXISTS tmp_prop_value ON Wikidata")->execute();
		$dtime = microtime(true) - $timestart;
		$this->vecho(1, sprintf("    %5d  %6.3f s.\n", count($rows), $dtime));
	}

	/**
	 * pour les erreurs globales (comp > self::COMP_INCONSISTENCIES),
	 * on ne garde que la propriété ISSN (P236) pour alimenter les logs
	 */
	public function cleanupProperties(): void
	{
		$sql = "DELETE FROM Wikidata WHERE comparison >= :c AND property <> :p";
		Yii::app()->db->createCommand($sql)->execute([':c' => self::COMP_INCONSISTENCIES_CRIT, ':p' => 'P236']);
	}

	/**
	 * Met à jour le champ titreId quand on peut le déterminer sans erreur à partir des ISSN
	 */
	private function detectTitreIds(): int
	{
		$sql = <<<SQLSetTitres
			UPDATE Wikidata
				JOIN (
					SELECT W.qId, MAX(I.TitreId) AS titreId
					FROM Wikidata W
					  JOIN Issn I ON (I.issn = W.value AND W.property = 'P236')
					GROUP BY W.qId
					HAVING MIN(I.TitreId) = MAX(I.TitreId)
				) AS SingleTitle USING(qId)
			SET Wikidata.titreId = SingleTitle.titreId
			SQLSetTitres;
		return Yii::app()->db->createCommand($sql)->execute();
	}

	private function detectMultipleTitres(): int
	{
		$sql = <<<SQLMultipleTitres
			UPDATE Wikidata
				JOIN (
					SELECT W.qId, GROUP_CONCAT(DISTINCT I.titreId) AS titreids, COUNT(DISTINCT I.titreId) AS nbtitres
					FROM Wikidata W
					  JOIN Issn I ON (I.issn = W.value AND W.property = 'P236')
					GROUP BY W.qId
					HAVING nbtitres > 1
				) AS Wanalyse USING(qId)
			SET comparison = :c, compQid = :qid, compDetails = Wanalyse.titreids, compDate = :date
			SQLMultipleTitres;
		return Yii::app()->db->createCommand($sql)->execute(
			[':c' => self::COMP_MULTIPLE_TITLES, ':qid' => self::COMP_MULTIPLE_TITLES, ':date' => $this->time]
		);
	}

	private function setConsistencyErrorByQid(string $qid, int $compCode, string $compDetails): void
	{
		$sql = "UPDATE Wikidata SET comparison = :c, compDetails = :d, compDate = :date, compQid = :qid WHERE qId = :id";
		Yii::app()->db->createCommand($sql)->execute([':c' => $compCode, ':d' => $compDetails, ':date' => $this->time, ':qid' => $compCode, ':id' => $qid]);
	}

	private function setConsistencyErrorByRevue(string $revueId, int $compCode, string $compDetails): void
	{
		$sql = "UPDATE Wikidata SET comparison = :c, compDetails = :d, compDate = :date, compQid = :qid WHERE revueId = :rid";
		Yii::app()->db->createCommand($sql)->execute([':c' => $compCode, ':d' => $compDetails, ':date' => $this->time, ':qid' => $compCode, ':rid' => $revueId]);
	}

}
