<?php

/**
 * This is the model class for table "Partenaire_Titre".
 *
 * The followings are the available columns in table 'Partenaire_Titre':
 * @property int $partenaireId
 * @property int $titreId
 * @property string $identifiantLocal
 * @property string $bouquet
 *
 * @property Titre $titre
 * @property Partenaire $partenaire
 */
class Possession extends CActiveRecord
{
	public const IMPORT_ADDED = 1;

	public const IMPORT_NOCHANGE = 2;

	public const IMPORT_UPDATED = 3;

	public const IMPORT_DELETED = 4;

	public const IMPORT_NOTFOUND = 5;

	public const IMPORT_ERROR = 6;

	public const IMPORT_INSERT_NOTALLOWED = 7;

	public const IMPORT_UPDATE_NOTALLOWED = 8;

	private const MESSAGES_PER_CODE = [
		self::IMPORT_NOTFOUND => "Suppression d'une possession inexistante",
		self::IMPORT_ERROR => "Erreur de validation / SQL",
		self::IMPORT_ADDED => "Ajout",
		self::IMPORT_NOCHANGE => "Aucun changement",
		self::IMPORT_UPDATED => "Mise à jour",
		self::IMPORT_DELETED => "Suppression",
		self::IMPORT_INSERT_NOTALLOWED => "Création non autorisée",
		self::IMPORT_UPDATE_NOTALLOWED => "Modification non autorisée",
	];

	public $overwrite = false;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Partenaire_Titre';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['partenaireId, titreId', 'required'],
			['partenaireId, titreId', 'numerical', 'integerOnly' => true],
			['identifiantLocal, bouquet', 'length', 'max' => 255],
			['overwrite', 'boolean'],
			['partenaireId, titreId, identifiantLocal, bouquet', 'safe', 'on' => 'search'],
		];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'titre' => [self::BELONGS_TO, 'Titre', 'titreId'],
			'partenaire' => [self::BELONGS_TO, 'Partenaire', 'partenaireId'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'partenaireId' => 'Partenaire',
			'titreId' => 'Titre',
			'identifiantLocal' => 'Identifiant Local',
			'bouquet' => 'Bouquet',
			'overwrite' => "Modification de l'existant",
		];
	}

	/**
	 * @return self[]
	 */
	public static function groupByBouquet(int $partenaireId): array
	{
		return self::model()->with('titre')
			->findAllByAttributes(
				['partenaireId' => $partenaireId],
				['order' => 'bouquet ASC, titre.titre ASC']
			);
	}

	/**
	 * Imports this possession for a given Partenaire.
	 */
	public function importAdd(bool $simulation): int
	{
		$existing = Possession::model()->findByAttributes(
			['partenaireId' => $this->partenaireId, 'titreId' => $this->titreId]
		);

		if (empty($existing)) {
			// INSERT
			$action = self::IMPORT_ADDED;
		} else {
			// UPDATE
			if (
				$existing->identifiantLocal == $this->identifiantLocal
				&& $existing->bouquet == $this->bouquet
			) {
				return self::IMPORT_NOCHANGE;
			}
			if (!$this->overwrite) {
				return self::IMPORT_UPDATE_NOTALLOWED;
			}
			$this->setIsNewRecord(false);
			$action = self::IMPORT_UPDATED;
		}
		if (!$simulation) {
			if (!$this->save()) {
				return self::IMPORT_ERROR;
			}
		}
		return $action;
	}

	/**
	 * Deletes this possession for a given Partenaire.
	 */
	public function importDelete(bool $simulation): int
	{
		$existing = Possession::model()->findByAttributes(
			['partenaireId' => $this->partenaireId, 'titreId' => $this->titreId]
		);

		if (empty($existing)) {
			return self::IMPORT_NOTFOUND;
		}
		if (!$simulation) {
			if ($existing->delete() != 1) {
				return self::IMPORT_ERROR;
			}
		}
		return self::IMPORT_DELETED;
	}

	public static function getMsgFromCode(int $code)
	{
		return self::MESSAGES_PER_CODE[$code] ?? '';
	}
}
