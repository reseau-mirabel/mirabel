<?php

namespace models\sudoc;

use components\Curl;
use Issn;
use models\lang\Normalize;
use models\marcxml\MarcTitre;
use models\marcxml\Parser as MarcxmlParser;

class ApiClient
{
	private Curl $curl;

	public function __construct(?Curl $curl = null)
	{
		if ($curl) {
			$this->curl = $curl;
		} else {
			$this->curl = new Curl();
		}
		$this->curl->setopt(CURLOPT_TIMEOUT, 10);
	}

	/**
	 * Return a MarcTitre by quering the MarcXML API of Sudoc
	 */
	public function getMarcTitre(string $ppn): ?MarcTitre
	{
		if (!$ppn) {
			return null;
		}
		$xml = $this->getXmlNotice($ppn);
		if (!$xml) {
			return null;
		}

		$parser = new MarcxmlParser();
		$marc = new MarcTitre();
		$parser->parse($xml, $marc);
		return $marc;
	}

	/**
	 * Return a Sudoc Notice by querying the MarcXML API of Sudoc.
	 */
	public function getNotice(string $ppn): ?Notice
	{
		$marc = $this->getMarcTitre($ppn);
		if ($marc === null) {
			return null;
		}

		$notice = new Notice();
		$notice->fromMarcXml($marc);

		return $notice;
	}

	/**
	 * Return a Sudoc Notice (first one matching) by querying the ISSN2PPN API then the MarcXML API.
	 */
	public function getNoticeByIssn(string $issn): ?Notice
	{
		$ppns = $this->getPpn($issn);
		if (!$ppns) {
			return null;
		}
		// take the first valid notice from the PPN list
		foreach ($ppns as $ppn) {
			$notice = $this->getNotice($ppn->ppn);
			if ($notice !== null) {
				return $notice;
			}
		}
		// ISSN, but empty response for every PPN => no holding
		$noticeNoHolding = new Notice();
		$noticeNoHolding->issn = $issn;
		$noticeNoHolding->support = Issn::SUPPORT_INCONNU;
		$noticeNoHolding->ppn = $ppns[0]->ppn;
		$noticeNoHolding->sudocNoHolding = true;
		return $noticeNoHolding;
	}

	/**
	 * Return a list of sudoc\Ppn associated with this ISSN.
	 *
	 * @param string $issn
	 * @throws \Exception
	 * @return Ppn[]
	 */
	public function getPpn(string $issn): array
	{
		$xml = $this->issn2ppn($issn);
		if (!$xml) {
			return [];
		}
		return $this->extractPpn($xml);
	}

	/**
	 * @return Ppn[]
	 */
	protected function extractPpn(string $xml): array
	{
		$result = [];
		$dom = new \DOMDocument();
		$dom->loadXML($xml);
		foreach ($dom->getElementsByTagName("result") as $node) {
			assert($node instanceof \DOMElement);
			if ($node->childNodes->length > 0) {
				foreach ($node->childNodes as $subnode) {
					if ($subnode->nodeType !== XML_ELEMENT_NODE) {
						continue;
					}
					assert($subnode instanceof \DOMElement);
					if ($subnode->tagName === 'ppn') {
						$result[] = new Ppn(trim($subnode->textContent), false);
					}
				}
			}
		}
		foreach ($dom->getElementsByTagName("resultNoHolding") as $node) {
			assert($node instanceof \DOMElement);
			if ($node->childNodes->length > 0) {
				foreach ($node->childNodes as $subnode) {
					if ($subnode->nodeType !== XML_ELEMENT_NODE) {
						continue;
					}
					assert($subnode instanceof \DOMElement);
					if ($subnode->tagName === 'ppn') {
						$result[] = new Ppn(trim($subnode->textContent), true);
					}
				}
			}
		}
		return $result;
	}

	/**
	 * Returns the Sudoc PPN number, using the ISSN and a webservice.
	 *
	 * @param string $issn
	 * @return string XML
	 */
	private function issn2ppn(string $issn): string
	{
		if (!$issn) {
			return "";
		}
		[$code, $response] = $this->download("https://www.sudoc.fr/services/issn2ppn/$issn");
		if ($code === 404) {
			return "";
		}
		return $response;
	}

	private function getXmlNotice(string $ppn): string
	{
		[$code, $response] = $this->download("https://www.sudoc.fr/$ppn.xml");
		if ($code === 404) {
			return "";
		}
		return $response;
	}

	/**
	 * @return list{int, string}
	 */
	private function download(string $url): array
	{
		try {
			$response = $this->curl->get($url)->getContent();
		} catch (\Throwable $e) {
			throw new \Exception("Sudoc, erreur HTTP : {$e->getMessage()}");
		}
		$code = $this->curl->getHttpCode();
		if ($code === 404) {
			return [404, ''];
		}
		return [$code, $response];
	}
}
