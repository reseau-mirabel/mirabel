<?php

namespace models\sudoc;

use Issn;
use models\marcxml\GeneralInfo;
use models\marcxml\MarcTitre;

class Notice
{
	/**
	 * @var ?string
	 */
	public $bnfArk;

	/**
	 * @var ?string
	 */
	public $dateDebut;

	/**
	 * @var ?string
	 */
	public $dateFin;

	/**
	 * @var ?string
	 */
	public $issn;

	/**
	 * @var ?string
	 */
	public $issnl;

	/**
	 * @var ?string e.g. "fre, eng"
	 */
	public $langues;

	/**
	 * @var string 2 uppercase letters
	 */
	public string $pays = '';

	/**
	 * @var ?string
	 */
	public $ppn;

	/**
	 * @var ?int See Issn::STATUT_*
	 */
	public $statut;

	public bool $sudocNoHolding = false;

	public string $support = '';

	/**
	 * @var ?string
	 */
	public $titre;

	/**
	 * @var ?string
	 */
	public $titreIssn;

	/**
	 * @var array[]
	 */
	public array $titreVariantes = [];

	/**
	 * @var ?string
	 */
	public $url;

	/**
	 * @var ?string
	 */
	public $worldcat;

	private array $log = [];

	private static $paysCorrespondance = [];

	private ?MarcTitre $marc = null;

	public function fromMarcXml(MarcTitre $marc): void
	{
		$this->log = [];

		$this->marc = $marc;

		$this->ppn = $marc->getControl(1);
		$this->sudocNoHolding = self::getSudocNoHolding($marc);

		$this->bnfArk = self::getBnfArk($marc);

		$info = $marc->getGeneralInfo();
		$this->dateDebut = self::normalizeDate((string) $info->yearStart);
		$this->dateFin = self::normalizeDate((string) $info->yearEnd);

		$this->support = $this->getSupport($info);

		$issn = $marc->getIssn();
		if ($issn) {
			$this->issn = $issn->issn;
			$this->issnl = $issn->issnl;
			$this->titreIssn = $issn->title;
			$this->statut = $issn->status;
		}

		$this->langues = $this->getLangCodes($marc);

		$this->pays = self::normalizeCountry($this->getCountry($marc));

		$title = $marc->getTitle();
		$this->titre = $title ? $title->title : null;
		$this->titreVariantes = $this->findTitleVariants($marc);

		$urls = $marc->getUrls();
		if ($urls) {
			$this->url = $marc->getUrls()[0];
		}

		$this->worldcat = $marc->getOtherSystemsControlNumber("OCoLC");

		$this->writeLogFile();
	}

	public function getLog(): array
	{
		return $this->log;
	}

	public function getMarc(): ?MarcTitre
	{
		return $this->marc;
	}

	private function findTitleVariants(MarcTitre $marc): array
	{
		// tmp
		$xData = $marc->getDataField("520", ["0", "1", " "], " ");
		if ($xData && !empty($xData[0]['x'])) {
			$this->log[] = "{$this->ppn} ; {$this->issn} ; " . sprintf('520$x non vide : "%s"\n', $xData[0]['x']);
		}

		// default lang which will applyto all titles unless the local context has another lang
		$langData = $marc->getDataField("101", ["0", "1", "2", " "], " ");
		$defaultLang = '   ';
		$defaultLangZone = '';
		if ($langData) {
			$defaultLang = $langData[0]['g'] ?? $langData[0]['f'] ?? $langData[0]['a'] ?? '   ';
			$defaultLangZone = '101$'
				. (isset($langData[0]['g']) ? 'g'
				: (isset($langData[0]['f']) ? 'f'
				: (isset($langData[0]['a']) ? 'a' : ' ')
				));
			if (count($langData) > 1) {
				$this->log[] = sprintf("{$this->ppn} ; {$this->issn} ; Plusieurs champs de langue en %s, %s\n", $defaultLangZone, json_encode($langData));
			}
			if (is_array($defaultLang)) {
				$this->log[] = sprintf("{$this->ppn} ; {$this->issn} ; Langue multiple en %s, %s\n", $defaultLangZone, json_encode($defaultLang));
				$defaultLang = $defaultLang[0];
			}
		}

		$variants = [];

		// Reference Title
		$refTitle = $marc->getDataField("200", ["0", "1", " "], " ");
		if ($refTitle && isset($refTitle[0]['a'])) {
			$variants[] = [
				'variante' => is_array($refTitle[0]['a']) ? $refTitle[0]['a'][0] : $refTitle[0]['a'], // ignore subtitles
				'lang' => $defaultLang,
				'zone' => '200$a',
				'zoneLang' => $defaultLangZone,
			];
		}

		// Read the titles, and add them with their local lang (or the default lang if not set locally)
		$zones = [500, 501, 503, 510, 512, 513, 514, 515, 516, 517, 518, 520, 530, 531, 532, 540, 541, 545, 579];
		foreach ($zones as $zone) {
			foreach (["0", "1", " ", "|"] as $indicateur1) {
				$data = $marc->getDataField("$zone", [$indicateur1], " ");
				if (!$data) {
					continue;
				}
				$strIndic = $indicateur1 === ' ' ? '#' : $indicateur1;
				foreach ($data as $d) {
					if (isset($d['z'])) {
						$this->log[] = sprintf("{$this->ppn} ; {$this->issn} ; Langue spécifique en zone %s : %s\n", "$zone.{$strIndic}#\$z", json_encode($d['z']));
					}
					$lang = $d['z'] ?? $defaultLang;
					if (!empty($d['a'])) {
						$variants[] = [
							'variante' => is_array($d['a']) ? $d['a'][0] : $d['a'], // ignore subtitles
							'lang' => $lang,
							'zone' => "$zone.{$strIndic}#\$a",
							'zoneLang' => isset($d['z']) ? "$zone\$z" : ($defaultLang ? $defaultLangZone : ''),
						];
					}
					if (!empty($d['e'])) {
						$variants[] = [
							'variante' => is_array($d['e']) ? $d['e'][0] : $d['e'], // ignore subtitles
							'lang' => $lang,
							'zone' => "$zone.{$strIndic}#\$e",
							'zoneLang' => isset($d['z']) ? "$zone\$z" : ($defaultLang ? $defaultLangZone : ''),
						];
					}
				}
			}
		}
		return $variants;
	}

	/**
	 * A MarcXML notice has a localization (returns false) if:
	 * - it has a 002 control field
	 * - or it has a 930$5 field
	 */
	private static function getSudocNoHolding(MarcTitre $marc): bool
	{
		if (!empty($marc->getControl(2))) {
			// localisation
			return false;
		}
		$data = $marc->getDataField("930", [" "], " ");
		if (!$data) {
			// no localisation
			return true;
		}
		foreach ($data as $d) {
			if ($d["5"]) {
				// localisation (the missing control2 is a data bug)
				return false;
			}
		}
		return true;
	}

	private function getSupport(GeneralInfo $info) : string
	{
		switch ($info->resourceType) {
			case GeneralInfo::RESOURCETYPE_ELECTRONIC:
				return Issn::SUPPORT_ELECTRONIQUE;
			case GeneralInfo::RESOURCETYPE_PRINTED:
				return Issn::SUPPORT_PAPIER;
			case GeneralInfo::RESOURCETYPE_MULTIMEDIA:
				$this->log[] = "{$this->ppn} ; ; Ce PPN a le type de ressource 'multimedia'\n";
				return Issn::SUPPORT_INCONNU;
			default:
				return Issn::SUPPORT_INCONNU;
		}
	}

	private static function getBnfArk(MarcTitre $marc): ?string
	{
		$data = $marc->getDataField("033", [" "], " ");
		$m = [];
		foreach ($data as $d) {
			if (!empty($d["a"]) && preg_match('#^https://catalogue.bnf.fr/ark:/12148/([^/]+)$#', $d["a"], $m)) {
				return $m[1];
			}
		}
		return null;
	}

	/**
	 * normalisation pour les codes de DOM-TOM cf. https://tickets.silecs.info/view.php?id=4819
	 * @param string $country
	 * @return string
	 */
	private function normalizeCountry(string $country): string
	{
		if (empty(self::$paysCorrespondance)) {
			self::$paysCorrespondance = \Config::read('pays.correspondance');
		}
		return self::$paysCorrespondance[$country] ?? $country;
	}

	/**
	 * 2 letters country code, upper case. See http://documentation.abes.fr/sudoc/formats/unmb/zones/102.htm
	 *
	 * @return string "" if unknown country, "ZZ" if multiple, else the Alpha2 of a country
	 */
	private function getCountry(MarcTitre $marc): string
	{
		$data = $marc->getDataField("102", [" "], " ");
		if (empty($data[0]["a"])) {
			return "";
		}
		if ($data[0]["a"] === 'XX') {
			// unknown country
			return "";
		}
		// special value 'ZZ' (multiple countries) is kept
		$code = $data[0]["a"];
		if (is_array($code)) {
			$code = array_unique(array_filter($code));
			if (count($code) > 1) {
				$this->log[] = sprintf("%s ; %s ; pays %s %s\n", $marc->getControl(1), $this->issn, $code[0], $code[1]);
			}
			return $code[0];
		}
		return $code;
	}

	/**
	 * Convert the array of ISO 639-2 (MarcXML) into a string of ISO 639-3.
	 */
	private function getLangCodes(MarcTitre $marc): ?string
	{
		$langs = \models\lang\Normalize::filter($marc->getLangCodes());
		return $langs ? join(", ", $langs) : null;
	}

	private function writeLogFile(): void
	{
		if (!$this->log) {
			return;
		}
		$dir = rtrim(\Yii::app()->getRuntimePath(), '/');
		error_log(date('Y-m-d H:i:s') . "\n" . join("\n", $this->log) . "\n\n", 3, "$dir/sudoc-log.txt");
		@chmod("$dir/sudoc-log.txt", 0666);
	}

	private static function normalizeDate(?string $date) : string
	{
		if (!$date || \strlen(\trim($date)) < 4) {
			return "";
		}
		$x = str_replace(['?', '.'], ['X', 'X'], $date);
		if (!preg_match('/^\d\d/', $x)) {
			return "";
		}
		return $x;
	}
}
