<?php

namespace models\sudoc;

class Ppn
{
	public string $ppn;

	public bool $noHolding;

	public function __construct(string $ppn, bool $noHolding)
	{
		$this->ppn = $ppn;
		$this->noHolding = $noHolding;
	}
}
