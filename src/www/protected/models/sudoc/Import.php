<?php

namespace models\sudoc;

use Issn;
use Titre;
use Yii;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Box\Spout\Common\Entity\Row;
use models\import\ImportType;

class Import
{
	public $simulation = false;

	public $verbosity = 0;

	/**
	 * Read a XLSX file and write a summary into another XLSX file.
	 *
	 * Read from columns A, B, C and write into D (only if column B can be parsed (cf extractIssnSupport()).
	 * See Mantis #4513.
	 */
	public function file(string $fileIn, string $fileOut): void
	{
		$reader = ReaderEntityFactory::createXLSXReader();
		$reader->setShouldPreserveEmptyRows(true);
		$reader->open($fileIn);

		$writer = WriterEntityFactory::createXLSXWriter();
		$writer->setShouldCreateNewSheetsAutomatically(true);
		$writer->openToFile($fileOut);

		$sheet = null;
		foreach ($reader->getSheetIterator() as $s) {
			$sheet = $s;
			break;
		}

		$position = 0;
		foreach ($sheet->getRowIterator() as $row) {
			$newRow = $this->processRow($row);
			/** @var \Box\Spout\Common\Entity\Row $newRow */
			$writer->addRow($newRow);

			$position++;
			if ($position > 1000) {
				break;
			}
		}

		$reader->close();
		$writer->close();
	}

	/**
	 * Extract the ISSN and support from the C column.
	 * @param string $colC
	 * @return list{string, string} [ISSN, support]
	 */
	private static function extractIssnSupport(string $colC): array
	{
		$m = [];
		if (preg_match('/^([epP])-issn.*:\s+(\d{4}-\d{3}[\dX])\b/', $colC, $m)) {
			return [
				$m[2],
				($m[1] === 'e' ? Issn::SUPPORT_ELECTRONIQUE : Issn::SUPPORT_PAPIER),
			];
		}
		if (preg_match('/^issn de la version imprimée :\s+(\d{4}-\d{3}[\dX])\b/i', $colC, $m)) {
			return [$m[1], Issn::SUPPORT_PAPIER];
		}
		if (preg_match('/^issn de la version électronique :\s+(\d{4}-\d{3}[\dX])\b/i', $colC, $m)) {
			return [$m[1], Issn::SUPPORT_ELECTRONIQUE];
		}
		// Strict parsing failed, heuristic starts
		$issn = '';
		if (preg_match('/\b(\d{4}-\d{3}[\dX])\b/i', $colC, $m)) {
			$issn = strtoupper($m[1]);
		}
		$support = '';
		if (preg_match('/\b(?:imprimée?|papier)\b/i', $colC, $m)) {
			$support = Issn::SUPPORT_PAPIER;
		}
		if (preg_match('/\bélectronique\b/i', $colC, $m)) {
			if ($support === '') {
				$support = Issn::SUPPORT_PAPIER;
			} else {
				$support = '';
			}
		}
		return [$issn, $support];
	}

	private function processRow(Row $row): Row
	{
		if ($row->getNumCells() < 3) {
			return $row;
		}
		$journalName = trim($row->getCellAtIndex(0)->getValue());
		if (empty($journalName)) {
			return $row;
		}
		$issnExisting = trim($row->getCellAtIndex(1)->getValue());
		if (empty($issnExisting)) {
			return $row;
		}
		$comment = trim($row->getCellAtIndex(2)->getValue());

		// issne
		[$issnMissing, $support] = self::extractIssnSupport($comment);
		if (!$issnMissing || !$support) {
			$this->echo(2, "IGNORE $issnExisting : $comment\n");
			return $row;
		}
		$this->echo(2, "For $issnExisting : found missing ISSN $issnMissing\n");
		$newRow = WriterEntityFactory::createRowFromArray([
			$journalName,
			$issnExisting,
			$comment,
			$this->processIssn($issnExisting, $issnMissing, $support),
		]);
		$boldStyle = (new StyleBuilder())
			->setFontBold()
			->build();
		$result = $newRow->getCellAtIndex(3);
		$result->setStyle($boldStyle);
		return $newRow;
	}

	/**
	 * Add the missing ISSN along the existing ISSN, returning a message describing what has changed.
	 */
	private function processIssn(string $issnExisting, string $issnMissing, string $support): string
	{
		// The missing issn is a duplicate?
		$titre = Titre::model()->findBySql("SELECT t.* FROM Titre t JOIN Issn i ON t.id = i.titreId WHERE i.issn = :issn", [':issn' => $issnExisting]);
		if (!$titre) {
			$this->echo(1, "ERR Le titre censé être associé à l'issn $issnExisting est introuvable\n");
			return "ERR Le titre censé être associé à l'issn $issnExisting est introuvable";
		}
		$exists = Yii::app()->db->createCommand("SELECT titreId FROM Issn WHERE issn = :issn")->queryScalar([':issn' => $issnMissing]);
		if ($exists) {
			if ($exists == $titre->id) {
				$this->echo(1, "INFO Cet issn $issnMissing est déjà associé à ce même titre\n");
				return "INFO Cet issn est déjà associé à ce même titre";
			}
			$this->echo(1, "ERR Cet issn $issnMissing est présent mais associé à un autre titre\n");
			return "WARN Cet issn est présent mais associé à un autre titre";
		}

		if ($this->simulation) {
			$this->echo(1, "INFO L'issn $issnMissing sera ajouté au titre {$titre->id}, revue {$titre->revueId}\n");
			return "INFO L'issn sera ajouté au titre {$titre->id}, revue {$titre->revueId}\n";
		}

		// insert the missing issn
		$intervention = $this->createIntervention($titre);
		$issnOldRecord = Issn::model()
			->findBySql(
				"SELECT * FROM Issn WHERE titreId = {$titre->id} AND issn = '' AND support = :se",
				[':se' => Issn::SUPPORT_ELECTRONIQUE]
			);
		if ($issnOldRecord instanceof Issn) {
			$issnOldRecord->setScenario('import');
			$issnRecord = clone $issnOldRecord;
		} else {
			$issnRecord = new Issn('import');
			$issnRecord->titreId = (int) $titre->id;
		}
		$issnRecord->issn = $issnMissing;
		$issnRecord->fillBySudoc();
		if (!$issnRecord->sudocPpn) {
			$this->echo(1, "INFO Cet issn $issnMissing n'a pas de PPN\n");
			$validator = new \models\validators\IssnValidator();
			if (!$validator->validateString($issnRecord->issn)) {
				return "ERR Issn non valide : " . join(" & ", $validator->getErrors());
			}
		}
		$issnRecord->support = $support;
		if ($issnOldRecord === null) {
			$intervention->contenuJson->create($issnRecord);
		} else {
			$intervention->contenuJson->update($issnOldRecord, $issnRecord);
		}
		if ($intervention->accept()) {
			$this->echo(1, "OK L'issn $issnMissing est ajouté au titre {$titre->id}, revue {$titre->revueId}\n");
			return "OK issn ajouté au titre {$titre->id}, revue {$titre->revueId}"
				. ($issnRecord->sudocPpn ? "" : " (pas de PPN trouvé)");
		}

		$this->echo(
			1,
			"ERR L'issn $issnMissing n'a pu être ajouté au titre {$titre->id}, revue {$titre->revueId}: "
			. print_r($intervention->getErrors(), true) . "\n"
		);
		return "L'issn n'a pu être ajouté au titre {$titre->id}, revue {$titre->revueId}";
	}

	private function createIntervention(Titre $titre)
	{
		$i = new \Intervention();
		$i->setAttributes(
			[
				'ressourceId' => null,
				'revueId' => $titre->revueId,
				'titreId' => $titre->id,
				'editeurId' => null,
				'statut' => 'attente',
				'import' => ImportType::getSourceId('SUDOC'),
				'ip' => '',
				'contenuJson' => new \InterventionDetail(),
				'suivi' => null !== \Suivi::isTracked($titre),
				'description' => "Modification du titre « {$titre->titre} »",
			],
			false
		);
		return $i;
	}

	private function echo(int $level, string $msg): void
	{
		if ($this->verbosity >= $level) {
			echo $msg;
		}
	}
}
