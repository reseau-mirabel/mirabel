<?php

namespace models\sphinx;

/**
 * Champs uniquement pour MATCH() en recherche :
 *   sigle, titrerecherche
 *
 * @property int $id Titre.id
 * @property int[] $abonnement [Partenaire.id]
 * @property int[] $acces [type] avec 1 <= type <= 5
 * @property int[] $acceslibre [type] avec 1 <= type <= 5
 * @property int[] $attribut [Sourceattribut.id]
 * @property int[] $categorie [Categorie.id]
 * @property int $cletri
 * @property int[] $collectionid
 * @property int[] $detenu [Partenaire.id]
 * @property int[] $editeurid Y compris les anciens éditeurs
 * @property int[] $editeuractuelid
 * @property int[] $grappe [Grappe.id]
 * @property int[] $issn avec la valeur numérique de l'ISSN hors clé
 * @property int[] $langue Each value is the 3-letter code converted from base 36 to base 10
 * @property int $lettre1
 * @property int[] $lien [Sourcelien.id]
 * @property int $nbacces
 * @property bool $obsolete
 * @property int $paysid
 * @property int[] $ressourceid
 * @property int[] $revuecategorie [Categorie.id]
 * @property int[] $revuecategorierec [Categorie.id]
 * @property int $revueid
 * @property bool $revuesanslangue
 * @property int[] $suivi array of Partenaire.id
 * @property string $titrecomplet
 * @property bool $vivant
 * @property int $hdatemodif timestamp
 * @property int $hdateverif timestamp
 *
 */
class Titres extends \CActiveRecord
{
	use Shared;

	public const ACCES_OTHER = 1;

	public const ACCES_INTEGRAL = 2;

	public const ACCES_RESUME = 3;

	public const ACCES_SOMMAIRE = 4;

	public const ACCES_INDEXATION = 5;

	protected const INTARRAY_FIELDS = [
		'abonnement', 'acces', 'acceslibre', 'attribut', 'categorie', 'collectionid', 'detenu',
		'editeuractuelid', 'editeurid', 'grappe', 'issn', 'lien', 'ressourceid', 'revuecategorie', 'suivi',
	];

	private ?\Titre $record = null;

	public function tableName()
	{
		return '{{titres}}';
	}

	public function getRecord(): ?\Titre
	{
		if ($this->record === null && $this->id > 0) {
			$this->record = \Titre::model()->findByPk($this->id);
		}
		return $this->record;
	}

	public function setRecord(\Titre $titre): void
	{
		$this->record = $titre;
	}

	/**
	 * @return string[] 3 letters codes
	 */
	public function getLangues(): array
	{
		$languesStr = $this->getAttribute('langue');
		if (!$languesStr) {
			return [];
		}
		$languesInt = explode(',', $languesStr);
		$langues = [];
		foreach ($languesInt as $l) {
			$langues[] = base_convert($l, 10, 36);
		}
		return $langues;
	}
}
