<?php

namespace models\sphinx;

use CDbConnection;
use CDbException;
use Yii;

trait Shared
{
	public $pageVar = "page";

	/**
	 * @inheritDoc CActiveRecord
	 *
	 * Le pilote pdo/mysql ne permet pas de valeurs de type tableau,
	 * donc les champs int[] sont reçus sous forme de texte "15,22,45".
	 */
	public function __get($name)
	{
		$value = parent::__get($name);
		if (is_string($value) && in_array($name, self::INTARRAY_FIELDS)) {
			return array_map('intval', explode(',', $value));
		}
		return $value;
	}

	/**
	 * @inheritDoc CActiveRecord
	 * @throws CDbException
	 * @return CDbConnection
	 */
	public function getDbConnection()
	{
		static $db = null;
		if ($db === null || $db->get() === null) {
			$db = Yii::app()->getComponent('sphinx');
			if (!($db instanceof CDbConnection)) {
				throw new CDbException("Le composant 'sphinx' ne donne pas de connexion.");
			}
			$db = \WeakReference::create($db);
		}
		return $db->get();
	}

	/**
	 * @return int[]
	 */
	public function getSuiviIds(): array
	{
		$suivi = $this->getAttribute('suivi');
		if (empty($suivi)) {
			return [];
		}
		if (is_array($suivi)) {
			return $suivi;
		}
		return array_map('intval', explode(',', $suivi));
	}
}
