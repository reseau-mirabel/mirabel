<?php

namespace models\sphinx;

/**
 * @property int $id Editeur.id
 * @property int $cletri
 * @property int $lettre1
 * @property int $nbrevues
 * @property int $nbtitresmorts
 * @property int $nbtitresvivants
 * @property string $nomcomplet
 * @property int $paysid
 * @property int[] $suivi array of Partenaire.id
 * @property int $hdatemodif timestamp
 * @property int $hdateverif timestamp
 */
class Editeurs extends \CActiveRecord
{
	use Shared;

	protected const INTARRAY_FIELDS = [
		'abonnement', 'acces', 'acceslibre', 'categorie', 'collectionid', 'detenu', 'editeurid', 'editeuractuelid',
		'issn', 'lien', 'ressourceid', 'revuecategorie', 'suivi', 'attribut', 'grappe',
	];

	/**
	 * @var ?\Editeur
	 */
	private $record;

	public function tableName()
	{
		return '{{editeurs}}';
	}

	public function getRecord(): ?\Editeur
	{
		if ($this->record === null && $this->id > 0) {
			$this->record = \Editeur::model()->findByPk($this->id);
		}
		return $this->record;
	}

	public function setRecord(\Editeur $editeur): void
	{
		$this->record = $editeur;
	}
}
