<?php

namespace models\sphinx;

/**
 * Search only fields, for MATCH():
 *   sigle, description, notecontenu, disciplines, collections
 *
 * @property int $id Ressource.id
 * @property bool $autoimport
 * @property bool $autoimportcollections
 * @property bool $exhaustif
 * @property bool $exhaustifcollections
 * @property int $cletri
 * @property int $lettre1
 * @property int $nbrevues
 * @property string $nomcomplet
 * @property int[] $suivi array of Partenaire.id
 * @property bool $web
 * @property int $hdatemodif timestamp
 * @property int $hdateverif timestamp
 */
class Ressources extends \CActiveRecord
{
	use Shared;

	protected const INTARRAY_FIELDS = [
		'suivi',
	];

	/**
	 * @var ?\Ressource
	 */
	private $record;

	public function tableName()
	{
		return '{{ressources}}';
	}

	public function getRecord(): ?\Ressource
	{
		if ($this->record === null && $this->id > 0) {
			$this->record = \Ressource::model()->findByPk($this->id);
		}
		return $this->record;
	}

	public function setRecord(\Ressource $ressource): void
	{
		$this->record = $ressource;
	}
}
