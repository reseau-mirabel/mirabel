<?php

use components\DateTimeHelper;
use components\SqlHelper;
use components\sphinx\DataProvider;
use processes\search\facets;

/**
 * Searches Titre through Sphinx
 */
class SearchTitre extends CFormModel
{
	public const MAX_IDS = 5000;

	public const MONIT_IGNORE = "";

	public const MONIT_YES = "1";

	public const MONIT_NO = "2";

	public $pid;

	public $titre;

	public $langues;

	public bool $languesEt = false;

	public $issn;

	public $hdateModif;

	public $hdateVerif;

	public $collectionId = 0;

	public $paysId = [];

	public $issnpaysid = 0;

	public $editeurId = [];

	public $aediteurId = [];

	public $ressourceId = [];

	public $categorie = [];

	public $cNonRec = [];

	public $categoriesEt = false;

	public $sanscategorie = false;

	public $suivi = [];

	public $detenu = [];

	public $vivant = null;

	public $acces = [];

	public $accesLibre = false;

	public $sansAcces = false;

	public $abonnement = false;

	public $aboCombine = 'NONE';

	public $attribut = [];

	public $grappe = 0;

	public $lien = [];

	public $owned = false;

	public $monitoredByMe = false;

	public $monitored = self::MONIT_IGNORE;

	/**
	 * @var int Partenaire.id as context for owned and monitored filters
	 */
	private $partenaireId = 0;

	private $attrExclude = [];

	private $filterAbonnement = [];

	private $ts = [];

	public function __construct($partenaireId = null)
	{
		if ($partenaireId > 0) {
			$this->partenaireId = (int) $partenaireId;
		}
		parent::__construct();
	}

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return [
			['titre, langues', 'length', 'max' => 255],
			["monitored", 'length', 'max' => 1],
			['issn', '\models\validators\IssnValidator', 'allowEmptyChecksum' => true, 'allowMissingCaret' => true],
			[
				['acces', 'categorie', 'cNonRec', 'editeurId', 'aediteurId', 'suivi', 'detenu', 'paysId'],
				'ext.validators.ArrayOfIntValidator',
				'allowNegative' => false,
			],
			[
				['attribut', 'lien', 'ressourceId'],
				'ext.validators.ArrayOfIntValidator',
				'allowNegative' => true,
			],
			['aboCombine', 'in', 'range' => ['NONE', 'ET', 'OU']],
			['owned, abonnement, monitoredByMe, accesLibre, sansAcces, categoriesEt, sanscategorie, vivant', 'boolean'],
			['collectionId, pid, grappe, issnpaysid', 'numerical', 'integerOnly' => true],
			['hdateModif, hdateVerif', 'validateDateInput'],
		];
	}

	public function validateDateInput(string $attr): void
	{
		$suffix = str_replace('hdate', '', $attr);
		if (!empty($this->{$attr}) && $this->{$attr} !== '!') {
			$this->ts[$suffix] = DateTimeHelper::parseDateInterval($this->{$attr});
			if ($this->ts[$suffix] === null) {
				$this->addError($attr, 'Format non-valide. Syntaxes possibles : "2010" "< 18/03/2020" ">2020-03".');
			}
		} else {
			$this->ts[$suffix] = null;
		}
	}

	/**
	 * Called automatically before validate().
	 *
	 * @return bool
	 */
	public function beforeValidate()
	{
		if (is_array($this->langues)) {
			$this->langues = join(',', $this->langues);
		}
		foreach (['acces', 'categorie', 'cNonRec', 'editeurId', 'aediteurId', 'ressourceId', 'suivi', 'detenu'] as $k) {
			if (is_string($this->{$k}) && preg_match('/^[\d_-]+$/', $this->{$k})) {
				$this->{$k} = array_map('intval', explode('_', $this->{$k}));
			} elseif (is_int($this->{$k})) {
				$this->{$k} = [$this->{$k}];
			}
		}

		if ($this->pid > 0) {
			$this->partenaireId = (int) $this->pid;
		}

		if ($this->owned) {
			if ($this->partenaireId) {
				$this->detenu = [$this->partenaireId];
			} else {
				$this->addError('owned', "Vous n'avez pas d'établissement de rattachement.");
			}
		}
		if ($this->abonnement) {
			if ($this->partenaireId) {
				$this->filterAbonnement = [$this->partenaireId];
			} else {
				$this->addError('abonnement', "Vous n'avez pas d'établissement de rattachement.");
			}
		}
		if ($this->monitored === self::MONIT_NO) {
			$this->suivi = range(1, 255);
			$this->attrExclude['suivi'] = true;
		} elseif ($this->monitored === self::MONIT_YES) {
			$this->attrExclude['suivi'] = false;
			$this->suivi = range(1, 255);
		}
		if ($this->monitoredByMe) {
			if ($this->partenaireId) {
				$this->attrExclude['suivi'] = false;
				$this->suivi = [$this->partenaireId];
			} else {
				$this->addError('monitoredByMe', "Vous n'avez pas d'établissement de rattachement.");
			}
		}
		if ($this->sanscategorie) {
			$this->categorie = [];
		}
		return parent::beforeValidate();
	}

	/**
	 * Called automatically after validate().
	 *
	 * @return void
	 */
	public function afterValidate()
	{
		if ($this->abonnement && $this->owned && $this->aboCombine === 'NONE') {
			$this->aboCombine = 'ET';
		}
		$this->fillTimestamps();
		foreach (['Modif', 'Verif'] as $name) {
			if (!empty($this->ts[$name]) && !empty($this->ts[$name][1]) && $this->ts[$name][0] > $this->ts[$name][1]) {
				$this->addError("hdate{$name}", "Date de début après la date de fin");
			}
		}
		if (!$this->pid && $this->partenaireId && ($this->monitoredByMe || $this->owned || $this->abonnement)) {
			$this->pid = $this->partenaireId;
		}
		if ($this->grappe) {
			$this->grappe = (int) $this->grappe;
			$grappe = Grappe::model()->findByPk(abs($this->grappe));
			if ($grappe instanceof \Grappe) {
				if (!(\Yii::app() instanceof \CConsoleApplication)) {
					$checkAccess = new \processes\grappe\Permissions(\Yii::app()->user);
					if (!$checkAccess->canRead($grappe)) {
						$this->grappe = 0;
					}
				}
			} else {
				$this->addError('grappe', "Cette grappe n'existe plus dans Mir@bel.");
			}
		}
		if ($this->collectionId > 0 && Collection::model()->findByPk($this->collectionId) === null) {
			$this->addError('collectionId', "Cette collection n'existe plus dans Mir@bel.");
		}
		parent::afterValidate();
	}

	/**
	 * @param "detenu"|"suivi" $attribute
	 */
	public function getPartenairesNames(string $attribute): string
	{
		$value = $this->{$attribute} ?? null;
		if (!$value) {
			return "";
		}
		$names = \Yii::app()->db
			->createCommand("SELECT nom FROM Partenaire WHERE id IN (" . join(',', array_map('intval', $value)) . ")")
			->queryColumn();
		return join(", ", $names);
	}

	/**
	 * Returns a short description of the search criteria.
	 */
	public function getTitleSuffix(): string
	{
		$suffixes = [];
		$sources = ['ressource' => 'ressource', 'éditeur' => 'editeur'];
		foreach ($sources as $print => $name) {
			$field = $name . "Id";
			if ($this->{$field}) {
				if (!is_array($this->{$field})) {
					$ids = (int) $this->{$field};
				} else {
					$ids = join(',', array_map('intval', $this->{$field}));
				}
				$sql = "SELECT IF(sigle!='',sigle,nom)"
					. " FROM " . ucfirst($name) . " WHERE id IN ($ids) ORDER BY nom";
				$names = Yii::app()->db->createCommand($sql)->queryColumn();
				if (!empty($names)) {
					if (count($this->{$field}) == 1) {
						$suffixes[] = "{$print} " . CHtml::encode($names[0]);
					} else {
						$suffixes[] = "{$print}s " . CHtml::encode(join(', ', $names));
					}
				}
			}
		}
		if ($this->collectionId) {
			$c = Collection::model()
				->findByPk($this->collectionId, ['with' => ['ressource']]);
			if ($c) {
				$suffixes[] = $c->getFullName();
			}
		}
		if ($suffixes) {
			return ' : ' . join(' / ', $suffixes);
		}
		return '';
	}

	/**
	 * Converts an ISSN 1234-5678 into a number 1234567.
	 */
	public static function issnToInt(string $issn): int
	{
		$int = substr(str_replace('-', '', $issn), 0, 7);
		return (ctype_digit($int) ? (int) $int : 0);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return [
			'abonnement' => "Mon établissement est abonné en ligne",
			'acces' => 'Accès en ligne',
			'accesLibre' => 'Les accès libres',
			'aediteurId' => "Éditeur actuel ou précédent",
			'attribut' => 'Attribut',
			'categorie' => 'Thématique',
			'collectionId' => 'Collection',
			'editeurId' => 'Éditeur',
			'detenu' => 'Disponible dans cet établissement',
			'hdateModif' => 'Date de modification',
			'hdateVerif' => 'Date de vérification',
			'issn' => 'ISSN',
			'issnpaysid' => "Pays de publication",
			'monitored' => 'Revues suivies…',
			'monitoredByMe' => 'Seulement les revues que mon établissement suit',
			'owned' => 'Les revues disponibles dans mon établissement',
			'paysId' => "Pays de l'éditeur",
			'ressourceId' => 'Ressource',
			'sansAcces' => 'Sans aucun accès en ligne',
			'sanscategorie' => 'Sans thématique',
			'suivi' => 'Suivi par',
			'titre' => 'Titre',
			'vivant' => "Exclure les revues mortes",
		];
	}

	/**
	 * @return Categorie[]
	 */
	public function getCategories(): array
	{
		if ($this->categorie) {
			return SqlHelper::sqlToPairsObject(
				"SELECT * FROM Categorie WHERE id in (" . join(",", array_map('intval', $this->categorie)) . ")",
				"Categorie"
			);
		}
		return [];
	}

	/**
	 * Return an HTML string.
	 */
	public function htmlSummary(): string
	{
		$html = '';
		$formattedCriteria = $this->arraySummary();
		if (empty($formattedCriteria)) {
			return "aucun filtre.";
		}
		foreach ($formattedCriteria as $k => $v) {
			if (is_integer($k)) {
				$html .= '[<em>' . CHtml::encode($v) . '</em>] ';
			} elseif (str_starts_with($v, '<span>')) {
				$html .= '[' . CHtml::encode($k) . " : <em>{$v}</em>] ";
			} else {
				$html .= '[' . CHtml::encode($k) . ' : <em>' . CHtml::encode($v) . '</em>] ';
			}
		}
		return rtrim($html);
	}

	public function exportAttributes(): \processes\search\titres\Params
	{
		return \processes\search\titres\Params::loadFromArray($this->getAttributes());
	}

	public function isEmpty(): bool
	{
		return empty($this->arraySummary());
	}

	public function search(int $pageSize = 25): DataProvider
	{
		$params = $this->exportAttributes();
		$provider = \processes\search\titres\Actor::getDataProvider($params);
		$provider->pagination = new \CPagination($pageSize);
		return $provider;
	}

	/**
	 * Send the search and collect data for facets.
	 */
	public function searchWithFacets(int $pageSize = 25): DataProvider
	{
		$params = $this->exportAttributes();
		$criteria = \processes\search\titres\Actor::createSqlCriteria($params);
		self::addFacets($criteria);

		$sort = new \CSort(\models\sphinx\Titres::class);
		$sort->defaultOrder = 'cletri ASC';

		return new DataProvider(
			\models\sphinx\Titres::model(),
			[
				'criteria' => $criteria,
				'pagination' => ['pageSize' => $pageSize],
				'sort' => $sort,
			]
		);
	}

	public static function addFacets(\components\sphinx\Criteria $criteria): void
	{
		// Extend the base SQL query to return facets values.
		$criteria->addFacet(new facets\CoveragedepthFacet); // acces
		$criteria->addFacet(new facets\CountryFacet); // pays
		$criteria->addFacet(new facets\DisciplineFacet()); // categorie
		$criteria->addFacet(new facets\LangFacet); // langue
	}

	/**
	 * Returns all the Revue.id that match a search.
	 *
	 * @return int[]
	 */
	public function searchRevueIds(): array
	{
		return $this->searchIds('revueid', self::MAX_IDS);
	}

	/**
	 * Returns all the Titre.id that match a search.
	 *
	 * @return int[]
	 */
	public function searchTitreIds(): array
	{
		return $this->searchIds('id', self::MAX_IDS);
	}

	public static function removeEmptyCriteria(array $get): array
	{
		foreach (['lien', 'attribut'] as $k) {
			if (isset($get[$k])) {
				$get[$k] = array_filter($get[$k]);
			}
		}
		return array_filter(
			$get,
			function ($x) {
				return $x !== '' && $x !== [];
			}
		);
	}

	/**
	 * Return a list of the parameters used, ready to be formatted.
	 */
	protected function arraySummary(): array
	{
		if ($this->partenaireId) {
			$instName = "“" . Partenaire::model()->findByPk($this->partenaireId)->getShortName() . "”";
		} else {
			$instName = "";
		}

		$sum = [];
		// simple text
		if ($this->titre) {
			$sum['Titre'] = $this->titre;
		}
		if ($this->langues) {
			if ($this->langues === "aucune" || $this->langues === "!") {
				$sum['Langues'] = "aucune";
			} else {
				$langCodes = preg_split('/[, ]\s*/', trim($this->langues, ", "));
				$langues = array_filter(array_map([\models\lang\Convert::class, 'codeToFullName'], $langCodes));
				$sum['Langues'] = join(
					$this->languesEt ? " ET " : " OU ",
					$langues
				);
			}
		}
		if ($this->issn) {
			$sum['ISSN'] = $this->issn;
		}
		if ($this->hdateModif) {
			$sum["Modifié"] = $this->hdateModif;
		}
		if ($this->hdateVerif) {
			$sum["Vérifié"] = $this->hdateVerif;
		}
		// boolean
		if ($this->vivant) {
			$sum[] = "Revue vivante";
		}
		if ($this->abonnement && $this->accesLibre && $this->owned) {
			$sum[] = "Accès libre ou abonné ou disponible à $instName";
		} elseif ($this->abonnement && $this->accesLibre) {
			$sum[] = "Accès libre ou abonné";
		} else {
			if ($this->accesLibre) {
				$sum[] = "Accès libre";
			}
			if ($this->abonnement && $this->owned) {
				$sum[] = "abonné en ligne {$this->aboCombine} disponible à $instName";
			} else {
				if ($this->abonnement) {
					$sum[] = ucfirst($instName) . " est abonné en ligne";
				}
				if ($this->owned) {
					$sum[] = "Disponible à $instName";
				}
			}
		}
		if ($this->sansAcces) {
			$sum[] = "Sans accès";
		}
		if ($this->monitoredByMe) {
			$sum[] = "Suivi par $instName";
		}
		if ($this->monitored === self::MONIT_YES) {
			$sum[] = "Suivi";
		} elseif ($this->monitored === self::MONIT_NO) {
			$sum[] = "Non suivi";
		}
		// single ID
		if ($this->collectionId) {
			$sum['Collection'] = Collection::model()->findByPk($this->collectionId)->nom;
		}
		if ($this->paysId) {
			$sum['Pays'] = \Yii::app()->db
				->createCommand("SELECT group_concat(nom) FROM Pays WHERE id IN (" . join(",", $this->paysId) . ") ORDER BY nom")
				->queryScalar();
		}
		if ($this->issnpaysid) {
			$sum["Pays de publication"] = \Yii::app()->db
				->createCommand("SELECT nom FROM Pays WHERE id = :id")
				->queryScalar([':id' => $this->issnpaysid]);
		}
		// liste of IDs
		$getName = function ($ar) {
			return $ar->nom;
		};
		if ($this->editeurId) {
			$sum['Éditeur'] = join(" ; ", array_map($getName, Editeur::model()->findAllByPk($this->editeurId)));
		}
		if ($this->aediteurId) {
			$sum["Éditeur actuel ou précédent"] = join(" ; ", array_map($getName, Editeur::model()->findAllByPk($this->aediteurId)));
		}
		if ($this->ressourceId) {
			$ids = join(',', array_map(fn ($x) => abs((int) $x), $this->ressourceId));
			$ressources = \Yii::app()->db->createCommand("SELECT id, nom FROM Ressource WHERE id IN ($ids)")
				->setFetchMode(PDO::FETCH_KEY_PAIR)
				->queryAll();
			$l = [];
			foreach ($this->ressourceId as $rid) {
				if ($rid > 0 && isset($ressources[$rid])) {
					$l[] = $ressources[$rid];
				} elseif ($rid < 0 && isset($ressources[0 - $rid])) {
					$l[] = "SAUF " . $ressources[0 - $rid];
				}
			}
			$sum['Ressource'] = join(" ; ", $l);
			unset($l);
		}
		$suivi = $this->monitoredByMe
			? array_filter($this->suivi, function ($x) {
				return $x != $this->partenaireId;
			})
			: $this->suivi;
		if ($suivi && $this->monitored === self::MONIT_IGNORE) {
			$sum['Suivi par'] = join(" ; ", array_map($getName, Partenaire::model()->findAllByPk($suivi)));
		}
		if ($this->detenu && !$this->owned) {
			$sum['Disponible à'] = join("; ", array_map($getName, Partenaire::model()->findAllByPk($this->detenu)));
		}
		if ($this->categorie) {
			$cNonRec = $this->cNonRec;
			$sum['Thématique'] = join(
				($this->categoriesEt ? " ET " : " OU "),
				array_map(
					function ($x) use ($cNonRec) {
						return $x->categorie . (in_array($x->id, $cNonRec) || $x->profondeur > 2 ? "" : " (récursif)");
					},
					$this->getCategories()
				)
			);
		}
		if ($this->sanscategorie) {
			$sum[] = "Sans thématique";
		}
		// special
		if ($this->acces) {
			$accesToText = function ($v) {
				$conv = ['', '', 'Texte intégral', 'Résumé', 'Sommaire', 'Indexation'];
				return $conv[$v];
			};
			$sum["Accès"] = join(" OU ", array_map($accesToText, $this->acces));
		}
		if ($this->lien) {
			$lienRequired = [];
			$lienProhibited = [];
			foreach ($this->lien as $l) {
				if ($l < 0) {
					$lienProhibited[] = -(int) $l;
				} elseif ($l > 0) {
					$lienRequired[] = (int) $l;
				}
			}
			if ($lienRequired) {
				$sum[] = "Avec lien " . join(
					", ",
					Yii::app()->db
						->createCommand("SELECT nom FROM Sourcelien WHERE id IN (" . join(",", $lienRequired) . ")")
						->queryColumn()
				);
			}
			if ($lienProhibited) {
				$sum[] = "Sans lien " . join(
					", ",
					Yii::app()->db
						->createCommand("SELECT nom FROM Sourcelien WHERE id IN (" . join(",", $lienProhibited) . ")")
						->queryColumn()
				);
			}
		}
		if ($this->attribut) {
			$attrRequired = [];
			$attrProhibited = [];
			foreach ($this->attribut as $a) {
				if ($a < 0) {
					$attrProhibited[] = -(int) $a;
				} elseif ($a > 0) {
					$attrRequired[] = (int) $a;
				}
			}
			if ($attrRequired) {
				$sum[] = "Avec attribut " . join(
					", ",
					Yii::app()->db
						->createCommand("SELECT nom FROM Sourceattribut WHERE id IN (" . join(",", $attrRequired) . ")")
						->queryColumn()
				);
			}
			if ($attrProhibited) {
				$sum[] = "Sans attribut " . join(
					", ",
					Yii::app()->db
						->createCommand("SELECT nom FROM Sourceattribut WHERE id IN (" . join(",", $attrProhibited) . ")")
						->queryColumn()
				);
			}
		}
		if ($this->grappe) {
			$gname = Yii::app()->db
				->createCommand("SELECT nom FROM Grappe WHERE id = " . (int) abs($this->grappe))
				->queryScalar();
			$gid = ($this->grappe > 0 ? (int) $this->grappe : 0 - $this->grappe);
			$sum[($this->grappe > 0 ? "Grappe " : "SAUF grappe ")] = '<span>'
				. CHtml::encode($gname)
				. CHtml::link(
					'<span class="micon-new-window"></span>',
					['/grappe/view', 'id' => $gid],
					['style' => 'padding-left:1ex', 'target' => '_blank', 'title' => "Voir la grappe \"$gname\" dans une nouvelle page"]
				)
				. '</span>';
		}
		return $sum;
	}

	protected function fillTimestamps(): void
	{
		if (empty($this->ts)) {
			$this->ts['Modif'] = DateTimeHelper::parseDateInterval($this->hdateModif);
			if ($this->hdateVerif !== '!') {
				$this->ts['Verif'] = DateTimeHelper::parseDateInterval($this->hdateVerif);
			}
		}
	}

	/**
	 * @param "id"|"revueid" $field
	 * @return int[]
	 */
	private function searchIds(string $field, int $batchSize): array
	{
		$params = $this->exportAttributes();
		$criteria = \processes\search\titres\Actor::createSqlCriteria($params);
		$criteria->select = substr_replace($criteria->select, $field, 0, 1); // * -> $field
		if ($batchSize > 0) {
			$criteria->limit = $batchSize;
			$criteria->offset = 0;
		}
		if ($field === 'id') {
			$criteria->group = "";
		}

		$db = Yii::app()->getComponent('sphinx');
		assert($db instanceof \components\sphinx\Connection);

		$ids = [];
		$exhausted = false;
		while (!$exhausted) {
			$cmd = $db->getCommandBuilder()->createFindCommand('{{titres}}', $criteria);
			$rows = $cmd->queryAll(true);
			foreach ($rows as $row) {
				$ids[] = (int) $row[$field];
			}
			$exhausted = count($rows) < $criteria->limit;
			$criteria->offset += $criteria->limit;
		}
		$cmd->order('titre ASC');
		return $ids;

	}
}
