<?php

namespace models\searches;

use components\sphinx\DataProvider;

class GlobalSearchResult
{
	/**
	 * @var ?DataProvider
	 */
	public $editeurs;

	/**
	 * @var ?DataProvider
	 */
	public $ressources;

	/**
	 * @var ?DataProvider
	 */
	public $titres;

	/**
	 * @var array
	 */
	public $hashes = [
		'titres' => '',
		'ressources' => '',
		'editeurs' => '',
	];
}
