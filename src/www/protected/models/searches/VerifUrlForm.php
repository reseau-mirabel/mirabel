<?php

class VerifUrlForm extends CFormModel
{
	public int $grappe = 0;

	/**
	 * @var int 0 | Partenaire.id
	 */
	public int $possessions = 0;

	public $msgContient = "";

	public $msgExclut = "HTTP 401";

	public $urlContient = "";

	public $urlExclut = "";

	public $suiviPar;

	public $nonSuivi = false;

	public $accesIntegralLibre = 0;

	public function rules()
	{
		return [
			['msgContient, msgExclut, urlContient, urlExclut', 'length', 'min' => 3, 'max' => 255],
			['accesIntegralLibre, grappe, possessions, suiviPar', 'numerical', 'integerOnly' => true],
			['nonSuivi', 'boolean'],
		];
	}

	public function afterValidate()
	{
		if ($this->grappe > 0) {
			$grappe = \Grappe::model()->findByPk($this->grappe);
			if (($grappe instanceof \Grappe)) {
				$permissions = new \processes\grappe\Permissions(\Yii::app()->user);
				if (!$permissions->canRead($grappe)) {
					// No access to this Grappe => reset the criteria
					$this->grappe = 0;
				}
			} else {
				// Invalid Grappe => reset the criteria
				$this->grappe = 0;
			}
		} else {
			$this->grappe = 0;
		}
		if ($this->possessions > 0) {
			if ($this->grappe > 0) {
				$this->possessions = 0;
			} elseif ((int) $this->possessions !== (int) \Yii::app()->user->partenaireId && !\Yii::app()->user->checkAccess('admin')) {
				// No access to this Partenaire => reset the criteria
				$this->possessions = 0;
			} elseif ($this->suiviPar > 0) {
				$this->addError('suiviPar', "Les paramètres 'possessions' et 'suivi' sont exclusifs. N'en choisissez qu'un !");
			}
		}
	}

	public function getSearchData()
	{
		if (!$this->validate()) {
			return ['', []];
		}
		$conditions = [];
		$params = [];
		if ($this->msgExclut) {
			$conditions[] = "v.msg NOT LIKE :msgExclut";
			$params[':msgExclut'] = '%' . $this->msgExclut . '%';
		}
		if ($this->msgContient) {
			$conditions[] = "v.msg LIKE :msgContient";
			$params[':msgContient'] = '%' . $this->msgContient . '%';
		}
		if ($this->urlContient) {
			$conditions[] = "v.url LIKE :urlContient";
			$params[':urlContient'] = '%' . $this->urlContient . '%';
		}
		if ($this->urlExclut) {
			$conditions[] = "v.url NOT LIKE :urlExclut";
			$params[':urlExclut'] = '%' . $this->urlExclut . '%';
		}
		if ($conditions) {
			return [" AND " . join(" AND ", $conditions), $params];
		}
		return ['', []];
	}

	/**
	 * @param "Cms"|"Editeur"|"Ressource"|"Revue" $target
	 */
	public function addSearchCriteria(CDbCommand $cmd, string $target): CDbCommand
	{
		/**
		 * Alias définis dans Urls:
		 * v => table VerifUrl
		 * c / e / t r / r => tables Cms / Editeur / Titre Revue / Ressource, suivant $target
		 */
		if (!$this->validate()) {
			$cmd->andWhere("1=0");
			return $cmd;
		}
		$this->accesIntegralLibre = (int) $this->accesIntegralLibre;

		$source = \processes\urlverif\UrlMassCheck::SOURCES[$target] ?? 0;
		if (!$source) {
			throw new \Exception("Undefined target while filtering URLs.");
		}
		$cmd->andWhere("v.source = $source");

		$this->searchGrappe($cmd, $target);
		$this->searchPossessions($cmd, $target);
		$this->searchSuivi($cmd, $target);

		if ($this->msgExclut) {
			$cmd->andWhere("v.msg NOT LIKE :msgExclut", [':msgExclut' => '%' . $this->msgExclut . '%']);
		}
		if ($this->msgContient) {
			$cmd->andWhere("v.msg LIKE :msgContient", [':msgContient' => '%' . $this->msgContient . '%']);
		}
		if ($this->urlContient) {
			$cmd->andWhere("v.url LIKE :urlContient", [':urlContient' => '%' . $this->urlContient . '%']);
		}
		if ($this->urlExclut) {
			$cmd->andWhere("v.url NOT LIKE :urlExclut", [':urlExclut' => '%' . $this->urlExclut . '%']);
		}
		if ($this->accesIntegralLibre > 0) {
			if ($target === 'Revue') {
				$cmd->join("Service", "t.id = Service.titreId AND Service.url = v.url");
				$cmd->group("v.id");
			} else {
				throw new \Exception("Undefined target while filtering URLs.");
			}
			$cmd->andWhere("Service.type = 'Intégral' AND Service.acces = 'Libre'");
		} elseif ($this->accesIntegralLibre < 0) {
			if ($target === 'Revue') {
				$cmd->leftJoin("Service", "t.id = Service.titreId AND Service.url = v.url AND Service.type = 'Intégral' AND Service.acces = 'Libre'");
				$cmd->group("v.id");
			} else {
				throw new \Exception("Undefined target while filtering URLs.");
			}
			$cmd->andWhere("Service.id IS NULL");
		}
		return $cmd;
	}

	public function attributeLabels()
	{
		return [
			'msgContient' => "L'erreur contient",
			'msgExclut' => "L'erreur ne contient pas",
			'nonSuivi' => "Revues non suivies",
			'urlContient' => "L'URL contient",
			'urlExclut' => "L'URL ne contient pas",
		];
	}

	/**
	 * Update $cmd with criteria relative to "Grappe".
	 */
	private function searchGrappe(CDbCommand $cmd, string $target): void
	{
		if ($this->grappe <= 0) {
			return;
		}
		if ($target === 'Revue') {
			// For a check on Revue, there is already a "JOIN Titre t".
			$cmd->join("Grappe_Titre gt", "gt.titreId = t.id");
			$cmd->andWhere("gt.grappeId = :grappe", [':grappe' => $this->grappe]);
		} elseif ($target === 'Editeur') {
			$cmd->join("Titre_Editeur te", "te.editeurId = e.id");
			$cmd->join("Grappe_Titre gt", "gt.titreId = te.titreId");
			$cmd->andWhere("gt.grappeId = :grappe", [':grappe' => $this->grappe]);
		}
		$cmd->group("v.id"); // JOINs can duplicate source records
	}

	/**
	 * Update $cmd with criteria relative to "Partenaire_Titre" AKA "Possession".
	 */
	private function searchPossessions(CDbCommand $cmd, string $target): void
	{
		if ($this->possessions <= 0) {
			return;
		}
		if ($target === 'Revue') {
			$cmd->join("Partenaire_Titre pt", "pt.titreId = t.id");
			$cmd->andWhere("pt.partenaireId = :partenaireId", [':partenaireId' => $this->possessions]);
			return;
		}
		if ($target === 'Editeur') {
			$cmd->join("Titre_Editeur te", "te.editeurId = e.id");
			$cmd->join("Partenaire_Titre pt", "pt.titreId = te.titreId");
			$cmd->andWhere("pt.partenaireId = :partenaireId", [':partenaireId' => $this->possessions]);
			$cmd->group("v.id");
			return;
		}
	}

	/**
	 * Update $cmd with criteria relative to "Suivi".
	 *
	 * Plusieurs propriétés interviennent, avec un comportement variable suivant le type de partenaire.
	 */
	private function searchSuivi(CDbCommand $cmd, string $target): void
	{
		if ($this->nonSuivi && $this->suiviPar && $target === 'Revue') {
			$cmd->leftJoin("Suivi s", "s.cible = 'Revue' AND s.cibleId = v.sourceId");
			$cmd->andWhere("s.partenaireId = :pid OR s.partenaireId IS NULL", [":pid" => $this->suiviPar]);
			return;
		}
		if ($this->suiviPar) {
			if ($target === 'Editeur') {
				// Liens d'éditeurs rattachées à des revues suivies
				$cmd->join("Titre_Editeur te", "te.editeurId = e.id");
				$cmd->join("Titre t", "t.id = te.titreId");
				$cmd->join("Suivi s", "s.cible = 'Revue' AND s.cibleId = t.revueId");
				$cmd->andWhere("s.partenaireId = :pid", [":pid" => $this->suiviPar]);
				$cmd->group("v.id");
				return;
			}
			// Revue ou Ressource => suivi du même type
			$cmd->join("Suivi s", "s.cible = '$target' AND s.cibleId = v.sourceId");
			$cmd->andWhere("s.partenaireId = :pid", [":pid" => $this->suiviPar]);
			return;
		}
		if ($this->nonSuivi && $target === 'Revue') {
			$cmd->leftJoin("Suivi s", "s.cible = 'Revue' AND s.cibleId = v.sourceId");
			$cmd->andWhere("s.partenaireId IS NULL");
			return;
		}
		return;
	}
}
