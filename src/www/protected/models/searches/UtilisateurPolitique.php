<?php

namespace models\searches;

class UtilisateurPolitique extends \CModel
{
	public $actif;

	public $email = '';

	public $nom = '';

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['actif', 'boolean'],
			['email, nom', 'safe'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeNames()
	{
		return [
			'actif' => "Actif",
			'email' => "Email, login",
			'nom' => "Nom complet",
		];
	}

	public function search(): \CSqlDataProvider
	{
		$command = \Yii::app()->db->createCommand()
			->from("Utilisateur u")
			->where('u.partenaireId IS NULL');

		if ($this->actif !== null && $this->actif !== '') {
			$command->andWhere("u.actif = " . (int) $this->actif);
		}
		if ($this->email) {
			$command->andWhere(
				"u.email LIKE :email",
				[':email' => '%' . strtr($this->email, ['%' => '\%', '_' => '\_', '\\' => '\\\\']) . '%']
			);
		}
		if ($this->nom) {
			$command->andWhere(
				"u.nomComplet LIKE :nom",
				[':nom' => '%' . strtr($this->nom, ['%' => '\%', '_' => '\_', '\\' => '\\\\']) . '%']
			);
		}

		$countCommand = clone $command;
		$countCommand->select("count(*)");
		$count = (int) $countCommand->queryScalar();

		$command->select("u.*, GROUP_CONCAT(CONCAT(IF(ue.confirmed = 0, '#pending# ', ''), e.nom) SEPARATOR ' + ') AS editeurs")
			->join("Utilisateur_Editeur ue", "ue.utilisateurId = u.id")
			->join("Editeur e", "ue.editeurId = e.id")
			->group(['u.id']);

		$sort = new \CSort();
		$sort->attributes = [
			'email' => [
				'asc' => 'u.email',
				'desc' => 'u.email DESC',
			],
			'nom' => [
				'asc' => 'u.nom',
				'desc' => 'u.nom DESC',
			],
			'derConnexion' => [
				'asc' => 'u.derConnexion',
				'desc' => 'u.derConnexion DESC',
			],
			'hdateCreation' => [
				'asc' => 'u.hdateCreation',
				'desc' => 'u.hdateCreation DESC',
			],
			'hdateModif' => [
				'asc' => 'u.hdateModif',
				'desc' => 'u.hdateModif DESC',
			],
		];
		$sort->defaultOrder = 'u.hdateCreation DESC';

		return new \CSqlDataProvider(
			$command,
			[
				'pagination' => ['pageSize' => 25],
				'sort' => $sort,
				'totalItemCount' =>  $count,
			]
		);
	}
}
