<?php

/**
 * Description of CollectionWithNums.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class CollectionWithNums extends Collection
{
	public $numRevues = 0;

	public $numTitres = 0;
}
