<?php

/**
 * Titre with an "extra" field, for various ActiveRecord results.
 */
class TitreWithExtra extends \Titre
{
	public $extra;
}
