<?php

namespace models\searches;

use CActiveDataProvider;
use CDbCriteria;
use CHtml;
use components\HtmlHelper;
use CSort;
use Intervention;
use models\import\ImportType;
use models\validators\DateValidator;
use Partenaire;
use Titre;
use Yii;

/**
 * @property ?int $import
 * @property string|int $utilisateurIdProp
 * @method int buildTsConditionFromDateAttr(string $a, string $b) from the behaviours
 */
class InterventionSearch extends Intervention
{
	/**
	 * @var ?int
	 */
	public $delai;

	public $ressource_nom;

	public $titre_titre;

	public $editeur_nom;

	public $partenaireId;

	public $suiviPartenaireId;

	public $auteur = false;

	public $suivi = true;

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			[
				'ressourceId, revueId, titreId, editeurId, import, utilisateurIdProp, utilisateurIdVal, partenaireId, suiviPartenaireId, delai',
				'numerical', 'integerOnly' => true,
			],
			['suivi, auteur', 'boolean'],
			['description, commentaire, email', 'length', 'max' => 255],
			['statut', 'in', 'range' => array_keys(Intervention::STATUTS)],
			[
				'ressource_nom, titre_titre, editeur_nom, hdateProp, hdateVal, ip, action',
				'safe', 'on' => 'search',
			],
		];
	}

	public function attributeLabels()
	{
		return array_merge(parent::attributeLabels(), [
			'delai' => "Délai de validation",
			'ressource_nom' => 'ressource',
			'titre_titre' => 'Titre',
			'editeur_nom' => 'Éditeur',
			'partenaireId' => 'Partenaire',
			'suiviPartenaireId' => 'Suivi par',
			'auteur' => "Limiter aux interventions dont je suis l'auteur",
		]);
	}

	public function attributeNames()
	{
		return array_merge(
			parent::attributeNames(),
			['delai', 'ressource_nom', 'titre_titre', 'editeur_nom', 'partenaireId', 'suiviPartenaireId', 'auteur', 'suivi']
		);
	}

	public function getAttributes($names = true)
	{
		$withNames = array_merge(
			parent::getAttributes(true),
			[
				'delai' => $this->delai,
				'ressource_nom' => $this->ressource_nom,
				'titre_titre' => $this->titre_titre,
				'editeur_nom' => $this->editeur_nom,
				'partenaireId' => $this->partenaireId,
				'suiviPartenaireId' => $this->suiviPartenaireId,
				'auteur' => $this->auteur,
				'suivi' => $this->suivi,
			]
		);
		if (is_array($names)) {
			return array_intersect_key($withNames, array_combine($names, $names));
		}
		return $withNames;
	}

	public function beforeValidate()
	{
		if ($this->utilisateurIdProp > 0 && $this->auteur) {
			$this->addError('utilisateurIdProp', "Ce champ est en conflit avec « Limiter aux interventions dont je suis l'auteur »");
		}
		return parent::beforeValidate();
	}

	public function afterValidate()
	{
		parent::afterValidate();
		if (empty($this->delai)) {
			$this->delai = null;
		}
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @param int|bool $pageSize Number of items for pagination (false to disable pagination).
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($pageSize = 25): CActiveDataProvider
	{
		$criteria = new CDbCriteria;
		$criteria->select = [
			't.*',
			<<<EOSQL
			IF(
				s1.partenaireId IS NULL AND s2.partenaireId IS NULL AND s3.partenaireId IS NULL,
				'',
				CONCAT(
					IFNULL(GROUP_CONCAT(DISTINCT s1.partenaireId SEPARATOR ','), ''),
					',',
					IFNULL(GROUP_CONCAT(DISTINCT s2.partenaireId SEPARATOR ','), ''),
					',',
					IFNULL(GROUP_CONCAT(DISTINCT s3.partenaireId SEPARATOR ','), '')
				)
			) AS suivi
			EOSQL
		];
		$criteria->with = [
			'ressource' => ['select' => 'nom, prefixe'],
			'titreRevue' => ['select' => 'titre, prefixe'],
			'editeur' => ['select' => 'nom, prefixe'],
		];
		$criteria->join =
			<<<EOSQL
			LEFT JOIN Suivi s1 ON s1.cible = 'Revue' AND t.revueId = s1.cibleId
			LEFT JOIN Suivi s2 ON s2.cible = 'Ressource' AND t.ressourceId = s2.cibleId
			LEFT JOIN Suivi s3 ON s3.cible = 'Editeur' AND t.editeurId = s3.cibleId
			EOSQL;
		$criteria->group = "t.id";

		$criteria->compare('t.description', $this->description, true);
		$criteria->compare('ressource.nom', $this->ressource_nom, true);
		$criteria->compare('titreRevue.titre', $this->titre_titre, true);
		$criteria->compare('editeur.nom', $this->editeur_nom, true);
		$criteria->compare('t.ressourceId', $this->ressourceId);
		$criteria->compare('t.revueId', $this->revueId);
		$criteria->compare('t.titreId', $this->titreId);
		$criteria->compare('t.editeurId', $this->editeurId);
		$criteria->compare('t.statut', $this->statut);
		$criteria->addCondition(DateValidator::buildTsConditionFromDateValue('t.hdateProp', (string) $this->hdateProp));
		if ($this->hdateVal) {
			$criteria->addCondition(DateValidator::buildTsConditionFromDateValue('t.hdateVal', (string) $this->hdateVal));
		} elseif ($this->statut === 'attente') {
			// optimize the SQL query by using this indexed column
			$criteria->addCondition("t.hdateVal IS NULL");
		} elseif ($this->statut === 'accepté') {
			// optimize the SQL query by using this indexed column
			$criteria->addCondition("t.hdateVal IS NOT NULL");
		}
		if ($this->delai > 0) {
			$criteria->addCondition("t.statut IN ('accepté', 'refusé')");
			$criteria->addCondition("t.import = 0");
			$criteria->addCondition("t.hdateVal IS NOT NULL AND (t.hdateVal - t.hdateProp) > {$this->delai}");
		}
		if (isset($this->import) && $this->import !== '') {
			$criteria->compare('t.import', (int) $this->import);
		}
		if ($this->utilisateurIdProp === '0') {
			$criteria->addCondition("utilisateurIdProp IS NULL");
			$criteria->compare('t.import', 0); // intervention manuelle
		} elseif ($this->utilisateurIdProp > 0) {
			$criteria->compare('utilisateurIdProp', $this->utilisateurIdProp);
		} elseif ($this->auteur) {
			$criteria->addCondition("utilisateurIdProp = " . (int) \Yii::app()->user->id);
		}
		$criteria->compare('utilisateurIdVal', $this->utilisateurIdVal);
		if ($this->partenaireId) {
			$usersId = Yii::app()->db
				->createCommand("SELECT id FROM Utilisateur WHERE partenaireId = :pid")
				->queryColumn([':pid' => $this->partenaireId]);
			$criteria->addInCondition('utilisateurIdProp', $usersId);
		}
		if ($this->suiviPartenaireId) {
			$criteria->addCondition(
				sprintf(
					's1.partenaireId = %d OR s2.partenaireId = %d OR s3.partenaireId = %d',
					$this->suiviPartenaireId,
					$this->suiviPartenaireId,
					$this->suiviPartenaireId
				)
			);
		}

		$criteria->compare('ip', $this->ip);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('commentaire', $this->commentaire, true);
		$criteria->compare('action', $this->action);

		if ($this->suivi) {
			/* More logical, but slower than listing the target IDs.
			 *
			$pid = (int) Yii::app()->user->getState('partenaireId');
			$criteria->addCondition("s1.partenaireId = $pid OR s2.partenaireId = $pid OR s3.partenaireId = $pid");
			 */
			$condSuivi = self::buildConditionSuivi(\Yii::app()->user);
			if ($condSuivi) {
				$criteria->addCondition($condSuivi);
			}
		}

		$sort = new CSort();
		$sort->attributes = ['statut', 'hdateProp', 'hdateVal'];
		if ($this->revueId || $this->ressourceId || $this->editeurId) {
			$sort->defaultOrder = "(t.statut = 'attente') DESC, hdateVal DESC, hdateProp DESC";
		} else {
			$sort->defaultOrder = 'hdateProp DESC';
		}

		$countCriteria = clone $criteria;
		$countCriteria->select = "t.id";
		if (empty($this->suivi) && empty($this->suiviPartenaireId)) {
			$countCriteria->join = '';
		}
		if (empty($this->ressource_nom) && empty($this->titre_titre) && empty($this->editeur_nom)) {
			$countCriteria->with = null;
		}

		return new CActiveDataProvider(
			$this,
			[
				'criteria' => $criteria,
				'pagination' => ['pageSize' => $pageSize],
				'sort' => $sort,
				'countCriteria' => $countCriteria,
			]
		);
	}

	/**
	 * @return array<string, string>
	 */
	public function getSummary(): array
	{
		$criteria = [];

		$texts = ['description', 'ressource_nom', 'titre_titre', 'editeur_nom', 'statut', 'action', 'hdateProp', 'hdateVal'];
		foreach ($texts as $k) {
			if ($this->{$k}) {
				$criteria[$k] = "[" . $this->getAttributeLabel($k) . " : " . $this->{$k} . "]";
			}
		}

		$ids = [
			'ressourceId' => 'Ressource.nom',
			'revueId' => 'Titre.titre',
			'editeurId' => 'Editeur.nom',
			'utilisateurIdProp' => 'Utilisateur.nomComplet',
			'utilisateurIdVal' => 'Utilisateur.nomComplet',
			'partenaireId' => 'Partenaire.nom',
			'suiviPartenaireId' => 'Partenaire.nom',
		];
		foreach ($ids as $k => $rel) {
			if ($k === 'utilisateurIdProp' && $this->{$k} === '0') {
				$criteria['utilisateurIdProp'] = "[" . $this->getAttributeLabel($k) . ' : anonyme]';
			}
			if ($this->{$k}) {
				[$class, $field] = explode('.', $rel);
				if ($class === 'Titre') {
					$record = Titre::model()->findBySql("SELECT * FROM Titre WHERE revueId = :id ORDER BY obsoletePar ASC LIMIT 1", [':id' => $this->{$k}]);
				} else {
					$record = $class::model()->findByPk($this->{$k});
				}
				$criteria[$k] = "[" . $this->getAttributeLabel($k)
					. " : " . ($record->{$field} ?? "{$this->{$k}}/non-trouvé") . "]";
			}
		}

		if ($this->auteur) {
			$criteria['auteur'] = "[Mes modifications]";
		}
		if ($this->suivi) {
			$criteria['suivi'] = "[Mon suivi]";
		}
		if ($this->delai) {
			$criteria['delai'] = "[Délai de validation > {$this->delai}s]";
		}
		if ($this->import) {
			$criteria['import'] = "[Import : " . ImportType::getSourceName($this->import) . "]";
		}

		return $criteria;
	}

	public function getColumnsText(): array
	{
		$columns = [];
		foreach ($this->getColumnsHtml() as $c) {
			if (is_array($c)) {
				unset($c['cssClassExpression']);
				$columns[] = $c;
			} elseif (is_scalar($c)) {
				$columns[] = $c;
			}
		}
		return $columns;
	}

	public function getColumnsHtml(): array
	{
		return [
			'description',
			[
				'name' => 'ressource_nom',
				'header' => 'Ressource',
				'value' => function (InterventionSearch $data): string {
					if (empty($data->ressourceId)) {
						return '';
					}
					$ressource = $data->ressource;
					if ($ressource === null) {
						return "Supprimée (ID {$data->ressourceId})";
					}
					return $ressource->nom;
				},
				'cssClassExpression' => function ($row, InterventionSearch $data): string {
					$param = [];
					if (!$data->suivi) {
						return "";
					}
					if ($data->ressourceId) {
						$param['model'] = 'Ressource';
						$param['id'] = $data->ressourceId;
					}
					return HtmlHelper::linkItemClass('intervention', $param);
				},
			],
			[
				'name' => 'titre_titre',
				'header' => 'Revue',
				'value' => function (InterventionSearch $data): string {
					if (empty($data->revueId)) {
						return '';
					}
					return ($data->titreRevue ? $data->titreRevue->getShortTitle() : '*sans-titre*');
				},
				'cssClassExpression' => function ($row, InterventionSearch $data) {
					$param = [];
					if (!$data->suivi) {
						return false;
					}
					if ($data->revueId) {
						$param['model'] = 'Revue';
						$param['id'] = $data->revueId;
					}
					return HtmlHelper::linkItemClass('intervention', $param);
				},
			],
			[
				'name' => 'editeur_nom',
				'header' => 'Éditeur',
				'value' => function (InterventionSearch $data) {
					if (!$data->editeurId) {
						return '';
					}
					$e = $data->editeur;
					return ($e ? $e->nom : '(suppr)');
				},
				'cssClassExpression' => function ($row, InterventionSearch $data) {
					if ($data->suivi && $data->editeurId) {
						return HtmlHelper::linkItemClass('', ['model' => 'Editeur', 'id' => $data->editeurId]);
					}
					return false;
				},
			],
			[
				'name' => 'utilisateurIdProp',
				'header' => 'Par …<br>→ pour …',
				'filter' => false,
				'type' => 'html',
				'value' => function (InterventionSearch $data): string {
					if ($data->utilisateurIdProp) {
						$u = $data->utilisateurProp;
						$from = $u ? CHtml::encode($data->utilisateurProp->nomComplet) : '(compte suppr.)';
					} else {
						$from = "";
					}
					$to = "";
					$suivi = array_unique(array_filter(explode(',', $data->suivi)));
					foreach ($suivi as $pId) {
						$partenaire = Partenaire::model()->findByPk($pId);
						if ($partenaire) {
							$to .= "<div>→ " . $partenaire->getSelfLink(true) . "</div>";
						}
					}
					return $from . $to;
				},
			],
			[
				'name' => 'statut',
				'value' => function (InterventionSearch $data): string {
					return Intervention::STATUTS[$data->statut];
				},
				'filter' => Intervention::STATUTS,
			],
			'hdateProp:datetime',
			'hdateVal:datetime',
			[
				'class' => 'BootButtonColumn',
				'header' => '',
				'template' => '{view}',
			],
		];
	}

	private static function buildConditionSuivi(\WebUser $user): string
	{
		if ($user->isGuest) {
			throw new \Exception("Erreur fatale de permission");
		}
		$suivi = $user->getState('suivi');
		$suiviEditeurs = $user->getState('suiviEditeurs');
		$suiviNonSuivi = $user->getState('EditeursNonSuivi');
		if (empty($suivi) && !$suiviEditeurs && !$suiviNonSuivi) {
			return '1=0';
		}
		$condition = [];
		if (!empty($suivi['Revue'])) {
			$condition[] = "(t.revueId IN (" . join(',', $suivi['Revue']) . "))";
		}
		if (!empty($suivi['Ressource'])) {
			$condition[] = "(t.ressourceId IN (" . join(',', $suivi['Ressource']) . "))";
		}
		if (!empty($suivi['Editeur'])) {
			$condition[] = "(t.editeurId IN (" . join(',', $suivi['Editeur']) . "))";
		}
		if (!empty($suiviEditeurs)) {
			$condition[] = "(t.editeurId IS NOT NULL)";
		}
		if (!empty($suiviNonSuivi)) {
			$condition[] = "(suivi = '')";
		}
		if (empty($condition)) {
			return "";
		}
		return join(' OR ', $condition);
	}
}
