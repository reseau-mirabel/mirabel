<?php

class RessourceSearch extends Ressource
{
	public $partenaireId;

	public $suivi;

	public function rules()
	{
		return [
			['nom', 'length', 'max' => 255],
		];
	}

	public function search()
	{
		$criteria = new CDbCriteria;
		$criteria->join = "JOIN Suivi s ON s.cibleId = t.id AND s.cible = 'Ressource'";
		$criteria->select = "t.*, GROUP_CONCAT(s.partenaireId) AS suivi";
		$criteria->group = "t.id";

		$criteria->compare('t.nom', $this->nom, true);
		$criteria->compare('s.partenaireId', $this->partenaireId);

		$sort = new CSort();
		$sort->attributes = [
			'nom' => 't.nom',
			'hdateModif' => 'hdateModif',
			'hdateVerif' => 'hdateVerif',
		];
		$sort->defaultOrder = ['nom' => CSort::SORT_ASC];

		return new CActiveDataProvider(
			$this,
			[
				'criteria' => $criteria,
				'pagination' => false,
				'sort' => $sort,
			]
		);
	}
}
