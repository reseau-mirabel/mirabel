<?php

namespace models\searches;

use components\sphinx\DataProvider;
use components\SqlHelper;
use models\sphinx\Editeurs;
use Pays;

class EditeurSearch extends \CModel
{
	public $global = '';

	public $idref = '';

	public $nom = '';

	public $pays = '';

	public $sherpa = '';

	public $ror = '';

	public $vivants = false;

	public $hdateModif = '';

	public $hdateVerif = '';

	/**
	 * @var ?Pays Cache for getPays()
	 */
	private $paysRecord;

	public function __construct()
	{
		// default scenario is "search"
		$this->setScenario('search');
	}

	public function attributeNames()
	{
		return ['idref', 'global', 'nom', 'pays', 'hdateModif', 'hdateVerif', 'sherpa', 'ror', 'vivants'];
	}

	public function attributeLabels()
	{
		return [
			'global' => "Tous les champs",
			'hdateModif' => "Date de modification",
			'hdateVerif' => "Date de vérification",
			'idref' => "IdRef",
			'sherpa' => "ID Open policy finder",
			'ror' => "ID ROR",
		];
	}

	public function rules()
	{
		return [
			['idref', 'match', 'pattern' => '/^([0-9]{8}[0-9X]|!|\*)$/'],
			['nom, global', 'length', 'max' => 255],
			['pays', 'match', 'pattern' => '/^([0-9]+|[A-Z]{2}|[A-Z]{3}|!|\*)$/'],
			['vivants', 'boolean'],
			['hdateModif, hdateVerif', 'validateTimeCondition'],
			['sherpa', 'match', 'pattern' => '/^(\d+|!|\*)$/'],
			['ror', 'match', 'pattern' => '/^(0[a-hj-km-np-tv-z|0-9]{6}[0-9]{2}|\*|!)$/'],
		];
	}

	public function beforeValidate()
	{
		$m = [];
		if ($this->nom) {
			$this->nom = trim($this->nom);
		}
		if ($this->global) {
			$this->global = trim($this->global);
		}
		if ($this->idref) {
			$this->idref = trim($this->idref);
			if (preg_match('#idref.fr/(\d{8}[\dX])\b#', $this->idref, $m)) {
				$this->idref = $m[1];
			}
		}
		if ($this->sherpa) {
			$this->sherpa = trim($this->sherpa);
			if (preg_match('#^https?://v2\.sherpa\.ac\.uk/id/publisher/(\d+)$#', $this->sherpa, $m)) {
				$this->sherpa = $m[1];
			}
		}
		if ($this->ror) {
			if (preg_match('#^https?://ror\.org/(.*)$#', trim((string) $this->ror), $m)) {
				$this->ror = $m[1];
			}
		}
		return parent::beforeValidate();
	}

	public function validateTimeCondition($attrName)
	{
		if ($this->{$attrName} !== '') {
			try {
				\components\DateTimeHelper::parseDateInterval($this->{$attrName});
			} catch (\Exception $_) {
				$this->addError($attrName, "Format non valide");
			}
		}
	}

	public function getSummary(): string
	{
		$criteria = [];
		if ($this->global) {
			$criteria[] = "[tout champ contenant « {$this->global} »]";
		}
		if ($this->nom) {
			$criteria[] = "[nommé « {$this->nom} »]";
		}
		if ($this->idref) {
			if ($this->idref === '*') {
				$criteria[] = "[avec IdRef]";
			} elseif ($this->idref === '!') {
				$criteria[] = "[sans IdRef]";
			} else {
				$criteria[] = "[IdRef {$this->idref}]";
			}
		}
		if ($this->pays) {
			$criteria[] = "[pays : {$this->fetchPays()->nom}]";
		}
		if ($this->vivants) {
			$criteria[] = "[avec titres en cours]";
		}
		if ($this->hdateModif) {
			$criteria[] = "[modifié: {$this->hdateModif}]";
		}
		if ($this->hdateVerif) {
			$criteria[] = "[vérifié: {$this->hdateVerif}]";
		}
		if ($this->sherpa) {
			if ($this->sherpa === '*') {
				$criteria[] = "[avec Open policy finder]";
			} elseif ($this->sherpa === '!') {
				$criteria[] = "[sans Open policy finder]";
			} else {
				$criteria[] = "[Open policy finder {$this->sherpa}]";
			}
		}
		if ($this->ror) {
			if ($this->ror === '*') {
				$criteria[] = "[avec ROR]";
			} elseif ($this->ror === '!') {
				$criteria[] = "[sans ROR]";
			} else {
				$criteria[] = "[ROR {$this->ror}]";
			}
		}
		return join(" ", $criteria);
	}

	public function isEmpty(): bool
	{
		return empty($this->idref) && empty($this->global) && empty($this->nom) && empty($this->pays)
			&& !$this->vivants && !$this->hdateModif && !$this->hdateVerif && !$this->sherpa && !$this->ror;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @param int|bool $pageSize Number of items for pagination (false to disable pagination).
	 * @return DataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($pageSize = 25): DataProvider
	{
		$criteria = new \components\sphinx\Criteria();

		if ($this->validate()) {
			if ($this->global) {
				$criteria->addCondition("MATCH('" . \components\sphinx\Schema::quoteFulltext($this->global) . "')");
			}

			if ($this->nom) {
				$pattern = "@nomcomplet " . \components\sphinx\Schema::quoteFulltext($this->nom);
				$criteria->addCondition("MATCH('" . $pattern . "')");
			}

			if ($this->pays === '*') {
				$criteria->addCondition("paysid > 0");
			} elseif ($this->pays === '!') {
				$criteria->addCondition("paysid = 0");
			} elseif (ctype_digit((string) $this->pays)) {
				$criteria->addCondition("paysid = " . (int) $this->pays);
			} elseif ($this->pays) {
				$pays = $this->fetchPays();
				if ($pays !== null) {
					$criteria->addCondition("paysid = {$pays->id}");
				}
			}

			if ($this->idref === '*') {
				$criteria->addCondition("idref > 0");
			} elseif ($this->idref === '!') {
				$criteria->addCondition("idref = 0");
			} elseif ($this->idref) {
				$criteria->addCondition('idref = ' . (int) substr($this->idref, 0, 8));
			}

			if ($this->sherpa === '*') {
				$criteria->addCondition("sherpa > 0");
			} elseif ($this->sherpa === '!') {
				$criteria->addCondition("sherpa = 0");
			} elseif ($this->sherpa) {
				$criteria->addCondition("sherpa = " . (int) $this->sherpa);
			}

			// Manticore: editeurs.ror is a string attribute.
			if ($this->ror === '*') {
				$criteria->addCondition("ror <> ''");
			} elseif ($this->ror === '!') {
				$criteria->addCondition("ror = ''");
			} elseif ($this->ror) {
				$criteria->addCondition("ror = '" . $this->ror . "'"); // Safe because it matches a regexp.
			}

			if ($this->vivants) {
				$criteria->addCondition('nbtitresvivants > 0');
			}

			if ($this->hdateModif) {
				$cond = SqlHelper::dateIntervalToSqlCondition($this->hdateModif, 'hdateModif');
				if ($cond) {
					$criteria->addCondition($cond);
				}
			}

			if ($this->hdateVerif) {
				$cond = SqlHelper::dateIntervalToSqlCondition($this->hdateVerif, 'hdateVerif');
				if ($cond) {
					$criteria->addCondition($cond);
				}
			}
		} else {
			$criteria->addCondition('id = 0');
		}

		$sort = new \CSort();
		$sort->attributes = [
			'nom' => ['asc' => 'cletri ASC', 'desc' => 'cletri DESC'],
			'nbrevues' => ['asc' => 'nbrevues DESC', 'desc' => 'nbrevues ASC'],
			'nbtitresvivants' => ['asc' => 'nbtitresvivants DESC', 'desc' => 'nbtitresvivants ASC'],
		];
		$sort->defaultOrder = ['nom' => \CSort::SORT_ASC];

		return new DataProvider(
			Editeurs::model(),
			[
				'source' => \Editeur::model(),
				'criteria' => $criteria,
				'pagination' => $pageSize ? ['pageSize' => $pageSize] : false,
				'sort' => $sort,
			]
		);
	}

	private function fetchPays(): ?Pays
	{
		if ($this->paysRecord === null && $this->pays) {
			if (ctype_digit($this->pays)) {
				$this->paysRecord = Pays::model()->findByAttributes(['id' => $this->pays]);
			} elseif (strlen($this->pays) === 2) {
				$this->paysRecord = Pays::model()->findByAttributes(['code2' => $this->pays]);
			} elseif (strlen($this->pays) === 3) {
				$this->paysRecord = Pays::model()->findByAttributes(['code' => $this->pays]);
			}
			if ($this->paysRecord !== null) {
				$this->pays = (int) $this->paysRecord->id;
			}
		}
		return $this->paysRecord;
	}
}
