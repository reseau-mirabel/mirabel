<?php

namespace models\searches;

use models\validators\DateValidator;

/**
 * @property string $import
 */
class SourcelienSearch extends \Sourcelien
{
	public string $logo = '';

	public function afterFind()
	{
		parent::afterFind();
		$this->logo = $this->hasLogo() ? $this->nomcourt : '';
	}

	public function attributeNames()
	{
		return array_merge(parent::attributeNames(), ["logo"]);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['logo, import', 'boolean'],
			['import', 'in', 'range' => ['', 'O', 'N']],
			['nom, nomlong, nomcourt, url, hdateCrea, hdateModif', 'safe'],
		];
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 */
	public function search(int $pageSize = 25): \IDataProvider
	{
		if (!$this->logo) {
			return $this->searchBySql($pageSize);
		}

		$p = $this->searchBySql(0);
		$p->setPagination(false);
		$rawData = array_filter(
			$p->getData(),
			function ($x) {
				return $x->logo === '';
			}
		);
		return new \CArrayDataProvider(
			$rawData,
			[
				'pagination' => ['pageSize' => $pageSize > 0 ? $pageSize : false],
			]
		);
	}

	private function searchBySql(int $pageSize): \CActiveDataProvider
	{
		$criteria = new \CDbCriteria;

		$criteria->compare('nom', $this->nom, true);
		$criteria->compare('nomlong', $this->nomlong, true);
		$criteria->compare('nomcourt', $this->nomcourt, true);
		$criteria->compare('url', $this->url, true);
		if ($this->import) {
			$criteria->addColumnCondition(['import' => (int) ($this->import === 'O')]);
		}
		$criteria->addCondition(DateValidator::buildTsConditionFromDateValue('hdateCrea', (string) $this->hdateCrea));
		$criteria->addCondition(DateValidator::buildTsConditionFromDateValue('hdateModif', (string) $this->hdateModif));

		$sort = new \CSort();
		$sort->attributes = ['nom', 'hdateCrea', 'hdateModif'];
		$sort->defaultOrder = 'nom ASC';

		return new \CActiveDataProvider(
			$this,
			[
				'criteria' => $criteria,
				'pagination' => ['pageSize' => $pageSize > 0 ? $pageSize : false],
				'sort' => $sort,
			]
		);
	}
}
