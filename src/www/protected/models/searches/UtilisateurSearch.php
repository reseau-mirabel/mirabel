<?php

use models\validators\DateValidator;

class UtilisateurSearch extends Utilisateur
{
	public const BRIEFLISTS= [
		'N' => '(aucune)',
		'C' => 'Contenus',
		'F' => 'Franciliens',
		'L' => 'Lyonnais',
		'CF' => 'Contenus + Franciliens',
		'CL' => 'Contenus + Lyonnais',
	];

	public $actif = 1; //default value

	public $partenaireType;

	public $special;

	public $listes;

	public static $specialValues = [
		'permAdmin' => "perm Administrateur",
		'permImport' => "perm Import",
		'permIndexation' => "perm Indexation",
		'permPartenaire' => "perm Partenaire",
		'permRedaction' => "perm Rédaction",
		'permPolitiques' => "perm Politiques",
		'politiques' => "politiques",
		'suiviEditeurs' => "suivi Éditeurs",
		'suiviNonSuivi' => "suivi Non-suivi",
		'suiviPartenairesEditeurs' => "suivi Partenaires-Éditeurs",
	];

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['partenaireId', 'numerical', 'integerOnly' => true],
			['actif', 'boolean'],
			['special', 'in', 'range' => array_keys(self::$specialValues)],
			['login, nom, prenom, email, '
				. 'derConnexion, hdateCreation, hdateModif, '
				. 'permAdmin, permImport, permPartenaire, permIndexation, permRedaction, permPolitiques, '
				. 'suiviEditeurs, suiviNonSuivi, partenaireType, listes',
				'safe',
			],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array_merge(
			parent::attributeLabels(),
			[
				'partenaireType' => "Type du partenaire",
				'special' => "Perm/Suivi",
				'listes' => "Listes",
			]
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @param int|bool $pageSize Number of items for pagination (false to disable pagination).
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($pageSize = 25)
	{
		if ($this->partenaireId || $this->special === 'politiques') {
			$this->partenaireType = null;
		}
		$this->partenaireId = (int) $this->partenaireId;

		$criteria = new CDbCriteria;
		$criteria->with = ['partenaire'];
		$criteria->select = "t.*, partenaire.type AS partenaireType";

		if ($this->partenaireId < 0) {
			$criteria->addCondition("partenaireId IS NULL");
		} elseif ($this->partenaireId > 0) {
			$criteria->compare('partenaireId', (int) $this->partenaireId);
		}

		if ($this->partenaireType === 'sans') {
			$criteria->addCondition("partenaireId IS NULL");
		} else {
			$criteria->compare('partenaire.type', $this->partenaireType);
		}

		if (isset($this->listes)) {
			switch ($this->listes) {
				case 'N':
					$criteria->addCondition("listeDiffusion = 0 AND listeFranciliens = 0 AND listeLyonnais = 0");
					break;
				case 'C':
					$criteria->addCondition("listeDiffusion = 1");
					break;
				case 'F':
					$criteria->addCondition("listeFranciliens = 1");
					break;
				case 'L':
					$criteria->addCondition("listeLyonnais = 1");
					break;
				case 'CF':
					$criteria->addCondition("listeDiffusion = 1 AND listeFranciliens = 1");
					break;
				case 'CL':
					$criteria->addCondition("listeDiffusion = 1 AND listeLyonnais = 1");
					break;
			}
		}

		$criteria->compare('login', $this->login, true);
		$criteria->compare('t.nom', $this->nom, true);
		$criteria->compare('t.prenom', $this->prenom, true);
		$criteria->compare('t.email', $this->email, true);

		$criteria->compare('actif', $this->actif);
		$criteria->addCondition(DateValidator::buildTsConditionFromDateValue('derConnexion', (string) $this->derConnexion));
		$criteria->addCondition(DateValidator::buildTsConditionFromDateValue('hdateCreation', (string) $this->hdateCreation));
		$criteria->addCondition(DateValidator::buildTsConditionFromDateValue('t.hdateModif', (string) $this->hdateModif));
		$criteria->compare('permAdmin', $this->permAdmin);
		$criteria->compare('permImport', $this->permImport);
		$criteria->compare('permPartenaire', $this->permPartenaire);
		$criteria->compare('permIndexation', $this->permIndexation);
		$criteria->compare('permRedaction', $this->permRedaction);
		$criteria->compare('permPolitiques', $this->permPolitiques);
		$criteria->compare('suiviEditeurs', $this->suiviEditeurs);
		$criteria->compare('suiviNonSuivi', $this->suiviNonSuivi);

		if ($this->special === 'politiques') {
			$criteria->join .= " JOIN Utilisateur_Editeur ue ON ue.utilisateurId = t.id";
			$criteria->group = 't.id';
		} elseif ($this->special) {
			$criteria->addColumnCondition([$this->special => 1]);
		}

		$sort = new CSort();
		$sort->attributes = ['partenaireId', 'login', 'derConnexion', 't.hdateCreation', 't.hdateModif'];
		$sort->defaultOrder = 't.nom ASC, t.prenom ASC';

		return new CActiveDataProvider(
			$this,
			[
				'criteria' => $criteria,
				'pagination' => ($pageSize ? ['pageSize' => $pageSize] : false),
				'sort' => $sort,
			]
		);
	}

	public function getSpecialities()
	{
		$specialities = [];
		foreach (self::$specialValues as $attr => $name) {
			if ($attr !== 'politiques' && $this->{$attr}) {
				$s = explode(" ", $name);
				$specialities[] = '<abbr title="' . $name . '">' . mb_substr($s[0], 0, 1) . mb_substr($s[1], 0, 1) . '</abbr>';
			}
		}
		return join(" ", $specialities);
	}
}
