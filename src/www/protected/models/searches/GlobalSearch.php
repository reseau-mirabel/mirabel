<?php

namespace models\searches;

use CFormModel;
use components\sphinx\Criteria;
use components\sphinx\DataProvider;
use components\sphinx\Schema;
use models\validators\IssnValidator;

/**
 * Searches all objects through Sphinx
 */
class GlobalSearch extends CFormModel
{
	public const INDEXES = ['titres', 'ressources', 'editeurs'];

	public $q;

	/**
	 * @var bool Display only the title that are freely accessible.
	 */
	public $accesLibre = false;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return [
			['q', 'length', 'max' => 255],
			['q', 'filter', 'filter' => function ($x) {
				return trim((string) $x);
			}],

		];
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return [
			'query' => 'Requête',
		];
	}

	public function search(string $index, $pageSize = 25): DataProvider
	{
		$criteria = new Criteria;
		$criteria->order = "cletri ASC";

		switch ($index) {
			case "editeurs":
				$model = \models\sphinx\Editeurs::model();
				break;
			case "ressources":
				$model = \models\sphinx\Ressources::model();
				break;
			case "titres":
				$model = \models\sphinx\Titres::model();
				$this->q = preg_replace('/([A-Z])\.\s?(?=[A-Z]\b)/', '$1', $this->q);
				$criteria->group = "revueid WITHIN GROUP ORDER BY obsolete ASC";
				if ($this->accesLibre) {
					$criteria->addCondition("acceslibre IN (1, 2, 3, 4, 5)"); // any type
				}
				break;
			default:
				throw new \Exception('Paramètre non valide.');
		}

		if ($this->q) {
			$issn = $this->readIssn($this->q);
			if ($index === 'titres' && $issn) {
				$criteria->addCondition("issn = $issn");
			} else {
				$q = $model->getDbConnection()->quoteValue(Schema::quoteFulltext($this->q));
				$criteria->addCondition("MATCH($q)");
			}
		}

		return new DataProvider(
			$model,
			[
				'criteria' => $criteria,
				'pagination' => [
					'pageSize' => $pageSize,
					'params' => array_merge($_GET, ['type' => $index]),
				],
			]
		);
	}

	protected function readIssn($candidate)
	{
		if (!preg_match('/^\d{4}-?\d{3}[\dX]$/', $candidate)) {
			return '';
		}
		$v = new IssnValidator();
		$v->allowEmpty = false;
		$v->allowEmptyChecksum = true;
		$v->allowMissingCaret = true;
		$v->specialValues = [];
		if ($v->validateString($candidate)) {
			return substr(str_replace('-', '', $candidate), 0, 7);
		}
		return '';
	}
}
