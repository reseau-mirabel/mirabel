<?php

use components\sphinx\Criteria;
use components\sphinx\Schema;

class RevueSearch extends CModel
{
	public $titrecomplet;

	public $partenaireId;

	public function attributeNames()
	{
		return ['partenaireId', 'titrecomplet'];
	}

	public function rules()
	{
		return [
			['partenaireId', 'numerical', 'integerOnly' => true],
			['titrecomplet', 'length', 'max' => 255],
		];
	}

	public function search(): \components\sphinx\DataProvider
	{
		$criteria = new Criteria;
		$criteria->group = "revueid WITHIN GROUP ORDER BY obsolete ASC";

		$criteria->select = "*, MAX(hdatemodif) AS r_hdatemodif, MAX(hdateverif) AS r_hdateverif, MAX(vivant) AS r_vivant";
		if (!$this->validate()) {
			$criteria->addCondition("id = 0");
		}
		if ($this->partenaireId) {
			$criteria->addColumnCondition(['suivi' => (int) $this->partenaireId]);
		}
		if ($this->titrecomplet) {
			$q = Yii::app()->db->quoteValue(Schema::quoteFulltext($this->titrecomplet));
			$criteria->addCondition("MATCH($q)");
		}

		$sort = new CSort(self::class);
		$sort->defaultOrder = 'cletri ASC';
		$sort->attributes = [
			'titrecomplet' => ['asc' => 'cletri ASC', 'desc' => 'cletri DESC'],
			'hdatemodif' => ['asc' => 'hdatemodif ASC', 'desc' => 'hdatemodif DESC'],
			'hdateverif' => ['asc' => 'hdateverif ASC', 'desc' => 'hdateverif DESC'],
		];

		return new \components\sphinx\DataProvider(
			\models\sphinx\Revues::model(),
			[
				'criteria' => $criteria,
				'pagination' => false,
				'sort' => $sort,
			]
		);
	}
}
