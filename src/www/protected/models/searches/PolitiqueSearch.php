<?php

namespace models\searches;

use models\validators\DateValidator;

class PolitiqueSearch extends \CModel
{
	public $creator = "";

	public $date = "";

	public $publisher = "";

	public $status = "";

	public function __construct()
	{
		// default scenario is "search"
		$this->setScenario('search');
	}

	public function attributeNames()
	{
		return ['creator', 'date', 'publisher', 'status'];
	}

	public function attributeLabels()
	{
		return [
			'creator' => "Créateur",
			'date' => "Dernière modif.",
			'publisher' => "Éditeur",
			'status' => "Statut",
		];
	}

	public function rules()
	{
		return [
			['creator, publisher, status', 'length', 'max' => 255],
			['date', 'match', 'pattern' => '/^[<>]?\s*\d{4}(-\d\d){0,2}$/', 'allowEmpty' => true],
		];
	}

	public function search(): \CActiveDataProvider
	{
		$criteria = new \CDbCriteria;
		$criteria->with = ['createur', 'editeur']; // joins with Politique.relations()

		if ($this->validate()) {
			if ($this->creator) {
				$criteria->addSearchCondition("createur.nomComplet", $this->creator);
			}
			if ($this->date) {
				$criteria->addCondition(DateValidator::buildTsConditionFromDateValue('lastUpdate', $this->date));
			}
			if ($this->publisher) {
				$criteria->addSearchCondition("editeur.nom", $this->publisher);
			}
			if ($this->status) {
				if (strpos($this->status, ',') === false) {
					$criteria->addColumnCondition(['status' => $this->status]);
				} else {
					$criteria->addInCondition('status', explode(',', $this->status));
				}
			}
		} else {
			$criteria->addCondition('1 = 0');
		}

		$sort = new \CSort();
		$sort->attributes = [
			'creator' => ['asc' => 'createur.nom ASC, createur.prenom ASC', 'desc' => 'createur.nom DESC, createur.prenom DESC'],
			'date' => ['asc' => 'lastUpdate ASC', 'desc' => 'lastUpdate DESC'],
			'publisher' => ['asc' => 'editeur.nom ASC', 'desc' => 'editeur.nom DESC'],
			'status' => ['asc' => 'status ASC', 'desc' => 'status DESC'],
		];
		$sort->defaultOrder = ['date' => \CSort::SORT_DESC];

		return new \CActiveDataProvider(
			\Politique::model(),
			[
				'criteria' => $criteria,
				'pagination' => false,
				'sort' => $sort,
			]
		);
	}
}
