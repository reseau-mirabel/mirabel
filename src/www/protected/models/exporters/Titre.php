<?php

namespace models\exporters;

class Titre
{
	/**
	 * @var int
	 */
	public $partenaireId = 0;

	/**
	 * Prints the CSV lines for the objects matching the revueId list.
	 *
	 * @param string $separator between the CSV cells.
	 * @throws \Exception
	 */
	public function possessionsToCsv($separator = ';'): void
	{
		if (empty($this->partenaireId)) {
			throw new \Exception("exporters/Titre : la configuration de partenaireId est absente.");
		}
		$header = [
			'revue_ID', 'bouquet', 'titre', 'titre_ID', 'issn', 'issne', 'issnl', 'identifiant_local', 'URL_Mirabel',
			'ppn', 'ppne', 'bnfark', 'bnfarke', 'date_de_debut', 'date_de_fin', 'titre_suivant', 'periodicite', 'langues', 'url',
			'acces',
		];
		$sql =
			<<<EOSQL
			WITH Titres_sans_acces AS (
				SELECT pt.titreId, count(s.id) AS acces
				FROM Partenaire_Titre pt
					LEFT JOIN Service s ON s.titreId = pt.titreId
				WHERE pt.partenaireId = {$this->partenaireId}
				GROUP BY pt.titreId
			)
			SELECT
				t.revueId,
				pt.bouquet,
				t.titre,
				t.id,
				GROUP_CONCAT(DISTINCT i1.issn) AS issn,
				GROUP_CONCAT(DISTINCT i2.issn) AS issne,
				GROUP_CONCAT(DISTINCT i3.issnl) AS issnl,
				pt.identifiantLocal,
				'url' AS url,
				GROUP_CONCAT(DISTINCT i1.sudocPpn) AS ppn,
				GROUP_CONCAT(DISTINCT i2.sudocPpn) AS ppne,
				GROUP_CONCAT(DISTINCT i1.bnfArk) AS bnfark,
				GROUP_CONCAT(DISTINCT i2.bnfArk) AS bnfarke,
				IFNULL(t.dateDebut, ''),
				IFNULL(t.dateFin, ''),
				IFNULL(t.obsoletePar, ''),
				t.periodicite,
				t.langues,
				t.url AS titreUrl,
				IF(acces > 0, 'oui', 'non')
			FROM Titre t
				JOIN Partenaire_Titre pt ON (pt.titreId = t.id AND pt.partenaireId = {$this->partenaireId})
				LEFT JOIN Issn i1 ON i1.titreId = t.id AND i1.support = :sp
				LEFT JOIN Issn i2 ON i2.titreId = t.id AND i2.support = :se
				LEFT JOIN Issn i3 ON i3.titreId = t.id AND i3.issnl IS NOT NULL
				LEFT JOIN Titres_sans_acces tsa ON t.id = tsa.titreId
			GROUP BY t.id
			ORDER BY t.revueId, t.dateDebut DESC;
			EOSQL;

		$out = fopen('php://output', 'w');
		fputcsv($out, $header, $separator, '"', '\\');
		$query = \Yii::app()->db->createCommand($sql)->query([
			':sp' => \Issn::SUPPORT_PAPIER,
			':se' => \Issn::SUPPORT_ELECTRONIQUE,
		]);
		foreach ($query as $row) {
			if ($row['langues']) {
				$row['langues'] = join(", ", json_decode($row['langues']));
			}
			$row['url'] = \Yii::app()->createAbsoluteUrl('revue/view', ['id' => $row['revueId']]);
			fputcsv($out, $row, $separator, '"', '\\');
		}
	}
}
