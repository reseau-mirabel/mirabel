<?php

namespace models\exporters;

class Revue
{
	/**
	 * @var int
	 */
	public $partenaireId = 0;

	private string $separator = ";";

	/**
	 * @param string $separator between the CSV cells.
	 */
	public function __construct(string $separator)
	{
		$this->separator = $separator;
	}

	public function csvHeader(): string
	{
		if ($this->partenaireId) {
			$header = [
				'possession', 'bouquet', 'titre', 'titre ID', 'issn', 'issne', 'issnl', 'revue ID', 'identifiant local', 'URL Mir@bel',
				'ppn', 'ppne', 'bnfark', 'bnfarke', 'date de début', 'date de fin', 'titre suivant', 'périodicité', 'langues', 'url',
			];
		} else {
			$header = ['titre', 'ID de titre', 'ID de revue', 'ISSN', 'ISSN-E', "préfixe du titre", "sigle", 'URL Mir@bel'];
		}
		$out = fopen('php://memory', 'w');
		fputcsv($out, $header, $this->separator, '"', '\\');
		return (string) stream_get_contents($out, -1, 0);
	}

	/**
	 * Prints the CSV lines for the objects matching the revueId list.
	 *
	 * @param int[] $ids array of revueId.
	 * @throws \Exception
	 */
	public function toCsv(array $ids): string
	{
		$out = fopen('php://memory', 'w');
		$conditions = self::buildConditions($ids);
		foreach ($conditions as $condition) {
			$this->appendToCsv($out, $condition);
		}
		return (string) stream_get_contents($out, -1, 0);
	}

	private function appendToCsv($out, string $condition): void
	{
		if ($this->partenaireId) {
			$sql =
				<<<EOSQL
				SELECT
					pt.partenaireId IS NOT NULL,
					pt.bouquet,
					t.titre,
					t.id,
					GROUP_CONCAT(DISTINCT i1.issn) AS issn,
					GROUP_CONCAT(DISTINCT i2.issn) AS issne,
					GROUP_CONCAT(DISTINCT i3.issnl) AS issnl,
					t.revueId,
					pt.identifiantLocal,
					'urlM' AS url,
					GROUP_CONCAT(DISTINCT i1.sudocPpn) AS ppn,
					GROUP_CONCAT(DISTINCT i2.sudocPpn) AS ppne,
					GROUP_CONCAT(DISTINCT i1.bnfArk) AS bnfark,
					GROUP_CONCAT(DISTINCT i2.bnfArk) AS bnfarke,
					IFNULL(t.dateDebut, ''),
					IFNULL(t.dateFin, ''),
					IFNULL(t.obsoletePar, ''),
					t.periodicite,
					t.langues,
					t.url AS titreUrl
				FROM Titre t
					LEFT JOIN Partenaire_Titre pt ON (pt.titreId = t.id AND partenaireId = {$this->partenaireId})
					LEFT JOIN Issn i1 ON i1.titreId = t.id AND i1.support = :sp
					LEFT JOIN Issn i2 ON i2.titreId = t.id AND i2.support = :se
					LEFT JOIN Issn i3 ON i3.titreId = t.id AND i3.issnl IS NOT NULL
				WHERE
					t.obsoletePar IS NULL $condition
				GROUP BY t.id
				ORDER BY t.titre;
				EOSQL;
		} else {
			$sql =
				<<<EOSQL
				SELECT
					t.titre,
					t.id,
					t.revueId,
					GROUP_CONCAT(DISTINCT i1.issn) AS issn,
					GROUP_CONCAT(DISTINCT i2.issn) AS issne,
					t.prefixe,
					t.sigle,
					'url' AS url
				FROM Titre t
					LEFT JOIN Issn i1 ON i1.titreId = t.id AND i1.support = :sp
					LEFT JOIN Issn i2 ON i2.titreId = t.id AND i2.support = :se
				WHERE
					t.obsoletePar IS NULL $condition
				GROUP BY t.id
				ORDER BY t.titre
				EOSQL;
		}

		$query = \Yii::app()->db->createCommand($sql)->query([
			':sp' => \Issn::SUPPORT_PAPIER,
			':se' => \Issn::SUPPORT_ELECTRONIQUE,
		]);
		foreach ($query as $row) {
			if (!empty($row['langues'])) {
				$row['langues'] = join(", ", json_decode($row['langues']));
			}
			$row['url'] = \Yii::app()->createAbsoluteUrl('revue/view', ['id' => $row['revueId']]);
			fputcsv($out, $row, $this->separator, '"', '\\');
		}
	}

	/**
	 * @return string[] each string is a part of a SQL condition.
	 */
	private static function buildConditions(array $ids): array
	{
		if (!$ids) {
			return [];
		}
		$chunks = array_chunk($ids, 1000);
		$conditions = [];
		foreach ($chunks as $c) {
			$conditions[] = "AND t.revueId IN (" . join(',', array_map('intval', $c)) . ")";
		}
		return $conditions;
	}
}
