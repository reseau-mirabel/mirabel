<?php

use components\SqlHelper;
use models\sudoc\ApiClient;
use models\sudoc\Notice;

/**
 * This is the model class for table "Issn".
 *
 * The followings are the available columns in table 'Issn':
 * @property int $id
 * @property int $titreId
 * @property ?string $issn
 * @property string $support
 * @property int $statut
 * @property string $titreReference
 * @property ?string $issnl
 * @property string $dateDebut
 * @property string $dateFin
 * @property string $pays 2 letters code (ISO 3166-1:2006)
 * @property ?string $sudocPpn
 * @property bool $sudocNoHolding
 * @property ?int $worldcatOcn
 * @property string $bnfArk
 *
 * @property Titre $titre
 * @property array $etatcollections
 */
class Issn extends CActiveRecord
{
	public const SUPPORT_ELECTRONIQUE = 'electronique';

	public const SUPPORT_INCONNU = 'inconnu';

	public const SUPPORT_PAPIER = 'papier';

	public const STATUT_VALIDE = 0;

	public const STATUT_ERRONE = 1;

	public const STATUT_ENCOURS = 2;

	public const STATUT_ANNULE = 3;

	public const STATUT_SANS = 4;

	public static $enumSupport = [
		self::SUPPORT_PAPIER => "Papier (ISSN)",
		self::SUPPORT_ELECTRONIQUE => "Électronique (ISSN-E)",
		self::SUPPORT_INCONNU => "Indéterminé",
	];

	public static $enumStatut = [
		self::STATUT_VALIDE => "Valide",
		self::STATUT_ERRONE => "Erroné",
		self::STATUT_ANNULE => "Annulé",
		self::STATUT_ENCOURS => "En cours",
		self::STATUT_SANS => "Sans ISSN",
	];

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Issn';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['issn', 'length', 'max' => 10],
			['support', 'required'],
			['issnl', \models\validators\IssnValidator::class, 'type' => 'ISSN-L'],
			['worldcatOcn, titreId', 'numerical', 'integerOnly' => true, 'allowEmpty' => true],
			['sudocPpn', \models\validators\PpnValidator::class],
			['pays', 'length', 'max' => 2],
			['bnfArk', 'length', 'max' => 100],
			['sudocNoHolding', 'boolean'],
			['titreReference', 'length', 'max' => 512],
			['support', 'in', 'range' => array_keys(self::$enumSupport)],
			['statut', 'numerical', 'integerOnly' => true, 'min' => 0, 'max' => 4],
			['dateDebut, dateFin', 'length', 'max' => 10],
		];
	}

	public function beforeValidate()
	{
		foreach (['dateDebut', 'dateFin', 'issn', 'issnl', 'sudocPpn'] as $k) {
			if ($this->getAttribute($k)) {
				$this->setAttribute($k, trim((string) $this->getAttribute($k), " \t \n​"));
			}
		}
		foreach (['id', 'issn', 'issnl', 'sudocPpn', 'worldcatOcn'] as $k) {
			if (!$this->getAttribute($k)) {
				$this->setAttribute($k, null);
			}
		}
		if ($this->issn) {
			$this->issn = strtoupper($this->issn);
		}
		return parent::beforeValidate();
	}

	public function afterValidate()
	{
		if (strpos((string) $this->sudocPpn, '/') !== false) {
			$this->sudocPpn = preg_replace('/^.+\//', '', $this->sudocPpn);
		}
		if ((int) $this->statut === self::STATUT_SANS) {
			if ($this->issn) {
				$this->addError('issn', "Le numéro doit être vide quand le statut est à <em>sans ISSN</em>.");
			}
		} elseif ((int) $this->statut === self::STATUT_ENCOURS) {
			if ($this->issn) {
				$this->addError('issn', "Le numéro doit être vide quand le statut est à <em>en cours</em>.");
			}
		} elseif (empty($this->issn)) {
			$this->addError('issn', "L'ISSN doit être rempli si le statut n'est ni 'en cours' ni 'sans ISSN'.");
		} else {
			if (in_array($this->statut, [self::STATUT_VALIDE, self::STATUT_ANNULE])) {
				$validator = CValidator::createValidator(\models\validators\IssnValidator::class, $this, 'issn', ['type' => 'ISSN']);
				$validator->validate($this);
			}
		}
		$m = [];
		if ($this->bnfArk) {
			if (preg_match('#^https?://catalogue\.bnf\.fr/ark:/12148/(cb\d+[a-z]?)$#', trim($this->bnfArk), $m)) {
				$this->bnfArk = $m[1];
			}
		} else {
			$this->setAttribute('bnfArk', null);
		}
		foreach (['dateDebut', 'dateFin'] as $attr) {
			if ($this->{$attr}) {
				$this->{$attr} = str_replace(['–', '—', '‒', '−', '‐'], '-', $this->{$attr});
				$this->{$attr} = trim(preg_replace('/[?.]$/', 'X', $this->{$attr}));
				if (!preg_match('/^(\d{4}(-\d\d){0,2}|\d\d[\dX]{2})$/', $this->{$attr})) {
					$this->addError($attr, "Une date de validité doit respecter soit le format AAAA[[-MM[-DD]] (2017-04), soit au plus deux chiffres indéterminés (201X).");
				}
				if ($this->{$attr} < "1500") {
					$this->addError($attr, "Une date de validité ne peut être antérieure à 1500.");
				}
			}
		}
		if ($this->dateDebut && $this->dateFin && str_replace('X', '0', $this->dateDebut) > str_replace('X', '9', $this->dateFin)) {
			$this->addError($attr, "La date de validité de début doit être antérieure à celle de fin.");
		}

		if ($this->scenario === 'import') {
			// No duplicate values at all.
			$byIssn = Titre::model()
				->findBySql(
					"SELECT t.* FROM Issn i JOIN Titre t ON t.id = i.titreId WHERE i.issn = :issn AND i.id <> :id",
					[':issn' => $this->issn, ':id' => (int) $this->id]
				);
			if ($byIssn instanceof Titre) {
				$this->addError('issn', "Cet ISSN {$this->issn} est en conflit avec l'ISSN du titre « {$byIssn->titre} » [{$byIssn->id}], revue d'ID {$byIssn->revueId}.");
				return false;
			}

			// A single value for each medium (support).
			if ($this->support !== self::SUPPORT_INCONNU && $this->statut === self::STATUT_VALIDE) {
				$issnSameMedium = (string) Yii::app()->db
					->createCommand("SELECT issn FROM Issn WHERE titreId = :tid AND support = :su AND statut = :st AND id <> :id LIMIT 1")
					->queryScalar([':tid' => (int) $this->titreId, ':su' => $this->support, ':st' => self::STATUT_VALIDE, ':id' => (int) $this->id]);
				if ($this->id && $issnSameMedium) {
					// Are we changing support or status?
					$oldValues = Yii::app()->db
						->createCommand("SELECT statut, support FROM Issn WHERE id = :id LIMIT 1")
						->queryRow(true, [':id' => (int) $this->id]);
					if ((int) $oldValues['statut'] === (int) $this->statut && $oldValues['support'] === $this->support) {
						// We're not updating the relevant fields, so allow the changes.
						$issnSameMedium = '';
					}
				}
				if ($issnSameMedium) {
					$this->addError('issn', "Ce titre a déjà un ISSN '{$issnSameMedium}' de même support '{$this->support}', et un import ne peut créer de doublon.");
				}
			}
		}

		return parent::afterValidate();
	}

	/**
	 * Return false and add errors if the ISSN is not unique in the DB.
	 */
	public function validateUnicity(Titre $titre): bool
	{
		if (!in_array($this->support, [Issn::SUPPORT_ELECTRONIQUE, Issn::SUPPORT_PAPIER])) {
			return true;
		}
		if ($this->statut != Issn::STATUT_VALIDE) {
			return true;
		}

		if (isset($this->support) && in_array($this->support, [Issn::SUPPORT_ELECTRONIQUE, Issn::SUPPORT_PAPIER])) {
			$dup = Issn::model()->findByAttributes([
				'issn' => $this->issn,
				'support' => ($this->support == Issn::SUPPORT_ELECTRONIQUE ? Issn::SUPPORT_PAPIER : Issn::SUPPORT_ELECTRONIQUE),
			]);
			if ($dup && (empty($this->id) || $dup->id != $this->id)) {
				$this->addError('issn', "Cet ISSN {$this->issn} est déjà déclaré comme ISSN de type différent (papier|électronique).");
				return false;
			}
		}

		if ($this->support === Issn::SUPPORT_ELECTRONIQUE) {
			// May be duplicated, but only in the same Revue
			$byIssn = Titre::model()
				->findBySql(
					"SELECT t.* FROM Issn i JOIN Titre t ON t.id = i.titreId WHERE i.issn = :issn AND t.revueId <> :revueId",
					[':issn' => $this->issn, ':revueId' => (int) $titre->revueId]
				);
		} else { // support papier, hors import
			// Duplicates are not allowed. (TODO same case as 'import'?)
			$byIssn = Titre::model()
				->findBySql(
					"SELECT t.* FROM Issn i JOIN Titre t ON t.id = i.titreId WHERE i.issn = :issn AND t.id <> :titreId",
					[':issn' => $this->issn, ':titreId' => (int) $titre->id]
				);
		}
		if ($byIssn instanceof Titre) {
			$this->addError('issn', "Cet ISSN {$this->issn} est en conflit avec l'ISSN du titre « {$byIssn->titre} » [{$byIssn->id}], revue d'ID {$byIssn->revueId}.");
			return false;
		}

		// ISSN = ISSN-L is okay, but only in the same Revue
		$byIssnl = Titre::model()
			->findBySql(
				"SELECT t.* FROM Issn i JOIN Titre t ON t.id = i.titreId WHERE i.issnl = :issn AND t.revueId != :revueId",
				[':issn' => $this->issn, ':revueId' => (int) $titre->revueId]
			);
		if ($byIssnl) {
			$this->addError('issn', "Cet ISSN {$this->issn} est en conflit avec l'ISSN-L du titre « {$byIssnl->titre} » [{$byIssnl->id}], revue d'ID {$byIssnl->revueId}.");
			return false;
		}

		return true;
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'titre' => [self::BELONGS_TO, 'Titre', 'titreId'],
			'etatcollections' => [self::HAS_MANY, 'Etatcollection', 'issnId'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'titreId' => 'Titre',
			'issn' => 'ISSN',
			'support' => "Support",
			'statut' => "Statut",
			'titreReference' => 'Titre-clé',
			'issnl' => 'ISSN de liaison',
			'dateDebut' => "Date de début d'ISSN",
			'dateFin' => "Date de fin d'ISSN",
			'pays' => "Pays",
			'country' => "Pays notice ISSN",
			'sudocPpn' => 'PPN (SUDOC)',
			'sudocNoHolding' => 'Sans notice SUDOC',
			'worldcatOcn' => 'OCLC (Worldcat)',
			'bnfArk' => "BNF Ark",
		];
	}

	/**
	 * Overload setAttributes() so that invalid country codes are ignored.
	 *
	 * @staticvar null|array $countries
	 * @param array $values
	 * @param bool $safeOnly
	 */
	public function setAttributes($values, $safeOnly = true)
	{
		static $countries = null;
		if ($countries === null) {
			$countries = SqlHelper::sqlToPairs("SELECT code2, 1 FROM Pays ORDER BY nom");
			$countries[''] = '';
		}

		parent::setAttributes($values, $safeOnly);

		if (!isset($countries[$this->pays])) {
			$this->pays = '';
		}
	}

	/**
	 * Modify the array by loading the POST data into existing and new records.
	 *
	 * New records must have the key "new...",
	 * deleted record must have a field "_delete" with non-empty value.
	 *
	 * self.isEmpty() is used to ignore the records to add that are empty.
	 *
	 * @return array{insert?: array, update?: array, delete?: array}
	 */
	public static function loadMultiple(array &$array, array $post): array
	{
		$className = __CLASS__;
		if (!isset($post[$className])) {
			return [];
		}
		$newData = $post[$className];
		$changes = ['insert' => [], 'update' => [], 'delete' => []];
		foreach ($array as $id => $existingRecord) {
			if (!empty($newData[$id]['_delete'])) {
				$changes['delete'][] = [
					'before' => $existingRecord,
				];
				unset($array[$id]);
			} elseif (isset($newData[$id])) {
				$previous = clone ($existingRecord);
				$array[$id]->setAttributes($newData[$id]);
				if ($array[$id]->attributes != $previous->attributes) {
					$changes['update'][] = [
						"before" => $previous,
						"after" => $array[$id],
					];
				}
			}
		}
		foreach ($newData as $id => $newAttr) {
			if (strncmp('new', $id, 3) === 0 && empty($newData[$id]['_delete'])) {
				$newRecord = new self;
				$newRecord->issn = $newAttr['issn'];
				$newRecord->fillBySudoc();
				$newRecord->setAttributes($newAttr);
				assert($newRecord instanceof Issn);
				if (!$newRecord->isEmpty()) {
					$array[$id] = $newRecord;
					$changes['insert'][] = [
						"after" => $newRecord,
					];
				}
			}
		}
		return array_filter($changes);
	}

	/**
	 * @param Issn[] $array
	 */
	public static function validateMultiple(array $array, Titre $titre): bool
	{
		$status = true;
		if ($array) {
			foreach ($array as $a) {
				$status = $a->validate() && $a->validateUnicity($titre) && (!$a->hasErrors()) && $status;
			}
		}
		return $status;
	}

	public function isEmpty(): bool
	{
		return (
			(int) $this->statut !== self::STATUT_ENCOURS
			&& (int) $this->statut !== self::STATUT_SANS
			&& !trim($this->issn)
			&& empty($this->sudocPpn)
			&& empty($this->worldcatOcn)
		);
	}

	public function getHtml(bool $microdata = false, bool $link = false): string
	{
		$options = ['title' => "International Standard Serial Number - " . self::$enumSupport[$this->support]];
		if ($this->statut == self::STATUT_ENCOURS || $this->statut == self::STATUT_SANS) {
			return CHtml::tag('span', $options, self::$enumStatut[$this->statut]);
		}
		if ($this->statut == self::STATUT_VALIDE) {
			$options['class'] = 'issn-number';
			if ($microdata) {
				$options['itemprop'] = 'issn';
			}
		} else {
			$options['class'] = 'issn-bad';
			$options['title'] .= " - " . Issn::$enumStatut[$this->statut];
		}
		if ($this->issn && !preg_match('/^\d{4}-\d{3}[\dX]$/', $this->issn)) {
			return '<span class="issn-bad">ISSN malformé</span>';
		}
		if ($link) {
			$options['target'] = '_blank';
			$options['title'] .= ' (nouvel onglet)';
			return CHtml::link($this->issn, "https://portal.issn.org/resource/ISSN/{$this->issn}", $options);
		}
		return CHtml::tag('span', $options, $this->issn);
	}

	/**
	 * @return string HTML
	 */
	public function getSudocLink(bool $icon = true): string
	{
		if (!$this->sudocPpn) {
			return '';
		}
		if ($icon && $this->sudocNoHolding) {
			return '';
		}
		$url = sprintf(Titre::URL_SUDOC, $this->sudocPpn);
		if ($icon) {
			$content = CHtml::image(
				Yii::app()->baseUrl . '/images/icons/sudoc.png',
				'SUDOC',
				['title' => 'SUDOC : Catalogue du Système Universitaire de Documentation - France']
			);
		} else {
			$content = $this->sudocPpn;
		}
		if ($this->sudocNoHolding) {
			return CHtml::tag('abbr', ['class' => 'sudoc-noholding', 'title' => "Pas de notice publique dans le Sudoc (no holding)"], htmlspecialchars($content));
		}
		return CHtml::link($content, $url, ['class' => ($icon ? 'noexternalicon' : ''), 'target' => '_blank', 'title' => "Notice SUDOC (nouvel onglet)"]);
	}

	public function getCountry(): string
	{
		if (!$this->pays) {
			return '';
		}
		if (strtoupper($this->pays) === 'ZZ') {
			return "international";
		}
		$name = $this->dbConnection
			->createCommand("SELECT nom FROM Pays WHERE code2 = :c")
			->queryScalar([':c' => strtoupper($this->pays)]);
		return $name ?: $this->pays;
	}

	public function getBnfLink(): string
	{
		if (!$this->bnfArk) {
			return '';
		}
		return CHtml::link(
			htmlspecialchars($this->bnfArk),
			sprintf(Titre::URL_BNF, $this->bnfArk),
			['target' => '_blank', 'title' => "Notice à la Bibliothèque Nationale de France (nouvel onglet)"]
		);
	}

	/**
	 * @param Partenaire $institute
	 * @throws CException
	 * @return string[] Liste des textes d'état de collection liés à l'établissement pour cet ISSN.
	 */
	public function getEtatcollection(Partenaire $institute): array
	{
		$rcrList = json_decode($institute->rcr, true);
		if (empty($rcrList)) {
			return [];
		}
		$rcrListString = "'" . implode("','", $rcrList) . "'";
		$etatsJson = \Yii::app()->db
			->createCommand(<<<EOSQL
				SELECT etats
				FROM Etatcollection
				WHERE issnId = {$this->id}
					AND rcr IN ({$rcrListString})
				EOSQL
			)->queryColumn();

		$etatsList = [];
		foreach ($etatsJson as $etatJson) {
			$etatList = json_decode($etatJson, true, 512, JSON_THROW_ON_ERROR);
			$etatsList = array_merge($etatsList, $etatList);
		}
		return $etatsList;
	}

	public function getWorldcatLink(): string
	{
		if (!$this->worldcatOcn) {
			return '';
		}
		return CHtml::link(
			htmlspecialchars((string) $this->worldcatOcn),
			sprintf(Titre::URL_WORLDCAT, $this->worldcatOcn),
			['target' => '_blank', 'title' => "Notice Worldcat (nouvel onglet)"]
		);
	}

	/**
	 * Query the Sudoc web services twice, first by ISSN, then by PPN, and fill in the attributes.
	 */
	public function fillBySudoc(?ApiClient $sudocApi = null): void
	{
		if (!$this->issn) {
			return;
		}
		if ($sudocApi === null) {
			$sudocApi = new ApiClient();
		}
		try {
			$notice = $sudocApi->getNoticeByIssn($this->issn);
			if ($notice) {
				$this->fillBySudocNotice($notice);
			}
		} catch (\Throwable $e) {
			Yii::log("L'ISSN {$this->issn} n'a pu être complété par le Sudoc : {$e->getMessage()}", 'warning');
		}
	}

	public function fillBySudocNotice(Notice $notice): void
	{
		$this->setAttributes(
			array_filter(
				[
					'bnfArk' => $notice->bnfArk,
					'dateDebut' => $notice->dateDebut,
					'dateFin' => $notice->dateFin,
					'issn' => $notice->issn,
					'issnl' => $notice->issnl,
					'pays' => $notice->pays,
					'support' => $notice->support,
					'statut' => $notice->statut,
					'sudocPpn' => $notice->ppn,
					'sudocNoHolding' => $notice->sudocNoHolding,
					'titreReference' => $notice->titreIssn,
					'worldcatOcn' => $notice->worldcat,
				],
				function ($x) {
					return $x !== null && $x !== '';
				}
			),
			false
		);
	}
}
