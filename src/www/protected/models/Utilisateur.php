<?php

use components\Tools;

/**
 * This is the model class for table "Utilisateur".
 *
 * The followings are the available columns in table 'Utilisateur':
 * @property int $id
 * @property int $partenaireId
 * @property string $login
 * @property ?string $email
 * @property string $nom
 * @property string $prenom
 * @property string $nomComplet
 * @property string $notes
 * @property string $message
 * @property bool $actif
 * @property int $derConnexion timestamp
 * @property int $hdateCreation timestamp
 * @property int $hdateModif timestamp
 * @property bool $permAdmin
 * @property bool $permImport
 * @property bool $permPartenaire
 * @property bool $permIndexation
 * @property bool $permRedaction
 * @property bool $permPolitiques
 * @property bool $suiviEditeurs
 * @property bool $suiviNonSuivi
 * @property bool $suiviPartenairesEditeurs
 * @property string $motdepasse
 * @property int $authmethod
 * @property bool $listeDiffusion
 * @property bool $listeFranciliens
 * @property bool $listeLyonnais
 *
 * @property Editeur[] $editeurs
 * @property ?Partenaire $partenaire
 */
class Utilisateur extends CActiveRecord implements TitledObject, JsonSerializable
{
	public const AUTHMETHOD_LDAP = 1;

	public const AUTHMETHOD_PASSWORD = 2;

	public const AUTHMETHOD_LEGACY = 3;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Utilisateur';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		$rules = [
			['email, nom, prenom', 'length', 'max' => 100],
			['nomComplet', 'length', 'max' => 255],
		];
		if (PHP_SAPI === 'cli' || Yii::app()->user->access()->toUtilisateur()->admin()) {
			array_push(
				$rules,
				['partenaireId, login', 'required'],
				['login', 'match', 'pattern' => '/^[\w\d.-]+$/'], // alphanum + "._-"
				['authmethod, partenaireId', 'numerical', 'integerOnly' => true],
				['login', 'length', 'max' => 50],
				['login', 'unique', 'on' => ['insert']],
				['notes', 'length', 'max' => 65535],
				[
					'actif, permAdmin, permImport, permPartenaire, permIndexation, permRedaction, permPolitiques,'
					. ' suiviEditeurs, suiviNonSuivi, suiviPartenairesEditeurs,'
					. ' listeDiffusion, listeFranciliens, listeLyonnais',
					'boolean',
				]
			);
		}
		return $rules;
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'partenaire' => [self::BELONGS_TO, 'Partenaire', 'partenaireId'],
			'editeurs' => [
				self::MANY_MANY,
				'Editeur',
				'Utilisateur_Editeur(utilisateurId, editeurId)',
				'index' => 'id',
				'order' => 'editeurs.nom',
			],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'partenaireId' => 'Partenaire',
			'login' => 'Login',
			'email' => 'Adresse électronique',
			'nom' => 'Nom',
			'prenom' => 'Prénom',
			'nomComplet' => 'Nom complet',
			'notes' => "Notes privées",
			'actif' => 'Actif',
			'derConnexion' => 'Dernière connexion',
			'hdateCreation' => 'Date de création',
			'hdateModif' => 'Modifié',
			'permAdmin' => 'Administrateur',
			'permImport' => 'Permission d\'import',
			'permPartenaire' => 'Permission sur son partenaire',
			'permIndexation' => 'Permission d\'indexation',
			'permRedaction' => 'Permission éditoriale globale',
			'permPolitiques' => 'Validation des politiques',
			'suiviEditeurs' => 'Suivi des éditeurs',
			'suiviNonSuivi' => 'Suivi des objets non suivis',
			'suiviPartenairesEditeurs' => "Suivi des partenaires-éditeurs",
			'motdepasse' => 'Mot de passe',
			'passwordDuplicate' => 'Mot de passe (bis)',
			'authmethod' => 'Authentification',
			'listeDiffusion' => "Liste de diffusion Contenus",
			'listeFranciliens' => "Liste de diffusion Franciliens",
			'listeLyonnais' => "Liste de diffusion Lyonnais",
		];
	}

	public function storePassword(string $plain): void
	{
		$this->motdepasse = password_hash($plain, PASSWORD_DEFAULT);
		$this->authmethod = self::AUTHMETHOD_PASSWORD;
	}

	/**
	 * Validates the given password according to the authentication method.
	 *
	 * @param string $password
	 * @return bool Success?
	 */
	public function validatePassword(string $password): bool
	{
		if (!$this->actif) {
			Yii::log("Login : essai de connexion de l'utilisateur inactif : {$this->login}", "warning");
			return false;
		}
		if ($this->partenaireId && $this->partenaire->statut === 'inactif') {
			Yii::log("Login : essai de connexion pour un partenaire inactif : {$this->login}", "warning");
			return false;
		}
		switch ($this->authmethod) {
			case self::AUTHMETHOD_PASSWORD:
				return $this->validateLocalPassword($password);
			case self::AUTHMETHOD_LDAP:
				return $this->validateLdapPassword($password);
			default:
				throw new \Exception("Unknown auth method");
		}
	}

	/**
	 * Find a record by login or email.
	 *
	 * @param string $login
	 * @return Utilisateur|null
	 */
	public static function findByName(string $login): ?Utilisateur
	{
		try {
			return self::model()->find(
				"actif = 1 AND login = :l OR email = :e",
				[':l' => $login, ':e' => $login]
			);
		} catch (\Throwable $_) {
			return null;
		}
	}

	/**
	 * Generate a new random password of 8 characters.
	 *
	 * @return string
	 */
	public static function generateRandomPassword(): string
	{
		$from = [
			["azertyuiopqsdfghjkmwxcvbnAZERTYUOPQSDFGHJKMWXCVBN", 5],
			["0123456789", 2],
			[",;:!?./%&()=+-", 1],
		];
		$password = '';
		foreach ($from as $source) {
			$password .= substr(str_shuffle($source[0]), 0, $source[1]);
		}
		return str_shuffle($password);
	}

	/**
	 * @return array [ ['role' => '...', 'editeur' => Editeur] ]
	 */
	public function getEditeursRoles(): array
	{
		$data = $this->dbConnection->createCommand(
			<<<EOSQL
			SELECT ue.role AS userRole, e.*
			FROM Utilisateur_Editeur ue
			  JOIN Editeur e ON ue.editeurId = e.id
			WHERE ue.utilisateurId = :uid
			ORDER BY e.nom
			EOSQL
		)->query([':uid' => $this->id]);
		$result = [];
		foreach ($data as $row) {
			$result[] = [
				'role' => $row['userRole'],
				'editeur' => Editeur::model()->populateRecord($row, false),
			];
		}
		return $result;
	}

	public function getSelfLink(bool $fullname = true): string
	{
		if ($this->id) {
			return CHtml::link(
				CHtml::encode($fullname ? $this->nomComplet : $this->login),
				['/utilisateur/view', 'id' => $this->id],
				['title' => $this->partenaire->nom]
			);
		}
		return CHtml::encode($fullname ? $this->nomComplet : $this->login);
	}

	/**
	 * Return HTML suitable for flash messages in the session.
	 */
	public function getLoginMessages(): string
	{
		$messages = [];
		$collections = Collection::model()
			->with(['ressource'])
			->findAllBySql(
				"SELECT c.* FROM Abonnement a JOIN Collection c ON c.id = a.collectionId"
				. " WHERE c.type = :type AND a.partenaireId = " . (int) $this->partenaireId,
				[':type' => Collection::TYPE_TEMPORAIRE]
			);
		if ($collections) {
			$message = "<p>Vous êtes abonné à des collections temporaires qui ont vocation à disparaître prochainement. "
				. "Il faut probablement supprimer ces reliquats et vous abonner à d'autres collections de ces ressources. "
				. "<ul>";
			foreach ($collections as $c) {
				/** @var Collection $c */
				$message .= "<li>Ressource " . $c->ressource->getSelfLink() . " : " . $c->nom . "</li>\n";
			}
			$message .= "</ul>\n";
			$messages[] = $message;
		}
		return join("<br /><br />", $messages);
	}

	/**
	 * @inheritdoc
	 */
	public function save($runValidation = true, $attributes = null): bool
	{
		$isCreation = $this->getIsNewRecord();
		$saved = parent::save($runValidation, $attributes);
		if ($saved && $isCreation && $this->partenaireId && $this->partenaire->editeurId > 0) {
			// Propose the new user as a manager of the publisher policy.
			Yii::app()->db
				->createCommand("INSERT INTO Utilisateur_Editeur (utilisateurId, editeurId, role, confirmed) VALUES (:uid, :eid, :role, 0)")
				->execute([
					':uid' => (int) $this->id,
					':eid' => (int) $this->partenaire->editeurId,
					':role' => Politique::ROLE_OWNER, // Should role depend on existing owners of this Editeur?
				]);
		}
		return $saved;
	}

	#[\ReturnTypeWillChange]
	public function jsonSerialize(): array
	{
		$a = $this->getAttributes();
		foreach (['id', 'partenaireId', 'derConnexion', 'hdateCreation', 'hdateModif'] as $k) {
			if (isset($a[$k])) {
				if ($a[$k]) {
					$a[$k] = (int) $a[$k];
				} else {
					$a[$k] = null;
				}
			}
		}
		foreach (['actif', 'listeDiffusion', 'listeFranciliens', 'listeLyonnais'] as $k) {
			if (isset($a[$k])) {
				$a[$k] = (bool) $a[$k];
			}
		}
		foreach (['motdepasse', 'authmethod'] as $k) {
			if (isset($a[$k])) {
				unset($a[$k]);
			}
		}
		return $a;
	}

	/**
	 * @inheritdoc
	 */
	protected function afterFind()
	{
		// Do not load the email fields of deleted users.
		if ((bool) $this->actif === false && strncmp($this->login, '_deleted_', 9) === 0) {
			$this->email = null;
			$this->nomComplet = "Ancien utilisateur";
		}
		return parent::afterFind();
	}

	/**
	 * Called automatically before save().
	 *
	 * @return bool
	 */
	protected function beforeSave()
	{
		if ($this->getIsNewRecord()) {
			$this->hdateCreation = $_SERVER['REQUEST_TIME'];
		}
		$this->hdateModif = $_SERVER['REQUEST_TIME'];

		if (empty($this->suiviPartenairesEditeurs)) {
			$this->suiviPartenairesEditeurs = false; // null and "" converted to false
		}

		if (empty($this->nomComplet)) {
			$this->nomComplet = trim("{$this->prenom} {$this->nom}");
		}
		$this->nom = Tools::normalizeText($this->nom);
		$this->prenom = Tools::normalizeText($this->prenom);
		$this->nomComplet = Tools::normalizeText($this->nomComplet);

		if ($this->id > 0 && !$this->actif) {
			// Rights on a publisher
			\Yii::app()->db->createCommand("DELETE FROM Utilisateur_Editeur WHERE utilisateurId = {$this->id}")->execute();
		}
		return parent::beforeSave();
	}

	/**
	 * Updates and saves the attributes whose values have changed in the LDAP.
	 *
	 * @param array $info Result of ldap_info().
	 */
	protected function updateFromLdap($info)
	{
		$attrNames = [
			'mail' => 'email',
			'sn' => 'nom',
			'givenname' => 'prenom',
			// Do not read 'cn' since its format is not suitable for M.
		];
		$values = [];
		foreach ($attrNames as $k => $v) {
			if (!empty($info[0][$k][0]) && $info[0][$k][0] != $this->{$v}) {
				$values[$v] = $info[0][$k][0];
			}
		}
		if (isset($values['nom']) || isset($values['prenom'])) {
			$values['nomComplet'] = trim("{$this->prenom} {$this->nom}");
		}
		if ($values) {
			$this->saveAttributes($values);
		}
	}

	private function validateLdapPassword(string $password): bool
	{
		if (PHP_SAPI === 'cli' && YII_ENV === 'test') {
			return true;
		}
		if (!Yii::app()->params->itemAt('ldap')) {
			Yii::log("Pas de serveur LDAP défini, authentification impossible.", 'error');
			return false;
		}
		$ldapconn = @ldap_connect(Yii::app()->params->itemAt('ldap')['server']);
		if (!$ldapconn) {
			Yii::log("Could not connect to LDAP server.", 'error');
			return false;
		}
		$queries = Yii::app()->params->itemAt('ldap')['queries'];
		foreach ($queries as $query) {
			$query = sprintf($query, $this->login);
			if (@ldap_bind($ldapconn, $query, $password)) {
				$query = preg_replace('/uid=\w+\s*,/', '', $query);
				$login = ldap_escape($this->login);
				$info = ldap_get_entries($ldapconn, ldap_list($ldapconn, $query, "uid={$login}"));
				@ldap_unbind($ldapconn);
				$this->updateFromLdap($info);
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns true if this user identifies with a password from the SQL.
	 *
	 * @param string $password
	 * @return bool
	 */
	private function validateLocalPassword(string $password): bool
	{
		if ($this->motdepasse === '') {
			return false;
		}
		return password_verify($password, $this->motdepasse);
	}
}
