<?php

namespace widgets;

class DynamicAttributeFilter extends IncludeExcludeFilter
{
	protected function fetchSources(): array
	{
		$visible = (\Yii::app()->user->checkAccess('admin') ? "" : "WHERE visibilite = 1");
		return array_map(
			function (array $x): array {
				return [
					'id' => (int) $x[0],
					'nom' => $x[1],
				];
			},
			\Yii::app()->db
				->createCommand("SELECT id, CONCAT(nom, IF(visibilite = 0, ' [admin]', '')) FROM Sourceattribut $visible ORDER BY nom")
				->queryAll(false)
		);
	}

	protected function getDisplayData(): array
	{
		return [
			'label' => "Attributs",
			'name' => 'SearchTitre[attribut]',
			'hint' => \Hint::model()->getMessage('SearchTitre', 'attribut'),
		];
	}
}
