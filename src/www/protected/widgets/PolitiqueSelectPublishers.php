<?php

namespace widgets;

use models\forms\UserPolitique;

class PolitiqueSelectPublishers extends Mithril
{
	private UserPolitique $user;

	public function __construct(UserPolitique $user)
	{
		parent::__construct();
		$this->user = $user;
	}

	public function run(string $htmlid = ''): string
	{
		if (defined('YII_ENV') && YII_ENV === 'test') {
			return '<input name="UserPolitique[editeurIds][0]" /><input name="UserPolitique[editeurIds][1]" />';
		}
		return parent::run($htmlid);
	}

	protected function getData(): array
	{
		return [
			'editeurIds' => $this->user->editeurIds,
			'editeurNames' => [$this->user->getEditeur(0), $this->user->getEditeur(1), $this->user->getEditeur(2)],
			'roles' => [$this->user->roles[0] ?? '', $this->user->roles[1] ?? '', $this->user->roles[2] ?? ''],
		];
	}

	protected function getJavascriptAssets(): array
	{
		return ['dist/politique-select-publishers.js'];
	}
}
