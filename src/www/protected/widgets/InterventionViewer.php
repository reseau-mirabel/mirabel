<?php

namespace widgets;

/**
 * For authenticated usage (not RSS feeds).
 */
class InterventionViewer
{
	public static function expandKeys(array $assoc, \CActiveRecord $model): array
	{
		$result = [];
		foreach ($assoc as $k => $v) {
			if ($k === 'liensJson' && !empty($v) && !($v instanceof \TitreLiens)) {
				$v = new \TitreLiens($v);
			}
			if (is_array($v) && isset($v['format']) && $v['format'] === 'raw') {
				$final = $v['value'] ?? '';
			} elseif (is_scalar($v)) {
				$final = self::expandScalarKey($model, $k, $v);
			} else {
				$final = $v;
			}
			$result[] = [$model->getAttributeLabel($k), $k => $final];
		}
		return $result;
	}

	public static function prettifyModelName(string $name): string
	{
		if ($name === 'Service') {
			return "Accès en ligne";
		}
		return $name;
	}

	private static function expandScalarKey(\CActiveRecord $model, string $attr, $v): string
	{
		if ($v > 0 && ($attr === "obsoletePar" || $attr === "titreId")) {
			$record = \Titre::model()->findByPk((int) $v);
			if ($record) {
				return "$v ["
					. \CHtml::link($record->getFullTitle(), ['/titre/view', 'id' => $record->id])
					. "]";
			}
			return "$v [titre supprimé]";
		}
		if ($v > 0 && $attr === "paysId") {
			$pays = \Pays::model()->findByPk($v);
			return $pays ? $pays->nom : "Pays $v (supprimé depuis)";
		}
		if ($attr === 'role' && $model instanceof \Editeur) {
			return $v ? (\Editeur::getPossibleRoles()[$v] ?? $v) : "non défini";
		}

		$record = null;
		$m = [];
		if ($v > 0 && $attr === "modifPar") {
			$record = \Utilisateur::model()->findByPk((int) $v);
		} elseif ($v > 0 && preg_match('/^(\w+)Id$/', $attr, $m)) {
			$class = '\\' . ucfirst($m[1]);
			$record = $class::model()->findByPk((int) $v);
		} else {
			$className = '\\' . get_class($model);
			$enumName = "enum" . ucfirst($attr);
			if (property_exists($className, $enumName)) {
				$enum = $className::${$enumName};
				if (is_array($enum) && isset($enum[(string) $v])) {
					$v = $enum[(string) $v];
				}
			}
		}

		if (substr($attr, -2) === 'Id' && ($v === 0 || $v === "0")) {
			// FK auto-filled from a just created PK
			$v = '*ID créé*';
		}
		if ($record) {
			if (method_exists($record, 'getSelfLink')) {
				return "$v [" . $record->getSelfLink() . "]";
			}
			if (method_exists($record, 'getFullName')) {
				return "$v [" . $record->getFullName() . "]";
			}
		}
		return \CHtml::encode((string) $v);
	}
}
