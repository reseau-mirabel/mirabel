<?php

namespace widgets;

use CClientScript;
use Yii;

/**
 * Default JS and CSS for Mirabel.
 */
class DefaultAssets
{
	private array $css = [
		"main.css",
		["screen.css", 'screen'],
		["screen-large.css", 'screen and (min-width: 1200px)'],
		["screen-small.css", 'screen and (max-width: 990px)'],
	];

	private array $js = [
		["main.js", CClientScript::POS_HEAD, ['defer' => true]],
		["sorttable.js", CClientScript::POS_HEAD, ['defer' => true]],
	];

	public function __construct()
	{
		// local untracked CSS
		if (file_exists(__DIR__ . "/assets/default/css/local.css")) {
			$this->css[] = "local.css";
		}
		// print CSS
		$isMediaPrint = isset($_GET['media']) && $_GET['media'] === 'print';
		$this->css[] = ['print.css', ($isMediaPrint ? '' : 'print')];

		// CSS for unofficial warning
		$publicPath = dirname(Yii::app()->getBasePath());
		if (file_exists("$publicPath/images/non-officielle.png")) {
			\Yii::app()->clientScript->registerCss(
				'non-officielle',
				<<<EOCSS
				@media screen {
					body {
						background-image: url(/images/non-officielle.png);
						background-repeat: no-repeat;
						background-position: left 200px;
						background-color:rgba(255,0,0,0.04);
					}
				}
				EOCSS
			);
		}

		$this->loadAssets();
	}

	public function run(): string
	{
		return '';
	}

	private function loadAssets(): void
	{
		$am = \Yii::app()->assetManager;
		$cs = \Yii::app()->clientScript;

		foreach ($this->css as $css) {
			if (is_array($css)) {
				$cssFile = $css[0];
				$cssConditions = $css[1];
			} else {
				$cssFile = $css;
				$cssConditions = '';
			}
			$url = $am->publish(__DIR__ . "/assets/default/css/$cssFile");
			$cs->registerCssFile($url, $cssConditions);
		}

		foreach ($this->js as [$jsFile, $position, $options]) {
			$url = $am->publish(__DIR__ . "/assets/default/js/$jsFile");
			$cs->registerScriptFile($url, $position, $options);
		}
	}
}
