const RECT_WIDTH = 1000;

jQuery(function($) {
	if (document.querySelector('#time-access-strip-container') === null) {
		$("body").append('<div id="time-access-strip-container"></div>');
	}
});
function resetHighlight() {
	const previousHighlight = document.querySelector('.time-access-strip g.group-service-bars > rect.highlight-year');
	if (previousHighlight !== null) {
		$(previousHighlight).popover('destroy');
		previousHighlight.remove();
	}
}

const states = new Map();

class JournalStrip {
	#state
	#svg
	constructor(svg) {
		this.#svg = svg;
		this.#state = this.#parse(svg);
	}
	getState() {
		return this.#state;
	}
	findYearByMousePosition(e) {
		if (!this.#state) {
			return 0;
		}
		const clickPosition = getMouseRelativePosition(e, this.#svg.querySelector('.frise-body'));
		if (clickPosition < 0 || clickPosition > 1) {
			return 0;
		}
		return Math.floor(this.#state.revueStart + clickPosition * this.#state.revueSpan);

		function getArrowbodyWidth(container) {
			const arrowBody = container.querySelector(".arrow-body");
			if (arrowBody === null) {
				return 0;
			}
			const arrowBodySize = arrowBody.getBoundingClientRect();
			return arrowBodySize.right - arrowBodySize.left;
		}

		function getMouseRelativePosition(e, container) {
			if (container === null) {
				return -1.0;
			}
			const containerSize = container.getBoundingClientRect();
			const width = getArrowbodyWidth(container);
			if (width <= 0) {
				return -1.0;
			}
			return (e.clientX - containerSize.left) / width;
		}
	}
	listYearServices(year) {
		const yearStr = `${year}`;
		const r = [];
		for (const service of this.#state.services) {
			if (service.start.substring(0, 4) <= yearStr && (service.end === '' || service.end.substring(0, 4) >= yearStr)) {
				r.push(service);
			}
		}
		return r;
	}
	#parse(svg) {
		const id = svg.getAttribute('id');
		if (!id || !id.startsWith("frise-")) {
			return null;
		}
		if (!states.has(id)) {
			const services = JSON.parse(svg.getAttribute('data-services') || 'null');
			if (services === null) {
				return '';
			}
			const revueStart = parseInt(svg.getAttribute('data-revue-start'));
			const revueEnd = parseInt(svg.getAttribute('data-revue-end'));
			const revueSpan = 1 + revueEnd - revueStart;
			states.set(id, {id, services, revueStart, revueEnd, revueSpan});
		}
		return states.get(id);
	}
};

window.addEventListener('mouseup', (event) => {
	if (event.target.closest('a') !== null) {
		return;
	}
	const inHref = event.target.closest('[data-href]');
	if (inHref !== null) {
		window.location = inHref.getAttribute('data-href');
		return;
	}
	resetHighlight();
	const svg = event.target.closest('svg.frise');
	if (svg === null) {
		return;
	}

	const journalStrip = new JournalStrip(svg);
	const yearSelected = journalStrip.findYearByMousePosition(event);
	if (yearSelected === 0) {
		return;
	}

	const grp = event.target.closest('g.group-service-bars');
	if (grp === null) {
		return;
	}
	drawYearRect(grp, journalStrip.getState(), yearSelected);

	$('.highlight-year', svg).popover({
		animation: false,
		html: true,
		container: '#time-access-strip-container',
		placement: 'bottom',
		title: `Accès au <strong>texte intégral</strong> en <strong>${yearSelected}</strong>`,
		content: describeYear(yearSelected, journalStrip.listYearServices(yearSelected)),
	}).popover('show');
	$('.popover').on('click', function(e) {
		if (e.target.name !== 'A') {
			$('.highlight-year', svg).popover('hide');
			this.resetHighlight();
		}
	});

	function drawYearRect(grp, state, yearSelected) {
		const currentYear = (new Date()).getFullYear();
		const lineSpan = 1 + currentYear - state.revueStart;
		grp.append(buildRect(yearSelected, state.revueStart, lineSpan));

		function buildRect(year, revueStart, lineSpan) {
			const rectX = Math.floor(RECT_WIDTH * (year - revueStart) / lineSpan);
			const rectWidth = Math.floor(RECT_WIDTH / lineSpan);
			const rect = document.createElementNS("http://www.w3.org/2000/svg", 'rect');
			rect.setAttribute('class', "highlight-year");
			rect.setAttribute('x', `${rectX}`);
			rect.setAttribute('width', `${rectWidth}`);
			rect.setAttribute('y', "14");
			rect.setAttribute('height', "15");
			rect.setAttribute('stroke-width', "1");
			rect.setAttribute('stroke', "black");
			rect.setAttribute('fill', "none");
			return rect;
		}
	}

	function describeYear(year, services) {
		if (services.length === 0) {
			return "Aucun accès en ligne connu.";
		}
		const contentList = [];
		const yearStr = `${year}`;
		for (const service of services) {
			let typeClass = service.type.toLowerCase();
			if (typeClass === 'abonné') {
				typeClass = 'abonne';
			}
			const name = htmlEscape(service.name);
			const comment = (service.lacunaire ? ' <i>lacunaire</i>' : "");
			const url = htmlEscape(service.url);
			contentList.push(
				`<tr><th>${name}</th>
				<td class="acces-couleur acces-${typeClass}" style="text-align:center" data-href="${url}"><i class="icon-globe icon-white"></i></td>
				<td style="text-align:center"><a href="${url}">${service.type}${comment}</a></td>
				</tr>`
			);
		}
		return `<table class="table table-condensed">` + contentList.reverse().join("") + "</table>";
	}
});

let lastYearSelected = new Map();

window.addEventListener('pointermove', (event) => {
	const svg = event.target.closest('svg.frise');
	if (svg === null) {
		for (const b of document.querySelectorAll('svg .pointed-year')) {
			$(b).popover('destroy');
			b.remove();
		}
		lastYearSelected = new Map();
		return;
	}

	const journalStrip = new JournalStrip(svg);
	const yearSelected = journalStrip.findYearByMousePosition(event);
	if (!yearSelected) {
		return;
	}
	const id = journalStrip.getState().id;
	if (lastYearSelected.get(id) === yearSelected) {
		return;
	}
	lastYearSelected.set(id, yearSelected);
	for (const b of svg.querySelectorAll('.pointed-year')) {
		$(b).popover('destroy');
		b.remove();
	}


	const body = svg.querySelector('svg.frise-body');
	if (body === null) {
		return;
	}
	const yearBlock = drawYearText(body, journalStrip.getState(), yearSelected);

	if (journalStrip.listYearServices(yearSelected).length === 0) {
		//resetHighlight();
		$(yearBlock).popover({
			animation: false,
			html: true,
			container: '#time-access-strip-container',
			placement: 'top',
			content: "aucun accès complet au texte intégral",
		}).popover('show');
	}

	function drawYearText(body, state, yearSelected) {
		const currentYear = (new Date()).getFullYear();
		const lineSpan = 1 + currentYear - state.revueStart;
		const txt = buildText(yearSelected, state.revueStart, lineSpan);
		body.append(txt);
		return txt;

		function buildText(year, revueStart, lineSpan) {
			const x = Math.floor(RECT_WIDTH * (year - revueStart + .5) / lineSpan); // center of year
			const width = 100;
			const txt = document.createElementNS("http://www.w3.org/2000/svg", 'text');
			txt.innerHTML = `${yearSelected}`;
			txt.setAttribute('class', "pointed-year");
			txt.setAttribute('x', `${x}`);
			txt.setAttribute('width', `${width}`);
			txt.setAttribute('y', "8");
			txt.setAttribute('height', "14");
			txt.setAttribute('stroke-width', "0.2");
			txt.setAttribute('stroke', "black");
			txt.setAttribute('fill', "#606060");
			return txt;
		}
	}
});
