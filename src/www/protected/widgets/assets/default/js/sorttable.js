(function() {
	function getCellValue(tr, idx) {
		return tr.children[idx].innerText;
	}

	function comparer(idx, asc) {
		function compareByType(v1, v2) {
			// sort based on a numeric or localeCompare, based on type...
			return (v1 !== '' && v2 !== '' && !isNaN(v1) && !isNaN(v2))
				? v1 - v2
				: v1.toString().localeCompare(v2);
		}
		return (function(a, b) {
			return compareByType(getCellValue(asc ? a : b, idx), getCellValue(asc ? b : a, idx));
		});
	}

	document.querySelectorAll('table.sortable > thead')
		.forEach(function(thead) {
			thead.addEventListener('click', function(event) {
				const th = event.target
				if (th.tagName !== 'TH') {
					return;
				}
				const table = th.closest('table');
				if (table.tBodies.length === 0) {
					console.error("TABLE.sortable is missing a TBODY")
					return;
				}
				const tbody = table.tBodies[0]

				let asc = !th.classList.contains('ascend'); // inverted
				for (let x of table.querySelectorAll(':scope > thead th.sorted')) {
					x.classList.remove("sorted", "ascend", "descend")
					console.log(x)
				}
				th.classList.add('sorted', asc ? 'ascend' : 'descend');

				Array.from(table.querySelectorAll(':scope > tbody > tr'))
					.sort(comparer(Array.from(th.parentNode.children).indexOf(th), asc))
					.forEach(tr => tbody.appendChild(tr));
			})
		});
})();
