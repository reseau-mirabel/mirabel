function initLinksForm(parentSelector, modelName) {
	const linksSources = JSON.parse(document.querySelector('#linksDefaultSources').text);

	const parent = $(parentSelector || "body");
	$('.saisie-liens input.source', parent).autocomplete({
		minlength: 0,
		source: linksSources,
	}).each(function() {
		const editeurs = this.closest('#Editeur-saisie-liens') !== null;
		var instance = $(this).autocomplete("instance");
		instance.menu.options.items = '> li:not(.ui-menu-item-disabled)';
		instance._renderItem = function(ul, item) {
			if (!editeurs && item.hasOwnProperty('forbid') && item.forbid !== '') {
				return $('<li class="ui-menu-item-disabled">')
					.append("<div>" + item.label + '</div><div class="item-forbid">Cette source est importée automatiquement.</div>')
					.appendTo(ul);
			}
			return $('<li>')
				.append("<div>" + item.label + "</div>")
				.appendTo(ul);
		};
	});

	function validateOtherLink(e) {
		const sourceName = $(this).closest('.controls').find('.source').val();
		validateOtherLinkInput(this, sourceName);
	}
	function validateOtherLinkInput(input, sourceName) {
		const url = $(input).val();
		if (url === '' || url[0] === '/') {
			return;
		}
		let errorMsg = '';
		const hostExtractor = new RegExp('^https?://([^/]+\\.[^/]+)(/|$)');
		const match = url.match(hostExtractor);
		if (!match) {
			errorMsg = "Cela ne semble pas être une URL correcte en http(s)://";
		} else {
			const host = match[1];
			for (const s of linksSources) {
				if (s.value !== sourceName) {
					continue;
				}
				if (s.domain !== '') {
					if (!host.endsWith(s.domain)) {
						errorMsg = "Cette URL n'est pas du domaine de la source, elle devrait contenir " + s.domain;
						break;
					}
				}
				if (s.urlRegex !== '') {
					const regex = new RegExp(s.urlRegex);
					if (!url.match(regex)) {
						errorMsg = "Cette URL n’est pas au format de la source de lien choisie. Vérifiez que l’URL est correcte et ne se termine pas par des éléments superflus : ? # / ?lang=fr ...";
					}
				}
			}
		}
		const row = $(input).closest('.controls');
		let icon = row.find('span.help-inline');
		if (errorMsg) {
			row.addClass('error');
			if (icon.length === 0) {
				icon = $(input).after('<span class="help-inline" title="' + errorMsg + '"><i class="icon icon-warning-sign"></i></span>');
			} else {
				icon.attr('title', errorMsg);
			}
		} else {
			row.removeClass('error');
			if (icon.length > 0) {
				icon.remove();
			}
		}
	}
	$(".saisie-liens input.url", parent).each(validateOtherLink);
	$(".saisie-liens", parent).on('input', '.url', validateOtherLink);
	$(".saisie-liens", parent).on('autocompleteselect', '.source', function(event, ui) {
		validateOtherLinkInput(this.parentNode.querySelector('input.url'), ui.item.value);
	});


	$(`#${modelName}-saisie-liens .add-input`, parent).on('click', function() {
		var newLinkHtml = `<div class="controls">
			<label class="subelement">Source : </label>
			<input name="${modelName}[liens][999][src]" type="text" class="source" />
			<span style="white-space: nowrap">
				<label class="subelement">Adresse : </label>
				<input name="${modelName}[liens][999][url]" type="text" size="80" value="" class="url input-xlarge" autocomplete="off" />
			</span>
		</div>`;
		var newElement = $(newLinkHtml);

		var numElements = $(`#${modelName}-saisie-liens input.source[name*="[liens]"]`, parent).length;

		var sourceName = newElement.find("input.source").attr("name")
			.replace(/\[liens\]\[\d+\]/, "[liens][" + numElements + "]");
		newElement.find("input.source")
			.attr("name", sourceName)
			.val("");

		var urlName = newElement.find("input.url").attr("name")
			.replace(/\[liens\]\[\d+\]/, "[liens][" + numElements + "]");
		newElement.find("input.url")
			.attr("name", urlName)
			.val("");

		var block = $(this).closest(".controls");
		newElement
			.insertBefore(block)
			.find("input.source").autocomplete({
				minlength: 0,
				source: linksSources
			});
	});
}