let policyFields = (function() {
	let policy;
	let hints;
	let translations;

	function addEmptyOa() {
		policy.oa.push({
			additional_oa_fee: "no",
			article_version: [],
			conditions: [],
			copyright_owner: "",
			"location": {location:[], named_repository: []}
		});
	}

	function addEmptyUrl() {
		policy.urls.push({url: "", description: ""});
	}

	const Hint = {
		oncreate: function (vnode) {
			$(vnode.dom).popover();
		},
		view: function (vnode) {
			if (!vnode.attrs.name) {
				return null;
			}
			const content = hints.get(name);
			if (typeof content === 'undefined' || content === '') {
				return null;
			}
			return m(
				"a",
				{
					href: "#",
					'data-title': "Aide",
					'data-content': content,
					'data-html': "true",
					'data-trigger': "hover",
					'data-container': 'body',
					rel: "popover"
				},
				"?"
			);
		}
	};

	const CheckboxList = {
		view: function(vnode) {
			const label = vnode.attrs.label;
			const name = vnode.attrs.name;
			return m("div.control-group",
				m("label.control-label", {"for" : name}, label),
				m("div.controls",
					Object.entries(vnode.attrs.candidates).map(function (c, index) {
						return m("div",
							m("label.checkbox",
								m("input", {
									type: "checkbox",
									name: name,
									checked: vnode.attrs.checked.includes(c[0]),
									value: c[0],
									onchange: vnode.attrs.onchange || null
								}),
								" ",
								c[1]
							),
							(index === 0 ? m(Hint, {name: name}) : null)
						);
					})
				),
			);
		}
	};

	const ArticleVersion = {
		view: function(vnode) {
			const oa = vnode.attrs.oa;
			return m(CheckboxList, {
				label: "Appliquée à",
				name: "oa_" + vnode.attrs.index + "_article_version",
				checked: oa.article_version || [],
				candidates: {
					submitted: "version soumise",
					accepted: "version acceptée",
					published: "version publiée"
				},
				onchange: function(event) {
					let result = [];
					let checkboxes = event.target.closest("div.controls").querySelectorAll('input[type="checkbox"]');
					for (let c of checkboxes) {
						if (c.checked) {
							result.push(c.value);
						}
					}
					oa.article_version = result;
					return true;
				}
			});
		}
	};

	const Conditions = {
		view: function(vnode) {
			const oa = vnode.attrs.oa;
			return m(CheckboxList, {
				label: "Conditions requises",
				name: "oa_" + vnode.attrs.index + "_conditions",
				checked: oa.conditions || [],
				candidates: translations['sherpa.fr.conditions'],
				onchange: function(event) {
					let result = [];
					let checkboxes = event.target.closest("div.controls").querySelectorAll('input[type="checkbox"]');
					for (let c of checkboxes) {
						if (c.checked) {
							result.push(c.value);
						}
					}
					oa.conditions = result;
					return true;
				}
			});
		}
	};

	const CopyrightOwner = {
		view: function(vnode) {
			const oa = vnode.attrs.oa;
			return m(Dropdown, {
				label: "Détenteur des droits",
				name: "oa_" + vnode.attrs.index + "_copyright_owner",
				value:  oa.copyright_owner || "",
				options: translations['sherpa.fr.copyrightowner'],
				onchange: function(event) {
					oa.copyright_owner = event.target.value;
				}
			});
		}
	};

	const Fee = {
		view: function(vnode) {
			const oa = vnode.attrs.oa;
			return m(Dropdown, {
				label: "Frais supplémentaires",
				name: "fee",
				value: oa.additional_oa_fee || "0",
				options: [["yes", "Oui"], ["no", "Non"]],
				onchange: function(event) {
					oa.additional_oa_fee = event.target.value;
				}
			});
		}
	};

	const License = {
		view: function(vnode) {
			const oa = vnode.attrs.oa;
			let values = [];
			if (oa.license) {
				values = oa.license.map(function(x) { return x.license; });
			}
			return m(CheckboxList, {
				label: "Licence autorisée",
				name: "oa_" + vnode.attrs.index + "_license",
				checked: values,
				format: 'raw',
				candidates: translations['sherpa.fr.license'],
				onchange: function(event) {
					let result = [];
					let checkboxes = event.target.closest("div.controls").querySelectorAll('input[type="checkbox"]');
					for (let c of checkboxes) {
						if (c.checked) {
							result.push({license: c.value});
						}
					}
					oa.license = result;
					return true;
				}
			});
		}
	};

	const Locations = {
		view: function(vnode) {
			const oa = vnode.attrs.oa;
			return m(CheckboxList, {
				label: "Site de diffusion",
				name: "oa_" + vnode.attrs.index + "_locations",
				checked: oa.location.location || [],
				candidates: translations['sherpa.fr.locations'],
				onchange: function(event) {
					let location = [];
					let checkboxes = event.target.closest("div.controls").querySelectorAll('input[type="checkbox"]');
					for (let c of checkboxes) {
						if (c.checked) {
							location.push(c.value);
						}
					}
					oa.location.location = location;
					return true;
				}
			});
		}
	};

	const LocationsNamed = {
		view: function(vnode) {
			const oa = vnode.attrs.oa;
			if (!oa.location.location || !oa.location.location.includes("named_repository")) {
				return null;
			}
			const values = oa.location && ('named_repository' in oa.location) ? oa.location.named_repository : [];
			const standardLocations = [
				"arXiv",
				"ESRC Research Catalogue",
				"Europe PubMed Central",
				"PubMed Central",
				"RePEC"
			];
			const candidates = {};
			for (let l of standardLocations) {
				candidates[l] = l;
			}
			if (!values.includes("")) {
				values.push("");
			}
			for (let v of values) {
				if (!standardLocations.includes(v)) {
					candidates[v] = [
						"Autre : ",
						m("input", {
							type: "text",
							name: "named_repository_free",
							value: v,
							oninput: function(event) {

							}
						})
					];
				}
			}
			values.pop();
			return m(CheckboxList, {
				label: "Archive spécifique",
				name: "oa_" + vnode.attrs.index + "_named_repository",
				checked: values || [],
				candidates: candidates
				// TODO onchange for oa.location.named_repository
			});
		}
	};

	const Dropdown = {
		view: function(vnode) {
			let value = vnode.attrs.value;
			let entries = (vnode.attrs.options instanceof Array) ?
				vnode.attrs.options
				: Object.entries(vnode.attrs.options);
			return m("div.control-group",
				m("label.control-label", {"for" : name}, vnode.attrs.label),
				m("div.controls",
					m("select", {name: name, onchange: vnode.attrs.onchange},
						entries.map(function(o) {
							if (o instanceof Array) {
								return m("option", {value: o[0], selected: o[0] === value}, o[1]);
							} else {
								return m("option", {value: o.value, selected: o.value === value}, o.text);
							}
						})
					),
					m(Hint, {name: name})
				),
			);
		}
	};

	const StringInput = {
		view: function(vnode) {
			const label = vnode.attrs.label;
			const name = vnode.attrs.name;
			const value = vnode.attrs.value;
			return m("div.control-group",
				m("label.control-label", {"for" : name}, label),
				m("div.controls",
					m("input.input-block-level", {type: "text", name: name, value: value, onchange: vnode.attrs.onchange}),
					m(Hint, {name: name})
				),
			);
		}
	};

	const UrlList = {
		view: function(vnode) {
			const label = vnode.attrs.label;
			const name = vnode.attrs.name;
			const value = vnode.attrs.value;
			return m("div.control-group",
				m("label.control-label", {}, label),
				m("div.controls",
					value.map(function(v, index) {
						const inputBaseName = name + "[" + index + "]";
						return m("div", {style: "margin-bottom: 10px"},
							m("div.input-prepend",
								m("span.add-on", "URL"),
								m("input.input-xxlarge", {type: "text", name: inputBaseName + "[url]", value: v.url || ""})
							),
							m("div.input-prepend",
								m("span.add-on", "Description"),
								m("input.input-xlarge", {type: "text", name: inputBaseName + "[description]", value: v.description || ""})
							)
						);
					}),
					m("div",
						m("button.btn",
							{
								type: "button",
								onclick: addEmptyUrl
							},
							m("span.icon.icon-plus"),
							" lien supplémentaire",
						),
						m(Hint, {name: name})
					)
				),
			);
		}
	};

	const OA = {
		view: function(vnode) {
			const params = {
				index: vnode.attrs.index,
				oa: vnode.attrs.oa
			};
			return m("fieldset",
				m("legend", "Règle de diffusion"),
				m(ArticleVersion, params),
				m(Conditions, params),
				m(Fee, params),
				m(CopyrightOwner, params),
				m(License, params),
				m(Locations, params),
				m(LocationsNamed, params)
			);
		}
	};

	const OneMoreRuleButton = {
		view: function() {
			return m("fieldset",
				m("hr"),
				m("button.btn",
					{
						type: "button",
						onclick: addEmptyOa
					},
					m("span.icon.icon-plus"),
					" Règle supplémentaire de diffusion",
				)
			);
		}
	};

	const Identification = {
		view: function() {
			return m("fieldset",
				m("legend", "Identification"),
				m(StringInput, {
					label: "Nom",
					name: "name",
					value: policy.name,
					onchange: function(event) {
						policy.name = event.target.value;
					}
				}),
				m(UrlList, {
					label: "Liens descriptifs",
					name: "urls",
					value: policy.urls
				}),
				m(Dropdown, {
					label: "Diffusion ouverte",
					name: "open",
					value: policy.open,
					options: [["1", "Oui"], ["0", "Non"]],
					onchange: function(event) {
						policy.open = event.target.value;
					}
				}),
			);
		}
	};

	const Main = {
		view: function() {
			return m("div",
				m(Identification),
				(policy.open === "1" ?
					policy.oa.map(function(oa, index) {
						return m(OA, {oa: oa, index: index});
					})
					: null),
				(policy.open === "1" ? m(OneMoreRuleButton) : null)
			);
		}
	};

	return {
		init: function(htmlId, data) {
			// find the root HTML element
			const root = document.getElementById(htmlId);
			if (!root) {
				alert("JS bug, root element #" + htmlId + " was not found.");
				return;
			}

			// load the publisher policy
			policy = data.policy;
			if (!policy.urls) {
				policy.urls = [];
			}
			addEmptyUrl();
			policy.open = policy.open ? "1" : "0";
			if (policy.oa.length === 0) {
				addEmptyOa();
			}

			// load the form hints
			if (data.hints) {
				hints = new Map(Object.entries(data.hints));
			} else {
				hints = new Map();
			}

			// load the translations and item lists
			translations = data.translations;
			for (let x in data.translations['sherpa.fr.license']) {
				data.translations['sherpa.fr.license'][x] = m.trust(data.translations['sherpa.fr.license'][x]);
			}
			translations['sherpa.fr.copyrightowner'][""] = "";

			m.mount(root, Main);
		}
	};
})();
