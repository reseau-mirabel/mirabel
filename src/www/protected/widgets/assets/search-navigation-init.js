document.addEventListener("DOMContentLoaded", storeAll);

function storeAll() {
	for (let source of document.querySelectorAll('script.navigation-storage-init')) {
		const config = JSON.parse(source.textContent);
		populateStorage(config.type, config.results, config.maxPageRank);
	}
}

function populateStorage(type, liste, maxPageRank) {
	sessionStorage.removeItem("search-navigation-" + type);
	const urlLocation = new URL(getUrlLocationHref());
	const tabPathnameUrl = urlLocation.pathname.split('/');
	if (tabPathnameUrl.length < 2) {
		return;
	}
	const typePage = tabPathnameUrl[1];
	const searchNavigation = {
	    type: type,
	    urlUp: urlLocation.href,
	    anchor: getAnchor(typePage, type),
	    liste: liste,
	    previous: getUrlPrevious(urlLocation.href),
	    next: getUrlNext(urlLocation.href, maxPageRank),
	};
	sessionStorage.setItem("search-navigation-" + type, JSON.stringify(searchNavigation));
}

function getUrlPrevious(href) {
	const url = new URL(href);
	const numPage = parseInt(url.searchParams.get("page"));
	if (Number.isNaN(numPage) || numPage < 2) {
		return null;
	}
	url.searchParams.set('page', numPage - 1);
	return url.href;
}

function getUrlNext(href, maxPageRank) {
	const url = new URL(href);
	let numPage = parseInt(url.searchParams.get("page"));
	if (Number.isNaN(numPage)) {
		numPage = 1;
	}
	const numberNextPage = numPage + 1;
	if (numberNextPage > maxPageRank) {
		return null;
	}
	url.searchParams.set('page', numberNextPage);
	return url.href;
}

function getAnchor(typePage, type) {
	if (typePage === 'site') {
		if (type !== 'revue') {
			return `#search-${type}s`;
		}
	}
	return '';
}

function getUrlLocationHref() {
	const urlHash = ['#search-editeurs', '#search-ressources'];
	let urlLocationHref = window.location.href;
	if (urlHash.includes(window.location.hash)) {
		urlLocationHref = urlLocationHref.replace(window.location.hash, '');
	}
	return urlLocationHref;
}
