// global state
const journals = readJournalsJson();
let journal = null;

forceCheckDates();
addChecksOnDates();
$('#Service_titreId').on('change', forceCheckDates);

initVolno();
initCollection();


function readJournalsJson() {
	const scr = document.getElementById('journal-data');
	if (!scr) {
		return null;
	}
	return JSON.parse(scr.textContent);
}

function findSelectedJournal() {
	if (journals.length === 1) {
		return journals[0];
	}
	if (journals.length > 1) {
		const input = document.getElementById('Service_titreId');
		if (input === null) {
			return null;
		}
		const id = parseInt(input.value || "0");
		if (id === 0) {
			return null;
		}
		for (let j of journals) {
			if (j.id === id) {
				return j;
			}
		}
	}
	return null;
}

function forceCheckDates() {
	journal = findSelectedJournal();
	if (journal) {
		checkStartDate.call(document.querySelector('#Service_dateBarrDebut'));
		checkEndDate.call(document.querySelector('#Service_dateBarrFin'));
	}
}

function checkStartDate() {
	if (journal === null || journal.dateDebut === '') {
		return;
	}
	const input = this.value;
	if (input.match(/^\d{4}(-\d\d){0,2}$/)) {
		const date = (input + "-01-01").substring(0, 10);
		if (date < journal.dateDebut) {
			warnOnDate(this, `Cette date déborde du titre (${journal.dateDebut} – ${journal.dateFin})`)
		} else {
			warnOnDate(this, "")
		}
	} else {
		warnOnDate(this, "")
	}
}

function checkEndDate() {
	if (journal === null || journal.dateFin === '') {
		return;
	}
	const input = this.value;
	if (input.match(/^\d{4}(-\d\d){0,2}$/)) {
		const dateFin = (journal.dateFin + "-12-31").substring(0, 10);
		if (input > dateFin) {
			warnOnDate(this, `Cette date déborde du titre (${journal.dateDebut} – ${journal.dateFin})`)
		} else {
			warnOnDate(this, "")
		}
	} else if (input === '' && journal.dateFin !== '') {
		warnOnDate(this, `Une date de fin est attendue car le titre est terminé en ${journal.dateFin}.`)
	} else {
		warnOnDate(this, "")
	}
}

function warnOnDate(inputField, message) {
	const existingWarning = inputField.closest('.controls').querySelector('span.error');
	if (message === '') {
		// no warning
		if (existingWarning) {
			inputField.closest('.control-group').classList.remove('warning');
			existingWarning.remove();
		}
		return;
	}
	inputField.closest('.control-group').classList.add('warning');
	if (!existingWarning) {
		const warn = document.createElement('span')
		warn.setAttribute('class', 'error')
		warn.textContent = message;
		inputField.closest('.controls').appendChild(warn);
	}
}

function addChecksOnDates() {
	$('#Service_dateBarrDebut').on("input", checkStartDate);
	$('#Service_dateBarrFin').on("input", checkEndDate);
}

function initVolno() {
	$('input.volno-num').each(function() {
		$(this).data('val', $(this).val());
	});

	$('div.volno').on('keyup', "input.volno-num", function() {
		if ($(this).val().match(/^\d*$/)) {
			$(this).parent().removeClass('error').find('.help-inline.error').remove();
		} else {
			if (!$(this).parent().hasClass('error')) {
				$(this).parent().addClass('error').append('<span class="help-inline error"> Ces champs sont numériques !</span>');
			}
			return true;
		}
		if ($(this).val() == $(this).data('val')) {
			return true;
		}
		$(this).data('val',  $(this).val());
		var dir = $(this).closest('div.volno');
		var vol = $('.vol', dir).val();
		vol = vol ? 'Vol. ' + vol : '';
		var no = $('.no', dir).val();
		no = no ? 'no ' + no : '';
		if ($('.joined', dir).val().match(/^(Vol\. \d+)?(, )?(no \d+)?$/)) {
			$('.joined', dir).val(vol !== '' && no !== '' ? vol + ", " + no : vol + no);
		} else {
			var group = $('.joined', dir).closest('.control-group').addClass('warning');
			if (group.find(".alert").size() === 0) {
				group.append("<div class=\"alert alert-warning\">L'affichage public a été personnalisé. Vérifier qu'il correspond bien aux champs numériques.</div>");
			}
		}
	}).on('keyup', "input.joined", function() {
		$(this).closest('.control-group').removeClass('warning').find('.alert').remove();
	});
}

// Collection list depends on the choice of a resource
function initCollection() {
	const collectionsContainer = $("#service-collections-dynamic");
	if (collectionsContainer.size() > 0) {
		const collections = collectionsContainer.data('collections');
		if (collections) {
			addCollectionCheckboxes(collections);
		} else {
			$("#Service_ressourceIdComplete").on("autocompleteselect", function(event, ui) {
				$.ajax({
					url: '/collection/ajax-list',
					data: { ressourceId: ui.item.id },
					method: "get",
					success: function (data) {
						console.log("AJAX collection: ", data);
						addCollectionCheckboxes(data);
					}
				});
			});
		}
	}

	function addCollectionCheckboxes(collections) {
		$("#service-collections-dynamic").empty();
		if (collections.length === 0) {
			return;
		}
		$("#service-collections-dynamic").html(
			'<div class="control-group "><label class="control-label" for="Service_collectionIds">'
			+ 'Collections<span class="required">*</span></label>'
			+ '<div class="controls"></div></div>'
		);
		const container = $("#service-collections-dynamic .controls").first();
		const excludeImport = collectionsContainer.attr('data-excludeimport');
		$.each(collections, function(index, item) {
			const checkbox = $('<label for="Service_collectionIds_' + index + '"' + (item.disabled || item.imported ? ' title="collection gérée par import automatique"' : '') + '></label>')
				.text(item.label + " ")
				.prepend('<input id="Service_collectionIds_' + index
					+ '" value="' + item.id
					+ '" name="Service[collectionIds][]" type="checkbox" '
					+ ('checked' in item && item.checked ? 'checked="checked" ' : '')
					+ (item.disabled || (excludeImport && item.imported) ? 'disabled="disabled" ' : '')
					+ '/>'
				);
			const div = $('<div></div>').append(checkbox);
			if ('hint' in item && item.hint) {
				const hint = $('<i class="icon-info-sign"></i>');
				hint.attr('title', item.hint);
				div.append(" ").append(hint);
			}
			container.append(div);
		});
		console.log("Collections display...done!");
	}
}
