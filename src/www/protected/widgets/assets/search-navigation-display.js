document.addEventListener("DOMContentLoaded", navigationDisplay);

function navigationDisplay() {
	const [ searchNavigation, currentIndex ] = loadFromStorage();
	if (searchNavigation === null) {
		return;
	}
	const liste = searchNavigation.liste;

	let previousLink;
	if (currentIndex === 0) {
		if (searchNavigation.previous === null) {
			previousLink = null;
		} else {
			const support = '<i class="icon-chevron-left" id="search-nav-prev"></i>';
			previousLink = makeLinkHtml(searchNavigation.previous + searchNavigation.anchor, support, 'Page précédente des résultats');
		}
	}
	if (previousLink === undefined) {
		const previous = liste.at(currentIndex-1);
		const previousUrl = '/' + searchNavigation.type + '/' + previous.id;
		previousLink = makeLinkHtml(previousUrl, '<i class="icon-chevron-left" id="search-nav-prev"></i>', 'Résultat précédent : ' + previous.name);
	}
	let nextLink;
	if (currentIndex === (liste.length - 1)) {
		if (searchNavigation.next === null) {
			nextLink = null;
		} else {
			const support = ' <i class="icon-chevron-right" id="search-nav-next"></i>';
			nextLink = makeLinkHtml(searchNavigation.next + searchNavigation.anchor, support, 'Page suivante des résultats');
		}
	}
	if (nextLink === undefined) {
		const next = liste.at(currentIndex+1);
		const nextUrl = '/' + searchNavigation.type + '/' + next.id;
		nextLink = makeLinkHtml(nextUrl, ' <i class="icon-chevron-right" id="search-nav-next"></i>', 'Résultat suivant : ' + next.name);
	}

	if (previousLink !== null) {
		document.querySelector("#search-navigation").append(previousLink);
	}
	const linkUrlUp = makeLinkHtml(searchNavigation.urlUp + searchNavigation.anchor, ' <i class="icon-list" id="search-nav-up"></i> ', 'Retour aux résultats de la recherche');
	document.querySelector("#search-navigation").append(linkUrlUp);
	if (nextLink !== null) {
		document.querySelector("#search-navigation").append(nextLink);
	}
}

function makeLinkHtml(url, support, info) {
	const a = document.createElement('a');
	a.setAttribute('title', info);
	a.setAttribute('href', url);
	a.innerHTML = support;
	return a;
}

function loadFromStorage() {
	const tabUrl = window.location.pathname.split('/');
	if (tabUrl.length < 3) {
		return [null, null];
	}
	const typePage = tabUrl[1];
	const currentId = parseInt(tabUrl[2]);
	if (Number.isNaN(currentId)) {
		console.error("Search-navigation: currentId is Nan");
		return [null, null];
	}

	const searchNavigationName = "search-navigation-" + typePage;
	if (!sessionStorage.getItem(searchNavigationName)) {
		return [null, null];
	}
	const searchNavigationString = sessionStorage.getItem(searchNavigationName);
	const searchNavigation = JSON.parse(searchNavigationString);
	if (typePage !== searchNavigation.type) {
		console.log('Search-navigation: typePage', typePage, '!=', searchNavigation.type);
		return [null, null];
	}
	const currentIndex = searchNavigation.liste.findIndex(element => element.id === currentId);
	if (currentIndex === -1) {
		return [null, null];
	}
	return [searchNavigation, currentIndex];
}
