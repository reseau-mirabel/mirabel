// Update the form according to the input with class "idref".
window.queryIdref = (function() {
	function queryIdref(idref) {
		if (!idref.match(/^[\dX]+$/)) {
			return Promise.reject('');
		}
		const query = `PREFIX dcterms: <http://purl.org/dc/terms/>
	PREFIX foaf: <http://xmlns.com/foaf/0.1/>
	PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
	SELECT ?nom
	WHERE { ?x dcterms:identifier "${idref}" . ?x foaf:primaryTopic ?y . ?y skos:prefLabel ?nom }`;
		return fetch(
				'https://data.idref.fr/sparql?query=' + encodeURIComponent(query),
				{
					method: 'GET',
					mode: 'cors',
					headers: {Accept: "application/sparql-results+json"}
				}
			).then((response) => response.json())
			.then((data) => data.results.bindings)
			.then(function(data) {
				if (data.length !== 1) {
					return '';
				}
				return data[0].nom.value;
			});
	}

	function appendNameAfter(idref, targetElement) {
		let liveElement = targetElement.parentNode.querySelector(":scope > .live-idref-name");
		if (liveElement === null) {
			liveElement = document.createElement('span');
			liveElement.setAttribute('class', 'live-idref-name');
			targetElement.insertAdjacentElement('afterend', liveElement);
		}
		if (idref === '') {
			liveElement.textContent = '';
			return;
		}
		queryIdref(idref).then(function(name) {
			const a = document.createElement('a');
			a.href = `https://www.idref.fr/${idref}`;
			a.target = "_blank";
			a.textContent = name;
			liveElement.innerHTML = "→ ";
			liveElement.append(a);
		});
	}

	function appendNameOnChange(x) {
		let lastValue = x.getAttribute('value') || '';
		return function() {
			const idref = x.value.replace(/^https?:\/\/www\.idref\.fr\//, '');
			if (idref === lastValue) {
				return;
			}
			lastValue = idref;
			if (!idref || idref.length !== 9) {
				appendNameAfter('', x);
				return;
			}
			appendNameAfter(idref, x);
		}
	}

	function applyToInputFields(root) {
		for (let x of root.querySelectorAll("a[data-idref]")) {
			const idref = x.getAttribute('data-idref');
			appendNameAfter(idref, x);
		}
		for (let x of root.querySelectorAll("input.idref")) {
			const idref = x.getAttribute('value') || '';
			appendNameAfter(idref, x);
			x.addEventListener('input', appendNameOnChange(x));
		}
	}

	addEventListener('DOMContentLoaded', () => applyToInputFields(document));

	return {
		applyToInputFields
	};
})();
