/* global console, document, fetch, m */

// Search idref.fr and select a response
const IdrefQuery = (function() {
	const start = {
		root: null,
		state: {
			query: "",
			lock: false,
			recordtype: "b",
			appr: {
				error: "",
				results: [],
				total: null,
			},
			exact: {
				error: "",
				results: [],
				total: null,
			},
		},
	};

	let state = {};
	let isEmbeddedInModal = false;

	function toggleRecordtype() {
		if (state.recordtype === 'b') {
			state.recordtype = 'a,b';
		} else {
			state.recordtype = 'b';
		}
		Search.searchFromState();
	}

	function loadEmbeddedForm(dialog, url, body) {
		for (let b of dialog.querySelectorAll('.modal-footer button')) {
			b.removeAttribute('style');
		}
		fetch(url, {credentials: 'same-origin', headers: {"X-Embedded-In-Html": "true"}, method: 'post', body: body})
			.then(response => response.text())
			.then(function(html) {
				state = Object.assign({}, start.state);
				m.redraw();
				start.root.setAttribute('style', 'display: none');

				const form = dialog.querySelector('#embedded-editeur-form')
				form.innerHTML = html;
				window.queryIdref.applyToInputFields(form);
				$('#embedded-editeur-form [rel="popover"]').popover({placement: "left"});
				window.initLinksForm('#embedded-editeur-form', 'Editeur');
			});
	}

	let DLD_ERROR = `L'interrogation de idref.fr a échoué. Vous pouvez <strong>réessayer dans quelques temps</strong>,
		ou <a class="btn btn-primary" href="/editeur/create-final">créer un éditeur sans ce pré-remplissage</a>`;

	const idrefBaseUrl = 'https://www.idref.fr/Sru/Solr?version=2.2&wt=json&indent=off&start=0&fl=id,ppn_z,affcourt_r,affcourt_z&sort=affcourt_z%20asc';
	const Search = {
		responses: {
			appr: {},
			exact: {}
		},
		query(suffix, pattern, maxResults) {
			const q = encodeURIComponent(pattern)
			let url = `${idrefBaseUrl}&rows=${maxResults}`;
			if (state.recordtype === 'b') {
				url += `&q=recordtype_z:b%20AND%20corpname_${suffix}:${q}`;
			} else {
				url += `&q=(recordtype_z:b%20AND%20corpname_${suffix}:${q})%20OR%20(recordtype_z:a%20AND%20persname_${suffix}:${q})`;
			}
			return fetch(url);
		},
		queryByPpn(ppn) {
			const q = encodeURIComponent(ppn)
			const url = `${idrefBaseUrl}&rows=3&q=ppn_z:${q}`;
			return fetch(url);
		},
		extractResults() {
			const formatter = doc => {
				return {
					key: doc.id,
					idref: doc.ppn_z,
					nom: doc.affcourt_z,
					autresNoms: doc.affcourt_r.filter(x => x !== doc.affcourt_z),
					mirabel: 0,
					mirabelName: "",
				};
			};
			const exactIds = new Set;

			const numFound = Search.responses.exact?.response?.numFound;
			if (numFound === undefined) {
				state.exact.total = null;
			} else if (numFound > 0) {
				state.exact.total = numFound;
				for (const doc of Search.responses.exact.response.docs) {
					exactIds.add(doc.id);
					state.exact.results.push(formatter(doc));
				}
			} else {
				state.exact.total = 0;
			}

			const numFoundAppr = Search.responses.appr?.response?.numFound;
			if (numFoundAppr === undefined) {
				state.appr.total = null;
			} else if (numFoundAppr > 0) {
				state.appr.total = numFoundAppr - numFound;
				if (state.appr.total < 0) {
					state.appr.total = 0;
				}
				// At most 20 results, excluding those present in "exact results".
				let count = 0;
				for (const doc of Search.responses.appr.response.docs) {
					if (!exactIds.has(doc.id)) {
						count++;
						if (count > 20) {
							break;
						}
						state.appr.results.push(formatter(doc));
					}
				}
			} else {
				state.appr.total = 0;
			}
		},
		identifyLocalPublishers() {
			const idrefToObject = new Map;
			if (state.exact.error === "" && state.exact.results.length > 0) {
				for (const r of state.exact.results) {
					idrefToObject.set(r.idref, r);
				}
			}
			if (state.appr.error === "" && state.appr.results.length > 0) {
				for (const r of state.appr.results) {
					idrefToObject.set(r.idref, r);
				}
			}
			const idrefs = [...idrefToObject.keys()];
			if (idrefs.length === 0) {
				return Promise.resolve();
			}
			return fetch(`/editeur/idref?idref=${idrefs.join(',')}`)
				.then(response => response.json())
				.then(records => {
					for (const publisher of records) {
						idrefToObject.get(publisher.idref).mirabel = publisher.id;
						idrefToObject.get(publisher.idref).mirabelName = publisher.nom;
					}
				})
		},
		searchFromState() {
			if (state.lock || state.query === '') {
				return Promise.resolve(false);
			}
			state.lock = true;
			m.redraw();
			state.appr = {
				error: "",
				results: [],
				total: null,
			};
			state.exact = {
				error: "",
				results: [],
				total: null,
			};
			const query = state.query;
			return Promise.allSettled([
					Search.searchExact(query),
					Search.searchAppr(query),
				])
				.then(Search.extractResults)
				.then(Search.identifyLocalPublishers)
				.finally(() => {
					state.lock = false;
					m.redraw();
				});
		},
		searchAppr(text) {
			if (text.match(/^\d{8}[0-9Xx]$/)) {
				Search.responses.appr = [];
				return Promise.resolve(null);
			}
			const noquotes = text.replaceAll('"', '');
			const pattern = (text.includes(" ") ? `"${noquotes}"~10` : `"${noquotes}"`);
			return Search.query('t', pattern, 30)
				.then(response => {
					if (!response.ok) {
						throw new Error("Network response was not 200 OK but " + response.status);
					}
					return response.json();
				})
				.then(data => {
					Search.responses.appr = data;
				})
				.catch(err => {
					console.log("recherche approximative", err);
					state.appr.error = DLD_ERROR;
				});
		},
		searchExact(text) {
			let searchPromise;
			if (text.match(/^\d{8}[0-9Xx]$/)) {
				searchPromise = Search.queryByPpn(text);
			} else {
				const pattern = `"${text.replaceAll('"', '')}"`;
				searchPromise = Search.query('s', pattern, 10);
			}
			return searchPromise
				.then(response => {
					if (!response.ok) {
						throw new Error("Network response was not 200 OK but " + response.status);
					}
					return response.json();
				})
				.then(data => {
					Search.responses.exact = data;
				})
				.catch(err => {
					console.log("recherche exacte", err);
					state.exact.error = DLD_ERROR;
				});
		},
	}

	const SearchBox = {
		view: function() {
			return m("form.form-horizontal.form-search", {onsubmit: () => { Search.searchFromState(); return false;} },
				m('div.control-group', m('div.input-append',
					m("label.control-label", {"for": "publishername", style: "font-weight: bold"}, "Nom de l'éditeur  "),
					m("input.input-large.search-query", {
						type: "search",
						name: "publishername",
						value: state.query,
						onchange: function(e) {
							state.query = e.target.value;
						},
					}),
					m('button', {type: "button", class: "btn", onclick: Search.searchFromState}, "Rechercher"),
					m('span.label.label-info', {style: "margin-left: 2ex;"}, (state.lock ? "requêtes en cours…" : "")),
				)),
				m('div.control-group', m('div.controls',
					state.query === '' ? null : m('label',
						m('input[type="checkbox"]', {onchange: toggleRecordtype}),
						" Recherche étendue aux personnes (libraires-éditeurs)"
					)
				)),
			);
		},
	};

	const ResultsBox = {
		view: function() {
			console.log(state.exact, state.appr)
			return m("div",
				m(ResultsList, {name: "Correspondances exactes", contents: state.exact, buttonNoIdref: false}),
				m(ResultsList, {name: "Correspondances approximatives", contents: state.appr, buttonNoIdref: true}),
				(state.appr.results.length < state.appr.total) ? m('div', "Résultats limités à 20 réponses.") : null,
			);
		},
	};

	const ResultsList = {
		view: function(vnode) {
			const name = vnode.attrs.name;
			const contents = vnode.attrs.contents; // error, results, total
			if (contents.error !== "") {
				return [
					m("h2", name),
					m("div.alert.alert-error", m.trust(contents.error)),
				];
			}
			if (contents.total === null) {
				return null;
			}
			return m("section", {style: "margin-top: 2em"},
				m("h2", name, contents.total > 0 ? m('small', ` (${contents.total} dans idref.fr)`) : null),
				contents.results.length === 0 ?
					m('div', "Aucune correspondance")
					: contents.results.map(v => m(Result, {v})),
				vnode.attrs.buttonNoIdref ? m(CreateButtonNoIdref) : null,
			);
		},
	}

	const Result = {
		view: function(vnode) {
			const v = vnode.attrs.v;
			return m("div.well.well-small", {style: "display: flex"},
				m("div", {style: "flex: 1"},
					m("ul.unstyled",
						m("li", m("strong", v.nom)),
						m(Li, {label: "Autres noms", value: v.autresNoms.join(" ; ")}),
						m(Li, {
							label: "Idref",
							value: m(
								'a',
								{href: `https://www.idref.fr/${v.idref}`, target: "_blank", title: "Consulter la notice dans un nouvel onglet"},
								v.idref
							)
						}),
					),
				),
				m("div",
					m(CreateButton, {v})
				),
			);
		},
	}

	const CreateButtonNoIdref = {
		oncreate: function(vnode) {
			if (!isEmbeddedInModal) {
				return;
			}
			vnode.dom.addEventListener('click', (ev) => {
				const dialog = vnode.dom.closest('dialog');
				loadEmbeddedForm(dialog, '/editeur/create-final', new FormData());
				ev.preventDefault();
				return false;
			})
		},
		view() {
			return m('div', {style: "text-align: right"},
				m('a.btn.btn-primary', {href: '/editeur/create-final'}, [
					"Créer un éditeur ", m('strong', "sans pré-remplissage IdRef")
				])
			)
		}
	}

	const CreateButton = {
		view(vnode) {
			const v = vnode.attrs.v;
			if (v.mirabel > 0) {
				return m('div', {style: "display:flex; flex-direction:column;"}, [
					m(
						"a",
						{href: `/editeur/${v.mirabel}`, target: "_blank", title: "Consulter la page de cet éditeur dans un nouvel onglet"},
						"Présent dans Mir@bel"
					),
					isEmbeddedInModal ?
						m("button.btn.btn-primary.link-publisher", {type: "button", "data-id": v.mirabel, "data-name": v.mirabelName}, "Associer au titre")
						: null,
				]);
			}
			return m("form.form-inline", { action: "/editeur/create-final", method: "POST", enctype: "multipart/form-data"},
				m('input', {type: "hidden", name: "prefill", value: 1}),
				m('input', {type: "hidden", name: "Editeur[nom]", value: v.nom}),
				m('input', {type: "hidden", name: "Editeur[idref]", value: v.idref}),
				m('button.btn.btn-success', {type: "submit"}, "Créer"),
			);
		}
	}

	const Li = {
		view: function(vnode) {
			if (vnode.attrs.value === null || vnode.attrs.value === '') {
				return null;
			}
			return m("li",
				m("span", vnode.attrs.label + " : "),
				vnode.attrs.value,
			);
		},
	}

	const Main = {
		oncreate: function(vnode) {
			const dialog = vnode.dom.closest('dialog');
			if (dialog === null) {
				return;
			}
			isEmbeddedInModal = true;
			// This form is embedded in a modal dialog.

			dialog.querySelector('.modal-footer button#submit-editeur').setAttribute('style', 'display:none;');
			vnode.dom.addEventListener('submit', (ev) => {
				loadEmbeddedForm(dialog, ev.target.action, new FormData(ev.target));
				ev.preventDefault();
				return false;
			})
		},
		view: function() {
			return m("div",
				m(SearchBox),
				m(ResultsBox),
			);
		}
	};

	return {
		init(htmlId) {
			// find the root HTML element
			state = Object.assign({}, start.state);
			start.root = document.getElementById(htmlId);
			if (!start.root) {
				alert("JS bug, root element #" + htmlId + " was not found.");
				return;
			}

			m.mount(start.root, Main);
		},
		reset() {
			state = Object.assign({}, start.state);
			m.redraw();
		},
	};
}());
