<?php

namespace widgets;

class SearchNavigationDisplay
{
	public function run(): string
	{
		$this->loadJavascriptAssets();
		return '<div id="search-navigation"></div>';
	}

	protected function loadJavascriptAssets(): void
	{
		$am = \Yii::app()->assetManager;
		$cs = \Yii::app()->clientScript;

		$cs->registerScriptFile(
			$am->publish(__DIR__ . "/assets/search-navigation-display.js"),
			\CClientScript::POS_HEAD,
			['type' => 'module']
		);
	}
}
