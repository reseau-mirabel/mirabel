<?php

namespace widgets;

class PolitiqueValidation extends Mithril
{
	private array $data;

	public function __construct(array $users, array $roles)
	{
		parent::__construct();
		$this->data = [
			'users' => array_values($users), // will be serialized with Utilisateur::jsonSerialize()
			'roles' => $roles,
			'owners' => self::getPublishersOwners(),
		];
	}

	protected function getData(): array
	{
		return $this->data;
	}

	protected function getJavascriptAssets(): array
	{
		return ['dist/politique-validation.js'];
	}

	private static function getPublishersOwners(): array
	{
		$cmd = \Yii::app()->db->createCommand(
			<<<EOSQL
			SELECT
				ue.editeurId,
				u.id,
				u.nomComplet
			FROM Utilisateur_Editeur ue
				JOIN Utilisateur u ON u.id = ue.utilisateurId
			WHERE ue.`role` = 'owner' AND ue.confirmed = 1 AND u.actif = 1
			EOSQL
		);
		$data = [];
		foreach ($cmd->query() as $row) {
			$editeurId = $row['editeurId'];
			if (!isset($data[$editeurId])) {
				$data[$editeurId] = [
					(int) $editeurId,
					[],
				];
			}
			$data[$editeurId][1][] = ['id' => (int) $row['id'], 'nomComplet' => $row['nomComplet']];
		}
		$guestRows = \Yii::app()->db->createCommand(
			"SELECT editeurId, count(*) FROM Utilisateur_Editeur WHERE role = 'guest' AND confirmed = 1 GROUP BY editeurId"
		)->queryAll(false);
		foreach ($guestRows as [$editeurId, $num]) {
			if (!isset($data[$editeurId])) {
				continue;
			}
			if ($num > 1) {
				$data[$editeurId][1][] = ['id' => 0, 'nomComplet' => "+ {$num} délégués"];
			} else {
				$data[$editeurId][1][] = ['id' => 0, 'nomComplet' => "+ 1 délégué"];
			}
		}
		return array_values($data);
	}
}
