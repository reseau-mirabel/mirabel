<?php

namespace widgets;

use Politique;

class PolitiqueExpert extends Mithril
{
	public Politique $politique;

	public function __construct(Politique $politique)
	{
		parent::__construct();
		$this->politique = $politique;
	}

	protected function getData(): array
	{
		$isAdminPolitique = \Yii::app()->user->access()->toPolitique()->isAdmin();
		return [
			'hints' => \Hint::model()->getMessages('Politique'),
			'translations' => self::getConfigPolitique(),
			'policy' => json_decode($this->politique->publisher_policy),
			'politique' => [
				'id' => (int) $this->politique->id,
				'editeurId' => (int) $this->politique->editeurId,
				'editeur' =>  $this->politique->editeur->nom,
				'model' => $this->politique->model === null ? null : (int) $this->politique->model,
				'notesAdmin' => $isAdminPolitique ? ($this->politique->notesAdmin ?: " ") : '',
				'titre' => ($this->politique->initialTitreId > 0) && $isAdminPolitique
					? \Yii::app()->db->createCommand("SELECT titre FROM Titre WHERE id = :id")->queryScalar([':id' => $this->politique->initialTitreId])
					: "",
				'status' => $this->politique->status,
				'statusDate' => self::getStatusDate(),
			],
			'readonly' => !\Yii::app()->user->access()->toPolitique()->update($this->politique),
		];
	}

	protected function getJavascriptAssets(): array
	{
		return ['dist/politique-expert.js'];
	}

	private static function getConfigPolitique(): array
	{
		$records = \Config::model()->findAllBySql("SELECT * FROM Config WHERE category = 'politique'");
		$result = [];
		foreach ($records as $r) {
			$k = substr($r->key, 16); // Idem str_replace('politique.liste.', '', $r->key);
			$result[$k] = $r->decode();
		}
		return $result;
	}

	private function getStatusDate(): string
	{
		$actions = [
			Politique::STATUS_DELETED => \PolitiqueLog::ACTION_DELETE,
			Politique::STATUS_PUBLISHED => \PolitiqueLog::ACTION_PUBLISH,
			Politique::STATUS_TOPUBLISH => \PolitiqueLog::ACTION_TOPUBLISH,
			Politique::STATUS_UPDATED => \PolitiqueLog::ACTION_UPDATE,
		];
		$action = $actions[$this->politique->status] ?? '';
		if (!$action) {
			return '';
		}
		return (string) \Yii::app()->db
			->createCommand("SELECT FROM_UNIXTIME(MAX(actionTime)) FROM PolitiqueLog WHERE action = :a AND politiqueId = :id")
			->queryScalar([':id' => $this->politique->id, ':a' => $action]);
	}
}
