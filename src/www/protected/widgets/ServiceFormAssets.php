<?php

namespace widgets;

use Titre;

class ServiceFormAssets
{
	private array $journals = [];

	public function run(): void
	{
		$this->loadJavascriptAssets();
		if ($this->journals) {
			self::insertJsonData($this->journals);
		}
	}

	public function addJournal(Titre $titre): void
	{
		$this->journals[] = $titre;
	}

	/**
	 * @param Titre[] $journals
	 */
	public function setJournals(array $journals): void
	{
		$this->journals = array_values($journals);
	}

	protected function loadJavascriptAssets(): void
	{
		$am = \Yii::app()->assetManager;
		$cs = \Yii::app()->clientScript;

		$cs->registerScriptFile(
			$am->publish(__DIR__ . "/assets/service-form.js"),
			\CClientScript::POS_HEAD,
			['type' => "module"]
		);
	}

	private static function insertJsonData(array $journals): void
	{
		$journalData = array_map(
			fn (\Titre $t) => ['id' => (int) $t->id, 'dateDebut' => $t->dateDebut, 'dateFin' => $t->dateFin],
			$journals
		);
		echo '<script id="journal-data" type="application/json">'
			. htmlspecialchars(
				json_encode($journalData, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE),
				ENT_NOQUOTES
			)
			. "</script>\n";
	}
}
