<?php

namespace widgets;

/**
 * To create a JS filter that lets the user include or exclude values from a list,
 * create a class that inherits from this abstract class,
 * then implement the abstract methods.
 */
abstract class IncludeExcludeFilter
{
	private bool $debug;

	/**
	 * @var array{id: int, nom: string}[]
	 */
	private array $sources = [];

	public function __construct()
	{
		$this->debug = defined('YII_DEBUG') && YII_DEBUG;
		$this->sources = $this->fetchSources();
	}

	/**
	 * @param int[] $ids List of integer ids (negative if excluded)
	 * @return string HTML
	 */
	public function run(array $ids): string
	{
		$this->loadJavascriptAssets();

		$data = $this->getDisplayData();
		$data['sources'] = $this->sources;
		$data['ids'] = $this->filterIds($ids);

		$encData = json_encode($data);
		$id = "IncludeExcludeFilter-" . md5($encData);
		// Call the JS init function
		\Yii::app()->clientScript->registerScript($id, "includeExcludeFilter().init('$id', $encData);");

		// The HTML block that will be dynamically filled.
		return '<div id="' . $id . '"></div>';
	}

	abstract protected function fetchSources(): array;

	abstract protected function getDisplayData(): array;

	protected function filterIds(array $ids): array
	{
		$allowed = [];
		foreach ($this->sources as $s) {
			$allowed[$s['id']] = true;
		}

		$result = [];
		foreach ($ids as $num) {
			$id = ($num < 0 ? 0 - (int) $num : (int) $num);
			if (isset($allowed[$id])) {
				$result[] = (int) $num;
			}
		}
		return $result;
	}

	protected function loadJavascriptAssets(): void
	{
		$am = \Yii::app()->assetManager;
		$cs = \Yii::app()->clientScript;

		$mithrilPath = VENDOR_PATH . '/npm-asset/mithril/' . ($this->debug ? 'mithril.js' : 'mithril.min.js');
		$cs->registerScriptFile($am->publish($mithrilPath));

		// This JS will load the code needed for includeExcludeFilter().init(htmlID, config)
		$cs->registerScriptFile($am->publish(__DIR__ . "/assets/include-exclude-filter.js"));
	}
}
