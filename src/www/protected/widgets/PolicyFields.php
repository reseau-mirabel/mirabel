<?php

namespace widgets;

use models\forms\PolitiqueForm;

class PolicyFields
{
	public bool $debug;

	private PolitiqueForm $policy;

	public function __construct(PolitiqueForm $policy)
	{
		$this->debug = defined('YII_DEBUG') && YII_DEBUG;
		$this->policy = $policy;
	}

	/**
	 * @return string HTML
	 */
	public function run(): string
	{
		$this->loadJavascriptAssets();

		$data = [
			'policy' => $this->policy,
			'hints' => \Hint::model()->getMessages('Policy'),
			'translations' => self::getSherpaConfig(),
		];
		$encData = json_encode($data);
		$id = "PolicyFields-" . md5($encData);
		// Call the JS init function
		\Yii::app()->clientScript->registerScript($id, "policyFields.init('$id', $encData);");

		// The HTML block that will be dynamically filled.
		return '<div id="' . $id . '"></div>';
	}

	private static function getSherpaConfig(): array
	{
		$records = \Config::model()->findAllByAttributes(['category' => 'sherpa']);
		$result = [];
		foreach ($records as $r) {
			$result[$r->key] = $r->decode();
		}
		return $result;
	}

	private function loadJavascriptAssets(): void
	{
		$am = \Yii::app()->assetManager;
		$cs = \Yii::app()->clientScript;

		$mithrilPath = VENDOR_PATH . '/npm-asset/mithril/' . ($this->debug ? 'mithril.js' : 'mithril.min.js');
		$cs->registerScriptFile($am->publish($mithrilPath));

		$cs->registerScriptFile($am->publish(__DIR__ . "/assets/policy-fields.js"));
	}
}
