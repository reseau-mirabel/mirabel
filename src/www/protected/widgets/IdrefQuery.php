<?php

namespace widgets;

class IdrefQuery extends Mithril
{
	protected function getData(): array
	{
		return [];
	}

	protected function getJavascriptAssets(): array
	{
		return ['idref-query.js'];
	}
}
