<?php

namespace widgets;

class LinksForm
{
	private string $modelName;

	public function __construct(string $modelName)
	{
		$this->modelName = $modelName;
	}

	public function run(string $parentSelector = ''): void
	{
		$this->loadJavascriptAssets();
		echo '<script id="linksDefaultSources" type="application/json">' . json_encode(\Sourcelien::listNames(), JSON_UNESCAPED_UNICODE) . "</script>\n";
		\Yii::app()->clientScript->registerScript(
			"links-input-{$this->modelName}",
			"initLinksForm('$parentSelector', '{$this->modelName}');"
		);
	}

	protected function loadJavascriptAssets(): void
	{
		$am = \Yii::app()->assetManager;
		$cs = \Yii::app()->clientScript;

		$cs->registerScriptFile($am->publish(__DIR__ . "/assets/links-form.js"));
	}
}
