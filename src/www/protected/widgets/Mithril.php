<?php

namespace widgets;

/**
 * Creating a custom component using mithril:
 *
 * 1. Create a file `assets/mywidget.js`.
 *    The JS for a class MyWidget must have a MyWidget(id, data) function.
 * 2. Create a file MyWidget.php,
 *    where loadJavascriptAssets() returns ['mywidget.js'].
 * 3. getData() must return the init data of the component.
 *
 * In the PHP view, call `(new MyWidget(...))->run()`.
 * If the HTML container element is static, pass its id: `(new MyWidget(...))->run('html-container-id')`.
 *
 * @codeCoverageIgnore
 */
abstract class Mithril
{
	public bool $debug;

	public function __construct()
	{
		$this->debug = defined('YII_DEBUG') && YII_DEBUG;
	}

	/**
	 * The JS for a class MyWidget must have a MyWidget(id, data) function.
	 *
	 * @param string $htmlid HTML id where the component will mount. If empty, a DIV block will be created.
	 * @return string HTML
	 */
	public function run(string $htmlid = ''): string
	{
		if (YII_ENV === 'test') {
			return '';
		}

		$this->loadJavascriptAssets($this->getJavascriptAssets());

		$encData = json_encode($this->getData());
		if ($htmlid) {
			$id = $htmlid;
		} else {
			$id = $this->getBaseName() . "-" . md5($encData);
		}
		// Call the JS init function
		\Yii::app()->clientScript->registerScript($id, $this->getBaseName() . ".init('$id', $encData);");

		if ($htmlid) {
			return '';
		}
		// The HTML block that will be dynamically filled.
		return <<<"EOHTML"
			<div id="$id"></div>
			<noscript>
				<p class="alert alert-error">JavaScript doit être activé dans le navigateur pour que cette page fonctionne correctement.</p>
			</noscript>
			EOHTML;
	}

	abstract protected function getData(): array;

	/**
	 * Return paths to JS files that will be published then loaded.
	 */
	abstract protected function getJavascriptAssets(): array;

	protected function loadJavascriptAssets(array $assets): void
	{
		$am = \Yii::app()->assetManager;
		$cs = \Yii::app()->clientScript;

		$mithrilPath = VENDOR_PATH . '/npm-asset/mithril/' . ($this->debug ? 'mithril.js' : 'mithril.min.js');
		$cs->registerScriptFile($am->publish($mithrilPath));

		foreach ($assets as $file) {
			$cs->registerScriptFile(
				$am->publish($file[0] === '/' ? $file : __DIR__ . "/assets/$file")
			);
		}
	}

	private function getBaseName(): string
	{
		$c = get_called_class();
		$p = strrpos($c, '\\');
		return substr($c, $p + 1);
	}
}
