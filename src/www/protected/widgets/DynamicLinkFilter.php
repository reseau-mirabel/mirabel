<?php

namespace widgets;

class DynamicLinkFilter extends IncludeExcludeFilter
{
	protected function fetchSources(): array
	{
		return array_map(
			function (array $x): array {
				return [
					'id' => (int) $x[0],
					'nom' => $x[1],
				];
			},
			\Yii::app()->db->createCommand("SELECT id, nom FROM Sourcelien ORDER BY nom")->queryAll(false)
		);
	}

	protected function getDisplayData(): array
	{
		return [
			'label' => "Autres liens",
			'name' => 'SearchTitre[lien]',
			'hint' => \Hint::model()->getMessage('SearchTitre', 'lien'),
		];
	}
}
