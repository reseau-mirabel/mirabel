<?php

namespace widgets;

use CHtml;
use CHttpCookie;

class CustomizePager
{
	public const VIEWMODE_DETAILS = 1;

	public const VIEWMODE_SIMPLE = 2;

	private const PAGESIZE_VALUES = [
		self::VIEWMODE_DETAILS => [10, 25],
		self::VIEWMODE_SIMPLE => [25, 50, 100],
	];

	private int $pageSize;

	private int $viewMode;

	public function __construct(\CCookieCollection $cookies)
	{
		$this->parseCookies($cookies);
	}

	public function run(): string
	{
		self::loadJs();

		return "<div>{$this->buildViewmodeHtml()} {$this->buildPagesizeHtml()}</div>";
	}

	public function getPageSize(): int
	{
		return $this->pageSize;
	}

	public function getViewMode(): int
	{
		return $this->viewMode;
	}

	private function buildPagesizeHtml(): string
	{
		return CHtml::dropDownList(
			'sel-pagesize',
			(string) $this->pageSize,
			array_combine(self::PAGESIZE_VALUES[$this->viewMode], self::PAGESIZE_VALUES[$this->viewMode]),
			['class' => 'sel-pagesize', 'style' => "width:8ex"]
		)
			. ' <label for="sel-pagesize" style="display:inline">par page</label>';
	}

	private function buildViewmodeHtml(): string
	{
		return "Résultats"
			. CHtml::dropDownList(
				'sel-viewmode',
				(string) $this->viewMode,
				[self::VIEWMODE_DETAILS => "détaillés", self::VIEWMODE_SIMPLE => "simples"],
				['class' => 'sel-viewmode', 'style' => "width:12ex"]
			);
	}

	private function parseCookies(\CCookieCollection $cookies): void
	{
		$viewModeCookie = $cookies->itemAt('searchres');
		$pageSizeCookie = $cookies->itemAt('pagesize');

		$viewMode = $viewModeCookie ? (int) $viewModeCookie->value : 0;
		$pageSize = $pageSizeCookie ? (int) $pageSizeCookie->value : 0;

		if ($viewMode === self::VIEWMODE_SIMPLE) {
			$this->viewMode = self::VIEWMODE_SIMPLE;
		} else {
			$this->viewMode = self::VIEWMODE_DETAILS;
		}
		if (in_array($pageSize, self::PAGESIZE_VALUES[$this->viewMode], true)) {
			$this->pageSize = $pageSize;
		} else {
			$this->pageSize = self::PAGESIZE_VALUES[$this->viewMode][0];
		}

		// If the user has a setting which differs from the effective value,
		// we store a cookie (until the browser closes).
		if ($viewModeCookie && $this->viewMode !== $viewMode) {
			$cookies->add('searchres', new CHttpCookie('searchres', (string) $this->viewMode));
		}
		if ($pageSizeCookie && $this->pageSize !== $pageSize) {
			$cookies->add('pagesize', new CHttpCookie('pagesize', (string) $this->pageSize));
		}
	}

	private function loadJs(): void
	{
		\Yii::app()->clientScript->registerScript('customizepager', <<<'EOJS'
			for(const selPagesize of document.querySelectorAll('.sel-pagesize')) {
				selPagesize.addEventListener('change', function(e) {
					document.cookie = "pagesize=" + this.value + ';path=/';
					window.location.replace(window.location.href.replace(/[&\?]page=\d+/, ''));
				});
			}
			for (const sel of document.querySelectorAll('.sel-viewmode')) {
				sel.addEventListener('change', function(e) {
					document.cookie = "searchres=" + this.value + ';path=/';
					window.location.replace(window.location.href.replace(/[&\?]page=\d+/, ''));
				});
			}
			EOJS
		);
	}
}
