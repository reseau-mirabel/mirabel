<?php

namespace widgets;

class PolitiqueAssistant extends Mithril
{
	private int $editeurId;

	private int $titreId;

	private string $returnUrl;

	public function __construct(int $editeurId, int $titreId, mixed $returnUrl)
	{
		parent::__construct();
		$this->editeurId = $editeurId;
		$this->titreId = $titreId;
		if (is_array($returnUrl)) {
			$route = array_shift($returnUrl);
			$this->returnUrl = \Yii::app()->createUrl($route, $returnUrl);
		} elseif (is_string($returnUrl)) {
			$this->returnUrl = $returnUrl;
		} else {
			throw new \Exception("The return URL is not valid.");
		}
	}

	protected function getData(): array
	{
		return [
			'editeurId' => $this->editeurId,
			'hints' => \Hint::model()->getMessages('Policy'),
			'titreId' => $this->titreId,
			'translations' => self::getConfigPolitique(),
			'returnUrl' => $this->returnUrl,
		];
	}

	protected function getJavascriptAssets(): array
	{
		return ['dist/politique-assistant.js'];
	}

	private static function getConfigPolitique(): array
	{
		$records = \Config::model()->findAllBySql("SELECT * FROM Config WHERE category = 'politique'");
		$result = [];
		foreach ($records as $r) {
			$k = substr($r->key, 16); // Idem str_replace('politique.liste.', '', $r->key);
			$result[$k] = $r->decode();
		}
		return $result;
	}
}
