<?php

namespace widgets;

use CJavaScript;
use Yii;

/**
 * Load the assets to draw an interactive world map.
 */
class WorldMap
{
	public function __construct(array $mapConfig, string $stamen = '')
	{
		$this->loadLeaflet();
		if ($stamen !== '') {
			Yii::app()->clientScript->registerScriptFile("https://maps.stamen.com/js/tile.stamen.js?v1.3.0");
		}

		$this->loadLocalJs();
		Yii::app()->clientScript->registerScript(
			'mapinit',
			'drawWorldMap(' . CJavaScript::encode($mapConfig) . ', ' . json_encode($stamen) . ');'
		);
	}

	public function run(): string
	{
		return <<<EOHTML
<div id="world-map" style="height: 600px;"></div>
<noscript>
	<p class="alert alert-warning">
		Cette carte ne s'affiche pas si JavaScript est désactivé dans votre navigateur.
	</p>
</noscript>
EOHTML;
	}

	private function loadLeaflet(): void
	{
		$url = \Yii::app()->assetManager->publish(VENDOR_PATH . '/npm-asset/leaflet/dist');
		Yii::app()->clientScript->registerCssFile("$url/leaflet.css");
		Yii::app()->clientScript->registerScriptFile("$url/leaflet.js");

		$url = \Yii::app()->assetManager->publish(VENDOR_PATH . '/npm-asset/leaflet.markercluster/dist');
		Yii::app()->clientScript->registerCssFile("$url/MarkerCluster.Default.css");
		Yii::app()->clientScript->registerCssFile("$url/MarkerCluster.css");
		Yii::app()->clientScript->registerScriptFile("$url/leaflet.markercluster.js");
	}

	private function loadLocalJs(): void
	{
		$am = \Yii::app()->assetManager;
		$cs = Yii::app()->clientScript;
		$cs->registerScriptFile($am->publish(__DIR__ . "/assets/worldmap/countries.js"));
		$cs->registerScriptFile($am->publish(__DIR__ . "/assets/worldmap/world-map.js"));
	}
}
