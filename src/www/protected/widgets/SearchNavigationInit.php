<?php

namespace widgets;

class SearchNavigationInit
{
	private string $type;

	private array $result;

	private int $maxPageRank;

	public function __construct(string $type, array $result, int $maxPageRank)
	{
		$this->type = $type;
		$this->result = $result;
		$this->maxPageRank = $maxPageRank;
	}

	public function run(): void
	{
		$this->loadJavascriptAssets();

		$scriptId = 'navigation-storage-init-' . $this->type;
		$json = json_encode([
			'type' => $this->type,
			'results' => $this->getContentList(),
			'maxPageRank' => $this->maxPageRank,
		]);
		echo <<<HTML
			<script id="$scriptId" class="navigation-storage-init" type="application/json">$json</script>
			HTML;
	}

	protected function loadJavascriptAssets(): void
	{
		$am = \Yii::app()->assetManager;
		$cs = \Yii::app()->clientScript;

		$cs->registerScriptFile($am->publish(__DIR__ . "/assets/search-navigation-init.js"));
	}

	/**
	 * @return list<array{"id": int, "name": string}>
	 */
	private function getContentList(): array
	{
		$liste = [];
		$id = ($this->type === 'revue' ? 'revueid' : 'id');
		$nom = ($this->type === 'revue' ? 'titrecomplet' : 'nomcomplet');
		foreach ($this->result as $r) {
			$liste[] = ['id' => (int) $r->{$id}, 'name' => $r->{$nom}];
		}
		return $liste;
	}
}
