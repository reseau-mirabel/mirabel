<?php

namespace widgets;

/**
 * Load the JS library that applies CkEditor to "textarea.htmleditor".
 */
class CkEditor
{
	public function __construct()
	{
		$url = \Yii::app()->assetManager->publish(VENDOR_PATH . '/ckeditor/ckeditor');
		\Yii::app()->clientScript->registerScriptFile("$url/ckeditor.js");

		// Inline config avoids a network request to load ckeditor/config.js.
		\Yii::app()->clientScript->registerScript(
			'ckeditor',
			<<<EOJS
			CKEDITOR.replaceAll(function( textarea, config ) {
				config.contentsLanguage = 'fr';
				config.language = 'fr';
				config.versionCheck = false;

				if (!textarea.classList.contains('htmleditor')) {
					return false;
				}

				// Do not filter the content (by default, CKeditor strips classes on load)
				config.allowedContent = true;

				// Whether a filler text (non-breaking space entity — &nbsp;) will be inserted into empty block elements
				config.fillEmptyBlocks = false;

				// The toolbar groups arrangement, optimized for two toolbar rows.
				config.toolbarGroups = [
					{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
					{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
					{ name: 'links' },
					{ name: 'insert' },
					{ name: 'tools' },
					{ name: 'document',    groups: [ 'mode', 'document', 'doctools' ] },
					{ name: 'others' },
					'/',
					{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
					{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align' ] },
					{ name: 'styles' },
					{ name: 'colors' },
				];

				// Remove some buttons provided by the standard plugins, which are
				// not needed in the Standard(s) toolbar.
				config.removeButtons = 'Underline,Subscript,Superscript';

				// Set the most common block elements.
				config.format_tags = 'p;h2;h3;pre';

				// Simplify the dialog windows.
				config.removeDialogTabs = 'image:advanced;link:advanced';

				// Whether to use HTML entities in the editor output.
				config.entities = false;

				// Whether to convert some Latin characters (Latin alphabet No. 1, ISO 8859-1) to HTML entities
				config.entities_latin = false;

				// Whether the editor must output an empty value ('') if its content only consists of an empty paragraph.
				config.ignoreEmptyParagraph = true;

				return true;
			});
			EOJS
		);
	}
}
