<?php

namespace widgets;

use Yii;

/**
 * Display the the password strength next to the tags "input.$class".
 */
class PasswordStrength
{
	public function __construct()
	{
		$am = \Yii::app()->assetManager;
		$cs = Yii::app()->clientScript;
		$path = VENDOR_PATH . '/npm-asset/';
		foreach (['core', 'language-common', 'language-en', 'language-fr'] as $package) {
			$cs->registerScriptFile($am->publish("$path/zxcvbn-ts--{$package}/dist/zxcvbn-ts.js"));
		}
	}

	/**
	 * @param string[] $userInputs Terms that appear elsewhere in the user data (email, etc).
	 */
	public function run(string $class, array $userInputs): void
	{
		$encodedUserInput = json_encode(self::cleanupInputs($userInputs));

		$cs = Yii::app()->clientScript;
		$cs->registerScript(
			"zxcvbn-$class",
			<<<EOJS
window.zxcvbnts.core.zxcvbnOptions.setOptions({
	translations: zxcvbnts['language-en'].translations,
	graphs: zxcvbnts['language-common'].adjacencyGraphs,
	dictionary: {
		...zxcvbnts['language-common'].dictionary,
		...zxcvbnts['language-en'].dictionary,
		...zxcvbnts['language-fr'].dictionary,
		userInputs: {$encodedUserInput},
	},
})
$("input.{$class}").after('<span class="help-inline {$class}-result"></span>')
$("input.{$class}").on('input', function() {
	let feedback = $(this).closest(".control-group");
	let feedbackResult = feedback.find(".{$class}-result");
	if (this.value === '') {
		feedback.removeClass("error success");
		feedbackResult.html('');
	} else if (zxcvbnts.core.zxcvbn(this.value).score >= 3) {
		feedback.removeClass('error').addClass("success");
		feedbackResult.html('<span class="glyphicon glyphicon-ok">OK</span>');
	} else {
		feedback.removeClass('success').addClass("error");
		feedbackResult.html('sécurité insuffisante');
	}
})
EOJS
		);
	}

	private static function cleanupInputs(array $userInputs): array
	{
		$inputs = [];
		foreach ($userInputs as $in) {
			if (!$in) {
				continue;
			}
			$inputs[] = $in;
			if (strpos($in, '@') > 0) {
				foreach (explode("@", $in) as $part) {
					$inputs[] = $part;
				}
			}
		}
		return array_filter($inputs);
	}
}
