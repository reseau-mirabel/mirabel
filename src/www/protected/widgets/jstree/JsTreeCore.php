<?php

namespace widgets\jstree;

class JsTreeCore
{
	/**
	 * @var array See http://www.jstree.com/api/#/?f=$.jstree.defaults.core.data
	 */
	public array $data;

	/**
	 * @var bool|int The open / close animation duration in milliseconds.
	 */
	public bool|int $animation = false;

	/**
	 * @var bool|string Allow modifying operations on the tree.
	 *                     To enable editing, set it to true or to a JS function.
	 */
	public bool|string $checkCallback = false;

	/**
	 * @var bool Web workers will be used to parse incoming JSON data,
	 *    so that the UI will not be blocked by large requests.
	 *    Workers are however about 30% slower.
	 */
	public bool $worker = true;

	public array $themes = [];

	public array $strings = [];

	public function translateToFr(): void
	{
		$this->strings = [
			"Loading ..." => "Chargement en cours",
			"New node" => "Nouveau nœud",
		];
	}

	public function getConfig(): array
	{
		$config = [
			"data" => $this->data,
			"animation" => $this->animation,
			"worker" => $this->worker,
			"check_callback" => $this->checkCallback,
		];
		if ($this->strings) {
			$config["strings"] = $this->strings;
		}
		if ($this->themes) {
			$config["themes"] = $this->themes;
		}
		return $config;
	}
}
