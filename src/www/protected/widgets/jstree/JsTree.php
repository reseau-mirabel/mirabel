<?php

namespace widgets\jstree;

use CJavaScript;
use Yii;

class JsTree
{
	public JsTreeCore $core;

	public array $types = [];

	public array $plugins;

	public function __construct(JsTreeCore $core, array $plugins = [])
	{
		$this->core = $core;
		$this->plugins = $plugins;
	}

	public function init(string $selector = ".jstree"): void
	{
		$this->registerAssets();
		$this->registerJsInit($selector);
	}

	private function registerAssets(): void
	{
		$path = VENDOR_PATH . '/npm-asset/jstree/dist';
		$am = Yii::app()->getAssetManager();
		$cs = Yii::app()->clientScript;

		// CSS
		$themesUrl = $am->publish("$path/themes");
		if (YII_DEBUG) {
			$cssFile = 'default/style.css';
		} else {
			$cssFile = 'default/style.min.css';
		}
		$cs->registerCssFile("$themesUrl/$cssFile");

		// JS
		$cs->registerCoreScript('jquery');
		if (YII_DEBUG) {
			$file = "$path/jstree.js";
		} else {
			$file = "$path/jstree.min.js";
		}
		$cs->registerScriptFile($am->publish($file));
	}

	private function registerJsInit(string $selector): void
	{
		$init = [
			'core' => $this->core->getConfig(),
			'types' => $this->types,
			'plugins' => $this->plugins,
		];
		Yii::app()->clientScript->registerScript(
			'initTree',
			'$(' . CJavaScript::encode($selector) . ').jstree(' . CJavaScript::encode($init) . ');'
		);
	}
}
