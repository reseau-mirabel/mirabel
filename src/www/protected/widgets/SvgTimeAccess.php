<?php

namespace widgets;

/**
 * La frise SVG est une ligne composée horizontalement de 3 éléments :
 *
 * 1. L'année de départ (taille fixe)
 * 2. Le corps (trait de la flèche et sa garniture, etc) (largeur extensible)
 * 3. La pointe de la fléche, ou rien si la revue a une date de fin (taille fixe).
 *
 * La partie 2 peut elle-même se composer horizontalement de :
 *
 * 2.a. Le trait de la flèche et sa garniture (composés verticalement, largeur extensible)
 * 2.b. L'année de fin (largeur fixe)
 * 2.c. Un espace vide si année de fin (largeur extensible).
 *
 * Les largeurs fixes ou variables sont gérées par des sous-balises svg avec les attributs
 *  width, preserveAspectRatio et viewport.
 * Les hauteurs sont définies par les constantes de cette classe.
 *
 * @todo Increase date precision: use months instead of full years.
 * @todo Better drawing of the year box when the end date exists.
 *
 * @psalm-type ServicesByType array{"Restreint": object[], "Abonné": object[], "Libre": object[]}
 */
class SvgTimeAccess
{
	private const HEIGHT = 36;

	private const ARROW_COLOR = 'var(--mirabel-orange)';

	private const ARROW_THICKNESS = 4;

	private const COLORS = [
		'Restreint' => '#bbbdc0',
		'Abonné' => '#5da2d9',
		'Libre' => '#0068b4', // last one, over the others
	];

	private ?\Partenaire $partenaire = null;

	public function __construct(?\Partenaire $partenaire, bool $interactive)
	{
		$this->partenaire = $partenaire;

		if ($interactive) {
			$jsUrl = \Yii::app()->getAssetManager()->publish(__DIR__ . '/assets/svg-time-access.js');
			\Yii::app()->getClientScript()->registerScriptFile($jsUrl);
		}
	}

	/**
	 * @return string HTML ("<svg ...")
	 */
	public function run(int $revueId, \DisplayServices $services): string
	{
		[$firstYear, $lastYear] = self::findYears($revueId, $services);
		$sortedServices = $this->sortServices($services);

		return '<div class="time-access-strip">'
			. self::svgBar($firstYear, $lastYear, $sortedServices)
			. '</div>';
	}

	/**
	 * Return the start and end years across all the Titre records of this Revue,
	 * or accross Service records if data is missing from Titre.
	 *
	 * @return list{int, int}
	 */
	private static function findYears(int $revueId, \DisplayServices $services): array
	{
		$row = \Yii::app()->db
			->createCommand("SELECT min(dateDebut) AS start, max(dateFin = '') AS alive, max(dateFin) AS end FROM Titre WHERE revueId = :rid GROUP BY revueId")
			->queryRow(true, [':rid' => $revueId]);
		$firstYear = (int) $row['start'];
		$lastYear = $row['alive'] ? 0 : (int) $row['end'];
		if ($firstYear === 0) {
			$firstYear = self::findOldestYear($services->filterServices());
		}
		return [$firstYear, $lastYear];
	}

	/**
	 * Find the oldest year in all the Service records.
	 *
	 * @param \Service[] $services
	 */
	private static function findOldestYear(array $services): int
	{
		$year = (int) date('Y');
		foreach ($services as $s) {
			assert($s instanceof \Service);
			if ($s->dateBarrDebut && (int) $s->dateBarrDebut < $year) {
				$year = (int) $s->dateBarrDebut;
			}
		}
		return $year;
	}

	/**
	 * @return ServicesByType
	 */
	private function sortServices(\DisplayServices $services): array
	{
		$sortedServices = [
			'Restreint' => [],
			'Abonné' => [],
			'Libre' => [],
		];
		foreach ($services->filterServices() as $s) {
			assert($s instanceof \Service);
			if ($s->type !== \Service::TYPE_INTEGRAL || $s->selection) {
				continue;
			}
			$access = $services->getServiceAccess($s);
			if (isset($sortedServices[$access])) {
				$attributes = (object) $s->getAttributes();
				$attributes->ressource = $s->ressource->sigle ?: $s->ressource->nom;
				$proxyUrl = $s->getProxies($this->partenaire, $services);
				$attributes->url = $s->getSelfUrl($proxyUrl);
				array_unshift($sortedServices[$access], $attributes);
			}
		}

		foreach ($sortedServices as &$tosort) {
			usort($tosort, function ($a, $b) { return strcmp($b->dateBarrDebut, $a->dateBarrDebut); });
		}
		return $sortedServices;
	}

	/**
	 * @param ServicesByType $sortedServices
	 */
	private function svgBar(int $firstYear, int $lastYear, array $sortedServices): string
	{
		if ($lastYear > 0) {
			$yearSpanStopped = (int) date('Y') - $lastYear - 1;
			if ($yearSpanStopped <= 0) {
				$yearSpanStopped = 1;
			}
			$yearsSpanTotal = 1 + $lastYear - $firstYear;
			$lastYearPublished = $lastYear;
		} else {
			// $lastYear === 0 means it's still alive.
			$yearSpanStopped = 0;
			$yearsSpanTotal = 1 + (int) date('Y') - $firstYear;
			$lastYearPublished = (int) date('Y');
		}
		if ($yearsSpanTotal < 1) {
			// Should never happen if the dates are valid.
			return '';
		}

		/*
		 * The SVG takes the full width of the container element,
		 * but it declares local coordinates (viewBox) so that elements contained in the SVG
		 * work in a constant width, where the body is 1000 "pixels" wide.
		 * The browser scales the local coordinates.
		 */
		$topSpace = 14;
		$size = [
			'all' => [
				'height' => self::HEIGHT,
				'width' => // inner width (real width is 100% of available space)
					1000 // body width
					+ (self::HEIGHT + 12) // start-year width
					+ (int) round(3.0 * self::ARROW_THICKNESS), // arrow-head width
			],
			'start' => [
				'height' => self::HEIGHT,
				'width' => self::HEIGHT + 12,
			],
			'body' => [
				'height' => self::HEIGHT,
				'width' => 1000,
				'x' => self::HEIGHT + 12, // start.width
				'y' => 0,
			],
			'body-access' => [
				'height' => (int) (self::HEIGHT - $topSpace - 2 * self::ARROW_THICKNESS),
				'y' => $topSpace,
			],
			'body-arrow' => [
				'height' => self::ARROW_THICKNESS,
				'width' => "100%",
				'x' => 0,
				'y' => self::HEIGHT - 2 * self::ARROW_THICKNESS,
			],
			'arrowhead' => [
				'height' => (int) (3 * self::ARROW_THICKNESS),
				'width' => (int) (3 * self::ARROW_THICKNESS),
				'x' => 1000 + (self::HEIGHT + 8), // start.width + body.width
				'y' => self::HEIGHT - 3 * self::ARROW_THICKNESS,
			],
		];
		$arrowColor = self::ARROW_COLOR;

		$globalInfo = htmlspecialchars(json_encode(self::extractServicesInfo($sortedServices)));
		$uniq = substr(uniqid(), -8);
		$svg = <<<EOSVG
			<svg class="frise" id="frise-{$uniq}" width="100%" height="{$size['all']['height']}" viewBox="0 0 {$size['all']['width']} {$size['all']['height']}" data-services="$globalInfo" data-revue-start="{$firstYear}" data-revue-end="{$lastYearPublished}">
			  <style>
			  svg text {
			    font-size: 16px;
			    text-anchor: middle;
			    dominant-baseline: central;
			  }
			  svg .frise-start text, svg .frise-end text {
				font-weight: normal;
			  }
			  </style>
			  <defs>
			    <pattern id="lacunaire-Restreint" x="0" y="{{BODY_ACCESS_Y}}" width="{{BODY_ACCESS_H}}" height="{{BODY_ACCESS_H}}" patternContentUnits="userSpaceOnUse" patternUnits="userSpaceOnUse">
				  <rect x="0" y="0" width="{{BODY_ACCESS_H}}" height="{{BODY_ACCESS_H}}" fill="{{COLOR_RESTREINT}}" stroke-width="0" />
			      <circle cx="6" cy="6" r="3" fill="white" stroke-width="0" opacity="0.6" />
			    </pattern>
			    <pattern id="lacunaire-Abonné" x="0" y="{{BODY_ACCESS_Y}}" width="{{BODY_ACCESS_H}}" height="{{BODY_ACCESS_H}}" patternContentUnits="userSpaceOnUse" patternUnits="userSpaceOnUse">
				  <rect x="0" y="0" width="{{BODY_ACCESS_H}}" height="{{BODY_ACCESS_H}}" fill="{{COLOR_ABONNE}}" stroke-width="0" />
			      <circle cx="6" cy="6" r="3" fill="white" stroke-width="0" opacity="0.6" />
			    </pattern>
			    <pattern id="lacunaire-Libre" x="0" y="{{BODY_ACCESS_Y}}" width="{{BODY_ACCESS_H}}" height="{{BODY_ACCESS_H}}" patternContentUnits="userSpaceOnUse" patternUnits="userSpaceOnUse">
				  <rect x="0" y="0" width="{{BODY_ACCESS_H}}" height="{{BODY_ACCESS_H}}" fill="{{COLOR_LIBRE}}" stroke-width="0" />
			      <circle cx="6" cy="6" r="3" fill="white" stroke-width="0" opacity="0.6" />
			    </pattern>
			  </defs>
			  <svg class="frise-start" height="{$size['start']['height']}" width="{$size['start']['width']}" x="0" preserveAspectRatio="xMinYMin slice">
			    <line x1="100%" y1="0" x2="100%" y2="100%" stroke-width="2" stroke="$arrowColor" pointer-events="none" />
			    <text x="50%" y="45%" pointer-events="none">{$firstYear}</text>
			  </svg>
			  <svg class="frise-body" height="{$size['body']['height']}" width="{$size['body']['width']}" x="{$size['body']['x']}" y="{$size['body']['y']}">
			EOSVG;

		$widthAlive = $size['body']['width'];
		if ($lastYear > 0) {
			$yearsSpanToNow = (int) date('Y') - $firstYear;
			$widthDead = (int) round($size['body']['width'] * $yearSpanStopped / $yearsSpanToNow);
			if ($widthDead < 1) {
				$widthDead = 1;
			}
			$widthAlive -= $widthDead;
			$svg .= <<<EOSVG
				<rect class="arrow-body" width="{$widthAlive}" height="{$size['body-arrow']['height']}" y="{$size['body-arrow']['y']}" fill="$arrowColor" stroke-width="1" stroke="$arrowColor" />
				<svg class="frise-endyear" height="100%" width="{$size['start']['width']}" x="{$widthAlive}" y="0" preserveAspectRatio="xMinYMin slice">
				  <line x1="0" y1="0" x2="0" y2="100%" fill="black" stroke-width="2" stroke="$arrowColor" fill="$arrowColor" pointer-events="none" />
				  <text x="50%" y="45%" pointer-events="none">{$lastYear}</text>
				</svg>
				EOSVG;
		} else {
			$svg .= <<<EOSVG
				<rect class="arrow-body" width="{$size['body-arrow']['width']}" height="{$size['body-arrow']['height']}" y="{$size['body-arrow']['y']}" fill="$arrowColor" stroke-width="1" stroke="$arrowColor" />
				EOSVG;
		}

		// body-access
		$svg .= '<g class="group-service-bars" stroke-width="0" stroke="white" vector-effect="fixed-position" pointer-events="visiblePainted">';
		foreach (self::COLORS as $access => $color) {
			foreach ($sortedServices[$access] as $s) {
				// $s has the same properties as \Service
				$times = self::extractTimes($s, $firstYear, $lastYearPublished);
				$svg .= sprintf(
					'<rect x="%d" width="%d" height="%d" y="%d" fill="%s" />',
					round($widthAlive * ($times['start'] - $firstYear) / $yearsSpanTotal), // x
					round($widthAlive * $times['duration'] / $yearsSpanTotal), // width
					$size['body-access']['height'],
					$size['body-access']['y'],
					$s->lacunaire ? "url(#lacunaire-$access)" : $color
				);
			}
		}
		$svg .= <<<EOSVG
			  </g>
			</svg>
			EOSVG; // end of .frise-body

		// arrowhead
		if (!$lastYear) {
			$h = $size['arrowhead']['height'];
			$h2 = (int) round($h / 2);
			$svg .= <<<EOSVG
			  <svg class="frise-arrowhead" height="{$size['arrowhead']['height']}" width="{$size['arrowhead']['width']}" x="{$size['arrowhead']['x']}" y="{$size['arrowhead']['y']}" preserveAspectRatio="xMinYMin slice">
			    <polygon points="0, 0 {$size['arrowhead']['width']}, $h2 0, $h" fill="$arrowColor" />
			  </svg>
			EOSVG;
		}
		$svg .= "</svg>";
		$interpolate = [
			'{{BODY_ACCESS_H}}' => (string) $size['body-access']['height'],
			'{{BODY_ACCESS_Y}}' => (string) $size['body-access']['y'],
			'{{COLOR_RESTREINT}}' => self::COLORS['Restreint'],
			'{{COLOR_ABONNE}}' => self::COLORS['Abonné'],
			'{{COLOR_LIBRE}}' => self::COLORS['Libre'],
		];
		return str_replace(array_keys($interpolate), array_values($interpolate), $svg);
	}

	/**
	 * @return array{"start": int, "end": int, "duration": int} In years.
	 */
	private static function extractTimes(object $s, int $firstYear, int $lastYear): array
	{
		$start = (int) $s->dateBarrDebut;
		if (!$start) {
			$start = $firstYear;
		}
		$end = (int) $s->dateBarrFin;
		if (!$end) {
			$end = $lastYear;
		}
		return [
			'start' => $start,
			'end' => $end,
			'duration' => 1 + $end - $start,
		];
	}

	/**
	 * @param ServicesByType $servicesByType
	 * @return list<array{"start": string, "end": string, "lacunaire": bool, "name": string, "type": string, "url": string}>
	 */
	private static function extractServicesInfo(array $servicesByType): array
	{
		$info = [];
		foreach ($servicesByType as $type => $services) {
			foreach ($services as $s) {
				// $s has the same properties as \Service, plus "ressource"
				$info[] = [
					'start' => $s->dateBarrDebut,
					'end' => $s->dateBarrFin,
					'lacunaire' => (bool) $s->lacunaire,
					'name' => $s->ressource,
					'type' => $type,
					'url' => $s->url,
				];
			}
		}
		return $info;
	}
}
