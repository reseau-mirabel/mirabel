<?php

class m200527_061907_m4444_issn_pays extends CDbMigration
{
	public function safeUp()
	{
		$this->addColumn("Issn", "pays", "CHAR(2) COLLATE ascii_bin DEFAULT NULL COMMENT 'ISO 3166-1 (2 letters)' AFTER dateFin");
		return true;
	}

	public function safeDown()
	{
		$this->dropColumn("Issn", "pays");
		return true;
	}
}
