<?php

class m220202_141521_m5126_cms_markdown extends \CDbMigration
{
	public function safeUp()
	{
		$this->execute("UPDATE Cms SET `type` = 'markdown' WHERE `name` = 'accueil-video'");

		$query = $this->dbConnection->createCommand("SELECT id, content FROM Cms WHERE `type` = 'markdown'");
		$update = $this->dbConnection->createCommand("UPDATE Cms SET content = :content WHERE id = :id");
		foreach ($query->queryAll() as $row) {
			$content = preg_replace(
				'/^(#+)([^\s#])/m',
				'$1 $2',
				$row['content']
			);
			if ($content !== $row['content']) {
				$update->execute([':content' => $content, ':id' => $row['id']]);
			}
		}

		return true;
	}

	public function safeDown()
	{
		return true;
	}
}
