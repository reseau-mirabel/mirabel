<?php

class m230529_043808_m5107_sourcelien_domainregex extends \CDbMigration
{
	public function up(): bool
	{
		$this->addColumn("Sourcelien", "urlRegex", "VARCHAR(4095) DEFAULT '' NOT NULL COMMENT 'validation url par regexp' AFTER `url`");
		// Tableau dérivé de :
		// echo 'SELECT CONCAT("[", sourceId, ", '\'^'", REPLACE(GROUP_CONCAT(DISTINCT REGEXP_REPLACE(url, "^https?://([^/]+)/.*$", "\\\\1") SEPARATOR "|"), ".", "\\\\."), "$'\''],") FROM LienTitre WHERE sourceId IS NOT NULL AND url LIKE "http%" GROUP BY sourceId;' | mysql mirabel2
		$values = [
			[1, '^https://doaj\\.org/toc/\d{4}-\d{3}[\dxX]'],
			[2, '^https?://([^/]+\\.)?facebook\\.com/[\w./%-]+$'],
			[4, '^https?://[^/]*instagram\\.com/[\w.-]+/?'],
			[5, '^https://([^/]+\\.)?linkedin\\.com/[\w./%\\\'-]+$'],
			[6, '^https://[^/]+\\.pinterest\\.(com|[a-z]{2})/[\w.-]+/$'],
			[7, '\\.tumblr\\.com/'],
			[8, '^https?://(?:www\\.)?twitter\\.com/[\w_@-]+$'],
			[9, '^https://vimeo\\.com/[\w_-]+$'],
			[10, '^https?://(?:www\.)?youtube\\.com'],
			[13, '^https?://journalbase\\.cnrs\\.fr/index.php\?op=\d+&id=\d+$'],
			[14, '^https?://road.issn.org/issn/\d{4}-\d{3}[\dxX]$'],
			[15, '(fr\\.viadeo\\.com|viadeo\\.journaldunet\\.com)/'],
			[16, '(jurisguide\\.fr|jurisguide\\.univ-paris1\\.fr)/'],
			[17, '^https://fr\\.wikipedia\\.org/'],
			[18, '^https://www\\.latindex\\.org/'],
			[19, '^https://([^/]+\\.archives-ouvertes\\.fr|hal\\.science)/'],
			[20, '^https://www\\.scoop\\.it/'],
			[21, '^https://kanalregister\\.hkdir\\.no/'],
			[22, '^https://www\\.flickr\\.com/'],
			[23, '^https://[^/]+\\.hypotheses\\.org'],
			[24, '^https://isidore\\.science'],
			[26, '^https?://[^/]*miar\\.ub\\.edu'],
			[27, '^https://www\\.cairn-int\\.info/'],
			[29, '^https://www\\.entrevues\\.org/'],
			[30, '^https://www\\.scopus\\.com/'],
			[31, '^https://v2\\.sherpa\\.ac\\.uk/'],
			[32, '^https://www\\.researchgate\\.net/'],
			[33, '^https://ar\\.wikipedia\\.org/wiki/'],
			[34, '^https://ca\\.wikipedia\\.org/wiki/'],
			[35, '^https://cs\\.wikipedia\\.org/wiki/'],
			[36, '^https://de\\.wikipedia\\.org/wiki/'],
			[37, '^https://el\\.wikipedia\\.org/wiki/'],
			[38, '^https://en\\.wikipedia\\.org/wiki/'],
			[39, '^https://es\\.wikipedia\\.org/wiki/'],
			[40, '^https://et\\.wikipedia\\.org/wiki/'],
			[41, '^https://eu\\.wikipedia\\.org/wiki/'],
			[42, '^https://gl\\.wikipedia\\.org/wiki/'],
			[43, '^https://he\\.wikipedia\\.org/wiki/'],
			[44, '^https://hu\\.wikipedia\\.org/wiki/'],
			[45, '^https://it\\.wikipedia\\.org/wiki/'],
			[46, '^https://nl\\.wikipedia\\.org/wiki/'],
			[47, '^https://no\\.wikipedia\\.org/wiki/'],
			[48, '^https://pl\\.wikipedia\\.org/wiki/'],
			[49, '^https://pt\\.wikipedia\\.org/wiki/'],
			[50, '^https://ro\\.wikipedia\\.org/wiki/'],
			[51, '^https://ru\\.wikipedia\\.org/wiki/'],
			[52, '^https://wa\\.wikipedia\\.org/wiki/'],
			[53, '^https://zh\\.wikipedia\\.org/wiki/'],
			[54, '^https://sh\\.wikipedia\\.org/wiki/'],
			[55, '^https://da\\.wikipedia\\.org/wiki/'],
			[56, '^https://flipboard\\.com/'],
			[57, '^https://sv\\.wikipedia\\.org/wiki/'],
			[58, '^https://uk\\.wikipedia\\.org/wiki/'],
			[59, '^https://sr\\.wikipedia\\.org/wiki/'],
			[60, '^https://hr\\.wikipedia\\.org/wiki/'],
			[61, '^https://tr\\.wikipedia\\.org/wiki/'],
			[62, '^https://search\\.crossref\\.org/'],
			[63, '^https://explore\\.openalex\\.org/'],
			[64, '^https://arxiv\\.org/'],
			[65, 'https://[^/]*(uni-regensburg|ezb\\.ur)\\.de/'],
			[66, '^https://www\\.cairn-mundo\\.info/'],
			[67, '^https://www\\.scimagojr\\.com/'],
			[68, 'https://[^/]+\\.clarivate\\.com/'],
		];
		$transaction = $this->dbConnection->beginTransaction();
		foreach ($values as [$id, $v]) {
			$this->execute("UPDATE Sourcelien SET urlRegex = :v WHERE id = $id", [':v' => $v]);
		}
		$transaction->commit();
		return true;
	}

	public function down(): bool
	{
		$this->dropColumn("Sourcelien", "urlRegex");
		return true;
	}
}
