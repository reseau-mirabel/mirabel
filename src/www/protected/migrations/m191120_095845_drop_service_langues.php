<?php

class m191120_095845_drop_service_langues extends CDbMigration
{
	/**
	 * Applies this migration.
	 */
	public function up()
	{
		$this->dropColumn("Service", "langues");
	}

	/**
	 * Reverts this migration.
	 *
	 * @return bool False if it failed.
	 */
	public function down()
	{
		echo "m191120_095845_drop_service_langues does not support migrating down.\n";
		return false;
	}
}
