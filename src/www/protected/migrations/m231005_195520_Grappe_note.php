<?php

class m231005_195520_Grappe_note extends \CDbMigration
{
	public function up(): bool
	{
		$this->addColumn('Grappe', 'note', "text NOT NULL DEFAULT '' comment 'note interne' AFTER description");
		return true;
	}

	public function down(): bool
	{
		$this->dropColumn('Grappe', 'note');
		return true;
	}
}
