<?php

class m211022_144520_m5037_sherpa_aide_choix_editeurs extends \CDbMigration
{
	public function up(): bool
	{
		if (Cms::getBlock("politique.creation-utilisateur.editeurs") !== null) {
			return true;
		}
		$this->insert("Cms", [
			'name' => "politique.creation-utilisateur.editeurs",
			'singlePage' => 0,
			'type' => 'html',
			'content' => "<!-- HTML affiché au début du bloc de sélection des éditeurs et des rôles, sur la page de création d'un compte d'utilisateur-politiques. -->
<p>Si vous ne trouvez pas l'éditeur dans Mir@bel, <a href=\"/site/contact\">contactez-nous</a>.</p>",
			'hdateCreat' => time(),
		]);
		return true;
	}

	public function down(): bool
	{
		$this->delete('Config', "`key` = 'sherpa.aide.choix-editeurs'");
		$this->delete('Cms', "`name` = 'politique.creation-utilisateur.editeurs'");
		return true;
	}
}
