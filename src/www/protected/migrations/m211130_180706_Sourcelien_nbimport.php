<?php

class m211130_180706_Sourcelien_nbimport extends \CDbMigration
{
	public function up(): bool
	{
		if (!$this->getDbConnection()->getSchema()->getTable("Sourcelien")->getColumn("nbimport")) {
			$this->addColumn(
				"Sourcelien",
				"nbimport",
				"int(10) unsigned NULL DEFAULT NULL COMMENT 'nombre de lignes dans les fichiers importés' AFTER `saisie` "
			);
		}
		return true;
	}

	public function down(): bool
	{
		$this->dropColumn("Sourcelien", "nbimport");
		return true;
	}
}
