<?php

class m221115_163807_m5433 extends \CDbMigration
{
	public function up(): bool
	{
		$this->insertMultiple(
			'Config', [
				[
					'createdOn' => time(),
					'category' => 'sherpa',
					'type' => 'html',
					'description' => "HTML affiché en haut de la page /titre/publication dans le cas où une politique est validée par Mir@bel, mais pas encore publiée par Sherpa.",
					'key' => 'sherpa.intro-politique-mirabel',
					'value' => <<<EOHTML
					<strong>Affichage provisoire</strong> car cette politique de publication est transmise à Sherpa Romeo, mais en attente de sa diffusion.
					EOHTML
				],
				[
					'createdOn' => time(),
					'category' => 'politique',
					'type' => 'boolean',
					'description' => "Si activé, <code>/titre/publication/</code> acceptera les titres dont la politique est validée par Mirabel, mais pas encore diffusée par Sherpa. Si désactivé, <code>/titre/publication/</code> ne traitera que les données de Sherpa."
						. " Un lien vers <code>/titre/publication<code> depuis la page de la revue et du titre sera présent en fonction de ce réglage.",
					'key' => 'politique.publication-si-validee',
					'value' => 1,
				],
			]);
		return true;
	}

	public function down(): bool
	{
		$this->delete('Config', "`key` IN('sherpa.intro-politique-mirabel', 'politique.publication-si-validee')");
		return true;
	}
}
