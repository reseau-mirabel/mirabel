<?php

class {ClassName} extends \CDbMigration
{
	public function up(): bool
	{
		// Use $this->insert() etc. for most operations,
		// but for complex operations you can resort to raw SQL with this.
		$sqls = [
			"",
		];
		foreach ($sqls as $sql) {
			$this->execute($sql);
		}
		return true;
	}

	public function down(): bool
	{
		echo "{ClassName} does not support migrating down.\n";
		return false;
	}

	// Instead of up/down, use safeUp/safeDown to do migration wrapped in a transaction.
	// Beware: MySQL transactions cannot alter the structure!
	// Delete this if you don't use it.
	/*
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
