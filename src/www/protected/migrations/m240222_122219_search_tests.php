<?php

class m240222_122219_search_tests extends \CDbMigration
{
	public function safeUp()
	{
		if (defined('YII_TARGET') && YII_TARGET === 'test') {
			$this->insert("Partenaire_Titre", [
				'partenaireId' => 1,
				'titreId' => 5,
				'identifiantLocal' => '',
				'bouquet' => '',
			]);
			$this->insert("Grappe", [
				'id' => 2,
				'nom' => "Atron",
				'description' => '<p>Alias Directeur général bien en chair &amp; même plus.</p>',
				'note' => "Commentaire désobligeant sur l'affiliation politique secrète.",
				'diffusion' => Grappe::DIFFUSION_PUBLIC,
				'niveau' => Grappe::NIVEAU_REVUE,
				'recherche' => '[{"accesLibre":1,"paysId":62}]',
				'partenaireId' => 2,
				'hdateCreation' => 1708606827,
				'hdateModif' => null,
			]);
			$this->insertMultiple('Grappe_Titre', [
				['grappeId' => 2, 'titreId' => 1, 'source' => 0],
				['grappeId' => 2, 'titreId' => 2, 'source' => 0],
				['grappeId' => 2, 'titreId' => 3, 'source' => 0],
				['grappeId' => 2, 'titreId' => 6, 'source' => 0],
				['grappeId' => 2, 'titreId' => 20, 'source' => 0],
				['grappeId' => 2, 'titreId' => 28, 'source' => 0],
				['grappeId' => 2, 'titreId' => 29, 'source' => 0],
				['grappeId' => 2, 'titreId' => 98, 'source' => 0],
				['grappeId' => 2, 'titreId' => 366, 'source' => 0],
				['grappeId' => 2, 'titreId' => 716, 'source' => 0],
				['grappeId' => 2, 'titreId' => 736, 'source' => 0],
				['grappeId' => 2, 'titreId' => 1105, 'source' => 0],
				['grappeId' => 2, 'titreId' => 1248, 'source' => 0],
			]);
		}
	}

	public function safeDown()
	{
		return true;
	}
}
