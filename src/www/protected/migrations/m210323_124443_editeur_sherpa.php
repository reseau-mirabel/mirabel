<?php

class m210323_124443_editeur_sherpa extends \CDbMigration
{
	public function up(): bool
	{
		$this->addColumn("Editeur", "sherpa", "INT UNSIGNED NULL DEFAULT NULL COMMENT 'sherpa publisher ID' AFTER idref");
		$this->insert(
			"Hint",
			[
				'model' => 'Editeur',
				'attribute' => 'sherpa',
				'name' => 'ID Sherpa',
				'description' => '',
				'hdateModif' => time(),
			]
		);
		return true;
	}

	public function down(): bool
	{
		$this->dropColumn("Editeur", "sherpa");
		$this->delete("Hint", "model = 'Editeur' AND attribute = 'sherpa'");
		return true;
	}
}
