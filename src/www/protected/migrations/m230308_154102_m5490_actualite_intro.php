<?php

class m230308_154102_m5490_actualite_intro extends \CDbMigration
{
	public function up(): bool
	{
		$this->insert('Cms', [
			'name' => 'actualite-intro',
			'singlePage' => 0,
			'pageTitle' => '',
			'type' => 'html',
			'content' => <<<'EOHTML'
				<!-- Ce bloc s'affiche sous le titre de la page /site/actualite. Si un filtre est présent, une phrase avec un lien s'affiche au-dessus. -->
				<p>
					Suivez également le réseau Mir@bel
					sur twitter <a href="https://twitter.com/mirabel_revues">@mirabel_revues <img src="/images/icone_twitter.png" width="14" height="14" /></a>
					et sur <a href="https://vimeo.com/mirabelrevues">vimeo</a>.
				</p>
				EOHTML,
			'hdateCreat' => time(),
		]);

		return true;
	}

	public function down(): bool
	{
		$this->delete("Cms", "name IN('actualite-intro')");
		return true;
	}
}
