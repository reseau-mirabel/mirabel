<?php

class m240403_065255_create_Etatcollection extends \CDbMigration
{
	public function up(): bool
	{
		if (!in_array('Etatcollection', $this->dbConnection->schema->tableNames)) {
			$this->createTable(
				"Etatcollection",
				[
					'issnId' => "INT UNSIGNED NOT NULL",
					'rcr' => "CHAR(10) NOT NULL COLLATE ascii_bin",
					'etats' => "JSON NOT NULL DEFAULT '[]' COMMENT 'liste des états de collection'",
					'lastImport' => "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP",
				]
			);
			$this->addForeignKey("etatcollection_issn_fk", "Etatcollection", "issnId", "Issn", "id", "CASCADE", "CASCADE");
			$this->addPrimaryKey('issnId_rcr_primary', 'Etatcollection', ["issnId", "rcr"]);

		}
		$this->addColumn(
			'Partenaire',
			'rcrHorsPossession',
			'TINYINT UNSIGNED NOT NULL DEFAULT 0 AFTER rcrPublic'
		);
		if (defined('YII_TARGET') && YII_TARGET === 'test') {
			$this->update(
				"Partenaire",
				[
					'rcrHorsPossession' => true,
					'rcrPublic' => true,
					'rcr' => json_encode(["012345678"]),
				],
				'id=2'
			);
			// Pour le partenaire #2,
			// insérer deux états de collection pour un titre de la revue #2 (en plus des possessions)
			// et un état pour la revue #20 (absente des possessions de ce partenaire).
			$sql = <<<SQL
				REPLACE INTO Etatcollection (issnId, rcr, etats)
				VALUES
					(2, '012345678', '["titre par sudoc-rcr avec possession"]'),
					(5700, '012345678', '["Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."]'),
					(1112, '012345678', '["titre par sudoc-rcr sans possession"]')
				SQL;
			$this->getDbConnection()->createCommand($sql)->execute();
		}
		return true;
	}

	public function down(): bool
	{
		$this->dropTable("Etatcollection");
		$this->dropColumn('Partenaire', 'rcrHorsPossession');
		return true;
	}
}
