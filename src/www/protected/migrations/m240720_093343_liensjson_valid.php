<?php

class m240720_093343_liensjson_valid extends \CDbMigration
{
	public function up(): bool
	{
		$this->execute("UPDATE Titre SET liensJson = '[]' WHERE liensJson = ''");
		$this->alterColumn("Titre", "liensJson", "JSON NOT NULL DEFAULT 'null'");
		$this->execute("UPDATE Editeur SET liensJson = '[]' WHERE liensJson = ''");
		$this->alterColumn("Editeur", "liensJson", "JSON NOT NULL DEFAULT 'null'");
		return true;
	}

	public function down(): bool
	{
		echo "m240720_093343_liensjson_valid does not support migrating down.\n";
		return false;
	}
}
