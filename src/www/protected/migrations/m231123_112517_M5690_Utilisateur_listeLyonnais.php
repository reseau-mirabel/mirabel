<?php

class m231123_112517_M5690_Utilisateur_listeLyonnais extends \CDbMigration
{
	public function up(): bool
	{
		$this->addColumn(
			'Utilisateur',
			'listeLyonnais',
			"tinyint(1) NOT NULL DEFAULT 0 COMMENT 'liste de diffusion mirabel-lyonnais'"
		);

		$sql = <<<ESQL
		UPDATE Utilisateur U
			JOIN Partenaire P ON (P.id=U.partenaireId)
		SET U.listeLyonnais=1
		WHERE U.actif=1 AND P.geo IN ('Lyon', 'Vaulx-en-Velin', 'Vénissieux', 'Villeurbanne');
		ESQL;
		$this->execute($sql);
		return true;
	}

	public function down(): bool
	{
		$this->dropColumn('Utilisateur', 'listeLyonnais');
		return true;
	}
}
