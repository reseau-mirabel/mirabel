<?php

class m211129_075624_m4979_politique_moderation_aide extends \CDbMigration
{
	public function up(): bool
	{
		$this->insert("Cms", [
			'name' => 'politique-moderation',
			'singlePage' => 0,
			'pageTitle' => '',
			'type' => 'html',
			'content' =>
				<<<EOHTML
				<!-- Texte d'aide à la recherche sur la page de modérations des politiques. Le bloc est replié par défaut. -->
				<div>
				<ul>
					<li>
						Utiliser la liste déroulante pour filtrer les statuts :
						<ul>
							<li><code>pending</code> politique créée par un utilisateur non validé</li>
							<li><code>draft</code> politique en cours de déclaration</li>
							<li><code>topublish</code> transmission à Sherpa Romeo demandée par l'éditeur, en attente de modération</li>
							<li><code>published</code> politique validée par un modérateur et transmise à Sherpa Romeo, visible dans l'API</li>
							<li><code>updated</code> politique modifiée après avoir été diffusée, n'est plus visible dans l'API</li>
							<li><code>todelete</code> politique à supprimer</li>
						</ul>
					Il est possible de combiner les valeurs avec une saisie manuelle, par exemple <code>topublish,published</code>.
					</li>
					<li>
						Exemples de filtre par date :
						<ul>
							<li><code>2021-10-18</code> date exacte</li>
							<li><code>2021-10</code> toutes les dates du mois d'octobre 2021</li>
							<li><code>&gt; 2021-09</code> toutes les dates plus récentes que septembre 2021</li>
							<li><code>&lt; 2021-10-18</code> toutes les dates plus anciennes que le 18 octobre 2021</li>
						</ul>
					</li>
				</ul>
				</div>
				EOHTML,
			'private' => 0,
			'hdateCreat' => time(),
			'hdateModif' => 0,
		]);
		return true;
	}

	public function down(): bool
	{
		$this->delete('Cms', "name = 'politique-moderation'");
		return true;
	}
}
