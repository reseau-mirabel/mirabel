<?php

class m230508_061702_m5508_editeur_dates extends \CDbMigration
{
	public function up(): bool
	{
		$this->addColumn("Editeur", "dateDebut", "VARCHAR(10) NOT NULL COLLATE ascii_bin DEFAULT '' AFTER sigle");
		$this->addColumn("Editeur", "dateFin", "VARCHAR(10) NOT NULL COLLATE ascii_bin DEFAULT '' AFTER dateDebut");
		$this->execute("UPDATE Editeur SET idref = '026485842' WHERE id = 19");
		return true;
	}

	public function down(): bool
	{
		$this->dropColumn("Editeur", "dateDebut");
		$this->dropColumn("Editeur", "dateFin");
		return true;
	}
}
