<?php

class m250218_131547_partenaire_description_presentation extends \CDbMigration
{
	public function safeUp(): bool
	{
		$partenaires = Partenaire::model()->findAll("description <> '' AND presentation = '' AND editeurId IS NOT NULL");
		foreach ($partenaires as $p) {
			printf("%4d %20s %s\n", $p->id, $p->sigle, $p->nom);
			$html = CHtml::encode(trim($p->description));
			$p->description = "";
			$p->presentation = nl2br("<p>" . str_replace("\n\n", "</p><p>", $html) . "</p>");
			$p->save(false);
		}
		return true;
	}

	public function safeDown(): bool
	{
		echo "m250218_131547_partenaire_description_presentation does not support migrating down.\n";
		return false;
	}
}
