<?php

class m210921_070954_m4847_hint_editeursearch extends \CDbMigration
{
	public function up(): bool
	{
		$this->execute(<<<'EOSQL'
			UPDATE Hint SET model = 'models\\searches\\EditeurSearch' WHERE model = 'editeur/search'
			EOSQL
		);
		return true;
	}

	public function down(): bool
	{
		$this->execute(<<<'EOSQL'
			UPDATE Hint SET model = 'editeur/search' WHERE model = 'models\\searches\\EditeurSearch'
			EOSQL
		);
		return true;
	}
}
