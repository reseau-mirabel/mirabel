<?php

class m220125_110005_m5108_politique_api_jetons extends \CDbMigration
{
	public function up(): bool
	{
		if ($this->getDbConnection()->getSchema()->getTable("PolitiqueJeton")) {
			return true;
		}
		$this->createTable(
			"PolitiqueJeton",
			[
				'id' => "INT unsigned NOT NULL PRIMARY KEY AUTO_INCREMENT",
				'name' => "VARCHAR(255) COLLATE utf8mb4_bin NOT NULL",
				'token' => "VARCHAR(32) COLLATE ascii_bin NOT NULL",
				'lastUpdate' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
			],
			"COMMENT 'Jetons pour accéder à /politique/api'"
		);
		return true;
	}

	public function down(): bool
	{
		$this->dropTable("PolitiqueJeton");
		return true;
	}
}
