<?php

class m210616_142528_m4904_politique_seuil extends \CDbMigration
{
	public function safeUp()
	{
		$this->insert('Config', [
			'key' => 'politique.seuil-affichage',
			'value' => 5,
			'type' => 'integer',
			'category' => 'politique',
			'description' => "Sur la page /politique, si l'éditeur sélectionné a strictement moins de X titres en cours d'édition (titre vivant et non éditeur précédent),"
				. " alors on n'affiche pas le bloc politique mais uniquement le bloc titre.",
			'required' => 0,
			'createdOn' => time(),
			'updatedOn' => null,
		]);
		return true;
	}

	public function safeDown()
	{
		$this->delete('Config', "`key` = 'politique.seuil-affichage'");
		return true;
	}
}
