<?php

class m210504_065024_politique_listes extends \CDbMigration
{
	public function safeUp()
	{
		$this->delete("Config", "`key` IN ('prerequisites', 'public_notes')");
		$now = time();
		$this->insertMultiple('Config', [
			[
				'createdOn' => $now,
				'category' => 'politique',
				'type' => 'csv',
				'description' => "CSV à deux ou trois colonnes, avec sur chaque ligne :<pre>valeur Sherpa;texte français;aide</pre>"
					. " Utilisé comme liste de valeurs possibles dans le mode expert de modification d'une politique."
					. " Les guillemets ne sont nécessaires que si la cellule contient ';'.",
				'key' => 'politique.liste.article_version',
				'value' =>
					<<<EOVALUE
					published;publiée;
					accepted;acceptée;
					submitted;soumise;
					EOVALUE
			],
			[
				'createdOn' => $now,
				'category' => 'politique',
				'type' => 'csv',
				'description' => "CSV à deux ou trois colonnes, avec sur chaque ligne :<pre>valeur Sherpa;texte français;aide</pre>"
					. " Utilisé comme liste de valeurs possibles dans le mode expert de modification d'une politique."
					. " Les guillemets ne sont nécessaires que si la cellule contient ';'.",
				'key' => 'politique.liste.conditions',
				'value' =>
					<<<EOVALUE
					"Must link to publisher version";"Un lien vers la version publiée doit apparaitre"
					"Must link to publisher version with DOI";"Le lien DOI vers la version publiée doit apparaitre"
					"Published source must be acknowledged";"La source originale doit être mentionnée"
					"Published source must be acknowledged with citation";"La version publiée doit être citée avec la source originale"
					"Must acknowledge published source with citation"; "La version publiée doit être citée avec la source originale"
					"Publisher copyright and source must be acknowledged with citation";"Les droits d'éditeur et la source originale doivent apparaitre"
					"Set statement to accompany deposit";"Le détenteur des droits doit apparaitre"
					"Written permission must be obtained from the publisher";"Une autorisation écrite exprès doit être obtenue auprès de l'éditeur"
					"Post-prints are subject to Springer Nature re-use terms";"La version acceptée est soumise aux conditions de réutilisation de Springer Nature"
					"Set statements to accompany deposit (see policy)";"Etablir les déclarations qui doivent accompagner le dépôt (voir politique)"
					"Set statement to accompany deposit (see policy)";"Etablir une déclaration qui doit accompagner le dépôt (voir politique)"
					"The publisher will deposit in on behalf of authors to a designated institutional repository, where a deposit agreement exists with the repository";"L'éditeur déposera au nom des auteurs dans un dépôt institutionnel désigné, lorsqu'un accord de dépôt existe avec le dépôt"
					"Must link to published article";"Lien obligatoire vers l’article publié"
					"Must link to publisher version";"Lien obligatoire vers la version de l’éditeur"
					EOVALUE
			],
			[
				'createdOn' => $now,
				'category' => 'politique',
				'type' => 'csv',
				'description' => "CSV à deux ou trois colonnes, avec sur chaque ligne :<pre>valeur Sherpa;texte français;aide</pre>"
					. " Utilisé comme liste de valeurs possibles dans le mode expert de modification d'une politique."
					. " Les guillemets ne sont nécessaires que si la cellule contient ';'.",
				'key' => 'politique.liste.copyright_owner',
				'value' =>
					<<<EOVALUE
					"authors";"Auteur"
					"journal";"Revue"
					"learned_society";"Société savante"
					"publishers";"Éditeur"
					"shared_authors_and_journal";"Partagés entre l'auteur et la revue"
					"shared_authors_and_learned_society";"Partagés entre l'auteur et la société savante"
					"shared_authors_and_publishers";"Partagés entre l'auteur et l'éditeur"
					EOVALUE
			],
			[
				'createdOn' => $now,
				'category' => 'politique',
				'type' => 'csv',
				'description' => "CSV à deux ou trois colonnes, avec sur chaque ligne :<pre>valeur Sherpa;texte français;aide</pre>"
					. " Utilisé comme liste de valeurs possibles dans le mode expert de modification d'une politique."
					. " Les guillemets ne sont nécessaires que si la cellule contient ';'.",
				'key' => 'politique.liste.license',
				'value' =>
					<<<EOVALUE
					cc_by;CC BY;
					cc_by_nc;CC BY NC;
					cc_by_nc_nd;CC BY NC ND;
					cc_by_nc_sa;CC BY NC SA;
					cc_by_sa;CC BY SA;
					cc_by_nd;CC BY ND;
					EOVALUE
			],
			[
				'createdOn' => $now,
				'category' => 'politique',
				'type' => 'csv',
				'description' => "CSV à deux ou trois colonnes, avec sur chaque ligne :<pre>valeur Sherpa;texte français;aide</pre>"
					. " Utilisé comme liste de valeurs possibles dans le mode expert de modification d'une politique."
					. " Les guillemets ne sont nécessaires que si la cellule contient ';'.",
				'key' => 'politique.liste.location',
				'value' =>
					<<<EOVALUE
					"academic_social_network";"Réseau social académique"
					"any_website";"Tout site web"
					"any_repository";"Toute archive"
					"authors_homepage";"Page personnelle de l'auteur"
					"funder_designated_location";"Dépôt désigné par le financeur"
					"institutional_repository";"Archive institutionnelle"
					"institutional_website";"Site web institutionnel"
					"named_academic_social_network";"Réseau social académique spécifique"
					"named_repository";"Archive spécifique"
					"non_commercial_social_network";"Réseau social non commercial"
					"non_commercial_website";"Site web non commercial"
					"non_commercial_institutional_repository";"Archive institutionnelle non commerciale"
					"non_commercial_repository";"Archive non commerciale "
					"non_commercial_subject_repository";"Archive thématique non commerciale"
					"preprint_repository";"Archive d'articles prépubliés"
					"subject_repository";"Archive thématique"
					"this_journal";"Accès ouvert sur le site de l'éditeur"
					EOVALUE
			],
			[
				'createdOn' => $now,
				'category' => 'politique',
				'type' => 'csv',
				'description' => "CSV de 1 à 3 colonnes, avec sur chaque ligne :<pre>valeur Sherpa;texte français;aide</pre>"
					. " Utilisé comme liste de valeurs possibles dans le mode expert de modification d'une politique."
					. " Les guillemets ne sont nécessaires que si la cellule contient ';'.",
				'key' => 'politique.liste.location.named_repository',
				'value' =>
					<<<EOVALUE
					arXiv;
					ESRC Research Catalogue;
					Europe PubMed Central;
					HAL;
					PubMed Central;
					RePEC;
					EOVALUE
			],
			[
				'createdOn' => $now,
				'category' => 'politique',
				'type' => 'csv',
				'description' => "CSV à deux ou trois colonnes, avec sur chaque ligne :<pre>valeur Sherpa;texte français;aide</pre>"
					. " Utilisé comme liste de valeurs possibles dans le mode expert de modification d'une politique."
					. " Les guillemets ne sont nécessaires que si la cellule contient ';'.",
				'key' => 'politique.liste.prerequisites',
				'value' =>
					<<<EOVALUE
					requires_publisher_permission;après autorisation de l'éditeur;
					when_required_by_funder;si imposé par le financeur;
					when_required_by_institution;si imposé par l'institution;
					when_required_by_law;si imposé par la loi;
					EOVALUE
			],
			[
				'createdOn' => $now,
				'category' => 'politique',
				'type' => 'csv',
				'description' => "CSV à deux ou trois colonnes, avec sur chaque ligne :<pre>valeur Sherpa;texte français;aide</pre>"
					. " Utilisé comme liste de valeurs possibles dans le mode expert de modification d'une politique. Plus de 70 valeurs distinctes dans Sherpa."
					. " Les guillemets ne sont nécessaires que si la cellule contient ';'.",
				'key' => 'politique.liste.public_notes',
				'value' =>
					<<<EOVALUE
					This pathway is strictly in accordance with Article 30 of the Law for a Digital Republic of October 7, 2016.;Ce processus respecte la loi pour une République numérique du 7 octobre 2016, article 30.;
					This pathway allows for a non-embargoed deposit strictly in locations and under the conditions indicated.;Ce processus autorise un dépôt sans-embargo uniquement dans le respect des lieux et emplacements précisés.;
					Authors can shar their accepted manuscript immediately by updating a preprint in arXiv or RePEc with the accepted manuscript;Les auteurs... (typo×90 dans la version anglaise);
					Deposit of submitted version is discouraged, but is permitted;Le dépôt de la version soumise est découragé, mais permis;
					Authors are encouraged to submit their published articles to institutional repositories;Les auteurs sont incités à déposer leur article publié dans les dépôts institutionnels;
					For Science, Technical and Medicine titles;Pour les titres de science, technique ou médecine;
					For Humanities and Social Science titles;Pour les titres de sciences humaines;
					Must acknowledge published source with citation;La version publiée doit être citée;
					Upon official publication;Lors de la publication officielle;
					Anytime;À tout moment
					EOVALUE
			],
		]);

		return true;
	}

	public function safeDown()
	{
		$this->delete("Config", "`category` = 'politique'");
		return true;
	}
}
