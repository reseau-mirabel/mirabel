<?php

class m220525_055001_m5289_partenaire_noteprivee extends \CDbMigration
{
	public function up(): bool
	{
		$this->addColumn("Partenaire", "notes", "TEXT NOT NULL DEFAULT '' COMMENT 'hidden note written by admins' AFTER prefixe");
		return true;
	}

	public function down(): bool
	{
		$this->dropColumn("Partenaire", "notes");
		return true;
	}
}
