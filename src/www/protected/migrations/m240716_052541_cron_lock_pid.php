<?php

class m240716_052541_cron_lock_pid extends \CDbMigration
{
	public function up(): bool
	{
		$this->addColumn("CronLock", "pid", "BIGINT unsigned NOT NULL DEFAULT 0 comment 'process id'");
		return true;
	}

	public function down(): bool
	{
		$this->dropColumn("CronLock", "pid");
		return true;
	}
}
