<?php

class m210414_073735_config_utilisateur_editeur extends \CDbMigration
{
	public function safeUp()
	{
		$this->delete('Config', "`key` = 'sherpa-romeo.link'");
		$this->insertMultiple(
			'Config',
			[
				[
					'key' => 'password.politique.mailOnCreate.body',
					'value' => "Bienvenue sur Mir@bel.

Voici le mot de passe pour vous connecter à Mir@bel :
	%PASSWORD%

Après connexion sur %URL%/site/login
vous pourrez définir la politique des éditeurs.

Si vous n'êtes pas à l'origine de cette demande de compte web,
vous pouvez simplement l'ignorer.

Une fois votre compte approuvé par l'équipe de Mir@bel,
...
",
					'type' => 'text',
					'category' => 'utilisateur-editeur',
					'description' => "<p>Contenu du courriel envoyé lorsqu'un visiteur anonyme se crée un compte d'utilisateur-politiques, en charge de déclarer la politique d'éditeurs.</p>
<p>Les éléments suivants seront remplacés :
  <em>%LOGIN%</em> (=email), <em>%NOMCOMPLET%</em> (si vide, utilise le nom), <em>%PASSWORD%</em>, <em>%URL%</em> (sans / final).</p>
<p>Les entêtes du message font aussi appel aux paramètres <code>email.from</code> et <code>email.replyTo</code>,
  et, comme tous les envois, au <code>email.returnPath</code> pour le rebond en cas d'erreur.</p>
",
					'required' => 1,
					'createdOn' => time(),
				],
				[
					'key' => 'password.politique.mailOnCreate.subject',
					'value' => "Bienvenue sur Mir@bel…",
					'type' => 'string',
					'category' => 'utilisateur-editeur',
					'description' => "Sujet du courriel envoyé lorsqu'un visiteur anonyme se crée un compte d'utilisateur-politiques.",
					'required' => 1,
					'createdOn' => time(),
				],
			]
		);
		return true;
	}

	public function safeDown()
	{
		$this->delete('Config', "`key` LIKE 'password.politique.mailOnCreate.%'");
		return true;
	}
}
