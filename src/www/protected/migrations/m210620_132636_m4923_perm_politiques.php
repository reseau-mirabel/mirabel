<?php

class m210620_132636_m4923_perm_politiques extends \CDbMigration
{
	public function up(): bool
	{
		if ($this->dbConnection->schema->getTable('Utilisateur')->getColumn('permPolitiques') === null) {
			$this->addColumn(
				'Utilisateur',
				'permPolitiques',
				"BOOL NOT NULL DEFAULT 0 COMMENT 'permission de valider des utilisateur-politiques et des politiques' AFTER permRedaction"
			);
			$this->execute("UPDATE Utilisateur SET permPolitiques = 1 WHERE suiviPartenairesEditeurs = 1");
		}
		return true;
	}

	public function down(): bool
	{
		$this->dropColumn('Utilisateur', 'permPolitiques');
		return true;
	}
}
