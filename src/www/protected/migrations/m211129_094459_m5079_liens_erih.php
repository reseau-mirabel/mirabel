<?php

class m211129_094459_m5079_liens_erih extends \CDbMigration
{
	public function up(): bool
	{
		$titres = Titre::model()->findAllBySql(
			"SELECT t.* FROM Titre t JOIN LienTitre lt ON lt.titreId = t.id WHERE lt.domain = 'dbh.nsd.uib.no' GROUP BY t.id"
		);
		$importId = ImportType::getSourceId('erihplus');
		$count = 0;
		foreach ($titres as $t) {
			/** @var Titre $t */
			$liens = json_decode($t->liensJson);
			$modified = false;
			foreach ($liens as $l) {
				if (strpos($l->url, 'dbh.nsd.uib.no') > 0) {
					$l->url = str_replace('dbh.nsd.uib.no', 'kanalregister.hkdir.no', $l->url);
					$modified = true;
				}
			}
			if ($modified) {
				$i = $t->buildIntervention(true);
				$i->setScenario('import');
				$i->description = "Modification du lien Erih+";
				$i->contenuJson->updateByAttributes($t, ['liensJson' => json_encode($liens)]);
				$i->contenuJson->isImport = true;
				$i->setImport($importId);
				$i->accept(true);
				$count++;
			}
		}
		echo "{$count} liens Erih+ modifiés\n";
		return true;
	}

	public function down(): bool
	{
		echo "m211129_094459_m5079_liens_erih does not support migrating down.\n";
		return false;
	}
}
