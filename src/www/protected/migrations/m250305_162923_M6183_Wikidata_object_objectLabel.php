<?php

class m250305_162923_M6183_Wikidata_object_objectLabel extends \CDbMigration
{
	public function up(): bool
	{
		$this->addColumn(
			'Wikidata',
			'object',
			"varchar(20) CHARACTER SET ascii NOT NULL DEFAULT '' AFTER `url`"
		);
		$this->addColumn(
			'Wikidata',
			'objectLabel',
			"varchar(4095) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' AFTER `object`"
		);
		return true;
	}

	public function down(): bool
	{
		$this->dropColumn('Wikidata', 'objectLabel');
		$this->dropColumn('Wikidata', 'object');
		return true;
	}
}
