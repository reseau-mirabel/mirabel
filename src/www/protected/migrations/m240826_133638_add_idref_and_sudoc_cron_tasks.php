<?php

class m240826_133638_add_idref_and_sudoc_cron_tasks extends CDbMigration
{
	public function up()
	{
		if (!defined('YII_TARGET')) {
			//IdRef Obsolete
			$this->insert("CronTask", [
				'name' => 'Vérification IdRef fusionnés',
				'description' => "Fournit la liste des éditeurs obsolètes car fusionnés dans IdRef.",
				'fqdn' => '\processes\cron\tasks\ModelIdrefObsolete',
				'config' => '{"verbose": 0}',
				'schedule' => '{"periodicity":"daily","hours":["06:00"],"days":[0]}',
				'active' => 0,
				'emailTo' => "mirabel_donnees@
mirabel_editeurs@",
				'emailSubject' => "Cron M vérification IdRef fusionnés",
				'timelimit' => 0,
			]);

			//Update By IdRef API
			$this->insert("CronTask", [
				'name' => 'Mise à jour des dates d\'éditeurs par idref.fr',
				'description' => "Mise à jour des dates des éditeurs via l'API de idref.fr.",
				'fqdn' => '\processes\cron\tasks\ModelUpdateByIdrefApi',
				'config' => '{"verbose": 0}',
				'schedule' => '{"periodicity":"daily","hours":["06:18"],"days":[3]}',
				'active' => 0,
				'emailTo' => "mirabel_editeurs@
mirabel_donnees@",
				'emailSubject' => "Cron M dates d'éditeurs par idref.fr",
				'timelimit' => 0,
			]);

			//Sudoc Check
			$this->insert("CronTask", [
				'name' => 'Sudoc check',
				'description' => "Vérifie pour chaque ISSN le ppn présent sur le SUDOC et ajoute ou met a jour la valeur locale",
				'fqdn' => '\processes\cron\tasks\ModelSudocCheck',
				'config' => '{"verbose": 0}',
				'schedule' => '{"periodicity":"daily","hours":["03:40"],"days":[0]}',
				'active' => 0,
				'emailTo' => "mirabel_donnees@
parra@",
				'emailSubject' => "Cron M sudoc check",
				'timelimit' => 0,
			]);

			//Sudoc Import
			$this->insert("CronTask", [
				'name' => 'Sudoc import',
				'description' => "Interroge l'API du SUDOC pour tous ISSN associé a un ppn dans Mir@bel pour :
1. Mettre à jour les informations du titre s'il y a un changement.
2. Récupérer les états de collections du titre.",
				'fqdn' => '\processes\cron\tasks\ModelSudocImport',
				'config' => '{"verbose": 0}',
				'schedule' => '{"periodicity":"daily","hours":["05:40"],"days":[0]}',
				'active' => 0,
				'emailTo' => "mirabel_donnees@
parra@",
				'emailSubject' => "Cron M sudoc import",
				'timelimit' => 0,
			]);
		}
	}

	public function down()
	{
		$this->delete("CronTask", 'name = "Vérification IdRef fusionnés"');
		$this->delete("CronTask", 'name = "Mise à jour des dates d\'éditeurs par idref.fr"');
		$this->delete("CronTask", 'name = "Sudoc check"');
		$this->delete("CronTask", 'name = "Sudoc import"');
	}
}
