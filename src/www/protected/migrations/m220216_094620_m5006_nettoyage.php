<?php

class m220216_094620_m5006_nettoyage extends \CDbMigration
{
	public function up(): bool
	{
		$this->dropColumn("Politique_Titre", "removedOn");
		return true;
	}

	public function down(): bool
	{
		echo "m220216_094620_m5006_nettoyage does not support migrating down.\n";
		return false;
	}
}
