<?php

class m241028_120813_m6033_organisation_internationale extends \CDbMigration
{
	public function safeUp()
	{
		$this->insert("Pays", [
			'nom' => "Organisation internationale",
			'code' => 'ZZZ',
			'code2' => 'ZZ',
		]);
	}

	public function safeDown()
	{
		$this->delete("Pays", "code2 = 'ZZ'");
		return true;
	}
}
