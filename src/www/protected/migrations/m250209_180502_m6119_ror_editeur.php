<?php

class m250209_180502_m6119_ror_editeur extends \CDbMigration
{
	public function up(): bool
	{
		$this->addColumn("Editeur", "ror", "VARCHAR(9) COLLATE ascii_bin DEFAULT NULL COMMENT 'ROR identifier' AFTER sherpa");
		return true;
	}

	public function down(): bool
	{
		$this->dropColumn("Editeur", "ror");
		return true;
	}
}
