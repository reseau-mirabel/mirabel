<?php

class m200902_074515_mysql_utf8mb4 extends CDbMigration
{
	public function up()
	{
		$sqls = [
			"ALTER TABLE Abonnement DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci",
			"ALTER TABLE CategorieAlias_Titre DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci",
			"ALTER TABLE CategorieStats DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci",
			"ALTER TABLE Categorie_Revue DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci",
			"ALTER TABLE Cms DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci",
			"ALTER TABLE Collection DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci,
MODIFY `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
MODIFY `identifiant` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
MODIFY `description` text COLLATE utf8mb4_unicode_ci NOT NULL",
			"ALTER TABLE Config DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci,
MODIFY `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
MODIFY `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
MODIFY `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
MODIFY `description` varchar(4096) COLLATE utf8mb4_unicode_ci DEFAULT ''",
			"ALTER TABLE Hint DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci",
			"ALTER TABLE Identification DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci,
MODIFY `idInterne` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'identifiant utilisé en interne par la resource'",
			"ALTER TABLE ImportedLink DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci",
			"ALTER TABLE Intervention DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci,
MODIFY `description` varchar(4096) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
MODIFY `statut` enum('attente','accepté','refusé') COLLATE utf8mb4_unicode_ci NOT NULL,
MODIFY `contenuJson` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON',
MODIFY `ip` varchar(40) COLLATE ascii_bin NOT NULL,
MODIFY `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'si utilisateur non identifié par son ID',
MODIFY `commentaire` varchar(4095) COLLATE utf8mb4_unicode_ci NOT NULL",
			"ALTER TABLE LienEditeur DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci,
MODIFY `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL",
			"ALTER TABLE LienTitre DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci,
MODIFY `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL",
			"ALTER TABLE LogExt DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci",
			"ALTER TABLE Partenaire_Titre DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci,
MODIFY `identifiantLocal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
MODIFY `bouquet` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL",
			"ALTER TABLE Revue DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci,
MODIFY `statut` enum('normal','suppr','attente') COLLATE ascii_bin NOT NULL",
			"ALTER TABLE RomeoPolicy DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci",
			"ALTER TABLE Service DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci,
MODIFY `type` enum('Intégral','Résumé','Sommaire','Indexation') COLLATE utf8mb4_unicode_ci NOT NULL,
MODIFY `acces` enum('Libre','Restreint') COLLATE utf8mb4_unicode_ci NOT NULL,
MODIFY `numeroDebut` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
MODIFY `numeroFin` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
MODIFY `dateBarrDebut` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'au format JSON',
MODIFY `dateBarrFin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'au format JSON',
MODIFY `dateBarrInfo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'précalculé à partir de dateBarrDebut',
MODIFY `embargoInfo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
MODIFY `notes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
MODIFY `statut` enum('normal','suppr','attente') COLLATE ascii_bin NOT NULL",
			"ALTER TABLE Service_Collection DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci",
			"ALTER TABLE Sourcelien DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci,
MODIFY `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
MODIFY `nomlong` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
MODIFY `nomcourt` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
MODIFY `url` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT ''",
			"ALTER TABLE Suivi DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci",
			"ALTER TABLE Titre_Editeur DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci",
			"ALTER TABLE Utilisateur DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci,
MODIFY `nom` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
MODIFY `prenom` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
MODIFY `nomComplet` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
MODIFY `motdepasse` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT 'Si Partenaire provisoire ou démo'",
			"ALTER TABLE VerifUrlEditeur DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci,
MODIFY `url` varchar(255) COLLATE utf8mb4_bin NOT NULL,
MODIFY `msg` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL",
			"ALTER TABLE VerifUrlRessource DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci,
MODIFY `url` varchar(255) COLLATE utf8mb4_bin NOT NULL,
MODIFY `msg` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL",
			"ALTER TABLE VerifUrlRevue DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci,
MODIFY `url` varchar(255) COLLATE utf8mb4_bin NOT NULL,
MODIFY `msg` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL",
			"ALTER TABLE Wikidata DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci,
MODIFY `qId` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
MODIFY `property` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
MODIFY `value` varchar(4095) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
MODIFY `url` varchar(4095) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
MODIFY `compUrl` varchar(511) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT 'valeur de référence dans Mirabel à comparer',
MODIFY `compDetails` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT 'url interne pour pointer sur des détails'",
			"ALTER TABLE tbl_migration DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci",
		];
		foreach ($sqls as $sql) {
			$this->execute($sql);
		}
	}

	public function down()
	{
		echo "m200902_074515_mysql_utf8mb4 does not support migrating down.\n";
		return false;
	}
}
