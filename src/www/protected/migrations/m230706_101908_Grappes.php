<?php

class m230706_101908_Grappes extends \CDbMigration
{
	public function up(): bool
	{
		if (!$this->getDbConnection()->getSchema()->getTable('Grappe')) {
			$this->createTable(
				"Grappe",
				[
					'id' => "INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY",
					'nom' => "varchar(255) NOT NULL",
					'description' => "text NOT NULL",
					'diffusion' => "tinyint unsigned NOT NULL COMMENT '1:public ...'",
					'niveau' => "tinyint unsigned NOT NULL COMMENT '1:titre 2:revue'",
					'recherche' => "json NOT NULL DEFAULT '[]' COMMENT 'critères de recherche'",
					'partenaireId' => "int unsigned DEFAULT NULL COMMENT 'eventuellement nullable'",
					'hdateCreation' => "int unsigned NOT NULL",
					'hdateModif' => "int unsigned DEFAULT NULL",
				]
			);
			$this->addForeignKey('grappe_partenaire_fk', "Grappe", "partenaireId", "Partenaire", "id", 'SET NULL');
		}
		if (!$this->getDbConnection()->getSchema()->getTable('Grappe_Titre')) {
			$this->createTable(
				"Grappe_Titre",
				[
					'grappeId' => "int unsigned NOT NULL",
					'titreId' => "int unsigned NOT NULL",
					'source' => "tinyint NOT NULL DEFAULT -1 COMMENT '-1:manual sinon numéro du critère'",
				]
			);
			$this->addPrimaryKey('grappe_titre_grappe_pk', "Grappe_Titre", ["grappeId", "titreId"]);
			$this->addForeignKey('grappe_titre_grappe_fk', "Grappe_Titre", "grappeId", "Grappe", "id", 'CASCADE');
			$this->addForeignKey('grappe_titre_titre_fk', "Grappe_Titre", "titreId", "Titre", "id", 'CASCADE');
		}
		return true;
	}

	public function down(): bool
	{
		$this->dropTable('Grappe_Titre');
		$this->dropTable('Grappe');
		return true;
	}
}
