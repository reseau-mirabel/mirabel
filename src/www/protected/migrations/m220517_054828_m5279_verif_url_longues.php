<?php

class m220517_054828_m5279_verif_url_longues extends \CDbMigration
{
	public function up(): bool
	{
		$this->alterColumn("VerifUrlEditeur", "url", "varbinary(511) NOT NULL");
		$this->alterColumn("VerifUrlRessource", "url", "varbinary(511) NOT NULL");
		$this->alterColumn("VerifUrlRevue", "url", "varbinary(511) NOT NULL");
		return true;
	}

	public function down(): bool
	{
		return true;
	}
}
