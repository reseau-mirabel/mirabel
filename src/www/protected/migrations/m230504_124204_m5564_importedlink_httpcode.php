<?php

class m230504_124204_m5564_importedlink_httpcode extends \CDbMigration
{
	public function up(): bool
	{
		$this->addColumn("ImportedLink", "httpcode", "smallint unsigned not null default 200 COMMENT '0 if network error'");
		$this->alterColumn("ImportedLink", "id", "binary(20) COMMENT 'usually a hash of the URL: UNHEX(SHA1(url))'");
		return true;
	}

	public function down(): bool
	{
		$this->dropColumn("ImportedLink", "httpcode");
		return true;
	}
}
