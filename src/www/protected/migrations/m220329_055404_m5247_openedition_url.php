<?php

class m220329_055404_m5247_openedition_url extends \CDbMigration
{
	public function safeUp()
	{
		$this->execute("UPDATE Titre SET url = REPLACE(url, 'http://', 'https://') WHERE url LIKE 'http://journals.openedition.org/%'");
		return true;
	}

	public function safeDown()
	{
		return true;
	}
}
