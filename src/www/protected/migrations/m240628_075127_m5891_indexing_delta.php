<?php

class m240628_075127_m5891_indexing_delta extends \CDbMigration
{
	public function up(): bool
	{
		if (!$this->getDbConnection()->getSchema()->getTable("IndexingRequiredTitre")) {
			$this->createTable(
				"IndexingRequiredTitre",
				[
					"id" => "INT unsigned NOT NULL PRIMARY KEY COMMENT 'Titre.id (but no FK is declared)'",
					"updated" => "BIGINT NOT NULL COMMENT 'timestamp of the last update of anything related'",
					"deleted" => "BOOLEAN NOT NULL COMMENT 'if 1, should add this ID to the kill list'",
				]
			);
		}
		return true;
	}

	public function down(): bool
	{
		$this->dropTable("IndexingRequiredTitre");
		return true;
	}
}
