<?php

class m230817_121554_m5487_verif_url_table extends \CDbMigration
{
	public function up(): bool
	{
		if ($this->getDbConnection()->getSchema()->getTable("VerifUrl")) {
			$this->dropTable("VerifUrl");
		}
		$this->createTable(
			'VerifUrl',
			[
				'id' => "int unsigned NOT NULL PRIMARY KEY AUTO_INCREMENT",
				'source' => "tinyint unsigned NOT NULL COMMENT 'enum of source tables'",
				'sourceId' => "int unsigned NOT NULL COMMENT 'fk of the source table'",
				'url' => "varbinary(511) NOT NULL",
				'success' =>  "tinyint(1) NOT NULL",
				'msg' => "varchar(255) NOT NULL DEFAULT '' COLLATE utf8mb4_unicode_ci",
				'hdate' => "timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()",
			]
		);
		$this->createIndex('verifurl_fk', 'VerifUrl', ['source', 'sourceId'], false);
		$sqls = [
			"INSERT INTO VerifUrl SELECT null, 2, editeurId, url, success, msg, hdate FROM VerifUrlEditeur",
			"INSERT INTO VerifUrl SELECT null, 3, ressourceId, url, success, msg, hdate FROM VerifUrlRessource",
			"INSERT INTO VerifUrl SELECT null, 4, revueId, url, success, msg, hdate FROM VerifUrlRevue",
		];
		foreach ($sqls as $sql) {
			$this->execute($sql);
		}
		return true;
	}

	public function down(): bool
	{
		echo "m230817_121554_m5487_verif_url_table does not support migrating down.\n";
		return false;
	}
}
