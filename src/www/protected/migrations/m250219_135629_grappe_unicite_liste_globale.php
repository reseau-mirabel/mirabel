<?php

class m250219_135629_grappe_unicite_liste_globale extends \CDbMigration
{
	public function up(): bool
	{
		$sqls = [
			// Supprime les doublons.
			<<<SQL
			DELETE FROM Partenaire_Grappe
			WHERE
				partenaireId IS NULL
				AND grappeId IN (SELECT pg.grappeId FROM Partenaire_Grappe pg WHERE pg.hdateCreation > Partenaire_Grappe.hdateCreation)
			SQL,
			// Remplace l'index d'unicité par une unicité sans NULL possible.
			"ALTER TABLE Partenaire_Grappe ADD INDEX partenaire_grappe_gid (grappeId)",
			"ALTER TABLE Partenaire_Grappe ADD INDEX partenaire_grappe_pid (partenaireId)",
			"ALTER TABLE Partenaire_Grappe DROP INDEX u_partenaire_grappe",
			"ALTER TABLE Partenaire_Grappe ADD COLUMN pid int unsigned AS (IFNULL(partenaireId, 0)) VIRTUAL",
			"ALTER TABLE Partenaire_Grappe ADD UNIQUE u_partenaire_grappe (grappeId, pid)",
			"ALTER TABLE Partenaire_Grappe DROP INDEX partenaire_grappe_gid",
		];

		foreach ($sqls as $sql) {
			$this->execute($sql);
		}
		return true;
	}

	public function down(): bool
	{
		echo "m250219_135629_grappe_unicite_liste_globale does not support migrating down.\n";
		return false;
	}
}
