<?php

class m231006_135150_Grappe_publicite extends \CDbMigration
{
	public function up(): bool
	{
		$this->addColumn('Grappe', 'publicite', "tinyint unsigned NOT NULL DEFAULT 1 comment 'De 1 à 5' AFTER diffusion");
		return true;
	}

	public function down(): bool
	{
		$this->dropColumn('Grappe', 'publicite');
		return true;
	}
}
