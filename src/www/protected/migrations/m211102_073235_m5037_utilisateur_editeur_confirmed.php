<?php

class m211102_073235_m5037_utilisateur_editeur_confirmed extends \CDbMigration
{
	public function up(): bool
	{
		if ($this->dbConnection->schema->getTable('Utilisateur_Editeur')->getColumn('confirmed') !== null) {
			return true;
		}
		$this->addColumn("Utilisateur_Editeur", "confirmed", "BOOL NOT NULL DEFAULT 0 AFTER `role`");
		$this->execute("UPDATE Utilisateur_Editeur SET confirmed = 1 WHERE `role` <> 'pending'");
		$this->execute("UPDATE Utilisateur_Editeur SET role = 'owner' WHERE `role` = 'pending'");
		$this->alterColumn("Utilisateur_Editeur", "role", "enum('guest','owner') CHARACTER SET ascii COLLATE ascii_bin NOT NULL COMMENT 'role is active once confirmed=1'");
		return true;
	}

	public function down(): bool
	{
		echo "m211102_073235_m5037_utilisateur_editeur_confirmed does not support migrating down.\n";
		return false;
	}
}
