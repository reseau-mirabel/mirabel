<?php

class m210826_104045_politique_log extends \CDbMigration
{
	public function up(): bool
	{
		if (!in_array('PolitiqueLog', $this->dbConnection->schema->tableNames)) {
			$this->createTable(
				"PolitiqueLog",
				[
					"id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT",
					"action VARCHAR(50) NOT NULL COMMENT 'liste d''autorité dans le PHP'",
					"actionTime BIGINT NOT NULL COMMENT 'timestamp'",
					"politiqueId INT UNSIGNED NOT NULL COMMENT 'pas de clé étrangère sur Politique.id'",
					"editeurId INT UNSIGNED NOT NULL COMMENT 'pas de clé étrangère sur Editeur.id'",
					"titreId INT UNSIGNED NOT NULL COMMENT 'pas de clé étrangère sur Titre.id'",
					"utilisateurId INT UNSIGNED NOT NULL COMMENT 'auteur, pas de clé étrangère sur Utilisateur.id'",
				],
				"COMMENT 'historique des opérations concernant les politiques publiées'"
			);
		}
		return true;
	}

	public function down(): bool
	{
		$this->dropTable("PolitiqueLog");
		return true;
	}
}
