<?php

class m230306_182314_m5490_actu_sans_filtre extends \CDbMigration
{
	public function up(): bool
	{
		$this->insert('Cms', [
			'name' => 'actualite-menu',
			'singlePage' => 0,
			'pageTitle' => '',
			'type' => 'html',
			'content' => '',
			'hdateCreat' => time(),
		]);
		$this->insert('Cms', [
			'name' => 'actualite-sans-filtre',
			'singlePage' => 0,
			'pageTitle' => '',
			'type' => 'html',
			'content' => '',
			'hdateCreat' => time(),
		]);
		return true;
	}

	public function down(): bool
	{
		$this->delete("Cms", "name IN('actualite-menu', 'actualite-sans-filtre')");
		return true;
	}
}
