<?php

class m221204_220205_Cms_categorisation extends \CDbMigration
{
	public function up(): bool
	{
		$this->addColumn(
			"Cms",
			"categorisation",
			"JSON NOT NULL DEFAULT '{}' COMMENT 'json partenaires, ressources et catégories liées' AFTER content"
		);
		if (defined('YII_TARGET') && YII_TARGET === 'test') {
			$this->update('Cms', ['categorisation' => '{"categories":["Partenariats"]}'], "name = 'brève' AND content LIKE '%partenaire%'");
			$this->update('Cms', ['categorisation' => '{"categories":["Partenariats","gâté-pourri"], "partenaires":[2]}'], "name = 'brève' AND id = 11");
			$this->update('Cms', ['categorisation' => '{"categories":["Gâteau guérit"],"ressources":[12,3]}'], "name = 'brève' AND id = 18");
		}
		return true;
	}

	public function down(): bool
	{
		$this->dropColumn('Cms', 'categorisation');
		return true;
	}
}
