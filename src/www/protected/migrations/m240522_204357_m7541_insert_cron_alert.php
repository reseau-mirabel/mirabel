<?php

class m240522_204357_m7541_insert_cron_alert extends \CDbMigration
{
	public function up(): bool
	{
		if (defined('YII_TARGET') && YII_TARGET !== 'test') {
			$this->insert("CronTask", [
				'name' => 'Alerte suivi',
				'description' => "",
				'fqdn' => '\processes\cron\tasks\ModelAlert',
				'config' => '{"intervention":"suivi","verbose":"1","since":"","before":""}',
				'schedule' => '{"periodicity":"daily","hours":["00:00"],"days":[0,1,2,3,4,5,6]}',
				'active' => 1,
				'emailTo' => "",
				'emailSubject' => "",
				'timelimit' => 0,
			]);
		}
		return true;
	}

	public function down(): bool
	{
		return true;
	}
}
