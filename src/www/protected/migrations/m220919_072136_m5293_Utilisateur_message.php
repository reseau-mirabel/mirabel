<?php

class m220919_072136_m5293_Utilisateur_message extends \CDbMigration
{
	public function up(): bool
	{
		$this->addColumn("Utilisateur", "message", "TEXT COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Message de l''utilisateur aux admins (modérateurs de politiques)' AFTER notes");
		$this->execute("UPDATE Utilisateur SET message = notes, notes = '' WHERE notes <> '' AND partenaireId IS NULL");
		return true;
	}

	public function down(): bool
	{
		$this->dropColumn("Utilisateur", "message");
		return true;
	}
}
