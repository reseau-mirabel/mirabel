<?php

class m220112_093847_m5125_cms_loginintro extends \CDbMigration
{
	public function up(): bool
	{
		$this->insert('Cms', [
			'name' => 'connexion-intro',
			'singlePage' => 0,
			'pageTitle' => "Intro sur la page de connexion",
			'type' => 'html',
			'content' => '',
			'private' => 0,
			'hdateCreat' => time(),
			'hdateModif' => 0,
		]);
		return true;
	}

	public function down(): bool
	{
		$this->delete('Cms', "name = 'connexion-intro' AND singlePage = 0");
		return true;
	}
}
