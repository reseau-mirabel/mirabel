<?php

class m241024_173431_m5857_Config_validate_forbid_params extends \CDbMigration
{
	/**
	 * Applies this migration.
	 */
	public function up()
	{
		$this->insert("Config", [
			'key' => 'validate.forbid.params',
			'value' => "SessionID\naccountid\n",
			'type' => 'text',
			'category' => 'validation',
			'description' => "<p>Lister les paramètres interdits dans les URLs des revues, ressources, etc.</p>",
			'required' => 0,
			'createdOn' => time(),
		]);
	}

	/**
	 * Reverts this migration.
	 *
	 * @return bool False if it failed.
	 */
	public function down()
	{
		$this->delete("Config", "`key` = 'validate.forbid.params'");
		return true;
	}
}
