<?php

class m211013_165452_config_validate_skip_domains extends \CDbMigration
{
	/**
	 * Applies this migration.
	 */
	public function up()
	{
		$this->insert("Config", [
			'key' => 'validate.skip.domains',
			'value' => "linkedin.com\njstor.org\nbrepolsonline.net",
			'type' => 'text',
			'category' => 'validation',
			'description' => "<p>Lister les domaines (DNS) dont les URLs seront acceptées mais jamais vérifiées.</p>
<p>Par exemple, ajouter une ligne <code>jstor.org</code> fera que <code>https://jstor.org/xyz</code> sera ignoré à la validation.</p>",
			'required' => 0,
			'createdOn' => time(),
		]);
	}

	/**
	 * Reverts this migration.
	 *
	 * @return bool False if it failed.
	 */
	public function down()
	{
		$this->dbConnection->createCommand("DELETE FROM Config WHERE `key` = 'validate.skip.domains'")->execute();
		return true;
	}
}
