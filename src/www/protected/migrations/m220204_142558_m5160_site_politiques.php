<?php

class m220204_142558_m5160_site_politiques extends \CDbMigration
{
	public function up(): bool
	{
		$now = time();
		$this->insertMultiple("Cms", [
			[
				'name' => 'page-politiques-1',
				'singlePage' => 0,
				'pageTitle' => '',
				'type' => 'markdown',
				'content' => "## [Déclarer mes politiques](/site/login?mode=politique)\n…",
				'hdateCreat' => $now,
			],
			[
				'name' => 'page-politiques-2',
				'singlePage' => 0,
				'pageTitle' => '',
				'type' => 'markdown',
				'content' => "## [Créer un compte](/politique/utilisateur)\n…",
				'hdateCreat' => $now,
			],
			[
				'name' => 'page-politiques-3',
				'singlePage' => 0,
				'pageTitle' => '',
				'type' => 'markdown',
				'content' => "## [Être accompagné](/site/page/politiques-publication)\n\nFoire aux questions\n\nAide méthodologique à la définition d'une politique",
				'hdateCreat' => $now,
			],
			[
				'name' => 'page-politiques-4',
				'singlePage' => 0,
				'pageTitle' => '',
				'type' => 'markdown',
				'content' => "## [En savoir plus sur le projet](/site/page/projet-ppa)\n\nOuvrir la Science avec Sherpa Romeo\n\nLa stratégie de non-cession des droits du Plan S à l’ANR",
				'hdateCreat' => $now,
			],
		]);
		return true;
	}

	public function down(): bool
	{
		$this->delete("Cms", "name LIKE 'page-politiques-_'");
		return true;
	}
}
