<?php

class m231016_162038_grappe_multi_sources extends \CDbMigration
{
	public function up(): bool
	{
		$this->execute(<<<EOSQL
			ALTER TABLE Grappe_Titre
			DROP PRIMARY KEY,
			ADD PRIMARY KEY (grappeId, titreId, source)
			EOSQL
		);
		return true;
	}

	public function down(): bool
	{
		echo "m231016_162038_grappe_multi_sources does not support migrating down.\n";
		return false;
	}
}
