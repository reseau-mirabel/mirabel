<?php

class m201005_074046_ressource_logourl extends CDbMigration
{
	public function up()
	{
		$this->addColumn("Ressource", "logoUrl", "varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' AFTER url");
		return true;
	}

	public function down()
	{
		$this->dropColumn("Ressource", "logoUrl");
		return true;
	}
}
