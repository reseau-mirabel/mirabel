<?php

class m250304_145026_m6164_liens_sans_domaine extends \CDbMigration
{
	public function safeUp()
	{
		$this->execute("UPDATE LienEditeur SET domain = 'interne à Mir@bel' WHERE domain = '' AND url LIKE '/%'");
		$this->execute("UPDATE LienTitre SET domain = 'interne à Mir@bel' WHERE domain = '' AND url LIKE '/%'");
		return true;
	}

	public function safeDown()
	{
		return true;
	}
}
