<?php

class m220706_095356_m5339_titre_default_values extends \CDbMigration
{
	public function up(): bool
	{
		$this->execute(<<<EOSQL
			ALTER TABLE Titre
				ALTER sigle SET DEFAULT '',
				ALTER url SET DEFAULT '',
				ALTER urlCouverture SET DEFAULT '',
				ALTER liensJson SET DEFAULT '',
				ALTER periodicite SET DEFAULT '',
				ALTER langues SET DEFAULT ''
			EOSQL
		);
		return true;
	}

	public function down(): bool
	{
		return true;
	}
}
