<?php

class m230711_095548_logos_ressource extends \CDbMigration
{
	public function up(): bool
	{
		chdir("src/www/images/ressources-logos");
		$files = glob('*.{jpg,gif,png,svg}', GLOB_BRACE);
		$m = [];
		foreach ($files as $f) {
			if (preg_match('/^(\d+)\.(...)$/', $f, $m)) {
				rename($f, sprintf("%06d.%s", $m[1], $m[2]));
			}
		}

		$thumbnails = glob('h55/*.png');
		foreach ($thumbnails as $f) {
			unlink($f);
		}

		$rows = \Yii::app()->db->createCommand("SELECT id, content FROM Cms WHERE content LIKE '%/ressources-logos/%';")->queryAll();
		$update = \Yii::app()->db->createCommand("UPDATE Cms SET content = :c WHERE id = :id");
		foreach ($rows as $row) {
			$update->execute([
				':id' => $row['id'],
				':c' => preg_replace_callback(
					'#/ressources-logos/h55/(\d+)\.#',
					function ($m) {
						return sprintf("/ressources-logos/h55/%06d.", $m[1]);
					},
					$row['content']
				),
			]);
		}
		return true;
	}

	public function down(): bool
	{
		return true;
	}
}
