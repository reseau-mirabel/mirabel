<?php

class m230711_152240_m5640_kbart_bacon extends \CDbMigration
{
	public function up(): bool
	{
		$interventions = Intervention::model()->findAllBySql(<<<EOSQL
			SELECT *
			FROM Intervention
			WHERE import = 7
				AND hdateVal >= 1689064860
				AND contenuJson LIKE '%"after":{"dateBarrDebut":"____-01-01"%'
			ORDER BY id ASC
			EOSQL
		);
		fprintf(STDERR, "%d interventions à annuler puis supprimer...\n", count($interventions));
		foreach ($interventions as $intv) {
			assert($intv instanceof Intervention);
			if (!$intv->revert()) {
				fprintf(STDERR, "La réversion a échoué pour l'intervention {$intv->id}.\n");
				continue;
			}
			if (!$intv->delete()) {
				fprintf(STDERR, "La suppression a échoué pour l'intervention {$intv->id}.\n");
			}
		}
		return true;
	}

	public function down(): bool
	{
		return true;
	}
}
