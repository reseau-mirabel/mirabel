<?php

class m220412_131144_m3031_lang_completion extends \CDbMigration
{
	public function up(): bool
	{
		if (Config::read('lang.completion') === null) {
			$this->insert("Config", [
				'key' => 'lang.completion',
				'value' => <<<EOTEXT
					filipino;fil
					EOTEXT,
				'type' => 'assoc',
				'category' => 'lang',
				'description' => <<<EOHTML
					<p>Liste de langues proposées à la complétion, en plus des principales langues.
					Par défaut, la liste est limitée aux langues à code de 2 lettres de la norme ISO-639-2 (environ 150 langues).
					<p>Ajouter les langues sous la forme CSV, avec d'abord le nom complet, puis le code à 3 lettres, éventuellement avec des guillemets.
					<pre><code>"filipino";fil</code></pre>
					EOHTML,
				'required' => 0,
				'createdOn' => time(),
			]);
		}
		return true;
	}

	public function down(): bool
	{
		$this->delete('Config', "`key` = 'lang.completion'");
		return true;
	}
}
