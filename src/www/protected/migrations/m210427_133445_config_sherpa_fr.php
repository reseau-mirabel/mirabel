<?php

class m210427_133445_config_sherpa_fr extends \CDbMigration
{
	public function safeUp()
	{
		$this->insert(
			'Config',
			[
				'createdOn' => time(),
				'category' => 'sherpa',
				'type' => 'assoc',
				'description' => "CSV à deux colonnes, avec sur chaque ligne :<pre>le texte de Sherpa;HTML de sa traduction en français.</pre>"
					. " Utilisé comme liste de valeurs possibles dans le mode expert de modification d'une politique."
					. " Les guillemets ne sont nécessaires que si la cellule contient ';'.",
				'key' => 'sherpa.fr.prerequisites',
				'value' =>
					<<<EOVALUE
					requires_publisher_permission;après autorisation de l'éditeur
					when_required_by_funder;si imposé par le financeur
					when_required_by_institution;si imposé par l'institution
					when_required_by_law;si imposé par la loi
					EOVALUE
			]
		);
		$this->insert(
			'Config',
			[
				'createdOn' => time(),
				'category' => 'sherpa',
				'type' => 'assoc',
				'description' => "CSV à deux colonnes, avec sur chaque ligne :<pre>le texte de Sherpa;HTML de sa traduction en français.</pre>"
					. " Utilisé comme liste de valeurs possibles dans le mode expert de modification d'une politique. Plus de 70 valeurs distinctes dans Sherpa."
					. " Les guillemets ne sont nécessaires que si la cellule contient ';'.",
				'key' => 'sherpa.fr.public_notes',
				'value' =>
					<<<EOVALUE
					This pathway is strictly in accordance with Article 30 of the Law for a Digital Republic of October 7, 2016.;Ce processus respecte la loi pour une République numérique du 7 octobre 2016, article 30.
					This pathway allows for a non-embargoed deposit strictly in locations and under the conditions indicated.;Ce processus autorise un dépôt sans-embargo uniquement dans le respect des lieux et emplacements précisés.
					Authors can shar their accepted manuscript immediately by updating a preprint in arXiv or RePEc with the accepted manuscript;Les auteurs... (typo×90 dans la version anglaise)
					Deposit of submitted version is discouraged, but is permitted;Le dépôt de la version soumise est découragé, mais permis
					Authors are encouraged to submit their published articles to institutional repositories;Les auteurs sont incités à déposer leur article publié dans les dépôts institutionnels
					For Science, Technical and Medicine titles;Pour les titres de science, technique ou médecine
					For Humanities and Social Science titles;Pour les titres de sciences humaines
					Must acknowledge published source with citation;La version publiée doit être citée
					Upon official publication;Lors de la publication officielle
					Anytime;À tout moment
					EOVALUE
			]
		);
		return true;
	}

	public function safeDown()
	{
		$this->delete("Config", "`key` IN ('prerequisites', 'public_notes')");
		return true;
	}
}
