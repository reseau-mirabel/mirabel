<?php

class m230130_195259_m4447_Ressources_elague_exhaustif extends \CDbMigration
{
	public function up(): bool
	{
		if (defined('YII_TARGET') && YII_TARGET === 'test') {
			return true;
		}
		$sql = <<<EOSQL
			UPDATE Ressource
			SET exhaustif=2 WHERE id IN (
				SELECT R.id
				FROM Ressource R
					JOIN Service S ON R.id = S.ressourceId
					JOIN Titre T ON T.id = S.titreId
				WHERE exhaustif = 1
				GROUP BY R.id
				HAVING COUNT(DISTINCT revueId) <= 2
			)
			EOSQL;
		$this->execute($sql);

		return true;
	}

	public function down(): bool
	{
		echo "m230130_195259_m4447_Ressources_elague_exhaustif does not support migrating down.\n";
		return false;
	}
}
