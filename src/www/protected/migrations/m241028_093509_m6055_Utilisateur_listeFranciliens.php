<?php

class m241028_093509_m6055_Utilisateur_listeFranciliens extends \CDbMigration
{
	public function up(): bool
	{
		$this->addColumn(
			'Utilisateur',
			'listeFranciliens',
			"tinyint NOT NULL DEFAULT 0 COMMENT 'liste de diffusion mirabel-franciliens' AFTER listeDiffusion"
		);

		$sql = <<<ESQL
			UPDATE Utilisateur U
				JOIN Partenaire P ON (P.id = U.partenaireId)
			SET U.listeFranciliens=1
			WHERE U.actif = 1 AND P.geo IN
				('Paris', 'Nanterre', 'Versailles', 'Champs-sur-Marne', 'Orsay', 'Villejuif',
				 'Rocquencourt', 'La Plaine Saint-Denis', 'Les Ulis', 'Montrouge', 'Aubervilliers');
			ESQL;
		$this->execute($sql);
		return true;
	}

	public function down(): bool
	{
		$this->dropColumn('Utilisateur', 'listeFranciliens');
		return true;
	}
}
