<?php

class m210428_205334_editeur_partenaire_politique extends \CDbMigration
{
	public function safeUp()
	{
		$this->execute(
			<<<EOSQL
			INSERT INTO Utilisateur_Editeur (utilisateurId, editeurId, role)
				SELECT DISTINCT u.id, p.editeurId, 'pending'
				FROM `Utilisateur` u
					JOIN `Partenaire` p ON u.`partenaireId` = p.id
				WHERE p.`editeurId` IS NOT NULL AND p.statut = 'actif'
			EOSQL
		);
		return true;
	}

	public function safeDown()
	{
		$this->execute(
			<<<EOSQL
			DELETE ue
				FROM Utilisateur_Editeur ue
					JOIN `Utilisateur` u ON ue.utilisateurId = u.id
					JOIN `Partenaire` p ON u.`partenaireId` = p.id
				WHERE ue.role = 'pending' AND p.`editeurId` IS NOT NULL
			EOSQL
		);
		return true;
	}
}
