<?php

class m240130_104458_m5441_partenaire_rcr extends \CDbMigration
{
	public function up(): bool
	{
		$this->addColumn(
			'Partenaire',
			'rcr',
			"int NULL COMMENT 'rcr du partenaire'"
		);
		return true;
	}

	public function down(): bool
	{
		$this->dropColumn(
			"Partenaire",
			"rcr"
		);
		return true;
	}
}
