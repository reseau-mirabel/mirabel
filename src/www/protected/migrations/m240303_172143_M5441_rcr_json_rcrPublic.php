<?php

class m240303_172143_M5441_rcr_json_rcrPublic extends \CDbMigration
{
	public function up(): bool
	{
		if ($this->dbConnection->schema->getTable('Partenaire')->getColumn('rcr') !== null) {
			$this->dropColumn('Partenaire', 'rcr');
		}
		$this->addColumn('Partenaire', 'rcr', "JSON NOT NULL DEFAULT '[]' COMMENT
'liste des rcr du partenaire'");

		$this->addColumn(
			'Partenaire',
			'rcrPublic',
			'TINYINT UNSIGNED NOT NULL DEFAULT 0 AFTER rcr'
		);

		$this->dbConnection->schema->refresh();
		return true;
		;
	}

	public function down(): bool
	{
		echo "m240303_172143_M5441_rcr_json_rcrPublic does not support migrating down.\n";
		return false;
	}
}
