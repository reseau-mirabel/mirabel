<?php

class m240329_092833_m5803_partenaire_persorecherche extends \CDbMigration
{
	public function up(): bool
	{
		$this->addColumn(
			"Partenaire",
			"persoRecherche",
			"JSON NULL COMMENT 'liste de Sourcelien.nomcourt' AFTER phrasePerso"
		);
		return true;
	}

	public function down(): bool
	{
		$this->dropColumn("Partenaire", "persoRecherche");
		return true;
	}
}
