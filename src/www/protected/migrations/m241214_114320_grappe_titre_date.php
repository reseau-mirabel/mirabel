<?php

class m241214_114320_grappe_titre_date extends \CDbMigration
{
	public function up(): bool
	{
		$this->addColumn("Grappe_Titre", "insertTime", "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP");
		return true;
	}

	public function down(): bool
	{
		$this->dropColumn("Grappe_Titre", "insertTime");
		return false;
	}
}
