<?php

class m250221_152343_m6183_Sourcelien_Mastodon_Bluesky extends \CDbMigration
{
	public function up(): bool
	{
		$this->insert('Sourcelien', [
			'nom' => 'Bluesky',
			'nomlong' => 'Réseau social de microblogage Bluesky',
			'nomcourt' => 'bluesky',
			'url' => 'bsky.app',
			'urlRegex' => '^https://bsky\.app/profile/',
			'import' => 0,
			'hdateCrea' => time(),
			'hdateModif' => time(),
		]);

		$this->insert('Sourcelien', [
			'nom' => 'Mastodon',
			'nomlong' => 'Réseau social fédéré de microblogage Mastodon',
			'nomcourt' => 'mastodon',
			'import' => 0,
			'hdateCrea' => time(),
			'hdateModif' => time(),
		]);

		$sid = $this->dbConnection->createCommand("SELECT id FROM Sourcelien WHERE `nomcourt` = 'bluesky'")->queryScalar();
		$sql = "UPDATE LienTitre SET sourceId = :sid WHERE name = 'Bluesky'";
		$this->dbConnection->createCommand($sql)->execute([':sid' => $sid]);

		$sid = $this->dbConnection->createCommand("SELECT id FROM Sourcelien WHERE `nomcourt` = 'mastodon'")->queryScalar();
		$sql = "UPDATE LienTitre SET sourceId = :sid WHERE name = 'Mastodon'";
		$this->dbConnection->createCommand($sql)->execute([':sid' => $sid]);
		return true;
	}

	public function down(): bool
	{
		$sql = "UPDATE LienTitre SET sourceId = NULL WHERE name = :name ";
		$this->dbConnection->createCommand($sql)->execute([':name' => 'Bluesky']);
		$this->dbConnection->createCommand($sql)->execute([':name' => 'Mastodon']);
		$this->delete('Sourcelien', "`nomcourt` = 'bluesky'");
		$this->delete('Sourcelien', "`nomcourt` = 'mastodon'");
		return true;
	}
}
