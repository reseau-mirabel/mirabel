<?php

class m241019_130951_selection_grappes_par_partenaire extends \CDbMigration
{
	public function up(): bool
	{
		if (!$this->getDbConnection()->getSchema()->getTable("Partenaire_Grappe")) {
			$this->createTable(
				"Partenaire_Grappe",
				[
					'partenaireId' => "INT unsigned NOT NULL",
					'grappeId' => "INT unsigned NOT NULL",
					'diffusion' => "TINYINT unsigned NOT NULL COMMENT '1 => public, 2 => authentifié'",
					'pages' => "INT unsigned NOT NULL COMMENT 'bitset: 1 => /partenaire, 2 => /partenaire/selection'",
					'position' => "INT UNSIGNED NOT NULL DEFAULT 0",
					'hdateCreation' => "BIGINT unsigned NOT NULL COMMENT 'unix timestamp'",
				],
				"COMMENT 'sélection de grappes pour chaque partenaire'"
			);
			$this->addPrimaryKey('pk_partenaire_grappe', "Partenaire_Grappe", ['partenaireId', 'grappeId']);
			$this->addForeignKey('fk_partenairegrappe_partenaire', "Partenaire_Grappe", "partenaireId", "Partenaire", "id", "CASCADE");
			$this->addForeignKey('fk_partenairegrappe_grappe', "Partenaire_Grappe", "grappeId", "Grappe", "id", "CASCADE");
		}
		$this->execute(<<<SQL
			INSERT IGNORE INTO Partenaire_Grappe
				SELECT partenaireId, id, diffusion, 0, 0, unix_timestamp()
				FROM Grappe
				WHERE partenaireId IS NOT NULL
			SQL
		);
		return true;
	}

	public function down(): bool
	{
		$this->dropTable("Partenaire_Grappe");
		return true;
	}
}
