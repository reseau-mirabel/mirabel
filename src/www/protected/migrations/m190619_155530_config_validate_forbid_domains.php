<?php

class m190619_155530_config_validate_forbid_domains extends CDbMigration
{
	/**
	 * Applies this migration.
	 */
	public function up()
	{
		$this->insert("Config", [
			'key' => 'validate.forbid.domains',
			'value' => "grenet.fr\n",
			'type' => 'text',
			'category' => 'validation',
			'description' => "<p>Lister les domaines (DNS) dont les URLs seront refusées pour les revues, ressources, etc.</p>
<p>Par exemple, ajouter une ligne <code>gog.com</code> fera que <code>https://sub.gog.com/xyz</code> sera refusé avec le message <em>Ce domaine est interdit dans les URL</em>.</p>",
			'required' => 0,
			'createdOn' => time(),
		]);
	}

	/**
	 * Reverts this migration.
	 *
	 * @return bool False if it failed.
	 */
	public function down()
	{
		$this->dbConnection->createCommand("DELETE FROM Config WHERE `key` = 'validate.forbid.domains'")->execute();
		return true;
	}
}
