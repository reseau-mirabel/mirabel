<?php

class m211201_164456_m5063_config_namedrepository extends \CDbMigration
{
	public function up(): bool
	{
		$this->update(
			"Config",
			[
				'description' => "CSV de 1 à 2 colonnes, avec sur chaque ligne :<pre>valeur Sherpa;texte français</pre>"
					. " Utilisé pour suggérer des noms d'archives dans la modification d'une politique, dans le champs à saisie libre."
					. " Les guillemets ne sont nécessaires que si la cellule contient ';'.",
			],
			"`key` = 'politique.liste.location.named_repository'"
		);
		return true;
	}

	public function down(): bool
	{
		return true;
	}
}
