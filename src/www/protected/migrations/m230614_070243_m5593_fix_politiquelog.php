<?php

class m230614_070243_m5593_fix_politiquelog extends \CDbMigration
{
	public function up(): bool
	{
		$sqls = [
			"ALTER TABLE Politique MODIFY COLUMN publisher_policy JSON NOT NULL DEFAULT '{}' COMMENT 'JSON object, cf Sherpa API'",
			"UPDATE PolitiqueLog pl SET pl.publisher_policy = '{}' WHERE pl.publisher_policy = ''",
			"ALTER TABLE PolitiqueLog MODIFY COLUMN publisher_policy JSON NOT NULL DEFAULT '{}' COMMENT 'JSON object, cf Sherpa API'",
			"UPDATE PolitiqueLog pl JOIN Politique p ON pl.politiqueId = p.id SET pl.publisher_policy = p.publisher_policy WHERE pl.publisher_policy = '{}'",
		];
		foreach ($sqls as $sql) {
			$this->execute($sql);
		}
		return true;
	}

	public function down(): bool
	{
		return true;
	}
}
