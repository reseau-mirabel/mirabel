<?php

class m210411_204008_upgrade_passwords extends \CDbMigration
{
	public function up(): bool
	{
		$this->renameColumn("Utilisateur", "authLdap", "authmethod");
		$this->update("Utilisateur", ['authmethod' => Utilisateur::AUTHMETHOD_LEGACY], "authmethod = 0");
		return true;
	}

	public function down(): bool
	{
		$this->update("Utilisateur", ['authmethod' => 0], "authmethod = " . Utilisateur::AUTHMETHOD_LEGACY);
		$this->renameColumn("Utilisateur", "authmethod", "authLdap");
		return true;
	}
}
