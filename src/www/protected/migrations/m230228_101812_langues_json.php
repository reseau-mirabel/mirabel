<?php

class m230228_101812_langues_json extends \CDbMigration
{
	public function up(): bool
	{
		$this->alterColumn("Titre", "langues", "VARCHAR(255) COLLATE ascii_bin DEFAULT '[]'");
		$this->execute(<<<EOSQL
			UPDATE Titre
			SET langues = CONCAT('["', REPLACE(langues, ', ', '","'), '"]')
			WHERE langues <> ''
			EOSQL
		);
		$this->execute(<<<EOSQL
			UPDATE Titre
			SET langues = '[]'
			WHERE langues = ''
			EOSQL
		);
		$this->alterColumn("Titre", "langues", "JSON NOT NULL DEFAULT ''");
		return true;
	}

	public function down(): bool
	{
		echo "m230228_101812_langues_json does not support migrating down.\n";
		return false;
	}
}
