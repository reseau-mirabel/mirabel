<?php

class m210113_124704_m4719_maintenance_cron extends \CDbMigration
{
	public function up(): bool
	{
		if (Config::read('maintenance.cron.arret') !== null) {
			return true;
		}
		$this->insert(
			"Config",
			[
				'key' => 'maintenance.cron.arret',
				'value' => '0',
				'type' => 'boolean',
				'category' => 'maintenance',
				'description' => <<<EOHTML
					La valeur 1 (oui à l'arrêt) bloque l'exécution de la plupart des commandes du cron. 0 (non à l'arrêt) est le fonctionnement normal.
					Les commandes du cron concernées sont toutes celles qui modifient les données : vérifications d'URLs, import d'accès, de liens, de données de Sherpa Romeo, du Sudoc, d'issn.org, de Wikidata.
					EOHTML,
				'required' => 0,
				'createdOn' => time(),
			]
		);
		return true;
	}

	public function down(): bool
	{
		echo "m210113_124704_m4719_maintenance_cron does not support migrating down.\n";
		return false;
	}
}
