<?php

class m220110_100420_drop_table_romeopolicy extends \CDbMigration
{
	public function up(): bool
	{
		$this->dropTable('RomeoPolicy');
		return true;
	}

	public function down(): bool
	{
		echo "m220110_100420_drop_table_romeopolicy does not support migrating down.\n";
		return false;
	}
}
