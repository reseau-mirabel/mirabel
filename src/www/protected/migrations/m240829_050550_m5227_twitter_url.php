<?php

class m240829_050550_m5227_twitter_url extends \CDbMigration
{
	public function safeUp(): bool
	{
		foreach (["Editeur", "Titre"] as $table) {
			$sqls = [
				// https://twitter.com
				<<<SQL
				CREATE TEMPORARY TABLE Twitter
					SELECT
						id,
						JSON_SEARCH(liensJson, 'one', 'https://twitter.com%') AS path,
						JSON_VALUE(liensJson, json_unquote(JSON_SEARCH(liensJson, 'one', 'https://twitter.com%'))) AS url
					FROM $table
					WHERE JSON_SEARCH(liensJson, 'one', 'https://twitter.com%') IS NOT NULL
				SQL,

				// https://www.twitter.com
				<<<SQL
				INSERT INTO Twitter
					SELECT
						id,
						JSON_SEARCH(liensJson, 'one', 'https://www.twitter.com%') AS path,
						JSON_VALUE(liensJson, json_unquote(JSON_SEARCH(liensJson, 'one', 'https://www.twitter.com%'))) AS url
					FROM $table
					WHERE JSON_SEARCH(liensJson, 'one', 'https://www.twitter.com%') IS NOT NULL
				SQL,

				// http://twitter.com
				<<<SQL
				INSERT INTO Twitter
					SELECT
						id,
						JSON_SEARCH(liensJson, 'one', 'http://twitter.com%') AS path,
						JSON_VALUE(liensJson, json_unquote(JSON_SEARCH(liensJson, 'one', 'http://twitter.com%'))) AS url
					FROM $table
					WHERE JSON_SEARCH(liensJson, 'one', 'http://twitter.com%') IS NOT NULL
				SQL,

				// http://www.twitter.com
				<<<SQL
				INSERT INTO Twitter
					SELECT
						id,
						JSON_SEARCH(liensJson, 'one', 'http://www.twitter.com%') AS path,
						JSON_VALUE(liensJson, json_unquote(JSON_SEARCH(liensJson, 'one', 'http://www.twitter.com%'))) AS url
					FROM $table
					WHERE JSON_SEARCH(liensJson, 'one', 'http://www.twitter.com%') IS NOT NULL
				SQL,

				<<<SQL
				UPDATE $table
					JOIN Twitter ON $table.id = Twitter.id
				SET liensJson = json_replace(
					liensJson,
					json_unquote(Twitter.path),
					regexp_replace(Twitter.url, '^https?://(www\.)?twitter.com', 'https://x.com')
				)
				SQL,

				<<<SQL
				UPDATE Lien$table
				SET url = regexp_replace(url, '^https?://(www\.)?twitter.com', 'https://x.com')
				WHERE url LIKE '%twitter.com%'
				SQL,

				"DROP TEMPORARY TABLE Twitter",

				<<<'SQL'
					UPDATE Sourcelien SET url = 'x.com', urlRegex = '^https?://(?:www\\.|mobile\\.)?(?:twitter|x)\\.com/[\\w_@-]+$' WHERE id = 8 AND nom = 'X'
					SQL,
				"UPDATE LienEditeur SET domain = 'x.com' WHERE domain = 'twitter.com'",
				"UPDATE LienTitre SET domain = 'x.com' WHERE domain = 'twitter.com'",
			];
			foreach ($sqls as $sql) {
				$this->execute($sql);
			}
		}
		return true;
	}

	public function safeDown(): bool
	{
		echo "m240829_050550_m5227_twitter_url does not support migrating down.\n";
		return false;
	}
}
