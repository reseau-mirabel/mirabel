<?php

class m210625_062637_m4908_editeur_role extends \CDbMigration
{
	public function up(): bool
	{
		$this->addColumn("Editeur", "role", "varchar(255) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL AFTER statut");
		$this->update(
			"Config",
			[
				'type' => 'csv',
				'value' =>
					<<<EOTEXT
					associate_organisation;associate_organisation;"organisation associée";
					associate_organisation;client_organisation;"organisation bénéficiaire";
					commercial_publisher;commercial_publisher;"éditeur commercial";
					;copublisher;"co-éditeur";
					;copyright_holder;"détenteur des droits";
					governmental_publisher;governmental_publisher;"organisme gouvernemental";
					imprint;imprint;"marque commerciale";
					independent_journal;independent_journal;"collectif indépendant";
					;primary_copublisher;"co-éditeur principal";
					society_publisher;society_publisher;"société académique";
					university_research;university_publisher;"laboratoire de recherche";
					university_press;university_publisher;"presses universitaires publiques";
					university_publisher;university_publisher;"université";
					EOTEXT,
				'description' =>
					<<<EOHTML
					<p>
					Liste des valeurs possibles pour décrire le <strong>rôle d'un éditeur sur un titre</strong>.
					Ces valeurs sont utilisées dans le formulaire d'éditeur, dans celui de la relation titre-éditeur,
					et pour la conversion en français des données de Sherpa-Romeo.
					</p>
					<ul>
					<li>La première colonne contient les noms internes autorisés pour <code>Editeur.role</code> et stockés en base de données. Elle peut être vide.</li>
					<li>
						La seconde colonne contient les valeurs correspondantes attendues par Sherpa-Romeo qui sont également les seules autorisées pour le stockage direct dans <code>Titre_Editeur.role</code>.
						À une valeur <code>Titre_Editeur.role</code> peut correspondre une valeur <code>Editeur.role</code> nulle (cas de "copublisher" et de "primary_copublisher").
						Plusieurs valeurs <code>Editeur.role</code> peuvent correspondre à une valeur <code>Titre_Editeur.role</code> identique.
					</li>
					<li>
						La troisième colonne donne les correspondances en français, affichées dans les pages web.
						Si une valeur de Sherpa-Romeo a plusieurs traductions, celle de la dernière ligne sera appliquée.
					</li>
					<li>La quatrième colonne (faculative) contient un texte d'aide au format HTML.</li>
					</ul>
					<p>
					Si une ligne est retirée (suppression d'une valeur dans les 2 premières colonnes),
					les enregistrements déjà présents ne seront pas modifiés,
					mais ils afficheront la valeur brute, et non sa correspondance en français (qui n'existe plus).
					</p>
					EOHTML
			],
			"`key` = 'sherpa.publisher_role'"
		);
		$this->insert("Hint", [
			'model' => "Editeur",
			'attribute' => 'role',
			'name' => "Fonction éditoriale",
			'description' => "La fonction de l'éditeur sur ses titres. Ce rôle pourrra être affiné titre par titre.",
			'hdateModif' => time(),
		]);
		return true;
	}

	public function down(): bool
	{
		return false;
	}
}
