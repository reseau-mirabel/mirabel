<?php

class m210526_105740_config_contact_visible extends \CDbMigration
{
	public function safeUp()
	{
		$this->insert('Config', [
			'key' => 'contact.visible.email',
			'value' => '',
			'type' => 'string',
			'category' => 'contact',
			'description' => "Adresse de contact visible dans des liens <code>mailto:</code>. "
				. "Le message sera émis depuis la messagerie de l'utilisateur, alors que <em>contact.email</em> est utilisé pour les messages émis du serveur de Mir@bel.",
			'createdOn' => time(),
		]);
		return true;
	}

	public function safeDown()
	{
		$this->delete('Config', "`key` = 'contact.visible.email'");
		return true;
	}
}
