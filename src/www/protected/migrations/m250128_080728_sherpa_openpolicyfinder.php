<?php

class m250128_080728_sherpa_openpolicyfinder extends \CDbMigration
{
	public function up(): bool
	{
		$source = Sourcelien::model()->findByAttributes(['nomcourt' => 'romeo']);
		if ($source instanceof Sourcelien) {
			$source->nom = "Open policy finder";
			$source->nomcourt = 'openpolicyfinder';
			$source->nomlong = "Open policy finder (anciennement Sherpa Romeo) : Politique des éditeurs en matière de droits, de dépôt et d'auto-archivage";
			$source->url = 'openpolicyfinder.jisc.ac.uk'; // Domaine
			$source->urlRegex = '^https://openpolicyfinder\.jisc\.ac\.uk/'; // Détection des URLs non-sourcées
			$source->save();
		}
		$this->execute(<<<SQL
			UPDATE LienTitre
			SET url = replace(url, 'v2.sherpa.ac.uk', 'openpolicyfinder.jisc.ac.uk'),
				domain = 'openpolicyfinder.jisc.ac.uk'
			WHERE url LIKE 'https://v2.sherpa.ac.uk%'
			SQL
		);
		$this->execute(<<<SQL
			UPDATE Titre
			SET liensJson = replace(liensJson, 'https://v2.sherpa.ac.uk/', 'https://openpolicyfinder.jisc.ac.uk/')
			WHERE liensJson LIKE '%https://v2.sherpa.ac.uk%'
			SQL
		);
		return true;
	}

	public function down(): bool
	{
		echo "m250128_080728_sherpa_openpolicyfinder does not support migrating down.\n";
		return false;
	}
}
