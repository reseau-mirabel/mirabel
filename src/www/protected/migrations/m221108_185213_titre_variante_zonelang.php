<?php

class m221108_185213_titre_variante_zonelang extends \CDbMigration
{
	public function up(): bool
	{
		$this->addColumn("TitreVariante", "ppn", "VARCHAR(9) NOT NULL COLLATE ascii_bin DEFAULT '' AFTER titreId");
		$this->addColumn("TitreVariante", "zoneLang", "VARCHAR(5) NOT NULL COLLATE ascii_bin DEFAULT '' AFTER zoneSudoc");
		return true;
	}

	public function down(): bool
	{
		$this->dropColumn("TitreVariante", "ppn");
		$this->dropColumn("TitreVariante", "zoneLang");
		return true;
	}
}
