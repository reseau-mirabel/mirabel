<?php

class m230117_091810_m2533 extends \CDbMigration
{
	public function up(): bool
	{
		$this->alterColumn("Cms", "content", "mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''");
		return true;
	}

	public function down(): bool
	{
		return true;
	}
}
