<?php

class m240225_193222_cms_grappeintro extends \CDbMigration
{
	public function up(): bool
	{
		$this->insert('Cms', [
			'name' => 'grappe-intro',
			'singlePage' => 0,
			'pageTitle' => '',
			'type' => 'html',
			'content' => <<<'EOHTML'
				<!-- Ce bloc s'affiche sous le titre de la page /grappe/index. -->
				<p>
					Chaque grappe dans Mir@bel est un ensemble de revues, ou de titres de revues.
				</p>
				EOHTML,
			'private' => 0,
			'hdateCreat' => time(),
			'hdateModif' => 0,
		]);
		return true;
	}

	public function down(): bool
	{
		$this->delete('Cms', "name = 'grappe-intro' AND singlePage = 0");
		return true;
	}
}
