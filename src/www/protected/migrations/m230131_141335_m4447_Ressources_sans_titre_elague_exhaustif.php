<?php

class m230131_141335_m4447_Ressources_sans_titre_elague_exhaustif extends \CDbMigration
{
	public function up(): bool
	{
		if (defined('YII_TARGET') && YII_TARGET === 'test') {
			return true;
		}
		$sql = <<<EOSQL
			UPDATE Ressource
			SET exhaustif=2 WHERE id IN (
				SELECT R.id
				FROM Ressource R
					LEFT JOIN Service S ON R.id = S.ressourceId
				WHERE exhaustif = 1 AND titreId IS NULL
			)
			EOSQL;
		$this->execute($sql);

		return true;
	}

	public function down(): bool
	{
		echo "m230131_141335_m4447_Ressources_sans_titre_elague_exhaustif does not support migrating down.\n";
		return false;
	}
}
