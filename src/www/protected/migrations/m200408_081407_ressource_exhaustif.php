<?php

class m200408_081407_ressource_exhaustif extends CDbMigration
{
	/**
	 * Applies this migration.
	 */
	public function up()
	{
		$this->addColumn("Ressource", "exhaustif", "TINYINT UNSIGNED NOT NULL DEFAULT 2 COMMENT '0:false, 1:true, 2:unknown' AFTER indexation");
		$this->addColumn("Collection", "exhaustif", "TINYINT UNSIGNED NOT NULL DEFAULT 2 COMMENT '0:false, 1:true, 2:unknown' AFTER importee");
		$this->insertMultiple(
			"Hint",
			[
				['model' => 'Collection', 'attribute' => 'exhaustif', 'name' => 'Exhaustif', 'description' => "", 'hdateModif' => time()],
				['model' => 'Ressource', 'attribute' => 'exhaustif', 'name' => 'Exhaustif', 'description' => "", 'hdateModif' => time()],
			]
		);
	}

	/**
	 * Reverts this migration.
	 *
	 * @return bool False if it failed.
	 */
	public function down()
	{
		$this->dropColumn("Ressource", "exhaustif");
		$this->dropColumn("Collection", "exhaustif");
		return true;
	}
}
