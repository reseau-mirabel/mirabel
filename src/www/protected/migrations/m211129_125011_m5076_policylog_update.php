<?php

class m211129_125011_m5076_policylog_update extends \CDbMigration
{
	public function up(): bool
	{
		if (!$this->getDbConnection()->getSchema()->getTable("PolitiqueLog")->getColumn("publisher_policy")) {
			$this->addColumn(
				"PolitiqueLog",
				"publisher_policy",
				"longtext CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '' COMMENT 'JSON object, cf Sherpa API'"
			);
		}
		return true;
	}

	public function down(): bool
	{
		$this->dropColumn("PolitiqueLog", "publisher_policy");
		return true;
	}
}
