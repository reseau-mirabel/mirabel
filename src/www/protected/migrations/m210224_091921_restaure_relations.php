<?php

class m210224_091921_restaure_relations extends \CDbMigration
{
	public function up(): bool
	{
		// Abonnement
		$this->deleteOrphans("Abonnement", 'ressource');
		$this->dropIndex('abonnement_ressource_fk', 'Abonnement');
		$this->dropIndex('abonnement_collection_fk', 'Abonnement');
		$this->addForeignKey('fk_Abonnement_Collection', 'Abonnement', 'collectionId', 'Collection', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_Abonnement_Partenaire', 'Abonnement', 'partenaireId', 'Partenaire', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_Abonnement_Ressource', 'Abonnement', 'ressourceId', 'Ressource', 'id', 'CASCADE', 'CASCADE');

		// CategorieAlias_Titre
		$this->deleteOrphans("CategorieAlias_Titre", 'titre');
		$this->dropIndex('categoryalias_titre_titre_fk', 'CategorieAlias_Titre');
		$this->dropIndex('categoriealias_titre_utilisateur_fk', 'CategorieAlias_Titre');
		$this->addForeignKey('fk_categoriealiastitre_categoriealias', 'CategorieAlias_Titre', 'categorieAliasId', 'CategorieAlias', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_categoriealiastitre_utilisateur', 'CategorieAlias_Titre', 'modifPar', 'Utilisateur', 'id', 'SET NULL', 'CASCADE');
		$this->addForeignKey('fk_categoryaliastitre_titre', 'CategorieAlias_Titre', 'titreId', 'Titre', 'id', 'CASCADE', 'CASCADE');

		// CategorieAlias
		$this->dropIndex('categoriealias_utilisateur_fk', 'CategorieAlias');
		$this->dropIndex('categoriealias_categorie_fk', 'CategorieAlias');
		$this->dropIndex('categoriealias_vocabulaire_fk', 'CategorieAlias');
		$this->addForeignKey('fk_categoriealias_categorie', 'CategorieAlias', 'categorieId', 'Categorie', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_categoriealias_utilisateur', 'CategorieAlias', 'modifPar', 'Utilisateur', 'id', 'SET NULL', 'CASCADE');
		$this->addForeignKey('fk_categoriealias_vocabulaire', 'CategorieAlias', 'vocabulaireId', 'Vocabulaire', 'id', 'CASCADE', 'CASCADE');

		// Categorie_Revue
		$this->deleteOrphans("Categorie_Revue", 'revue');
		$this->dropIndex('categorie_revue_utilisateur_fk', 'Categorie_Revue');
		$this->dropIndex('categorie_revue_revue_fk', 'Categorie_Revue');
		$this->addForeignKey('fk_categorierevue_categorie', 'Categorie_Revue', 'categorieId', 'Categorie', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_categorierevue_revue', 'Categorie_Revue', 'revueId', 'Revue', 'id', 'CASCADE', 'CASCADE');
		$this->alterColumn("Categorie_Revue", "modifPar", "int(10) unsigned DEFAULT NULL");
		$this->addForeignKey('fk_categorierevue_utilisateur', 'Categorie_Revue', 'modifPar', 'Utilisateur', 'id', 'SET NULL', 'CASCADE');

		// Categorie
		$this->dropIndex('categorie_parent_fk', 'Categorie');
		$this->dropIndex('categorie_utilisateur_fk', 'Categorie');
		$this->addForeignKey('fk_categorie_parent', 'Categorie', 'parentId', 'Categorie', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_categorie_utilisateur', 'Categorie', 'modifPar', 'Utilisateur', 'id', 'SET NULL', 'CASCADE');

		// Cms
		$this->dropIndex('partenaireId', 'Cms');
		$this->addForeignKey('fk_Cms_Partenaire', 'Cms', 'partenaireId', 'Partenaire', 'id', 'CASCADE', 'CASCADE');

		// Collection
		$this->dropIndex('ressourceId', 'Collection');
		$this->dropIndex('fk_Collection_Ressource1', 'Collection');
		$this->addForeignKey('fk_Collection_Ressource', 'Collection', 'ressourceId', 'Ressource', 'id', 'CASCADE', 'CASCADE');

		// Editeur
		$this->dropIndex('fk_editeur_pays', 'Editeur');
		$this->addForeignKey('fk_Editeur_Pays', 'Editeur', 'paysId', 'Pays', 'id', 'SET NULL', 'CASCADE');

		// Identification
		$this->deleteOrphans("Identification", 'titre');
		$this->dropIndex('fk_Titre_Ressource_Titre1', 'Identification');
		$this->dropIndex('fk_Titre_Ressource_Ressource1', 'Identification');
		$this->addForeignKey('fk_Identification_Ressource', 'Identification', 'ressourceId', 'Ressource', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_Identification_Titre', 'Identification', 'titreId', 'Titre', 'id', 'CASCADE', 'CASCADE');

		// Intervention
		$this->dropIndex('fk_Intervention_Ressource1', 'Intervention');
		$this->dropIndex('fk_Intervention_Titre1', 'Intervention');
		$this->dropIndex('fk_Intervention_Editeur1', 'Intervention');
		$this->dropIndex('fk_Intervention_Revue1', 'Intervention');
		foreach (['editeur', 'ressource', 'revue', 'titre'] as $source) {
			$this->cleanupIntervention($source);
		}
		$this->addForeignKey('fk_Intervention_Editeur', 'Intervention', 'editeurId', 'Editeur', 'id', 'SET NULL', 'CASCADE');
		$this->addForeignKey('fk_Intervention_Ressource', 'Intervention', 'ressourceId', 'Ressource', 'id', 'SET NULL', 'CASCADE');
		$this->addForeignKey('fk_Intervention_Revue', 'Intervention', 'revueId', 'Revue', 'id', 'SET NULL', 'CASCADE');
		$this->addForeignKey('fk_Intervention_Titre', 'Intervention', 'titreId', 'Titre', 'id', 'SET NULL', 'CASCADE');

		// Partenaire
		$this->addForeignKey('fk_editeur_partenaire', 'Partenaire', 'editeurId', 'Editeur', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_partenaire_pays', 'Partenaire', 'paysId', 'Pays', 'id', 'SET NULL', 'CASCADE');

		// Partenaire_Titre
		$this->dropIndex('fk_Partenaire_Titre_Partenaire1', 'Partenaire_Titre');
		$this->dropIndex('fk_Partenaire_Titre_Titre1', 'Partenaire_Titre');
		$this->addForeignKey('fk_PartenaireTitre_Partenaire', 'Partenaire_Titre', 'partenaireId', 'Partenaire', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_PartenaireTitre_Titre', 'Partenaire_Titre', 'titreId', 'Titre', 'id', 'CASCADE', 'CASCADE');

		// Service
		$this->dropIndex('fk_Service_Ressource', 'Service');
		$this->dropIndex('fk_Service_Titre1', 'Service');
		$this->addForeignKey('fk_Service_Ressource', 'Service', 'ressourceId', 'Ressource', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_Service_Titre', 'Service', 'titreId', 'Titre', 'id', 'CASCADE', 'CASCADE');

		// Service_Collection
		$this->deleteOrphans("Service_Collection", 'collection');
		$this->deleteOrphans("Service_Collection", 'service');
		$this->dropIndex('fkservicecollectioncollection', 'Service_Collection');
		$this->addForeignKey('fk_servicecollection_collection', 'Service_Collection', 'collectionId', 'Collection', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_servicecollection_service', 'Service_Collection', 'serviceId', 'Service', 'id', 'CASCADE', 'CASCADE');

		// Suivi
		$this->dropIndex('fk_Droits_Partenaire1', 'Suivi');
		$this->addForeignKey('fk_Suivi_Partenaire', 'Suivi', 'partenaireId', 'Partenaire', 'id', 'CASCADE', 'CASCADE');

		// Titre
		$this->dropIndex('fk_Titre_Revue1', 'Titre');
		$this->dropIndex('fk_Titre_Titre1', 'Titre');
		$this->addForeignKey('fk_Titre_Revue', 'Titre', 'revueId', 'Revue', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_Titre_Titre', 'Titre', 'obsoletePar', 'Titre', 'id', null, 'CASCADE');

		// Utilisateur
		$this->dropIndex('fk_Utilisateur_Partenaire1', 'Utilisateur');
		$this->addForeignKey('fk_Utilisateur_Partenaire', 'Utilisateur', 'partenaireId', 'Partenaire', 'id', 'CASCADE', 'CASCADE');

		return true;
	}

	public function down(): bool
	{
		return false;
	}

	private function deleteOrphans(string $mainTable, string $source): void
	{
		$field = "{$source}Id";
		$table = ucfirst($source);
		$ids = $this->dbConnection
			->createCommand("SELECT x.$field FROM `$mainTable` x LEFT JOIN $table t ON x.$field = t.id WHERE x.$field IS NOT NULL AND t.id IS NULL")
			->queryColumn();
		$this->delete($mainTable, "$field IN (" . join(',', $ids) . ")");
	}

	private function cleanupIntervention(string $source): void
	{
		$field = "{$source}Id";
		$table = ucfirst($source);
		$ids = $this->dbConnection
			->createCommand("SELECT x.$field FROM Intervention x LEFT JOIN $table t ON x.$field = t.id WHERE x.$field IS NOT NULL AND t.id IS NULL")
			->queryColumn();
		$this->update("Intervention", [$field => null], "$field IN (" . join(',', $ids) . ")");
	}
}
