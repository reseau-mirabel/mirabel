<?php

class m230327_134314_Sourceattribut_visibilite extends \CDbMigration
{
	public function up(): bool
	{
		$this->addColumn('Sourceattribut', 'visibilite', "tinyint NOT NULL DEFAULT 0");
		return true;
	}

	public function down(): bool
	{
		$this->dropColumn('Sourceattribut', 'visibilite');
		return true;
	}
}
