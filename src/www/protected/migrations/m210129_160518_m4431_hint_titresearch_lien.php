<?php

class m210129_160518_m4431_hint_titresearch_lien extends \CDbMigration
{
	public function up(): bool
	{
		$this->insert(
			"Hint",
			[
				'model' => 'SearchTitre',
				'attribute' => 'lien',
				'name' => 'Liens',
				'description' => '',
				'hdateModif' => time(),
			]
		);
		return true;
	}

	public function down(): bool
	{
		echo "m210129_160518_m4431_hint_titresearch_lien does not support migrating down.\n";
		return false;
	}
}
