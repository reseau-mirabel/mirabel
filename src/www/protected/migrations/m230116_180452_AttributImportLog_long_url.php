<?php

class m230116_180452_AttributImportLog_long_url extends \CDbMigration
{
	public function up(): bool
	{
		$this->alterColumn(
			'AttributImportLog',
			'fileName',
			'TEXT'
		);

		$this->alterColumn(
			'AttributImportLog',
			'url',
			'TEXT'
		);
		return true;
	}

	public function down(): bool
	{
		echo "m230116_180452_AttributImportLog_long_url does not support migrating down.\n";
		return false;
	}
}
