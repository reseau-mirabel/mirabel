<?php

class m211001_074551_m5003_user_note extends \CDbMigration
{
	public function up(): bool
	{
		$this->addColumn("Utilisateur", "notes", "TEXT NOT NULL DEFAULT '' COMMENT 'hidden note written by admins' AFTER nomComplet");
		return true;
	}

	public function down(): bool
	{
		$this->dropColumn("Utilisateur", "notes");
		return true;
	}
}
