<?php

class m230205_160536_Redirections extends \CDbMigration
{
	public function up(): bool
	{
		if (!$this->getDbConnection()->getSchema()->getTable("RedirectionEditeur")) {
			$this->createTable(
				'RedirectionEditeur',
				[
					'sourceId' => "INT unsigned NOT NULL COMMENT 'éditeur fusionné' ",
					'cibleId' => "INT unsigned NOT NULL COMMENT 'éditeur destinataire' ",
					'hdateModif' => "BIGINT NOT NULL DEFAULT 0",
				]
			);
			$this->addPrimaryKey('pk_RedirectionEditeur', 'RedirectionEditeur', ['sourceId', 'cibleId']);
			$this->addForeignKey(
				'fk_RedirectionEditeur_cible',
				'RedirectionEditeur',
				'cibleId',
				'Editeur',
				'id'
			);
		}

		if (!$this->getDbConnection()->getSchema()->getTable("RedirectionRevue")) {
			$this->createTable(
				'RedirectionRevue',
				[
					'sourceId' => "INT unsigned NOT NULL COMMENT 'revue fusionnée' ",
					'cibleId' => "INT unsigned NOT NULL COMMENT 'revue destinataire' ",
					'hdateModif' => "BIGINT NOT NULL DEFAULT 0",
				]
			);
			$this->addPrimaryKey('pk_RedirectionRevue', 'RedirectionRevue', ['sourceId', 'cibleId']);
			$this->addForeignKey(
				'fk_RedirectionRevue_cible',
				'RedirectionRevue',
				'cibleId',
				'Revue',
				'id'
			);
		}

		if (defined('YII_TARGET') && YII_TARGET === 'test') {
			$this->execute('TRUNCATE TABLE RedirectionRevue');
			$this->insert('RedirectionRevue', ['sourceId' => 7, 'cibleId' => 20, 'hdateModif' => 1675621822]); // Recueil Dalloz
			$this->execute('TRUNCATE TABLE RedirectionEditeur');
			$this->insert('RedirectionEditeur', ['sourceId' => 3, 'cibleId' => 19, 'hdateModif' => 1675621822]); // Dalloz
		}

		return true;
	}

	public function down(): bool
	{
		if ($this->getDbConnection()->getSchema()->getTable("RedirectionEditeur")) {
			$this->dropTable('RedirectionEditeur');
		}
		if ($this->getDbConnection()->getSchema()->getTable("RedirectionRevue")) {
			$this->dropTable('RedirectionRevue');
		}
		return true;
	}
}
