<?php

class m221202_180918_Config_bandeau extends \CDbMigration
{
	/**
	 * Applies this migration.
	 *
	 * @return bool False if it failed.
	 */
	public function up(): bool
	{
		if (Config::read('bandeau.invite') !== null) {
			return true;
		}

		$this->insert("Config", [
			'key' => 'bandeau.invite',
			'value' => "0",
			'type' => 'boolean',
			'category' => 'bandeau',
			'description' => "Affiche le bandeau pour les invités (utilisateurs non authentifiés).",
			'required' => 0,
			'createdOn' => time(),
		]);

		$this->insert("Config", [
			'key' => 'bandeau.authentifie',
			'value' => "0",
			'type' => 'boolean',
			'category' => 'bandeau',
			'description' => "Affiche le bandeau pour les utilisateurs authentifiés.",
			'required' => 0,
			'createdOn' => time(),
		]);

		$this->insert("Config", [
			'key' => 'bandeau.texte',
			'value' => "",
			'type' => 'html',
			'category' => 'bandeau',
			'description' => "Texte en HTML du message de bandeau.",
			'required' => 0,
			'createdOn' => time(),
		]);

		return true;
	}

	/**
	 * Reverts this migration.
	 *
	 * @return bool False if it failed.
	 */
	public function down(): bool
	{
		$this->delete('Config', ['category' => 'bandeau']);
		return true;
	}
}
