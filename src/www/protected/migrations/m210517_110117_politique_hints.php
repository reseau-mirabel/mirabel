<?php

class m210517_110117_politique_hints extends \CDbMigration
{
	public function safeUp()
	{
		$now = time();
		$this->insertMultiple("Hint", [
			[
				'model' => 'Politique',
				'attribute' => 'copyright_owner',
				'name' => 'Détenteurs des droits',
				'description' => '',
				'hdateModif' => $now,
			],
			[
				'model' => 'Politique',
				'attribute' => 'fee',
				'name' => "Frais d'option OA",
				'description' => '',
				'hdateModif' => $now,
			],
			[
				'model' => 'Politique',
				'attribute' => 'embargo',
				'name' => "Embargo",
				'description' => '',
				'hdateModif' => $now,
			],
			[
				'model' => 'Politique',
				'attribute' => 'policy-name',
				'name' => "Nom de la politique",
				'description' => '',
				'hdateModif' => $now,
			],
			[
				'model' => 'Politique',
				'attribute' => 'policy-urls',
				'name' => "URLs de la politique",
				'description' => '',
				'hdateModif' => $now,
			],
		]);
		return true;
	}

	public function safeDown()
	{
		$this->delete("Hint", "model = 'Politique'");
		return true;
	}
}
