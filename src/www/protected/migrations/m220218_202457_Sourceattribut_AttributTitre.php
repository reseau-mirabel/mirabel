<?php

class m220218_202457_Sourceattribut_AttributTitre extends \CDbMigration
{
	public function up(): bool
	{
		if ($this->getDbConnection()->getSchema()->getTable("Sourceattribut")) {
			return true;
		}

		$this->createTable(
			'Sourceattribut',
			[
				'id' => "INT unsigned NOT NULL PRIMARY KEY AUTO_INCREMENT",
				'nom' => "VARCHAR(255) COLLATE utf8mb4_bin NOT NULL",
				'identifiant' => "VARCHAR(32) COLLATE ascii_bin NOT NULL",
				'description' => "VARCHAR(255) COLLATE utf8mb4_bin NOT NULL",
				'sourcelienId' => "INT unsigned NULL DEFAULT NULL COMMENT 'référence optionnelle' ",
			],
			"COMMENT 'Attributs personnalisés sur les titres'"
		);
		$this->addForeignKey(
			'fk_Sourceattribut_sourcelienId',
			'Sourceattribut',
			'sourcelienId',
			'Sourcelien',
			'id',
			'SET NULL',
			'CASCADE'
		);

		$this->createTable(
			'AttributTitre',
			[
				'titreId' => "INT unsigned NOT NULL",
				'sourceattributId' => "INT unsigned NOT NULL",
				'valeur' => "TEXT COLLATE utf8mb4_bin NOT NULL",
				'hdate' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
			],
			"COMMENT 'Valeurs des attributs personnalisés sur les titres'"
		);
		$this->addPrimaryKey('pk_AttributTitre', 'AttributTitre', ['titreId', 'sourceattributId']);
		$this->addForeignKey(
			'fk_AttributTitre_Titre',
			'AttributTitre',
			'titreId',
			'Titre',
			'id',
			'CASCADE',
			'CASCADE'
		);
		$this->addForeignKey(
			'fk_AttributTitre_Sourceattribut',
			'AttributTitre',
			'sourceattributId',
			'Sourceattribut',
			'id',
			'CASCADE',
			'CASCADE'
		);

		$this->createTable(
			'AttributImportLog',
			[
				'id' => "INT unsigned NOT NULL PRIMARY KEY AUTO_INCREMENT",
				'sourceattributId' => "INT unsigned NULL COMMENT 'valeur NULL ssi import non finalisé'",
				'logtime' => "BIGINT NOT NULL DEFAULT 0 COMMENT 'timestamp'",
				'fileName' => "VARCHAR(255) COLLATE utf8mb4_bin NOT NULL COMMENT 'nom du fichier original'",
				'url' => "VARBINARY(512) NOT NULL COMMENT 'url éventuelle'",
				'fileKey' => "VARCHAR(255) COLLATE ascii_bin NOT NULL COMMENT 'clé de stockage du fichier-tableur'",
				'reportKey' => "VARCHAR(255) COLLATE ascii_bin NOT NULL DEFAULT '' COMMENT 'clé de stockage du rapport'",
				'userId' => "INT unsigned NULL",
			],
			"COMMENT 'Historique des imports de valeurs dans des attributs de titres'"
		);
		$this->addForeignKey(
			'fk_AttributLog_Sourceattribut',
			'AttributImportLog',
			'sourceattributId',
			'Sourceattribut',
			'id',
			'CASCADE',
			'CASCADE'
		);
		$this->addForeignKey('fk_LogAttribut_Utilisateur', 'AttributImportLog', 'userId', 'Utilisateur', 'id', 'SET NULL', 'CASCADE');

		if (YII_ENV === 'test') {
			$this->execute("INSERT IGNORE INTO Sourceattribut VALUES (1, 'attr de test', 'attr-test', '', 1)");
			$this->execute("INSERT IGNORE INTO Sourceattribut VALUES (2, 'encore', 'attr-encore', '', NULL)");
		}

		return true;
	}

	public function down(): bool
	{
		$this->dropTable('AttributTitre');
		$this->dropTable('AttributImportLog');
		$this->dropTable('Sourceattribut');
		return true;
	}
}
