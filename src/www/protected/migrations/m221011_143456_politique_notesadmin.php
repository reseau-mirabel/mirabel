<?php

class m221011_143456_politique_notesadmin extends \CDbMigration
{
	public function up(): bool
	{
		$this->addColumn("Politique", "notesAdmin", "TEXT COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'hidden comment written by admins'");
		return true;
	}

	public function down(): bool
	{
		$this->dropColumn("Politique", "notesAdmin");
		return true;
	}
}
