<?php

class m220413_072458_latindex_cleanup extends \CDbMigration
{
	public function up(): bool
	{
		$titres = Titre::model()->findAllBySql("SELECT t.* FROM `LienTitre` lt JOIN Titre t ON t.id =lt.titreId WHERE lt.url LIKE '%latindex%' AND lt.url NOT LIKE '%www.latindex.org%'");
		foreach ($titres as $titre) {
			/** @var Titre $titre */
			$liens = $titre->getLiens();
			foreach ($liens as $lien) {
				/** @var Lien $lien */
				if (strpos($lien->url, "latindex") !== false) {
					$lien->url = preg_replace('#https?://www\.latindex\.unam\.mx/#', 'https://www.latindex.org/', $lien->url);
				}
			}
			$titre->setLiens($liens);
			$titre->save(false);
		}
		return true;
	}

	public function down(): bool
	{
		return true;
	}
}
