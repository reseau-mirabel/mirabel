<?php

class m231010_131523_m5689_cms_twitter_description extends \CDbMigration
{
	public function up(): bool
	{

		$this->addColumn('Cms', 'dcdescription', "TEXT NOT NULL DEFAULT '' COMMENT 'dc:description and twitter:description' AFTER categorisation");
		return true;
	}

	public function down(): bool
	{
		$this->dropColumn('Cms', 'description');
		return true;
	}
}
