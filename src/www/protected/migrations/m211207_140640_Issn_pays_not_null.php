<?php

class m211207_140640_Issn_pays_not_null extends \CDbMigration
{
	public function up(): bool
	{
		$this->update(
			'Issn',
			[ 'pays' => '' ],
			"`pays` IS NULL"
		);

		$this->alterColumn(
			'Issn',
			'pays',
			"char(2) CHARACTER SET ascii COLLATE ascii_bin DEFAULT '' NOT NULL COMMENT 'ISO 3166-1 (2 letters)'"
		);
		return true;
	}

	public function down(): bool
	{
		echo "m211207_140640_Issn_pays_not_null does not support migrating down.\n";
		return false;
	}
}
