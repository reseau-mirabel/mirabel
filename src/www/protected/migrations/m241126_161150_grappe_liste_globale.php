<?php

class m241126_161150_grappe_liste_globale extends \CDbMigration
{
	public function up(): bool
	{
		Yii::app()->db->schema->refresh();
		$schema = $this->getDbConnection()->getSchema();
		if (defined('YII_TARGET') && YII_TARGET === 'test') {
			$this->execute("UPDATE Grappe SET publicite = 5");
		}

		// Partenaire_Grappe.partenaireId can be NULL
		$this->createIndex('u_partenaire_grappe', "Partenaire_Grappe", ['partenaireId', 'grappeId'], true);
		$this->dropPrimaryKey('PRIMARY', "Partenaire_Grappe");
		$this->alterColumn("Partenaire_Grappe", "partenaireId", "INT unsigned NULL COMMENT 'if NULL, selection for the global list. Else for the related Partenaire.'");

		// Remove Grappe.publicite
		if ($schema->getTable("Grappe")->getColumn('publicite')) {
			$this->execute(<<<SQL
				INSERT IGNORE INTO Partenaire_Grappe (partenaireId, grappeId, diffusion, pages, position, hdateCreation)
					SELECT NULL, id, diffusion, 1, 0, unix_timestamp()
					FROM Grappe
					WHERE diffusion = :d AND publicite = :p
				SQL,
				[':d' => Grappe::DIFFUSION_PUBLIC, ':p' => 5] // Grappe::PUBLICITE_MAXIMALE was 5
			);
			$this->dropColumn("Grappe", "publicite");
		}
		Yii::app()->db->schema->refresh();
		return true;
	}

	public function down(): bool
	{
		echo "m241126_161150_grappe_liste_globale does not support migrating down.\n";
		return false;
	}
}
