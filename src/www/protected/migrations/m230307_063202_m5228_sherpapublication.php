<?php

class m230307_063202_m5228_sherpapublication extends \CDbMigration
{
	public function up(): bool
	{
		if ($this->getDbConnection()->getSchema()->getTable("SherpaPublication") !== null) {
			return true;
		}

		$this->createTable("SherpaPublication", [
			'id' => "INT UNSIGNED PRIMARY KEY",
			'titreId' => "INT UNSIGNED NOT NULL",
			'content' => "JSON NOT NULL",
			'lastUpdate' => "BIGINT NOT NULL DEFAULT 0 COMMENT 'timestamp of the last update by Sherpa'",
			'lastFetch' => "BIGINT NOT NULL DEFAULT 0 COMMENT 'timestamp of our last fetch request'",
		]);
		$this->createIndex("spn_titre", "SherpaPublication", ["titreId"], true);
		$this->addForeignKey('spn_titre_fk', 'SherpaPublication', 'titreId', 'Titre', 'id', 'CASCADE', 'CASCADE');
		$this->loadFiles();

		$this->createTable("SherpaPublisher", [
			'id' => "INT UNSIGNED PRIMARY KEY",
			'editeurId' => "INT UNSIGNED NULL DEFAULT NULL",
			'content' => "JSON NOT NULL",
			'lastUpdate' => "BIGINT NOT NULL DEFAULT 0 COMMENT 'timestamp of the last update by Sherpa'",
			'lastFetch' => "BIGINT NOT NULL DEFAULT 0 COMMENT 'timestamp of our last fetch request'",
		]);
		$this->createIndex("spr_editeur", "SherpaPublisher", ["editeurId"], true);
		$this->addForeignKey('spr_editeur_fk', 'SherpaPublisher', 'editeurId', 'Editeur', 'id', 'SET NULL', 'SET NULL');

		if (defined('YII_TARGET') && YII_TARGET === 'test') {
			$this->insertMultiple("SherpaPublication", [
				[
					'id' => 14330,
					'titreId' => 28,
					'content' => <<<'EOJSON'
						{"type":"journal","id":14330,"publisher_policy":[{"permitted_oa":[{"additional_oa_fee_phrases":[{"language":"en","value":"no","phrase":"No"}],"additional_oa_fee":"no","conditions":["Published source must be acknowledged","Must link to publisher version"],"article_version_phrases":[{"phrase":"Submitted","value":"submitted","language":"en"},{"language":"en","value":"accepted","phrase":"Accepted"},{"language":"en","phrase":"Published","value":"published"}],"public_notes":["Publisher's version/PDF may be used, but is discouraged","Publisher preferred link to publisher version instead, rather than deposit of Publisher's version/PDF"],"article_version":["submitted","accepted","published"],"location":{"location":["authors_homepage","institutional_website"],"location_phrases":[{"language":"en","value":"authors_homepage","phrase":"Author's Homepage"},{"value":"institutional_website","phrase":"Institutional Website","language":"en"}]}}],"uri":"http://v2.sherpa.ac.uk/id/publisher_policy/645","open_access_prohibited_phrases":[{"phrase":"No","value":"no","language":"en"}],"publication_count":1,"internal_moniker":"Default Policy","urls":[{"url":"http://cybergeo.revues.org/5021","description":"Copyright Information"}],"open_access_prohibited":"no","id":645}],"issns":[{"issn":"1278-3366","type":"print","type_phrases":[{"value":"print","phrase":"Print","language":"en"}]}],"listed_in_doaj":"yes","publishers":[{"publisher":{"uri":"http://v2.sherpa.ac.uk/id/publisher/645","publication_count":1,"url":"http://cybergeo.revues.org/","country":"fr","name":[{"preferred_phrases":[{"phrase":"Name","value":"name","language":"en"}],"language_phrases":[{"value":"en","phrase":"English","language":"en"}],"language":"en","preferred":"name","name":"Cybergeo : European Journal of Geography"}],"country_phrases":[{"phrase":"France","value":"fr","language":"en"}],"id":645},"relationship_type":"independent_journal","relationship_type_phrases":[{"value":"independent_journal","phrase":"Independant Journal","language":"en"}]}],"title":[{"language_phrases":[{"phrase":"English","value":"en","language":"en"}],"title":"Cybergeo : European Journal of Geography","language":"en","acronym":"Cybergeo"}],"type_phrases":[{"phrase":"Journal","value":"journal","language":"en"}],"url":"http://cybergeo.revues.org/","listed_in_doaj_phrases":[{"phrase":"Yes","value":"yes","language":"en"}],"system_metadata":{"date_created":"2010-09-07 11:03:12","uri":"http://v2.sherpa.ac.uk/id/publication/14330","publicly_visible_phrases":[{"value":"yes","phrase":"Yes","language":"en"}],"publicly_visible":"yes","id":14330,"date_modified":"2020-01-29 15:27:57"}}
						EOJSON,
					'lastUpdate' => 0,
					'lastFetch' => time(),
				],
				[
					'id' => 14527,
					'titreId' => 1400,
					'content' => <<<'EOJSON'
						{"type_phrases":[{"phrase":"Journal","value":"journal","language":"en"}],"title":[{"language_phrases":[{"language":"en","value":"en","phrase":"English"}],"title":"Actes de la Recherche en Sciences Sociales","language":"en"}],"listed_in_doaj_phrases":[{"language":"en","value":"no","phrase":"No"}],"url":"http://www.seuil.com/fiche-ouvrage.php/?EAN=9782020628259","system_metadata":{"date_created":"2010-09-07 15:56:25","uri":"http://v2.sherpa.ac.uk/id/publication/14527","publicly_visible_phrases":[{"phrase":"Yes","value":"yes","language":"en"}],"publicly_visible":"yes","date_modified":"2020-01-29 15:28:00","id":14527},"listed_in_doaj":"no","publishers":[{"publisher":{"id":513,"country_phrases":[{"language":"en","phrase":"France","value":"fr"}],"country":"fr","name":[{"preferred":"name","name":"Editions du Seuil","preferred_phrases":[{"phrase":"Name","value":"name","language":"en"}],"language_phrases":[{"phrase":"English","value":"en","language":"en"}],"language":"en"}],"publication_count":5,"url":"http://www.seuil.com/","uri":"http://v2.sherpa.ac.uk/id/publisher/513"},"relationship_type":"commercial_publisher","relationship_type_phrases":[{"language":"en","phrase":"Commercial Publisher","value":"commercial_publisher"}]}],"type":"journal","id":14527,"publisher_policy":[{"internal_moniker":"Default Policy","id":513,"uri":"http://v2.sherpa.ac.uk/id/publisher_policy/513","permitted_oa":[{"article_version_phrases":[{"value":"submitted","phrase":"Submitted","language":"en"}],"article_version":["submitted"],"location":{"location":["any_website"],"location_phrases":[{"language":"en","value":"any_website","phrase":"Any Website"}]},"additional_oa_fee_phrases":[{"language":"en","phrase":"No","value":"no"}],"additional_oa_fee":"no"},{"additional_oa_fee_phrases":[{"phrase":"No","value":"no","language":"en"}],"additional_oa_fee":"no","prerequisites":{"prerequisites_phrases":[{"language":"en","phrase":"If Publisher Permission Granted","value":"requires_publisher_permission"}],"prerequisites":["requires_publisher_permission"]},"article_version":["accepted"],"article_version_phrases":[{"language":"en","phrase":"Accepted","value":"accepted"}],"location":{"location":["any_website"],"location_phrases":[{"language":"en","value":"any_website","phrase":"Any Website"}]}}],"open_access_prohibited":"no","open_access_prohibited_phrases":[{"language":"en","value":"no","phrase":"No"}],"publication_count":5}],"issns":[{"type":"print","issn":"0335-5322","type_phrases":[{"value":"print","phrase":"Print","language":"en"}]}]}
						EOJSON,
					'lastUpdate' => 0,
					'lastFetch' => time(),
				],
			]);
		}

		return true;
	}

	public function down(): bool
	{
		$this->dropTable("SherpaPublisher");
		$this->dropTable("SherpaPublication");
		return true;
	}

	private function loadFiles()
	{
		$files = glob(\Yii::getPathOfAlias('application.runtime.sherpa') . '/*.json');
		$m = [];
		foreach ($files as $file) {
			if (preg_match('#/(\d+)\.json$#', $file, $m)) {
				$titreId = (int) $m[1];
				$exists = $this->dbConnection->createCommand("SELECT 1 FROM Titre WHERE id = $titreId")->queryScalar();
				if (!$exists) {
					echo "Titre $titreId n'existe plus.\n";
					continue;
				}
				$content = file_get_contents($file);
				$decoded = json_decode($content, false);
				$publication = $decoded->items[0];
				$lastUpdate = $publication->system_metadata->date_modified ?? $publication->system_metadata->date_created ?? 0;
				try {
					$this->insert("SherpaPublication", [
						'id' => $publication->id,
						'titreId' => $titreId,
						'content' => json_encode($publication, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE),
						'lastUpdate' => $lastUpdate ? DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $lastUpdate)->getTimestamp() : 0,
						'lastFetch' => filemtime($file),
					]);
				} catch (\CDbException $e) {
					echo "Erreur pour id={$publication->id} : {$e->getMessage()}\n";
				}
				unlink($file);
			}
		}
	}
}
