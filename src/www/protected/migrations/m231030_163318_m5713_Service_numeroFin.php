<?php

class m231030_163318_m5713_Service_numeroFin extends \CDbMigration
{
	public function up(): bool
	{
		$this->alterColumn(
			'Service',
			'numeroDebut',
			"varchar(60) NOT NULL"
		);
		$this->alterColumn(
			'Service',
			'numeroFin',
			"varchar(60) NOT NULL"
		);
		return true;
	}

	public function down(): bool
	{
		echo "m231030_163318_m5713_Service_numeroFin does not support migrating down.\n";
		return false;
	}
}
