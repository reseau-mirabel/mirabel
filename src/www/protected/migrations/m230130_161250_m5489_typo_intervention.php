<?php

class m230130_161250_m5489_typo_intervention extends \CDbMigration
{
	public function up(): bool
	{
		$this->execute("UPDATE Intervention SET description = REPLACE(description, 'Suppresson', 'Suppression') WHERE description LIKE 'Suppresson %'");
		return true;
	}

	public function down(): bool
	{
		echo "m230130_161250_m5489_typo_intervention does not support migrating down.\n";
		return false;
	}
}
