<?php

class m211012_095113_bnfArk_champ_canonique extends \CDbMigration
{
	public function up(): bool
	{
		$sql = "UPDATE Issn SET bnfArk=REPLACE(bnfArk, 'https://catalogue.bnf.fr/ark:/12148/', '') "
			. "WHERE bnfArk LIKE 'https://catalogue.bnf.fr/ark:/12148/%'";
		$this->execute($sql);
		return true;
	}

	public function down(): bool
	{
		echo "m211012_095113_bnfArk_champ_canonique does not support migrating down.\n";
		return false;
	}
}
