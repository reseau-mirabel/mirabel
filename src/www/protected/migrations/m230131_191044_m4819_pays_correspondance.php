<?php

class m230131_191044_m4819_pays_correspondance extends \CDbMigration
{
	public function up(): bool
	{
		if (Config::read('pays.correspondance') === null) {
			$this->insert("Config", [
				'key' => 'pays.correspondance',
				'value' => <<<EOTEXT
					RE;FR
					GP;FR
					MQ;FR
					YT;FR
					GF;FR
					PF;FR
					EOTEXT,
				'type' => 'assoc',
				'category' => 'pays',
				'description' => <<<EOHTML
					<p>Table de corresponsance entre territoires (TOM) et pays pour les notices Sudoc.</p>
					EOHTML,
				'required' => 0,
				'createdOn' => time(),
			]);
		}
		return true;
	}

	public function down(): bool
	{
		$this->delete('Config', "`key` = 'pays.correspondance'");
		return true;
	}
}
