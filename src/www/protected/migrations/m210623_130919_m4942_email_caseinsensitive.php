<?php

class m210623_130919_m4942_email_caseinsensitive extends \CDbMigration
{
	public function up(): bool
	{
		$this->alterColumn("Utilisateur", "email", "varchar(100) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL DEFAULT ''");
		return true;
	}

	public function down(): bool
	{
		return true;
	}
}
