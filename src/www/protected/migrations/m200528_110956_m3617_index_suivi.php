<?php

class m200528_110956_m3617_index_suivi extends CDbMigration
{
	/**
	 * Applies this migration.
	 */
	public function up()
	{
		$this->createIndex("suivi_cible", "Suivi", ["cible", "cibleId"]);
	}

	/**
	 * Reverts this migration.
	 *
	 * @return bool False if it failed.
	 */
	public function down()
	{
		$this->dropIndex("suivi_cible", "Suivi");
		return true;
	}
}
