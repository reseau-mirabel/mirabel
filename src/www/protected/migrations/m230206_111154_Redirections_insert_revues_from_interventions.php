<?php

class m230206_111154_Redirections_insert_revues_from_interventions extends \CDbMigration
{
	public function up(): bool
	{
		$sql = <<<EOSQL
			SELECT ContenuJson
			FROM `Intervention`
			WHERE description LIKE '%Déplacement du titre %'
				AND ContenuJson LIKE '%"model":"Revue","operation":"delete"%';
			EOSQL;
		$contenuJson = Yii::app()->db->createCommand($sql)->queryColumn();

		$redirection = [];
		foreach ($contenuJson as $item) {
			$detail = json_decode($item);
			$modif = $detail->content[0];
			$redirection[$modif->before->revueId] = $modif->after->revueId;
		}
		echo count($redirection) . " redirections de Revue.";

		$hdate = time();
		foreach ($redirection as $source => $cible) {
			$this->insert('RedirectionRevue', ['sourceId' => $source, 'cibleId' => $cible, 'hdateModif' => $hdate]);
		}
		return true;
	}

	public function down(): bool
	{
		$this->execute('TRUNCATE TABLE RedirectionRevue');
		return true;
	}
}
