<?php

class m210623_171332_m4766_editeur_idref_unique extends \CDbMigration
{
	public function up(): bool
	{
		$this->createIndex('editeur_idref_unique', 'Editeur', 'idref', true);
		return true;
	}

	public function down(): bool
	{
		$this->dropIndex('editeur_idref_unique', 'Editeur');
		return true;
	}
}
