<?php

class m250228_080713_m5806_fichiers extends \CDbMigration
{
	public function up(): bool
	{
		if (!file_exists('composer.lock')) {
			echo "Cette commande doit être lancée depuis la racine du projet.\n";
			return false;
		}
		$pwd = realpath(getcwd());
		if (!defined('DATA_DIR')) {
			echo "Ajouter une ligne :\ndefined('DATA_DIR') or define('DATA_DIR', '$pwd/data');\n au début de src/www/protected/config.php\n  et relancer la migration.\n";
			return false;
		}
		if (str_ends_with(DATA_DIR, '/')) {
			echo "Retirer le '/' final de la constante DATA_DIR dans src/www/protected/config.php\n  et relancer la migration.\n";
			return false;
		}
		if (!is_dir(DATA_DIR)) {
			mkdir(DATA_DIR, 0777);
		}

		self::migrateLogs();
		self::migrateCache();
		self::mv('protected/runtime/sessions', 'sessions');

		self::migrateWeb();
		self::migrateUploads();

		return true;
	}

	public function down(): bool
	{
		echo "m250228_080713_m5806_fichiers does not support migrating down.\n";
		return false;
	}

	/**
	 * Move the log files under "data/logs/".
	 *
	 * This method must be called *before* migrateCache().
	 */
	private static function migrateLogs(): void
	{
		// Check the actual config.
		/** @var CLogRouter $log */
		$log = Yii::app()->getComponent('log');
		$dest = DATA_DIR . '/logs';
		foreach ($log->routes as $route) {
			/** @var CLogRoute $route */
			if ($route instanceof CFileLogRoute && $route->getLogPath() !== $dest) {
				throw new \Exception("Des logs sont stockés hors de '$dest/'. Corriger config/local.php.");
			}
		}

		// Move the log files.
		foreach (glob("src/www/protected/runtime/*.log*") as $f) {
			if (!rename($f, "$dest/" . basename($f))) {
				throw new \Exception("Échec du déplacement de $f dans $dest/");
			}
		}
	}

	/**
	 * Move the cache under data/.
	 *
	 * Yii::app()->runtimePath was "src/www/protected/runtime".
	 * Yii::app()->runtimePath is now "data/cache/yii".
	 */
	private static function migrateCache(): void
	{
		if (glob("src/www/protected/runtime/*.log")) {
			throw new \Exception("Cannot move cache when there arer logs under runtime/");
		}
		if (is_dir(DATA_DIR . '/cache/yii')) {
			chmod(DATA_DIR . '/cache', 0777);
			chmod(DATA_DIR . '/cache/yii', 0777);
		} else {
			self::mkdir('/cache');
			self::mkdir('/cache/yii');
		}
		self::mv('protected/runtime/cache', 'cache/yii/cache');
		self::mv('protected/runtime/feeds', 'cache/feeds');
		self::mv('protected/runtime/filestore', 'filestore');
		self::mv('protected/runtime/kbart', 'cache/yii/kbart');

		if (is_dir('src/www/assets')) {
			self::mv('assets', 'cache/assets');
		}
	}

	private static function migrateUploads(): void
	{
		self::mv('protected/data/upload/editeurs', 'upload/editeurs');
		self::mv('protected/data/upload/conventions', 'upload/conventions');
		self::mv('protected/data/upload', 'upload/prive');
		self::mv('images/videos', 'upload/videos');
	}

	private static function migrateWeb(): void
	{
		self::mkdir('/images');
		self::mkdir('/public');
		self::mkdir('/public/images');

		// Fichiers inutilisés depuis 2014 dbeac0784ac8d369107b ?
		foreach (glob("src/www/images/bandeau*") as $f) {
			unlink($f);
		}

		self::mv('images/editeurs/raw', 'images/editeurs');
		self::mv('images/editeurs', 'public/images/editeurs');

		self::mv('images/logos/h55', 'public/images/partenaires');
		self::mv('images/logos', 'images/partenaires');

		self::mv('images/ressources-logos/h55', 'public/images/ressources');
		self::mv('images/ressources-logos', 'images/ressources');

		self::mv('images/titres-couvertures/raw', 'images/titres-couvertures');
		self::mv('images/titres-couvertures', 'public/images/titres-couvertures');

		self::mv('public/sudoc-import', 'public/sudoc-import');
		self::mv('public', 'upload/public');
	}

	private static function mkdir(string $subdir): void
	{
		$dir = DATA_DIR . '/' . ltrim($subdir, '/');
		if (is_dir($dir)) {
			return;
		}
		if (!mkdir($dir, 0777)) {
			throw new \Exception("Échec de mkdir('$dir').");
		}
	}

	/**
	 * Move a directory from src/www/... to data/...
	 *
	 * If the source is missing, an empty target directory will be created.
	 */
	private static function mv($src, $dest): void
	{
		$to = DATA_DIR . "/$dest";
		if (!is_dir(dirname($to))) {
			mkdir(dirname($to), 0777, true);
		}
		if (file_exists("src/www/$src")) {
			rename("src/www/$src", $to);
		} elseif (!is_dir($to)) {
			mkdir($to, 0777, true);
		}
	}
}
