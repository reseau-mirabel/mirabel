<?php

class m211118_071447_m5063_hints_politique extends \CDbMigration
{
	public function up(): bool
	{
		$now = time();
		$this->insertMultiple("Hint", [
			[
				'model' => 'Politique',
				'attribute' => 'article_version',
				'name' => "Appliquée à",
				'description' => "",
				'hdateModif' => $now,
			],
			[
				'model' => 'Politique',
				'attribute' => 'prerequisites',
				'name' => "Prérequis",
				'description' => "",
				'hdateModif' => $now,
			],
			[
				'model' => 'Politique',
				'attribute' => 'location',
				'name' => "Site de diffusion",
				'description' => "",
				'hdateModif' => $now,
			],
			[
				'model' => 'Politique',
				'attribute' => 'named_repository',
				'name' => "Archive nommée",
				'description' => "",
				'hdateModif' => $now,
			],
			[
				'model' => 'Politique',
				'attribute' => 'license',
				'name' => "Licence autorisée",
				'description' => "",
				'hdateModif' => $now,
			],
			[
				'model' => 'Politique',
				'attribute' => 'public_notes',
				'name' => "Remarques publiques",
				'description' => "",
				'hdateModif' => $now,
			],
		]);
		return true;
	}

	public function down(): bool
	{
		echo "m211118_071447_m5063_hints_politique does not support migrating down.\n";
		return false;
	}
}
