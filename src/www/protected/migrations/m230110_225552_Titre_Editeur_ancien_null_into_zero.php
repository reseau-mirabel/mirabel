<?php

class m230110_225552_Titre_Editeur_ancien_null_into_zero extends \CDbMigration
{
	public function up(): bool
	{
		$this->update(
			'Titre_Editeur',
			[ 'ancien' => 0 ],
			'ancien IS NULL'
		);

		$this->alterColumn(
			'Titre_Editeur',
			'ancien',
			'tinyint(1) NOT NULL DEFAULT 0'
		);
		return true;
	}

	public function down(): bool
	{
		echo "m230110_225552_Titre_Editeur_ancien_null_into_zero does not support migrating down.\n";
		return false;
	}
}
