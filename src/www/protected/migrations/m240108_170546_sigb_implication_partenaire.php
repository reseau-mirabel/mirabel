<?php

class m240108_170546_sigb_implication_partenaire extends \CDbMigration
{
	public function up(): bool
	{
		$this->addColumn(
			'Partenaire',
			'sigb',
			"varchar(255) NOT NULL DEFAULT '' COMMENT 'sigb utilisé par le partenaire'"
		);

		$this->addColumn(
			'Partenaire',
			'implication',
			"text NOT NULL DEFAULT '' COMMENT 'implication dans mirabel'"
		);

		$this->addColumn(
			'Partenaire',
			'usageLocal',
			"json NOT NULL DEFAULT '[]' COMMENT 'usage fait des données de M par le partenaire'"
		);

		if (defined('YII_TARGET') && YII_TARGET === 'test') {
			$this->execute("UPDATE Partenaire SET usageLocal = '[\"abracadabra\"]' WHERE id = 1");
		}

		return true;
	}

	public function down(): bool
	{
		$this->dropColumn(
			"Partenaire",
			"usageLocal",
		);
		$this->dropColumn("Partenaire", "implication");
		$this->dropColumn("Partenaire", "sigb");
		return true;
	}
}
