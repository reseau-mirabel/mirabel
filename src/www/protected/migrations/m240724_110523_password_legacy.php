<?php

class m240724_110523_password_legacy extends \CDbMigration
{
	public function up(): bool
	{
		$this->update(
			"Utilisateur",
			['authMethod' => Utilisateur::AUTHMETHOD_PASSWORD, 'motdepasse' => ''],
			"authMethod = 3" // Utilisateur::AUTHMETHOD_LEGACY
		);
		return true;
	}

	public function down(): bool
	{
		echo "m240724_110523_password_legacy does not support migrating down.\n";
		return false;
	}
}
