<?php

class m220307_152246_m5209_contact_donnes_email extends \CDbMigration
{
	public function safeUp()
	{
		$this->delete("Config", "`key` = 'contact.donnees.email'");
		$this->insert("Config", [
			'key' => 'contact.donnees.email',
			'value' => '',
			'type' => 'string',
			'category' => 'contact',
			'description' => <<<EOHTML
				Cette adresse est utilisée par le formulaire de contact sur les accès verrouillés,
				quand l'accès n'est pas suivi par un partenaire de Mir@bel.
				Si ce champ est vide, la destination sera <em>contact.email</em>.
				EOHTML,
			'required' => 0,
			'createdOn' => time(),
			'updatedOn' => null,
		]);
	}

	public function safeDown()
	{
		$this->delete("Config", "`key` = 'contact.donnees.email'");
	}
}
