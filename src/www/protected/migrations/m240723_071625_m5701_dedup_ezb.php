<?php

class m240723_071625_m5701_dedup_ezb extends \CDbMigration
{
	public function up(): bool
	{
		$titres = Titre::model()->findAllBySql(<<<SQL
			SELECT t.id, t.titre, t.liensJson
			FROM Titre t
			WHERE `liensJson` LIKE '%ezb.uni-regensburg.de%ezb.ur.de%'
			ORDER BY t.titre
			SQL
		);
		foreach ($titres as $titre) {
			assert($titre instanceof Titre);
			self::updateTitreRecord($titre);
		}
		return true;
	}

	public function down(): bool
	{
		echo "m240723_071625_m5701_dedup_ezb does not support migrating down.\n";
		return false;
	}

	private function updateTitreRecord(Titre $titre)
	{
		$liens = $titre->getLiens();
		foreach ($liens as $l) {
			assert($l instanceof Lien);
			if (strpos($l->url, 'ezb.uni-regensburg.de') === false) {
				continue;
			}

			fputcsv(STDOUT, [$titre->id, $titre->titre, $l->url], ';', '"', '\\');
			$liens->remove($l);
			$titre->liensJson = json_encode($liens, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
			if (!$titre->saveAttributes(['liensJson'])) {
				throw new \Exception("Save failed.");
			}
			$liens->save("Titre", $titre->id);
			return;
		}
	}
}
