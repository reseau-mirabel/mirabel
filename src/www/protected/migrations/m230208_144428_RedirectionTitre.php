<?php

class m230208_144428_RedirectionTitre extends \CDbMigration
{
	public function up(): bool
	{
		if (!$this->getDbConnection()->getSchema()->getTable("RedirectionTitre")) {
			$this->createTable(
				'RedirectionTitre',
				[
					'sourceId' => "INT unsigned NOT NULL COMMENT 'titre fusionné' ",
					'cibleId' => "INT unsigned NOT NULL COMMENT 'titre destinataire' ",
					'hdateModif' => "BIGINT NOT NULL DEFAULT 0",
				]
			);
			$this->addPrimaryKey('pk_RedirectionTitre', 'RedirectionTitre', ['sourceId', 'cibleId']);
			$this->addForeignKey(
				'fk_RedirectionTitre_cible',
				'RedirectionTitre',
				'cibleId',
				'Titre',
				'id'
			);
		}

		if (defined('YII_TARGET') && YII_TARGET === 'test') {
			$this->execute('TRUNCATE TABLE RedirectionTitre');
			$this->insert('RedirectionTitre', ['sourceId' => 4, 'cibleId' => 366, 'hdateModif' => 1675867673]); // S!lence
		}

		return true;
	}

	public function down(): bool
	{
		if ($this->getDbConnection()->getSchema()->getTable("RedirectionTitre")) {
			$this->dropTable('RedirectionTitre');
		}
		return true;
	}
}
