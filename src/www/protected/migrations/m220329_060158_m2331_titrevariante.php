<?php

class m220329_060158_m2331_titrevariante extends \CDbMigration
{
	public function up(): bool
	{
		if ($this->getDbConnection()->getSchema()->getTable("TitreVariante")) {
			return true;
		}
		$this->createTable(
			"TitreVariante",
			[
				'id' => "INT unsigned NOT NULL PRIMARY KEY AUTO_INCREMENT",
				'titreId' => "INT unsigned NOT NULL",
				'variante' => "VARCHAR(1024) COLLATE utf8mb4_unicode_ci NOT NULL",
				'lang' => "CHAR(3) COLLATE ascii_bin NOT NULL DEFAULT '   '",
				'zoneSudoc' => "CHAR(8) COLLATE ascii_bin NOT NULL DEFAULT '        '",
				'lastUpdate' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
			],
			"COMMENT 'Variantes de titres'"
		);
		return true;
	}

	public function down(): bool
	{
		$this->dropTable("TitreVariante");
		return true;
	}
}
