<?php

class m240213_163709_m5748_test_ldap_users_selfupdate extends \CDbMigration
{
	public function up(): bool
	{
		if (!defined('YII_TARGET') || YII_TARGET !== 'test') {
			return true;
		}
		$this->insert('Utilisateur', [
			'id' => 4,
			'partenaireId' =>1,
			'login' => 'aramis',
			'nom' => 'Aramis',
			'prenom' => 'Henri',
			'email'=> 'aramis@3mousquetaires.example.org',
			'nomComplet'=> "Henri d'Aramitz",
			'actif' => 1,
			'hdateCreation' => time(),
			'hdateModif' => time(),
			'suiviPartenairesEditeurs' => 0,
			'listeDiffusion' => 1,
			'authmethod' => 1, //LDAP
		]);
		return true;
	}

	public function down(): bool
	{
		if (!defined('YII_TARGET') || YII_TARGET !== 'test') {
			return true;
		}
		$this->delete('Utilisateur', ['login' => 'aramis']);
		return true;
	}
}
