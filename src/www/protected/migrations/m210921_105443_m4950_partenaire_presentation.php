<?php

class m210921_105443_m4950_partenaire_presentation extends \CDbMigration
{
	public function up(): bool
	{
		$cmsTable = $this->getDbConnection()->getSchema()->getTable("Cms");

		if (!in_array('presentation', $cmsTable->columns)) {
			$this->addColumn("Partenaire", "presentation", "TEXT NOT NULL COMMENT 'HTML text' AFTER description");
		}

		if (in_array('partenaireId', $cmsTable->columns)) {
			$cms = Cms::model()->findAll("name = 'partenaire' AND partenaireId IS NOT NULL");
			foreach ($cms as $c) {
				/** @var \Cms $c */
				$this->update(
					"Partenaire",
					['presentation' => $c->toHtml()],
					"id = {$c->partenaireId}"
				);
			}

			$this->delete("Cms", "name = 'partenaire' AND partenaireId IS NOT NULL");

			$this->dropForeignKey("fk_Cms_Partenaire", "Cms");
			$this->dropColumn("Cms", "partenaireId");
		}
		return true;
	}

	public function down(): bool
	{
		return false;
	}
}
