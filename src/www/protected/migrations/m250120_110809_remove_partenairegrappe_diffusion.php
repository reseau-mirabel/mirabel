<?php

class m250120_110809_remove_partenairegrappe_diffusion extends \CDbMigration
{
	public function up(): bool
	{
		$this->dropColumn("Partenaire_Grappe", "diffusion");
		return true;
	}

	public function down(): bool
	{
		echo "m250120_110809_remove_partenairegrappe_diffusion does not support migrating down.\n";
		return false;
	}
}
