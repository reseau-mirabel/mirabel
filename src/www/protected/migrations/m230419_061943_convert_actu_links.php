<?php

class m230419_061943_convert_actu_links extends \CDbMigration
{
	public function up(): bool
	{
		$this->execute("UPDATE Cms SET content = replace(content, '/site/page/actualite#actu', '/site/actualite/')");
		return true;
	}

	public function down(): bool
	{
		echo "m230419_061943_convert_actu_links does not support migrating down.\n";
		return false;
	}
}
