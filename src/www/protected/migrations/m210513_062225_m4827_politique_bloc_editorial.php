<?php

class m210513_062225_m4827_politique_bloc_editorial extends \CDbMigration
{
	public function safeUp()
	{
		$this->insert("Cms", [
			'name' => "politique.creation-utilisateur",
			'singlePage' => 0,
			'type' => 'html',
			'content' => "<p>
	Ce compte web vous permettra de déclarer la politique de l'éditeur sur ses revues.
	...
	Un courriel sera immédiatement envoyé avec vos informations de connexion.
</p>
<p>
	Il est recommandé d'utiliser une adresse électronique institutionnelle pour faciliter la validation du compte
	par l'équipe de Mir@bel.
</p>",
			'hdateCreat' => time(),
		]);
		$this->execute("UPDATE Config SET `key` = REPLACE(`key`, 'utilisateur-editeur.', 'password.politique.'), category = 'politique.' WHERE `key` LIKE 'utilisateur-editeur.%'");
		return true;
	}

	public function safeDown()
	{
		$this->delete("Cms", "name = 'politique.creation-utilisateur'");
		return true;
	}
}
