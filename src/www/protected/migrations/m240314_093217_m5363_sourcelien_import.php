<?php

class m240314_093217_m5363_sourcelien_import extends \CDbMigration
{
	public function up(): bool
	{
		$this->renameColumn("Sourcelien", "saisie", "import");
		$this->alterColumn("Sourcelien", "import", "tinyint not null default 0 COMMENT 'boolean'");
		$this->execute("UPDATE Sourcelien SET import = 1 - import");
		return true;
	}

	public function down(): bool
	{
		echo "m240314_093217_m5363_sourcelien_import does not support migrating down.\n";
		return false;
	}
}
