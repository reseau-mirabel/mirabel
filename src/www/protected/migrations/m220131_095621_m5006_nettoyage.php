<?php

class m220131_095621_m5006_nettoyage extends \CDbMigration
{
	public function up(): bool
	{
		$this->dropColumn("Politique", "locked");
		$this->dropForeignKey('fk_politique_replace', "Politique");
		$this->dropColumn("Politique", "replace");
		return true;
	}

	public function down(): bool
	{
		echo "m220131_095621_m5006_nettoyage does not support migrating down.\n";
		return false;
	}
}
