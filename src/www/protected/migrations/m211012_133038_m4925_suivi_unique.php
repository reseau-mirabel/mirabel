<?php

class m211012_133038_m4925_suivi_unique extends \CDbMigration
{
	public function up(): bool
	{
		$this->alterColumn("Suivi", "cible", "enum('Revue','Ressource','Editeur') COLLATE ascii_bin NOT NULL COMMENT 'Table de l''objet suivi'");
		$this->createIndex("suivi_unique", "Suivi", "partenaireId, cible, cibleId", true);
		try {
			$this->dropIndex("fk_Suivi_Partenaire", "Suivi");
		} catch (\CDbException $e) {
			echo "L'index fk_Suivi_Partenaire a déjà été supprimé : {$e->getMessage()}\n";
		}
		return true;
	}

	public function down(): bool
	{
		$this->createIndex('fk_Suivi_Partenaire', 'Suivi', 'partenaireId');
		$this->dropIndex("suivi_unique", "Suivi");
		return true;
	}
}
