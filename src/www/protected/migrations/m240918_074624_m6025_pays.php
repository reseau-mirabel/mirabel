<?php

class m240918_074624_m6025_pays extends \CDbMigration
{
	public function safeUp()
	{
		// Supprime "Îles Cook" qui n'est pas souverain
		$this->delete("Pays", "code2 = 'CK'");
		// Ajoute "Hong-Kong" qui n'est pas souverain, mais...
		$this->insert("Pays", ['nom' => "Hong-Kong, région de la Chine", 'code' => 'HKG', 'code2' => 'HK']);
		// Change la description
		$this->update(
			"Config",
			['description' => <<<'HTML'
				<p>
					Table de correspondance entre territoires (TOM) et pays pour les notices Sudoc.
					Chaque ligne doit contenir deux codes ISO3166-1-alpha2 (2 lettres majuscules) séparés par ";".
				</p>
				<div>Les <em>pays</em> acceptés par Mir@bel sont :
				<ul>
					<li>les 193 membres de l'ONU,</li>
					<li>les deux observateurs à l'ONU que sont la Palestine et le Saint-Siège (nom officiel du Vatican),</li>
					<li>Hong-Kong, région administrative spéciale de la Chine,</li>
					<li>Taïwan, dont le statut souverain est disputé.</li>
				</ul>
				HTML],
			"`key` = 'pays.correspondance'"
		);
		return true;
	}

	public function safeDown()
	{
		return false;
	}
}
