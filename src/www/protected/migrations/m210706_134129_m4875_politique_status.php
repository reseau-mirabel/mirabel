<?php

class m210706_134129_m4875_politique_status extends \CDbMigration
{
	public function up(): bool
	{
		$this->addColumn("Politique", "statut", "enum('pending','draft','topublish','published','updated','todelete','deleted') CHARACTER SET ascii COLLATE ascii_bin NOT NULL COMMENT 'see doc for possible transitions according to user role' AFTER status");
		$this->execute("UPDATE Politique SET statut = 'deleted' WHERE status = 'deleted'");
		$this->execute("UPDATE Politique SET statut = 'draft' WHERE status = 'active'");
		$this->execute("UPDATE Politique SET statut = 'pending' WHERE status = 'new'");
		$this->dropColumn("Politique", "status");
		$this->renameColumn("Politique", "statut", "status");
		return true;
	}

	public function down(): bool
	{
		echo "m210706_134129_m4875_politique_status does not support migrating down.\n";
		return false;
	}
}
