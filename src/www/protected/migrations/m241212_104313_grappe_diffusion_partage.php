<?php

class m241212_104313_grappe_diffusion_partage extends \CDbMigration
{
	public function safeUp()
	{
		// Convert the "diffusion" for Grappe instances
		// which are referenced in lists of any other Partenaire.
		// It splits the value 1 into 1 and 2, and shifts other values.
		$this->execute("UPDATE Grappe SET diffusion = diffusion + 1");
		$this->execute("UPDATE Partenaire_Grappe SET diffusion = diffusion + 1");
		// If it's used outside, it's shared!
		$this->execute(<<<SQL
			UPDATE Grappe g
				JOIN Partenaire_Grappe l ON l.grappeId = g.id AND (l.partenaireId IS NULL OR l.partenaireId <> g.partenaireId)
			SET g.diffusion = :d
			WHERE g.diffusion > 1
			SQL,
			[':d' => Grappe::DIFFUSION_PARTAGE]
		);
	}

	public function safeDown()
	{
		$this->execute("UPDATE Partenaire_Grappe SET diffusion = IF(diffusion = 1, 1, diffusion - 1)");
		$this->execute("UPDATE Grappe SET diffusion = IF(diffusion = 1, 1, diffusion - 1)");
		return true;
	}
}
