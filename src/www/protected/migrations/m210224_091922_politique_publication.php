<?php

class m210224_091922_politique_publication extends \CDbMigration
{
	public function up(): bool
	{
		$this->alterColumn("Utilisateur", "partenaireId", "int(10) unsigned NULL");

		if ($this->getDbConnection()->getSchema()->getTable("Politique")) {
			return true;
		}
		$this->createTable(
			"Utilisateur_Editeur",
			[
				'utilisateurId' => "INT(10) unsigned NOT NULL",
				'editeurId' => "INT(10) unsigned NOT NULL",
				'role' => "ENUM('guest', 'pending', 'owner') COLLATE ascii_bin NOT NULL COMMENT '*pending* needs validation to become *owner*'",
				'lastUpdate' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
			],
			"COMMENT 'Permet à un utilisateur de gérer les politiques de publication de ses éditeurs'"
		);
		$this->addPrimaryKey('pk_utilisateur_editeur', "Utilisateur_Editeur", ['utilisateurId', 'editeurId']);
		$this->addForeignKey('fk_utilisateurediteur_utilisateur', "Utilisateur_Editeur", 'utilisateurId', 'Utilisateur', 'id', "CASCADE", "CASCADE");
		$this->addForeignKey('fk_utilisateurediteur_editeur', "Utilisateur_Editeur", 'editeurId', 'Editeur', 'id', "CASCADE", "CASCADE");

		$this->createTable(
			"Politique",
			[
				'id' => "INT unsigned NOT NULL PRIMARY KEY AUTO_INCREMENT",
				'editeurId' => "INT(10) unsigned NOT NULL",
				'utilisateurId' => "INT(10) unsigned NULL DEFAULT NULL",
				'replace' => "INT unsigned NULL COMMENT 'id of the Politique record replaced by this record'",
				'locked' => "BOOLEAN NOT NULL DEFAULT 0 COMMENT 'non-owners cannot modify locked records'",
				'status' => "ENUM('new', 'active', 'dead') COLLATE ascii_bin NOT NULL DEFAULT 'new' COMMENT '*new* has no links to Titre, *active* has alive links, *dead* has only removed links'",
				'initialTitreId' => "int unsigned DEFAULT NULL COMMENT 'Titre.id selected when this record was created'",
				'name' => "VARCHAR(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT 'copy from publisher_policy.internal_moniker'",
				'lastUpdate' => "BIGINT NOT NULL COMMENT 'timestamp'",
				'model' => "SMALLINT NULL DEFAULT NULL COMMENT '[libre, apc, hybride, embargo, abonnement]'",
				'publisher_policy' => "LONGTEXT COLLATE ascii_bin NOT NULL DEFAULT '' COMMENT 'JSON object, cf Sherpa API'",
			],
			"COMMENT 'Politique de publication, appliquable à des titres par Politique_Titre'"
		);
		$this->addForeignKey('fk_politique_replace', "Politique", 'replace', 'Politique', 'id', "SET NULL", "CASCADE");
		$this->addForeignKey('fk_politique_utilisateur', "Politique", 'utilisateurId', 'Utilisateur', 'id', "SET NULL", "CASCADE");
		$this->addForeignKey('fk_politique_editeur', "Politique", 'editeurId', 'Editeur', 'id', "CASCADE", "CASCADE");

		$this->createTable(
			"Politique_Titre",
			[
				'politiqueId' => "INT unsigned NOT NULL",
				'titreId' => 'INT(10) unsigned NOT NULL',
				'confirmed' => 'BOOLEAN NOT NULL DEFAULT 0',
				'createdOn' => "BIGINT NOT NULL COMMENT 'timestamp'",
				'removedOn' => "BIGINT NULL DEFAULT NULL COMMENT 'timestamp'",
			],
			"COMMENT 'Qualification des liens entre Politique et Titre'"
		);
		$this->addPrimaryKey('pk_politiquetitre', "Politique_Titre", ['politiqueId', 'titreId']);
		$this->createIndex('politiquetitre_titre', "Politique_Titre", 'titreId', true);
		$this->addForeignKey('fk_politiquetitre_politique', "Politique_Titre", 'politiqueId', 'Politique', 'id', "CASCADE", "CASCADE");
		$this->addForeignKey('fk_politiquetitre_titre', "Politique_Titre", 'titreId', 'Titre', 'id', "CASCADE", "CASCADE");

		return true;
	}

	public function down(): bool
	{
		$this->dropTable("Politique_Titre");
		$this->dropTable("Politique");
		$this->dropTable("Utilisateur_Editeur");
		$this->delete("Utilisateur", "partenaireId IS NULL");
		$this->alterColumn("Utilisateur", "partenaireId", "int(10) unsigned NOT NULL");
		return true;
	}
}
