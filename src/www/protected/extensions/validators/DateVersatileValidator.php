<?php

/**
 * In rules():
 * ['dateStart, dateEnd', 'ext.validators.DateVersatileValidator'],
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class DateVersatileValidator extends CValidator
{
	public $onlyFullDate = false;

	public $nothingAfter = false;

	public $padding = false;

	public $modernDate = 1050;

	public $allowempty = true;

	/**
	 * Validates the attribute of the object.
	 *
	 * If there is any error, the error message is added to the object.
	 *
	 * @param CModel $object the object being validated.
	 * @param string $attribute the attribute being validated.
	 */
	protected function validateAttribute($object, $attribute)
	{
		if ($object->{$attribute} === null || trim($object->{$attribute}) === '' || $object->{$attribute} === '000-00-00') {
			if (!$this->allowempty) {
				$object->addError($attribute, "Cette date ne peut être vide.");
			}
			if ($object->{$attribute} !== null) {
				$object->{$attribute} = '';
			}
			return;
		}
		$value = str_replace(
			['/', "\xFE\xFF", "\xEF\xBB\xBF"],
			['-', '', ''],
			trim($object->{$attribute}) . ' '
		);
		if (preg_match('/^(\d{4})(-\d\d)?(-\d\d)?\s+(.*?)$/', $value, $match)) {
			// ISO
			[, $year, $month, $day, $comment] = $match;
		} elseif (preg_match('/^(\d\d-)?(\d\d-)?(\d{4})\s+(.*?)$/', $value, $match)) {
			// European
			[, $day, $month, $year, $comment] = $match;
			if (empty($month)) {
				$month = $day;
				$day = '';
			}
		} else {
			$object->addError($attribute, "Format de date non valide (YYYY-mm-dd ou dd/mm/YYYY).");
			return;
		}
		$year = (int) $year;
		$month = (int) trim($month, '-');
		$day = (int) trim($day, '-');
		if (!checkdate($month ?: 1, $day ?: 1, $year)) {
			$object->addError($attribute, "Date non valide mais de format correct (il n'y a pas de 35 mars, etc).");
			return;
		}
		if ($this->onlyFullDate && (!$day || !$month)) {
			$object->addError($attribute, 'La date est incomplète (format attendu, YYYY-mm-dd ou dd/mm/YYYY).');
			return;
		}
		if ($this->modernDate && $year < $this->modernDate) {
			$object->addError($attribute, "La date est trop ancienne, elle doit être postérieure à {$this->modernDate}.");
			return;
		}
		if ($this->nothingAfter && $comment) {
			$object->addError($attribute, 'La date est suivie de texte excédentaire.');
			return;
		}
		if ($this->padding) {
			$object->{$attribute} = trim(sprintf('%4d-%02d-%02d %s', $year, $month, $day, $comment));
		} else {
			if (empty($month)) {
				$object->{$attribute} = trim(sprintf('%4d %s', $year, $comment));
			} elseif (!$day) {
				$object->{$attribute} = trim(sprintf('%4d-%02d %s', $year, $month, $comment));
			} else {
				$object->{$attribute} = trim(sprintf('%4d-%02d-%02d %s', $year, $month, $day, $comment));
			}
		}
	}
}
