<?php

class ArrayOfIntValidator extends CValidator
{
	/**
	 * @var bool whether the attribute value can be null or empty. Defaults to true,
	 * meaning that if the attribute is empty, it is considered valid.
	 */
	public bool $allowEmpty = true;

	public bool $allowNegative = false;

	/**
	 * @return array{null|int[], string} [parsed values, error message]
	 */
	public function parseValue(mixed $value): array
	{
		$error = "";
		if ($value === null) {
			return [null, $error];
		}
		if (is_scalar($value)) {
			$value = (string) $value;
			if (strpos($value, ',') !== false) {
				$value = explode(',', $value);
			} else {
				$value = explode('_', $value);
			}
		} elseif (!is_array($value)) {
			$error = "Format non-valide (ni scalaire ni tableau).";
			return [[], $error];
		}
		if ($value === []) {
			if (!$this->allowEmpty) {
				$error = "Ce champ ne peut être vide.";
			}
			return [[], $error];
		}
		$result = [];
		foreach ($value as $v) {
			if ($v === '') {
				continue;
			}
			if (
				is_int($v)
				|| ctype_digit($v)
				|| ($this->allowNegative && is_scalar($v) && preg_match('/^-?\d+\s*$/', (string) $v))
			) {
				$result[] = (int) $v;
			} else {
				$error = "Ce champ contient une valeur non-valide.";
				// Remove the invalid value from the parsed results.
			}
		}
		return [$result, $error];
	}

	/**
	 * Validates the attribute of the object.
	 *
	 * If there is any error, the error message is added to the object.
	 *
	 * @param CModel $object the object being validated.
	 * @param string $attribute Name of the attribute being validated.
	 */
	protected function validateAttribute($object, $attribute)
	{
		[$value, $error] = $this->parseValue($object->{$attribute});
		if ($error) {
			$object->addError($attribute, $error);
		}
		$object->{$attribute} = $value;
	}
}
