<?php
/**
 * BootInputVertical class file.
 * @author Christoffer Niska <ChristofferNiska@gmail.com>
 * @copyright Copyright &copy; Christoffer Niska 2011-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package bootstrap.widgets.input
 */

Yii::import('bootstrap.widgets.input.BootInput');

/**
 * Bootstrap vertical form input widget.
 * @since 0.9.8
 */
class BootInputVertical extends BootInput
{
	protected $hint = '';

	public function run()
	{
		$this->hint = $this->getHint();
		echo CHtml::openTag('div', ['class'=>'control-group ' . $this->getContainerCssClass()]);
		parent::run();
		echo '</div>';
	}

	/**
	 * Renders a checkbox.
	 */
	protected function checkBox(): void
	{
		$attribute = $this->attribute;
		echo '<label class="checkbox" for="' . $this->getAttributeId($attribute) . '">';
		echo $this->form->checkBox($this->model, $this->attribute, $this->htmlOptions) . PHP_EOL;
		echo $this->model->getAttributeLabel($attribute);
		echo $this->getError() . $this->hint;
		echo '</label>';
	}

	/**
	 * Renders a list of checkboxes.
	 */
	protected function checkBoxList(): void
	{
		echo $this->getLabel();
		echo $this->form->checkBoxList($this->model, $this->attribute, $this->data, $this->htmlOptions);
		echo $this->getError() . $this->hint;
	}

	/**
	 * Renders a list of inline checkboxes.
	 */
	protected function checkBoxListInline(): void
	{
		$this->htmlOptions['inline'] = true;
		$this->checkBoxList();
	}

	/**
	 * Renders a drop down list (select).
	 */
	protected function dropDownList(): void
	{
		echo $this->getLabel();
		echo $this->form->dropDownList($this->model, $this->attribute, $this->data, $this->htmlOptions);
		echo $this->getError() . $this->hint;
	}

	/**
	 * Renders a file field.
	 */
	protected function fileField(): void
	{
		echo $this->getLabel();
		echo $this->form->fileField($this->model, $this->attribute, $this->htmlOptions);
		echo $this->getError() . $this->hint;
	}

	/**
	 * Renders a password field.
	 */
	protected function passwordField(): void
	{
		echo $this->getLabel();
		echo $this->getPrepend();
		$append = $this->getAppend();
		echo $this->form->passwordField($this->model, $this->attribute, $this->htmlOptions);
		echo $append;
		echo $this->getError() . $this->hint;
	}

	/**
	 * Renders a radio button.
	 */
	protected function radioButton(): void
	{
		$attribute = $this->attribute;
		echo '<label class="radio" for="' . $this->getAttributeId($attribute) . '">';
		echo $this->form->radioButton($this->model, $this->attribute, $this->htmlOptions) . PHP_EOL;
		echo $this->model->getAttributeLabel($attribute);
		echo $this->getError() . $this->hint;
		echo '</label>';
	}

	/**
	 * Renders a list of radio buttons.
	 */
	protected function radioButtonList(): void
	{
		echo $this->getLabel();
		echo $this->form->radioButtonList($this->model, $this->attribute, $this->data, $this->htmlOptions);
		echo $this->getError() . $this->hint;
	}

	/**
	 * Renders a list of inline radio buttons.
	 */
	protected function radioButtonListInline(): void
	{
		$this->htmlOptions['inline'] = true;
		$this->radioButtonList();
	}

	/**
	 * Renders a textarea.
	 */
	protected function textArea(): void
	{
		echo $this->getLabel();
		echo $this->form->textArea($this->model, $this->attribute, $this->htmlOptions);
		echo $this->getError() . $this->hint;
	}

	/**
	 * Renders a text field.
	 */
	protected function textField(): void
	{
		echo $this->getLabel();
		echo $this->getPrepend();
		$append = $this->getAppend();
		echo $this->form->textField($this->model, $this->attribute, $this->htmlOptions);
		echo $append;
		echo $this->getError() . $this->hint;
	}

	/**
	 * Renders a CAPTCHA.
	 */
	protected function captcha(): void
	{
		echo $this->getLabel() . '<div class="captcha">';
		echo '<div class="widget">' . $this->widget('CCaptcha', $this->getCaptchaOptions(), true) . '</div>';
		echo $this->form->textField($this->model, $this->attribute, $this->htmlOptions);
		echo $this->getError() . $this->hint;
		echo '</div>';
	}

	/**
	 * Renders an uneditable field.
	 */
	protected function uneditableField(): void
	{
		echo $this->getLabel();
		echo CHtml::tag('span', $this->htmlOptions, $this->model->{$this->attribute});
		echo $this->getError() . $this->hint;
	}
}
