<?php
/**
 * BootInputHorizontal class file.
 * @author Christoffer Niska <ChristofferNiska@gmail.com>
 * @copyright Copyright &copy; Christoffer Niska 2011-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package bootstrap.widgets.input
 */

Yii::import('bootstrap.widgets.input.BootInput');

/**
 * Bootstrap horizontal form input widget.
 * @since 0.9.8
 */
class BootInputHorizontal extends BootInput
{
	protected $hint = '';

	/**
	 * Runs the widget.
	 */
	public function run()
	{
		$this->hint = $this->getHint();
		echo CHtml::openTag('div', ['class'=>'control-group ' . $this->getContainerCssClass()]);
		switch ($this->type) {
			case self::TYPE_NUMBER:
				$this->numberField();
				break;
			case self::TYPE_EMAIL:
				$this->emailField();
				break;
			case self::TYPE_AUTOCOMPLETE:
				$this->autocompleteField();
				break;
			case 'free':
				$this->freeField();
				break;
			default:
				parent::run();
		}
		echo '</div>';
	}

	/**
	 * Returns the label for this block.
	 * @return string the label
	 */
	protected function getLabel()
	{
		if (!isset($this->htmlOptions['labelOptions'])) {
			$this->htmlOptions['labelOptions'] = [];
		}

		if (isset($this->htmlOptions['labelOptions']['class'])) {
			$this->htmlOptions['labelOptions']['class'] .= ' control-label';
		} else {
			$this->htmlOptions['labelOptions']['class'] = 'control-label';
		}

		return parent::getLabel();
	}

	/**
	 * Renders a checkbox.
	 */
	protected function checkBox(): void
	{
		$attribute = $this->attribute;
		$htmlOptions = $this->htmlOptions;
		if (isset($this->htmlOptions['label'])) {
			$label = $this->htmlOptions['label'];
			unset($this->htmlOptions['label']);
		} else {
			$label = $this->model->getAttributeLabel($attribute);
		}

		CHtml::resolveNameID($this->model, $attribute, $htmlOptions);
		if (isset($htmlOptions['label-position']) && $htmlOptions['label-position'] == 'left') {
			unset($htmlOptions['label-position']);
			echo '<label class="control-label" for="' . $this->getAttributeId($this->attribute) . '">';
			echo $label;
			echo "</label>";
			echo '<div class="controls">';
			echo $this->form->checkBox($this->model, $attribute, $htmlOptions) . PHP_EOL;
		} else {
			if (isset($htmlOptions['label'])) {
				unset($htmlOptions['label']);
			}
			echo '<div class="controls">';
			echo '<label class="checkbox" for="' . $this->getAttributeId($this->attribute) . '">';
			echo $this->form->checkBox($this->model, $this->attribute, $htmlOptions) . PHP_EOL;
			echo $label;
			echo "</label>";
		}
		echo $this->getError() . $this->hint;
		echo '</div>';
	}

	/**
	 * Renders a list of checkboxes.
	 */
	protected function checkBoxList(): void
	{
		echo $this->getLabel();
		echo '<div class="controls">';
		echo $this->form->checkBoxList($this->model, $this->attribute, $this->data, $this->htmlOptions);
		echo $this->getError() . $this->hint;
		echo '</div>';
	}

	/**
	 * Renders a list of inline checkboxes.
	 */
	protected function checkBoxListInline(): void
	{
		$this->htmlOptions['inline'] = true;
		$this->checkBoxList();
	}

	/**
	 * Renders a drop down list (select).
	 */
	protected function dropDownList(): void
	{
		echo $this->getLabel();
		echo '<div class="controls">';
		echo $this->getPrepend();
		$append = $this->getAppend();
		echo $this->form->dropDownList($this->model, $this->attribute, $this->data, $this->htmlOptions);
		echo $append;
		echo $this->getError() . $this->hint;
		echo '</div>';
	}

	/**
	 * Renders a file field.
	 */
	protected function fileField(): void
	{
		echo $this->getLabel();
		echo '<div class="controls">';
		$error = $this->getError();
		echo $this->form->fileField($this->model, $this->attribute, $this->htmlOptions);
		echo $error . $this->hint;
		echo '</div>';
	}

	/**
	 * Renders a password field.
	 */
	protected function passwordField(): void
	{
		echo $this->getLabel();
		echo '<div class="controls">';
		echo $this->getPrepend();
		$append = $this->getAppend();
		echo $this->form->passwordField($this->model, $this->attribute, $this->htmlOptions);
		echo $append;
		echo $this->getError() . $this->hint;
		echo '</div>';
	}

	/**
	 * Renders a radio button.
	 */
	protected function radioButton(): void
	{
		$attribute = $this->attribute;
		echo '<div class="controls">';
		echo '<label class="radio" for="' . $this->getAttributeId($attribute) . '">';
		echo $this->form->radioButton($this->model, $attribute, $this->htmlOptions) . PHP_EOL;
		echo $this->model->getAttributeLabel($attribute);
		echo $this->getError() . $this->hint;
		echo '</label></div>';
	}

	/**
	 * Renders a list of radio buttons.
	 */
	protected function radioButtonList(): void
	{
		echo $this->getLabel();
		echo '<div class="controls">';
		echo $this->form->radioButtonList($this->model, $this->attribute, $this->data, $this->htmlOptions);
		echo $this->getError() . $this->hint;
		echo '</div>';
	}

	/**
	 * Renders a list of inline radio buttons.
	 */
	protected function radioButtonListInline(): void
	{
		$this->htmlOptions['inline'] = true;
		$this->radioButtonList();
	}

	/**
	 * Renders a textarea.
	 */
	protected function textArea(): void
	{
		echo $this->getLabel();
		echo '<div class="controls">';
		echo $this->form->textArea($this->model, $this->attribute, $this->htmlOptions);
		echo $this->getError() . $this->hint;
		echo '</div>';
	}

	protected function emailField(): void
	{
		echo $this->getLabel();
		echo '<div class="controls">';
		echo $this->getPrepend();
		$append = $this->getAppend();
		echo $this->form->emailField($this->model, $this->attribute, $this->htmlOptions);
		echo $append;
		echo $this->getError() . $this->hint;
		echo '</div>';
	}

	protected function numberField(): void
	{
		echo $this->getLabel();
		echo '<div class="controls">';
		echo $this->getPrepend();
		$append = $this->getAppend();
		echo $this->form->numberField($this->model, $this->attribute, $this->htmlOptions);
		echo $append;
		echo $this->getError() . $this->hint;
		echo '</div>';
	}

	/**
	 * Renders a text field.
	 */
	protected function textField(): void
	{
		echo $this->getLabel();
		echo '<div class="controls">';
		echo $this->getPrepend();
		$append = $this->getAppend();
		echo $this->form->textField($this->model, $this->attribute, $this->htmlOptions);
		echo $append;
		echo $this->getError() . $this->hint;
		echo '</div>';
	}

	/**
	 * Renders a CAPTCHA.
	 */
	protected function captcha(): void
	{
		echo $this->getLabel();
		echo '<div class="controls"><div class="captcha">';
		echo '<div class="widget">' . $this->widget('CCaptcha', $this->getCaptchaOptions(), true) . '</div>';
		echo $this->form->textField($this->model, $this->attribute, $this->htmlOptions);
		echo $this->getError() . $this->hint;
		echo '</div></div>';
	}

	/**
	 * Renders an uneditable field.
	 */
	protected function uneditableField(): void
	{
		echo $this->getLabel();
		echo '<div class="controls">';
		echo CHtml::tag('span', $this->htmlOptions, $this->model->{$this->attribute});
		echo $this->getError() . $this->hint;
		echo '</div>';
	}

	/**
	 * Renders a CJuiAutoComplete field.
	 */
	protected function autoCompleteField(): void
	{
		echo $this->getLabel();
		echo '<div class="controls">';
		echo $this->getPrepend();
		$append = $this->getAppend();
		$source = $this->data['source'];
		unset($this->data['source']);
		Yii::app()->getController()->widget(
			'zii.widgets.jui.CJuiAutoComplete',
			[
				'name' => CHtml::resolveName($this->model, $this->attribute),
				'value' => $this->model->{$this->attribute},
				'source' => $source,
				// cf http://api.jqueryui.com/autocomplete/
				'options' => $this->data,
			]
		);
		echo $append;
		echo $this->getError() . $this->hint;
		echo '</div>';
	}

	/**
	 * Renders a free field.
	 */
	protected function freeField(): void
	{
		echo $this->getLabel();
		echo '<div class="controls">';
		echo CHtml::tag('span', $this->htmlOptions, $this->value);
		echo $this->getError() . $this->hint;
		echo '</div>';
	}
}
