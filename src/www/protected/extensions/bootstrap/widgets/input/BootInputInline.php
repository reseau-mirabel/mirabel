<?php
/**
 * BootInputInline class file.
 * @author Christoffer Niska <ChristofferNiska@gmail.com>
 * @copyright Copyright &copy; Christoffer Niska 2011-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package bootstrap.widgets.input
 */

Yii::import('bootstrap.widgets.input.BootInputVertical');

/**
 * Bootstrap vertical form input widget.
 * @since 0.9.8
 */
class BootInputInline extends BootInputVertical
{
	/**
	 * Renders a drop down list (select).
	 */
	protected function dropDownList(): void
	{
		echo $this->getLabel();
		echo $this->form->dropDownList($this->model, $this->attribute, $this->data, $this->htmlOptions);
	}

	/**
	 * Renders a password field.
	 */
	protected function passwordField(): void
	{
		if (empty($this->htmlOptions['placeholder'])) {
			$this->htmlOptions['placeholder'] = $this->model->getAttributeLabel($this->attribute);
		}
		echo $this->form->passwordField($this->model, $this->attribute, $this->htmlOptions);
	}

	/**
	 * Renders a textarea.
	 */
	protected function textArea(): void
	{
		if (empty($this->htmlOptions['placeholder'])) {
			$this->htmlOptions['placeholder'] = $this->model->getAttributeLabel($this->attribute);
		}
		echo $this->form->textArea($this->model, $this->attribute, $this->htmlOptions);
	}

	/**
	 * Renders a text field.
	 */
	protected function textField(): void
	{
		if (empty($this->htmlOptions['placeholder'])) {
			$this->htmlOptions['placeholder'] = $this->model->getAttributeLabel($this->attribute);
		}
		echo $this->form->textField($this->model, $this->attribute, $this->htmlOptions);
		echo $this->getHint();
	}
}
