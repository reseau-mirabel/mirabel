<?php
/**
 * BootBaseMenu class file.
 * @author Christoffer Niska <ChristofferNiska@gmail.com>
 * @copyright Copyright &copy; Christoffer Niska 2011-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package bootstrap.widgets
 */

abstract class BootBaseMenu extends CWidget
{
	/**
	 * @var array the menu items.
	 */
	public $items = [];

	/**
	 * @var string the item template.
	 */
	public $itemTemplate;

	/**
	 * @var bool whether to encode item labels.
	 */
	public $encodeLabel = true;

	/**
	 * @var array the HTML attributes for the widget container.
	 */
	public $htmlOptions = [];

	/**
	 * Runs the widget.
	 */
	public function run()
	{
		echo CHtml::openTag('ul', $this->htmlOptions);
		$this->renderItems($this->items);
		echo '</ul>';
	}

	/**
	 * Renders the items in this menu.
	 * @abstract
	 * @param array $items the menu items
	 */
	abstract public function renderItems($items);

	/**
	 * Renders a single item in the menu.
	 * @param array $item the item configuration
	 * @return string the rendered item
	 */
	protected function renderItem($item)
	{
		if (!isset($item['linkOptions'])) {
			$item['linkOptions'] = [];
		}

		if (isset($item['icon'])) {
			if (strpos($item['icon'], 'icon') === false) {
				$pieces = explode(' ', $item['icon']);
				$item['icon'] = 'icon-' . implode(' icon-', $pieces);
			}

			$item['label'] = '<i class="' . $item['icon'] . '"></i> ' . $item['label'];
		}

		if (!isset($item['header']) && !isset($item['url'])) {
			$item['url'] = '#';
		}

		if (isset($item['url'])) {
			return CHtml::link($item['label'], $item['url'], $item['linkOptions']);
		}
		return $item['label'];
	}

	/**
	 * Checks whether a menu item is active.
	 * @param array $item the menu item to be checked
	 * @param string $route the route of the current request
	 * @return bool the result
	 */
	protected function isItemActive($item, $route)
	{
		if (isset($item['url']) && is_array($item['url'])) {
			$itemRoute = trim($item['url'][0], '/');
			if (strpos($itemRoute, '/') === false) {
				$itemRoute = $this->controller->getId() . '/' . $itemRoute;
			}
			if (strcasecmp($itemRoute, $route) === 0) {
				if (count($item['url']) > 1) {
					foreach (array_splice($item['url'], 1) as $name=>$value) {
						if (!isset($_GET[$name]) || $_GET[$name] != $value) {
							return false;
						}
					}
				}
				return true;
			}
		}

		return false;
	}
}
