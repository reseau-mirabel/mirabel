<?php

/**
 * BootDetailView class file.
 * @author Christoffer Niska <ChristofferNiska@gmail.com>
 * @copyright Copyright &copy; Christoffer Niska 2011-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package bootstrap.widgets
 */
Yii::import('zii.widgets.CDetailView');

/**
 * Bootstrap detail view widget.
 * Used for setting default HTML classes and disabling the default CSS.
 */
class BootDetailView extends CDetailView
{
	// Table types.
	public const TYPE_PLAIN = '';

	public const TYPE_STRIPED = 'striped';

	public const TYPE_BORDERED = 'bordered';

	public const TYPE_CONDENSED = 'condensed';

	/**
	 * @var string|array the table type.
	 * Valid values are '', 'striped', 'bordered' and/or 'condensed'.
	 */
	public $type = [self::TYPE_STRIPED, self::TYPE_CONDENSED];

	/**
	 * @var string the URL of the CSS file used by this detail view.
	 * Defaults to false, meaning that no CSS will be included.
	 */
	public $cssFile = false;

	/**
	 * @var bool Line where the value is empy won't be displayed.
	 */
	public $hideEmptyLines = false;

	/**
	 * Initializes the widget.
	 */
	public function init()
	{
		parent::init();

		// When hiding empty values, hide NULL, unless configured differently.
		if ($this->hideEmptyLines) {
			$this->nullDisplay = '';
		}

		$classes = ['table'];

		if (is_string($this->type)) {
			$this->type = explode(' ', $this->type);
		}

		$validTypes = [self::TYPE_STRIPED, self::TYPE_BORDERED, self::TYPE_CONDENSED];

		foreach ($this->type as $type) {
			if (in_array($type, $validTypes)) {
				$classes[] = 'table-' . $type;
			}
		}

		$classesStr = implode(' ', $classes);
		if (isset($this->htmlOptions['class'])) {
			$this->htmlOptions['class'] .= ' ' . $classesStr;
		} else {
			$this->htmlOptions['class'] = $classesStr;
		}
	}

	/**
	 * Renders the detail view.
	 * This is the main entry of the whole detail view rendering.
	 */
	public function run()
	{
		foreach ($this->attributes as $k => $attribute) {
			if (is_object($attribute['value'] ?? '') && ($attribute['value'] instanceof \Closure)) {
				$this->attributes[$k]['value'] = call_user_func_array($attribute['value'], [$this->data]);
			}
			if (is_string($attribute)) {
				if (!preg_match('/^([\w\.]+)(:(\w*))?(:(.*))?$/', $attribute, $matches)) {
					throw new CException(Yii::t('zii', 'The attribute must be specified in the format of "Name:Type:Label", where "Type" and "Label" are optional.'));
				}
				$attribute = [
					'name' => $matches[1],
					'type' => $matches[3] ?? 'text',
				];
				if (isset($matches[5])) {
					$attribute['label'] = $matches[5];
				}
				$this->attributes[$k] = $attribute;
			}
			if (isset($attribute['type']) && !in_array($attribute['type'], ['', 'html', 'raw', 'text', 'ntext'], true)) {
				if (empty($attribute['cssClass'])) {
					$this->attributes[$k]['cssClass'] = $attribute['type'];
				} else {
					$this->attributes[$k]['cssClass'] .= " {$attribute['type']}";
				}
			}
		}
		parent::run();
	}

	/**
	 * @inheritdoc
	 * @param mixed $templateData data that will be inserted into {@link itemTemplate}
	 *    (assoc array, not string as the upstream documentation wrongly states)
	 */
	protected function renderItem($options, $templateData)
	{
		if ($this->hideEmptyLines && is_array($templateData)) {
			$value = (string) ($templateData['{value}'] ?? '');
			if ($value === '') {
				return;
			}
		}
		return parent::renderItem($options, $templateData);
	}
}
