
function addNewCompleteItem(containerId, id, label) {
	jQuery('#' + containerId + 'Template').clone()
		.find('label').append(label).end()
		.find('input').val(id).attr('checked', 'checked').end()
		.removeAttr('id').insertBefore('#Complete' + containerId).removeClass('hidden');
}
