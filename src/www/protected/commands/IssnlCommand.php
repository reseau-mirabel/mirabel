<?php

use processes\issn\IssnlImport;
use processes\issn\IssnOrgCheck;
use processes\issn\IssnOrgFromFile;
use processes\issn\IssnOrgZip;

class IssnlCommand extends CConsoleCommand
{
	use \commands\models\MaintenanceModeTrait;

	public $verbose = 0;

	public $simulation = false;

	public function actionCheck()
	{
		$checker = new IssnOrgCheck(STDOUT);
		if ($this->verbose > 0) {
			$checker->verbose = $this->verbose;
		}
		$checker->check();
	}

	/**
	 * @param string $download Si download=1, télécharge le zip depuis issn.org.
	 */
	public function actionCron(string $download = "")
	{
		if ((bool) $download) {
			$downloader = new IssnOrgZip();
			if ($this->verbose > 0) {
				$downloader->verbose = $this->verbose;
			}
			$downloaded = $downloader->downloadIssnorgZip(true);
			$tries = 1;
			while (!$downloaded && $tries < 3) {
				// wait 1 minute and download again
				sleep(60);
				$downloaded = $downloader->downloadIssnorgZip(false);
				$tries++;
			}
			if (!$downloaded) {
				echo "Le téléchargement du zip d'issn.org a échoué ($tries tentatives) :\n{$downloader->getLastError()}\n";
				if (!$downloader->restoreZip()) {
					echo "Arrêt faute de zip restant d'un précédent téléchargement.";
					return 1;
				}
			}
		}
		IssnlImport::rebuildIfMissing();

		echo "## Incohérences entre M et issn.org pour les ISSN valides de M (recherche par ISSN-L de M)\n";
		$this->actionCheck();
		echo "\n\n## Ajout de données issn.org dans M\n";
		$this->actionImport();
	}

	public function actionImport()
	{
		$issnorg = new IssnOrgFromFile(STDOUT);
		if ($this->simulation) {
			$issnorg->simulation = true;
		}
		if ($this->verbose > 0) {
			$issnorg->verbose = $this->verbose;
		}
		$issnorg->fillIssnl();
		$issnorg->fillIssn();
	}

	public function actionPrepare()
	{
		$i = new IssnlImport();
		$i->verbose = (int) $this->verbose;
		$i->import();
	}
}
