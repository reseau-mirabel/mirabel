<?php
/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class VolnoCommand extends CConsoleCommand
{
	public $verbose = 1;

	public $unknown = false;

	public $limit;

	public $separator = "\t";

	/**
	 * Normalize the VolNo field.
	 */
	public function actionToCsv()
	{
		echo join($this->separator, ["Service.id", "Titre.id", "column", "date", "Old value", "New value", "vol", "no", "format", "problem"])
			. "\n";
		$cmd = Yii::app()->db->createCommand(
			"SELECT id, titreId, numeroDebut, numeroFin, dateBarrDebut, dateBarrFin"
			. " FROM Service WHERE numeroDebut != '' OR numeroFin != ''"
			. ($this->limit ? " LIMIT {$this->limit}" : '')
		)->setFetchMode(PDO::FETCH_ASSOC);
		foreach ($cmd->query() as $row) {
			foreach (['numeroDebut', 'numeroFin'] as $col) {
				$this->readVolNoField($row[$col], $col, $row);
			}
		}
	}

	public function actionFromCsv($args = [])
	{
		if (!$args || count($args) !== 1) {
			echo "Fournir le nom du fichier CSV à appliquer comme seul paramètre.";
			return 1;
		}
		$csv = fopen($args[0], 'r');
		if (!$csv) {
			echo "Le fichier CSV N'a pu être lu.";
			return 1;
		}
		$header = array_map('trim', fgetcsv($csv, 0, $this->separator));
		$map = array_flip($header);
		$updateStart = Yii::app()->db->createCommand(
			"UPDATE Service SET numeroDebut = :numero, volDebut = :vol, noDebut = :no WHERE id = :id"
		);
		$updateEnd = Yii::app()->db->createCommand(
			"UPDATE Service SET numeroFin = :numero, volFin = :vol, noFin = :no WHERE id = :id"
		);
		while (($row = fgetcsv($csv, 0, $this->separator)) !== false) {
			$params = [
				':numero' => $row[$map['New value']],
				':vol' => $row[$map['vol']] ? (int) $row[$map['vol']] : null,
				':no' => $row[$map['no']] ? (int) $row[$map['no']] : null,
				':id' => $row[$map['Service.id']],
			];
			$col = $row[$map['column']];
			if ($col === 'numeroDebut') {
				if ($updateStart->execute($params)) {
					fprintf(STDERR, "OK\tnumeroDebut modifié\t%d\n", $row[$map['Service.id']]);
				} else {
					fprintf(STDERR, "ERREUR\tnumeroDebut non modifié\t%d\n", $row[$map['Service.id']]);
				}
			} elseif ($col === 'numeroFin') {
				if ($updateEnd->execute($params)) {
					fprintf(STDERR, "OK\tnumeroFin modifié\t%d\n", $row[$map['Service.id']]);
				} else {
					fprintf(STDERR, "ERREUR\tnumeroFin non modifié\t%d\n", $row[$map['Service.id']]);
				}
			} else {
				fprintf(STDERR, "ERREUR\tColonne '$col' non reconnue\n");
			}
		}
		fclose($csv);
	}

	protected function readVolNoField($fieldData, string $fieldName, $row)
	{
		if (trim($fieldData) === '') {
			return;
		}
		$year = (int) $row[str_replace('numero', 'dateBarr', $fieldName)];
		$split = $this->splitNumero($fieldData, $year);
		if ($this->unknown || $split[4] !== 'Aucun motif reconnu') {
			$after = array_merge([$row['id'], $row['titreId'], $fieldName, $year], $split);
			echo join($this->separator, $after) . "\n";
		}
	}

	protected function splitNumero($rawVolno, int $year)
	{
		$vol = '';
		$no = '';
		$knownPattern = true;

		$volno = preg_replace(
			'/,(\w)/',
			', $1',
			preg_replace(
				'/\s+,/',
				',',
				str_replace(',,', ',', trim($rawVolno))
			)
		);
		$m = [];
		$volType = 'Vol. ';
		if (preg_match('/^(\d+)$/', $volno, $m)) { // "1" -> no=1
			$volType = '-';
			$vol = '';
			$no = $m[1];
		} elseif (preg_match('/^n[o°]\.? ?(.+)$/iu', $volno, $m)) { // "no 1" -> no=1
			$volType = '-';
			$vol = '';
			$no = $m[1];
		} elseif (preg_match('/^(\d+),? N[o°]\.? ?(.+)$/iu', $volno, $m)) { // "2000, no 1" -> vol=2000,no=1
			$volType = '';
			$vol = $m[1];
			$no = $m[2];
		} elseif (preg_match('/^Vol(?:\. ?| |ume )(.+?),? n[o°]\.? ?(.+)$/iu', $volno, $m)) { // Vol 9, no 8 -> vol=9,no=8
			$volType = 'Vol. ';
			$vol = $m[1];
			$no = $m[2];
		} elseif (preg_match('/^[Tt]\. (\d\w*?)[, ]+[Nn][o°]\.? ?(.+)$/u', $volno, $m)) { // T. 9, no 8 -> vol=9,no=8
			$volType = 'T. ';
			$vol = $m[1];
			$no = $m[2];
		} elseif (preg_match('/^A\.? (\d.+),? [Nn][o°]\.? ?(.+)$/u', $volno, $m)) { // A 1999, no 8 -> vol=1999,no=8
			$volType = 'A. ';
			$vol = $m[1];
			$no = $m[2];
		} elseif (preg_match('/^Vol(?:\. ?| |ume )(\S+)$/iu', $volno, $m)) { // Vol 9 -> vol=9,no=
			$volType = 'Vol. ';
			$vol = $m[1];
			$no = '';
		} elseif (preg_match('/^[Tt]\. ?(\d+)$/', $volno, $m)) { // T. 9 -> vol=9,no=
			$volType = 'T. ';
			$vol = $m[1];
			$no = '';
		} else {
			$knownPattern = false;
		}

		// il faut quand on a un numéro 0 faire comme si on n'avait pas de numéro
		if ($vol === '0') {
			$vol = '';
		}
		if ($no === '0') {
			$no = '';
		}

		// pas de vol, ni de no, égal à la date
		// signaler si vol ou no est presque égal à la date
		$msg = '';
		if ($year) {
			if ($vol) {
				/*
				if ($vol == $year) {
					$msg = sprintf("CORRECTION de \"%s\" : vol=%d / no=%d / annee=%d => vol=''", $volno, $vol, $no, $year);
					$vol = '';
				}
				 */
				if (abs((int) $vol - $year) < 2) {
					$msg = sprintf("ATTENTION année~vol : vol=%d / annee=%d", $vol, $year);
				}
			}
			if ($no) {
				if (abs((int) $no - $year) < 2) {
					$msg = sprintf("ATTENTION année~no : no=%d / annee=%d", $no, $year);
				}
			}
		}

		// rebuild text volno
		if ($knownPattern) {
			if ($vol) {
				if ($no) {
					$new = "$volType$vol, no $no";
				} else {
					$new = "$volType$vol";
				}
			} else {
				if ($no) {
					$new = "no $no";
				} else {
					$new = "";
				}
			}
		} else {
			$new = $volno;
		}

		// numeric vol/no
		if ($vol) {
			$raw = trim($vol);
			$vol = (string) (int) $vol;
			if (preg_match('/^\d+-\d+$/', $raw)) {
				$msg .= " ATTENTION: volume double";
			} elseif (strlen($vol) < strlen($raw)) {
				$msg .= " ATTENTION: volume abrégé";
			}
		}
		if ($no) {
			$raw = trim($no);
			$no = str_replace('spécial', '', $no);
			$no = (string) (int) trim($no);
			if (preg_match('/^\d+-\d+$/', $raw)) {
				$msg .= " ATTENTION: numéro double";
			} elseif (strlen($no) < strlen($raw)) {
				$msg .= " ATTENTION: numéro abrégé";
			}
		}

		return [
			$rawVolno, // rank 0, col 5 (after s.id, t.id, field, year)
			$new,
			$vol,
			$no,
			$knownPattern ? trim($volType) : 'Aucun motif reconnu', // rank 4
			trim($msg),
		];
	}
}
