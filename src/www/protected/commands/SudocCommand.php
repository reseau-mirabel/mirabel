<?php

use models\sudoc\ApiClient;
use commands\models\sudoc\PpnInfo;

class SudocCommand extends CConsoleCommand
{
	use \commands\models\MaintenanceModeTrait;

	/**
	 * @var int Global parameter, e.g. --verbose=2
	 */
	public $verbose = 1;

	/**
	 * @var bool Global parameter, e.g. --simulation=1
	 */
	public $simulation = false;

	/**
	 * Corrige les doublons d'ISSN sur un même titre. Script à usage unique ?.
	 */
	public function actionFixDuplicatePpn()
	{
		$issns = Issn::model()->findAllBySql(<<<EOSQL
			WITH Duplicates AS (
				SELECT titreId, sudocPpn FROM Issn WHERE sudocPpn <> '' GROUP BY titreId, sudocPpn HAVING count(*) = 2
			)
			SELECT i.* FROM Issn i JOIN Duplicates d ON (i.titreId, i.sudocPpn) = (d.titreId, d.sudocPpn) ORDER BY i.titreId, i.issn
			EOSQL
		);
		for ($i = 0; $i < count($issns); $i++) {
			self::fixDuplicatePpn($issns[$i], $issns[$i + 1]);
			$i++; // read 2 records for each iteration
		}
	}

	public function actionImportFile($file)
	{
		$sudocImport = new \models\sudoc\Import();
		$sudocImport->simulation = (bool) $this->simulation;
		$sudocImport->verbosity = (int) $this->verbose;
		$newfile = preg_replace('/\.xlsx$/i', '_imported.xlsx', $file);
		$sudocImport->file($file, $newfile);
	}

	/**
	 * List ISSNs that have no PPN, according to the Sudoc ISSN2PPN web-service.
	 */
	public function actionIssnsMissingInSudoc()
	{
		$ppnInfo = new PpnInfo();
		$ppnInfo->simulation = (bool) $this->simulation;
		$ppnInfo->csvColumns = 4;
		$ppnInfo->csvSeparator = ";";
		$ppnInfo->csv("ISSN", "dans le SUDOC ?", "PPN", "Sudoc (no)holding");

		$api = new ApiClient();
		$issns = Yii::app()->db->createCommand("SELECT issn FROM Issn WHERE issn IS NOT NULL AND sudocPpn IS NULL")->queryColumn();
		foreach ($issns as $issn) {
			$sudocPpns = $api->getPpn($issn);
			if ($sudocPpns) {
				$ppn = join(
					",",
					array_map(function ($x) {
						return $x->ppn;
					}, $sudocPpns)
				);
				$info = join(
					",",
					array_map(function ($x) {
						return $x->noHolding ? "noHolding" : "";
					}, $sudocPpns)
				);
				$ppnInfo->csv($issn, "1", $ppn, $info);
			} else {
				$ppnInfo->csv($issn, "0", "", "");
			}
		}
	}

	/**
	 * List of ISSNs in M that have a PPN different from what the Sudoc PPN answer
	 */
	public function actionIssnsWithWrongPpn()
	{
		$ppnInfo = new PpnInfo();
		$ppnInfo->simulation = (bool) $this->simulation;
		$ppnInfo->csvColumns = 4;
		$ppnInfo->csvSeparator = ";";
		$ppnInfo->csv("ISSN", "ppnM correct", "PPN de M", "ppnSudoc (web-service)");
		$api = new ApiClient();
		$rows = Yii::app()->db
			->createCommand(<<<EOSQL
				SELECT DISTINCT i.issn, i.sudocPpn AS sudoc
				FROM Titre t
				  JOIN Issn i ON t.id = i.titreId AND i.support = 'electronique'
				WHERE i.issn IS NOT NULL AND i.sudocPpn IS NOT NULL
				ORDER BY t.id
				EOSQL
			)
			->queryAll();
		foreach ($rows as $row) {
			$ppns = [];
			$issn = $row['issn'];
			$sudocPpns = $api->getPpn($issn);
			foreach ($sudocPpns as $sudocPpn) {
				$ppns[] = $sudocPpn->ppn;
			}
			$status = (in_array($row['sudoc'], $ppns) ? "OK" : "ERR?");
			if ($status !== 'OK' || $this->verbose > 1) {
				$ppnInfo->csv($issn, $status, $row['sudoc'], join(' ', $ppns));
			}
			sleep(rand(1, 3));
		}
	}

	/**
	 * Lit un fichier Excel qui permet d'ajouter des PPN-E à partir de l'ISSN.
	 */
	public function actionPpne($args = [])
	{
		if (count($args) !== 1) {
			echo "sudoc ppne </chemin/vers/ppne.xlsx>\n";
			return 1;
		}
		$file = $args[0];
		if (!file_exists($file)) {
			echo "Fichier '$file' introuvable\n";
			return 1;
		}
		(new \commands\models\sudoc\Ppne($file))->run();
	}

	/**
	 * Display a Sudoc notice for each ISSN|PPN passed as argument.
	 *
	 * @param array $args list of ISSN or PPN
	 */
	public function actionQuery($args = [])
	{
		$api = new ApiClient();
		foreach ($args as $identifier) {
			if (strpos($identifier, '-') !== false) {
				$notice = $api->getNoticeByIssn($identifier);
			} else {
				$notice = $api->getNotice($identifier);
			}
			echo "## $identifier\nNotice : ", json_encode($notice, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE), "\n";
		}
	}

	/**
	 * Récupère et enregistre les variantes de titres dans la table dédiée, à partir du Sudoc.
	 *
	 * Si le paramètre $since est fourni, l'interrogation portera seulement sur les titres sans variantes
	 * ou dont les variantes de titres sont plus anciennes que cette date.
	 *
	 * @param string $since ISO timestamp or date, ie "2020-04-18".
	 */
	public function actionTitres(string $since = '')
	{
		$process = new commands\models\SudocTitres($since);
		$process->run();
		return 1;
	}

	public function actionCount801B(int $milisec = 50)
	{
		$countIssn = [
			"ISSN France" => 0,
			"ISSN" => 0,
			"both" => 0,
			"none" => 0,
		];

		$ppnRows = Yii::app()->db->createCommand(
			<<<EOSQL
			SELECT DISTINCT sudocPpn
			FROM Issn
			WHERE sudocPpn IS NOT NULL
			EOSQL
		);
		$api = new ApiClient();

		$count = 0;
		foreach ($ppnRows->queryColumn() as $ppn) {
			try {
				$marc = $api->getMarcTitre($ppn);
				if ($marc === null) {
					sleep(1);
					$marc = $api->getMarcTitre($ppn);
				}
			} catch (\Throwable $e) {
				fprintf(STDERR, "Erreur en interrogeant le SUDOC : %s\n", $e->getMessage());
				continue;
			}
			if ($marc !== null) {
				$values = $marc->readValue('801$b');
				if (in_array("ISSN", $values) && in_array("ISSN France", $values)) {
					$countIssn["both"]++;
				} elseif (in_array("ISSN", $values)) {
					$countIssn["ISSN"]++;
				} elseif (in_array("ISSN France", $values)) {
					$countIssn["ISSN France"]++;
				} else {
					$countIssn["none"]++;
				}
			} else {
				fprintf(STDERR, "PPN %s : Aucune notice trouvée\n", $ppn);
			}
			usleep($milisec * 1000);
			$count++;
			if ($count % 1000 == 0) {
				fputcsv(STDOUT, array_keys($countIssn), ';', '"', '\\');
				fputcsv(STDOUT, array_values($countIssn), ';', '"', '\\');
			}
		}
		fputcsv(STDOUT, array_keys($countIssn), ';', '"', '\\');
		fputcsv(STDOUT, array_values($countIssn), ';', '"', '\\');
	}

	private function fixDuplicatePpn(Issn $recordNoIssn, Issn $recordWithIssn): void
	{
		if ($recordNoIssn['support'] !== Issn::SUPPORT_ELECTRONIQUE) {
			fputcsv(STDOUT, [$recordNoIssn->titreId, $recordWithIssn->issn, "ERREUR", "Le support n'est pas 'électronique'."], ';', '"', '\\');
			return;
		}
		foreach (['dateDebut', 'dateFin', 'worldcatOcn', 'bnfArk'] as $k) {
			if ($recordNoIssn[$k] !== '' && $recordWithIssn[$k] !== '' && $recordNoIssn[$k] !== $recordWithIssn[$k]) {
				fputcsv(STDOUT, [$recordNoIssn->titreId, $recordWithIssn->issn, "ERREUR", "Les champs '$k' divergent."], ';', '"', '\\');
				return;
			}
		}
		$messages = "";
		foreach (['dateDebut', 'dateFin', 'worldcatOcn', 'bnfArk'] as $k) {
			if ($recordNoIssn[$k] !== '' && $recordWithIssn[$k] === '') {
				$messages .= "Reprise de $k. ";
				$recordWithIssn->setAttribute($k, $recordNoIssn->getAttribute($k));
			}
		}
		if ($messages) {
			$recordWithIssn->save(false);
		}
		$recordNoIssn->delete();
		$messages .= "Suppression de l'enregistrement purement PPN.";
		fputcsv(STDOUT, [$recordNoIssn->titreId, $recordWithIssn->issn, "OK", $messages], ';', '"', '\\');
	}
}
