<?php

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class UtilisateurCommand extends CConsoleCommand
{
	public function actionList($login = "")
	{
		$condition = ['order' => 'login'];
		if ($login) {
			$condition["condition"] = "login LIKE '$login'";
		}
		$users = Utilisateur::model()->findAll($condition);
		foreach ($users as $u) {
			printf(" %3d ; %s ; %s\n", $u->id, ($u->motdepasse ? "mdp" : " - "), $u->login);
		}
	}

	public function actionCreate($login, $motdepasse, $partenaireId = 0, $admin=0)
	{
		$user = new Utilisateur();
		$user->login = $login;
		$user->storePassword($motdepasse);
		$user->permAdmin = $admin;
		if (!$partenaireId) {
			echo "Choisir un identifiant de partenaire.\n";
			foreach (Partenaire::model()->findAll() as $p) {
				printf(" - %3d : %s\n", $p->id, $p->nom);
			}
			$partenaireId = $this->prompt("Partenaire ?");
		}
		$user->partenaireId = $partenaireId;
		if (!$user->validate()) {
			echo "Erreur, l'utilisateur n'est pas valide :";
			print_r($user->getErrors());
			return 1;
		}
		if (!$this->confirm("Créer l'utilisateur {$user->login} / {$user->partenaire->nom} ?", true)) {
			return 2;
		}
		if (!$user->save(false)) {
			echo "Erreur, l'utilisateur n'a pas pu être enregistré :";
			print_r($user->getErrors());
			return 1;
		}
		return 0;
	}

	public function actionPassword($id=null, $login=null, $email=null, $args=[])
	{
		if ($id) {
			$user = Utilisateur::model()->findByPk((int) $id);
		} elseif ($login) {
			$user = Utilisateur::model()->findByAttributes(['login' => $login]);
		} elseif ($email) {
			$user = Utilisateur::model()->findByAttributes(['email' => $email]);
		} else {
			$user = null;
		}
		/** @var Utilisateur $user */
		if (!($user instanceof Utilisateur)) {
			echo "L'utilisateur n'a pas été trouvé.\n";
			return 1;
		}
		echo "Utilisateur {$user->login} (id={$user->id})\n";
		if ($args) {
			$password = $args[0];
		} else {
			$password = trim($this->prompt("Nouveau mot de passe ? "));
		}
		if (!$password) {
			echo "Pas de mot de passe. Abandon.\n";
			return 2;
		}
		$user->storePassword($password);
		if ($user->save(false)) {
			echo "Le nouveau mot de passe a été attribué.\n";
			return 0;
		}
		echo "Error writing the new password.";
		return 3;
	}
}
