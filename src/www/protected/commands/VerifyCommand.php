<?php

use processes\urlverif\UrlMassCheck;

class VerifyCommand extends CConsoleCommand
{
	use \commands\models\MaintenanceModeTrait;

	/**
	 * @var int
	 */
	public $verbose = 0;

	/**
	 * @var int If set to a positive value, will stop global check after this many iterations.
	 */
	public $limit = 0;

	/**
	 * Search for bad URLs in Cms content.
	 */
	public function actionLinksCms(): int
	{
		$this->verifyLinks("Cms", false, false);
		return 0;
	}

	/**
	 * Search for bad URLs in Revue/Titre.
	 */
	public function actionLinksRevues(string $partiel = ''): int
	{
		$this->verifyLinks("Revue", (bool) $partiel, true);
		return 0;
	}

	/**
	 * Search for bad URLs.
	 */
	public function actionLinksEditeurs(string $partiel = ''): int
	{
		$this->verifyLinks("Editeur", (bool) $partiel, false);
		return 0;
	}

	/**
	 * Search for bad URLs.
	 */
	public function actionLinksRessources($partiel = false): int
	{
		$this->verifyLinks("Ressource", (bool) $partiel, false);
		return 0;
	}

	/**
	 * @param "Cms"|"Editeur"|"Ressource"|"Revue" $className
	 */
	private function verifyLinks(string $className, bool $partiel, bool $outputCsv): void
	{
		$urlCheck = new UrlMassCheck($partiel); // Slower checks when only broken URLs are tested.
		$urlCheck->verbose = (int) $this->verbose;
		if ($outputCsv) {
			$urlCheck->setCsvOutput(STDOUT);
		}

		if ($partiel) {
			$urlCheck->removeObsoleteLinks($className);
			$urlCheck->checkBrokenLinks($className);
		} else {
			$criteria = $this->limit > 0 ? ['limit' => (int) $this->limit] : [];
			$records = $className::model()->findAll($criteria);
			$urlCheck->checkAllLinks($className, UrlMassCheck::extractLinks($records));
		}

		if ($this->verbose > 0) {
			echo $urlCheck->getStats($className);
		}
	}
}
