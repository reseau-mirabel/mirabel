<?php

use components\curl\Downloader;
use processes\attribut\FileImport;
use processes\attribut\ImportConfig;

class AttributCommand extends CConsoleCommand
{
	public function actionImport(string $uri, string $config)
	{
		$parsedConfig = json_decode($config, true, 512, JSON_THROW_ON_ERROR);
		try {
			$iconfig = new ImportConfig($parsedConfig);
		} catch (\Exception $e) {
			echo "ERREUR: ", $e->getMessage(), "\n";
			return 1;
		}

		$attribut = Sourceattribut::model()->findByPk($iconfig->sourceattributId);
		if (!($attribut instanceof Sourceattribut)) {
			fprintf(STDERR, "Aucun attribut n'a cet identifiant : %s\n", $iconfig->sourceattributId);
			return 1;
		}

		$log = new AttributImportLog();
		$log->sourceattributId = (int) $attribut->id;
		$log->userId = null;
		if (!self::storeFile($uri, $log)) {
			return 1;
		}

		$process = new FileImport($log, $iconfig);
		if (!$process->save()) {
			fprintf(STDERR, "Erreur lors de l'enregistrement.");
			return 1;
		}
		$log->logtime = time();
		$log->save(false);

		$report = $process->getReport();
		if ($report->numChanges > 0 || $report->numDeletes > 0) {
			printf("%d valeurs enregistrées\n", $report->numChanges);
			printf("%d valeurs supprimées\n", $report->numDeletes);
			printf(
				"%s/attribut/report/%d\n",
				Yii::app()->params->itemAt('baseUrl'),
				$process->getLog()->id
			);
		}

		return 0; // OK
	}

	public function actionUnknown(string $identifiant)
	{
		try {
			$report = \processes\attribut\Report::loadLast($identifiant);
		} catch (\Exception $ex) {
			fprintf(STDERR, $ex->getMessage() . "\n");
			return 1;
		}

		if (!$report->unknownTitles) {
			fprintf(STDERR, "Le rapport de cet import ne contient pas de liste de titres inconnus.");
			return 1;
		}

		fputcsv(STDOUT, ["Ligne", "Titre", "URL de création"], "\t", '"', '\\');
		foreach ($report->unknownTitles as $position => [$issns, $titre]) {
			$url = "";
			if ($issns) {
				$url = Yii::app()->getParams()->itemAt('baseUrl') . "/titre/create-by-issn?issn=" . str_replace(" ", "+", $issns);
			}
			fputcsv(STDOUT, [$position, $titre, $url], "\t", '"', '\\');
		}
		return 0; // OK
	}

	private function storeFile(string $uri, AttributImportLog $log): bool
	{
		if (strncmp($uri, "http", 4) === 0) {
			$downloader = new Downloader('tableur');
			try {
				$downloader->download($uri);
				$sourceFile = $downloader->getLocalFilepath();
				$log->url = $uri;
				$log->fileName = $downloader->getRemoteFilename();
				$log->fileKey = FileImport::storeFile($sourceFile);
			} catch (Throwable $e) {
				fprintf(STDERR, "Erreur lors du téléchargement : {$e->getMessage()}");
				return false;
			}
		} else {
			$tmpFile = tempnam(sys_get_temp_dir(), 'mirabel_');
			copy($uri, $tmpFile);
			$log->url = '';
			$log->fileName = basename($uri);
			$log->fileKey = FileImport::storeFile($tmpFile, CFileHelper::getExtension($uri));
		}
		if (!$log->fileKey) {
			fprintf(STDERR, "Erreur, données incomplètes.");
			return false;
		}
		return true;
	}
}
