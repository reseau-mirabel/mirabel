<?php

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class NormalizeCommand extends CConsoleCommand
{
	public function actionEditeurGeo()
	{
		$remplacements = [
			"/, (Côte d'Or|Ile-de-France|Nord-Pas-de-Calais)/" => '',
			'/ \([ \d]+\)/' => '',
			'/ \(([^)]+)\)/' => ', $1',
			'/ Mass\./' => 'Massachussets',
		];
		$editeurs = Yii::app()->db
			->createCommand(
				"SELECT id, geo FROM Editeur "
				. "WHERE geo LIKE '%\t%' OR geo LIKE ' %' OR geo LIKE '% ' OR geo LIKE '%(%' OR geo LIKE '%, %'"
			)->queryAll(false);
		$update = Yii::app()->db
			->createCommand("UPDATE Editeur SET geo = :geo WHERE id = :id");
		$updated = 0;
		foreach ($editeurs as $row) {
			[$id, $geo] = $row;
			$newgeo = preg_replace(
				array_keys($remplacements),
				array_values($remplacements),
				trim(str_replace("\t", "", $geo))
			);
			if ($newgeo !== $geo) {
				printf("%d\t%s\t%s\n", $id, $newgeo, $geo);
				$updated += $update->execute([':geo' => $newgeo, ':id' => $id]);
			} else {
				fprintf(STDERR, "Attention, pas de changement pour : %d / %s\n", $id, $geo);
			}
		}
		fprintf(STDERR, "%d champs geo modifiés\n", $updated);
	}
}
