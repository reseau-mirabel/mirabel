<?php

/**
 * ./yii cache [action]
 */
class CacheCommand extends CConsoleCommand
{
	public function actionFlush(): int
	{
		$this->actionMain();
		$this->actionAssets();
		return 0;
	}

	/**
	 * flush the main cache
	 */
	public function actionMain(): int
	{
		Yii::app()->db->schema->refresh();
		print("Schema cache refreshed.\n");

		foreach (['cache'] as $cacheId) {
			if (Yii::app()->hasComponent($cacheId)) {
				$component = Yii::app()->getComponent($cacheId);
				if (($component instanceof \ICache) && $component->flush()) {
					printf("Component '%s' flushed.\n", $cacheId);
				} else {
					printf("Component '%s': flushing failed.\n", $cacheId);
				}
			} else {
				printf("No cache component named '%s'.\n", $cacheId);
			}
		}
		return 0;
	}

	/**
	 * clear assets folder
	 */
	public function actionAssets(): int
	{
		print("Flushing Yii assets...\n");
		$assetPath = realpath(DATA_DIR . '/cache/assets');
		printf("\tAssuming assets are stored in '%s'.\n", $assetPath);

		if (!$assetPath || !is_dir($assetPath)) {
			printf("\tAsset path '%s' is invalid.\n", $assetPath);
			return 1;
		}

		$dirs = self::collectAssetTargets($assetPath);
		printf("\tFound %d directories... ", count($dirs));
		if (!empty($dirs)) {
			// inverse order of directories to avoid can not delete non-empty directory
			foreach ($dirs as $dir) {
				if (!self::unlinkRecursive($dir)) {
					printf("\n\tDeleting of asset dir '%s' failed.\n", $dir);
				}
			}
		}
		echo "Done\n";
		return 0;
	}

	/**
	 * recurse into given path and collect files and folders
	 *
	 * @param string $assetPath
	 * @return string[] dirs
	 */
	protected static function collectAssetTargets(string $assetPath): array
	{
		$assetfiles = glob($assetPath . DIRECTORY_SEPARATOR . "*");
		$dirs = [];
		foreach ($assetfiles as $assetFile) {
			if ($assetFile[0] === ".") {
				continue;
			}
			if (is_dir($assetFile)) {
				$dirs[] = $assetFile;
			}
		}
		return $dirs;
	}

	protected static function unlinkRecursive(string $path): bool
	{
		if (is_dir($path)) {
			$dh = @opendir($path);
			if (!$dh) {
				return false;
			}
			$success = true;
			while (($obj = readdir($dh)) !== false) {
				$subpath = $path . DIRECTORY_SEPARATOR . $obj;
				if ($obj !== '.' && $obj !== '..') {
					if (is_dir($subpath)) {
						$success = $success && self::unlinkRecursive($subpath);
					} else {
						$success = $success && unlink($subpath);
					}
				}
			}
			closedir($dh);
			return $success && rmdir($path);
		}
		return unlink($path);
	}
}
