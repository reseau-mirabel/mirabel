<?php

use commands\models\LinksObsolescence;
use commands\models\LinkUpdater;
use commands\models\linkimport;

class LinksCommand extends CConsoleCommand
{
	use \commands\models\MaintenanceModeTrait;

	/**
	 * @var array|string Array if --email is repeated.
	 */
	public $email = [];

	public $simulation = 0;

	/**
	 * @var int verbose = 1: CSV lists updated but unchanged links
	 */
	public int $verbose = 0;

	/**
	 * @var int If set to a positive value, will stop after this many iterations.
	 */
	public int $limit = 0;

	public string $obsoleteSince = '';

	public string $obsoleteDelete = '0';

	public function actionOpenalex(): int
	{
		$importer = new linkimport\Openalex(get_object_vars($this));
		echo $importer->import();
		return 0;
	}

	/**
	 * Create CSV files that list potential errors for DOAJ and ROAD. Not for cron.
	 */
	public function actionCheckDoajRoad(): int
	{
		foreach (['road', 'doaj'] as $src) {
			$filename = $src . "_revues-sans-accès-libre-txtintegral.csv";
			echo $filename, "\n";
			$handle = fopen($filename, "w");
			fputcsv($handle, ["revueId"], "\t", '"', '\\');
			$sourceId = (int) Yii::app()->db
				->createCommand("SELECT id FROM Sourcelien WHERE nomcourt = :n")
				->queryScalar([':n' => $src]);
			$sql = <<<EOSQL
				SELECT t.revueId, MAX(t.titre) AS titre
				FROM Titre t
				    JOIN LienTitre l ON l.titreId = t.id AND l.sourceId = $sourceId
				    LEFT JOIN Service s ON s.titreId = t.id AND s.type = 'Intégral' AND s.acces = 'Libre'
				GROUP BY t.revueId
				    HAVING count(s.id) = 0
				EOSQL;
			$sth = Yii::app()->db->createCommand($sql)->query();
			$sth->setFetchMode(PDO::FETCH_NUM);
			foreach ($sth as $row) {
				fputcsv($handle, $row, "\t", '"', '\\');
			}
		}
		return 0;
	}

	public function actionCairnInt(string $csv): int
	{
		if (!file_exists($csv)) {
			echo "Fichier CSV '$csv' non trouvé.\n";
			return 1;
		}
		$importer = new linkimport\CairnInt(get_object_vars($this));
		$importer->csvFile = $csv;
		echo $importer->import();
		return 0;
	}

	public function actionCairnMundo(string $csv): int
	{
		if (!file_exists($csv)) {
			echo "Fichier CSV '$csv' non trouvé.\n";
			return 1;
		}
		$importer = new linkimport\CairnMundo(get_object_vars($this));
		$importer->csvFile = $csv;
		echo $importer->import();
		return 0;
	}

	public function actionScopus(string $url = ""): int
	{
		$importer = new linkimport\Scopus(get_object_vars($this));
		if ($url) {
			$importer->xlsx = $url;
		}
		echo $importer->import();
		return 0;
	}

	public function actionMiar(string $csv): int
	{
		if (strncmp($csv, 'http', 4) !== 0 && !file_exists($csv)) {
			echo "Fichier CSV '$csv' non trouvé.\n";
			return 1;
		}
		$importer = new linkimport\Miar(get_object_vars($this));
		$importer->csvFile = $csv;
		echo $importer->import();
		return 0;
	}

	public function actionEzb(string $csv): int
	{
		if (strncmp($csv, 'http', 4) !== 0 && !file_exists($csv)) {
			echo "Fichier CSV '$csv' non trouvé.\n";
			return 1;
		}
		$importer = new linkimport\Ezb(get_object_vars($this));
		$importer->csvFile = $csv;
		echo $importer->import();
		return 0;
	}

	/**
	 * Import the DOAJ links by downloading a CSV file.
	 */
	public function actionDoaj(): int
	{
		$importer = new linkimport\Doaj(get_object_vars($this));
		echo $importer->import();
		return 0;
	}

	/**
	 * Remove the DOAJ links using a CSV file.
	 *
	 * @param string $tsv File path
	 */
	public function actionDoajRemove($tsv): int
	{
		if (!file_exists($tsv)) {
			echo "Fichier TSV '$tsv' non trouvé.\n";
			return 1;
		}

		$linkUpdater = new LinkUpdater(Sourcelien::model()->findByAttributes(['nomcourt' => 'doaj']));
		$linkUpdater->simulation = $this->simulation;
		$linkUpdater->verbose = $this->verbose;

		$linkUpdater->removeByIssn($tsv, 'https://doaj.org/toc/{{id}}');
		fprintf(STDERR, $linkUpdater->getStats());
		return 0;
	}

	/**
	 * Import the ROAD links from a XML MARC21 file, downloaded if not up to date.
	 */
	public function actionRoad(): int
	{
		$importer = new linkimport\Road(get_object_vars($this));
		echo $importer->import();
		return 0;
	}

	public function actionErih(): int
	{
		$importer = new linkimport\Erih(get_object_vars($this));
		echo $importer->import();
		return 0;
	}

	/**
	 * Import the HAL links from their REST API.
	 *
	 * @return int
	 */
	public function actionHal(): int
	{
		$importer = new linkimport\Hal(get_object_vars($this));
		echo $importer->import();
		return 0;
	}

	/**
	 * Import the Latindex links by downloading a JSON dump.
	 */
	public function actionLatindex(): int
	{
		$importer = new linkimport\Latindex(get_object_vars($this));
		echo $importer->import();
		return 0;
	}

	/**
	 * Import the WOS (Web of Science) links by downloading a CSV file.
	 */
	public function actionWos(string $csv): int
	{
		if (strncmp($csv, 'http', 4) !== 0 && !file_exists($csv)) {
			echo "Fichier CSV '$csv' non trouvé.\n";
			return 1;
		}
		$importer = new linkimport\Wos(get_object_vars($this));
		$importer->csvFile = $csv;
		echo $importer->import();
		return 0;
	}

	/**
	 * Détecte et supprime les liens de titres (ignore les liens d'éditeurs)
	 * qui n'ont pas été importés et qui jouxtent un lien importé de même titre et même source.
	 *
	 * @param string $sourceId Si 0 ou vide, cherche pour toutes les sources de liens.
	 */
	public function actionDeleteUnimportedDuplicates(string $sourceId): int
	{
		$sId = (int) $sourceId;
		$condition = ($sourceId > 0 ? "WHERE Imported.sourceId = $sId" : "");
		$titres = Titre::model()->findAllBySql(<<<EOSQL
			SELECT t.id, t.titre, t.liensJson, l.url, l.sourceId AS revueId
			FROM Titre t
				JOIN LienTitre Imported ON Imported.titreId = t.id
				JOIN ImportedLink i ON i.sourceId = Imported.sourceId AND i.titreId = t.id AND i.url = Imported.url
				JOIN LienTitre l ON l.sourceId = Imported.sourceId AND l.titreId = t.id AND l.id <> Imported.id AND l.url <> Imported.url
			$condition
			GROUP BY t.id, Imported.sourceId
			EOSQL
		);
		foreach ($titres as $titre) {
			assert($titre instanceof Titre);
			$liens = $titre->getLiens();
			$urlToRemove = $titre->url;
			$localSourceId = (int) $titre->revueId;
			$hasChanged = false;
			foreach ($liens as $l) {
				assert($l instanceof Lien);
				if ((int) $l->sourceId === (int) $localSourceId && $l->url === $urlToRemove) {
					fputcsv(STDOUT, [$titre->id, $titre->titre, $l->url], ';', '"', '\\');
					if (!$this->simulation) {
						$liens->remove($l);
						$hasChanged = true;
						break;
					}
				}
			}
			if ($hasChanged) {
				$titre->liensJson = json_encode($liens, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
				if ($titre->saveAttributes(['liensJson'])) {
					$liens->save("Titre", $titre->id);
				}
			}
		}
		return 0;
	}

	public function actionDeleteByCsv($sourceId, $csv)
	{
		$handle = fopen($csv, "r");
		while ($row = fgetcsv($handle, 0, ";")) {
			$titre = Titre::model()->findByPk($row[0]);
			assert($titre instanceof Titre);
			$liens = $titre->getLiens();
			$urlToRemove = $row[2];
			$hasChanged = false;
			foreach ($liens as $l) {
				assert($l instanceof Lien);
				if ((int) $l->sourceId === (int) $sourceId && $l->url === $urlToRemove) {
					fputcsv(STDOUT, [$titre->id, $titre->titre, $l->url], ';', '"', '\\');
					if (!$this->simulation) {
						$liens->remove($l);
						$hasChanged = true;
						break;
					}
				}
			}
			if ($hasChanged) {
				$titre->liensJson = json_encode($liens, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
				if ($titre->saveAttributes(['liensJson'])) {
					$liens->save("Titre", $titre->id);
				}
			}
		}
		return 0;
	}

	/**
	 * Lists links imported but not seen recently, for all the sources.
	 */
	public function actionRemoved(string $since): int
	{
		if (ctype_digit($since)) {
			$timestamp = (int) $since;
		} else {
			$timestamp = (int) strtotime($since);
		}
		[$count, $content] = (new LinksObsolescence(null))->detectObsolete($timestamp);
		if ($content) {
			printf("\n## $count liens obsolètes (pas importés depuis %s)\n", date('Y-m-d', $timestamp));
			echo $content;
		}
		return 0;
	}

	/**
	 * Used exceptionally to set the import date of some links. Not for the cron nor unmodified usage.
	 *
	 * @param string $date Set the import date to this.
	 * @return int
	 */
	public function actionResetImported(string $date): int
	{
		$timestamp = strtotime($date);
		$sourcesImported = Yii::app()->db
			->createCommand("SELECT id FROM Sourcelien WHERE nomcourt IN ('doaj', 'road')")
			->queryColumn();
		foreach (Titre::model()->findAll() as $titre) {
			assert($titre instanceof \Titre);
			foreach ($titre->getLiens() as $lien) {
				/** @var Lien $lien */
				if (in_array($lien->sourceId, $sourcesImported)) {
					Yii::app()->db
						->createCommand("INSERT IGNORE INTO ImportedLink VALUES (UNHEX(:hash), :sid, :tid, :url, :ts)")
						->execute([
							':hash' => sha1($lien->url),
							':sid' => $lien->sourceId,
							':tid' => $titre->id,
							':url' => $lien->url,
							':ts' => $timestamp,
						]);
				}
			}
		}
		return 0;
	}

	public function actionReencode()
	{
		$this->reencode("Titre");
		$this->reencode("Editeur");
		$this->reencodeInterventions();

		$tChanges = $this->syncLinkTables("Titre");
		if ($tChanges) {
			echo "$tChanges enregistrements ajoutés dans LienTitre\n";
		}
		$eChanges = $this->syncLinkTables("Editeur");
		if ($eChanges) {
			echo "$eChanges enregistrements ajoutés dans LienEditeur\n";
		}
	}

	public function actionRename($id, $newName, $oldName = '')
	{
		$source = Sourcelien::model()->findByPk($id);
		if (!($source instanceof Sourcelien)) {
			echo "Bad ID";
			return 1;
		}
		$quotedOldName = \Yii::app()->db->quoteValue($oldName ?: $source->nom);
		$quotedNewName = \Yii::app()->db->quoteValue($newName);

		foreach (['Editeur', 'Titre'] as $table) {
			$sql = <<<SQL
				UPDATE $table
				SET liensJson = json_replace(liensJson, json_unquote(json_search(liensJson, 'one', $quotedOldName)), $quotedNewName)
				WHERE liensJson <> '[]'
					AND json_contains(json_extract(liensJson, '$[*].sourceId'), '{$source->id}') = 1
					AND json_search(liensJson, 'one', $quotedOldName) IS NOT NULL
				SQL;
			\Yii::app()->db->createCommand($sql)->execute();
			\Yii::app()->db->createCommand("UPDATE Lien{$table} SET name = $quotedNewName WHERE sourceId = {$source->id}")->execute();
		}

		$source->nom = $newName;
		$source->save(false);
	}

	/**
	 * Réencode le champ SQL $table.liensJson de façon optimale.
	 */
	private function reencode(string $table): void
	{
		$query = \Yii::app()->db->createCommand("SELECT id, liensJson FROM $table WHERE liensJson <> '[]'");
		foreach ($query->query() as $row) {
			$links = json_decode($row['liensJson'], false);
			foreach ($links as $k => $l) {
				if (isset($l->sourceId)) {
					$links[$k]->sourceId = ($l->sourceId > 0 ? (int) $l->sourceId : null);
				}
			}
			$encoded = json_encode($links, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
			if ($encoded !== $row['liensJson']) {
				\Yii::app()->db->createCommand("UPDATE $table SET liensJson = :l WHERE id = :id")
					->execute([':l' => $encoded, ':id' => $row['id']]);
			}
		}
	}

	private function reencodeInterventions(): void
	{
		$query = \Yii::app()->db->createCommand("SELECT id, contenuJson FROM Intervention WHERE contenuJson LIKE '%liensJson%'");
		foreach ($query->query() as $row) {
			$contents = json_decode($row['contenuJson'], false);
			if (!empty($contents->content[0]->before->liensJson)) {
				if (is_string($contents->content[0]->before->liensJson)) {
					$links = json_decode($contents->content[0]->before->liensJson, false);
				} else {
					$links = $contents->content[0]->before->liensJson;
				}
				if (is_object($links)) {
					$links = array_values((array) $contents->content[0]->before->liensJson);
				}
				foreach ($links as $k => $l) {
					if (isset($l->sourceId)) {
						$links[$k]->sourceId = ($l->sourceId > 0 ? (int) $l->sourceId : null);
					}
				}
				$contents->content[0]->before->liensJson = json_encode($links, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
			}
			if (!empty($contents->content[0]->after->liensJson)) {
				if (is_string($contents->content[0]->after->liensJson)) {
					$links = json_decode($contents->content[0]->after->liensJson, false);
				} else {
					$links = $contents->content[0]->after->liensJson;
				}
				if (is_object($links)) {
					$links = array_values((array) $contents->content[0]->after->liensJson);
				}
				if (!is_array($links)) {
					print_r($links);
					echo $contents->content[0]->after->liensJson . "\n\n";
				}
				foreach ($links as $k => $l) {
					if (isset($l->sourceId)) {
						$links[$k]->sourceId = ($l->sourceId > 0 ? (int) $l->sourceId : null);
					}
				}
				$contents->content[0]->after->liensJson = json_encode($links, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
			}
			$encoded = json_encode($contents, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
			if ($encoded !== $row['contenuJson']) {
				\Yii::app()->db->createCommand("UPDATE Intervention SET contenuJson = :l WHERE id = :id")
					->execute([':l' => $encoded, ':id' => $row['id']]);
			}
		}
	}

	/**
	 * Create the XXXLiens records from the $table.liensJson.
	 * Does not delete any record.
	 */
	private function syncLinkTables(string $table): int
	{
		$lcTable = strtolower($table);
		$exists = \Yii::app()->db->createCommand("SELECT 1 FROM Lien{$table} WHERE {$lcTable}Id = :id AND url = :url");
		$insert = \Yii::app()->db->createCommand("INSERT Lien{$table} ({$lcTable}Id, sourceId, domain, name, url) VALUES (:id, :sid, :domain, :name, :url)");

		$count = 0;
		$query = \Yii::app()->db->createCommand("SELECT id, liensJson FROM $table WHERE liensJson <> '[]'");
		foreach ($query->query() as $row) {
			$links = json_decode($row['liensJson'], false);
			foreach ($links as $l) {
				if (!$exists->queryScalar([':id' => $row['id'], ':url' => $l->url])) {
					$insert->execute([
						':id' => $row['id'],
						':sid' => empty($l->sourceId) ? null : (int) $l->sourceId,
						':domain' => Liens::extractDomain($l->url),
						':name' => $l->src,
						':url' => $l->url,
					]);
					$count++;
				}
			}
		}
		return $count;
	}
}
