<?php

use components\email\Mailer;

class EmailCommand extends CConsoleCommand
{
	public function actionSend($to, $subject, $bodyFile, $attachFile = "")
	{
		if (!$bodyFile || !file_exists($bodyFile)) {
			echo "Missing: --bodyFile=/path/to/XXX\n";
			return 1;
		}
		if (strpos($to, "@") === false) {
			if (ctype_digit($to)) {
				$user = Utilisateur::model()->findByPk($to);
			} elseif (strpos($to, "@") === false) {
				$user = Utilisateur::model()->findByAttributes(['login' => $to]);
			}
			if (!isset($user)) {
				echo "Destinataire incorrect : ID | log | email\n";
				return 1;
			}
			assert($user instanceof Utilisateur);
			$to = $user->email;
		}
		$message = Mailer::newMail()
			->subject(sprintf("[%s] %s", Yii::app()->name, $subject))
			->from(Config::read('email.from'))
			->setTo($to)
			->text(file_get_contents($bodyFile));
		if ($attachFile && file_exists($attachFile)) {
			$message->attachFromPath($attachFile);
		}
		$replyTo = Config::read('email.replyTo');
		if ($replyTo) {
			$message->replyTo($replyTo);
		}
		Mailer::sendMail($message);
	}
}
