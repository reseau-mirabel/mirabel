<?php

use commands\models\linkimport;
use models\sherpa\Api;

class SherpaCommand extends CConsoleCommand
{
	use \commands\models\MaintenanceModeTrait;

	public $verbose = 1;

	public function actionPublication(string $titreId): int
	{
		$key = Yii::app()->params->itemAt('sherpa')['api-key'];
		$api = new Api($key);
		$issns = \Yii::app()->db
			->createCommand("SELECT issn FROM Issn WHERE titreId = :id AND issn <> ''")
			->queryColumn([':id' => $titreId]);
		if (!$issns) {
			echo "Aucun ISSN pour interroger Sherpa\n";
			return 0;
		}
		$item = $api->fetchPublication((int) $titreId, $issns);
		fprintf(STDERR, "First item of the response, as parsed by M:\n");
		echo json_encode($item, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
		return 1;
	}

	public function actionTest(string $issn = '1179-3163'): int
	{
		$key = Yii::app()->params->itemAt('sherpa')['api-key'];
		$api = new Api($key);
		$json = $api->fetchPublicationByIssn($issn);
		$publications = json_decode($json); // into \stdClass
		if (empty($publications->items)) {
			echo "Empty response, unknown ISSN.\n";
			exit(0);
		}
		$item = new models\sherpa\Item($publications->items[0]);
		fprintf(STDERR, "First item of the response, as parsed by M:");
		echo json_encode($item, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
		return 1;
	}

	/**
	 * Import the links and store the raw responses of Sherpa into the Sherpa* tables.
	 */
	public function actionImport(array $email = [], int $limit = 0, string $obsoleteSince = '', int $obsoleteDelete = 0): int
	{
		$params = [
			'email' => $email,
			'limit' => $limit,
			'obsoleteDelete' => $obsoleteDelete,
			'obsoleteSince' => $obsoleteSince,
			'verbose' => $this->verbose,
		];
		$importer = new linkimport\Sherpa($params);
		$importer->import();
		return 0;
	}

	public function actionIdentifyPublishers()
	{
		$jsons = Yii::app()->db->createCommand("SELECT content FROM SherpaPublication")->queryColumn();
		foreach ($jsons as $json) {
			$data = json_decode($json, false);
			if (empty($data->publishers) || empty($data->issns[0]->issn)) {
				continue;
			}
			$publishers = $data->publishers;
			$issn = $data->issns[0]->issn;
			$editeurs = Editeur::model()->findAllBySql(
				<<<EOSQL
				SELECT e.*
				FROM Editeur e
					JOIN Titre_Editeur te ON e.id = te.editeurId
					JOIN Issn i USING(titreId)
				WHERE i.issn = :issn AND e.sherpa IS NULL
				EOSQL,
				[':issn' => $issn]
			);
			foreach ($editeurs as $e) {
				assert($e instanceof Editeur);
				foreach ($publishers as $publisher) {
					$p = $publisher->publisher;
					if (
						$e->nom === $p->name[0]->name
						|| (isset($p->url) && rtrim($e->url, "/") === rtrim($p->url, "/"))
					) {
						$i = $e->buildIntervention(true);
						$i->action = 'editeur-U';
						$i->contenuJson->updateByAttributes($e, ['sherpa' => $p->id]);
						$i->accept(true);
						$e->sherpa = $p->id;
						fputcsv(STDOUT, [$e->id, $e->nom, $e->sherpa, $issn], ';', '"', '\\');
					}
				}
			}
		}
	}
}
