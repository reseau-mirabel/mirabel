<?php

use processes\intervention\MailAlert;

/**
 * Alert by e-mail
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class AlertCommand extends CConsoleCommand
{
	public $verbose = 0;

	public $simulation = false;

	public $since = 0;

	public $before = 0;

	public $cc = '';

	public $ccToPartEd = '';

	public $ccToPartVe = '';

	private MailAlert $mailAlert;

	/**
	 * Default action, called when no argument is given.
	 */
	public function actionIndex()
	{
		echo $this->getHelp();
	}

	public function getHelp()
	{
		return <<<EOL
			./yii alert who
			    Liste les partenaires faisant du suivi.
			./yii alert all [--simulation=1] [--since=2012-10-25]
			    Envoie un courriel à chaque établissement ayant des interventions en attente.
			    Idem pour les objets non suivis.
			./yii alert <nom|id> [--simulation=1] [--since=2012-10-25]
			    Envoie un courriel à l'établissement s'il a des interventions en attente.
			./yii alert nonsuivi
			    Envoie un courriel aux utilisateurs surveillant les interventions en attente non suivies.
			./yii alert partenairesEditeurs
			    Envoie un courriel aux utilisateurs surveillant les interventions des partenaires-éditeurs.

			Options :
			    --simulation=1 : ne pas envoyer de courriel, afficher dans le terminal.
			    --since=<date> : interventions postérieures à cette date (par défaut, les dernières 24h).
			    --verbose=<0..3>  : fixe la verbosité des messages dans la console (par défaut, 1, décompte des envois).
			    --ccToPartVe=<email> : (suivi/part) copie des alertes dont le destinataire est un partenaire veilleur
			    --ccToPartEd=<email> : (suivi/part) copie des alertes dont le destinataire est un partenaire éditeur
			    --cc=<email>   : copie des alertes, inactif pour le suivi si un des --ccTo* est défini.

			Exemples :
			* Pour envoyer à chaque partenaire (ayant une adresse e-mail)
			  les propositions en attente créée dans la journée :
			      ./yii alert all
			  équivalent à :
			      php yii alert all --since="1 day ago"
			* Pour voir dans la console ce qui serait envoyé à Sciences Po Lyon
			  si on demandait les propositions depuis la mi-octobre 2012 :
			      ./yii alert "Sciences Po Lyon" --simulation=1 --verbose=2 --since="2012-10-15 12:00"

			EOL;
	}

	public function beforeAction($action, $params)
	{
		$this->mailAlert = $this->initMailAlert();
		return parent::beforeAction($action, $params);
	}

	/**
	 * Action "all" that alerts everyone.
	 */
	public function actionAll()
	{
		$this->mailAlert->alertInterventionSuivi();
		$this->mailAlert->alertInterventionNonSuivi();
		$this->mailAlert->alertInterventionPartenaireEditeurs();
		echo $this->mailAlert->getLogString($this->getLogLevel());
	}

	public function actionSuivi()
	{
		$this->mailAlert->alertInterventionSuivi();
		echo $this->mailAlert->getLogString($this->getLogLevel());
	}

	/**
	 * Action "who" that lists the possible names.
	 */
	public function actionWho()
	{
		echo "Partenaires faisant du suivi :\n";
		$cmdSuivi = Yii::app()->db->createCommand(
			"SELECT p.id, p.nom "
			. "FROM Partenaire p JOIN Suivi s ON (s.partenaireId = p.id) "
			. "GROUP BY p.id ORDER BY p.nom"
		);
		foreach ($cmdSuivi->query() as $row) {
			printf(" * %3d - %s\n", $row['id'], $row['nom']);
		}
		echo "Utilisateurs en charge des objets non suivis :\n";
		$cmdNonSuivi = Yii::app()->db->createCommand(
			"SELECT u.id, u.login, u.nomComplet "
			. "FROM Utilisateur u "
			. "WHERE suiviNonSuivi = 1 ORDER BY u.nom"
		);
		foreach ($cmdNonSuivi->query() as $row) {
			printf(" * %20s - %s\n", $row['login'], $row['nomComplet']);
		}
	}

	/**
	 * Action "nonsuivi".
	 */
	public function actionNonSuivi()
	{
		$this->mailAlert->alertInterventionNonSuivi();
		echo $this->mailAlert->getLogString($this->getLogLevel());
	}

	/**
	 * Action "partenaires-editeurs" will send an email about each recent Intervention pending validation
	 * that was proposed by an user belonging to a Partenaire-Éditeur.
	 */
	public function actionPartenairesEditeurs()
	{
		$this->mailAlert->alertInterventionPartenaireEditeurs();
		echo $this->mailAlert->getLogString($this->getLogLevel());
	}

	/**
	 * Intercepts the action call so that Partenaire name/ID can be redirected.
	 * This takes place before "beforeAction()" and can prevent running a normal action.
	 *
	 * @param array $args
	 */
	public function run($args)
	{
		[$action, $options] = $this->resolveRequest($args);
		if ($action) {
			if (method_exists($this, 'action' . $action)) {
				return parent::run($args);
			}
			if (ctype_digit($action)) {
				$p = Partenaire::model()->findByPk($action);
			} else {
				$p = Partenaire::model()->findByAttributes(['nom' => $action]);
			}
			if (!$p) {
				echo "Aucun partenaire ne correspond.\n\n";
				echo $this->help;
				return 1;
			}

			// Parse and fill-in the controller since we have short-cut the normal "action" process.
			$this->verbose = $options['verbose'] ?? 1;
			$this->simulation = isset($options['simulation']) ? (boolean) $options['simulation'] : false;
			$this->since = $options['since'] ?? '';
			$this->before = $options['before'] ?? '';

			$this->mailAlert = $this->initMailAlert();
			$errorCode = $this->mailAlert->alertOnly($p);
			echo $this->mailAlert->getLogString($this->getLogLevel());
			return $errorCode;
		}
		return parent::run($args);
	}

	private function initMailAlert(): MailAlert
	{
		$ma = new MailAlert($this->since, $this->before, $this->simulation);
		$ma->setRecipients($this->ccToPartEd, $this->ccToPartVe, $this->cc);
		return $ma;
	}

	private function getLogLevel(): string
	{
		return match ((int) $this->verbose) {
			0 => \CLogger::LEVEL_WARNING,
			1 => \CLogger::LEVEL_INFO,
			2 => \CLogger::LEVEL_TRACE,
			default => \CLogger::LEVEL_WARNING,
		};
	}
}
