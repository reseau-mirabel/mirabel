<?php

/**
 * Import d'accès
 */
class ImportCommand extends CConsoleCommand
{
	use \commands\models\MaintenanceModeTrait;

	public $verbose = 1;

	public $simulation = false;

	public $url = '';

	private $sources = ['Cairn', 'CairnMagazine'];

	public function getHelp()
	{
		$sources = join(' | ', $this->sources);
		return <<<EOL
			./yii import <source> [--verbose=1] [--simulation=0]
			    avec <source> : all | $sources"

			./yii import kbart --ressource=1 <fichier.tsv>
			    avec les params requis :
			        --ressource=<id>
			        --defautType=<Intégral|Résumé|Sommaire|Indexation>
			    et les params facultatifs (valeurs par défaut) :
			        --collection=<id>
			        --lacunaire=0
			        --selection=0
			        --checkDeletion=1
			        --ssiConnu=0
			        --acces=Libre (ou Restreint)
			        --ignoreUrl=0

			./yii import delete [--ressource=1] [--since=YYYY-MM-DD]
			    avec :
			        --ressource=<id>   Si absent, toutes les ressources en autoImport
			        --since=YYYY-MM-DD Si absent, date du dernier import de chaque ressource

			Paramètres communs à tous les cas :
			        --verbose=1     : 0-concis 1-normal 2-debug
			        --simulation=0
			EOL;
	}

	/**
	 * Default action, called when no argument is given.
	 */
	public function actionIndex()
	{
		echo $this->getHelp();
	}

	/**
	 * Action "all" that imports from every source.
	 */
	public function actionAll()
	{
		foreach ($this->sources as $src) {
			$this->import($src);
		}
	}

	public function actionBmj()
	{
		$curl = new \components\Curl();
		$html = $curl->get("https://journals.bmj.com/kbart")->getContent();
		$m = [];
		if (!preg_match('#<a href="(/[^"]+)"#i', $html, $m)) {
			throw new \Exception("Le téléchargement pour BMJ de https://journals.bmj.com/kbart ne contient pas l'URL du KBART.");
		}
		$kbartUrl = "https://resources.bmj.com" . $m[1];
		$fh = tmpfile();
		if ($fh === false) {
			throw new \Exception("Could not create a temp file.");
		}
		$curl->getFile($kbartUrl, $fh);
		if ($curl->getSize() < 100) {
			throw new \Exception("Le fichier KBART pour BMJ fait moins de 100 octets.");
		}

		$fileStore = new components\FileStore('import-acces');
		$fileStore->flushOlderThan(15*86400);
		$fileStore->set("bmj.txt", stream_get_meta_data($fh)['uri']);

		$this->actionKbart('', 0, 95, 0, 0, 'Libre', true, 1, 0, [$fileStore->get('bmj.txt')]);
	}

	/**
	 * ImportKbart caller.
	 *
	 * @param int $ressource
	 */
	public function actionKbart($defautType='', $ressource=0, $collection=0, $lacunaire=0, $selection=0, $acces='Libre', $ssiConnu=false, $checkDeletion=1, $ignoreUrl=0, $args=[])
	{
		if ($collection > 0) {
			$coll = Collection::model()->findByPk((int) $collection);
			if (!$coll) {
				echo "La collection avec cet ID n'a pas été trouvée !\n";
				return 8;
			}
			if (!$ressource) {
				$ressourceId = $coll->ressourceId;
			} elseif ($ressource == $coll->ressourceId) {
				$ressourceId = (int) $ressource;
			} else {
				echo "Incohérence : la collection est rattachée à la ressource {$coll->ressourceId} et non $ressource !\n";
				return 9;
			}
			$collectionId = (int) $coll->id;
		} else {
			$collectionId = null;
			$ressourceId = (int) $ressource;
		}
		if (!$ressourceId) {
			echo "Il faut un paramètre numérique '--ressource=<id>'.\n\n";
			$this->actionIndex();
			return 1;
		}
		if (!$args) {
			echo "Il manque un fichier en paramètre.\n\n";
			$this->actionIndex();
			return 1;
		}
		$filename = $args[0];
		if (strncmp($filename, 'http', 4) !== 0 && (!file_exists($filename) || !is_readable($filename))) {
			echo "Le fichier '$filename' ne peut pas être ouvert.\n\n";
			$this->actionIndex();
			return 1;
		}

		Yii::import('application.models.import.ImportKbart');
		$params = [
			'url' => $filename,
			'ressourceId' => $ressourceId,
			'collectionId' => $collectionId,
			'defaultType' => $defautType,
			'lacunaire' => $lacunaire,
			'selection' => $selection,
			'ignoreUnknownTitles' => $ssiConnu,
			'acces' => $acces,
			'checkDeletion' => $checkDeletion,
			'ignoreUrl' => $ignoreUrl,
		];
		$import = new ImportKbart($params);
		$import->verbose = $this->verbose;
		$import->simulation = $this->simulation;
		$import->importAll();
		if ($this->verbose > 1 || !$import->log->isEmpty()) {
			echo "Import KBART v2...\n";
			if (strncmp($filename, 'http', 4) === 0) {
				echo "Source : $filename\n";
			}
			echo $import->log->format('string');
			echo "Import KBART...FINI\n";
		}
	}

	/**
	 * List accesses that were not imported recently or not imported at all.
	 */
	public function actionDelete(string $ressource = "0", string $since = '', string $emails = "")
	{
		$p = new \processes\service\VanishedServicesNotify($since);
		$p->setVerbose((int) $this->verbose);
		$p->run((int) $ressource);
		if ($emails) {
			$p->sendEmails();
		} else {
			echo $p->getTextReport();
		}
	}

	public function actionRevert($where = "")
	{
		$interventions = Intervention::model()->findAll($where);
		if (!$interventions) {
			echo "Aucune intervention ne correspond.\n";
			return 0;
		}
		if (!$this->confirm("Annuler puis supprimer " . count($interventions) . " interventions ? ")) {
			echo "Annulation.\n";
			return 0;
		}
		foreach ($interventions as $i) {
			/** @var Intervention $i */
			if ($i->revert()) {
				if ($i->delete()) {
					fputcsv(STDOUT, [$i->id, $i->revueId, "Annulée et supprimée"], ';', '"', '\\');
				} else {
					fputcsv(STDOUT, [$i->id, $i->revueId, "Annulée SANS suppression"], ';', '"', '\\');
				}
			} else {
				fputcsv(STDOUT, [$i->id, $i->revueId, "ERREUR: " . print_r($i->getErrors(), true)], ';', '"', '\\');
			}
		}
		return 0;
	}

	/**
	 * Intercepts the action call so that source names can be redirected to import().
	 *
	 * @param array $args
	 */
	public function run($args)
	{
		[$action, $options] = $this->resolveRequest($args);
		if ($action) {
			$this->verbose = $options['verbose'] ?? 1;
			$this->simulation = isset($options['simulation']) ? (boolean) $options['simulation'] : false;
			$this->url = $options['url'] ?? null;
			$source = ucfirst(strtolower($action));
			foreach ($this->sources as $s) {
				if (strcasecmp($source, $s) == 0) {
					return $this->import($s);
				}
			}
		}
		return parent::run($args);
	}

	/**
	 * The actual instantiation and call of import.
	 *
	 * @param string $source Among $this->sources.
	 */
	protected function import(string $source): int
	{
		Yii::import('application.models.import.Import' . $source);
		$className = 'Import' . $source;
		$params = [
			'url' => $this->url,
		];
		$import = new $className($params);
		/** @var Import $import */
		$import->verbose = $this->verbose;
		$import->simulation = $this->simulation;
		$import->importAll();
		if ($this->verbose > 1 || !$import->log->isEmpty()) {
			echo $source, "...\n";
			echo $import->log->format('string');
			echo $source, "...FINI\n";
		}
		return 0;
	}
}
