<?php

use models\import\ImportType;
use processes\images\Resizer;

class EditeurCommand extends CConsoleCommand
{
	/**
	 * Envoie un courriel à 'suivi.editeurs.email' (Config) pour les éditeurs créés (depuis 24h) sans idref.
	 */
	public function actionEmailCreations($since = "")
	{
		try {
			$ec = new commands\models\EditeurCreations();
			$ec->email($since ?: "24 hours ago");
			return 0;
		} catch (Exception $ex) {
			echo $ex->getMessage() . "\n";
			return 1;
		}
	}

	public function actionExportForIdref($asCsv = "")
	{
		\commands\models\EditeurExport::exportForIdref((bool) $asCsv);
	}

	public function actionExportJson()
	{
		$result = \commands\models\EditeurExport::exportJson();
		echo json_encode($result, JSON_PRETTY_PRINT);
	}

	/**
	 * Identifie les éditeurs par le webservice d'idref.fr et produit un CSV (sans écriture en base de données).
	 */
	public function actionIdref()
	{
		$editeurs = Yii::app()->db
			->createCommand(
				<<<EOSQL
				SELECT e.id, LOWER(e.nom) AS nom, p.code2 AS alpha2 FROM Editeur e JOIN Pays p ON e.paysId = p.id WHERE e.idref IS NULL ORDER BY e.nom
				EOSQL
			)->queryAll();
		foreach ($editeurs as $e) {
			$countryCode = $e['alpha2'];
			$name = str_replace(' ', '\ ', $e['nom']);
			$q = rawurlencode(join(
				" AND ",
				[
					"corpname_s:{$name}", // le nom contient exactement
					"d102_s:{$countryCode}",
					"recordtype_z:b", // type Collectivité
				]
			));
			$url = "https://www.idref.fr/Sru/Solr?wt=json&indent=on&rows=2&fl=ppn_z,affcourt_z,&q=($q)";
			$raw = json_decode(file_get_contents($url));
			if (empty($raw->response)) {
				fputcsv(STDOUT, [$e['id'], "NO RESPONSE", $e['nom']], ';', '"', '\\');
				continue;
			}
			$response = $raw->response;
			if (empty($response->numFound)) {
				fputcsv(STDOUT, [$e['id'], '', "NOT FOUND", $e['nom']], ';', '"', '\\');
			} elseif ($response->numFound > 1) {
				fputcsv(STDOUT, [$e['id'], '', "MULTIPLE", $e['nom']], ';', '"', '\\');
			} else {
				fputcsv(STDOUT, [$e['id'], $response->docs[0]->ppn_z ?? '?', "OK", $e['nom']], ';', '"', '\\');
			}
			usleep(10000);
		}
	}

	/**
	 * Update Editeur.idref from a CSV "Editeur.id;Editeur.idref".
	 *
	 * @param string $csv file name
	 */
	public function actionIdrefInterventions(string $csv)
	{
		$colIdref = 7;
		$colIdM = 9;

		fprintf(STDERR, "Seules les corrections d'IdRef sont détaillées.\n");

		$handle = fopen($csv, "r");
		$count = 0;
		while ($row = fgetcsv($handle, 0, ";")) {
			$idref = $row[$colIdref];
			if (!$idref || !ctype_digit($idref[0])) {
				continue;
			}

			$editeur = Editeur::model()->findByPk($row[$colIdM]);
			if ($editeur === null) {
				fputcsv(STDOUT, ["EDITEUR ABSENT", "", "", $idref], ';', '"', '\\');
				continue;
			}
			if ($editeur->idref === $idref) {
				fputcsv(STDOUT, ["INCHANGÉ", $editeur->id, $editeur->idref, $idref], ';', '"', '\\');
				continue;
			}
			if ($editeur->idref) {
				// idref changes!
				fputcsv(STDOUT, ["MODIF", $editeur->id, $editeur->idref, $idref], ';', '"', '\\');
			} else {
				fputcsv(STDOUT, ["CRÉATION", $editeur->id, "", $idref], ';', '"', '\\');
				$editeur->idref = null;
			}

			$i = $editeur->buildIntervention(true);
			$i->commentaire = "idref.fr";
			$now = time();
			$i->hdateProp = $now;
			$i->contenuJson->updateByAttributes($editeur, ['idref' => $idref]);
			$i->import = ImportType::getSourceId('idref.fr');
			if (!$i->accept(true)) {
				$errorMsg = strip_tags($i->contenuJson->getErrors()["Editeur.idref"][0]);
				fputcsv(STDOUT, ["ERREUR", $editeur->id, $editeur->idref, $idref, $errorMsg], ';', '"', '\\');
			}
			$count++;
		}
		fprintf(STDERR, "$count enregistrements dans M (nouveaux IdRef + corrections)\n");
	}

	/**
	 * Rafraîchit les logos quand les images d'origine (déjà téléchargées) sont plus récentes que les images de M.
	 */
	public function actionLogos()
	{
		$imageTool = new Resizer(DATA_DIR . '/public/images/editeurs', DATA_DIR . '/images/editeurs');
		$imageTool->filePattern = 'editeur-%06d';
		$imageTool->boundingbox = '450x110>';
		$imageTool->resizeAll();
	}

	public function actionRelationsIncompletes()
	{
		\commands\models\EditeurExport::exportRelationsIncompletes();
	}
}
