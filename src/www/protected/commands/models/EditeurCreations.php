<?php

namespace commands\models;

use components\email\Mailer;
use Config;
use Yii;

class EditeurCreations
{
	public $from;

	public $to;

	public function __construct()
	{
		$this->from = Config::read('email.from');
		$this->to = Config::read('suivi.editeurs.email');
	}

	public function email(string $since)
	{
		if (!$this->to) {
			throw new \Exception("Envoi impossible tant que 'suivi.editeurs.email' n'est pas configuré.");
		}
		if (!$this->from) {
			throw new \Exception("Envoi impossible tant que 'email.from' n'est pas configuré.");
		}

		$timestamp = (int) strtotime($since);
		if (!$timestamp) {
			throw new \Exception("Le paramètre --since= n'est pas valide.");
		}

		$body = $this->getBody($timestamp);
		if ($body !== '') {
			$message = Mailer::newMail()
				->subject(Yii::app()->name . " : création d'éditeurs")
				->from($this->from)
				->setTo($this->to)
				->text($body);
			if (!Mailer::sendMail($message)) {
				throw new \Exception("Erreur lors de l'envoi du courriel à '{$this->to}'.");
			}
		}
	}

	private function getBody(int $timestamp): string
	{
		$body = "";
		$baseUrl = Yii::app()->params->itemAt('baseUrl');

		$sqlP = <<<EOSQL
			SELECT id, contenuJson
			FROM Intervention
			WHERE
				hdateProp >= {$timestamp}
				AND hdateVal IS NULL
				AND import = 0
				AND action = 'editeur-C'
				AND (json_exists(contenuJson, '$.content[0].after.idref') = 0
				OR json_value(contenuJson, '$.content[0].after.idref') IS NULL)
			ORDER BY hdateProp
			EOSQL;
		$propositions = Yii::app()->db->createCommand($sqlP)->queryAll();
		if ($propositions) {
			$body .= "## Éditeurs proposés sans idref\n\n";
			foreach ($propositions as $row) {
				$attributes = json_decode($row['contenuJson'], true)['content'][0]['after'];
				$url = "$baseUrl/intervention/{$row['id']}";
				$body .= "* {$attributes['nom']}\n  {$url}\n";
			}
			$body .= "\n";
		}

		$creationsFr = self::listCreations($timestamp, "e.paysId = 62"); // France
		if ($creationsFr) {
			$body .= "## Éditeurs français créés sans idref\n\n$creationsFr\n";
		}

		$creations = self::listCreations($timestamp, "e.paysId <> 62"); // Hors France
		if ($creations) {
			$body .= "## Éditeurs non-français créés sans idref\n\n$creations\n";
		}

		return $body;
	}

	private static function listCreations(int $timestamp, string $where): string
	{
		$baseUrl = Yii::app()->params->itemAt('baseUrl');
		$sql = <<<SQL
			SELECT e.id, e.nom, p.nom AS pays
			FROM Editeur e
				JOIN Intervention i ON i.editeurId = e.id
				LEFT JOIN Pays p ON p.id = e.paysId
			WHERE i.hdateProp >= {$timestamp} AND i.action = 'editeur-C' AND e.idref IS NULL
				AND $where
			ORDER BY e.nom
			SQL;
		$created = \Yii::app()->db->createCommand($sql)->queryAll();
		$result = "";
		foreach ($created as $e) {
			$result .= "* {$e['nom']}\n  {$baseUrl}/editeur/{$e['id']}\n";
			if ($e['pays'] && $e['pays'] !== "France") {
				$result .= "  {$e['pays']}\n";
			}
		}
		return $result;
	}
}
