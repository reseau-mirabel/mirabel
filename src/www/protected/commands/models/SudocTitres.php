<?php

namespace commands\models;

use components\db\InsertBuffer;
use models\sudoc\ApiClient;
use Yii;

class SudocTitres
{
	private string $sql;

	public function __construct(string $since)
	{
		$this->sql = self::initSql($since);
	}

	public function run(): void
	{
		$ppnRows = Yii::app()->db->createCommand($this->sql)->query();

		$buffer = new InsertBuffer("INSERT INTO TitreVariante (titreId, ppn, variante, lang, zoneSudoc, zoneLang) VALUES ");
		$buffer->setBatchSize(100);
		$buffer->stopDbConnection();

		$api = new ApiClient();
		foreach ($ppnRows as $row) {
			try {
				$notice = $api->getNotice($row['sudocPpn']);
				if ($notice === null) {
					sleep(1);
					$notice = $api->getNotice($row['sudocPpn']);
				}
				if ($notice !== null) {
					foreach ($notice->getLog() as $log) {
						fprintf(STDERR, $log);
					}
				}
			} catch (\Throwable $e) {
				fprintf(STDERR, "Erreur en interrogeant le SUDOC : %s\n", $e->getMessage());
				continue;
			}
			if ($notice !== null && $notice->titreVariantes) {
				foreach ($notice->titreVariantes as $v) {
					$buffer->add([
						(int) $row['titreId'],
						$row['sudocPpn'],
						$v['variante'],
						$v['lang'],
						$v['zone'],
						$v['zoneLang'],
					]);
				}
			} else {
				fprintf(STDERR, "%d ; %s ; Aucune notice trouvée\n", (int) $row['titreId'], $row['sudocPpn']);
			}
			usleep(100_000); // 100 ms
		}

		$buffer->close();
	}

	private static function initSql(string $since): string
	{
		$extraJoin = '';
		$extraWhere = '';
		if ($since) {
			Yii::app()->db->createCommand("DELETE FROM TitreVariante WHERE lastUpdate < :s")->execute([':s' => $since]);
			$extraJoin = "LEFT JOIN TitreVariante v ON i.titreId = v.titreId";
			$extraWhere = "AND v.lastUpdate IS NULL";
		} else {
			Yii::app()->db->createCommand("DELETE FROM TitreVariante")->execute();
		}
		return <<<EOSQL
			SELECT i.sudocPpn, i.titreId
			FROM Issn i
			  JOIN Titre_Editeur te USING(titreId)
			  JOIN Editeur e ON te.editeurId = e.id AND e.paysId = 62 -- France
			  $extraJoin
			WHERE i.sudocPpn IS NOT NULL AND i.sudocPpn <> '' $extraWhere
			GROUP BY i.id
			EOSQL;
	}
}
