<?php

namespace commands\models;

use components\db\InsertBuffer;
use Yii;

class IssnTempTable
{
	private const BULK_INSERT_SIZE = 500;

	private int $count = 0;

	private InsertBuffer $buffer;

	public function __construct()
	{
		$this->buffer = new InsertBuffer("INSERT INTO knownissn (id, issn) VALUES ");
		$this->buffer->setBatchSize(self::BULK_INSERT_SIZE);
		self::createTempTable();
	}

	public function __destruct()
	{
		$this->close();
	}

	public function close(): void
	{
		if (!defined('YII_TARGET') || YII_TARGET !== 'test') {
			// Creating an index speeds up the JOIN, but breaks the SQL transaction in tests.
			Yii::app()->db->createCommand("ALTER TABLE knownissn ADD INDEX IF NOT EXISTS knownissn_issn (issn)")->execute();
		}
		$this->buffer->close();
	}

	public static function exists(): bool
	{
		return (bool) Yii::app()->db->createCommand("SHOW CREATE TABLE knownissn");
	}

	public function getCount(): int
	{
		return $this->count;
	}

	public static function getTableName(): string
	{
		return "knownissn";
	}

	public function insertIssn($id, string $rawIssn): bool
	{
		$issn = trim(strtoupper($rawIssn));
		if (!$issn) {
			return false;
		}
		if (!preg_match('/^\d{4}-?\d{3}[0-9X]$/', $issn)) {
			fprintf(STDERR, "Ignore l'ISSN '%s' mal formé\n", $issn);
			return false;
		}
		if (preg_match('/^\d{7}/', $issn)) {
			$issn = substr($issn, 0, 4) . "-" . substr($issn, 4);
		}
		$this->buffer->add([$id, $issn]);
		$this->count++;
		return true;
	}

	private static function createTempTable(): void
	{
		Yii::app()->db
			->createCommand("DROP TEMPORARY TABLE IF EXISTS knownissn")
			->execute();
		Yii::app()->db
			->createCommand("CREATE TEMPORARY TABLE knownissn (id VARCHAR(255) NOT NULL COLLATE ascii_bin, issn CHAR(9) NOT NULL COLLATE ascii_bin)")
			->execute();
	}
}
