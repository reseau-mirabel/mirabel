<?php

namespace commands\models;

class SphinxConf
{
	/**
	 * Return the text formatted as suitable for a Manticore/Sphinx config file.
	 */
	public static function format(string $text): string
	{
		$clean = preg_replace('/\n\s*\n+/', "\n", trim($text));
		$eolEscaped = preg_replace('/\s*\n/', " \\\n", $clean);
		return " \\\n{$eolEscaped}\n";
	}

	/**
	 * Load the template from views/sphinx/.
	 *
	 * @param string $filename File name, without the php extension.
	 * @param array $data Extra variables for the template, declared as [name => value].
	 */
	public static function render(string $filename, array $data = []): string
	{
		$dir = dirname(__DIR__) . '/views/sphinx';
		$absolute = "$dir/$filename";
		/** @var \CConsoleApplication */
		$app = \Yii::app();
		return $app->getCommand()->renderFile($absolute, $data, true);
	}
}
