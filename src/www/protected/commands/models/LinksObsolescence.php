<?php

namespace commands\models;

use CDbDataReader;
use Exception;
use Lien;
use models\import\ImportType;
use Sourcelien;
use Titre;
use Yii;

class LinksObsolescence
{
	private const ALERT_RATE = 0.05;

	/**
	 * @var \Sourcelien|null
	 */
	private $sourceLien;

	private bool $delete = false;

	private int $importType = 0;

	public function __construct(?Sourcelien $s)
	{
		$this->sourceLien = $s;
	}

	public function setAutoDelete(bool $delete)
	{
		if ($delete) {
			if ($this->sourceLien === null) {
				throw new Exception("La suppression de liens obsolètes n'est possible que sur une source donnée.");
			}
			$this->importType = ImportType::getSourceId($this->sourceLien->nomcourt);
		}
		$this->delete = $delete;
	}

	/**
	 * @return list{int, string} [count, text]
	 */
	public function detectObsolete(int $since): array
	{
		if ($this->delete) {
			$deletionRate = $this->computeAlertRate($since);
			if ($deletionRate > self::ALERT_RATE) {
				return [0, sprintf("%.1f %% des liens seraient supprimés ! Abandon.", $deletionRate * 100)];
			}
		}

		$query = $this->findObsoleteLinks($since);
		$exportedKeys = ['revueId', 'id', 'titre', 'nom', 'lastUrl', 'lastSeen'];
		$deleteLinkReference = Yii::app()->db->createCommand("DELETE FROM ImportedLink WHERE id = :id");
		$out = fopen('php://temp', 'w');
		$count = 0;
		foreach ($query as $row) {
			$export = [];
			foreach ($exportedKeys as $k) { // array_intersect_key() gives wrong order, so a loop is simpler.
				$export[$k] = $row[$k];
			}
			if ($row['actualDate']) {
				if ($this->delete) {
					$this->deleteLink($row);
					$export[] = "Absent de la source, donc suppression dans M";
				} else {
					$export[] = "Absent de la source";
				}
			} else {
				$export[] = "Supprimé de Mirabel";
				$deleteLinkReference->execute([':id' => $row['ilId']]);
			}
			$export[] = Yii::app()->params->itemAt('baseUrl') . "/revue/{$row['revueId']}";
			fputcsv($out, $export, ';', '"', '\\');
			$count++;
		}
		$content = (string) stream_get_contents($out, -1, 0);
		fclose($out);
		return [$count, $content];
	}

	/**
	 * Return a list (with title) of links not imported since this timestamp, or "" if none.
	 */
	public function listObsoleteLinks(int $timestamp): string
	{
		if ($timestamp === 0) {
			return '';
		}
		[$count, $content] = $this->detectObsolete($timestamp);
		if (!$content) {
			return "";
		}
		return sprintf("\n## %d liens obsolètes (pas importés depuis %s)\n%s", $count, date('Y-m-d', $timestamp), $content);
	}

	/**
	 * Returns #links_to_be_deleted / #links_in_source.
	 */
	private function computeAlertRate(int $since): float
	{
		if ($this->sourceLien === null) {
			throw new Exception("La suppression de liens obsolètes n'est possible que sur une source donnée.");
		}
		$total = (int) Yii::app()->db
			->createCommand("SELECT count(*) FROM LienTitre WHERE sourceId = :id")
			->queryScalar([':id' => $this->sourceLien->id]);
		if ($total === 0) {
			return 0.0;
		}
		$toDelete = (int) Yii::app()->db
			->createCommand(<<<EOSQL
				SELECT count(*)
				FROM LienTitre lt
					JOIN ImportedLink l ON l.titreId = lt.titreId AND l.url = lt.url
				WHERE lt.sourceId = :id AND l.lastSeen < :timestamp
				EOSQL
			)
			->queryScalar([':id' => $this->sourceLien->id, ':timestamp' => $since]);
		return ($toDelete / $total);
	}

	private function deleteLink(array $row): void
	{
		$titre = Titre::model()->populateRecord($row, false);
		assert($titre instanceof Titre);
		$titre->setScenario('import');
		$oldTitle = clone $titre;

		$liens = $titre->getLiens();
		foreach ($liens as $l) {
			assert($l instanceof Lien);
			if ((int) $l->sourceId === (int) $this->sourceLien->id) {
				$liens->remove($l);
			}
		}
		$titre->setLiens($liens);

		$i = $titre->buildIntervention(true);
		$i->setImport($this->importType);
		$i->description = "Modification du titre « {$titre->titre} »";
		$i->contenuJson->update($oldTitle, $titre);
		$i->setScenario('import');
		$i->accept(true);
	}

	private function findObsoleteLinks(int $since): CDbDataReader
	{
		$condSourcelien = $this->sourceLien === null ? "" : "AND l.sourceId = {$this->sourceLien->id}";
		// Do NOT join using titreId since a single ImportedLink may match 2 Titre, see #5248
		$command = Yii::app()->db->createCommand(
			<<<EOSQL
			SELECT
				t.*,
				s.nom,
				l.id AS ilId, l.url as lastUrl, FROM_UNIXTIME(l.lastSeen) AS lastSeen,
				lt.hdate AS actualDate
			FROM Titre t
				JOIN ImportedLink l ON l.titreId = t.id
				JOIN Sourcelien s ON s.id = l.sourceId
				LEFT JOIN LienTitre lt ON l.sourceId = lt.sourceId AND l.url = lt.url AND lt.titreId = t.id
			WHERE
				l.lastSeen < :timestamp
				$condSourcelien
			ORDER BY t.titre
			EOSQL
		);
		return $command->query([':timestamp' => $since]);
	}
}
