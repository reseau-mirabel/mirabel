<?php

namespace commands\models;

/**
 * Methods that create and fill a tmp table "knownissn".
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class LoadIssns
{
	public static $verbose = 0;

	public static $maxResults = PHP_INT_MAX;

	/**
	 * Create and fill the temporary table "knownissn".
	 *
	 * @param string $csvFile File name of the CSV data.
	 * @param int[] $issnCols Columns containing the ISSN numbers. E.g. [0] for extracting ISSNs from the first column.
	 * @param int|null $identifierCol Column containing the identifier, or null for using the matching ISSN.
	 * @param string $separator
	 * @return array{int, int} [#ISSN, #lines]
	 */
	public static function readFromCsv(string $csvFile, array $issnCols, ?int $identifierCol, string $separator): array
	{
		$handle = fopen($csvFile, "r");
		if (!$handle) {
			echo "Could not read the CSV file.";
			exit(1);
		}
		fgetcsv($handle, 0, $separator); // skip header line
		return self::readFromCsvHandle($handle, $issnCols, $identifierCol, $separator);
	}

	/**
	 * Create and fill the temporary table "knownissn".
	 *
	 * @param resource $handle
	 * @param int[] $issnCols Columns containing the ISSN numbers. E.g. [0] for extracting ISSNs from the first column.
	 * @param int|null $identifierCol Column containing the identifier, or null for using the matching ISSN.
	 * @param string $separator
	 * @return array{int, int} [#ISSN, #lines]
	 */
	public static function readFromCsvHandle($handle, array $issnCols, ?int $identifierCol, string $separator): array
	{
		$issnTempTable = new IssnTempTable();
		$countLines = 0;
		while (($rawdata = fgetcsv($handle, 0, $separator)) !== false) {
			$data = array_map('trim', $rawdata);
			$issns = [];
			$matches = [];
			foreach ($issnCols as $col) {
				if (preg_match_all('/\b(\d{4}-\d{3}[\dxX])\b/', $data[$col] ?? '', $matches)) {
					$issns = array_merge($issns, $matches[1]);
				}
			}
			if (!$issns) {
				if (self::$verbose > 0) {
					fprintf(STDERR, "Le fichier CSV est mal formé, sans ISSN sur la ligne = %s\n", json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
				}
				continue;
			}
			$countLines++;
			if ($identifierCol === null) {
				foreach ($issns as $issn) {
					$issnTempTable->insertIssn($issn, $issn);
				}
			} else {
				$id = $data[$identifierCol] ?? '' ?: $issns[0];
				foreach ($issns as $issn) {
					$issnTempTable->insertIssn($id, $issn);
				}
			}
		}
		fclose($handle);
		$issnTempTable->close();
		return [$issnTempTable->getCount(), $countLines];
	}

	public static function readFromJsonStream(string $path, string $issne, string $issnp, string $identifier): int
	{
		$issnTempTable = new IssnTempTable();

		$parser = new \JsonCollectionParser\Parser();
		$parser->parse($path, function (array $item) use ($issne, $issnp, $identifier, $issnTempTable) {
			if (empty($item[$identifier]) || !is_scalar($item[$identifier])) {
				return;
			}
			$id = $item[$identifier];
			if (!empty($item[$issne])) {
				$issnTempTable->insertIssn($id, $item[$issne]);
			}
			if (!empty($item[$issnp])) {
				$issnTempTable->insertIssn($id, $item[$issnp]);
			}
			// hack because Latindex has inconsistent ISSN data
			if (empty($item[$issne]) && empty($item[$issnp]) && !empty($item["issn_l"])) {
				$issnTempTable->insertIssn($id, $item["issn_l"]);
			}
		});

		$issnTempTable->close();
		return $issnTempTable->getCount();
	}

	/**
	 * Create and fill the temporary table "knownissn".
	 *
	 * @param string $xmlfile File name of the MARC21 XML data.
	 * @return int #ISSN
	 */
	public static function readFromMarcxml($xmlfile)
	{
		$issnTempTable = new IssnTempTable();
		$reader = new \XMLReader();
		if (substr($xmlfile, -3) === ".gz") {
			$xmlstream = "compress.zlib://$xmlfile";
		} else {
			$xmlstream = $xmlfile;
		}
		$reader->open($xmlstream, null, LIBXML_COMPACT | LIBXML_NONET);
		// Cf MARC21: https://www.loc.gov/marc/bibliographic/bd022.html
		$usefulAttributes = ["a" => 1, "l" => 1];
		do {
			$unfinished = true;
			while ($unfinished && $reader->name !== 'ns0:datafield' && $reader->getAttribute("tag") !== '022') {
				$unfinished = $reader->read();
			}
			if ($unfinished) {
				$node = $reader->expand();
				$id = null;
				$issns = [];
				foreach ($node->childNodes as $child) {
					if ($child->nodeType !== XML_ELEMENT_NODE || !($child instanceof \DOMElement)) {
						continue;
					}
					if (
						isset($usefulAttributes[$child->getAttribute("code")])
						&& preg_match('/^\d{4}-\d{3}[\dX]$/', trim($child->textContent))) {
						$issns[] = trim($child->textContent);
						if ($child->getAttribute("code") === "a") {
							$id = trim($child->textContent);
						}
					}
				}
				foreach ($issns as $issn) {
					$issnTempTable->insertIssn($id, $issn);
				}
			}
		} while ($unfinished && $reader->next('record'));
		$reader->close();
		$issnTempTable->close();
		return $issnTempTable->getCount();
	}

	/**
	 * Create and fill the temporary table "knownissn".
	 *
	 * @param \Traversable $metadatas
	 * @return int #ISSN
	 */
	public static function readFromArray($metadatas)
	{
		$issnTempTable = new IssnTempTable();
		foreach ($metadatas as $metadata) {
			if (isset($metadata['issn'])) {
				foreach ($metadata['issn'] as $issn) {
					$issnTempTable->insertIssn($metadata['id'], $issn);
				}
			}
		}
		$issnTempTable->close();
		return $issnTempTable->getCount();
	}
}
