<?php

namespace commands\models\sudoc;

use Issn;
use models\sudoc\ApiClient;
use Titre;
use Yii;

/**
 * Check the (ISSN, PPN) data in Mirabel against the Sudoc API queried by ISSN.
 *
 * If a PPN is missing and unique, it will be recorded through an Intervention.
 */
class CheckByIssn
{
	private int $limit = 0;

	private bool $simulation = false;

	private ApiClient $sudocApi;

	private int $verbose = 1;

	/**
	 * @param ApiClient $sudocApi $sudocApi->getPpn() is the only method used.
	 */
	public function __construct(ApiClient $sudocApi)
	{
		$this->sudocApi = $sudocApi;
	}

	public function setConfig(array $config)
	{
		$this->limit = $config['limit'] ?? 0;
		$this->simulation = $config['simulation'] ?? false;
		$this->verbose = $config['verbose'] ?? 1;
	}

	/**
	 * @param ?resource $output E.g. STDOUT or `$output = fopen("php://memory", 'w')`
	 */
	public function run($output): void
	{
		$ppnInfo = new PpnInfo($output);
		$ppnInfo->simulation = (bool) $this->simulation;
		$ppnInfo->csvColumns = 4;
		$ppnInfo->csvSeparator = "\t";

		$rows = Yii::app()->db
			->createCommand(
				"SELECT issn, sudocPpn FROM Issn WHERE issn IS NOT NULL ORDER BY id DESC"
				. ($this->limit ? " LIMIT {$this->limit}" : "")
			)->query();
		foreach ($rows as $row) {
			$issn = (string) $row['issn'];
			$ppn = (string) $row['sudocPpn'];
			if (!$issn) {
				continue;
			}
			$this->checkIssnPpn($ppnInfo, $issn, $ppn);
		}
	}

	private function checkIssnPpn(PpnInfo $ppnInfo, string $issn, string $ppn): void
	{
		try {
			$info = $this->sudocApi->getPpn($issn);
		} catch (\Throwable $e) {
			$ppnInfo->csv($issn, $ppn, "ERR", $e->getMessage());
			return;
		}

		if (count($info) === 0) {
			if ($ppn) {
				$ppnInfo->csv($issn, $ppn, "WARN", "Aucun PPN dans le Sudoc pour cet ISSN");
			} elseif ($this->verbose > 1) {
				$ppnInfo->csv($issn, $ppn, "OK", "Aucun PPN dans le Sudoc pour cet ISSN");
			}
		} elseif (count($info) === 1) {
			if (empty($ppn)) {
				$titre = Titre::model()->findBySql("SELECT t.* FROM Titre t JOIN Issn i ON t.id = i.titreId WHERE i.issn = :i", [':i' => $issn]);
				$intervention = $ppnInfo->createIntervention($titre);
				$before = Issn::model()->findByAttributes(['issn' => $issn]);
				$after = clone ($before);
				$after->sudocPpn = $info[0]->ppn;
				$after->sudocNoHolding = (int) $info[0]->noHolding;
				$intervention->contenuJson->update($before, $after);
				if ($intervention->accept(true)) {
					$ppnInfo->csv($issn, $ppn, "ADD", "Le PPN {$info[0]->ppn} a été ajouté à cet ISSN (intervention {$intervention->id})");
				} else {
					$ppnInfo->csv($issn, $ppn, "ERR", "Erreur lors de l'ajout du PPN {$info[0]->ppn} : " . print_r($intervention->getErrors(), true));
				}
			} elseif ($info[0]->ppn != $ppn) {
				$ppnInfo->csv($issn, $ppn, "WARN", "Le PPN $ppn pour cet ISSN devrait être {$info[0]->ppn}");
			} elseif ($this->verbose > 1) {
				$ppnInfo->csv($issn, $ppn, "OK", "Le PPN de M correspond à celui du Sudoc pour cet ISSN");
			}
		} else { // count($info) > 1
			$ppnInfo->csv($issn, $ppn, "WARN", "Multiples PPN pour cet ISSN dans le Sudoc");
			if (!empty($ppn)) {
				$found = false;
				$sudocPpns = [];
				foreach ($info as $i) {
					if ($i->ppn === $ppn) {
						$found = true;
					}
					$sudocPpns[] = $i->ppn;
				}
				if (!$found) {
					$ppnInfo->csv($issn, $ppn, "WARN", "Le PPN selon M ne figure pas dans ceux de cet ISSN pour le Sudoc : " . join(", ", $sudocPpns));
				}
			}
		}
	}
}
