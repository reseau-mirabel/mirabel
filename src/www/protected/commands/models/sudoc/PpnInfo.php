<?php

namespace commands\models\sudoc;

use Exception;
use Intervention;
use InterventionDetail;
use Issn;
use models\import\ImportType;
use models\sudoc\Notice;
use Suivi;
use Titre;

class PpnInfo
{
	public $simulation = false;

	public bool $verbose = false;

	public $csvColumns = 0;

	public $csvSeparator = "\t";

	private $output;

	/**
	 * @param resource $output File descriptor, e.g. fopen('php://memory', 'w')
	 */
	public function __construct($output = null)
	{
		if ($output === null) {
			$this->output = fopen('php://stdout', 'w');
		} else {
			$this->output = $output;
		}
	}

	/**
	 * @param array $row Mirabel data
	 * @param Notice $notice SUDOC data
	 */
	public function apply(array $row, Notice $notice): void
	{
		$titre = new Titre();
		$titre->setAttributes(
			[
				'id' => (int) $row['titreId'],
				'titre' => $row['titre'],
				'revueId' => (int) $row['revueId'],
			],
			false
		);

		$analysis = self::analyzeFields($row, $notice);
		if ($analysis['conflicts']) {
			$this->csv(
				"WARNING",
				$row['issn'],
				$row['sudocPpn'],
				(string) $titre->id,
				$titre->titre,
				"Champs ignorés car conflits : " . join(" ", $analysis['conflicts'])
			);
		}
		if ($analysis['errors']) {
			$this->csv(
				"WARNING",
				$row['issn'],
				$row['sudocPpn'],
				(string) $titre->id,
				$titre->titre,
				"Champs ignorés car formats incorrects : " . join(" ", $analysis['errors'])
			);
		}

		// detect changes
		// sudoc\Notice => Issn
		$oldRow = $row;
		$changedAttributes = [];
		$details = [];
		foreach ($analysis['fields'] as $k => $v) {
			if ($v instanceof \Closure) {
				$v = $v($notice);
			}
			if (!$v) {
				// Field is not to be written into Mir@bel.
				continue;
			}
			if ($notice->{$k} !== null && $notice->{$k} !== "" && $row[$v] != $notice->{$k}) {
				$changedAttributes[] = $v;
				$row[$v] = is_bool($notice->{$k}) ? (int) $notice->{$k} : $notice->{$k};
				$details[] = "[$v={$row[$v]}]";
			}
		}

		// update record
		if ($changedAttributes) {
			// fix existing bad dates
			if ($oldRow['dateDebut'] && !ctype_digit($oldRow['dateDebut'][0]) && !$row['dateDebut']) {
				$oldRow['dateDebut'] = '';
				$row['dateDebut'] = '';
			}
			if ($oldRow['dateFin'] && !ctype_digit($oldRow['dateFin'][0]) && !$row['dateFin']) {
				$oldRow['dateFin'] = '';
				$row['dateFin'] = '';
			}

			if ($this->simulation) {
				$this->csv("OK", $row['issn'], $row['sudocPpn'], (string) $titre->id, $titre->titre, join(", ", $details));
			} else {
				$recordFieldNames = array_filter(array_values($analysis['fields']));
				$errorMsg = $this->updateIssn($oldRow, $row, $recordFieldNames, $titre);
				$this->csv(($errorMsg === '' ? "OK" : "ERR"), $row['issn'], $row['sudocPpn'], (string) $titre->id, $titre->titre, $errorMsg ?: join(", ", $details));
			}
		} else {
			if ($this->verbose) {
				$this->csv("NOP", $row['issn'], $row['sudocPpn'], (string) $titre->id, $titre->titre, "");
			}
		}
	}

	public function createIntervention(Titre $titre)
	{
		$i = new Intervention();
		$i->setAttributes(
			[
				'ressourceId' => null,
				'revueId' => (int) $titre->revueId,
				'titreId' => (int) $titre->id,
				'editeurId' => null,
				'statut' => 'attente',
				'import' => ImportType::getSourceId('SUDOC'),
				'ip' => '',
				'contenuJson' => new InterventionDetail(),
				'suivi' => null !== Suivi::isTracked($titre),
				'description' => "Modification du titre « {$titre->titre} »",
			],
			false
		);
		return $i;
	}

	public function csv(string ...$data): void
	{
		if (count($data) !== $this->csvColumns) {
			throw new Exception("format incompatible pour le CSV");
		}
		fputcsv($this->output, $data, $this->csvSeparator, '"', '\\');
	}

	/**
	 * Checks the input and return a list of fields to write: Notice field => Issn field.
	 *
	 * @return array{conflicts: string[], errors: string[], fields: array<string, string>}
	 */
	private static function analyzeFields(array $row, Notice $n): array
	{
		$conflicts = [];
		$errors = [];
		$fields = [
			'bnfArk' => 'bnfArk',
			'dateDebut' => 'dateDebut',
			'dateFin' => 'dateFin',
			'pays' => '', // Empty, so Issn.pays will NOT be updated!
			'sudocNoHolding' => 'sudocNoHolding',
			'support' => ($n->issn || $n->support === Issn::SUPPORT_INCONNU ? '' : 'support'),
			'worldcat' => 'worldcatOcn',
		];
		if ($n->issn
			&& $row['support'] !== Issn::SUPPORT_INCONNU // ie M has a support value
			&& !in_array($n->support, ['', Issn::SUPPORT_INCONNU, $row['support']], true)
		) {
			$conflicts[] = "[support : SUDOC={$n->support} M+issn.org={$row['support']}]";
		}
		// normalize dates
		if ($n->dateDebut) {
			if (preg_match('/^\s*\d\d[\dX][\dX]$/', $n->dateDebut)) {
				if ($row['dateDebut'] && $n->dateDebut !== $row['dateDebut']) {
					$conflicts[] = "[dateDebut : SUDOC={$n->dateDebut} M={$row['dateDebut']}]";
					$fields['dateDebut'] = '';
				}
			} else {
				$errors[] = sprintf("Date de début '%s'", $n->dateDebut);
				$n->dateDebut = '';
			}
		}
		if ($n->dateFin) {
			if (preg_match('/^\s*\d\d[\dX][\dX]$/', $n->dateFin)) {
				if ($row['dateFin'] && $n->dateFin !== $row['dateFin']) {
					$conflicts[] = "[dateFin : SUDOC={$n->dateFin} M={$row['dateFin']}]";
					$fields['dateFin'] = '';
				}
			} else {
				$errors[] = sprintf("Date de fin '%s'", $n->dateFin);
				$n->dateFin = '';
			}
		}

		return [
			'conflicts' => $conflicts,
			'errors' => $errors,
			'fields' => $fields,
		];
	}

	/**
	 * Return "" if OK, else an error message.
	 */
	private function updateIssn(array $oldValues, array $newValues, array $fields, Titre $titre): string
	{
		$newIssn = Issn::model()->populateRecord($oldValues, false);
		/** @var Issn $newIssn */
		$newIssn->scenario = 'import';
		$oldIssn = clone $newIssn;
		$newIssn->setAttributes($newValues, false);
		if ($newIssn->validate($fields)) {
			$i = $this->createIntervention($titre);
			$i->contenuJson->update($oldIssn, $newIssn);
			if (!$i->accept(true)) {
				return sprintf(
					"Intervention impossible à écrire pour « %s » : valeurs %s / ERREUR %s\n",
					$titre->titre,
					json_encode($newValues, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
					json_encode($i->errors, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
				);
			}
		} else {
			return sprintf(
				"Les données à importer pour le titre « %s » ne sont pas valides : %s / ERREUR %s\n",
				$newIssn->titre->titre,
				json_encode($newValues, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
				json_encode($newIssn->errors, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
			);
		}
		return "";
	}
}
