<?php

namespace commands\models\sudoc;

use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use Box\Spout\Reader\XLSX\Sheet;
use models\import\ImportType;

class Ppne
{
	private const COLUMNS = [
		'URL M',
		'titre',
		'ISSN-P',
		'PPN-P',
		'PPN-E',
		'ISSN-E',
		'commentaire',
	];

	private string $filePath;

	public function __construct(string $filePath)
	{
		if (!is_readable($filePath)) {
			throw new \Exception("File '$filePath' is not readable.");
		}
		$this->filePath = $filePath;
	}

	public function run(): void
	{
		$sheet = $this->openFirstSheet();
		foreach ($sheet->getRowIterator() as $row) {
			/** @var \Box\Spout\Common\Entity\Row $row */
			$row7 = array_slice($row->toArray(), 0, 7);
			$data = array_combine(self::COLUMNS, array_map('trim', $row7));
			if (strncmp($data['URL M'], "http", 4) !== 0) {
				continue;
			}
			self::load($data);
		}
	}

	private static function load(array $data): void
	{
		if ($data['PPN-E'] === '' || strlen($data['ISSN-P']) !== 9) {
			self::log($data, "Pas d'action, le PPN-E ou l'ISSN-P sont absents");
			return;
		}

		// La clé de correspondance est l'ISSN de la version papier (du même titre)
		$issnp = \Issn::model()->findBySql(
			"SELECT * FROM Issn WHERE issn = :issn AND support = :s",
			[':issn' => $data['ISSN-P'], ':s' => \Issn::SUPPORT_PAPIER]
		);
		if ($issnp === null) {
			self::log($data, "ISSN-P non trouvé");
			return;
		}
		assert($issnp instanceof \Issn);

		// ne pas intégrer le ppn électronique s'il existe déjà (unicité) sur un autre titre
		$exists = \Issn::model()->findBySql(
			"SELECT * FROM Issn WHERE titreId <> :tid AND sudocPpn = :ppn AND support = :s",
			[':tid' => (int) $issnp->titreId, ':ppn' => $data['PPN-E'], ':s' => \Issn::SUPPORT_ELECTRONIQUE]
		);
		if ($exists instanceof \Issn) {
			self::log($data, "Ce PPN-E est présent dans le titre d'ID {$exists->titreId}");
			return;
		}

		$intervention = $issnp->titre->buildIntervention(true);
		$intervention->import = ImportType::getSourceId('SUDOC');

		$issne = \Issn::model()->findBySql(
			"SELECT * FROM Issn WHERE titreId = :tid AND support = :s",
			[':tid' => (int) $issnp->titreId, ':s' => \Issn::SUPPORT_ELECTRONIQUE]
		);
		if ($issne instanceof \Issn) {
			// ne pas intégrer de ppn électronique s'il y en a déjà un pour ce titre
			if ($issne->sudocPpn) {
				if ($issne->sudocPpn !== $data['PPN-E']) {
					// signaler s'il y en a déjà un et qu'il est différent de l'entrée proposée
					self::log($data, "Le PPN-E '{$issne->sudocPpn}' est présent dans M au lieu de l'attendu");
				} else {
					self::log($data, "OK, le PPN-E '{$issne->sudocPpn}' est déjà présent dans M");
				}
				return;
			}
			$intervention->contenuJson->updateByAttributes($issne, ['sudocPpn' => $data['PPN-E']]);
		} else {
			// Il faut donc créer une entrée ISSN de type "en cours" + support "électronique" avec seulement le ppn de la colonne "PPN ELECTRONIQUE "
			$issne = new \Issn();
			$issne->titreId = (int) $issnp->titreId;
			$issne->sudocPpn = $data['PPN-E'];
			$issne->statut = \Issn::STATUT_ENCOURS;
			$issne->support = \Issn::SUPPORT_ELECTRONIQUE;
			$intervention->contenuJson->create($issne);
		}
		$intervention->action = 'revue-U';
		$intervention->description = "Ajout du PPN-E";
		if ($intervention->accept(true)) {
			self::log($data, "AJOUTÉ, intervention {$intervention->id}");
		} else {
			self::log($data, "ERREUR, l'enregistrement a échoué : " . print_r($intervention->getErrors(), true));
		}
	}

	private static function log(array $data, string $message): void
	{
		$out = fopen('php://output', 'w');
		$log = [$data['URL M'], $data['ISSN-P'], $data['PPN-E'], $message, $data['titre']];
		fputcsv($out, $log, ';', '"', '\\');
	}

	private function openFirstSheet(): Sheet
	{
		$reader = ReaderEntityFactory::createXLSXReader();
		$reader->setShouldPreserveEmptyRows(true);
		$reader->open($this->filePath);
		foreach ($reader->getSheetIterator() as $s) {
			return $s;
		}
		throw new \Exception("No worksheet found in the XLSX file");
	}
}
