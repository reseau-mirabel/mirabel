<?php

namespace commands\models\sudoc;

use models\Etatcollection;
use models\sudoc\ApiClient;
use models\sudoc\Notice;
use Yii;
use const STDERR;

class ImportByPpn
{
	private int $limit = 0;

	private bool $onlyDubious = true;

	private bool $simulation = false;

	private ApiClient $sudocApi;

	/**
	 * @var int 0 only changes, 1 all PPN, 2 debug
	 */
	private int $verbose = 1;

	/**
	 * @param ApiClient $sudocApi $sudocApi->getNotice() is the only method used.
	 */
	public function __construct(ApiClient $sudocApi)
	{
		$this->sudocApi = $sudocApi;
	}

	public function setConfig(array $config)
	{
		$this->limit = $config['limit'] ?? 0;
		$this->onlyDubious = $config['onlyDubious'] ?? true;
		$this->simulation = $config['simulation'] ?? false;
		$this->verbose = $config['verbose'] ?? 1;
	}

	/**
	 * @param ?resource $output E.g. STDOUT or `$output = fopen("php://memory", 'w')`
	 */
	public function run($output): void
	{
		$ppnInfo = new PpnInfo($output);
		$ppnInfo->simulation = $this->simulation;
		$ppnInfo->verbose = (bool) $this->verbose;
		$ppnInfo->csvColumns = 6;
		$ppnInfo->csvSeparator = "\t";
		$ppnInfo->csv("Erreur", "issn", "ppn", "Titre.id", "titre", "Modifications");

		$cmd = $this->findDataToCheck();
		foreach ($cmd->query() as $row) {
			$row['issn'] = (string) $row['issn']; // remove null
			if ($this->verbose > 1) {
				fprintf(STDERR, "Sending request to SUDOC for PPN %s\n", $row['sudocPpn']);
			}
			try {
				$notice = $this->sudocApi->getNotice($row['sudocPpn']);
			} catch (\Throwable $e) {
				$errorMsg = str_replace("\n", " ", $e->getMessage());
				$ppnInfo->csv("ERR", $row['issn'], $row['sudocPpn'], $row['titreId'], $row['titre'], $errorMsg);
				continue;
			}
			if ($this->verbose > 1) {
				fprintf(STDERR, "SUDOC for titreId %d: %s\n", $row['titreId'], json_encode($notice, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));
			}
			if ($notice) {
				$ppnInfo->apply($row, $notice);
				if ($notice->getMarc()) {
					$this->createEtatscollectionFromMarc($notice->getMarc(), $row['id']);
				}
			} elseif ($row['issn']) {
				$notice = new Notice();
				$notice->issn = $row['issn'];
				$notice->ppn = $row['sudocPpn'];
				$notice->sudocNoHolding = true;
				$ppnInfo->apply($row, $notice);
			} else {
				$ppnInfo->csv("ERR", '', $row['sudocPpn'], $row['titreId'], $row['titre'], "notice non-trouvée");
			}
		}
	}

	private function findDataToCheck(): \CDbCommand
	{
		$extraCondition = ($this->onlyDubious ? " AND (i.worldcatOcn IS NULL OR i.sudocNoHolding = 1 OR i.support = 'inconnu' OR i.dateDebut = '' OR i.dateFin = '')" : "");
		$limitCondition = ($this->limit ? "LIMIT {$this->limit}" : '');
		return Yii::app()->db
			->createCommand(<<<EOSQL
				SELECT i.*, t.titre, t.revueId
				FROM Issn i
					JOIN Titre t ON i.titreId = t.id
				WHERE
					i.sudocPpn IS NOT NULL AND i.sudocPpn <> ''
					$extraCondition
				ORDER BY i.id ASC $limitCondition
				EOSQL
			)
			->setFetchMode(\PDO::FETCH_ASSOC);
	}

	private function createEtatscollectionFromMarc(\models\marcxml\MarcTitre $marc, int $issnId): void
	{
		$dataFields = $marc->readFields('955');
		$rcrEtats = [];
		foreach ($dataFields as $dataField) {
			$rawRcr = $dataField['5'] ?? null;
			$etat = $dataField['r'] ?? null;
			if ($rawRcr && $etat) {
				$rcr = explode(':', $rawRcr)[0];
				if (!isset($rcrEtats[$rcr])) {
					$rcrEtats[$rcr] = [];
				}
				$rcrEtats[$rcr][] = $etat;
			}
		}

		Etatcollection::remplaceEtatcollection($issnId, $rcrEtats);
		// Delete all Etatcollection not in the last import for this Issn
		Yii::app()->db
			->createCommand(<<<EOSQL
				DELETE FROM Etatcollection
				WHERE lastImport < DATE_SUB(NOW(), INTERVAL 24 HOUR)
					AND issnId = {$issnId};
				EOSQL
			)
			->execute();
	}
}
