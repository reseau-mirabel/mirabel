<?php

namespace commands\models;

use Exception;
use ImportedLink;
use Sourcelien;
use Titre;
use Yii;
use models\import\ImportType;

class LinkUpdater
{
	public int $simulation = 0;

	/**
	 * @var int
	 * verbose = 1: CSV lists updated but unchanged links
	 * verbose = 2: CSV lists unrecognized Héloïse links
	 */
	public int $verbose = 0;

	private int $importType = 0;

	private ?Sourcelien $defaultSource = null;

	/**
	 * @var Sourcelien[]
	 */
	private array $sources = [];

	private int $linksErrors = 0;

	private int $linksUpdated = 0;

	private int $linksCreated = 0;

	private int $linksRemoved = 0;

	private int $titlesChanged = 0;

	public function __construct(?Sourcelien $source = null)
	{
		if ($source) {
			$this->addSource($source);
		}
	}

	public function addSource(Sourcelien $source): void
	{
		$this->sources[$source->id] = $source;
		if ($this->defaultSource === null) {
			$this->defaultSource = $source;
		}
		if ($this->importType) {
			if ($this->importType !== ImportType::getSourceId($source->nomcourt)) {
				throw new \Exception("The sources must all have the same import type!");
			}
		} else {
			$this->importType = ImportType::getSourceId($source->nomcourt);
			if (!$this->importType) {
				throw new \Exception("This source '{$source->nomcourt}' does not have an import type!");
			}
		}
	}

	public function hasChanges(): bool
	{
		return $this->linksCreated > 0 || $this->linksErrors > 0 || $this->linksRemoved || $this->linksUpdated > 0;
	}

	public function getStats(): string
	{
		if ($this->verbose === 0 && !$this->hasChanges()) {
			return "";
		}
		$stats = [];
		$multiple = count($this->sources) > 1;
		if ($multiple && $this->titlesChanged > 0) {
			$stats[] = sprintf("Titres modifiés %d", $this->titlesChanged);
		}
		if ($this->linksUpdated) {
			$stats[] = sprintf("Modifications %d", $this->linksUpdated);
		}
		if ($this->linksCreated) {
			$stats[] = sprintf("Créations %d", $this->linksCreated);
		}
		if ($this->linksRemoved) {
			$stats[] = sprintf("Suppressions %d", $this->linksRemoved);
		}
		if (!$this->linksUpdated && !$this->linksCreated && !$this->linksRemoved) {
			$stats[] = "Aucun changement (ni modification, ni création, ni suppression)";
		}
		if ($this->linksErrors) {
			$stats[] = sprintf("Erreurs : %d", $this->linksErrors);
		}
		return join(" / ", $stats)
			. sprintf("\n\n## Import des liens (%s)", $multiple ? "sources multiples" : $this->defaultSource->nom);
	}

	/**
	 * Update the timestamp of the Titre's links, without changing the URLs.
	 *
	 * Useful when there was a network error: when in doubt, suppose they're present.
	 */
	public function refreshLink(int $titreId, int $httpCode): void
	{
		$ids = join(", ", array_keys($this->sources));
		\Yii::app()->db
			->createCommand(<<<EOSQL
				REPLACE INTO ImportedLink (id, sourceId, titreId, url, lastSeen, httpcode)
					SELECT UNHEX(SHA1(l.url)), l.sourceId, l.titreId, l.url, :now, $httpCode
					FROM LienTitre l
					WHERE l.titreId = {$titreId} AND l.sourceId IN ($ids)
				EOSQL
			)
			->execute([':now' => time()]);
	}

	/**
	 * @param string $urlPattern Containing {{id}} or {{issn}}
	 * @return int number of Titre records updated
	 */
	public function removeByIssn(string $csv, string $urlPattern): int
	{
		[$countIssns, ] = LoadIssns::readFromCsv($csv, [1, 2], 2, "\t");
		if ($this->verbose >= 2) {
			fprintf(STDERR, "ISSN lus (suppression) : %d\n", $countIssns);
		}
		$titres = \Yii::app()->db->createCommand(
			<<<EOSQL
			SELECT t.*, k.id AS sourcekey, k.issn
			FROM Titre t
				JOIN Issn i ON (i.titreId = t.id)
				JOIN knownissn k ON (i.issn = k.issn OR i.issnl = k.issn)
			WHERE k.id != ''
			GROUP BY t.id
			EOSQL
		)->queryAll();
		fprintf(STDERR, "Suppressions possibles : %d\n", count($titres));
		if (!$this->simulation) {
			foreach ($titres as $row) {
				$sourceKey = $row['sourcekey']; // id in the loaded data (may be an ISSN)
				$issn = $row['issn']; // ISSN shared by loaded data and Mirabel data
				unset($row['sourcekey'], $row['issn']);
				$titre = Titre::model()->populateRecord($row);
				$titre->setConfirm(true);
				$url = str_replace(['{{id}}', '{{issn}}'], [$sourceKey, $issn], $urlPattern);
				if ($this->removeLink($titre, $sourceKey, $url)) {
					$this->linksRemoved++;
				}
			}
		}
		return count($titres);
	}

	/**
	 * Uses the temp table created by LoadIssn::readFromCsv().
	 *
	 * @param string|callable $urlPattern (if string, must contain {{id}} or {{issn}})
	 * @return string CSV of changes
	 */
	public function updateByIssn($urlPattern): string
	{
		self::validateUrlPattern($urlPattern);
		// Find by ISSN and ISSN-L
		$titres = \Yii::app()->db
			->createCommand(
				<<<EOSQL
				SELECT * FROM (
					SELECT t.*, k.id AS sourcekey, k.issn FROM Titre t JOIN Issn i ON (i.titreId = t.id) JOIN knownissn k ON (i.issn = k.issn) WHERE i.issn IS NOT NULL
					UNION
					SELECT t.*, k.id AS sourcekey, k.issn FROM Titre t JOIN Issn i ON (i.titreId = t.id) JOIN knownissn k ON (i.issnl = k.issn) WHERE i.issn IS NOT NULL
				) t
				GROUP BY id
				EOSQL
			)
			->query();
		return $this->updateByIssnRaw($urlPattern, $titres);
	}

	/**
	 * @param string|callable $urlPattern Function (string $idJoinedByComma, string $issnJoinedByComma): string
	 * @return string CSV of changes
	 */
	public function updateByIssnGroup($urlPattern): string
	{
		$sql = <<< EOSQL
			SELECT
				t.*,
				GROUP_CONCAT(DISTINCT k.id SEPARATOR ',') AS sourcekey,
				GROUP_CONCAT(DISTINCT k.issn SEPARATOR ',') AS issn
			FROM Titre t
				JOIN Issn i ON (i.titreId = t.id)
				JOIN knownissn k ON (i.issn = k.issn)
			WHERE i.issn IS NOT NULL
			GROUP BY t.id
			EOSQL;
		$groupes = \Yii::app()->db->createCommand($sql)->query();
		return $this->updateByIssnRaw($urlPattern, $groupes);
	}

	/**
	 * @param Titre $titre
	 * @param string $identifier Only used in logs (e.g. list of ISSN numbers)
	 * @param string $url URL attached to this Titre and $this->defaultSource
	 * @return string CSV rows
	 */
	public function updateLink(Titre $titre, string $identifier, string $url): string
	{
		assert($this->defaultSource !== null);
		return $this->updateLinks($titre, $identifier, [[(int) $this->defaultSource->id, $url]]);
	}

	/**
	 * @param Titre $titre
	 * @param string $identifier Only used in logs (e.g. list of ISSN numbers)
	 * @param array $urls e.g. [ [sourceID1, URL1], [sourceName2, URL2] ]
	 * @return string CSV rows
	 */
	public function updateLinks(Titre $titre, string $identifier, array $urls): string
	{
		if (empty($urls)) {
			return "";
		}
		if (!isset($urls[0]) || !is_array($urls[0]) || count($urls[0]) !== 2) {
			throw new \Exception("updateLinks() expects URLs like [ [sourceID1, URL1], [sourceID2, URL2] ]");
		}

		$remainingUrls = [];
		foreach ($urls as [$sourceId, $url]) {
			$source = $this->getSource($sourceId);
			$remainingUrls[] = [
				(int) $source->id,
				$url,
				$source->getDomain(),
			];

			$importedLink = new ImportedLink();
			$importedLink->url = $url;
			$importedLink->sourceId = (int) $source->id;
			$importedLink->titreId = (int) $titre->id;
			$importedLink->save();
		}

		$liens = $titre->getLiens();
		$output = fopen('php://memory', 'w');
		$linksCreated = 0;
		$linksUpdated = 0;
		foreach ($liens as $lien) {
			/** \Lien @var $lien */
			$lien->disableUrlValidation();
			foreach ($remainingUrls as $i => [$sourceId, $url, $sourceDomain]) {
				if ($lien->url !== $url && !in_array($sourceDomain, \components\UrlHelper::extractAllDomains($lien->url))) {
					continue;
				}
				$source = $this->getSource($sourceId);
				if ($lien->url === $url && $lien->src === $source->nom) {
					if ($this->verbose > 1) {
						self::addCsvlogRow($output, $titre, $identifier, $url, "Lien inchangé", "OK");
					}
				} else {
					// (same URL but different source name) OR (same domain but different URL)
					// So we update in-place the Lien object.
					$lien->src = $source->nom;
					$lien->sourceId = (int) $source->id;
					$lien->url = $url;
					$linksUpdated++;
					self::addCsvlogRow($output, $titre, $identifier, $url, "Lien modifié", "OK");
				}
				unset($remainingUrls[$i]);
			}
		}
		// adding the remaining links
		foreach ($remainingUrls as [$sourceId, $url, ]) {
			$source = $this->getSource($sourceId);
			$liens->add([
				'src' => $source->nom,
				'sourceId' => (int) $source->id,
				'url' => $url,
			]);
			self::addCsvlogRow($output, $titre, $identifier, $url, "Lien ajouté", "OK");
			$linksCreated++;
		}
		if ($linksCreated || $linksUpdated) {
			$success = $this->updateTitle($titre, $liens);
			if ($success !== 'OK') {
				self::addCsvlogRow($output, $titre, $identifier, '', "enregistrement", $success);
			}
			$this->linksUpdated += $linksUpdated;
			$this->linksCreated += $linksCreated;
			$this->titlesChanged++;
		}
		return (string) stream_get_contents($output, -1, 0);
	}

	public static function getCsvlogHeader(): string
	{
		return join(';', ['Revue-id', 'Titre-id', 'Titre-alpha', 'Identifiant', 'Url', 'Message', 'État', "Revue-URL"]) . "\n";
	}

	/**
	 * @param string|callable $urlPattern
	 * @param iterable&\Countable $titres
	 * @return string CSV of changes
	 */
	private function updateByIssnRaw($urlPattern, $titres): string
	{
		if ($this->verbose >= 2) {
			fprintf(STDERR, "Titres de Mir@bel concernés : %d\n\n", count($titres));
		}
		if ($this->simulation) {
			return "";
		}
		$output = "";
		foreach ($titres as $row) {
			$sourceKey = $row['sourcekey']; // id in the loaded data (may be an ISSN) ; may be a group_concat
			$issn = $row['issn']; // ISSN shared by loaded data and Mirabel data ; may be a group_concat
			unset($row['sourcekey'], $row['issn']);
			$titre = Titre::model()->populateRecord($row);
			$titre->setConfirm(true);
			if ($sourceKey) {
				if (is_string($urlPattern)) {
					$url = str_replace(['{{id}}', '{{issn}}'], [$sourceKey, $issn], $urlPattern);
				} else {
					$url = $urlPattern($sourceKey, $issn);
				}
				if ($url) {
					$output .= $this->updateLink($titre, $sourceKey, $url);
				} else {
					\Yii::log(
						"URL vide pour l'import de l'ISSN '$issn'"
							. ($this->defaultSource ? " (source {$this->defaultSource->nom})" : ""),
						'warning',
						'import'
					);
				}
			} else {
				Yii::log("Utilisation d'un ISSN vide pour le lien du titre {$titre->id}", \CLogger::LEVEL_WARNING);
			}
		}
		return $output;
	}

	/**
	 * Validation applicable si $pattern est une chaîne, sans effet si c'est une fonction callable
	 * @param string|callable $pattern
	 * @throws Exception
	 */
	private function validateUrlPattern($pattern): void
	{
		if (!is_string($pattern)) {
			return;
		}
		if (strpos($pattern, '{{id}}') === false && strpos($pattern, '{{issn}}') === false) {
			throw new Exception("Invalid URL pattern, with nothing to fill in");
		}
	}

	/**
	 * @param resource $f
	 */
	private static function addCsvlogRow($f, Titre $titre, string $identifier, string $url, string $message, string $status): void
	{
		$urlRevue = Yii::app()->params->itemAt('baseUrl') . "/revue/{$titre->revueId}";
		fputcsv($f, [$titre->revueId, $titre->id, $titre->titre, $identifier, $url, $message, $status, $urlRevue], ';', '"', '\\');
	}

	/**
	 * @param int|string $id An int Source.id or a string Source.nomcourt.
	 */
	private function getSource($id): Sourcelien
	{
		if (isset($this->sources[$id])) {
			return $this->sources[$id];
		}
		foreach ($this->sources as $s) {
			if ($s->nomcourt === $id) {
				return $s;
			}
		}
		throw new \Exception("updateLinks() received an unexpected source ID '$id'.");
	}

	private function removeLink(Titre $titre, string $issn, string $url): bool
	{
		$liens = $titre->getLiens();
		foreach ($liens as $lien) {
			/** @var \Lien $lien */
			if ($lien->url === $url) {
				$liens->remove($lien);
				$success = $this->updateTitle($titre, $liens);
				$output = fopen('php://output', 'w');
				self::addCsvlogRow($output, $titre, $issn, '', "Lien supprimé", $success);
				$this->linksRemoved++;
				return true;
			}
		}
		return false;
	}

	/**
	 * Update the DB record (with an Intervention), then return "OK" or an error message.
	 */
	private function updateTitle(Titre $titre, \Liens $liens): string
	{
		$titre->setScenario('import');
		$oldTitle = clone $titre;
		$titre->setLiens($liens);
		$success = "OK";
		if (!$this->simulation) {
			$i = $titre->buildIntervention(true);
			$i->setImport($this->importType);
			$i->description = "Modification du titre « {$titre->titre} »";
			$i->contenuJson->update($oldTitle, $titre);
			$i->setScenario('import');
			try {
				if (!$i->accept(true)) {
					$success = "ERREUR " . print_r($i->getErrors(), true);
					$this->linksErrors++;
				}
			} catch (\Exception $e) {
				$transaction = Yii::app()->db->getCurrentTransaction();
				if ($transaction !== null) {
					$transaction->rollback();
				}
				Yii::log("updateLink() failed with Titre {$titre->id}: {$e->getMessage()}\nliens: " . print_r($liens, true), \CLogger::LEVEL_ERROR);
				$success = "ERREUR (exception)";
				$this->linksErrors++;
			}
		}
		return $success;
	}
}
