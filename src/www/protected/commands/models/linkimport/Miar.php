<?php

namespace commands\models\linkimport;

use commands\models\LoadIssns;

class Miar extends DefaultImport
{
	private const COLNUM_ISSN = [0]; // commence à 0

	private const COLNUM_URL = 2;

	private const CSV_SEPARATOR = ';';

	public string $csvFile = "";

	protected function importLinks(): CommandOutput
	{
		if (strncmp($this->csvFile, 'http', 4) === 0) {
			$csv = $this->downloadFile($this->csvFile);
		} else {
			$csv = $this->csvFile;
		}
		if (!$csv) {
			throw new \Exception("L'URL (ou le chemin) vers le fichier à importer est absente.");
		}

		self::checkCsvColumns($csv);

		[$countIssns, $countLines] = LoadIssns::readFromCsv(
			$csv,
			self::COLNUM_ISSN,
			self::COLNUM_URL,
			self::CSV_SEPARATOR
		);
		$countIssns += self::addOtherIssn($csv);
		if ($this->verbose >= 2) {
			fprintf(STDERR, "ISSN lus (MàJ) : %d  sur %d lignes de CSV\n", $countIssns, $countLines);
		}

		$source = $this->findSource('MIAR');
		$output = new CommandOutput();
		try {
			$output->setChanges($this->getLinkUpdater()->updateByIssn('{{id}}'));
		} catch (\Throwable $t) {
			$output->addError($t->getMessage());
		}
		$output->setSummary($this->getLinkUpdater()->getStats());
		$output->setObsolete($this->getLinksObsolescence($source)->listObsoleteLinks($this->obsoleteSince));
		$output->setNeverImported($this->listNeverImported($source));
		$source->nbimport = $countLines;
		$source->save(false, ['nbimport']);

		return $output;
	}

	protected static function getImportName(): string
	{
		return "MIAR";
	}

	private static function checkCsvColumns(string $file): void
	{
		if (!is_file($file)) {
			throw new \Exception("Fichier '$file' non trouvé.");
		}
		$fh = fopen($file, "r");
		if ($fh === false) {
			throw new \Exception("Erreur en ouvrant le fichier.");
		}

		$header = fgetcsv($fh, 0, self::CSV_SEPARATOR);
		if (strtolower($header[self::COLNUM_URL]) !== 'url') {
			throw new \Exception(sprintf("La colonne n°%d devrait s'intituler 'URL'. Abandon.", self::COLNUM_URL));
		}

		$row = fgetcsv($fh, 0, self::CSV_SEPARATOR);
		foreach (self::COLNUM_ISSN as $col) {
			if ($row[$col] === '' || preg_match('/\b\d{4}-\d{3}[\dXx]\b/', $row[$col])) {
				continue;
			}
			throw new \Exception("La colonne n°$col devrait être vide ou contenir un ISSN. Abandon.");
		}
		if (strncmp($row[self::COLNUM_URL], 'http', 4) !== 0) {
			throw new \Exception(sprintf("La colonne n°%d devrait contenir une URL. Abandon.", self::COLNUM_URL));
		}
	}

	/**
	 * Si l'ISSN principal est lié à un titre mort,
	 *  et l'ISSN secondaire au titre vivant de la même revue,
	 *  alors on ajouter l'ISSN secondaire avec l'URL déclarée par MIAR pour cette ligne.
	 *
	 * @param string $file
	 * @return int Number of new Issn added into the temp table.
	 */
	private static function addOtherIssn(string $file): int
	{
		$issnTable = \commands\models\IssnTempTable::getTableName();
		$deadIssnToAlive = \components\SqlHelper::sqlToPairs(<<<EOSQL
			SELECT tmp.issn, GROUP_CONCAT(iv.issn SEPARATOR ',') AS v
			FROM $issnTable tmp
				JOIN Issn i ON i.issn = tmp.issn
				JOIN Titre t ON i.titreId = t.id
				JOIN Titre tv ON t.revueId = tv.revueId AND tv.obsoletePar IS NULL
				JOIN Issn iv ON iv.titreId = tv.id
			WHERE t.obsoletePar IS NOT NULL
			GROUP BY tmp.issn
			EOSQL
		);

		$transaction = \Yii::app()->db->beginTransaction();
		$insert = \Yii::app()->db
			->createCommand("INSERT INTO $issnTable (id, issn) VALUES (:id, :issn)");
		$added = 0;

		$handle = fopen($file, "r");
		fgetcsv($handle, 0, self::CSV_SEPARATOR); // skip header line
		while (($rawdata = fgetcsv($handle, 0, self::CSV_SEPARATOR)) !== false) {
			[$mainIssn, $otherIssns, $url] = array_map('trim', $rawdata);
			if (!$otherIssns) {
				continue;
			}
			if (!isset($deadIssnToAlive[$mainIssn])) {
				continue;
			}
			$aliveIssns = explode(',', $deadIssnToAlive[$mainIssn]);
			foreach (explode(',', $otherIssns) as $otherIssn) {
				if (in_array($otherIssn, $aliveIssns, true)) {
					// L'ISSN alternatif $otherIssn est lié à un titre vivant.
					$insert->execute([':id' => $url, ':issn' => $otherIssn]);
					$added++;
				}
			}
		}
		$transaction->commit();
		return $added;
	}
}
