Import de liens
===============

Pour ajouter un nouvel import :

1. S'assurer que la source de liens existe (table `Sourcelien`).
2. Déclarer qu'un import existe pour cette source, dans `ImportType`, avec son `Sourcelien.nomcourt`.
3. Créer dans ce répertoire une classe dérivant de `DefaultImport`.
4. Ajouter dans `LinksCommand` l'action qui instancie cette classe.


Classe d'import
---------------

La classe parente a deux méthodes abstraites qu'il faut implémenter :

- `abstract protected function getImportName(): string;`
  renvoie un nom qui sert de sujet en cas d'envoi d'email.
- `abstract protected function importLinks(): CommandOutput;`
  renvoie le CSV des modifications.

Les affichages sur STDOUT ou STDERR ne doivent être utilisé que pour du débogage.
Avec la verbosité standard (`verbose=0`), rien ne doit être affiché directement,
tout doit être transmis dans l'insance `CommandOutput`.


Technique
---------

Le procédé usuel est :
1. Lire un fichier associant des *ISSN* à des *clés* spécifiques à la source.
2. Croiser ces ISSN avec ceux connus de M pour générer des URL.
   Chaque URL est construite à partir de son contexte (ISSN, clé).
3. Remplir l'objet `CommandOutput` avec les comptes-rendus de ces opérations.

Par exemple :
1. `LoadIssns::readFromCsv("a.csv", 0, 1, 2, ";")` charge dans une table temporaraire
   les données de "a.csv" formées de clé en colonne 2, et de l'ISSN en colonne 0 ou 1.
2. `$this->getLinkUpdater()->updateByIssn('https://hop.la/{{id}}')` utilise cette table temporaire
   pour associer à chaque titre (identifié par ses ISSN) une URL terminée par la clé lue à l'étape 1.
3. `$output->setChanges($csv)` où `$csv` est le résultat de l'étape 2, puis autres `$output->set*()`.
Cf `Doaj` pour une illustration.

### Variante 1

Si les données ne sont pas lues sous formes de couples (ISSN, clé) dans un CSV,
la table temporaire peut être alimentée manuellement avec `IssnTempTable`.
Cf `Openalex` pour une illustration.

### Variante 2

Si l'identification après chargement de l'ensemble des ISSN n'est pas adaptée,
alors il faut gérer par soi-même l'identification des titres,
et procéder aux mises à jour de liens par `LinkUpdater::updateLink()`.
Cf `Hal` pour une illustration.
