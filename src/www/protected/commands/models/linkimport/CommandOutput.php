<?php

namespace commands\models\linkimport;

class CommandOutput
{
	private array $errors = [];

	private string $summary = '';

	private string $changes = '';

	private string $obsolete = '';

	private string $neverImported = '';

	public function addError(string $error): void
	{
		$this->errors[] = $error;
	}

	public function getText(): string
	{
		$text = "";
		if ($this->errors) {
			$text .= "ERREURS :\n" . join("\n", $this->errors) . "\n\n";
		}
		$sections = array_map('trim', [$this->summary, $this->changes, $this->obsolete, $this->neverImported]);
		$text .= join("\n\n", array_filter($sections));
		if ($text) {
			$text .= "\n";
		}
		return $text;
	}

	public function setSummary(string $summary): void
	{
		$this->summary = $summary;
	}

	public function setChanges(string $changes): void
	{
		$this->changes = $changes;
	}

	public function setObsolete(string $obsolete): void
	{
		$this->obsolete = $obsolete;
	}

	public function setNeverImported(string $neverImported): void
	{
		$this->neverImported = $neverImported;
	}
}
