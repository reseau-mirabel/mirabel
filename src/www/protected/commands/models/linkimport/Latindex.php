<?php

namespace commands\models\linkimport;

use commands\models\LoadIssns;

class Latindex extends DefaultImport
{
	private const FULL_URL = "https://www.latindex.org/latindex/exportar/busquedaAvanzada/json/%7B%22idMod%22:%220%22%7D";

	private const LINK_PATTERN = 'https://www.latindex.org/latindex/ficha/{{id}}';

	protected function importLinks(): CommandOutput
	{
		$jsonFilename = $this->downloadFile(self::FULL_URL);

		$countIssns = LoadIssns::readFromJsonStream($jsonFilename, 'issn_imp', 'issn_e', 'folio_u');
		if ($this->verbose >= 2) {
			fprintf(STDERR, "ISSN lus (MàJ) : %d\n", $countIssns);
		}

		$source = $this->findSource('latindex');
		$output = new CommandOutput();
		try {
			$output->setChanges($this->getLinkUpdater()->updateByIssn(self::LINK_PATTERN));
		} catch (\Throwable $t) {
			$output->addError($t->getMessage());
		}
		$output->setSummary($this->getLinkUpdater()->getStats());
		$output->setObsolete($this->getLinksObsolescence($source)->listObsoleteLinks($this->obsoleteSince));
		$source->nbimport = $countIssns;
		$source->save(false, ['nbimport']);
		return $output;
	}

	protected static function getImportName(): string
	{
		return "Latindex";
	}
}
