<?php

namespace commands\models\linkimport;

use commands\models\IssnTempTable;
use components\Curl;
use Config;
use Issn;
use Yii;
use const STDERR;

/**
 * Import des liens de la source 'openalex'.
 *
 * La documentation de l'API : https://docs.openalex.org/api
 * On utilise le point d'entrée "venues" avec un filtre par ISSN pour des échanges par lots d'ISSN :
 *   /venues?filter=issn:xxx|yyy|zzz
 *
 * Les affichages sur STDOUT ou STDERR ne doivent être utilisé que pour du débogage.
 */
class Openalex extends DefaultImport
{
	/**
	 * Maximum pour les filtres 50, cf. https://docs.openalex.org/api/get-lists-of-entities/filter-entity-lists#addition-or
	 * Maximum pour la pagination (en sortie) 200, cf https://docs.openalex.org/api#text-paging
	 */
	private const BATCH_SIZE = 50;

	private const URL_API = 'https://api.openalex.org';

	private const LINK_BASE_SINGLE = 'https://explore.openalex.org/sources/';

	private const LINK_BASE_MULTI = 'https://explore.openalex.org/works?sort=cited_by_count:desc&filter=primary_location.source.id:';

	/**
	 * @var int Total number of results in the responses of Openalex
	 */
	private int $countResults = 0;

	/**
	 * @var ?callable Function f(array $issns): string.
	 */
	private $apiFetcher = null;

	public function setFetcher(callable $f): void
	{
		$this->apiFetcher = $f;
	}

	public static function buildLink(string $keys, string $issns): string
	{
		if (!preg_match('/^[Ss0-9,]+$/', $keys)) {
			Yii::log("Indentifiants incorrects dans l'import OpenAlex : $keys (ISSNs $issns)", 'warning', 'import');
			return '';
		}
		if (strpos($keys, ',')) {
			return self::LINK_BASE_MULTI . str_replace(',', '%7C', $keys); // "|" => ascii "0x7C"
		}
		return self::LINK_BASE_SINGLE . $keys;
	}

	protected function importLinks(): CommandOutput
	{
		$source = $this->findSource('openalex');
		$output = new CommandOutput();

		$countIssnInserted = $this->loadOpenalexSources($output);
		$this->vecho(1, sprintf("Réponses de l'API : %d retours soit %d ISSN.\n", $this->countResults, $countIssnInserted));

		try {
			$output->setChanges($this->getLinkUpdater()->updateByIssnGroup(self::buildLink(...)));
		} catch (\Throwable $t) {
			$output->addError($t->getMessage());
		}
		$output->setSummary($this->getLinkUpdater()->getStats());
		$output->setObsolete($this->getLinksObsolescence($source)->listObsoleteLinks($this->obsoleteSince));
		$output->setNeverImported($this->listNeverImported($source));
		$source->nbimport = $countIssnInserted;
		$source->save(false, ['nbimport']);
		return $output;
	}

	protected static function getImportName(): string
	{
		return "OpenAlex";
	}

	/**
	 * Query the OpenAlex API with every Mirabel ISSN and load the temp table required by LinkUpdater::updateByIssn().
	 *
	 * @return int Number of [ISSN, URL] inserted into the temp table.
	 */
	private function loadOpenalexSources(CommandOutput $output): int
	{
		$sql = "SELECT issn FROM Issn WHERE statut = :s ORDER BY titreId"
			. ($this->limit ? " LIMIT {$this->limit}" : "");
		$issns = Yii::app()->db->createCommand($sql)->queryColumn([':s' => Issn::STATUT_VALIDE]);
		$issnbatches = array_chunk($issns, self::BATCH_SIZE, false);

		if ($this->apiFetcher === null) {
			$this->apiFetcher = $this->createFetcher();
		}

		$issnTempTable = new IssnTempTable();
		$timeQueue = [];
		foreach ($issnbatches as $issnBatch) {
			// Slow down: max rate is 10 requests per second (with a 10% margin here)
			$now = microtime(true);
			if (count($timeQueue) >= 9) {
				$msElapsed = (int) ($now - $timeQueue[0]);
				assert($msElapsed >= 0);
				if ($msElapsed < 1100) {
					usleep($msElapsed);
				}
			}
			$timeQueue[] = microtime(true);
			if (count($timeQueue) > 9) {
				array_shift($timeQueue);
			}

			// Download from the API
			try {
				$response = ($this->apiFetcher)($issnBatch);
			} catch (\Throwable $t) {
				$output->addError($t->getMessage());
				continue;
			}

			$this->insertOpenalexIssns($response, $issnTempTable);
		}
		$issnTempTable->close();
		return $issnTempTable->getCount();
	}

	/**
	 * Return a function that converts an array of ISSNs into a JSON string.
	 *
	 * @codeCoverageIgnore
	 */
	private function createFetcher(): callable
	{
		$curl = new Curl;
		$curl->setopt(CURLOPT_TIMEOUT, 10); // seconds
		// indiquer un email pour être poli (/polite pool/ dans la doc), mais ça ne change rien en réalité
		$curl->setopt(CURLOPT_USERAGENT, sprintf('PHP cURL mailto:%s', Config::read('contact.visible.email')));

		return function (array $issns) use ($curl): string {
			// Fetch the data from the API
			$queryUrl = sprintf('%s/sources?per-page=%d&filter=issn:%s', self::URL_API, count($issns), join('|', $issns));
			$curlget = $curl->get($queryUrl);
			if ($curlget->getHttpCode() >= 400) {
				throw new \Exception("Erreur de téléchargement pour '$queryUrl' : HTTP {$curl->getHttpCode()}");
			}
			return $curlget->getContent();
		};
	}

	/**
	 * Load the response data into the temp table
	 */
	private function insertOpenalexIssns(string $response, IssnTempTable $table): void
	{
		$content = json_decode($response, false, 512, JSON_THROW_ON_ERROR);
		$this->countResults += count($content->results);
		foreach ($content->results as $result) {
			$id = str_replace('https://openalex.org/', '', $result->id);
			foreach ($result->issn as $issn) {
				$table->insertIssn($id, $issn);
			}
		}
	}

	/**
	 * @param int $verbosity seuil
	 * @param string $text
	 */
	private function vecho(int $verbosity, string $text): void
	{
		if ($this->verbose >= $verbosity) {
			fprintf(STDERR, $text . "\n");
		}
	}
}
