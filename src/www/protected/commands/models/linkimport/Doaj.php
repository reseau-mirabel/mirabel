<?php

namespace commands\models\linkimport;

use commands\models\LoadIssns;

class Doaj extends DefaultImport
{
	private const COLNUM_ISSNE = 6;

	private const COLNUM_ISSNP = 5;

	private const CSV_SEPARATOR = ',';

	protected function importLinks(): CommandOutput
	{
		$csvFilename = $this->downloadFile("https://doaj.org/csv");

		[$countIssns, $countLines] = LoadIssns::readFromCsv(
			$csvFilename,
			[self::COLNUM_ISSNP, self::COLNUM_ISSNE],
			self::COLNUM_ISSNE,
			self::CSV_SEPARATOR
		);
		if ($this->verbose >= 2) {
			fprintf(STDERR, "ISSN lus (MàJ) : %d  sur %d lignes de CSV\n", $countIssns, $countLines);
		}

		$source = $this->findSource('DOAJ');
		$output = new CommandOutput();
		try {
			$output->setChanges($this->getLinkUpdater()->updateByIssn('https://doaj.org/toc/{{id}}'));
		} catch (\Throwable $t) {
			$output->addError($t->getMessage());
		}
		$output->setSummary($this->getLinkUpdater()->getStats());
		$output->setObsolete($this->getLinksObsolescence($source)->listObsoleteLinks($this->obsoleteSince));
		$output->setNeverImported($this->listNeverImported($source));
		$source->nbimport = $countLines;
		$source->save(false, ['nbimport']);

		return $output;
	}

	protected static function getImportName(): string
	{
		return "DOAJ";
	}
}
