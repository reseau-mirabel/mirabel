<?php

namespace commands\models\linkimport;

class CairnMundo extends CairnInt
{
	protected static function getImportName(): string
	{
		return "Cairn Mundo";
	}

	protected function initSource(): \Sourcelien
	{
		return $this->findSource('cairnmundo');
	}
}
