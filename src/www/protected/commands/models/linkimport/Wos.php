<?php

namespace commands\models\linkimport;

use commands\models\LoadIssns;

class Wos extends DefaultImport
{
	private const COLNUM_ISSNP = 1;

	private const COLNUM_ISSNE = 2;

	private const CSV_SEPARATOR = ',';

	private const CSV_HEADER = [
		"Journal title",
		"ISSN",
		"eISSN",
	];

	private const LINK_MODEL = 'https://mjl.clarivate.com/cgi-bin/jrnlst/jlresults.cgi?PC=MASTER&ISSN={{id}}';

	public string $csvFile = "";

	protected function importLinks(): CommandOutput
	{
		// $this->csvFile peut venir d'un appel ligne de commande (url ou fichier local)
		// ou du webcron (obligatoirement url dans ce cas)
		if (strncmp($this->csvFile, 'http', 4) === 0) {
			$basename = preg_replace('/[^\w\.-]+/', '', basename($this->csvFile));
			$csv = $this->downloadFile($this->csvFile, $basename);
		} else {
			$csv = $this->csvFile;
		}
		if (!$csv) {
			throw new \Exception("L'URL (ou le chemin) vers le fichier à importer est absente.");
		}

		self::checkCsvColumns($csv);

		[$countIssns, $countLines] = LoadIssns::readFromCsv(
			$csv,
			[self::COLNUM_ISSNP, self::COLNUM_ISSNE],
			null,
			self::CSV_SEPARATOR
		);
		if ($this->verbose >= 2) {
			fprintf(STDERR, "ISSN lus (MàJ) : %d  sur %d lignes de CSV\n", $countIssns, $countLines);
		}

		$source = $this->findSource('WOS');
		$output = new CommandOutput();
		try {
			$output->setChanges($this->getLinkUpdater()->updateByIssn(self::LINK_MODEL));
		} catch (\Throwable $t) {
			$output->addError($t->getMessage());
		}
		$output->setSummary($this->getLinkUpdater()->getStats());
		$output->setObsolete($this->getLinksObsolescence($source)->listObsoleteLinks($this->obsoleteSince));
		$output->setNeverImported($this->listNeverImported($source));
		$source->nbimport = $countLines;
		$source->save(false, ['nbimport']);

		return $output;
	}

	protected static function getImportName(): string
	{
		return "WOS";
	}

	private function checkCsvColumns($file): void
	{
		if (!is_file($file)) {
			throw new \Exception("Fichier '$file' non trouvé.");
		}
		$fh = fopen($file, "r");
		if ($fh === false) {
			throw new \Exception("Erreur en ouvrant le fichier.");
		}

		$header = fgetcsv($fh, 0, self::CSV_SEPARATOR);
		foreach (self::CSV_HEADER as $col => $hvalue) {
			if ($header[$col] === $hvalue) {
				continue;
			}
			throw new \Exception("La colonne n°$col devrait contenir '$hvalue'. Abandon.\n");
		}
	}
}
