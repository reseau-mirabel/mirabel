<?php

namespace commands\models\linkimport;

use commands\models\LinksObsolescence;
use commands\models\LinkUpdater;
use components\Curl;
use components\email\Mailer;
use Config;
use Sourcelien;
use Yii;
use function ctype_digit;

abstract class DefaultImport
{
	private const DLD_DEFAULT_FILENAME = 'toimport';

	/**
	 * @var array
	 */
	protected $email = [];

	/**
	 * @var int If set to a positive value, will stop after this many iterations.
	 */
	protected $limit = 0;

	protected bool $obsoleteDelete = false;

	/**
	 * @var int timestamp
	 */
	protected $obsoleteSince = 0;

	/**
	 * @var int cache duration in days. Do not download if the file is recent enough (0 => last hour).
	 */
	protected $cacheDuration = 0;

	/**
	 * @var bool
	 */
	protected $simulation = false;

	/**
	 * @var int
	 */
	protected $verbose = 0;

	/**
	 * @var LinkUpdater
	 */
	private $linkUpdater;

	/**
	 * @param array $config Possibke keys: email, limit, obsoleteSince, simulation, verbose
	 */
	public function __construct(array $config)
	{
		if (!empty($config['email'])) {
			$this->setEmail($config['email']);
		}
		if (!empty($config['cacheDuration'])) {
			$this->cacheDuration = (int) $config['cacheDuration'];
		}
		if (!empty($config['obsoleteSince'])) {
			$this->setObsoleteSince($config['obsoleteSince']);
		}
		if (isset($config['limit'])) {
			$this->limit = (int) $config['limit'];
		}
		if (isset($config['simulation'])) {
			$this->simulation = (bool) $config['simulation'];
		}
		if (isset($config['verbose'])) {
			$this->verbose = (int) $config['verbose'];
		}
		$this->obsoleteDelete = !$this->simulation
			&& ($this->obsoleteSince > 0)
			&& ($config['obsoleteDelete'] ?? false);
	}

	public static function getLastDownloadDate(): string
	{
		$store = self::initStore();
		$inStorePath = $store->get(self::DLD_DEFAULT_FILENAME);
		if (!$inStorePath) {
			return "";
		}
		return date("Y-m-d H:i:s", stat($inStorePath)['mtime']);
	}

	public function import(): string
	{
		$output = $this->importLinks();
		$body = $output->getText();
		if ($body && $this->email) {
			$message = Mailer::newMail()
				->subject(sprintf("[%s] Mises à jour %s", Yii::app()->name, static::getImportName()))
				->from(Config::read('email.from'))
				->setTo($this->email)
				->text($body);
			Mailer::sendMail($message);
			return "";
		}
		return $body;
	}

	protected static function initStore(): \components\FileStore
	{
		$asciiName = preg_replace('/\W/', '-', \Norm::unaccent(static::getImportName()));
		$storeName = "liens_" . trim($asciiName, "-");
		$store = new \components\FileStore($storeName);
		return $store;
	}

	/**
	 * Download an URL into the local file-system and return its temporary file name.
	 *
	 * @return string E.g. "/tmp/Erih_2020-12-14_QFXpHS"
	 */
	protected function downloadFile(string $url, string $localFilename = self::DLD_DEFAULT_FILENAME): string
	{
		if ($this->verbose > 0) {
			fprintf(STDERR, "Téléchargement de '%s'\n", $url);
		}
		$expireSeconds = $this->cacheDuration > 0 ? 86400 * $this->cacheDuration : 3600;
		$store = self::initStore();

		$currentFile = $store->get($localFilename);
		if ($currentFile) {
			$localTime = stat($currentFile)[10];
			if ((time() - $localTime) < $expireSeconds) {
				if ($this->verbose > 0) {
					fprintf(STDERR, "Le téléchargement de '%s' est suffisament récent.\n", $url);
				}
				return $currentFile;
			}

			// The local file is old.
			// Ask if the remote file is newer, ONLY IF the URL's last part looks static.
			if (preg_match('/^[A-Za-z0-9_ .-]+$/', basename($url))) {
				$curl = new Curl();
				$remoteTime = $curl->getFileTime($url);
				if ($remoteTime && $remoteTime <= $localTime) {
					if ($this->verbose > 0) {
						fprintf(STDERR, "La copie de '%s' est à jour, pas de téléchargement.\n", $url);
					}
					return $currentFile;
				}
			}
		}

		// No local file or obsolete local file.
		$store->flushOlderThan($expireSeconds);
		if ($this->verbose > 1) {
			fprintf(STDERR, "Le téléchargement de '%s' est en cours...\n", $url);
		}
		$dld = new \components\curl\Downloader("*");
		$dld->download($url);
		$store->set($localFilename, $dld->getLocalFilepath());

		return $store->get($localFilename);
	}

	protected function findSource(string $sourceName): Sourcelien
	{
		$source = Sourcelien::model()->findByAttributes(['nomcourt' => $sourceName]);
		if (!$source) {
			throw new \Exception("Source '$sourceName' introuvable. Erreur dans le code source.");
		}
		if ($this->linkUpdater === null) {
			$this->linkUpdater = new LinkUpdater($source);
			$this->linkUpdater->simulation = (int) $this->simulation;
			$this->linkUpdater->verbose = $this->verbose;
		} else {
			$this->linkUpdater->addSource($source);
		}
		return $source;
	}

	protected function getLinksObsolescence(Sourcelien $source): LinksObsolescence
	{
		$lo = new LinksObsolescence($source);
		$lo->setAutoDelete($this->obsoleteDelete);
		return $lo;
	}

	protected function getLinkUpdater()
	{
		if ($this->linkUpdater === null) {
			throw new \Exception("linkUpdater was not set. The code must call findSource() first.");
		}
		return $this->linkUpdater;
	}

	protected function listNeverImported(Sourcelien $source): string
	{
		if (\commands\models\IssnTempTable::exists()) {
			// Titre ayant un lien de cette source, mais pas d'ISSN de cet import
			$tempTable = \commands\models\IssnTempTable::getTableName();
			$rows = Yii::app()->db
				->createCommand(<<<EOSQL
					WITH num_imported_issn AS (
						SELECT l.titreId, count(i.issn) AS mapped
						FROM LienTitre l
							JOIN Issn i ON l.titreId = i.titreId
							LEFT JOIN $tempTable tt ON i.issn = tt.issn
						WHERE l.sourceId = {$source->id}
						GROUP BY l.titreId
					)
					SELECT
						t.revueId, t.id, t.titre,
						lt.url
					FROM Titre t
						JOIN num_imported_issn n ON n.titreId = t.id AND n.mapped = 0 -- no imported ISSN
						JOIN LienTitre lt ON lt.titreId = t.id AND lt.sourceId = {$source->id}
					ORDER BY t.titre
					EOSQL)
				->queryAll();
		} else {
			// Do NOT join using titreId since a single ImportedLink may match 2 Titre, see #5248
			$rows = Yii::app()->db
				->createCommand(<<<EOSQL
					SELECT
						t.revueId, t.id, t.titre,
						lt.url
					FROM Titre t
						JOIN LienTitre lt ON lt.titreId = t.id
						LEFT JOIN ImportedLink l ON l.sourceId = lt.sourceId AND l.url = lt.url
					WHERE
						lt.sourceId = {$source->id}
						AND l.id IS NULL
					ORDER BY t.titre
					EOSQL
				)
				->queryAll();
		}
		if (!$rows) {
			return '';
		}
		$out = fopen('php://temp', 'w');
		foreach ($rows as $row) {
			$urlM = sprintf("%s/revue/%d", \Yii::app()->params->itemAt('baseUrl'), $row['revueId']);
			fputcsv($out, [$row['revueId'], $row['id'], $row['url'], $row['titre'], $urlM], ';', '"', '\\');
		}
		$content = "\n# Liens présents dans M mais jamais importés\n" . stream_get_contents($out, -1, 0);
		fclose($out);
		return $content;
	}

	/**
	 * @return string The name of the import, used in the subject of the email.
	 */
	abstract protected static function getimportName(): string;

	/**
	 * The main import process, called by import().
	 */
	abstract protected function importLinks(): CommandOutput;

	/**
	 * @param array|string $to
	 * @throws \Exception
	 * @return void
	 */
	protected function setEmail($to): void
	{
		if (is_array($to)) {
			$this->email = $to;
		} elseif (is_string($to) && strpos($to, '@') !== false) {
			$this->email = [$to];
		} else {
			throw new \Exception("Invalid 'email' value. Array|string expected, got " . var_export($to, true));
		}
	}

	/**
	 * @param string $since
	 * @throws \Exception
	 * @return void
	 */
	protected function setObsoleteSince($since): void
	{
		$timestamp = null;
		if (ctype_digit($since)) {
			$timestamp = (int) $since;
		} elseif (is_string($since)) {
			$timestamp = strtotime($since);
		}
		if (empty($timestamp)) {
			throw new \Exception("Invalid time value. Expected timestamp or e.g. '3 days ago', got " . var_export($since, true));
		}
		$this->obsoleteSince = $timestamp;
	}
}
