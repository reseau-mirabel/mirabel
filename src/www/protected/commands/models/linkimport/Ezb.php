<?php

namespace commands\models\linkimport;

use commands\models\LoadIssns;

class Ezb extends DefaultImport
{
	private const CSV_HEADER = [
		'E-ISSN',
		'P-ISSN',
		'EZB-Id',
	];

	private const COLNUM_URL = null;

	public string $csvFile = "";

	private $csvSeparator = "\t";

	protected function importLinks(): CommandOutput
	{
		if (strncmp($this->csvFile, 'http', 4) === 0) {
			$csv = $this->downloadFile($this->csvFile);
		} else {
			$csv = $this->csvFile;
		}
		if (empty($csv)) {
			throw new \Exception("L'import a besoin d'un chemin ou URL vers le CSV.");
		}
		$handle = fopen($csv, "r");
		if (!$handle) {
			throw new \Exception("Impossible d'ouvrir le fichier CSV d'EZB en lecture.");
		}

		$columns = $this->findCsvColumns($handle);
		if (count($columns) !== count(self::CSV_HEADER)) {
			throw new \Exception("L'entête du fichier CSV d'EZB ne contient pas de colonne " . join(" ou ", self::CSV_HEADER));
		}

		[$countIssns, $countLines] = LoadIssns::readFromCsvHandle(
			$handle,
			[$columns['E-ISSN'], $columns['P-ISSN']],
			self::COLNUM_URL,
			$this->csvSeparator
		);
		if ($this->verbose >= 2) {
			fprintf(STDERR, "ISSN lus (MàJ) : %d  sur %d lignes de CSV\n", $countIssns, $countLines);
		}

		$source = $this->findSource('ezb');
		$source->nbimport = $countLines;

		$output = new CommandOutput();
		$callableUrl = function ($keyIgnored, $issngroup) {
			$urlbase = 'https://ezb.ur.de/searchres.phtml?lang=en';
			//ex. &jq_bool2=OR&jq_type2=IS&jq_term2=1875-6719
			$url = $urlbase;
			$i = 0;
			foreach (explode(',', $issngroup) as $issn) {
				$i++;
				$url .= ($i > 1 ? sprintf("&jq_bool%d=OR", $i) : "");
				$url .= sprintf("&jq_type%d=IS&jq_term%d=%s", $i, $i, $issn);
			}
			return $url;
		};
		try {
			$output->setChanges($this->getLinkUpdater()->updateByIssnGroup($callableUrl));
		} catch (\Throwable $t) {
			$output->addError($t->getMessage());
		}
		$output->setSummary($this->getLinkUpdater()->getStats());

		$output->setObsolete($this->getLinksObsolescence($source)->listObsoleteLinks($this->obsoleteSince));
		$output->setNeverImported($this->listNeverImported($source));
		$source->save(false, ['nbimport']);
		return $output;
	}

	protected static function getImportName(): string
	{
		return "EZB";
	}

	/**
	 * @param resource $fh
	 * @return array<"E-ISSN"|"P-ISSN"|"EZB-Id", int>
	 */
	private function findCsvColumns($fh): array
	{
		$firstLine = fgets($fh);
		$m = [];
		if (preg_match('/^"sep=(.+)"\n?$/', $firstLine, $m)) {
			$this->csvSeparator = $m[1];
		}

		// Search the first row with multiple columns.
		$header = [];
		do {
			$header = fgetcsv($fh, 0, $this->csvSeparator);
		} while (count($header) < 5);

		$result = [];
		foreach ($header as $colnum => $label) {
			foreach (self::CSV_HEADER as $expected) {
				if (strcasecmp(trim($label), $expected) === 0) {
					$result[$expected] = $colnum;
				}
			}
		}
		return $result;
	}
}
