<?php

namespace commands\models\linkimport;

use Titre;
use Yii;
use models\sherpa\Api;

/**
 * Import the Sherpa links.
 *
 * Api::fetchPublication() will store the raw responses of Sherpa into the Sherpa* tables.
 */
class Sherpa extends DefaultImport
{
	/**
	 * @var Api
	 */
	private $api;

	protected function importLinks(): CommandOutput
	{
		$this->initApi();

		$source = $this->findSource('openpolicyfinder');
		$cmd = Yii::app()->db->createCommand(
			"SELECT titreId, GROUP_CONCAT(issn SEPARATOR ',') AS issns FROM Issn WHERE issn IS NOT NULL GROUP BY titreId"
			. ($this->limit ? " LIMIT {$this->limit}" : "")
		);

		$changes = "";
		foreach ($cmd->queryAll() as $row) {
			$titreId = (int) $row['titreId'];
			$issns = explode(',', $row['issns']);
			$changes .= $this->querySherpa($this->getLinkUpdater(), $titreId, $issns);
		}

		$output = new CommandOutput();
		$output->setChanges($changes);
		$output->setSummary($this->getLinkUpdater()->getStats());
		$output->setObsolete($this->getLinksObsolescence($source)->listObsoleteLinks($this->obsoleteSince));
		return $output;
	}

	protected static function getImportName(): string
	{
		return "Open policy finder";
	}

	private function initApi()
	{
		$key = Yii::app()->params->itemAt('sherpa')['api-key'];
		$this->api = new Api($key);
	}

	private function querySherpa(\commands\models\LinkUpdater $updater, int $titreId, array $issns): string
	{
		try {
			$pub = $this->api->fetchPublication($titreId, $issns);
		} catch (\components\curl\NetworkException $e) {
			fprintf(STDERR, "(réseau) {$e->getMessage()}\n");
			$updater->refreshLink($titreId, 0); // httpCode = 0  =>  network error (timeout, etc)
			return "";
		} catch (\Throwable $e) {
			fprintf(STDERR, $e->getMessage() . "\n");
			Yii::log($e->getMessage(), \CLogger::LEVEL_ERROR, 'sherpa');
			return "";
		}
		if ($pub === null) {
			if ($this->verbose >= 2) {
				fprintf(STDERR, "## Titre %d: ISSNs %s => pas d'entrée OPF\n", $titreId, join(",", $issns));
			}
			return "";
		}
		$url = $pub->getRomeoUrl();
		if ($url) {
			$titre = Titre::model()->findByPk($titreId);
			return $updater->updateLink($titre, join(",", $issns), $url);
		}
		return "";
	}
}
