<?php

namespace commands\models\linkimport;

use commands\models\LoadIssns;

class Erih extends DefaultImport
{
	protected function importLinks(): CommandOutput
	{
		$csvFilename = $this->downloadFile("https://kanalregister.hkdir.no/publiseringskanaler/erihplus/periodical/listApprovedAsCsv");
		[$countIssns, $countLines] = LoadIssns::readFromCsv($csvFilename, [1, 2], 0, ";");
		if ($this->verbose >= 2) {
			fprintf(STDERR, "ISSN lus (MàJ) : %d  sur %d lignes de CSV\n", $countIssns, $countLines);
		}

		$source = $this->findSource('erihplus');
		$output = new CommandOutput();
		try {
			$output->setChanges($this->getLinkUpdater()->updateByIssn('https://kanalregister.hkdir.no/publiseringskanaler/erihplus/periodical/info.action?id={{id}}'));
		} catch (\Throwable $t) {
			$output->addError($t->getMessage());
		}
		$output->setSummary($this->getLinkUpdater()->getStats());
		$output->setObsolete($this->getLinksObsolescence($source)->listObsoleteLinks($this->obsoleteSince));
		return $output;
	}

	protected static function getImportName(): string
	{
		return "ERIH+";
	}
}
