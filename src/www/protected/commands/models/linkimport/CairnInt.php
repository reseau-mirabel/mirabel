<?php

namespace commands\models\linkimport;

use Sourcelien;
use Titre;
use commands\models\LoadIssns;

class CairnInt extends DefaultImport
{
	private const COLUMN_ISSNP = 2;

	private const COLUMN_ISSNE = 3;

	public string $csvFile = "";

	protected function initSource(): Sourcelien
	{
		return $this->findSource('cairnint');
	}

	/**
	 * Format en entrée :
	 * - Fichier CSV, avec pour séparateurs des tabulations
	 * - ISSN et ISSNE en colonnes 2 et 3
	 * - Une colonne a pour titre "URL" (i.e. une cellule de la première ligne a exactement ce contenu).
	 */
	protected function importLinks(): CommandOutput
	{
		if (!is_readable($this->csvFile)) {
			throw new \Exception("Fichier inaccessible");
		}

		$titres = $this->findJournals();
		$urls = $this->readUrlsFromCsv();
		$source = $this->initSource();

		// apply the changes
		$changes = "";
		if (!$this->simulation) {
			foreach ($titres as $row) {
				$issn = $row['issn'];
				unset($row['issn']);
				$titre = Titre::model()->populateRecord($row);
				$titre->setConfirm(true);
				if (!empty($urls[$issn])) {
					$changes .= $this->getLinkUpdater()->updateLink($titre, $issn, $urls[$issn]);
				}
			}
		}

		$output = new CommandOutput();
		$output->setChanges($changes);
		$output->setSummary($this->getLinkUpdater()->getStats());
		$output->setObsolete($this->getLinksObsolescence($source)->listObsoleteLinks($this->obsoleteSince));
		return $output;
	}

	protected static function getImportName(): string
	{
		return "Cairn Int";
	}

	/**
	 * Identify journals (Titre) by issn(e|p|l) == issn(e|p).
	 *
	 * @return array Each value has the fields of Titre + an 'issn' field.
	 */
	private function findJournals(): array
	{
		$this->loadIssns();

		$titres = \Yii::app()->db
			->createCommand(<<<EOSQL
				SELECT * FROM (
					SELECT t.*, k.issn AS issn FROM Titre t JOIN Issn i ON (i.titreId = t.id) JOIN knownissn k ON (i.issn = k.issn)
					UNION
					SELECT t.*, k.id AS issn FROM Titre t JOIN Issn i ON (i.titreId = t.id) JOIN knownissn k ON (i.issn = k.id)
					UNION
					SELECT t.*, k.issn AS issn FROM Titre t JOIN Issn i ON (i.titreId = t.id) JOIN knownissn k ON (i.issnl = k.issn)
					UNION
					SELECT t.*, k.id AS issn FROM Titre t JOIN Issn i ON (i.titreId = t.id) JOIN knownissn k ON (i.issnl = k.id)
				) t
				GROUP BY id
				EOSQL
			)
			->queryAll();
		if ($this->verbose >= 2) {
			fprintf(STDERR, "Titres de Mir@bel concernés : %d\n\n", count($titres));
		}
		return $titres;
	}

	/**
	 * Load the ISSN numbers from the CSV into the tmp table `knownissn`.
	 */
	private function loadIssns(): void
	{
		[$countIssns, $countLines] = LoadIssns::readFromCsv(
			$this->csvFile,
			[self::COLUMN_ISSNP, self::COLUMN_ISSNE],
			self::COLUMN_ISSNE,
			"\t"
		);
		if ($this->verbose >= 2) {
			fprintf(STDERR, "ISSN lus (MàJ) : %d  sur %d lignes de CSV\n", $countIssns, $countLines);
		}
	}

	/**
	 * Read the URL associated to each issn
	 *
	 * @return array E.g. {issn1: URL1, ...}
	 */
	private function readUrlsFromCsv(): array
	{
		$urls = [];
		$handle = fopen($this->csvFile, "r");
		$header = fgetcsv($handle, 0, "\t");
		$columnUrl = array_search("URL", $header, true);
		while (($data = fgetcsv($handle, 0, "\t")) !== false) {
			if ($data[self::COLUMN_ISSNP]) {
				$urls[$data[self::COLUMN_ISSNP]] = $data[$columnUrl];
			}
			if ($data[self::COLUMN_ISSNE]) {
				$urls[$data[self::COLUMN_ISSNE]] = $data[$columnUrl];
			}
		}
		fclose($handle);
		return $urls;
	}
}
