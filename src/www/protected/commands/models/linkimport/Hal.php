<?php

namespace commands\models\linkimport;

use components\Curl;
use Titre;
use Yii;

class Hal extends DefaultImport
{
	private const BATCH_SIZE_ISSN = 100;

	private const BATCH_SIZE_ID = 100;

	private ?Curl $curl = null;

	public function setCurl(Curl $c): void
	{
		$this->curl = $c;
	}

	protected function importLinks(): CommandOutput
	{
		$output = new CommandOutput();

		$journalsByIssn = $this->findJournalsByIssn((int) $this->limit);
		if (!$journalsByIssn) {
			$output->addError("Aucune revue n'a été trouvée pour les ISSN de Mirabel.");
			return $output;
		}
		$journals = $this->filterJournalsByNotices($journalsByIssn);
		if (!$journals) {
			$output->addError("Aucune revue avec notice n'a été trouvée pour les ISSN de Mirabel.");
			return $output;
		}
		unset($journalsByIssn);

		$titres = $this->findTitreRecords($journals);
		unset($journals);

		$source = $this->findSource('HAL');
		$changes = "";
		foreach ($titres as $data) {
			$journalIds = $data['docid'];
			sort($journalIds, SORT_NUMERIC);
			$url = sprintf('https://hal.science/search/index/q/*/journalId_i/%s', join("%20OR%20", $journalIds));
			$changes .= $this->getLinkUpdater()->updateLink($data['titre'], $data['issns'][0], $url);
		}

		$output->setChanges($changes);
		$stats = $this->getLinkUpdater()->getStats();
		if ($this->verbose >= 1) {
			$stats .= sprintf("Titres de Mirabel dans HAL : %d\n", count($titres));
		}
		$output->setSummary($stats);
		$output->setObsolete($this->getLinksObsolescence($source)->listObsoleteLinks($this->obsoleteSince));
		return $output;
	}

	protected static function getImportName(): string
	{
		return "HAL";
	}

	protected function findTitreRecords(array $journals): array
	{
		$result = [];
		foreach ($journals as $docid => $data) {
			$issns = $data['issns'];
			$notices = (int) $data['notices'];
			$titre = Titre::model()
				->findBySql("SELECT t.* FROM Issn i JOIN Titre t ON t.id = i.titreId WHERE i.issn IN ('" . join("','", $issns) . "') LIMIT 1");
			$titre->confirm = true;
			$titre->suivre = null;
			if (isset($result[$titre->id])) {
				$result[$titre->id]['docid'][] = (int) $docid;
				$result[$titre->id]['notices'] += $notices;
				if ($this->verbose > 0) {
					$previous = $result[$titre->id];
					echo "Doublon pour titreId {$titre->id} : docid/notices $docid/$notices ajouté à {$previous['docid'][0]}/{$previous['notices']}\n";
				}
				continue;
			}
			$result[$titre->id] = [
				'titre' => $titre,
				'docid' => [(int) $docid],
				'issns' => $issns,
				'notices' => $notices,
			];
		}
		return $result;
	}

	/**
	 * Query the HAL API to find the docid of each journal that has an ISSN in Mirabel.
	 *
	 * @return array [ docid => [ISSNP, ISSNE] ]
	 */
	protected function findJournalsByIssn(int $limit): array
	{
		$journals = [];
		foreach (['electronique', 'papier'] as $support) {
			$issns = Yii::app()->db
				->createCommand("SELECT issn FROM Issn WHERE issn IS NOT NULL ORDER BY titreId, id")
				->queryColumn();
			$chunks = array_chunk($issns, self::BATCH_SIZE_ISSN);
			$curl = $this->getCurl();
			foreach ($chunks as $chunk) {
				$field = ($support === 'papier' ? 'issn_s' : 'eissn_s');
				$url = "https://api.archives-ouvertes.fr/ref/journal/?rows=200&fl=docid,issn_s,eissn_s&q="
					. rawurlencode($field . ':("' . join('" OR "', $chunk) . '")');
				try {
					$response = json_decode($curl->get($url)->getContent());
				} catch (\Exception $e) {
					fprintf(STDERR, "%s  %s\n", $url, $e->getMessage());
					sleep(1);
					$response = json_decode($curl->get($url)->getContent());
				}
				if (empty($response->response->docs)) {
					continue;
				}
				// cf "https://api.archives-ouvertes.fr/ref/journal/?rows=200&fl=docid,issn_s,eissn_s&q=eissn_s%3A%28%221760-7558%22%20OR%20%221782-138X%22%20OR%20%221963-1707%22%29"
				foreach ($response->response->docs as $doc) {
					$issns = [];
					if (isset($doc->issn_s)) {
						$issns[] = $doc->issn_s;
					}
					if (isset($doc->eissn_s)) {
						$issns[] = $doc->eissn_s;
					}
					$journals[$doc->docid] = $issns;
				}
				if ($limit && count($journals) > $limit) {
					break;
				}
			}
		}
		return $journals;
	}

	/**
	 * Query the HAL API to filter out the journals that have 0 notice.
	 *
	 * @return array [ docid => [ISSNP, ISSNE] ]
	 */
	protected function filterJournalsByNotices(array $journals): array
	{
		$result = [];
		$batchSize = self::BATCH_SIZE_ID;
		$chunks = array_chunk($journals, $batchSize, true);
		$curl = $this->getCurl();
		foreach ($chunks as $chunk) {
			$url = "https://api.archives-ouvertes.fr/search/?wt=json&rows=0&facet=true&facet.field=journalId_i&facet.mincount=1&facet.limit=$batchSize&q="
				. rawurlencode('journalId_i:(' . join(' OR ', array_keys($chunk)) . ')');
			try {
				$response = json_decode($curl->get($url)->getContent());
			} catch (\Exception $e) {
				fprintf(STDERR, "%s  %s\n", $url, $e->getMessage());
				sleep(1);
				$response = json_decode($curl->get($url)->getContent());
			}
			if (empty($response->facet_counts->facet_fields->journalId_i)) {
				continue;
			}
			// cf "https://api.archives-ouvertes.fr/search/?wt=json&rows=0&facet=true&facet.field=journalId_i&facet.mincount=1&facet.limit=30&q=journalId_i:(97924%20OR%2018707%20OR%2054661)"
			for ($i = 0; $i < count($response->facet_counts->facet_fields->journalId_i); $i += 2) {
				$docid = $response->facet_counts->facet_fields->journalId_i[$i];
				$notices = $response->facet_counts->facet_fields->journalId_i[$i+1];
				if ($this->verbose > 1) {
					echo "$docid: $notices notices\n";
				}
				if (isset($result[$docid]) && $result[$docid]['notices'] > $notices) {
					if ($this->verbose > 0) {
						echo "Duplicate docid $docid with notices $notices < {$result[$docid]['notices']}\n";
					}
					continue;
				}
				$result[$docid] = [
					'issns' => $journals[$docid],
					'notices' => $notices,
				];
			}
		}
		return $result;
	}

	private function getCurl(): Curl
	{
		if ($this->curl === null) {
			$this->curl = new Curl();
			$this->curl->setopt(CURLOPT_TIMEOUT, 10);
		}
		return $this->curl;
	}
}
