<?php

namespace commands\models\linkimport;

use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use Box\Spout\Reader\XLSX\Sheet;
use commands\models\IssnTempTable;

class Scopus extends DefaultImport
{
	/**
	 * local key => text of the header cell in the XLSX file
	 */
	private const HEADER = [
		'ISSNE' => 'EISSN',
		'ISSN' => 'ISSN',
		'SOURCEID' => 'Sourcerecord ID',
		'ACTIVE' => 'Active or Inactive',
	];

	public string $xlsx = "";

	protected function getUrl(): string
	{
		if (strncmp($this->xlsx, 'http', 4) === 0) {
			return $this->xlsx;
		}
		$html = file_get_contents("https://www.elsevier.com/solutions/scopus/how-scopus-works/content");
		$m = [];
		if (preg_match('#href="//(downloads.ctfassets.net/[\w/]+/ext_list\w+\.xlsx)"#', $html, $m)) {
			return "https://{$m[1]}";
		}
		throw new \Exception("L'URL vers le fichier Excel n'a pas été détectée dans la page HTML de Scopus.");
	}

	protected function importLinks(): CommandOutput
	{
		if ($this->xlsx && strncmp($this->xlsx, 'http', 4) !== 0) {
			$xlsxFile = $this->xlsx;
		} else {
			$xlsxFile = $this->downloadFile($this->getUrl());
		}
		$sheet = self::openFirstSheet($xlsxFile);

		$columns = self::findColumns($sheet);
		if (count($columns) !== count(self::HEADER)) {
			throw new \Exception("L'entête du fichier CSV d'EZB ne contient pas de colonne " . join(" ou ", self::HEADER));
		}

		$issnTempTable = new IssnTempTable();
		[$countIssns, $countLines] = self::loadIssnTable($issnTempTable, $sheet, $columns);
		if ($this->verbose >= 2) {
			fprintf(STDERR, "ISSN lus (MàJ) : %d  sur %d lignes\n", $countIssns, $countLines);
		}

		$source = $this->findSource('scopus');
		$source->nbimport = $countLines;

		$output = new CommandOutput();
		try {
			$output->setChanges($this->getLinkUpdater()->updateByIssn("https://www.scopus.com/sourceid/{{id}}"));
		} catch (\Throwable $t) {
			$output->addError($t->getMessage());
		}
		$output->setSummary($this->getLinkUpdater()->getStats());

		$output->setObsolete($this->getLinksObsolescence($source)->listObsoleteLinks($this->obsoleteSince));
		$output->setNeverImported($this->listNeverImported($source));
		$source->save(false, ['nbimport']);
		return $output;
	}

	protected static function getImportName(): string
	{
		return "Scopus";
	}

	/**
	 * @return array<"ISSNE"|"ISSN"|"SOURCEID"|"ACTIVE", int>
	 */
	private static function findColumns(Sheet $sheet): array
	{
		foreach ($sheet->getRowIterator() as $row) {
			$result = [];
			foreach ($row->toArray() as $colnum => $label) {
				foreach (self::HEADER as $k => $expected) {
					if (strcasecmp(trim($label), $expected) === 0) {
						$result[$k] = $colnum;
					}
				}
			}
			return $result;
		}
		return [];
	}

	/**
	 * @return array{int, int} (#ISSN, #rows_with_issn)
	 */
	private function loadIssnTable(IssnTempTable $issnTempTable, Sheet $sheet, array $columns): array
	{
		$countLines = 0;
		foreach ($sheet->getRowIterator() as $row) {
			/** @var \Box\Spout\Common\Entity\Row $row */
			$row20 = array_slice($row->toArray(), 0, 20);
			$data = [];
			foreach ($columns as $k => $v) {
				$data[$k] = trim($row20[$v] ?? '');
			}
			if (!$data['SOURCEID']) {
				break; // no ID
			}
			if (!ctype_digit($data['SOURCEID'])) {
				continue; // header?
			}
			if ($data['ACTIVE'] !== 'Active') {
				continue;
			}
			if (!$data['ISSNE'] && !$data['ISSN']) {
				continue; // no ISSN
			}
			$countLines++;
			if (strlen($data['ISSNE']) === 8) {
				$issnTempTable->insertIssn($data['SOURCEID'], substr($data['ISSNE'], 0, 4) . "-" . substr($data['ISSNE'], 4));
			}
			if (strlen($data['ISSN']) === 8) {
				$issnTempTable->insertIssn($data['SOURCEID'], substr($data['ISSN'], 0, 4) . "-" . substr($data['ISSN'], 4));
			}
		}
		$issnTempTable->close();
		return [$issnTempTable->getCount(), $countLines];
	}

	private static function openFirstSheet(string $path): Sheet
	{
		$reader = ReaderEntityFactory::createXLSXReader();
		$reader->setShouldPreserveEmptyRows(true);
		$reader->open($path);
		foreach ($reader->getSheetIterator() as $s) {
			return $s;
		}
		throw new \Exception("No worksheet found in the XLSX file");
	}
}
