<?php

namespace commands\models\linkimport;

use Titre;
use commands\models\LoadIssns;

class Road extends DefaultImport
{
	private const IMPORT_URL = "ftp://ROAD:open_access@ftp.issn.org/ROAD.xml.gz";

	private const URL_PATTERN = 'http://road.issn.org/issn/%s';

	public static function getLastDownloadDate(): string
	{
		$store = self::initStore();
		$inStorePath = $store->get("ROAD.xml.gz");
		if (!$inStorePath) {
			return "";
		}
		return date("Y-m-d H:i", stat($inStorePath)['mtime']);
	}

	protected function importLinks(): CommandOutput
	{
		$filename = $this->downloadFile(self::IMPORT_URL, "ROAD.xml.gz");

		$countIssns = LoadIssns::readFromMarcxml($filename);
		if ($this->verbose >= 2) {
			fprintf(STDERR, "ISSN lus : %d\n", $countIssns);
		}

		$source = $this->findSource('ROAD');
		$titres = \Yii::app()->db
			->createCommand(
				<<<EOSQL
				SELECT * FROM (
					-- by ISSN
					SELECT t.*, k.id AS issn
						FROM Titre t JOIN Issn i ON i.titreId = t.id JOIN knownissn k ON i.issn = k.issn
					UNION
					-- by ISSN-L
					SELECT t.*, k.id AS issn
						FROM Titre t JOIN Issn i ON i.titreId = t.id JOIN knownissn k ON i.issnl = k.issn
				) t
				GROUP BY id
				EOSQL
			)->queryAll();
		if ($this->verbose >= 2) {
			fprintf(STDERR, "Titres de Mir@bel concernés : %d\n\n", count($titres));
		}

		$output = new CommandOutput();
		$output->setChanges($this->applyChanges($titres));
		$output->setSummary($this->getLinkUpdater()->getStats());
		$output->setObsolete($this->getLinksObsolescence($source)->listObsoleteLinks($this->obsoleteSince));
		return $output;
	}

	protected static function getImportName(): string
	{
		return "ROAD";
	}

	private function applyChanges(array $titres): string
	{
		$changes = "";
		foreach ($titres as $row) {
			$issn = $row['issn'];
			unset($row['issn']);
			$titre = Titre::model()->populateRecord($row);
			$titre->setConfirm(true);
			$url = sprintf(self::URL_PATTERN, $issn);
			$changes .= $this->getLinkUpdater()->updateLink($titre, $issn, $url);
		}
		return $changes;
	}
}
