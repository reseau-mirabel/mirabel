<?php

namespace commands\models\linkimport;

use commands\models\LinksObsolescence;
use models\wikidata\Import;
use models\wikidata\Synchro;
use Yii;

class Wikipedia extends DefaultImport
{
	protected function importLinks(): CommandOutput
	{
		if (!$this->synchro()) {
			throw new \Exception("Erreur lors de la synchro Wikidata.");
		}

		$wImport = new Import($this->verbose, (int) $this->simulation, $this->limit);
		$changes = $wImport->importWplinks();
		$output = new CommandOutput();
		$output->setChanges($changes);
		$output->setSummary($wImport->getLinkUpdater()->getStats());

		$obsolete = "";
		if ($this->obsoleteSince) {
			// 3. obsolete links
			$sourceswp = \Sourcelien::model()->findAll("nomcourt LIKE 'wikipedia-%'");
			foreach ($sourceswp as $source) {
				$obsolete .= (new LinksObsolescence($source))->listObsoleteLinks($this->obsoleteSince);
			}
		}
		$output->setObsolete($obsolete);

		return $output;
	}

	protected static function getImportName(): string
	{
		return "Wikipedia";
	}

	/**
	 * If the properties were not fetched in the last hour, synchronize from wikidata to Mirabel.
	 */
	private function synchro(): bool
	{
		$oldest = (int) Yii::app()->db
			->createCommand("SELECT MIN(hdate) FROM Wikidata WHERE property LIKE 'WP:%'")
			->queryScalar();
		$secondsElapsed = time() - $oldest;
		if ($secondsElapsed < 3600) {
			return true;
		}
		$job = new Synchro();
		$job->verbose = 0;
		return $job->run((bool) $this->simulation, 0);
	}
}
