<?php

namespace commands\models;

use processes\service\ExportServices;
use processes\service\ServiceFilter;

class BaconUpload
{
	private $dirUrl;

	private $username;

	private $password;

	public function __construct($dirUrl, $username, $password)
	{
		$this->dirUrl = rtrim($dirUrl, '/');
		$this->username = $username;
		$this->password = $password;
	}

	public function run()
	{
		$filenames = [
			ServiceFilter::BACON_DOAJ => "MIRABEL_GLOBAL_DOAJ-PARTIEL_%s.txt",
			ServiceFilter::BACON_EPISCIENCES => "EPISCIENCES_GLOBAL_ALLJOURNALS_%s.txt",
			ServiceFilter::BACON_GLOBAL => "MIRABEL_GLOBAL_TEXTEINTEGRAL_%s.txt",
			ServiceFilter::BACON_LIBRE => "MIRABEL_GLOBAL_LIBRESACCES_%s.txt",
			ServiceFilter::BACON_REPERES => "REPERES_GLOBAL_LIBRESACCES_%s.txt",
		];
		foreach ($filenames as $baconMode => $pattern) {
			$fh = self::createTempFile();
			$bytes = self::writeKbartContent($fh, $baconMode);
			$filename = sprintf($pattern, date('Y-m-d'));
			$this->upload($fh, $bytes, $filename);
			fclose($fh);
		}
	}

	/**
	 * @param resource $fh
	 * @param int $size file content size
	 * @param string $filename
	 */
	private function upload($fh, int $size, string $filename): void
	{
		fseek($fh, 0);
		$curl = curl_init();
		curl_setopt_array($curl, [
			// config for an upload
			CURLOPT_PUT => true,
			CURLOPT_UPLOAD => true,
			// destination (URL to a file, not a directory)
			CURLOPT_URL => "{$this->dirUrl}/{$filename}",
			// content
			CURLOPT_INFILE => $fh,
			CURLOPT_INFILESIZE => $size,
			// auth
			CURLOPT_USERNAME => $this->username,
			CURLOPT_PASSWORD => $this->password,
			// 10 minutes max to download the file
			CURLOPT_TIMEOUT => 600,
			// tmp
			CURLOPT_RETURNTRANSFER => true,
		]);
		curl_exec($curl);
		if (curl_errno($curl) !== 0) {
			echo "Erreur en déposant le fichier KBART dans le webdav de Bacon\n";
			echo curl_error($curl);
		}
		curl_close($curl);
	}

	/**
	 * Return a file handler for a temporary file of up to 1Mb.
	 *
	 * @return resource
	 */
	private static function createTempFile()
	{
		$r = fopen('php://temp/maxmemory:1024000', 'w');
		if ($r === false) {
			throw new \Exception("Memory allocation failed (1MB).");
		}
		return $r;
	}

	/**
	 * @param resource $fh
	 * @return int bytes written
	 */
	private static function writeKbartContent($fh, string $baconMode): int
	{
		// criteria on Service
		$filter = new ServiceFilter();
		$filter->setScenario('filter');
		$filter->bacon = $baconMode;

		$export = new ExportServices([], $filter);
		return $export->exportToKbartV2($fh, false);
	}
}
