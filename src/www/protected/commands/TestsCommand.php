<?php

class TestsCommand extends CConsoleCommand
{
	public function actionReset(string $interactive = '1'): int
	{
		if (!self::checkDb()) {
			echo "Abandon.\n";
			return 1;
		}

		$isInteractive = (bool) $interactive;
		if ($isInteractive) {
			if (!$this->confirm("Réinitialiser la base de données ?", false)) {
				return 1;
			}
		}

		self::resetDb();
		return 0; // OK
	}

	private static function checkDb(): bool
	{
		if (!defined('YII_TARGET') || YII_TARGET !== 'test') {
			echo "This command can only run from yii_test.\n";
			return false;
		}

		$dbConnection = Yii::app()->db->connectionString;
		$m = [];
		if (preg_match('/\bdbname=(.+?)(;|$)/', $dbConnection, $m)) {
			$dbname = $m[1];
		}
		if (empty($dbname)) {
			echo "Le nom de la base de données n'a pu être lu...\n";
			return false;
		}
		if (strpos($dbname, '_test') === false) {
			echo "Le nom de la base de données ne contient pas '_test'...\n";
			return false;
		}
		return true;
	}

	private static function resetDb(): void
	{
		$hostname = 'localhost';
		$m = [];
		if (preg_match('/\bhost=(.+?)(;|$)/', Yii::app()->db->connectionString, $m)) {
			$hostname = escapeshellarg($m[1]);
		}
		$username = escapeshellarg(Yii::app()->db->username);
		$password = escapeshellarg(Yii::app()->db->password);
		if (preg_match('/\bdbname=(.+?)(;|$)/', Yii::app()->db->connectionString, $m)) {
			$dbname = escapeshellarg($m[1]);
		} else {
			throw new \Exception("DB name not found.");
		}
		system("mysql -h $hostname -u $username -p$password $dbname < tests/_data/mirabel2_tests.sql");
	}
}
