<?php

use \processes\kbart\noninteractive;

class KbartCommand extends \CConsoleCommand
{
	public function actionImport(
		$file,
		$ressourceId,
		$collectionIds = '',
		$acces = '',
		$contenu = '',
		$overrideDatesWithEmbargo = '0',
		$lacunaire = '0',
		$selection = '0'
	) {
		$config = new noninteractive\Config();
		$config->setRessourceAndCollections((int) $ressourceId, explode(',', $collectionIds));
		$config->setDefaultAccess($acces);
		$config->setDefaultCoveragedepth($contenu);
		$config->overrideDatesWithEmbargo = (bool) $overrideDatesWithEmbargo;
		$config->lacunaire = (bool) $lacunaire;
		$config->selection = (bool) $selection;
		$config->verbose = 2; // Max verbosity

		$importAuto = new noninteractive\ImportAll($config);
		$importAuto->store($file);
		$importer = $importAuto->createImporter();
		$stats = $importAuto->write($importer);

		$writer = new noninteractive\WritingStatsRenderer();
		echo $writer->getSummary($stats) . "\n\n";
		echo $writer->renderAsText($stats, $config);
	}
}
