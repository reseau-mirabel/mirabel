<?php

class RessourceCommand extends CConsoleCommand
{
	/**
	 * Rafraîchit les logos quand les images d'origine (déjà téléchargées) sont plus récentes que les images de M.
	 */
	public function actionLogos()
	{
		$imageTool = new processes\images\Resizer(
			finalPath: DATA_DIR . '/public/images/ressources',
			rawPath: DATA_DIR . '/images/ressources'
		);
		$imageTool->filePattern = '%06d';
		$imageTool->boundingbox = '450x110>';
		$imageTool->resizeAll();
	}
}
