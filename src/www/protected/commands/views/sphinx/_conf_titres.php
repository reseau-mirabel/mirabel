<?php

use commands\models\SphinxConf as S;

/** @var array<string, string> $db */
/** @var array<string, string> $sphinx */

?>
source <?= $sphinx['tablePrefix'] ?>titres
{
	<?= S::render("sql_init_session.php", ['db' => $db]) ?>

	sql_joined_field = variantes from query; \
		SELECT titreId, variante FROM TitreVariante ORDER BY titreId, variante

	sql_attr_uint = revueid
	sql_attr_uint = lettre1
	sql_attr_uint = nbacces
	sql_attr_uint = cletri

	sql_attr_bool = obsolete
	sql_attr_bool = vivant

	sql_attr_timestamp = hdatemodif
	sql_attr_timestamp = hdateverif

	sql_attr_multi = uint paysid from query; <?= S::format(<<<SQL
		SELECT te.titreId, e.paysId
		FROM Titre_Editeur te
			JOIN Editeur e ON te.editeurId = e.id AND te.ancien = 0
		ORDER BY te.titreId ASC
		SQL) ?>
	sql_attr_multi = uint issnpaysid from query;\
		SELECT i.titreId, p.id AS issnpaysid \
		FROM Issn i LEFT JOIN Pays p ON i.pays = p.code2 WHERE i.pays != '' AND i.statut = 0 ORDER BY titreId ASC
	sql_attr_multi = uint editeurid from query;\
		SELECT titreId, editeurId FROM Titre_Editeur ORDER BY titreId ASC
	sql_attr_multi = uint editeuractuelid from query;\
		SELECT titreId, editeurId FROM Titre_Editeur WHERE ancien = 0 ORDER BY titreId ASC
	sql_attr_multi = uint ressourceid from query; \
		SELECT titreId, ressourceId FROM Service ORDER BY titreId ASC
	sql_attr_multi = uint collectionid from query; \
		SELECT s.titreId, sc.collectionId FROM Service s JOIN Service_Collection sc ON sc.serviceId = s.id ORDER BY s.titreId ASC
	sql_attr_multi = uint suivi from query; \
		SELECT t.id, s.partenaireId FROM Suivi s JOIN Titre t ON (s.cible = 'Revue' AND s.cibleId = t.revueId) ORDER BY t.id ASC
	sql_attr_multi = uint detenu from query; \
		SELECT titreId, partenaireId FROM Partenaire_Titre ORDER BY titreId ASC
	sql_attr_multi = uint acces from query;  <?= S::format(<<<SQL
		SELECT DISTINCT titreId,
			CASE type WHEN 'Integral' THEN 2 WHEN 'Resume' THEN 3 WHEN 'Sommaire' THEN 4 WHEN 'Indexation' THEN 5 ELSE 1 END AS type
		FROM Service ORDER BY titreId ASC
		SQL) ?>
	sql_attr_multi = uint acceslibre from query; <?= S::format(<<<SQL
		SELECT DISTINCT titreId,
			CASE type WHEN 'Integral' THEN 2 WHEN 'Resume' THEN 3 WHEN 'Sommaire' THEN 4 WHEN 'Indexation' THEN 5 ELSE 1 END AS type
		FROM Service WHERE acces = 'libre' ORDER BY titreId ASC
		SQL) ?>
	sql_attr_multi = bigint issn from query;  <?= S::format(<<<SQL
		(SELECT titreId, CONVERT(LEFT(REPLACE(issn , '-', ''), 7), UNSIGNED) FROM Issn WHERE issn != '')
		UNION
		(SELECT titreId, CONVERT(LEFT(REPLACE(issnl , '-', ''), 7), UNSIGNED) FROM Issn WHERE issnl != '')
		ORDER BY titreId ASC
		SQL) ?>
	# categorie : pour chaque titre, categorieId associé soit via la revue soit par un alias
	sql_attr_multi = bigint categorie from query; <?= S::format(<<<SQL
		SELECT DISTINCT * FROM (
			SELECT t.id, IFNULL(cr.categorieId, ca.categorieId) AS categorieId FROM Titre t
			LEFT JOIN Categorie_Revue cr ON t.revueId = cr.revueId
			LEFT JOIN CategorieAlias_Titre ct ON ct.titreId = t.id LEFT JOIN CategorieAlias ca ON ct.categorieAliasId = ca.id
		) t WHERE categorieId IS NOT NULL ORDER BY t.id, t.categorieId
		SQL) ?>
	# revuecategorie : pour chaque titre, categorieId associé soit via la revue soit par un alias sur un des titres de la revue
	sql_attr_multi = bigint revuecategorie from query;  <?= S::format(<<<SQL
		SELECT DISTINCT * FROM (
			SELECT t.id, IFNULL(cr.categorieId, ca.categorieId) AS categorieId FROM Titre t
			LEFT JOIN Categorie_Revue cr ON t.revueId = cr.revueId
			JOIN Titre t2 ON t.revueId = t2.revueId LEFT JOIN CategorieAlias_Titre ct ON ct.titreId = t2.id LEFT JOIN CategorieAlias ca ON ct.categorieAliasId = ca.id
		) t WHERE categorieId IS NOT NULL ORDER BY t.id, t.categorieId
		SQL) ?>
	# revuecategorierec : idem revuecategorie, mais récursif (le thème et son éventuel parent)
	sql_attr_multi = bigint revuecategorierec from query;  <?= S::format(<<<SQL
		WITH
			RevueCategorieDirect AS (
				SELECT t.id, IFNULL(cr.categorieId, ca.categorieId) AS categorieId
				FROM Titre t
					LEFT JOIN Categorie_Revue cr ON t.revueId = cr.revueId
					JOIN Titre t2 ON t.revueId = t2.revueId
					LEFT JOIN CategorieAlias_Titre ct ON ct.titreId = t2.id
					LEFT JOIN CategorieAlias ca ON ct.categorieAliasId = ca.id
			)
		SELECT t.id, t.categorieId FROM RevueCategorieDirect t
		UNION
		SELECT t.id, c.parentId FROM RevueCategorieDirect t JOIN Categorie c ON t.categorieId = c.id AND c.profondeur = 3
		ORDER BY id
		SQL) ?>
	# abonnement : liste de partenaireId abonnés à une ressource ou une collections des accès de ce titre
	sql_attr_multi = uint abonnement from query;  <?= S::format(<<<SQL
		SELECT * FROM (
			SELECT s.titreId, a.partenaireId
				FROM Abonnement a
				JOIN Service s ON s.ressourceId = a.ressourceId
				WHERE a.collectionId IS NULL AND a.partenaireId IS NOT NULL
			UNION
			SELECT s.titreId, a.partenaireId
				FROM Abonnement a
				JOIN Service_Collection sc ON sc.collectionId = a.collectionId
				JOIN Service s ON sc.serviceId = s.id
				WHERE a.partenaireId IS NOT NULL
		) t GROUP BY titreId, partenaireId
		SQL) ?>
	# lien : liste de sourceId associés à ce titre
	sql_attr_multi = uint lien from query;  <?= S::format(<<<SQL
		SELECT DISTINCT titreId, sourceId FROM LienTitre WHERE sourceId IS NOT NULL ORDER BY titreId
		SQL) ?>
	sql_attr_multi = uint attribut from query;  <?= S::format(<<<SQL
		SELECT titreId, sourceattributId FROM AttributTitre ORDER BY titreId
		SQL) ?>
	sql_attr_multi = uint grappe from query;  <?= S::format(<<<SQL
		SELECT titreId, grappeId FROM Grappe_Titre GROUP BY titreId, grappeId
		SQL) ?>
	# Map the 3-letter lang code to an integer
	sql_attr_multi = uint langue from query;  <?= S::format(<<<SQL
		SELECT * FROM (
			SELECT t.id, conv(trim(BOTH '"' FROM json_extract(t.langues, '$[0]')), 36, 10) AS l FROM Titre t
			UNION ALL
			SELECT t.id, conv(trim(BOTH '"' FROM json_extract(t.langues, '$[1]')), 36, 10) FROM Titre t WHERE length(t.langues) > 7
			UNION ALL
			SELECT t.id, conv(trim(BOTH '"' FROM json_extract(t.langues, '$[2]')), 36, 10) FROM Titre t WHERE length(t.langues) > 12
		) t WHERE l IS NOT NULL ORDER BY t.id
		SQL) ?>
		# With MariaDB >= 10.6
		# SELECT t.id, conv(l.lang, 36, 10) FROM Titre t JOIN json_table(t.langues, '$[*]' columns(lang varchar(10) path '$')) AS l ORDER BY t.id

	sql_attr_string = titrecomplet
}

source <?= $sphinx['tablePrefix'] ?>titres_full : <?= $sphinx['tablePrefix'] ?>titres
{
	# Inherits from the "titres" source and overwrites some commands.

	sql_query_pre = SET @indexTime = UNIX_TIMESTAMP()
	sql_query_post = DELETE FROM IndexingRequiredTitre WHERE updated < @indexTime

	<?= S::render("sql_query_titres.php", ['delta' => false]) ?>
}
table <?= $sphinx['tablePrefix'] ?>titres_full
{
	source = <?= $sphinx['tablePrefix'] ?>titres_full
	path   = indexes/<?= $sphinx['tablePrefix'] ?>titres_full
	charset_table    = <?php require(__DIR__ . '/charsetTable.txt'); echo "\n"; ?>
	morphology       = libstemmer_fr
	min_stemming_len = 2
	stopwords        = stopwords.txt
	min_word_len   = 1
	min_prefix_len = 3
	html_strip = 0
	index_exact_words = 1
	expand_keywords   = 1
}

source <?= $sphinx['tablePrefix'] ?>titres_delta : <?= $sphinx['tablePrefix'] ?>titres
{
	# Inherits from the "titres" source and overwrites some commands.
	<?= S::render("sql_query_titres.php", ['delta' => true]) ?>
}
table <?= $sphinx['tablePrefix'] ?>titres_delta : <?= $sphinx['tablePrefix'] ?>titres_full
{
	source = <?= $sphinx['tablePrefix'] ?>titres_delta
	path   = indexes/<?= $sphinx['tablePrefix'] ?>titres_delta
	killlist_target = <?= $sphinx['tablePrefix'] ?>titres_full:id
}

table <?= $sphinx['tablePrefix'] ?>titres
{
    type = distributed
    local = <?= $sphinx['tablePrefix'] ?>titres_full
    local = <?= $sphinx['tablePrefix'] ?>titres_delta
}
