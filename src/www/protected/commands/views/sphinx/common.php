<?php
/** @var array $sphinx */
?>

indexer
{
	mem_limit = 64M
}
searchd
{
	listen = <?php
	if (isset($sphinx['unix_socket'])) {
		echo $sphinx['unix_socket'];
	} elseif (isset($sphinx['port'])) { 
		echo $sphinx['port'];
	} else {
		echo "/tmp/sphinx-mirabel-mysql.socket";
	}
	?>:mysql41

	log         = log/searchd.log
	binlog_path =
	#query_log  = log/query.log
	pid_file    = run/searchd.pid

	client_timeout  = 300
	seamless_rotate = 1
	unlink_old      = 1
	max_packet_size	 = 8M
	max_filters       = 256
	max_filter_values = 4096
	max_batch_queries = 32

	collation_server      = libc_ci
	collation_libc_locale = fr_FR.utf8
}
