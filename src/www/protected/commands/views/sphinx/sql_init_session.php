<?php
/** @var array<string, string> $db */
?>
type      = mysql
sql_host  = <?= $db['host'] . "\n" ?>
sql_user  = <?= $db['user'] . "\n" ?>
sql_pass  = <?= $db['pass'] . "\n" ?>
sql_db    = <?= $db['dbname'] . "\n" ?>

sql_query_pre = SET NAMES utf8mb4
sql_query_pre = SET SESSION query_cache_type=OFF
sql_query_pre = SET SESSION max_heap_table_size=268435456
sql_query_pre = SET SESSION tmp_table_size=268435456
