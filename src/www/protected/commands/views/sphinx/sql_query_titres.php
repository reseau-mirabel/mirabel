<?php

/**
 * The main SQL query that collects data for the Manticore table named "titres".
 *
 * The query exists in 2 modes:
 * - normal (all the records),
 * - delta (only the modified records).
 *
 * In normal mode, ranged queries are used, in order to split the data into smaller chunks when querying MariaDB.
 * See https://manual.manticoresearch.com/Data_creation_and_modification/Adding_data_from_external_storages/Fetching_from_databases/Ranged_queries#Ranged-queries
 */

use commands\models\SphinxConf as S;

/** @var bool $delta */

if ($delta) {
	$deltaSql = "JOIN IndexingRequiredTitre irt ON irt.id = t.id AND irt.deleted = 0";
	?>

# Ranged queries are useless in a delta index.
sql_query_range =

	<?php
} else {
	$deltaSql = 'WHERE t.revueId BETWEEN $start AND $end'; // $start and $end are NOT variables in PHP.
	?>

# Setting this option enables ranged queries,
# i.e. SQL fetch queries must use variables $start and $end
sql_query_range = SELECT MIN(revueId), MAX(revueId) FROM Titre
sql_range_step = 5000

<?php } ?>


sql_query = <?= S::format(str_replace('{{deltaSql}}', $deltaSql, <<<SQL
	WITH ServicesByRevue AS (
		SELECT t2.revueId, count(s.id) AS nbacces
		FROM Titre t2 LEFT JOIN Service s ON s.titreId = t2.id
		GROUP BY t2.revueId
	),
	TitreSorted AS (
		SELECT id, RANK() OVER (ORDER BY REPLACE(REPLACE(titre, '[', ''), '(', '')) AS rank
		FROM Titre
	)
	SELECT
		t.id,
		t.sigle,
		CONCAT(t.prefixe,t.titre, IF(t.sigle<>'', CONCAT(' — ', t.sigle), '')) as titrecomplet,
		REGEXP_REPLACE(CONCAT(t.prefixe,t.titre, IF(t.sigle<>'', CONCAT(' — ', t.sigle), '')) COLLATE utf8mb4_bin, '([A-Z])\\.\\s?(?=[A-Z]\\b)', '\\1') AS titrerecherche,
		FIND_IN_SET(LEFT(REPLACE(t.titre, '[', ''), 1), 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z') AS lettre1,
		t.revueId AS revueid,
		TitreSorted.rank AS cletri,
		(t.obsoletePar IS NOT NULL) AS obsolete,
		GREATEST(t.hdateModif, MAX(t.hdateModif)) AS hdatemodif,
		IFNULL(r.hdateVerif, 0) AS hdateverif,
		sr.nbacces,
		(t.dateFin = '') AS vivant
	FROM Titre t
		JOIN Revue r ON t.revueId = r.id
		JOIN ServicesByRevue sr ON sr.revueId = t.revueId
		JOIN TitreSorted ON t.id = TitreSorted.id
	{{deltaSql}}
	GROUP BY t.id
	ORDER BY REPLACE(REPLACE(t.titre, '[', ''), '(', '')
	SQL
)) ?>
