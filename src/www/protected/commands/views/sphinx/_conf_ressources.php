<?php

use commands\models\SphinxConf as S;

/** @var array<string, string> $db */
/** @var array<string, string> $sphinx */

?>

source <?= $sphinx['tablePrefix'] ?>ressources
{
	<?= S::render("sql_init_session.php", ['db' => $db]) ?>

	sql_query_pre = <?= S::format(<<<SQL
		CREATE TEMPORARY TABLE RessourceTmp
		(
			cletri INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			id INT(11) UNSIGNED NOT NULL,
			nbrevues SMALLINT UNSIGNED NOT NULL
		) ENGINE=InnoDB DEFAULT CHARACTER SET utf8mb4 ENGINE=MEMORY
		SELECT NULL AS cletri, r.id AS id, COUNT(DISTINCT t.revueId) AS nbrevues
		FROM Ressource r
			LEFT JOIN Service s ON s.ressourceId = r.id
			LEFT JOIN Titre t ON s.titreId = t.id
		GROUP BY r.id ORDER BY r.nom
		SQL) ?>
	sql_query_pre = ALTER TABLE RessourceTmp ADD INDEX i_tmp (id)
	sql_ranged_throttle	= 0

	sql_query = <?= S::format(<<<SQL
		SELECT
			r.id,
			r.sigle,
			CONCAT(r.prefixe, r.nom, IF(r.sigle<>'', CONCAT(' — ', r.sigle), '')) as nomcomplet,
			r.description, r.noteContenu, r.disciplines,
			FIND_IN_SET(LEFT(r.nom, 1), 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z') AS lettre1,
			cletri,
			r.autoImport AS autoimport,
			SUM(c.importee = 1) > 0 AS autoimportcollections,
			(r.exhaustif = 1) AS exhaustif,
			SUM(c.exhaustif = 1) > 0 AS exhaustifcollections,
			(r.type = 'SiteWeb') AS web,
			r.hdateModif AS hdatemodif,
			r.hdateVerif AS hdateverif,
			nbrevues
		FROM Ressource r JOIN RessourceTmp USING (id)
			LEFT JOIN Collection c ON c.ressourceId = r.id
		GROUP BY r.id
		ORDER BY cletri
		SQL) ?>

	sql_joined_field = collections from query; \
		SELECT ressourceId, nom FROM Collection WHERE visible = 1 ORDER BY ressourceId ASC

	sql_attr_uint = lettre1
	sql_attr_uint = nbrevues
	sql_attr_uint = cletri
	sql_attr_bool = autoimportcollections
	sql_attr_bool = exhaustifcollections

	sql_attr_bool = autoimport
	sql_attr_bool = exhaustif
	sql_attr_bool = web

	sql_attr_timestamp = hdatemodif
	sql_attr_timestamp = hdateverif

	sql_attr_multi = uint suivi from query; \
		SELECT cibleId, partenaireId FROM Suivi WHERE cible = 'Ressource' ORDER BY cibleId ASC

	sql_field_string = nomcomplet
}

table <?= $sphinx['tablePrefix'] ?>ressources : <?= $sphinx['tablePrefix'] ?>titres_full
{
	source = <?= $sphinx['tablePrefix'] ?>ressources
	path   = indexes/<?= $sphinx['tablePrefix'] ?>ressources
	charset_table    = U+2E->., <?php include(__DIR__ . '/charsetTable.txt'); echo "\n"; ?>
}
