<?php

use commands\models\SphinxConf as S;

/** @var array<string, string> $db */
/** @var array<string, string> $sphinx */

?>

source <?= $sphinx['tablePrefix'] ?>editeurs
{
	<?= S::render("sql_init_session.php", ['db' => $db]) ?>

	sql_query_pre = <?= S::format(<<<SQL
		CREATE TEMPORARY TABLE EditeurTmp
		(
			cletri INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			id INT UNSIGNED NOT NULL,
			nomcomplet VARCHAR(255) NOT NULL,
			lettre1 TINYINT UNSIGNED NOT NULL,
			nbrevues INT UNSIGNED NOT NULL,
			nbtitresmorts INT UNSIGNED NOT NULL DEFAULT 0,
			nbtitresvivants INT UNSIGNED NOT NULL DEFAULT 0
		) DEFAULT CHARACTER SET utf8mb4 ENGINE=MEMORY
		SELECT NULL AS cletri,
			e.id,
			CONCAT(e.prefixe, e.nom, IF(e.dateDebut<>'', CONCAT(' (',CAST(LEFT(e.dateDebut,4) as CHAR),' à ',IF(e.dateFin<>'',cast(LEFT(e.dateFin, 4) as CHAR), '…'),')'),''), IF(e.sigle<>'', CONCAT(' — ', e.sigle), '')) as nomcomplet,
			FIND_IN_SET(LEFT(e.nom, 1), 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z') AS lettre1,
			COUNT(DISTINCT t.revueId) AS nbrevues,
			0 AS nbtitresmorts,
			0 AS nbtitresvivants
		FROM Editeur e
			LEFT JOIN Titre_Editeur te ON te.editeurId = e.id
			LEFT JOIN Titre t ON t.id = te.titreId
		WHERE e.statut = 'normal'
		GROUP BY e.id
		ORDER BY e.nom
		SQL) ?>

	sql_query_pre = ALTER TABLE EditeurTmp ADD INDEX i_tmp (id)
	sql_query_pre = <?= S::format(<<<SQL
		UPDATE EditeurTmp
		JOIN (
			SELECT te.editeurId AS id, count(DISTINCT t.id) AS nb
			FROM Titre_Editeur te
				JOIN Titre t ON t.id = te.titreId
			WHERE t.obsoletePar IS NULL AND t.dateFin = '' AND IFNULL(te.ancien, 0) = 0
			GROUP BY te.editeurId
			) t USING(id)
		SET nbtitresvivants = t.nb
		SQL) ?>
	sql_query_pre = <?= S::format(<<<SQL
		UPDATE EditeurTmp
		JOIN (
			SELECT te.editeurId AS id, count(DISTINCT t.id) AS nb
			FROM Titre_Editeur te
				JOIN Titre t ON t.id = te.titreId
			GROUP BY te.editeurId
			) t USING(id)
		SET nbtitresmorts = t.nb - nbtitresvivants
		SQL) ?>
	sql_ranged_throttle	= 0

	sql_query = <?= S::format(<<<SQL
		SELECT
			id,
			sigle,
			nomcomplet,
			description, geo,
			lettre1,
			cletri,
			LEFT(idref, 8) AS idref,
			nbrevues,
			nbtitresmorts,
			nbtitresvivants,
			paysId AS paysid,
			sherpa,
			ror,
			hdateModif AS hdatemodif,
			hdateVerif AS hdateverif
		FROM Editeur JOIN EditeurTmp USING (id)
		WHERE statut = 'normal'
		ORDER BY cletri
		SQL) ?>

	sql_attr_uint = lettre1
	sql_attr_uint = cletri
	sql_attr_uint = idref
	sql_attr_uint = nbrevues
	sql_attr_uint = nbtitresmorts
	sql_attr_uint = nbtitresvivants
	sql_attr_uint = paysid
	sql_attr_uint = sherpa

	sql_attr_timestamp = hdatemodif
	sql_attr_timestamp = hdateverif

	sql_attr_multi	= uint suivi from query; \
		SELECT cibleId, partenaireId FROM Suivi WHERE cible = 'Editeur' ORDER BY cibleId ASC

	sql_field_string	= nomcomplet
	sql_attr_string	= ror
}
table <?= $sphinx['tablePrefix'] ?>editeurs : <?= $sphinx['tablePrefix'] ?>titres_full
{
	source = <?= $sphinx['tablePrefix'] ?>editeurs
	path   = indexes/<?= $sphinx['tablePrefix'] ?>editeurs
}
