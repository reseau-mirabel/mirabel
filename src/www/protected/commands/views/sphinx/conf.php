<?php

/** @var array<string, string> $db */
/** @var array<string, string> $sphinx */

require __DIR__ . "/_conf_titres.php";
require __DIR__ . "/_conf_ressources.php";
require __DIR__ . "/_conf_editeurs.php";
