<?php

class GrappeCommand extends CConsoleCommand
{
	public function actionRefresh()
	{
		$grappes = Grappe::model()->findAllBySql("SELECT * FROM Grappe WHERE recherche <> '[]'");
		foreach ($grappes as $g) {
			assert($g instanceof Grappe);
			(new \processes\grappe\ContentManager($g))->refreshSearches();
			usleep(100_000);
		}
	}
}
