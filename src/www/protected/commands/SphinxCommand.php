<?php

/**
 * Tools for configuring Sphinx.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class SphinxCommand extends CConsoleCommand
{
	public $verbose = 1;

	/**
	 * Default action, called when no argument is given.
	 */
	public function actionIndex()
	{
		echo "./yii sphinx <action> [--verbose=?]\n";
		echo "\t avec <action> : conf | confLocal | confCommon\n";
	}

	/**
	 * Action "conf" that concatenates local and common.
	 */
	public function actionConf()
	{
		if ($this->actionConfLocal() !== 0) {
			return 1;
		}
		return $this->actionConfCommon();
	}

	/**
	 * Action "confLocal" that configures the sources and indexes.
	 */
	public function actionConfLocal()
	{
		if (!self::checkConfig()) {
			return 1;
		}

		$db = self::parseConnectionConfig(Yii::app()->getComponent('db'));
		$sphinx = self::parseConnectionConfig(Yii::app()->getComponent('sphinx'));
		require __DIR__ . '/views/sphinx/conf.php';

		return 0;
	}

	/**
	 * Action "confCommon" that configures the searchd server.
	 */
	public function actionConfCommon(): int
	{
		if (!self::checkConfig()) {
			return 1;
		}

		$sphinx = self::parseConnectionConfig(Yii::app()->getComponent('sphinx'));
		// $sphinx will be used by this include
		require __DIR__ . '/views/sphinx/common.php';

		if ($this->verbose > 0) {
			fprintf(
				STDERR,
				"## Tester le serveur searchd de Manticore avec :\n\tmysql "
				. (isset($sphinx['unix_socket']) ? "--socket={$sphinx['unix_socket']}\n" : "--port {$sphinx['port']}\n")
			);
		}
		return 0;
	}

	private static function parseConnectionConfig(?\IApplicationComponent $db): array
	{
		if (!($db instanceof \CDbConnection)) {
			throw new \Exception("La configuration est incomplète.");
		}
		$result = [
			'host' => 'localhost', // default value
		];
		foreach (explode(';', $db->connectionString) as $term) {
			if (strpos($term, '=') !== false) {
				[$k, $v] = explode('=', $term);
				$key = preg_replace('/^mysql:/', '', $k);
				$result[$key] = $v;
			}
		}
		$result['user'] = $db->username;
		$result['pass'] = $db->password;
		$result['tablePrefix'] = $db->tablePrefix ?? '';
		return $result;
	}

	private static function checkConfig(): bool
	{
		$sphinx = Yii::app()->getComponent('sphinx');
		if ($sphinx === null) {
			fprintf(STDERR, "Sphinx n'est pas configuré dans config/local.php\n");
			return false;
		}
		if (empty(Yii::app()->db->connectionString)) {
			fprintf(STDERR, "MySQL n'est pas configuré dans config/local.php\n");
			return false;
		}
		return true;
	}
}
