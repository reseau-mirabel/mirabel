<?php

use commands\models\linkimport;
use components\SqlHelper;
use models\wikidata\Cache;
use models\wikidata\Compare;
use models\wikidata\Export;
use models\wikidata\Import;
use models\wikidata\Query;
use models\wikidata\Stats;
use models\wikidata\Synchro;

/**
 * Wikidata cache, import and export
 *
 * @author Guillaume Allègre <guillaume.allegre@silecs.info>
 */
class WikidataCommand extends CConsoleCommand
{
	use \commands\models\MaintenanceModeTrait;
	use \models\traits\VerbosityEcho;

	public $verbose = 0;

	public $simulation = 0;

	public $limit = 0;

	public function getHelp()
	{
		return <<<EOL
			Ce script alimente le cache Wikidata et calcule les différences avec les données Mir@bel.
			Voir la documentation technique à l'adresse web  .../doc/wikidata.md

			== CRON ==
			yii wikidata cronFetchCompare   enchaine tous les fetch et toutes les comparaisons automatisables
			yii wikidata cronImportWplinks  importe les liens Wikipédia (provenant de Wikidata) avec détection des liens obsolètes

			== Exports ==
			yii wikidata exportQuickstatements          crée le fichier exportable à saisir dans QS

			== Imports ==
			yii wikidata updateWplinksName (ONE-SHOT)   met à jour les liens "Wikipédia" manuels en "Wikipédia (langue)"
			yii wikidata wplinksStats                   affiche le nombre de liens Wikipédia par langue dans LienTitre

			== Diagnostic / Statistiques ==
			yii wikidata propStats          affiche le nombre de liens externes par propriété stockés dans le cache Wikidata
			yii wikidata showProperties     affiche toutes les propriétés disponibles
			yii wikidata showProperty       affiche les détails d'une propriété (--prop=)
			yii wikidata testNetwork        teste les urls de Query avec des requêtes minimales
			yii wikidata wpStats            affiche le nombre de liens Wikipédia par langue stockés dans le cache Wikidata

			Paramètres communs :
				--verbose=1     : 0-concis 1-normal 2-debug
				--simulation=0  : 0-save 1-validate 2-nothing  (pour la mise en cache seulement)
				--limit=0       : limite optionnelle, sparql ou sql selon le contexte
				--obsoleteSince=... pour les liens Wikipedia

			EOL;
	}

	/**
	 * Default action, called when no argument is given.
	 */
	public function actionIndex()
	{
		echo $this->getHelp();
	}

	public function actionTestNetwork()
	{
		$WQ = new Query('en', 'application/sparql-results+json');
		print_r($this->testNetwork($WQ));
	}

	public function actionCronFetchCompare(): int
	{
		$job = new Synchro();
		$job->verbose = (int) $this->verbose;
		$success = $job->run((bool) $this->simulation, (int) $this->limit);
		return ($success ? 0 : 1);
	}

	public function actionCronImportWplinks(array $email = [], string $obsoleteSince = "")
	{
		$params = array_merge(
			get_object_vars($this),
			[
				'email' => $email,
				'obsoleteSince' => $obsoleteSince,
			]
		);
		$importer = new linkimport\Wikipedia($params);
		$importer->import();
		return 0;
	}

	public function actionWpStats()
	{
		print_r(Stats::getWikipediaLanguageStats());
	}

	public function actionShowProperties()
	{
		print_r(Cache::PROPERTIES_TREE);
	}

	public function actionShowProperty($prop)
	{
		printf("%s  (%s) :\n", $prop, Cache::getWdProperties()[$prop]);
		$WQuery = new Query('fr', '');
		print_r($WQuery->getPropertyDetails($prop));
	}

	public function actionPropStats()
	{
		$prev = 0;
		foreach (Stats::getPropertyStats() as $row) {
			if ($row['propertyType'] != $prev) {
				echo "\n";
				$prev = $row['propertyType'];
			}
			printf(
				"%2d  %6s %-12s  %5d  %20s \n",
				$row['propertyType'],
				$row['property'],
				Cache::getWdProperties()[$row['property']],
				$row['Nb'],
				$row['Date'],
			);
		}
		echo "\n";
	}

	public function actionUpdateWplinksName()
	{
		$this->fixLinkSourcesForEditors();

		$WImport = new Import($this->verbose, $this->simulation, $this->limit);
		$WImport->updateWplinksChangeName();
		return 0;
	}

	public function actionWplinksStats()
	{
		print_r(Stats::getWikipediaLinksStats());
	}

	public function actionExportQuickstatements()
	{
		$WExport = new Export();
		$fhandler = fopen('quickstatements.csv', 'w');
		$rows = $WExport->genQuickstatements($fhandler);
		echo "quickstatements.csv : $rows lignes.\n";
		fclose($fhandler);

		$this->notifyCsvFile('log-multiple-revues.csv', 'logWarning', 'nbrevues');
		$this->notifyCsvFile('log-multiple-titres.csv', 'logWarning', 'nbtitres');
		$this->notifyCsvFile('log-multiple-elements.csv', 'logMultielements', 'nbtitres');
	}

	/**
	 * Teste chaque url interrogée avec une requête minimale
	 *
	 * @return string[]
	 */
	public function testNetwork(Query $wq): array
	{
		$diagnostic = [];
		try {
			$wq->querySparql('SELECT ?i WHERE {?i wdt:P31 wd:Q146.} LIMIT 1');
			$diagnostic[] = 'OK';
		} catch (\Exception $e) {
			$diagnostic[] = $e->getMessage();
		}
		try {
			$wq->getPropertyDetails('P31');
			$diagnostic[] = 'OK';
		} catch (\Exception $e) {
			$diagnostic[] = $e->getMessage();
		}
		return $diagnostic;
	}

	private function notifyCsvFile(string $filename, string $method, string $param): void
	{
		$WExport = new Export();
		$fhandler = fopen($filename, 'w');
		$WExport->logHeaders($fhandler);
		$rows = $WExport->{$method}($fhandler, $param);
		echo "$filename : $rows lignes.\n";
		fclose($fhandler);
	}

	private function fixLinkSourcesForEditors(): void
	{
		$sources = SqlHelper::sqlToPairsObject("SELECT * FROM Sourcelien WHERE nomcourt LIKE 'wikipedia-__'", Sourcelien::class, 'url');

		$rows = Yii::app()->db
			->createCommand(
				<<<EOSQL
				SELECT e.id, e.liensJson, l.domain, l.url
				FROM LienEditeur l
				  JOIN Editeur e ON l.editeurId = e.id
				WHERE l.name = 'Wikipedia' AND l.domain LIKE '%.wikipedia.org'
				EOSQL
			)->queryAll();
		$updateEditeur = Yii::app()->db->createCommand("UPDATE Editeur SET liensJson = :l WHERE id = :id");
		foreach ($rows as $row) {
			$newSource = $sources[$row['domain']];
			$decoded = json_decode($row['liensJson']);
			foreach ($decoded as $i => $link) {
				if ($link->src === 'Wikipedia' && $link->url === $row['url']) {
					$decoded[$i] = [
						'sourceId' => (int) $newSource->id,
						'src' => $newSource->nom,
						'url' => $link->url,
					];
				}
			}
			$updateEditeur->execute([':l' => json_encode($decoded), ':id' => $row['id']]);
		}

		$update = Yii::app()->db->createCommand("UPDATE LienEditeur SET sourceId = :sid, name = :n WHERE domain = :d");
		foreach ($sources as $source) {
			$update->execute([':sid' => $source->id, ':n' => $source->nom, ':d' => $source->url]);
		}
	}
}
