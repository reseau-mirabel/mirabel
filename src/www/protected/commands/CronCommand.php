<?php

use processes\cron\Cron;
use processes\cron\SystemConverter;

class CronCommand extends CConsoleCommand
{
	/**
	 * Convert the system cron which is read on STDIN.
	 *
	 * Usage: crontab -l | ./yii cron convert
	 */
	public function actionConvert($verbose = "1")
	{
		$commands = [];
		while (($row = fgets(STDIN)) !== false) {
			$trimmed = trim($row, " \n");
			if (!$trimmed || $trimmed[0] === '#') {
				continue;
			}
			$splits = preg_split('#(php |./)yii #', $trimmed);
			if (count($splits) === 1) {
				$commands[] = $trimmed;
			} else {
				$base = rtrim(array_shift($splits), ' &');
				foreach ($splits as $split) {
					$s = rtrim($split, ' &');
					$commands[] = "{$base} && ./yii {$s}";
				}
			}
		}

		$converter = new SystemConverter();
		$converter->verbose = (int) $verbose;
		$saved = 0;
		foreach ($commands as $row) {
			try {
				$attributes = $converter->extractAttributesFromLine($row);
			} catch (\Exception $e) {
				fprintf(STDERR, "ERREUR d'analyse « %s » pour '%s'\n", $e->getMessage(), $row);
				continue;
			}
			if (!$attributes) {
				continue;
			}

			$exists = \Yii::app()->db
				->createCommand("SELECT 1 FROM CronTask WHERE fqdn = :fqdn AND name = :n LIMIT 1")
				->queryScalar([':fqdn' => $attributes['fqdn'], ':n' => $attributes['name']]);
			if ($exists) {
				if ($verbose > 1) {
					echo "DÉJÀ PRÉSENT: {$row}";
				}
				continue;
			}

			$task = new processes\cron\ar\Task();
			$task->setAttributes($attributes, false);
			$task->description = '';
			$task->active = true;
			$task->timelimit = 0;
			$task->lastUpdate = (int) $_SERVER['REQUEST_TIME'];
			if ($task->save(false)) {
				if ($verbose > 0) {
					echo "CONVERTED AND SAVED: {$row}";
				}
				$saved++;
			} else {
				echo "ERROR while saving " . json_encode($attributes) . "\n";
				return 1;
			}
		}
		echo "$saved lignes enregistrées dans le cron interne.\n";
		return 0;
	}

	public function actionRun(array $args = [])
	{
		if (\Config::read('maintenance.cron.arret')) {
			echo "Cette commande est bloquée par le mode de maintenance du cron.\n";
			exit(1); // send a failure state to the shell
		}
		if (!$args || !ctype_digit($args[0])) {
			fprintf(STDERR, "Paramètre requis : durée en minute entre les appels (si possible, 1)\n");
			return 1;
		}
		$cron = new Cron();
		$cron->run((int) $args[0]);
	}
}
