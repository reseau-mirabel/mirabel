<?php

use processes\categorie\IndexationImport;

require_once Yii::getPathOfAlias('application.models.import') . '/SimpleLogger.php';

/**
 * Read indexation tags.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class IndexationCommand extends CConsoleCommand
{
	/**
	 * Fill the table CategorieStats.
	 */
	public function actionUpdateStats()
	{
		CategorieStats::fill();
	}

	public function actionImport($vocabulaire, $csv)
	{
		if (!file_exists($csv)) {
			echo "Fichier CSV '$csv' non trouvé.\n";
			return 1;
		}

		$import = new IndexationImport();
		if (ctype_digit($vocabulaire)) {
			$import->vocabulaire = Vocabulaire::model()->findByPk($vocabulaire);
		} else {
			$import->vocabulaire = Vocabulaire::model()->findByAttributes(['titre' => $vocabulaire]);
		}
		if (!$import->vocabulaire) {
			echo "Vocabulaire non trouvé, par titre comme par ID.\n";
			return 1;
		}

		$expected = $import->parseCsvFile($csv, STDOUT);

		fprintf(STDERR, "## Mots-clés inconnus de Mir@bel :\n" . join("\n", $import->getUnknownWords()) . "\n");

		$inserts = $import->save();
		fprintf(STDERR, "## Indexations enregistrées : %d / %d\n", $inserts, $expected);
		return 0;
	}

	public function actionFix()
	{
		$cmd = Yii::app()->db->createCommand(<<<EOSQL
			UPDATE Categorie c LEFT JOIN Categorie p ON c.parentId = p.id
			SET c.chemin = IF(c.parentId IS NULL, '', CONCAT(p.chemin, '/', LPAD(c.parentId,5,'0')))
			WHERE c.profondeur = :p
			EOSQL
		);
		for ($depth = 0; $depth <= Categorie::MAX_DEPTH; $depth++) {
			$cmd->execute([':p' => $depth]);
		}
	}
}
