<?php

namespace controllers\statsacces;

use CHtml;
use StatsaccesController;

class IndexAction extends \CAction
{
	public function run(): void
	{
		$years = glob(StatsaccesController::getStatsPath('', '') . "/*");
		$reports = [];
		foreach ($years as $yearPath) {
			$m = [];
			if (!preg_match('#/(\d{4})$#', $yearPath, $m)) {
				continue;
			}
			$year = (int) $m[1];
			$reports[$year] = self::readByYear($year, $yearPath);
		}
		$this->getController()->render("index", ["reports" => $reports]);
	}

	private static function readByYear(int $year, string $yearPath): array
	{
		$report = array_fill(0, 13, '-');
		$report[0] = $year;
		foreach (glob($yearPath . "/??") as $monthPath) {
			$m = [];
			if (!preg_match('#/(\d{2})$#', $monthPath, $m)) {
				continue;
			}
			$dir = (int) $m[1];
			if ($dir <= 0 || $dir > 12) {
				continue;
			}
			$date = sprintf('%d-%02d', $year, $dir);
			$report[$dir] = CHtml::link($date, ["/statsacces/$date"], ['target' => '_blank']);
		}
		return $report;
	}
}
