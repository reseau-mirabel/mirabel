<?php

namespace controllers\upload;

use CHttpException;
use Upload;
use Yii;

/**
 * Deletes an uploaded file which is in a protected dir.
 */
class DeleteAction extends \CAction
{
	public function run(): void
	{
		$file = $_POST['file'] ?? 'file parameter is missing';
		$serverPath = self::checkAccess($file);

		$del = unlink($serverPath);
		if (Yii::app()->request->isAjaxRequest) {
			if ($del) {
				echo '<em>supprimé</em>';
			} else {
				throw new CHttpException(500, "Erreur lors de la suppression.");
			}
		} else {
			$this->getController()->redirect('index');
		}
	}

	private static function checkAccess(string $webPath): string
	{
		if (!Yii::app()->user->checkAccess('redaction/editorial') && !Yii::app()->user->checkAccess('permPartenaire')) {
			throw new CHttpException(403, "Vous n'avez pas de droit de rédaction pour les fonctions éditoriales.");
		}
		if (!Yii::app()->request->isPostRequest || empty($_POST['file'])) {
			throw new CHttpException(400, "POST only.");
		}
		$serverPath = Upload::toServerPath($webPath);
		if ($serverPath === '') {
			throw new CHttpException(403, "L'accès à ce fichier n'est pas autorisé.");
		}
		if (is_dir($webPath)) {
			throw new CHttpException(403, "La suppression des répertoires est interdite.");
		}
		return $serverPath;
	}
}
