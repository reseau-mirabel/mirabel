<?php

namespace controllers\upload;

use CHttpException;
use CUploadedFile;
use CMarkdownParser;
use CHtml;
use Upload;
use Yii;

class IndexAction extends \CAction
{
	/**
	 * Default upload page.
	 *
	 * @param string $dest One the destinations declared in init().
	 */
	public function run($dest = 'public', $partenaireId = 0)
	{
		$controller = $this->getController();
		assert($controller instanceof \UploadController);

		if (!$partenaireId) {
			$partenaireId = (int) \Yii::app()->user->getState('partenaireId');
		}
		$this->checkAccess($dest, (int) $partenaireId);
		$permRename = \Yii::app()->user->checkAccess('admin');
		$model = new Upload($dest, $permRename);

		if ($dest === 'logos-p') {
			$model->destName = sprintf('%03d', $partenaireId);
		}
		if (self::receiveFile($model)) {
			$controller->redirect(['index', 'dest' => $dest]);
			return;
		}

		$lisezmoi = self::fetchReadme($model);
		if ($dest === 'conventions') {
			$fileList = self::listFiles($model->getDestination()->path, 'name', 'DESC');
		} else {
			$fileList = self::listFiles($model->getDestination()->path);
		}
		$controller->render(
			'index',
			[
				'model' => $model,
				'uploaded' => $fileList,
				'lisezmoi' => $lisezmoi,
			]
		);
	}

	private function checkAccess(string $dest, int $partenaireId): void
	{
		$user = Yii::app()->user;
		if ($dest === 'logos-p') {
			if (!$user->access()->toPartenaire()->logo((int) $partenaireId) && !$user->checkAccess('redaction/editorial')) {
				throw new CHttpException(403, "Vous n'avez pas de droit de modifier les données de votre établissement.");
			}
		} elseif ($dest === 'editeurs') {
			if (!$user->checkAccess('suiviPartenairesEditeurs') && !$user->checkAccess('redaction/editorial')) {
				throw new CHttpException(403, "Vous n'avez pas de droit d'accéder aux lettres aux éditeurs.");
			}
		} else {
			if (!$user->checkAccess('redaction/editorial')) {
				$pid = Yii::app()->user->getPartenaireId();
				if ($user->access()->toPartenaire()->logo($pid)) {
					$this->getController()->redirect(['index', 'dest' => 'logos-p', 'partenaireId' => $pid]);
				} else {
					throw new CHttpException(403, "Vous n'avez pas de droit de rédaction pour les fonctions éditoriales.");
				}
			}
		}
	}

	private static function fetchReadme(Upload $model): string
	{
		$path = rtrim($model->getDestination()->path, '/');
		if (file_exists($path . '/lisezmoi.md')) {
			return (new CMarkdownParser())->safeTransform(file_get_contents($path . '/lisezmoi.md'));
		}
		return '';
	}

	/**
	 * Lists the files a directory.
	 *
	 * @return array Each item has the keys (name, size, date, path).
	 */
	private static function listFiles(string $path, string $order = 'name', string $direction = 'ASC'): array
	{
		chdir($path);
		$relPath = rtrim(str_replace(dirname(Yii::app()->getBasePath()), '', $path), '/');
		$list = [];
		foreach (glob('*') as $filename) {
			if (is_dir($filename) || is_link($filename)) {
				continue;
			}
			$list[] = [
				'name' => $filename,
				'size' => filesize($filename),
				'date' => date('Y-m-d', filemtime($filename)),
				'path' => $relPath . '/' . $filename,
			];
		}
		if ($order !== 'name' || $direction !== 'ASC') {
			usort(
				$list,
				function ($x, $y) use ($order, $direction) {
					if ($direction === 'ASC') {
						return is_int($x[$order]) ? $x[$order] <=> $y[$order] : strcmp($x[$order], $y[$order]);
					}
					return is_int($x[$order]) ? $y[$order] <=> $x[$order] : strcmp($y[$order], $x[$order]);
				}
			);
		}
		return $list;
	}

	/**
	 * Helper function that returns an Upload object, or false if there was an error.
	 *
	 * @param Upload $model
	 * @return bool Upload succeeded
	 */
	private static function receiveFile($model)
	{
		if (!isset($_POST['Upload'])) {
			return false;
		}
		$model->setAttributes($_POST['Upload']);
		$model->file = CUploadedFile::getInstance($model, 'file');
		if (!$model->file) {
			$model->addError('file', "Aucun fichier envoyé.");
			return false;
		}
		$model->validate();
		if (!$model->hasErrors()) {
			if ($model->moveFile()) {
				Yii::app()->user->setFlash(
					'success',
					"Fichier déposé sur le serveur : " . CHtml::link($model->getViewUrl(), $model->getViewUrl(), ['target'=>'_blank'])
				);
				return true;
			}
			Yii::app()->user->setFlash(
				'error',
				"Le fichier n'a pas pu être écrit (doublon) dans le répertoire " . $model->getDestination()->path
			);
		}
		return false;
	}
}
