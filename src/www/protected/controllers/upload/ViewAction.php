<?php

namespace controllers\upload;

use CFileHelper;
use CHttpException;
use Upload;
use Yii;

/**
 * Serves an uploaded file which is in a protected dir.
 */
class ViewAction extends \CAction
{
	public function run(string $file)
	{
		$filePath = Upload::toServerPath($file);
		if ($filePath === '') {
			throw new CHttpException(403, "L'accès à ce fichier n'est pas autorisé.");
		}

		// Guess the mime type, then send the appropriate header.
		$mime = CFileHelper::getMimeTypeByExtension($filePath);
		if (!$mime) {
			$mime = CFileHelper::getMimeType($filePath);
		}
		if (!$mime) {
			$mime = '';
		}
		self::sendHttpHeader($mime, basename($file));

		// First, stop the application to avoid timeout
		// Workaround Yii1's debug mode where HTML is appended to the response.
		ob_start();
		Yii::app()->end(0, false);
		ob_end_clean();

		readfile($filePath);
	}

	private static function sendHttpHeader(string $mime, string $basename): void
	{
		$encFile = rawurlencode($basename);

		if (!$mime) {
			header('Content-Type: application/force-download');
			header('Content-Disposition: attachment;filename=' . $encFile);
			return;
		}

		$char = '';
		if (strncmp($mime, 'text/', 5)) {
			$char = '; charset="UTF-8"';
		}
		header('Content-Type: ' . $mime . $char);
		if ($mime === 'application/pdf') {
			header("Content-Disposition: inline; filename=" . $encFile);
		}
	}
}
