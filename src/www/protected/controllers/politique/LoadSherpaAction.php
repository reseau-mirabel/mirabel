<?php

namespace controllers\politique;

use CHttpException;
use components\access\Politique;
use processes\politique\LoadSherpa;
use processes\politique\Workflow;
use Yii;

class LoadSherpaAction extends \CAction
{
	public function run(string $editeurId, string $titreId)
	{
		$process = new LoadSherpa(Yii::app()->user->id);
		$process->setEditeur((int) $editeurId);
		$process->setTitre((int) $titreId);

		$politique = $process->createPolitique();
		if (!(new Workflow($politique, $process->getRoleOnPublisher()))->update()) {
			throw new CHttpException(403, "Cette opération ne vous est pas permise.");
		}
		if (!$politique->save(false)) {
			throw new CHttpException(500, print_r($politique->getErrors(), true));
		}

		if ($process->getRoleOnPublisher() !== Politique::ROLE_EDITEUR_PENDING) {
			$process->applyPolicyToJournal($politique);
			Yii::app()->session->add('policy-applyTo', join(',', $process->getTitleIds()));
		}
		Yii::app()->user->setFlash(
			'success',
			"La politique déclarée dans “Open policy finder” a été chargée. Pour le moment, elle n'est affectée qu'au titre choisi."
			. " Ce formulaire est pré-rempli avec les autres affectations lues dans les données de Open policy finder."
		);
		$this->getController()->redirect(['/politique/titres', 'id' => (int) $politique->id]);
	}
}
