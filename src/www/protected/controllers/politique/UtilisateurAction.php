<?php

namespace controllers\politique;

use models\forms\UserPolitique;
use Utilisateur;
use Yii;

class UtilisateurAction extends \CAction
{
	public function run(): void
	{
		if (!Yii::app()->user->isGuest) {
			$utilisateur = Utilisateur::model()->findByPk((int) Yii::app()->user->id);
			assert($utilisateur instanceof Utilisateur);
			$editeurs = $utilisateur->editeurs;
			if (count($editeurs) === 0) {
				$this->getController()->redirect(['/utilisateur/politiques-editeurs']);
			} elseif (count($editeurs) === 1) {
				$e = reset($editeurs);
				$this->getController()->redirect(['/politique', 'editeurId' => $e->id]);
			} else {
				$this->getController()->redirect(['/politique']);
			}
			return;
		}

		$model = new UserPolitique();
		if (isset($_POST['UserPolitique'])) {
			$model->setAttributes($_POST['UserPolitique']);
			if ($model->save()) {
				Yii::app()->user->setFlash('success', "Un courriel vous a été envoyé avec votre mot de passe.");
				$this->getController()->redirect('/site/login');
				return;
			}
			Yii::app()->user->setFlash('error', "Erreur lors de la création du compte.");

		}
		$this->getController()->render(
			'utilisateur',
			['model' => $model]
		);
	}
}
