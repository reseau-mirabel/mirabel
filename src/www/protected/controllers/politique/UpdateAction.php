<?php

namespace controllers\politique;

use CHttpException;
use Politique;
use models\forms\PolitiqueForm;

/**
 * Display the update form.
 * DB update is made with AJAX through the 'save' action.
 */
class UpdateAction extends \CAction
{
	public function run(string $id)
	{
		$politique = Politique::model()->findByPk((int) $id);
		$this->checkAccess($politique);

		$politiqueForm = new PolitiqueForm();
		$politiqueForm->loadPolitique($politique);

		$this->getController()->render(
			'update',
			[
				'politique' => $politique,
				'politiqueForm' => $politiqueForm,
			]
		);
	}

	private function checkAccess(?Politique $politique): void
	{
		if (!($politique instanceof Politique)) {
			throw new CHttpException(404);
		}
	}
}
