<?php

namespace controllers\politique;

use CHttpException;
use Politique;
use PolitiqueLog;
use Yii;
use processes\politique\Workflow;

class DeleteAction extends \CAction
{
	/**
	 * Supprime une politique, avec ses éventuelles associations à des titres.
	 */
	public function run(string $id)
	{
		$politique = Politique::model()->findByPk((int) $id);
		$this->checkAccess($politique);

		$toLog = ($politique->status === Politique::STATUS_PUBLISHED);

		$access = Yii::app()->user->access()->toPolitique();
		$actorRole = $access->getRole((int) $politique->editeurId);
		if (!(new Workflow($politique, $actorRole))->delete()) {
			throw new CHttpException(403, "Vous n'avez pas la permission de supprimer cette politique.");
		}

		if ($politique->status === Politique::STATUS_DELETED) {
			if ($toLog) {
				$this->logChanges($politique);
			}
			Yii::app()->db->createCommand("DELETE FROM Politique_Titre WHERE politiqueId = {$politique->id}")->execute();
			$politique->delete();
			$message = "Politique supprimée";
			if ($this->returnOnAjax($message)) {
				return;
			}
			Yii::app()->user->setFlash('success', $message);
		} elseif ($politique->status === Politique::STATUS_TODELETE) {
			$politique->save(false);
			$message = "Politique en attente de suppression";
			if ($this->returnOnAjax($message)) {
				return;
			}
			Yii::app()->user->setFlash('success', $message);
		}
		$this->getController()->redirect(['/politique', 'editeurId' => $politique->editeurId]);
	}

	private function checkAccess(?Politique $politique): void
	{
		if (!Yii::app()->request->isPostRequest) {
			throw new CHttpException(400);
		}
		if (!($politique instanceof Politique)) {
			throw new CHttpException(404);
		}
		$access = Yii::app()->user->access()->toPolitique();
		if (!$access->delete($politique)) {
			throw new CHttpException(403, "L'accès pour suppression n'est pas autorisé.");
		}
	}

	/**
	 * Log the removal of each link with a Titre and of the Politique itself.
	 */
	private function logChanges(Politique $politique): void
	{
		$ids = \Yii::app()->db
			->createCommand("SELECT titreId FROM Politique_Titre WHERE politiqueId = {$politique->id}")
			->queryColumn();
		$ids[] = 0;
		foreach ($ids as $id) {
			$log = new PolitiqueLog();
			$log->action = PolitiqueLog::ACTION_DELETE;
			$log->actionTime = $_SERVER['REQUEST_TIME'];
			$log->politiqueId = $politique->id;
			$log->editeurId = $politique->editeurId;
			$log->titreId = (int) $id;
			$log->publisher_policy = $politique->publisher_policy;
			$log->save(false);
		}
	}

	private function returnOnAjax(string $message): bool
	{
		if (!Yii::app()->getRequest()->isAjaxRequest) {
			return false;
		}
		header('Content-Type: application/json; charset="UTF-8"');
		echo json_encode(['status' => 200, 'message' => $message]);
		return true;
	}
}
