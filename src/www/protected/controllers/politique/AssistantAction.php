<?php

namespace controllers\politique;

use CHttpException;
use Editeur;
use Politique;
use Titre;
use Yii;

class AssistantAction extends \CAction
{
	public function run($editeurId, $titreId = "")
	{
		$editeur = Editeur::model()->findByPk((int) $editeurId);
		$this->checkAccess($editeur);

		$titre = $this->findJournal((int) $titreId, (int) $editeur->id);
		$politique = $this->findPolicy($titre, (int) $editeur->id);

		$this->getController()->render(
			'assistant',
			[
				'politique' => $politique,
				'editeur' => $editeur,
				'titre' => $titre,
			]
		);
	}

	private function checkAccess(?Editeur $editeur)
	{
		if ($editeur === null) {
			throw new CHttpException(404, "Cet éditeur n'a pas été trouvé.");
		}
		if (!Yii::app()->user->access()->toPolitique()->create($editeur)) {
			throw new \CHttpException(403, "Accès non autorisé");
		}
	}

	private function findJournal(int $titreId, int $editeurId): ?Titre
	{
		if ($titreId) {
			return Titre::model()
				->findBySql(
					"SELECT t.* FROM Titre t JOIN Titre_Editeur te ON te.titreId = t.id WHERE t.id = :tid AND te.editeurId = :eid",
					[':tid' => $titreId, ':eid' => $editeurId]
				);
		}
		$titres = Titre::model()
			->findAllBySql(
				"SELECT t.* FROM Titre t JOIN Titre_Editeur te ON te.titreId = t.id WHERE te.editeurId = :eid LIMIT 2",
				[':eid' => $editeurId]
			);
		if (count($titres) === 1) {
			return $titres[0];
		}
		return null;
	}

	private function findPolicy(?Titre $titre, int $editeurId): ?Politique
	{
		if ($titre === null) {
			return null;
		}
		return Politique::model()
			->findBySql(
				"SELECT p.* FROM Politique_Titre pt JOIN Politique p ON pt.politiqueId = p.id WHERE pt.titreId = :tid AND p.editeurId = :eid",
				[':tid' => $titre->id, ':eid' => $editeurId]
			);
	}
}
