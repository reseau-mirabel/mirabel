<?php

namespace controllers\politique;

use Politique;
use processes\politique\Workflow;
use Yii;

class RejectAction extends \CAction
{
	/**
	 * @param string $id Politique.id
	 * @param string $multi Publish even if several Titre records are related?
	 */
	public function run(string $id, string $multi = "")
	{
		$politique = Politique::model()->findByPk((int) $id);
		if (!($politique instanceof Politique)) {
			if (Yii::app()->request->isAjaxRequest) {
				\Controller::header('json');
				echo json_encode([
					"status" =>  404,
					"message" => "La politique demandée n'existe pas dans la base de données.",
				]);
			} else {
				throw new \CHttpException(404, "Politique introuvable");
			}
			return;
		}

		$userRole = Yii::app()->user->access()->toPolitique()->getRole((int) $politique->editeurId);
		$process = new Workflow($politique, $userRole);
		$result = $process->reject();
		if ($result) {
			$result = $politique->save(false);
			// No log, since this action does not change the status for Sherpa.
		}
		if (Yii::app()->request->isAjaxRequest) {
			\Controller::header('json');
			echo json_encode($result);
		} else {
			Yii::app()->user->setFlash('success', "Politique modifiée");
			$this->controller->redirect(['/politique/update', 'id' => $politique->id]);
		}
	}
}
