<?php

namespace controllers\politique;

use Yii;

class JetonsAction extends \CAction
{
	public function run(): void
	{
		if (!Yii::app()->user->checkAccess('admin')) {
			throw new \CHttpException(403);
		}

		$jetons = self::listRecords();

		if (Yii::app()->request->getIsPostRequest()) {
			$input = Yii::app()->request->getPost('PolitiqueJeton', []);
			$jeton = self::findById((int) $input['id'], $jetons);
			unset($input['id']);
			if ($jeton instanceof \PolitiqueJeton) {
				$jeton->setAttributes($input);
				$changed = false;
				if ($jeton->name !== '' && $jeton->token !== '') {
					$changed = $jeton->save();
				} elseif ($jeton->id) {
					$changed = $jeton->delete();
				}
				if ($changed) {
					Yii::app()->user->setFlash('success', "Modifications enregistrées");
					$this->getController()->refresh();
					return;
				}
			}
		}

		$this->getController()->render(
			'jetons',
			[
				'jetons' => $jetons,
			]
		);
	}

	/**
	 * @param \PolitiqueJeton[] $jetons
	 */
	private static function findById(int $id, array $jetons): ?\PolitiqueJeton
	{
		foreach ($jetons as $j) {
			if ((int) $j->id === $id) {
				unset($j->lastUpdate);
				return $j;
			}
		}
		return null;
	}

	/**
	 * @return \PolitiqueJeton[]
	 */
	private function listRecords(): array
	{
		$jetons = \PolitiqueJeton::model()->sorted()->findAll();
		$new = new \PolitiqueJeton();
		$new->id = 0;
		$jetons[] = $new;
		return $jetons;
	}
}
