<?php

namespace controllers\politique;

use Politique;
use Yii;

class AnnotateAction extends \CAction
{
	public function run(string $id)
	{
		if (!Yii::app()->user->access()->toPolitique()->isAdmin()) {
			throw new \CHttpException(403);
		}

		$politique = Politique::model()->findByPk((int) $id);
		if (!($politique instanceof Politique)) {
			throw new \CHttpException(404);
		}

		if (Yii::app()->getRequest()->isPostRequest) {
			$politique->notesAdmin = Yii::app()->getRequest()->getPost('notesAdmin', '');
			$politique->update(['notesAdmin']);
			$this->getController()->redirect(['/politique/update', 'id' => $politique->id]);
			return;
		}

		$this->getController()->render('annotate', ['politique' => $politique]);
	}
}
