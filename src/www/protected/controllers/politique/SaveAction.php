<?php

namespace controllers\politique;

use CHttpException;
use Editeur;
use Politique;
use PolitiqueLog;
use Yii;
use processes\politique\Workflow;

/**
 * /politique/save action, only for AJAX and POST
 */
class SaveAction extends \CAction
{
	public function run(): void
	{
		$politique = empty($_POST['politiqueId']) ? null : Politique::model()->findByPk((int) $_POST['politiqueId']);
		if ($politique) {
			// There is no way to change the Editeur of a Politique.
			$editeur = $politique->editeur;
		} else {
			$editeur = empty($_POST['editeurId']) ? null : Editeur::model()->findByPk((int) $_POST['editeurId']);
		}

		$this->checkAccess($politique, $editeur);

		$userRole = Yii::app()->user->access()->toPolitique()->getRole((int) $editeur->id);
		$policy = $this->readPolicy($_POST['publisher_policy'], $editeur);

		$updatePublished = false;
		if ($politique === null) {
			// create
			$newP = new Politique();
			$action = new Workflow($newP, $userRole);
			if (!$action->create()) {
				throw new CHttpException(403, "Cette opération ne vous est pas permise.");
			}
			$newP->utilisateurId = (int) Yii::app()->user->id; // not when updating, only the creator ID
			$newP->editeurId = (int) $editeur->id;
			if (!empty($_POST['titreId'])) {
				$newP->initialTitreId = (int) $_POST['titreId'];
			}
		} else {
			// update the existing record
			assert($politique instanceof Politique);
			$updatePublished = ($politique->status === Politique::STATUS_PUBLISHED);
			$newP = clone $politique;
			$action = new Workflow($newP, $userRole);
			if (!$action->update()) {
				throw new CHttpException(403, "Cette opération ne vous est pas permise.");
			}
		}
		$newP->name = $policy['internal_moniker'];
		if (isset($_POST['model'])) {
			$newP->model = (int) $_POST['model'];
		}
		$newP->publisher_policy = json_encode($policy);

		header('Content-Type: application/json; charset="UTF-8"');

		if ($newP->save()) {
			if (self::isPolicyLinkable($newP) && $userRole !== \components\access\Politique::ROLE_EDITEUR_PENDING) {
				\Yii::app()->db
					->createCommand("REPLACE INTO Politique_Titre (politiqueId, titreId, confirmed, createdOn) VALUES (:p, :t, 0, :now)")
					->execute([
						':p' => (int) $newP->id,
						':t' => (int) $newP->initialTitreId,
						':now' => time(),
					]);
			}
			if ($updatePublished) {
				self::logChanges($politique);
			}
			Yii::app()->user->setFlash('success', "La politique est enregistrée sous le nom « {$newP->name} ».");
			echo json_encode(['success' => true]);
		} else {
			Yii::log(
				"Erreur d'enregistrement d'une politique envoyée par l'assistant.\nUID={$newP->utilisateurId}\nPOST=" . var_export($_POST, true),
				\CLogger::LEVEL_ERROR,
				'politique'
			);
			http_response_code(500);
			echo json_encode([
				'success' => false,
				'message' => "Une erreur est survenue lors de l'enregistrement."
					. " Elle est signalée aux administrateurs du site qui récupéreront ces données et les enregistreront. Désolé pour ce bug.",
				'errors' => $politique->getErrors(),
			]);
		}
	}

	private function checkAccess(?Politique $politique, ?Editeur $editeur): void
	{
		if ($editeur === null) {
			throw new CHttpException(404, "L'éditeur n'a pas été trouvé.");
		}

		if ($politique !== null && !Yii::app()->user->access()->toPolitique()->update($politique)) {
			throw new CHttpException(403, "Accès non autorisé");
		}

		$userRole = Yii::app()->user->access()->toPolitique()->getRole((int) $editeur->id);
		if (!$userRole) {
			throw new CHttpException(403, "Aucune permission sur cet éditeur.");
		}
	}

	private static function isPolicyLinkable(Politique $policy): bool
	{
		if (!$policy->initialTitreId) {
			return false;
		}
		return $policy->status !== Politique::STATUS_PENDING;
	}

	private function readPolicy(string $json, Editeur $editeur): array
	{
		$policy = json_decode($json, true);

		// Set a unique name
		if (empty($policy['internal_moniker'])) {
			$names = Yii::app()->db
				->createCommand("SELECT p.name FROM Politique p WHERE p.utilisateurId = :uid AND editeurId = :eid")
				->queryColumn([':uid' => Yii::app()->user->id, ':eid' => $editeur->id]);
			if (!$names) {
				$name = "politique de {$editeur->nom}";
			} else {
				for ($i = 1; $i < 99; $i++) {
					$name = "politique $i de {$editeur->nom}";
					if (!in_array($name, $names)) {
						break;
					}
				}
			}
			$policy['internal_moniker'] = $name;
		}

		// Filter out null values in licenses
		foreach ($policy['permitted_oa'] as $k => $oa) {
			if (isset($oa['license']) && count($oa['license']) > 0) {
				$policy['permitted_oa'][$k]['license'] = array_filter(
					$oa['license'],
					function ($x) {
						return $x !== null;
					}
				);
			}
		}
		if (empty($policy['internal_moniker'])) {
			// TODO ?
		}
		return $policy;
	}

	private static function logChanges(Politique $politique): void
	{
		$ids = \Yii::app()->db
			->createCommand("SELECT titreId FROM Politique_Titre WHERE politiqueId = {$politique->id}")
			->queryColumn();
		foreach ($ids as $id) {
			$log = new PolitiqueLog();
			$log->action = PolitiqueLog::ACTION_UPDATE;
			$log->politiqueId = (int) $politique->id;
			$log->editeurId = (int) $politique->editeurId;
			$log->titreId = (int) $id;
			$log->publisher_policy = $politique->publisher_policy;
			$log->save(false);
		}
	}
}
