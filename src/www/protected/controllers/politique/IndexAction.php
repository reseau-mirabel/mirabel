<?php

namespace controllers\politique;

use processes\politique\Index as IndexProcess;
use Yii;

class IndexAction extends \CAction
{
	public function run(string $editeurId = '')
	{
		if (Yii::app()->user->isGuest) {
			$this->getController()->redirect(['/site/login', 'mode' => 'politique']);
			return;
		}
		$model = new IndexProcess(Yii::app()->user->id);
		$model->setEditeur((int) $editeurId);

		// redirect if wrong publisher id
		if ($editeurId && ($model->editeur === null || (int) $model->editeur->id !== (int) $editeurId)) {
			$url = ['/politique/index'];
			if ($model->editeur !== null) {
				$url['editeurId'] = $model->editeur->id;
			}
			$this->getController()->redirect($url);
			return;
		}

		$this->getController()->render('index', ['model' => $model]);
	}
}
