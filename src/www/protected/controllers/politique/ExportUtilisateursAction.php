<?php

namespace controllers\politique;

class ExportUtilisateursAction extends \CAction
{
	public function run(): void
	{
		if (!\Yii::app()->user->checkAccess('admin')) {
			throw new \CHttpException(403);
		}

		$sql = <<<SQL
			SELECT
				u.nom, u.prenom, u.email, u.actif, u.permPolitiques, u.partenaireId,
				p.id AS politiqueId, p.name AS politique, p.status AS politiqueStatut, p.initialTitreId, p.notesAdmin,
				e.nom AS editeur
			FROM Utilisateur u
				LEFT JOIN Politique p ON u.id = p.utilisateurId
				LEFT JOIN Utilisateur_Editeur ue ON ue.utilisateurId = u.id
				LEFT JOIN Editeur e ON e.id = ue.editeurId
			WHERE u.`permPolitiques` = 1 OR e.id > 0 OR p.id > 0
			ORDER BY u.nom, u.prenom, p.name
			SQL;
		$dataProv = \Yii::app()->db->createCommand($sql)->setFetchMode(\PDO::FETCH_NUM)->query();

		$csv = self::export($dataProv);
		\Controller::header('nocache');
		\Controller::header('csv');
		header('Content-Disposition: inline; filename="Mirabel_politiques-utilisateurs_' . date('Y-m-d') . '.csv"');
		echo $csv;
	}

	private static function export(\Iterator $dataProv): string
	{
		$header = [
			"Nom", "Prénom", "email", "actif", "permission Politiques", "ID Partenaire",
			"ID Politique", "Politique", "statut P", "ID titre initial", "notes d'admin",
			"Éditeur",
		];
		$out = fopen('php://memory', 'w');
		fputcsv($out, $header, ';', '"', '\\');
		foreach ($dataProv as $data) {
			fputcsv($out, $data, ';', '"', '\\');
		}
		return (string) stream_get_contents($out, -1, 0);
	}
}
