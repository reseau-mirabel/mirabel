<?php

namespace controllers\politique;

use processes\politique\Roles as RolesProcess;
use Yii;

class RolesAction extends \CAction
{
	public function run(string $editeurId)
	{
		$model = new RolesProcess((int) Yii::app()->user->id, (int) $editeurId);
		if (Yii::app()->getRequest()->isPostRequest) {
			$intervention = $model->createIntervention($_POST['TitreEditeur']);
			if ($intervention !== null) {
				if (Yii::app()->user->access()->toEditeur()->roles()) {
					$intervention->accept(false);
				}
				if ($intervention->save()) {
					$message = $intervention->statut === 'attente' ? " Elles seront appliquées après validation par le réseau Mir@bel." : "";
					Yii::app()->user->setFlash('success', "Merci d'avoir complété ces données.$message");
					$this->getController()->redirect(['/politique/index', 'editeurId' => $model->editeur->id]);
				} else {
					Yii::app()->user->setFlash('danger', "Erreur lors de l'enregistrement. Contactez l'équipe de Mir@bel.");
				}
			}
		}
		$this->getController()->render('roles', ['model' => $model]);
	}
}
