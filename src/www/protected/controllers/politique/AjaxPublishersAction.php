<?php

namespace controllers\politique;

use Yii;

/**
 * Lists the publishers related to a policy
 */
class AjaxPublishersAction extends \CAction
{
	public function run($id)
	{
		$result = [
			'par_politique' => null,
			'par_titres' => [],
		];

		$politique = Yii::app()->db
			->createCommand(<<<EOSQL
				SELECT
					e.id,
					e.nom,
					p.utilisateurId,
					IF(p.utilisateurId IS NULL, '', CONCAT(u.prenom, ' ', u.nom)) AS utilisateur,
					p.lastUpdate
				FROM Editeur e
					JOIN Politique p ON p.editeurId = e.id
					LEFT JOIN Utilisateur u ON p.utilisateurId = u.id
				WHERE p.id = :id
				EOSQL
			)->queryAll(true, [':id' => (int) $id]);
		if ($politique) {
			$result['par_politique'] = [
				'id' => (int) $politique[0]['id'],
				'nom' => $politique[0]['nom'],
				'utilisateurId' => (int) $politique[0]['utilisateurId'],
				'utilisateur' => $politique[0]['utilisateur'],
				'lastUpdate' => date('Y-m-d H:i', $politique[0]['lastUpdate']),
			];
		}

		$titres = Yii::app()->db
			->createCommand(<<<EOSQL
				SELECT
					e.id,
					e.nom,
					e.role AS role,
					GROUP_CONCAT(DISTINCT te.role SEPARATOR '|') AS roles
				FROM Editeur e
					JOIN Titre_Editeur te ON te.editeurId = e.id
					JOIN Politique_Titre pt ON pt.titreId = te.titreId
				WHERE pt.politiqueId = :id
				GROUP BY e.id
				ORDER BY e.nom
				EOSQL
			)->queryAll(true, [':id' => (int) $id]);
		$eRoles = \Editeur::getPossibleRoles();
		$tRoles = \TitreEditeur::getPossibleRoles();
		foreach ($titres as $e) {
			$result['par_titres'][] = [
				'id' => (int) $e['id'],
				'nom' => $e['nom'],
				'role' => empty($e['role']) ? null : $eRoles[$e['role']],
				'roles' => empty($e['roles']) ? [] :
					array_map(
						function ($r) use ($tRoles) {
							return $tRoles[$r] ?? $r;
						},
						explode('|', $e['roles'])
					),
			];
		}

		header('Content-Type: application/json; charset="UTF-8"');
		echo json_encode($result, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
	}
}
