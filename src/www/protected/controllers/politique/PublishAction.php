<?php

namespace controllers\politique;

use Politique;
use processes\politique\Publish;
use Yii;

/**
 * Publish a policy according to the user's permissions:
 * - if owner, set the state to TOPUBLISH,
 * - if moderator, set the state to PUBLISHED.
 */
class PublishAction extends \CAction
{
	/**
	 * @param string $id Politique.id
	 * @param string $multi Publish even if several Titre records are related?
	 */
	public function run(string $id, string $multi = "")
	{
		$politique = Politique::model()->findByPk((int) $id);
		if (!($politique instanceof Politique)) {
			if (Yii::app()->request->isAjaxRequest) {
				\Controller::header('json');
				echo json_encode([
					"status" =>  404,
					"message" => "La politique demandée n'existe pas dans la base de données.",
				]);
			} else {
				throw new \CHttpException(404, "Politique introuvable");
			}
			return;
		}
		$userRole = Yii::app()->user->access()->toPolitique()->getRole((int) $politique->editeurId);
		$process = new Publish($politique, $userRole);
		$result = $process->run((bool) $multi);
		if (Yii::app()->request->isAjaxRequest) {
			\Controller::header('json');
			echo json_encode($result);
		} else {
			if ($result['status'] === 200) {
				Yii::app()->user->setFlash('success', "Politique modifiée. {$result['message']}");
			} else {
				Yii::app()->user->setFlash('success', "Erreur lors de la modification de la politique : {$result['message']}");
			}
			$this->controller->redirect(['/politique/update', 'id' => $politique->id]);
		}
	}
}
