<?php

namespace controllers\politique;

use CHttpException;
use models\searches\PolitiqueSearch;
use Politique;
use Yii;

class ModerationAction extends \CAction
{
	public function run(): void
	{
		if (!Yii::app()->user->access()->hasPermission('politiques')) {
			throw new CHttpException(403, "Accès non autorisé");
		}
		$model = new PolitiqueSearch();
		$model->setAttributes($_GET['q'] ?? ['status' => Politique::STATUS_TOPUBLISH]);
		$this->getController()->render(
			'moderation',
			[
				'model' => $model,
			]
		);
	}
}
