<?php

namespace controllers\politique;

use Yii;

/**
 * Cf ticket #5249.
 *
 * Renvoie une URL d'accès en ligne pour un Titre dont :
 * - l'accès est en texte intégral,
 * - la ressource est Cairn
 * - une des collections n'est ni "Cairn International" ni "Cairn Ouvrages".
 */
class AjaxCairnUrlAction extends \CAction
{
	public function run($id): void
	{
		$titreId = (int) $id;
		$url = (string) Yii::app()->db
			->createCommand(<<<EOSQL
				SELECT s.url
				FROM Service s
				    JOIN Service_Collection sc ON sc.serviceId = s.id
				    JOIN Collection c ON sc.collectionId = c.id AND c.nom NOT IN ('Cairn International', 'Cairn Ouvrages')
				WHERE s.titreId = $titreId AND s.ressourceId = 3 AND s.type = 'Intégral'
				LIMIT 1
				EOSQL)
			->queryScalar();
		\Controller::header('json');
		echo json_encode($url);
	}
}
