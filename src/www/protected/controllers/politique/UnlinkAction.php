<?php

namespace controllers\politique;

use CHttpException;
use Politique;
use PolitiqueLog;
use Yii;

/**
 * Retire l'association à une politique, avec suppression de la politique si orpheline.
 */
class UnlinkAction extends \CAction
{
	public function run($titreId)
	{
		$politique = Politique::model()->findBySql("SELECT p.* FROM Politique p JOIN Politique_Titre pt ON p.id = pt.politiqueId WHERE pt.titreId = " . (int) $titreId);
		if ($politique === null) {
			$politique = Politique::model()->findBySql("SELECT * FROM Politique WHERE initialTitreId = " . (int) $titreId);
		}
		$this->checkAccess($politique);

		if ($politique->status === Politique::STATUS_PUBLISHED) {
			$log = new PolitiqueLog();
			$log->action = PolitiqueLog::ACTION_DELETE;
			$log->actionTime = $_SERVER['REQUEST_TIME'];
			$log->politiqueId = $politique->id;
			$log->editeurId = $politique->editeurId;
			$log->titreId = (int) $titreId;
			$log->save(false);
		}
		Yii::app()->db->createCommand("DELETE FROM Politique_Titre WHERE titreId = :tid")->execute([':tid' => (int) $titreId]);
		$editeurId = (int) $politique->editeurId;
		if (count($politique->getTitresAssigned()) === 0) {
			$politique->status = Politique::STATUS_TODELETE;
			$politique->save(false);
			Yii::app()->user->setFlash('success', "Politique détachée du titre et supprimée.");
		} else {
			Yii::app()->user->setFlash('success', "Politique détachée du titre.");
		}
		$this->getController()->redirect(['/politique', 'editeurId' => $editeurId]);
	}

	private function checkAccess(?Politique $politique): void
	{
		if (!Yii::app()->request->isPostRequest) {
			throw new CHttpException(400);
		}
		if ($politique === null) {
			throw new CHttpException(404, "Aucune politique n'est associée à ce titre.");
		}
		if (!Yii::app()->user->access()->toPolitique()->update($politique)) {
			throw new CHttpException(403);
		}
	}
}
