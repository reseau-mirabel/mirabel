<?php

namespace controllers\politique;

use CHttpException;
use models\forms\PolitiqueTitresForm;
use Politique;
use processes\politique\Publish;
use Yii;

class TitresAction extends \CAction
{
	public function run(string $id, string $mode = '')
	{
		$politique = $this->loadModel((int) $id);
		$politiqueTitresForm = new PolitiqueTitresForm($politique, (int) Yii::app()->user->id);

		$applyTo = Yii::app()->session->get('policy-applyTo', '');
		if ($applyTo) {
			// Preload the form with the session data sent by load-sherpa
			$politiqueTitresForm->load(['ids' => explode(',', $applyTo)]);
			Yii::app()->session->remove('policy-applyTo');
		} elseif (isset($_POST['pt'])) {
			$politiqueTitresForm->load($_POST['pt']);
			if ($politiqueTitresForm->save()) {
				if ($mode === 'transmission') {
					$userRole = Yii::app()->user->access()->toPolitique()->getRole((int) $politique->editeurId);
					$process = new Publish($politique, $userRole);
					$process->run(true);
					Yii::app()->user->setFlash('success', "Les données sont enregistrées et seront transmises à Open policy finder.");
					$this->getController()->redirect(['/politique/index', 'editeurId' => $politique->editeur->id]);
					return;
				}
				Yii::app()->user->setFlash('success', "Les données sont enregistrées.");
				$this->getController()->refresh();
				return;
			}
		}

		$this->getController()->render(
			'titres',
			[
				'politique' => $politique,
				'politiqueTitresForm' => $politiqueTitresForm,
				'mode' => $mode,
			]
		);
	}

	public function loadModel(int $id): Politique
	{
		$model = Politique::model()->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, "La politique demandée n'existe pas.");
		}
		return $model;
	}
}
