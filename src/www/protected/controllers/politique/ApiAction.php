<?php

namespace controllers\politique;

use Yii;
use components\openapi\Specification;
use processes\politique\ApiServer;

class ApiAction extends \CAction
{
	public function run(): void
	{
		$r = Yii::app()->getRequest();
		$pathpieces = $this->extractPath($r);

		if (!$pathpieces) {
			$file = Yii::getPathOfAlias('webroot') . '/files/politique-api/openapi.json';
			$openapi = new Specification(json_decode(file_get_contents($file)));
			$this->getController()->render('api', ['api' => $openapi]);
			return;
		}

		$matomo = Yii::app()->getComponent('matomo');
		assert($matomo instanceof \components\Matomo);
		$matomo->sendFromServer();

		$this->useReadonlyDbConnection();
		header('Content-Type: application/json; charset="UTF-8"');
		header('Access-Control-Allow-Origin: *');
		if (!$this->checkToken()) {
			http_response_code(401);
			echo json_encode(['error' => [
				'code' => 401,
				'message' => "No token or wrong token. Please ask for an access token through the contact form.",
			]]);
			return;
		}
		$action = array_shift($pathpieces);
		try {
			$api = new ApiServer($pathpieces, ['since' => $r->getQuery('since')]);
			$data = $api->run($action);
			echo json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
		} catch (\CHttpException $e) {
			http_response_code($e->statusCode ?: 500);
			echo json_encode(['error' => [
				'code' => ($e->statusCode ?: (int) $e->getCode()) ?: 500,
				'message' => $e->getMessage(),
			]]);
		} catch (\Throwable $e) {
			Yii::log($e->getMessage(), 'error', 'politique/api');
			http_response_code(500);
			echo json_encode(['error' => [
				'code' => 500,
				'message' => "internal error",
			]]);
		}
	}

	private function checkToken(): bool
	{
		$token = Yii::app()->getRequest()->getQuery('api_key', '');
		if (!$token && function_exists('apache_request_headers')) {
			$headers = apache_request_headers();
			$token = $headers['X-Api-Key'] ?? '';
		}
		return (bool) Yii::app()->db
			->createCommand("SELECT 1 FROM PolitiqueJeton WHERE token = :t")
			->queryScalar([':t' => $token]);
	}

	private function extractPath(\CHttpRequest $request): array
	{
		$pathinfo = explode('/', $request->getPathInfo());

		// remove "politique/api/"
		array_shift($pathinfo);
		array_shift($pathinfo);

		return $pathinfo;
	}

	private function useReadonlyDbConnection(): void
	{
		if (defined('YII_ENV') && YII_ENV === 'test') {
			return;
		}
		// use an distinct MySQL account if it exists
		$dbconfig = Yii::app()->params->itemAt('api.db');
		if ($dbconfig) {
			// merge the API specific config onto the default one
			Yii::app()->setComponent('db', $dbconfig, true);
			if (Yii::app()->db->isInitialized) {
				Yii::app()->setComponent('db', null);
			}
		}
	}
}
