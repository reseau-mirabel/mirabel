<?php

namespace controllers\politique;

use Yii;
use processes\politique\ApiServer;

class ApiTestAction extends \CAction
{
	public string $sqlitefile = "politique-test-1.sqlite";

	public function run(): void
	{
		$r = Yii::app()->getRequest();
		$pathpieces = $this->extractPath($r);

		if (!$pathpieces) {
			if (strpos($_SERVER['HTTP_ACCEPT'] ?? '', 'text/html') === false || ($_GET['format'] ?? '') === 'json') {
				$this->renderJson();
			} else {
				$this->renderHtml();
			}
			return;
		}

		header('Content-Type: application/json; charset="UTF-8"');

		$action = array_shift($pathpieces);
		try {
			$api = new ApiServer($pathpieces, ['since' => $r->getQuery('since')]);
			$api->setDb($this->getSqliteDb());
			$data = $api->run($action);
			echo json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
		} catch (\CHttpException $e) {
			http_response_code($e->statusCode ?: 500);
			echo json_encode(['error' => [
				'code' => ($e->statusCode ?: (int) $e->getCode()) ?: 500,
				'message' => $e->getMessage(),
			]]);
		} catch (\Throwable $e) {
			Yii::log($e->getMessage(), 'error', 'politique/api');
			http_response_code(500);
			echo json_encode(['error' => [
				'code' => 500,
				'message' => "internal error",
			]]);
		}
	}

	private function extractPath(\CHttpRequest $request): array
	{
		$pathinfo = explode('/', $request->getPathInfo());

		// remove "politique/api-test/"
		array_shift($pathinfo);
		array_shift($pathinfo);

		return $pathinfo;
	}

	private function getSqliteDb(): \CDbConnection
	{
		$path = Yii::getPathOfAlias('application');
		$db = new \CDbConnection("sqlite:$path/data/{$this->sqlitefile}");
		$db->enableProfiling = false;
		$db->emulatePrepare = true;
		$db->setAttribute(\PDO::ATTR_STRINGIFY_FETCHES, true); // Backward compatibility, required for PHP8.1+
		$db->setAttribute(\PDO::SQLITE_ATTR_OPEN_FLAGS, \PDO::SQLITE_OPEN_READONLY);
		return $db;
	}

	private function fetchRecords(): array
	{
		return $this->getSqliteDb()
			->createCommand(<<<EOSQL
				SELECT
					t.id AS publication_id,
					t.titre AS publication,
					e.id AS publisher_id,
					e.nom AS publisher,
					p.id AS policy_id,
					c.description
				FROM
					Titre t
					JOIN Titre_Editeur te ON te.titreId = t.id
					JOIN Editeur e ON te.editeurId = e.id
					LEFT JOIN Politique_Titre pt ON pt.titreId = t.id
					LEFT JOIN Politique p ON pt.politiqueId = p.id AND p.editeurId = e.id AND p.status IN ('published', 'updated')
					LEFT JOIN policy_use_case c ON t.id = c.publication_id AND e.id = c.publisher_id AND p.id = c.policy_id
				GROUP BY t.id, e.id
				ORDER BY t.titre, t.id
				EOSQL
			)
			->queryAll();
	}

	private function renderHtml(): void
	{
		$this->getController()->render('api-test', ['records' => $this->fetchRecords()]);
	}

	private function renderJson(): void
	{
		$response = [
			'publication' => [],
			'publisher' => [],
			'policy' => [],
		];
		$records = $this->fetchRecords();
		$baseUrl = Yii::app()->getBaseUrl(true);
		foreach ($records as $r) {
			$response['publication'][] = "$baseUrl/politique/api-test/publication/{$r['publication_id']}";
			$response['publisher'][] = "$baseUrl/politique/api-test/publisher/{$r['publisher_id']}";
			$response['policy'][] = "$baseUrl/politique/api-test/policy/{$r['policy_id']}";
		}

		\Controller::header('json');
		echo json_encode($response, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
	}
}
