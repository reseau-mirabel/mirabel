<?php

namespace controllers\politique;

use Politique;
use Titre;
use Yii;
use models\sherpa\Item;

/**
 * Lists the policies that a user can assign to a journal.
 */
class AjaxCandidatesAction extends \CAction
{
	public function run($editeurId = 0, $titreId = 0)
	{
		header('Content-Type: application/json; charset="UTF-8"');

		$policies = Politique::model()->findAll(
			"editeurId = :eid AND status IN (:stdraft, :sttopublish, :stpublished, :stupdated)",
			[
				':eid' => (int) $editeurId,
				':stdraft' => Politique::STATUS_DRAFT,
				':sttopublish' => Politique::STATUS_TOPUBLISH,
				':stpublished' => Politique::STATUS_PUBLISHED,
				':stupdated' => Politique::STATUS_UPDATED,
			]
		);
		$titre = Titre::model()->findBySql(
			"SELECT t.* FROM Titre t JOIN Titre_Editeur te ON te.titreId = t.id WHERE te.editeurId = :eid AND t.id = :tid",
			[':eid' => $editeurId, ':tid' => $titreId]
		);
		if (!$this->checkAccess((int) $editeurId, $titre)) {
			return;
		}

		if (!$titre->getLiens()->containsSource(31)) { // 31 = Open policy finder
			$sherpaItem = null;
		} else {
			$sherpaItem = Item::loadFromCache((int) $titre->id);
		}
		if (!$policies && !$sherpaItem) {
			echo json_encode([
				'status' => "warning",
				'message' => "Ce titre n'a pas de données sur sa politique de publication (source Open policy finder).",
			]);
			return;
		}
		echo json_encode([
			'status' => "success",
			'html' => $this->getController()->renderPartial(
				'_candidates',
				['policies' => $policies, 'sherpaItem' => $sherpaItem, 'titreId' => (int) $titreId],
				true
			),
		]);
	}

	private function checkAccess(int $editeurId, ?Titre $titre): bool
	{
		if (empty($editeurId)) {
			http_response_code(400);
			echo json_encode(['status' => "error", 'message' => "URL param 'editeurId' requis"]);
			return false;
		}
		if (!Yii::app()->user->access()->toPolitique()->assign($editeurId)) {
			http_response_code(403);
			echo json_encode(['status' => "error", 'message' => "Accès non autorisé"]);
			return false;
		}
		if ($titre === null) {
			http_response_code(404);
			echo json_encode(['status' => "error", 'message' => "Ce titre n'a pas été trouvé parmi les revues de l'éditeur."]);
			return false;
		}
		return true;
	}
}
