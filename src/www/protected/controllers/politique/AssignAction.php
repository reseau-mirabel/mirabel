<?php

namespace controllers\politique;

use CHttpException;
use models\forms\PolitiqueTitresForm;
use Politique;
use Yii;

class AssignAction extends \CAction
{
	public function run(string $politiqueId, string $titreId)
	{
		$politique = $this->loadModel((int) $politiqueId);

		// Check authorization
		new PolitiqueTitresForm($politique, (int) Yii::app()->user->id);

		$assigned = Yii::app()->db
			->createCommand("REPLACE INTO Politique_Titre (politiqueId, titreId, confirmed, createdOn) VALUES (:pid, :tid, 1, :now)")
			->execute([
				':pid' => $politique->id,
				':tid' => (int) $titreId,
				':now' => time(),
			]);
		if ($assigned) {
			if ($politique->status === Politique::STATUS_PENDING) {
				$politique->status = Politique::STATUS_DRAFT;
				$politique->save(false);
			}
			Yii::app()->user->setFlash('success', "La politique a été attachée au titre.");
		} else {
			Yii::app()->user->setFlash('error', "Erreur interne lors de l'affectation de la politique au titre.");
		}

		$this->getController()->redirect(['/politique', 'editeurId' => $politique->editeurId]);
	}

	public function loadModel(int $id): Politique
	{
		$model = Politique::model()->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, "La politique demandée n'existe pas.");
		}
		return $model;
	}
}
