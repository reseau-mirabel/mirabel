<?php

use components\HtmlHelper;

class EditeurController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'actions' => ['ajax-complete', 'ajax-has-policy-owner', 'carte', 'create', 'create-by-idref', 'create-final', 'export', 'export-idref', 'idref', 'index', 'pays', 'search', 'update', 'view'],
				'users' => ['*'],
			],
			[
				'allow',
				'actions' => ['admin', 'checkLinks', 'delete', 'verify'],
				'roles' => ['avec-partenaire'],
			],
			[
				'allow',
				'actions' => ['ajax-idref', 'create-partenaire'],
				'users' => ['@'],
			],
			[
				'deny',  // otherwise deny all users
				'users' => ['*'],
			],
		];
	}

	/**
	 * Maps actions to dedicated classes.
	 */
	public function actions()
	{
		return [
			'ajax-complete' => 'controllers\editeur\AjaxCompleteAction',
			'ajax-has-policy-owner' => 'controllers\editeur\AjaxHasPolicyOwnerAction',
			'ajax-idref' => 'controllers\editeur\AjaxIdrefAction',
			'carte' => 'controllers\editeur\CarteAction',
			'checkLinks' => 'controllers\editeur\CheckLinksAction',
			'create-final' => 'controllers\editeur\CreateAction',
			'create-by-idref' => 'controllers\editeur\CreateByIdrefAction',
			'create-partenaire' => 'controllers\editeur\CreatePartenaireAction',
			'delete' => 'controllers\editeur\DeleteAction',
			'export' => 'controllers\editeur\ExportAction',
			'export-idref' => 'controllers\editeur\ExportIdrefAction',
			'idref' => 'controllers\editeur\IdrefAction',
			'index' => 'controllers\editeur\IndexAction',
			'pays' => 'controllers\editeur\PaysAction',
			'search' => 'controllers\editeur\SearchAction',
			'update' => 'controllers\editeur\UpdateAction',
			'verify' => 'controllers\editeur\VerifyAction',
			'view' => 'controllers\editeur\ViewAction',
		];
	}

	public function beforeRender($view)
	{
		$directCreation = Yii::app()->user->access()->toEditeur()->createDirect();
		if (empty($_GET['id']) && !$directCreation) {
			if ($this->getAction()->id !== 'search') {
				$this->layout = '//layouts/column1';
			}
			return parent::beforeRender($view);
		}
		$sidebar = \Yii::app()->getComponent('sidebar');
		assert($sidebar instanceof \components\WebSidebar);
		$sidebar->menu = [
			['label' => 'Liste', 'url' => ['index']],
			['label' => 'Carte', 'url' => ['carte']],
			[
				'label' => 'Nouvel éditeur',
				'url' => ['create'],
				'visible' => $directCreation,
			],
		];
		if (!empty($_GET['id'])) {
			$model = self::loadModel($_GET['id']);
			$partenaire = $model->partenaire;
			array_push(
				$sidebar->menu,
				['label' => 'Cet éditeur', 'itemOptions' => ['class' => 'nav-header']],
				['label' => 'Vérifier les liens', 'url' => ['checkLinks', 'id' => $model->id]],
				[
					'label' => 'Modifier',
					'url' => ['update', 'id' => $model->id],
					'visible' => true, // direct or to-be-validated update
				],
				[
					'label' => "Supprimer",
					'url' => ['/editeur/delete', 'id' => $model->id],
					'visible' => Yii::app()->user->access()->toEditeur()->delete($model),
				]
			);
			if (empty($partenaire)) {
				$sidebar->menu[] = [
					'encodeLabel' => false,
					'label' => HtmlHelper::postButton(
						"Créer un partenariat",
						['create-partenaire', 'id' => $model->id],
						[],
						['class' => 'btn btn-link', 'style' => "padding: 0; white-space: normal;"]
					),
					'visible' => Yii::app()->user->checkAccess('suiviPartenairesEditeurs'),
				];
			} elseif ($partenaire->statut === 'actif') {
				$sidebar->menu[] = ['label' => 'Partenariat', 'itemOptions' => ['class' => 'nav-header']];
				$sidebar->menu[] = [
					'encodeLabel' => false,
					'label' => CHtml::link("Partenaire", ['partenaire/view-admin', 'id' => $partenaire->id]),
				];
			} else {
				$sidebar->menu[] = ['label' => 'Partenariat (inactif)', 'itemOptions' => ['class' => 'nav-header']];
				$sidebar->menu[] = [
					'encodeLabel' => false,
					'label' => CHtml::link("Réactiver", ['partenaire/update', 'id' => $partenaire->id]),
				];
			}
			if (Yii::app()->user->checkAccess('verify', $model)) {
				$sidebar->menu[] = ['label' => 'Vérification', 'itemOptions' => ['class' => 'nav-header']];
				$sidebar->menu[] = [
					'encodeLabel' => false,
					'label' => HtmlHelper::postButton(
						"Indiquer que l'éditeur a été vérifié",
						['verify', 'id' => $model->id],
						[],
						['class' => 'btn btn-primary btn-small', 'style' => "white-space: normal;"]
					),
				];
			}
		}
		return parent::beforeRender($view);
	}

	public function actionCreate()
	{
		$this->redirect('create-by-idref');
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 *
	 * If the data model is not found, an HTTP exception will be raised.
	 *
	 * @param string $id the ID of the model to be loaded
	 * @return Editeur
	 */
	public static function loadModel($id): Editeur
	{
		$model = Editeur::model()->findByPk((int) $id);
		if (!($model instanceof Editeur)) {
			throw new CHttpException(404, "L'éditeur demandé n'existe pas.");
		}
		$model->id = (int) $model->id;
		return $model;
	}
}
