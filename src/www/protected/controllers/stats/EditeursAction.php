<?php

namespace controllers\stats;

class EditeursAction extends \CAction
{
	public function run(): void
	{
		$this->getController()->render(
			'editeurs',
		);
	}
}
