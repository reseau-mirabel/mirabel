<?php

namespace controllers\stats;

use processes\stats\Suivi;

class SuiviAction extends \CAction
{
	public function run(): void
	{
		$this->getController()->render(
			'suivi',
			[
				'suivi' => new Suivi(),
			]
		);
	}
}
