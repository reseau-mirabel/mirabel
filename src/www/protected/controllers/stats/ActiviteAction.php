<?php

declare(strict_types=1);

namespace controllers\stats;

use CHttpException;
use models\forms\StatsForm;
use Partenaire;
use processes\stats\Activite;
use Yii;

class ActiviteAction extends \CAction
{
	public function run(string $partenaireId)
	{
		$userData = new StatsForm();

		if (isset($_GET['filter'])) {
			$userData->attributes = $_GET['filter'];
			$userData->validate();
		} else {
			$userData->initDates();
		}

		$statsActivite = new Activite(Yii::app()->db);
		$statsActivite->partenaire = Partenaire::model()->findByPk($partenaireId);
		if (!$userData->errors) {
			$statsActivite->setInterval($userData->startTs, $userData->endTs);
		}

		if (!$statsActivite->partenaire) {
			throw new CHttpException(404, "Ce partenaire n'a pas été trouvé.");
		}
		if (!Yii::app()->user->checkAccess('stats/partenaire', ['Partenaire' => $statsActivite->partenaire])) {
			throw new CHttpException(403, "Permission refusée");
		}

		$this->getController()->render(
			'activite',
			[
				'statsActivite' => $statsActivite,
				'userData' => $userData,
			]
		);
	}
}
