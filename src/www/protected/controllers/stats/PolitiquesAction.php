<?php

namespace controllers\stats;

use processes\stats\PolitiqueEditeurs;
use processes\stats\PolitiqueUtilisateurs;

class PolitiquesAction extends \CAction
{
	public function run(): void
	{
		$this->getController()->render(
			'politiques',
			[
				'editeurs' => new PolitiqueEditeurs(),
				'utilisateurs' => new PolitiqueUtilisateurs(),
			],
		);
	}
}
