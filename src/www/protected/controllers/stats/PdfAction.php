<?php

namespace controllers\stats;

use CHttpException;
use Controller;
use Yii;

class PdfAction extends \CAction
{
	public function run(string $file)
	{
		if (!preg_match('/^[A-Za-z0-9_.@-]+$/', $file)) {
			throw new CHttpException(401, 'Invalid file name');
		}
		$base = realpath(Yii::getPathOfAlias('application.views.stats'));
		$pdf = $base . '/history/' . $file;
		if (!file_exists($pdf)) {
			throw new CHttpException(404, 'PDF not found');
		}
		$pdf = realpath($pdf);
		if (!preg_match('/\.pdf$/', $pdf)) {
			throw new CHttpException(404, 'PDF not found or not allowed');
		}

		Controller::header('pdf');
		header('Content-Disposition: inline;filename=' . $file);
		readfile($pdf);
	}
}
