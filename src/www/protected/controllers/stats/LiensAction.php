<?php

declare(strict_types=1);

namespace controllers\stats;

use CHtml;
use CHttpException;
use Sourcelien;

class LiensAction extends \CAction
{
	public function run(string $domain = '', string $name = '')
	{
		if ($domain) {
			$liensTitre = Sourcelien::listByDomain("Titre", $domain);
			$liensEditeur = Sourcelien::listByDomain("Editeur", $domain);
			$pattern = "%d liens du domaine";
			$subject = $domain;
		} elseif ($name) {
			$liensTitre = Sourcelien::listByName("Titre", $name);
			$liensEditeur = Sourcelien::listByName("Editeur", $name);
			$pattern = "%d liens ayant le nom";
			$subject = $name;
		} else {
			throw new CHttpException(400, "URL non valide. Un argument requis est absent.");
		}
		$titrePage = "Dans Mir@bel, "
			. sprintf($pattern, count($liensTitre) + count($liensEditeur))
			. " <em>" . CHtml::encode($subject) . "</em>";
		$this->getController()->render(
			'liens-et-titres',
			[
				'liensTitre' => $liensTitre,
				'liensEditeur' => $liensEditeur,
				'titrePage' => $titrePage,
			]
		);
	}
}
