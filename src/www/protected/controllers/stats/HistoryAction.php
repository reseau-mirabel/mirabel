<?php

namespace controllers\stats;

use Yii;

class HistoryAction extends \CAction
{
	public function run(): void
	{
		$pdfs = array_map(
			'basename',
			glob(Yii::getPathOfAlias('application.views.stats') . '/history/*.pdf')
		);
		sort($pdfs);

		$this->getController()->render(
			'history',
			['pdfs' => $pdfs]
		);
	}
}
