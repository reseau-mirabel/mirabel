<?php

namespace controllers\stats;

use models\forms\StatsForm;
use processes\stats\Activite;
use Yii;

class InterventionsAction extends \CAction
{
	public function run(): void
	{
		$statsActivite = new Activite(Yii::app()->db);

		$userData = new StatsForm();
		if (isset($_GET['filter'])) {
			$userData->setAttributes($_GET['filter']);
			$userData->validate();
		} else {
			$userData->initDates();
		}
		if (!$userData->errors) {
			$statsActivite->setInterval($userData->startTs, $userData->endTs);
		}

		$this->getController()->render(
			'interventions',
			[
				'statsActivite' => $statsActivite,
				'userData' => $userData,
			]
		);
	}
}
