<?php

namespace controllers\stats;

use processes\stats\LiensScienceOuverte;
use processes\stats\scienceouverte\StatsAttributParPays;
use Sourcelien;
use Yii;

class ScienceOuverteAction extends \CAction
{
	public function run(): void
	{
		$doaj = Sourcelien::model()->findByAttributes(['nomcourt' => 'doaj']);
		$liensSO = new LiensScienceOuverte(Yii::app()->db, [(int) $doaj->id]);

		$this->getController()->render(
			'science-ouverte',
			[
				'liens' => $liensSO,
				'ratio' => $liensSO->getFrequenciesTitresRatio(),
				'paysDoaj' => $liensSO->listFrequenciesCountry((int) $doaj->id),
				'paysDoajSceau' => self::readFrequency('doaj-sceau', ['Yes']),
				'paysDoajSansApc' => self::readFrequency('apc-sans-doaj', ["0", "NO", "No", "no"]),
				'numsDoajFrance' => self::countValues('doaj-france', 'France'),
			]
		);
	}

	private static function readFrequency(string $attr, array $values): array
	{
		try {
			$stats = new StatsAttributParPays($attr, $values);
			return $stats->listFrequencies();
		} catch (\Throwable $e) {
			Yii::app()->user->setFlash('error', $e->getMessage());
			return [];
		}
	}

	private static function countValues(string $attrName, string $value): array
	{
		$attr = \Sourceattribut::model()->findByAttributes(['identifiant' => $attrName]);
		if (!($attr instanceof \Sourceattribut)) {
			Yii::app()->user->setFlash('error', "L'attribut de titre nommé '$attrName' n'existe pas.");
			return [];
		}
		$sql = <<<'EOSQL'
			WITH attrvalues AS (
				SELECT t.id, t.revueId
				FROM AttributTitre a
					JOIN Titre t ON t.id = a.titreId
				WHERE a.sourceattributId = :attrId AND a.valeur = :value
			)
			SELECT
				count(DISTINCT id) AS titres,
				count(DISTINCT revueId) AS revues
			FROM attrvalues
			EOSQL;
		return Yii::app()->db
			->createCommand($sql)
			->queryRow(true, [':attrId' => (int) $attr->id, ':value' => $value]);
	}
}
