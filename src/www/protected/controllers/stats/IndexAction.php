<?php

namespace controllers\stats;

use models\forms\StatsForm;
use processes\stats\Activite;
use processes\stats\Contenu;
use processes\stats\Liens;
use Yii;

class IndexAction extends \CAction
{
	public function run(): void
	{
		$statsActivite = new Activite(Yii::app()->db);

		$userData = new StatsForm();
		if (isset($_GET['filter'])) {
			$userData->setAttributes($_GET['filter']);
			$userData->validate();
		} else {
			$userData->initDates();
		}
		if (!$userData->errors) {
			$statsActivite->setInterval($userData->startTs, $userData->endTs);
		}
		$stats = new Liens(Yii::app()->db);
		$stats->run();

		$this->getController()->render(
			'index',
			[
				'stats' => new Contenu(Yii::app()->db),
				'statsActivite' => $statsActivite,
				'userData' => $userData,
				'liens' => $stats,
			]
		);
	}
}
