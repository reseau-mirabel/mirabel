<?php

namespace controllers\partenaire;

use Partenaire;
use processes\grappe\Selection;
use processes\partenaire\GrappeAffichage;
use processes\partenaire\GrappesSelection;
use Yii;

class GrappesSelectionAction extends \CAction
{
	public function run(int $id = 0)
	{
		if (!$id) {
			$id = (int) Yii::app()->user->partenaireId;
		}
		$partenaire = self::findPartenaire($id);
		$permissions = new \processes\grappe\Permissions(Yii::app()->user);
		if (!$permissions->canCreate()) {
			throw new \CHttpException(403);
		}

		$formData = new GrappesSelection($partenaire);

		// POST for saving selected elements to the selection?
		$select = \Yii::app()->request->getPost('GrappesSelection', []);
		if ($formData->load($select) && $formData->addGrappesToSelection()) {
			$count = count($formData->add);
			Yii::app()->user->setFlash('success', "Ces {$count} grappes ont été ajoutées à votre sélection.");
			$this->getController()->redirect(['/partenaire/grappes-affichage', 'id' => $partenaire->id]);
			return;
		}
		// GET to search some elements and display them in a list?
		$search = \Yii::app()->request->getQuery('GrappesSelection', []);
		if (isset($search['nom'])) {
			$formData->load($search);
		}

		$selection = new Selection($partenaire, \Yii::app()->user);

		$this->getController()->render(
			'/partenaire/grappes-selection',
			[
				'formData' => $formData,
				'partenaire' => $partenaire,
				'hasGrappesList' => $selection->hasGrappesInList(GrappeAffichage::PAGE_PARTENAIRE),
				'hasGrappesPage' => $selection->hasGrappesInList(GrappeAffichage::PAGE_SELECTION),
			]
		);
	}

	private function findPartenaire(int $id): Partenaire
	{
		$partenaire = Partenaire::model()->findByPk($id);
		if (!($partenaire instanceof Partenaire)) {
			throw new \CHttpException(404);
		}
		if (!Yii::app()->user->access()->toPartenaire()->update($partenaire)) {
			throw new \CHttpException(403);
		}
		return $partenaire;
	}
}
