<?php

namespace controllers\partenaire;

use Partenaire;

class AdminAction extends \CAction
{
	public function run(): void
	{
		$model = new Partenaire('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Partenaire'])) {
			$model->setAttributes($_GET['Partenaire']);
		}

		$this->getController()->render(
			'admin',
			['model' => $model]
		);
	}
}
