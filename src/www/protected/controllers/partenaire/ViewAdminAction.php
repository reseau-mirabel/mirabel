<?php

namespace controllers\partenaire;

use AbonnementInfo;
use Partenaire;
use processes\grappe\Selection;
use processes\partenaire\GrappeAffichage;
use Yii;

class ViewAdminAction extends \CAction
{
	public const MAX_ACTUS = 3;

	public function run(string $id)
	{
		$controller = $this->getController();
		assert($controller instanceof \Controller);

		$model = \PartenaireController::loadModel($id);

		$selectionGrappes = new Selection($model, \Yii::app()->user);

		$isStandardUser = Yii::app()->user->checkAccess("avec-partenaire");

		\Yii::app()->getComponent('sidebar')->menu = self::buildSidebarMenu($model);
		$controller->render(
			'view-admin',
			[
				'model' => $model,
				'logoImg' => $model->getLogoImg(),
				'abonnementInfo' => $isStandardUser ? new AbonnementInfo($model->id) : null,
				'uploadedFiles' => $model->listConventionFiles(),
				'actus' => self::getActus($model->id),
				'numGrappes' => (int) \Grappe::model()->count("partenaireId = {$model->id}"),
				/** @todo remplacer count(getList()) par countList(), lequel se substitue à hasList() */
				'numGrappesPagePart' => count($selectionGrappes->getVisibleGrappes(GrappeAffichage::PAGE_PARTENAIRE)),
				'numGrappesPageSel' => count($selectionGrappes->getVisibleGrappes(GrappeAffichage::PAGE_SELECTION)),
			]
		);
	}

	public static function getActus(int $id): array
	{
		$actu = new \models\cms\BreveSearchForm();
		$actu->partenaireId = $id;
		$actuProvider = $actu->search();
		$actuProvider->getCriteria()->limit = self::MAX_ACTUS + 1;
		return $actuProvider->getData();
	}

	private static function buildSidebarMenu(Partenaire $p): array
	{
		if (!Yii::app()->user->access()->toPartenaire()->suivi($p)) {
			return [];
		}
		$permAdminGlobal = Yii::app()->user->checkAccess('admin');
		$permModif = Yii::app()->user->access()->toPartenaire()->update($p);
		$permView = Yii::app()->user->access()->toPartenaire()->view($p);
		$menu = [
			['label' => 'Liste des partenaires', 'url' => ['index']],
			['label' => 'Administration des partenaires', 'url' => ['admin'], 'visible' => $permAdminGlobal],
			['label' => 'Créer un partenaire', 'url' => ['create'], 'visible' => $permAdminGlobal],
			['label' => ''],
			['label' => 'Ce partenaire', 'itemOptions' => ['class' => 'nav-header']],
			['label' => "Stats d'activité", 'url' => ['/stats/activite', 'partenaireId' => $p->id], 'visible' => $permView],
			['label' => 'Modifier', 'url' => ['update', 'id' => $p->id], 'visible' => $permModif],
			[
				'label' => 'Déposer un logo',
				'url' => ['logo', 'id' => $p->id],
				'visible' => $permModif,
			],
			[
				'label' => 'Gérer les personnalisations',
				'url' => ['personnaliser', 'id' => $p->id],
				'visible' => $permModif && !$p->editeurId,
			],
			[
				'label' => \components\HtmlHelper::postButton(
					"Supprimer",
					['delete', 'id' => $p->id],
					[],
					[
						'class' => "btn btn-link",
						'style' => "padding: 0",
						'onclick' => 'return confirm("Êtes-vous certain de vouloir supprimer cet partenaire ?")'
							. "\nToutes ses relations de possession et de suivi seront supprimées définitivement.",
					]
				),
				'encodeLabel' => false,
				'visible' => $permAdminGlobal,
			],
			[
				'label' => 'Créer un utilisateur',
				'url' => ['/utilisateur/create', 'partenaireId' => $p->id],
				'visible' => Yii::app()->user->access()->toUtilisateur()->create($p),
			],
		];
		if ($p->type === 'editeur') {
			array_push(
				$menu,
				['label' => ''],
				['label' => 'Éditeur', 'itemOptions' => ['class' => 'nav-header']],
				['label' => $p->editeur->sigle ?: $p->sigle ?: $p->editeur->nom, 'url' => ['/editeur/view', 'id' => $p->editeurId]],
				[
					'label' => 'Modifier',
					'url' => ['/editeur/update', 'id' => $p->editeurId],
					'visible' => $permModif,
				],
			);
		}
		if ($p->type !== 'import') {
			$canMonitor = Yii::app()->user->access()->toPartenaire()->suivi($p);
			array_push(
				$menu,
				['label' => ''],
				['label' => 'Suivi', 'itemOptions' => ['class' => 'nav-header']],
				['label' => "Tableau de bord", 'url' => ['tableau-de-bord', 'id' => $p->id]],
				[
					'label' => 'Revues',
					'url' => ['suivi', 'id' => $p->id, 'target' => 'Revue'],
					'visible' => $canMonitor,
				],
				[
					'label' => 'Ressources',
					'url' => ['suivi', 'id' => $p->id, 'target' => 'Ressource'],
					'visible' => $canMonitor,
				],
				[
					'label' => 'Éditeurs',
					'url' => ['suivi', 'id' => $p->id, 'target' => 'Editeur'],
					'visible' => ($p->type === 'editeur'),
				],
			);
		}
		if ($permModif && (new \processes\grappe\Permissions(Yii::app()->user))->canCreate()) {
			array_push(
				$menu,
				[
					'label' => 'Grappes',
					'itemOptions' => ['class' => 'nav-header']
				],
				[
					'label' => 'Administrer mes grappes',
					'url' => ['/grappe/partenaire', 'id' => $p->id],
				],
				[
					'label' => 'Créer une grappe',
					'url' => ['/grappe/create'],
				],
				[
					'label' => "Paramètres d'affichage",
					'url' => ['/partenaire/grappes-affichage', 'id' => $p->id],
				],
			);
		}

		if ($p->canOwnTitles() && Yii::app()->user->checkAccess('possession/update', ['Partenaire' => $p])) {
			$hasPossessions = (bool) Yii::app()->db
				->createCommand("SELECT 1 FROM Partenaire_Titre WHERE partenaireId = :id LIMIT 1")
				->queryScalar([':id' => $p->id]);
			array_push(
				$menu,
				['label' => ''],
				['label' => 'Possessions', 'itemOptions' => ['class' => 'nav-header']],
				['label' => 'Gestion des titres', 'url' => ['possession', 'id' => $p->id]],
				['label' => 'Import CSV', 'url' => ['/possession/import', 'partenaireId' => $p->id]],
				['label' => 'Export CSV', 'url' => ['/possession/export', 'partenaireId' => $p->id]],
				['label' => "Export CSV détaillé", 'url' => ['/possession/export-more', 'partenaireId' => $p->id]],
				['label' => "Vérifier les liens", 'url' => ['/verification/liens-revues', 'VerifUrlForm' => ['possessions' => $p->id]], 'visible' => $hasPossessions],
			);
		}
		if ($p->canOwnTitles() && Yii::app()->user->checkAccess('abonnement/update', $p)) {
			array_push(
				$menu,
				['label' => 'Abonnements', 'itemOptions' => ['class' => 'nav-header']],
				['label' => 'Gestion des abonnements', 'url' =>  ['abonnements', 'id' => $p->id]],
			);
		}
		return $menu;
	}
}
