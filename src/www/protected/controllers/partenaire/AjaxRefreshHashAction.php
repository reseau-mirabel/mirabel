<?php

namespace controllers\partenaire;

use CHtml;
use CHttpException;
use Controller;
use Partenaire;
use Yii;

class AjaxRefreshHashAction extends \CAction
{
	public function run(string $id)
	{
		$partenaire = \PartenaireController::loadModel($id);
		$permModif = Yii::app()->user->access()->toPartenaire()->update($partenaire);
		if (!$permModif) {
			throw new CHttpException(403, "Vous n'avez pas la permission de modifier ce partenaire.");
		}
		$partenaire->hash = Partenaire::getRandomHash();

		Controller::header("text");
		if ($partenaire->save(false)) {
			echo CHtml::encode($partenaire->hash);
		} else {
			echo "[erreur]";
		}
	}
}
