<?php

namespace controllers\partenaire;

use CUploadedFile;
use processes\images\Downloader as ImageDownloader;
use processes\images\Resizer;
use processes\upload\UploadDestination;
use Upload;
use Yii;

/**
 * Gère 3 cas provenant de la même page web :
 * 1. 'delete=1' ou 'Partenaire[logoUrl]=' => supprime les logos et vide le champ logoUrl
 * 2. 'Partenaire[logoUrl]=http...' => télécharge via la classe ImageDownloader
 * 3. 'Upload[file]=...' => enregistre via la classe Upload
 *
 * Dans les 2 derniers cas Resizer publie la version redimensionnée de l'image brute.
 */
class LogoAction extends \CAction
{
	public function run(string $id)
	{
		/** @var \PartenaireController $controller */
		$controller = $this->getController();

		$model = \PartenaireController::loadModel((int) $id);

		$upload = new Upload(UploadDestination::logoPartenaire());
		$upload->overwrite = true;
		$newImage = false;

		$imageTool = new ImageDownloader(DATA_DIR . '/public/images/partenaires', DATA_DIR . '/images/partenaires');
		$imageTool->filePattern = '%03d';

		// remove the logo
		if (!empty($_POST['delete'])) {
			$imageTool->delete($model->id);
			Yii::app()->user->setFlash('success', "Le logo a été supprimé.");
			Yii::app()->session->add('force-refresh', true);
			$controller->redirect(['partenaire/view', 'id' => $model->id]);
			return;
		}

		if (!empty($_POST['Partenaire'])) {
			$model->logoUrl = $_POST['Partenaire']['logoUrl'];
			$newImage = $this->download($imageTool, $model);
			if (Yii::app()->hasEnded()) {
				return;
			}
		}

		if (!empty($_POST['Upload'])) {
			$upload->setAttributes($_POST['Upload']);
			$upload->file = CUploadedFile::getInstance($upload, 'file');
			$newImage = self::upload($upload, $imageTool, $model);
		}

		if ($newImage) {
			$resizer = new Resizer(DATA_DIR . '/public/images/partenaires', DATA_DIR . '/images/partenaires');
			$resizer->filePattern = '%03d';
			$resizer->boundingbox = '450x110>';
			if ($resizer->resizeByIdentifier($model->id)) {
				Yii::app()->user->setFlash('success', "Le logo a été mis à jour.");
				Yii::app()->session->add('force-refresh', true);
				$controller->redirect(['partenaire/view', 'id' => $model->id]);
				return;
			}
			Yii::app()->user->setFlash('error', "Erreur lors du redimensionnement de l'image.");
		}

		$controller->render(
			'logo',
			[
				'model' => $model,
				'upload' => $upload,
			]
		);
	}

	private function download(ImageDownloader $imageTool, \Partenaire $model): bool
	{
		if (!$model->validate(['logoUrl'])) {
			return false;
		}
		if ($model->logoUrl === '') {
			$model->save(false);
			$imageTool->delete($model->id);
			Yii::app()->user->setFlash('success', "Le logo a été supprimé.");
			Yii::app()->session->add('force-refresh', true);
			$this->getController()->redirect(['partenaire/view', 'id' => $model->id]);
			return false;
		}
		$imageTool->download($model->id, $model->logoUrl);
		$filename = glob($imageTool->getRawImageFileName($model->id) . ".*")[0] ?? '';
		if (self::getExtension($filename) === '') {
			// Not an image
			return false;
		}
		$model->save(false);
		return true;
	}

	private static function upload(Upload $upload, ImageDownloader $imageTool, \Partenaire $model): bool
	{
		if (!$upload->file) {
			Yii::app()->user->setFlash('error', "Erreur lors du dépôt de ce fichier.");
			return false;
		}
		$imageTool->delete($model->id);
		$extension = self::getExtension($upload->file->getTempName());
		if ($extension === '') {
			return false;
		}
		$filename = $imageTool->getRawImageFileName($model->id) . ".$extension";
		if ($upload->file->saveAs($filename) || rename($upload->file->getTempName(), $filename)) {
			// M#3422 remove the logoUrl when a upload superseeds it
			$model->logoUrl = '';
			$model->save(false);
			return true;
		}
		Yii::app()->user->setFlash('error', "Erreur lors de la copie de ce fichier sur le serveur.");
		return false;
	}

	/**
	 * Guess the file extension ("jpg" or "png"), or "" if it's not a valid image.
	 */
	private static function getExtension(string $file): string
	{
		$extension = \CFileHelper::getExtensionByMimeType($file);
		if (!$extension) {
			Yii::app()->user->setFlash('error', "Le format de l'image n'a put être identifiée. Le logo doit être au format JPEG ou PNG.");
			return "";
		}
		if (!in_array($extension, ['jpeg', 'jpg', 'png'])) {
			Yii::app()->user->setFlash('error', "Le logo doit être au format JPEG ou PNG.");
			return "";
		}
		return $extension;
	}
}
