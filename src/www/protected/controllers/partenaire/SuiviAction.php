<?php

namespace controllers\partenaire;

use CHttpException;
use processes\partenaire\Follow;
use Suivi;
use Yii;

/**
 * Updates the records in the table Suivi.
 */
class SuiviAction extends \CAction
{
	/**
	 * @param string $id Partenaire.id
	 * @param string $target Among "Revue", "Ressource", "Editeur".
	 */
	public function run(string $id, string $target)
	{
		$partenaire = \PartenaireController::loadModel($id);
		if (!in_array($target, ['Revue', 'Ressource', 'Editeur'])) {
			throw new CHttpException(404, "L'argument 'target' n'est pas valide.");
		}
		if (!Yii::app()->user->access()->toPartenaire()->suivi($partenaire)) {
			throw new CHttpException(403, "Vous n'avez pas la permission de modifier les suivis.");
		}

		$controller = $this->getController();
		assert($controller instanceof \Controller);

		$process = new Follow($partenaire, $target);

		if (Yii::app()->user->partenaireType === 'editeur' && $process->addToPartenaireEditeur($_POST['Suivi'] ?? [])) {
			if ($partenaire->id === (int) Yii::app()->user->partenaireId) {
				// refresh the "suivi" in the current session
				Yii::app()->user->setState('suivi', $partenaire->getRightsSuivi());
			}
			return $controller->flashAndRedirect($process->getRedirection());
		}

		if ($process->updateExisting($_POST['existing'] ?? []) || $process->add($_POST['Suivi'] ?? [])) {
			if ($partenaire->id === (int) Yii::app()->user->partenaireId) {
				// refresh the "suivi" in the current session
				Yii::app()->user->setState('suivi', $partenaire->getRightsSuivi());
			}
			return $controller->flashAndRedirect($process->getRedirection());
		}

		$controller->render(
			'suivi',
			[
				'process' => $process,
				'duplicates' => Suivi::findDuplicates($partenaire->id, $target),
			]
		);
	}
}
