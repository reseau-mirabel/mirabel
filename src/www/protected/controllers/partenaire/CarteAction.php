<?php

namespace controllers\partenaire;

use CJavaScriptExpression;
use Partenaire;

class CarteAction extends \CAction
{
	public function run(string $stamen = '')
	{
		if (!preg_match('/^[\w-]+$/', $stamen)) {
			$stamen = '';
		}

		/** @var Partenaire[] */
		$partenaires = Partenaire::model()->findAllBySql(
			"SELECT * FROM Partenaire WHERE statut = 'actif' AND latitude IS NOT NULL AND latitude <> 0"
		);
		$places = [];
		foreach ($partenaires as $p) {
			$geo = $p->getSelfLink() . "<br />";
			if ($p->type ==='editeur') {
				$geo .= 'Éditeur<br />';
			} elseif ($p->type ==='import') {
				$geo .= 'Import<br />';
			}
			$places[] = [
				(double) $p->latitude,
				(double) $p->longitude,
				$geo,
				$p->type,
			];
		}
		$this->getController()->render(
			'carte',
			[
				'partenaires' => Partenaire::getListByType(),
				'stamen' => $stamen,
				'mapConfig' => [
					'countries' => [], // $countries,
					'countriesMax' => 0, // $countries ? max($countries) : 0,
					'markers' => $places,
					'radius' => [
						'max' => 200000, // meters
						'method' => 'linear',
					],
					'popupMessage' => new CJavaScriptExpression('function(x) { return x + " partenaires"; }'),
				],
			]
		);
	}
}
