<?php

namespace controllers\partenaire;

use processes\grappe\Selection;
use processes\partenaire\GrappeAffichage;
use Yii;

class GrappesAffichageAction extends \CAction
{
	public function run(string $id)
	{
		$partenaire = self::findPartenaire((int) $id);
		$formData = GrappeAffichage::loadFromDb($partenaire);

		if (self::save($formData, (int) $partenaire->id)) {
			Yii::app()->user->setFlash('success', "Modification enregistrées.");
			$this->controller->refresh();
			return;
		}

		$selection = new Selection($partenaire, \Yii::app()->user);

		$this->getController()->render(
			'grappes-affichage',
			[
				'formData' => $formData,
				'partenaire' => $partenaire,
				'hasGrappesList' => $selection->hasGrappesInList(GrappeAffichage::PAGE_PARTENAIRE),
				'hasGrappesPage' => $selection->hasGrappesInList(GrappeAffichage::PAGE_SELECTION),
			]
		);
	}

	private function findPartenaire(int $id): \Partenaire
	{
		$partenaire = \Partenaire::model()->findByPk($id);
		if (!($partenaire instanceof \Partenaire)) {
			throw new \CHttpException(404);
		}
		if (!Yii::app()->user->access()->toPartenaire()->update($partenaire)) {
			throw new \CHttpException(403);
		}
		return $partenaire;
	}

	/**
	 * @param GrappeAffichage[] $records
	 */
	private static function save(array $records, int $partenaireId): bool
	{
		if (!isset($_POST['GrappeAffichage'])) {
			return false;
		}
		$valid = true;
		$transaction = \Yii::app()->db->beginTransaction();
		$position = 0;
		foreach ($_POST['GrappeAffichage'] as $id => $input) {
			$position++;
			if (isset($records[$id])) {
				$valid = self::updateDiffusion($records[$id], $input, $position) && $valid;
			}
		}
		if (!$valid) {
			Yii::app()->user->setFlash('erreur', "Erreur lors de l'enregistrement. Modifications annulées.");
			$transaction->rollback();
			return false;
		}
		$transaction->commit();
		return true;
	}

	private static function updateDiffusion(GrappeAffichage $relation, array $input, int $position): bool
	{
		$relation->setAttributes($input);
		if (!$relation->validate()) {
			return false;
		}
		$relation->write($position);
		return true;
	}
}
