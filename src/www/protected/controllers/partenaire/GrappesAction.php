<?php

namespace controllers\partenaire;

use processes\grappe\Selection;
use processes\partenaire\GrappeAffichage;

class GrappesAction extends \CAction
{
	public function run(string $id)
	{
		$partenaire = self::findPartenaire((int) $id);
		$selection = new Selection($partenaire, \Yii::app()->user);
		$this->getController()->render(
			'grappes',
			[
				'grappes' => $selection->getVisibleGrappes(GrappeAffichage::PAGE_SELECTION),
				'partenaire' => $partenaire,
			]
		);
	}

	private function findPartenaire(int $id): \Partenaire
	{
		$partenaire = \Partenaire::model()->findByPk($id);
		if (!($partenaire instanceof \Partenaire)) {
			throw new \CHttpException(404);
		}
		return $partenaire;
	}
}
