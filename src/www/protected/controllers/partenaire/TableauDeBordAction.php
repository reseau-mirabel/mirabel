<?php

namespace controllers\partenaire;

use PartenaireController;
use processes\intervention\HtmlHelper;
use processes\titres\TrierTitre;
use RessourceSearch;
use RevueSearch;
use Yii;

class TableauDeBordAction extends \CAction
{
	public function run(string $id)
	{
		$partenaire = PartenaireController::loadModel($id);
		if (!Yii::app()->user->access()->toPartenaire()->view($partenaire)) {
			throw new \CHttpException(403);
		}

		$revueSearch = new RevueSearch();
		$revueSearch->setAttributes(Yii::app()->request->getParam("RevueSearch", []));
		$revueSearch->partenaireId = $partenaire->id;

		$ressourceSearch = new RessourceSearch('search');
		$ressourceSearch->setAttributes(Yii::app()->request->getParam("RessourceSearch", []));
		$ressourceSearch->partenaireId = $partenaire->id;

		$controller = $this->getController();
		$controller->layout = '//layouts/column1';
		$controller->render(
			'tableau-de-bord',
			[
				'partenaire' => $partenaire,
				'suiviRevues' => $revueSearch,
				'suiviRessources' => $ressourceSearch,
				'revueIdWithOrderProblem' => TrierTitre::getRevueIdWithObsoleteParProblemsByPartenaire($partenaire),
				'intvByCategory' => [
					'En attente' => HtmlHelper::listInterventionsSuivi($partenaire->id, 'attente'),
					'Validées' => HtmlHelper::listInterventionsSuivi($partenaire->id, 'accepté', (int) strtotime("1 month ago")),
				],
				'shares' => Yii::app()->db
					->createCommand("SELECT DISTINCT s1.cible FROM Suivi s1 JOIN Suivi s2 ON s1.cibleId = s2.cibleId AND s1.cible = s2.cible WHERE s1.partenaireId <> s2.partenaireId AND s1.partenaireId = :pid")
					->queryColumn([':pid' => $partenaire->id]),
			]
		);
	}
}
