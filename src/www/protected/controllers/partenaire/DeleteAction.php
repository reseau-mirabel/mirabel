<?php

namespace controllers\partenaire;

use CHttpException;
use Yii;

class DeleteAction extends \CAction
{
	public function run(string $id)
	{
		if (!Yii::app()->request->isPostRequest) {
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}

		$partenaire = \PartenaireController::loadModel($id);
		if ($partenaire->utilisateurs) {
			Yii::app()->user->setFlash('error', "Ce partenaire a des utilisateurs, la suppression est impossible.");
			$this->getController()->redirect(['view', 'id' => $partenaire->id]);
			return;
		}

		if ($partenaire->delete()) {
			Yii::app()->user->setFlash('success', "Partenaire supprimé");
		} else {
			Yii::app()->user->setFlash('error', "Erreur lors de la suppression");
		}
		if (Yii::app()->user->checkAccess('admin')) {
			$this->getController()->redirect(['/partenaire/admin']);
			return;
		}
		$this->getController()->redirect(['/partenaire/index']);
	}
}
