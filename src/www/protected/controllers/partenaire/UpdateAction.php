<?php

namespace controllers\partenaire;

use Abonnement;
use CHttpException;
use Yii;

class UpdateAction extends \CAction
{
	public function run(string $id)
	{
		$model = \PartenaireController::loadModel($id);
		if (!Yii::app()->user->access()->toPartenaire()->update($model)) {
			throw new CHttpException(403, 'Vous ne pouvez pas modifier ce partenaire.');
		}

		if (isset($_POST['Partenaire'])) {
			$oldAttributes = $model->attributes;
			$model->setAttributes($_POST['Partenaire']);
			if ($model->save()) {
				$message = "Le partenaire a été modifié.";
				if (empty($oldAttributes['proxyUrl']) && $model->proxyUrl) {
					$num = Abonnement::model()->updateAll(['proxy' => 1], "mask = 1 AND partenaireId = {$model->id}");
					if ($num) {
						$message .= " $num abonnements actuels sont configurés pour utiliser le proxy.";
					}
				}
				if ($oldAttributes['proxyUrl'] && empty($model->proxyUrl)) {
					$num = Abonnement::model()->updateAll(['proxy' => 0], "mask = 1 AND partenaireId = {$model->id}");
					if ($num) {
						$message .= " $num abonnements actuels sont configurés pour ne plus utiliser de proxy.";
					}
				}
				Yii::app()->user->setFlash('success', $message);
				if (Yii::app()->user->partenaireId == $model->id) {
					// update the current user's session
					Yii::app()->user->setState('proxyUrl', $model->proxyUrl);
					Yii::app()->user->setState('partenaireType', $model->type);
				}
				$this->getController()->redirect(['view-admin', 'id' => $model->id]);
			}
		}

		$controller = $this->getController();
		$controller->layout = '//layouts/column1';
		$controller->render(
			'update',
			['model' => $model]
		);
	}
}
