<?php

namespace controllers\partenaire;

use Partenaire;
use Ressource;

class IndexAction extends \CAction
{
	public function run(): void
	{
		$r = Ressource::model()->findAllByAttributes(['partenaire' => 1], ['order' => 'nom']);
		$this->getController()->render(
			'index',
			[
				'partenaires' => Partenaire::getListByType(),
				'ressources' => $r,
			]
		);
	}
}
