<?php

namespace controllers\partenaire;

use CHtml;
use DetailedHttpException;
use Partenaire;
use processes\grappe\Selection;
use processes\partenaire\GrappeAffichage;
use Yii;

class ViewAction extends \CAction
{
	public const MAX_ACTUS = 5;

	public function run(string $id)
	{
		$model = \PartenaireController::loadModel($id);

		$isStandardUser = Yii::app()->user->checkAccess("avec-partenaire");
		if ($model->statut === Partenaire::STATUT_INACTIF && !$isStandardUser) {
			throw new DetailedHttpException(
				404,
				"Cet établissement partenaire de Mir@bel n'a pas été trouvé",
				"Consultez la " . CHtml::link("liste des partenaires", ['index']) . "."
			);
		}

		$selectionGrappes = new Selection($model, \Yii::app()->user);

		/** @var \Controller $controller */
		$controller = $this->getController();
		$controller->setCanonicalUrl(['/partenaire/view', 'id' => $model->id]);
		$controller->render(
			'view-public',
			[
				'model' => $model,
				'actus' => self::getActus($model->id),
				'grappes' => $selectionGrappes->getVisibleGrappes(GrappeAffichage::PAGE_PARTENAIRE),
				'grappesPage' => $selectionGrappes->hasGrappesInList(GrappeAffichage::PAGE_SELECTION),
			]
		);
	}

	private static function getActus(int $id): array
	{
		$actu = new \models\cms\BreveSearchForm();
		$actu->partenaireId = $id;
		$actuProvider = $actu->search();
		$actuProvider->getCriteria()->limit = self::MAX_ACTUS + 1;
		return $actuProvider->getData();
	}
}
