<?php

namespace controllers\partenaire;

use AbonnementSearch;
use CHttpException;
use PartenaireController;
use Yii;

class AbonnementsAction extends \CAction
{
	public function run(string $id)
	{
		$partenaire = PartenaireController::loadModel($id);
		if (!Yii::app()->user->checkAccess('abonnement/update', $partenaire)) {
			throw new CHttpException(403, "Vous n'avez pas la permission de modifier ceci.");
		}

		$abonnement = new AbonnementSearch();
		$abonnement->unsetAttributes();

		$abonnement->partenaireId = (int) $partenaire->id;
		if (isset($_GET['AbonnementSearch'])) {
			$abonnement->setAttributes($_GET['AbonnementSearch']);
		}

		$this->getController()->render(
			'abonnements',
			[
				'partenaire' => $partenaire,
				'abonnement' => $abonnement,
			]
		);
	}
}
