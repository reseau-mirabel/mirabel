<?php

namespace controllers\partenaire;

use CHttpException;
use processes\partenaire\Owning;
use Yii;

/**
 * Updates the records a Partenaire owns.
 */
class PossessionAction extends \CAction
{
	public function run(string $id)
	{
		$model = \PartenaireController::loadModel($id);
		if (!$model->canOwnTitles()) {
			throw new CHttpException(404, "Un tel partenaire n'a pas de possessions dans Mir@bel.");
		}
		if (!Yii::app()->user->checkAccess('possession/update', ['Partenaire' => $model])) {
			throw new CHttpException(403, "Vous n'avez pas la permission de modifier ceci.");
		}

		$process = new Owning($model);
		if ($process->updateExisting($_POST['existing'] ?? []) || $process->add($_POST['Possession'] ?? [])) {
			$controller = $this->getController();
			assert($controller instanceof \Controller);
			return $controller->flashAndRedirect($process->getRedirection());
		}

		$process->findExisting($_GET['tri'] ?? '');

		$this->getController()->render(
			'possession',
			[
				'process' => $process,
				'bouquets' => Yii::app()->db
					->createCommand(
						"SELECT bouquet FROM Partenaire_Titre WHERE partenaireId = :pid GROUP BY bouquet"
					)->queryColumn([':pid' => $model->id]),
			]
		);
	}
}
