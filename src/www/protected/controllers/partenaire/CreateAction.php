<?php

namespace controllers\partenaire;

use CHttpException;
use processes\partenaire\Create;
use Yii;

class CreateAction extends \CAction
{
	public function run(): void
	{
		$controller = $this->getController();
		assert($controller instanceof \Controller);

		$process = new Create();

		$editeurPartenaire = Yii::app()->session->get('editeur-partenaire');
		if ($editeurPartenaire) {
			// creating a partenaire-editeur
			if (!Yii::app()->user->checkAccess('suiviPartenairesEditeurs')) {
				throw new CHttpException(403, "Vous n'êtes pas autorisé à créer un partenaire-éditeur.");
			}
			$process->setEditeur($editeurPartenaire);
		} else {
			// creating a partenaire-veilleur
			if (!Yii::app()->user->access()->toPartenaire()->create()) {
				throw new CHttpException(403, "Vous n'êtes pas autorisé à créer un partenaire.");
			}
		}

		if ($process->load($_POST['Partenaire'] ?? []) && $process->save()) {
			Yii::app()->session->remove('editeur-partenaire');
			$controller->flashAndRedirect($process->getRedirection());
			return;
		}

		$controller->layout = '//layouts/column1';
		$controller->render(
			'create',
			['model' => $process->model]
		);
	}
}
