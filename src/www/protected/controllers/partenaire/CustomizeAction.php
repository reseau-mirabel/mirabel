<?php

namespace controllers\partenaire;

use models\forms\CustomizePartenaireForm;
use Yii;

class CustomizeAction extends \CAction
{
	public function run(string $id)
	{
		$partenaire = self::findPartenaire((int) $id);
		$formData = new CustomizePartenaireForm($partenaire);

		if (Yii::app()->request->isPostRequest) {
			$formData->setAttributes($_POST['CustomizePartenaireForm']);
			if ($formData->validate()) {
				if ($formData->save($partenaire)) {
					Yii::app()->user->setFlash('success', "Personnalisation enregistrée");
					$this->getController()->redirect(['/partenaire/view', 'id' => $partenaire->id]);
					return;
				}
				Yii::app()->user->setFlash('error', "Erreur d'enregistrement en base de données");
				Yii::log(json_encode($partenaire->getErrors(), JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE), 'error');
			}
		}

		$this->getController()->render(
			'customize',
			['formData' => $formData, 'partenaire' => $partenaire]
		);
	}

	private function findPartenaire(int $id): \Partenaire
	{
		$partenaire = \Partenaire::model()->findByPk($id);
		if (!($partenaire instanceof \Partenaire)) {
			throw new \CHttpException(404);
		}
		if (!Yii::app()->user->access()->toPartenaire()->update($partenaire)) {
			throw new \CHttpException(403);
		}
		return $partenaire;
	}
}
