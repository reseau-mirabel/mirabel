<?php

namespace controllers\partenaire;

use Yii;

/**
 * Retire une grappe d'une sélection.
 */
class GrappesRemoveAction extends \CAction
{
	public function run(int $id)
	{
		if ($id > 0) {
			$partenaire = self::findPartenaire((int) $id);
			$partenaireId = (int) $partenaire->id;
		} else {
			$permissions = new \processes\grappe\Permissions(Yii::app()->user);
			if (!$permissions->canAdmin()) {
				throw new \CHttpException(403);
			}
			$partenaireId = null;
		}

		$grappeId = Yii::app()->request->getPost('grappeId', 0);
		if ($grappeId) {
			$removed = \Yii::app()->db
				->createCommand("DELETE FROM Partenaire_Grappe WHERE partenaireId <=> :p AND grappeId = :g")
				->execute([':p' => $partenaireId, ':g' => $grappeId]);
			if ($removed) {
				Yii::app()->user->setFlash('success', "La grappe a été retirée de la sélection.");
			}
		}
		if ($partenaireId > 0) {
			$this->controller->redirect(['grappes-affichage', 'id' => $id]);
		} else {
			$this->controller->redirect(['/grappe/admin-publique', 'id' => $id]);
		}
	}

	private function findPartenaire(int $id): \Partenaire
	{
		$partenaire = \Partenaire::model()->findByPk($id);
		if (!($partenaire instanceof \Partenaire)) {
			throw new \CHttpException(404);
		}
		if (!Yii::app()->user->access()->toPartenaire()->update($partenaire)) {
			throw new \CHttpException(403);
		}
		return $partenaire;
	}
}
