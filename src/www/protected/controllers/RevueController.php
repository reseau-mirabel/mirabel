<?php

use controllers\revue as actions;

class RevueController extends Controller
{
	/**
	 * @var string the default layout for the views. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	public function filters(): array
	{
		return [
			'accessControl', // see accessRules()
		];
	}

	/**
	 * This method is used by the 'accessControl' filter.
	 */
	public function accessRules(): array
	{
		return [
			[
				'allow',
				'actions' => ['ajax-complete', 'ajax-titre', 'carte', 'checkLinks', 'customize-links', 'export', 'index', 'search', 'view', 'view-by', 'viewBy'],
				'users' => ['*'],
			],
			[
				'allow',
				'actions' => ['categories', 'delete', 'exportToWikidata', 'trier-titre', 'retire-categorie', 'verify'],
				'roles' => ['avec-partenaire'],
			],
			[
				'deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	/**
	 * Maps actions to dedicated classes.
	 */
	public function actions()
	{
		return [
			'ajax-complete' => 'controllers\revue\AjaxCompleteAction',
			'ajax-titre' => 'controllers\revue\AjaxTitreAction',
			'carte' => 'controllers\revue\CarteAction',
			'categories' => 'controllers\revue\CategoriesAction',
			'checkLinks' => 'controllers\revue\CheckLinksAction',
			'customize-links' => actions\CustomizeLinks::class,
			'delete' => 'controllers\revue\DeleteAction',
			'export' => 'controllers\revue\ExportAction',
			'exportToWikidata' => 'controllers\revue\ExportToWikidataAction',
			'index' => 'controllers\revue\IndexAction',
			'trier-titre' => 'controllers\revue\TrierTitreAction',
			'retire-categorie' => 'controllers\revue\RetireCategorieAction',
			'search' => 'controllers\revue\SearchAction',
			'verify' => 'controllers\revue\VerifyAction',
			'view' => 'controllers\revue\ViewAction',
			'view-by' => 'controllers\revue\ViewByAction',
			'viewBy' => 'controllers\revue\ViewByAction',
		];
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 *
	 * If the data model is not found, an HTTP exception will be raised.
	 *
	 * @param string $id the ID of the model to be loaded
	 * @return Revue
	 */
	public static function loadModel($id): Revue
	{
		$model = Revue::model()->findByPk((int) $id);
		if ($model === null) {
			throw new CHttpException(404, "La revue demandée n'existe pas.");
		}
		return $model;
	}
}
