<?php

use processes\redirection\Display;

class RedirectionController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return ['accessControl'];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			['allow', 'roles' => ['admin'], ],
			['deny',  'users' => ['*'], ],
		];
	}

	public function actionIndex()
	{
		$this->render(
			'index',
			[
				'redirEditeur' => Display::fetchList('Editeur'),
				'redirRevue' => Display::fetchList('Revue'),
				'redirTitre' => Display::fetchList('Titre'),
			]
		);
	}
}
