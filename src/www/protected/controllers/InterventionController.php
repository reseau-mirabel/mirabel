<?php

use models\searches\InterventionSearch;

class InterventionController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'roles' => ['avec-partenaire'],
			],
			[
				'deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	public function actions()
	{
		return [
			'accept' => \controllers\intervention\AcceptAction::class,
			'revert' => \controllers\intervention\RevertAction::class,
			'send-reminder' => \controllers\intervention\SendReminderAction::class,
		];
	}

	/**
	 * Displays a particular model.
	 *
	 * @param string $id Intervention.id
	 * @param string $statut ""|"warning"
	 */
	public function actionView($id, $statut = '')
	{
		$this->containerClass = "container full-width";
		$model = $this->loadModel($id);
		$this->render(
			'view',
			[
				'model' => $model,
				'canValidate' => Yii::app()->user->checkAccess('validate', $model),
				'statut' => $statut,
			]
		);
	}

	/**
	 * Rejects.
	 *
	 * @param string $id the ID of the model.
	 */
	public function actionReject($id)
	{
		$model = $this->loadModel($id);
		if (!Yii::app()->user->checkAccess('validate', $model)) {
			throw new CHttpException(403, "Vous n'avez pas la permission de valider sur cette intervention.");
		}

		if ($model->reject()) {
			if (Yii::app()->request->isAjaxRequest) {
				Yii::app()->end();
			} else {
				Yii::app()->user->setFlash('success', "Intervention rejetée.");
				$this->redirect(['view', 'id' => $model->id]);
			}
		} else {
			Yii::app()->user->setFlash(
				'error',
				"L'intervention n'a pas pu être refusée. " . join(" ; ", $model->render()->errors())
			);
			$this->redirect(['view', 'id' => $model->id]);
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->layout = '//layouts/column1';
		$model = new InterventionSearch('search');
		$model->unsetAttributes();  // clear any default values
		$model->import = null;
		$model->suivi = false;
		if (isset($_GET['q'])) {
			$model->setAttributes($_GET['q']);
			$model->validate();
		} else {
			$model->statut = 'attente';
		}

		$this->render(
			'admin',
			['model' => $model]
		);
	}

	public function actionExport(array $q)
	{
		$model = new InterventionSearch('search');
		$model->unsetAttributes();  // clear any default values
		$model->import = null;
		$model->suivi = false;
		$model->setAttributes($q);
		$model->validate();

		\Controller::header("csv");
		$name = \Norm::urlParam(\Yii::app()->name);
		header(
			sprintf('Content-Disposition: attachment; filename="%s_Interventions_%s.csv"', $name, date('Y-m-d'))
		);

		$dp = $model->search();
		$dp->setPagination(false);
		$exporter = new \components\DataProviderExporter($dp, $model->getColumnsText());
		$exporter->writeCsv(fopen('php://output', 'w'));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 *
	 * If the data model is not found, an HTTP exception will be raised.
	 *
	 * @param string $id the ID of the model to be loaded
	 * @return Intervention
	 */
	public function loadModel($id)
	{
		$model = Intervention::model()->findByPk((int) $id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		return $model;
	}
}
