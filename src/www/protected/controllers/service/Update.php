<?php

namespace controllers\service;

use CHtml;
use CHttpException;
use Suivi;
use Titre;
use Yii;

class Update extends \CAction
{
	/**
	 * @param string $id the ID of the model to be updated
	 */
	public function run($id)
	{
		$controller = $this->getController();
		assert($controller instanceof \ServiceController);

		$model = $controller->loadModel($id);
		$directAccess = Yii::app()->user->checkAccess('service/update-direct', ['Service' => $model]);
		$forceProposition = false;
		if ($directAccess) {
			$controller->layout = '//layouts/column2';

			if (isset($_POST['force_proposition'])) {
				$forceProposition = (bool) $_POST['force_proposition'];
			} elseif (!Suivi::checkDirectAccessByIds($model->titre->revueId, $model->ressourceId)) {
				$forceProposition = true;
			}
		}
		if ($directAccess && !empty($model->titre->revueId)) {
			$titres = Titre::model()->findAllByAttributes(
				['revueId' => $model->titre->revueId],
				['order' => 'dateDebut DESC, titre ASC', 'index' => 'id']
			);
		} else {
			$titres = [];
		}
		$forbidCollectionsUpdate = !Yii::app()->user->checkAccess("avec-partenaire")
			|| ($model->ressource->autoImport && !Yii::app()->user->checkAccess('collection/admin'));

		// switch to a proposition model
		$noProposition = $directAccess && !$forceProposition;

		if (isset($_POST['Service'])) {
			if (!Yii::app()->user->checkAccess("avec-partenaire")) {
				unset($_POST['Service']['ressourceId'], $_POST['Service']['titreId']);
			}
			$old = clone $model;
			$collectionIds = $model->setAttributes($_POST['Service']);
			$i = $model->buildIntervention($noProposition);
			if (isset($_POST['Intervention']['email'])) {
				$i->setAttributes($_POST['Intervention']);
			}
			if (!$forbidCollectionsUpdate) {
				if (!$model->ressource->hasCollections()) {
					$collectionIds = [];
				}
				$i->updateCollections($model, $old->getCollectionIds(), $collectionIds);
			}
			$changedAttributes = array_keys(array_diff_assoc($model->getAttributes(), $old->getAttributes()));
			if ($model->validate($changedAttributes, false)) {
				if ($model->ressource->autoImport && !Yii::app()->user->checkAccess('service/update-autoimport')) {
					throw new CHttpException(403, "Seul un admin peut modifier un accès en ligne automatiquement importé.");
				}
				$i->description = "Modification d'un accès en ligne revue « " . $model->titre->titre
					. " » / « {$model->ressource->nom} »";
				$i->contenuJson->update($old, $model);
				unset($old);
				if ($controller->validateIntervention($i, $noProposition)) {
					$controller->redirect(['/revue/view', 'id' => $model->titre->revueId]);
					return;
				}
			}
		}
		if (!isset($i)) {
			$i = $model->buildIntervention($directAccess);
		}

		$controller->render(
			'update',
			[
				'model' => $model,
				'direct' => $directAccess,
				'intervention' => $i,
				'titres' => $titres,
				'forceProposition' => $forceProposition,
				'forbidCollectionsUpdate' => $forbidCollectionsUpdate,
			]
		);
	}
}
