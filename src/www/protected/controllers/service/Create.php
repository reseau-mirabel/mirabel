<?php

namespace controllers\service;

use CHtml;
use CHttpException;
use Intervention;
use Revue;
use Service;
use Suivi;
use Titre;
use Yii;

/**
 * Creates a new Sevice record.
 *
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class Create extends \CAction
{
	/**
	 * @var bool
	 */
	private $directAccess;

	public function run($revueId = null, $titreId = null)
	{
		$controller = $this->getController();
		assert($controller instanceof \ServiceController);

		$model = new Service();
		if (!empty($titreId)) {
			$model->titreId = (int) $titreId;
			$revueId = (int) ($model->titre === null ? -1 : $model->titre->revueId);
		}
		$this->checkAccess($model, (int) $revueId);

		$ressourceId = self::readRessourceId();
		$directAccessPossible = Yii::app()->user->checkAccess(
			'service/create-direct',
			['revueId' => (int) $revueId, 'ressourceId' => $ressourceId]
		);
		$forceProposition = $directAccessPossible && (
			// user submitted the checkbox, or someone else is monitoring this data
			self::readForceProposition() ?? !Suivi::checkDirectAccessByIds((int) $revueId, $ressourceId)
		);
		$this->directAccess = $directAccessPossible && !$forceProposition;

		$i = $this->writeIntervention($model, $_POST['Service'] ?? []);
		if ($i === null) {
			return; // redirected
		}

		$titres = [];
		if (!empty($revueId) && $revueId > 0) {
			$i->revueId = (int) $revueId;
			if ($i->revue !== null) {
				$i->suivi = Suivi::isTracked($i->revue) !== null;
				$titres = Titre::model()->findAllByAttributes(
					['revueId' => $revueId],
					['order' => '(obsoletePar IS NULL) DESC, dateDebut DESC, titre ASC', 'index' => 'id']
				);
			} else {
				$i->revueId = null;
			}
		}

		$controller->render(
			'create',
			[
				'model' => $model,
				'direct' => $this->directAccess,
				'titres' => $titres,
				'intervention' => $i,
				'forceProposition' => $forceProposition,
			]
		);
	}

	private function checkAccess(Service $model, int $revueId): void
	{
		if ($model->titreId > 0) {
			if ($model->titre === null) {
				throw new CHttpException(400, "titreId non valide.");
			}
		}
		if ($revueId && Revue::model()->findByPk($revueId) === null) {
			throw new CHttpException(400, "revueId non valide.");
		}
	}

	private static function readForceProposition(): ?bool
	{
		if (!isset($_POST['force_proposition'])) {
			return null;
		}
		return (bool) $_POST['force_proposition'];
	}

	private static function readIntervention(): array
	{
		return (array) $_POST['Intervention'];
	}

	private static function readRessourceId(): ?int
	{
		if (empty($_POST['Service']['ressourceId'])) {
			return null;
		}
		return (int) $_POST['Service']['ressourceId'];
	}

	private function writeIntervention(Service $model, array $formData): ?Intervention
	{
		if (empty($formData)) {
			return new Intervention();
		}
		$collectionIds = $model->setAttributes($formData);
		$i = $model->buildIntervention($this->directAccess);
		$i->action = 'service-C';
		$i->setAttributes(self::readIntervention());
		if ($model->validate(null, false)) {
			$i->description = "Création d'un accès en ligne revue « {$model->titre->titre} »"
				. " / « {$model->ressource->nom} »";
			$i->contenuJson->create($model);
			$i->updateCollections($model, [], $collectionIds);

			$controller = $this->getController();
			assert($controller instanceof \Controller);
			if ($controller->validateIntervention($i, $this->directAccess)) {
				$controller->redirect(['/revue/view', 'id' => $model->titre->revueId]);
				return null;
			}
		}
		return $i;
	}
}
