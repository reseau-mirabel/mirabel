<?php

namespace controllers\service;

use CHttpException;
use SearchTitre;
use Yii;
use processes\service\ExportServices;
use processes\service\ServiceFilter;

/**
 * Export services from selected journals.
 */
class Export extends \CAction
{
	public function run(string $format = 'csv')
	{
		// criteria on Service (and Bacon exports)
		$filter = $this->createFilter($_GET['Service'] ?? []);
		if (!$filter->validate()) {
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}

		// criteria on Titre
		$revuesIds = $this->createJournalList($_GET['q'] ?? [], $filter);

		// export
		$export = new ExportServices($revuesIds, $filter);
		if ($format === 'tsv' || $format === 'kbart' || $filter->isBacon()) {
			$format = 'kbart';
			if ($filter->bacon === ServiceFilter::BACON_EPISCIENCES) {
				$filename = "EPISCIENCES_GLOBAL_ALLJOURNALS_%s.txt";
			} elseif ($filter->bacon === ServiceFilter::BACON_DOAJ) {
				$filename = "MIRABEL_GLOBAL_DOAJ-PARTIEL_%s.txt";
			} elseif ($filter->bacon === ServiceFilter::BACON_GLOBAL) {
				$filename = "MIRABEL_GLOBAL_TEXTEINTEGRAL_%s.txt";
			} elseif ($filter->bacon === ServiceFilter::BACON_LIBRE) {
				$filename = "MIRABEL_GLOBAL_LIBRESACCES_%s.txt";
			} elseif ($filter->bacon === ServiceFilter::BACON_REPERES) {
				$filename = "REPERES_GLOBAL_LIBRESACCES_%s.txt";
			} else {
				$filename = \Norm::urlParam(Yii::app()->name) . "_acces-en-ligne_%s.txt";
			}
		} else {
			$filename = \Norm::urlParam(Yii::app()->name) . '_acces-en-ligne_%s.csv';
		}
		$filenameWithDate = sprintf($filename, date('Y-m-d'));

		header("Content-Disposition: attachment; filename=\"{$filenameWithDate}\"");
		$out = fopen('php://output', 'w');
		switch ($format) {
			case 'csv':
				header('Content-Type: text/csv; charset="UTF-8"; header=present');
				$export->exportToCsv($out, $filter->lacunaire || $filter->selection);
				break;
			default:
				header('Content-Type: text/tab-separated-values; charset="UTF-8"; header=present');
				$extraFields = !$filter->isBacon();
				$export->exportToKbartV2($out, $extraFields);
		}
	}

	private function createFilter(array $userData): ServiceFilter
	{
		$filter = new ServiceFilter();
		$filter->setScenario('filter');
		$filter->setAttributes($userData);
		if (empty($filter->ressourceId)) {
			$controller = $this->getController();
			assert($controller instanceof \Controller);
			$filter->ressourceId = $controller->autoCompleteOffline('ressourceIdComplete', 'Ressource');
		}
		return $filter;
	}

	private function createJournalList(array $userData, ServiceFilter $filter): ?array
	{
		$model = new SearchTitre(Yii::app()->user->getState('instituteId'));
		if ($userData) {
			$model->setAttributes($userData);
		} elseif ($filter->ressourceId) {
			$model->ressourceId = [$filter->ressourceId];
		}
		if (!$model->validate()) {
			throw new CHttpException(404, "Invalid request. Bad query: " . print_r($model->errors, true));
		}
		if (array_filter($model->attributes)) {
			return $model->searchRevueIds();
		}
		return null;
	}
}
