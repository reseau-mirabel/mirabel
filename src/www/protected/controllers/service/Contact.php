<?php

namespace controllers\service;

use Config;
use Service;
use Suivi;
use Yii;
use components\email\Mailer;
use components\email\Message;
use models\forms\ContactServiceForm;

/**
 * Displays the contact page (on an imported service page).
 */
class Contact extends \CAction
{
	public function run($id)
	{
		$controller = $this->getController();
		assert($controller instanceof \ServiceController);

		$service = $controller->loadModel($id);
		$model = new ContactServiceForm();
		$model->subjectr = Yii::app()->name . ' — accès importé - '
			. "{$service->titre->titre} / {$service->ressource->nom}";

		$message = $this->prepareMessage($_POST['ContactServiceForm'] ?? [], $model, $service);
		if ($message !== null) {
			if (Mailer::sendMail($message)) {
				Yii::app()->user->setFlash(
					'info',
					"Le message a été envoyé aux personnes qui suivent cette ressource dans Mir@bel. "
					. "Nous vous remercions de votre intérêt pour Mir@bel et vous répondrons "
					. "dans les meilleurs délais."
				);
				$controller->redirect(['/revue/view', 'id' => $service->titre->revueId]);
			} else {
				Yii::log("Envoi raté d'email", 'error', 'service/contact');
				Yii::app()->user->setFlash(
					'error',
					'Le message n\'a pu être envoyé. Le problème a été signalé aux administrateurs.'
				);
				$controller->refresh();
			}
			return;
		}
		$controller->render(
			'contact',
			[
				'model' => $model,
				'titre' => $service->titre,
				'bodyStart' => $this->getPrependBody($service),
			]
		);
	}

	private function getPrependBody(Service $service): string
	{
		return "Je souhaite signaler une erreur dans cet accès en ligne, sur la page "
			. $this->getController()->createAbsoluteUrl('/revue/view', ['id' => $service->titre->revueId])
			. ".\nURL actuelle : {$service->url}";
	}

	private function prepareMessage(array $formData, ContactServiceForm $model, Service $service): ?Message
	{
		if (empty($formData)) {
			return null;
		}
		if (!empty($_POST['nopost'])) {
			return null;
		}
		$model->setAttributes($formData);
		if (!$model->validate()) {
			return null;
		}
		$partenaire = Suivi::isTracked($service->ressource);
		$dest = (
			$partenaire ?
			$partenaire->email
			: (Config::read('contact.donnees.email') ?: Config::read('contact.email'))
		);
		return Mailer::newMail()
			->subject($model->subjectr)
			->from(Config::read('email.from'))
			->replyTo(new \Symfony\Component\Mime\Address($model->emailr, $model->name))
			->setTo($dest)
			->text(<<<TXT
				[Expéditeur : {$model->name} <{$model->emailr}>]

				{$this->getPrependBody($service)}

				{$model->body}
				TXT
			);
	}
}
