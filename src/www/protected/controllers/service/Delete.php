<?php

namespace controllers\service;

use CHttpException;
use Collection;
use Yii;

class Delete extends \CAction
{
	/**
	 * @param string $id the ID of the model to be deleted
	 */
	public function run($id)
	{
		if (!Yii::app()->request->isPostRequest) {
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}

		$controller = $this->getController();
		assert($controller instanceof \ServiceController);

		$service = $controller->loadModel($id);
		$i = $service->buildIntervention(true);
		$i->action = 'service-D';
		$i->description = "Suppression d'un accès en ligne, revue « {$service->titre->titre} »"
			. " / « {$service->ressource->nom} »";
		$i->contenuJson->delete($service, "Suppression de l'accès en ligne");
		$i->accept();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if (isset($_GET['ajax'])) {
			return;
		}

		$collections = $service->collections;
		if ($collections) {
			$msgs = [];
			foreach ($collections as $c) {
				/** @var Collection $c */
				$services = $c->services;
				if ($services && count($services) === 1) {
					$msgs[] = "Suite à cette suppression d'accès, la collection {$c->nom} est sans accès.";
				}
			}
			if ($msgs) {
				Yii::app()->user->setFlash('warning', join("\n<br />", $msgs));
			} else {
				Yii::app()->user->setFlash('success', "L'accès en ligne a été supprimé.");
			}
		} else {
			Yii::app()->user->setFlash('success', "L'accès en ligne a été supprimé.");
		}
		if (isset($_POST['returnUrl'])) {
			$url = $_POST['returnUrl'];
		} else {
			$url = ['/revue/view', 'id' => $service->titre->revueId];
		}
		$controller->redirect($url);
	}
}
