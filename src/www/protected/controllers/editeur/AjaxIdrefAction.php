<?php

namespace controllers\editeur;

use processes\idref\Notice;

class AjaxIdrefAction extends \CAction
{
	public function run(string $idref = '')
	{
		$controller = $this->getController();
		\assert($controller instanceof \Controller);
		\Controller::header('json');

		$_SERVER['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest';

		$notice = Notice::fetchByIdref($idref);
		echo json_encode($notice, YII_DEBUG ? JSON_PRETTY_PRINT : 0);
	}
}
