<?php

namespace controllers\editeur;

use Yii;

class UpdateAction extends \CAction
{
	public function run(string $id)
	{
		$controller = $this->getController();
		assert($controller instanceof \EditeurController);

		$model = \EditeurController::loadModel($id);
		$directAccess = Yii::app()->user->access()->toEditeur()->updateDirect($model);
		if ($directAccess && !empty($_POST['force_proposition'])) {
			$directAccess = false;
		}
		$i = $model->buildIntervention($directAccess);

		if (isset($_POST['Editeur'])) {
			$old = clone $model;
			$model->attributes = $_POST['Editeur'];
			if (isset($_POST['Intervention']['email'])) {
				$i->attributes = $_POST['Intervention'];
			}
			$changedAttributes = array_keys(array_diff_assoc($model->getAttributes(), $old->getAttributes()));
			if ($model->validate($changedAttributes)) {
				$i->description = "Modification de l'éditeur « {$model->nom} »";
				$i->action = 'editeur-U';
				$i->contenuJson->update($old, $model);
				unset($old);

				if ($controller->validateIntervention($i, $directAccess)) {
					if (self::isImportantChange($i)) {
						$i->emailForEditorChange();
					}
					$controller->redirect($model->getSelfUrl());
					return;
				}
				Yii::app()->user->setFlash('error', "Erreur lors de l'enregistrement.");
			}
		} else {
			// Au chargement initial de la page, valider sans requête HTTP.
			$model->getLiens()->validate(urlValidation: false);
		}

		$controller->render(
			'update',
			[
				'model' => $model,
				'direct' => $directAccess,
				'intervention' => $i,
			]
		);
	}

	/**
	 * Decide is an Intervention contains important changes that will trigger an email.
	 */
	private function isImportantChange(\Intervention $i): bool
	{
		foreach ($i->contenuJson->toArray() as $change) {
			if (($change['model'] ?? '') !== 'Editeur' || $change['operation'] !== 'update') {
				continue;
			}
			foreach (['nom', 'idref', 'role', 'sherpa'] as $a) {
				if (!empty($change['before'][$a])) {
					// valeur non vide modifiée => modification ou suppression
					return true;
				}
			}
			// Is there a modification of a local URL?
			// i.e. an URL starting with "/" which exists only before/after the Intervention.
			if (isset($change['before']['liensJson']) || isset($change['after']['liensJson'])) {
				$before = json_decode($change['before']['liensJson'] ?? '[]', true);
				$after = json_decode($change['after']['liensJson'] ?? '[]', true);
				$beforeUrls = [];
				if ($before) {
					foreach ($before as $l) {
						$beforeUrls[] = $l['url'];
					}
				}
				$afterUrls = [];
				if ($after) {
					foreach ($after as $l) {
						$afterUrls[] = $l['url'];
					}
				}
				foreach (array_diff($afterUrls, $beforeUrls) as $url) {
					if ($url[0] === '/') {
						return true;
					}
				}
				foreach (array_diff($beforeUrls, $afterUrls) as $url) {
					if ($url[0] === '/') {
						return true;
					}
				}
			}
		}
		return false;
	}
}
