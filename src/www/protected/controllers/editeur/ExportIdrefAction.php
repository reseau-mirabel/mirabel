<?php

namespace controllers\editeur;

use Yii;

class ExportIdrefAction extends \CAction
{
	public function run(): void
	{
		$this->sendHttpHeader();

		echo "IdRef;\"id M\"\n";
		$query = Yii::app()->db->createCommand(
			"SELECT idref, id FROM Editeur WHERE idref IS NOT NULL ORDER BY id"
		);
		foreach ($query->query() as $row) {
			echo "{$row['idref']};{$row['id']}\n";
		}
	}

	private function sendHttpHeader(): void
	{
		$controller = $this->getController();
		\assert($controller instanceof \Controller);
		\Controller::header('csv');
		$name = \Norm::urlParam(\Yii::app()->name);
		header('Content-Disposition: inline; filename="' . $name . '_editeurs-idref_' . date('Y-m-d') . '.csv"');
	}
}
