<?php

namespace controllers\editeur;

use CHttpException;
use Editeur;

/**
 * As a normal page, redirect to the publisher's page (single idref).
 * As an application/json request, return the {id, idref, nom} of the matching publishers (multiple idref).
 */
class IdrefAction extends \CAction
{
	public function run(string $idref)
	{
		if (strlen($idref) > 1000 || !preg_match('/^[0-9Xx,]+$/', $idref)) {
			throw new CHttpException(400, "Paramètre 'idref' non valide");
		}
		if (!self::isHtmlRequested()) {
			\Controller::header('json');
			$idrefs = explode(',', $idref);
			echo json_encode(self::findPublishers($idrefs));
			return;
		}

		$model = Editeur::model()->findByAttributes(['idref' => $idref]);
		if ($model === null) {
			throw new CHttpException(404, "Aucun éditeur dans Mir@bel n'a cet IdRef.");
		}
		$this->getController()->redirect($model->getSelfUrl());
	}

	private static function isHtmlRequested(): bool
	{
		$request = \Yii::app()->getRequest();
		$accept = $request->getPreferredAcceptType();
		if ($accept === false) {
			// If no type, suppose that HTML is suitable.
			return true;
		}
		return ($accept['type'] === 'text' && $accept['subType'] === 'html');
	}

	private static function findPublishers(array $idrefs): array
	{
		$joined = "'" . join("','", $idrefs) . "'";
		$cmd = \Yii::app()->db
			->createCommand(<<<EOSQL
				SELECT e.id, e.idref, e.nom FROM Editeur e WHERE e.idref IN ($joined)
				EOSQL
			);
		$result = [];
		foreach ($cmd->query() as $row) {
			$result[] = [
				'id' => (int) $row['id'],
				'idref' => $row['idref'],
				'nom' => $row['nom'],
			];
		}
		return $result;
	}
}
