<?php

namespace controllers\editeur;

use Editeur;
use Yii;

class ViewAction extends \CAction
{
	public function run(string $id, string $nom = '')
	{
		$model = $this->loadModel((int) $id);
		if ($model === null) {
			return;
		}
		if ($this->needsRedirection($model, $nom)) {
			return;
		}

		$controller = $this->getController();
		assert($controller instanceof \EditeurController);
		$controller->setCanonicalUrl($model->getSelfUrl());
		$controller->appendToHtmlHead(
			\Controller::linkEmbeddedFeed('rss2', "Éditeur {$model->nom}", ['editeur' => $model->id])
		);

		if (Yii::app()->session->contains('force-refresh')) {
			Yii::app()->session->remove('force-refresh');
			$forceRefresh = true;
		} else {
			$forceRefresh = false;
		}

		$controller->render(
			'view',
			[
				'model' => $model,
				'searchNavigation' => $controller->loadSearchNavigation(),
				'forceRefresh' => $forceRefresh,
				'actus' => ($model->partenaire === null ? [] : \controllers\partenaire\ViewAdminAction::getActus((int) $model->partenaire->id)),
			]
		);
	}

	private function loadModel(int $id): ?Editeur
	{
		$model = Editeur::model()->findByPk($id);
		if ($model instanceof Editeur) {
			$model->id = (int) $model->id;
			return $model;
		}
		$url = (new \processes\redirection\Redirector('Editeur'))->getRoute($id);
		if ($url) {
			$this->getController()->redirect($url, true, 301);
			return null;
		}
		throw new \CHttpException(404, "L'éditeur demandé n'existe pas.");
	}

	private function needsRedirection(Editeur $editeur, string $nom): bool
	{
		$selfUrl = $editeur->getSelfUrl();
		$expectedName = $selfUrl['nom'] ?? '';
		if ($expectedName && (!$nom || $nom !== $expectedName)) {
			$searchNav = $_GET['s'] ?? '';
			if ($searchNav) {
				$selfUrl['s'] = $searchNav;
			}
			$this->getController()->redirect($selfUrl);
			return true;
		}
		return false;
	}
}
