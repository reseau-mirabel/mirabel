<?php

namespace controllers\editeur;

use CHttpException;
use components\paginationalpha\ListViewAlpha;
use components\sphinx\Criteria;
use components\sphinx\DataProvider;
use components\SqlHelper;
use CSort;
use Editeur;
use models\sphinx\Editeurs;
use processes\editeur\ListFilter;
use SearchNavigation;
use Yii;

/**
 * Lists all models.
 */
class IndexAction extends \CAction
{
	public static $pageSize = 50;

	public function run(): void
	{
		$filter = ListFilter::load($_GET);

		if ($filter->lettre !== '') {
			$listView = self::createListView($filter);
		} else {
			$listView = null;
		}

		$dataProvider = self::createDataProvider($filter);
		try {
			$data = $dataProvider->getData();
		} catch (\Throwable $e) {
			Yii::log($e->getMessage(), 'warning');
			throw new CHttpException(400, "Erreur dans la recherche d'éditeurs. Navigation au-delà de la taille des résultats ?");
		}
		if ($listView !== null) {
			$listView->dataProvider = $dataProvider;
		}

		$searchCache = new SearchNavigation();
		$searchHash = $searchCache->save(
			'editeur/index',
			['lettre' => $filter->lettre, 'suivi' => $filter->suivi],
			'id', // key: Editeurs.id
			$data,
			$dataProvider->getPagination()
		);

		$this->getController()->render(
			'index',
			[
				'dataProvider' => $dataProvider,
				'filter' => $filter,
				'listView' => $listView,
				'searchHash' => $searchHash,
				'totalEditeurs' => Editeur::model()->count("statut = 'normal'"),
			]
		);
	}

	private static function createDataProvider(ListFilter $filter): DataProvider
	{
		$criteria = new Criteria;

		if ($filter->suivi > 0) {
			$criteria->addCondition("suivi = {$filter->suivi}");
		} elseif ($filter->suivi < 0) {
			$criteria->addCondition("suivi > 0");
		} else {
			$criteria->addCondition("lettre1 = {$filter->letterRank}");
		}

		$pagination = [
			'pageSize' => self::$pageSize,
			'params' => ['lettre' => $filter->lettre],
		];
		if ($filter->suivi !== 0) {
			$pagination['params']['suivi'] = $filter->suivi;
		}

		$sort = new CSort(Editeurs::class);
		$sort->defaultOrder = 'cletri ASC';

		return new DataProvider(
			Editeurs::model(),
			[
				'criteria' => $criteria,
				'pagination' => $pagination,
				'sort' => $sort,
			]
		);
	}

	private static function createListView(ListFilter $filter): ListViewAlpha
	{
		$listView = new ListViewAlpha();
		$extraSql = "";
		if ($filter->suivi !== 0) {
			$extraSql = "JOIN Suivi s ON s.cible = 'Editeur' AND s.cibleId = Editeur.id"
				. ($filter->suivi > 0 ? " AND s.partenaireID = {$filter->suivi}" : "");
		}
		$listView->setCountByLetter(
			SqlHelper::sqlToPairs(<<<EOSQL
				SELECT
					FIND_IN_SET(LEFT(nom, 1), 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z') AS rank,
					COUNT(DISTINCT Editeur.id) AS num
				FROM Editeur
				$extraSql
				GROUP BY rank
				EOSQL
			)
		);
		$listView->setLetterTitleTemplate("{count} éditeurs");

		// Update the filter by validating the letter,  computing its rank, incorporating filters.
		$letterPagination = $listView->getLetterPagination();
		$filter->letterRank = $letterPagination->getCurrentPage(false);
		$filter->lettre = $letterPagination->getPageLetter($filter->letterRank);
		if ($filter->suivi) {
			$letterPagination->params['suivi'] = $filter->suivi;
		}

		return $listView;
	}
}
