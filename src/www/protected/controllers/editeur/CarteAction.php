<?php

namespace controllers\editeur;

use CHtml;
use CJavaScriptExpression;
use Yii;

class CarteAction extends \CAction
{
	public function run($stamen = '')
	{
		if (!preg_match('/^[\w-]+$/', $stamen)) {
			$stamen = '';
		}
		$countries = [];
		$max = 0;
		$query = Yii::app()->db->createCommand(
			"SELECT p.code, p.id, p.nom, count(distinct e.id) AS editeurs, count(distinct t.revueId) AS revues"
			. " FROM Editeur e JOIN Pays p ON e.paysId = p.id"
			. " LEFT JOIN Titre_Editeur te ON te.editeurId = e.id LEFT JOIN Titre t ON te.titreId = t.id"
			. " GROUP BY p.id, p.code"
		);
		foreach ($query->query() as $row) {
			if ($max < $row['editeurs']) {
				$max = $row['editeurs'];
			}
			$countries[$row['code']] = [
				$row['editeurs'],
				CHtml::tag('div', [], CHtml::encode($row['nom']))
				. CHtml::tag(
					'strong',
					[],
					CHtml::link(
						$row['editeurs'] . " éditeur" . ($row['editeurs'] > 1 ? "s" : ""),
						['/editeur/pays', 'paysId' => $row['code'], 'paysNom' => $row['nom']]
					)
				)
				. "<br /> ⇔ "
				. CHtml::link(
					$row['revues'] . " revue" . ($row['revues'] > 1 ? "s" : ""),
					['/revue/search', 'q' => ['paysId' => $row['id']]]
				),
			];
		}
		$this->getController()->render(
			'carte',
			[
				'stamen' => $stamen,
				'mapConfig' => [
					'countries' => $countries,
					'countriesMax' => $max,
					'radius' => [
						'max' => 12, // pixels
						'method' => 'logarithmic',
					],
					'popupMessage' => new CJavaScriptExpression('function(x) { return x[1]; }'),
				],
			]
		);
	}
}
