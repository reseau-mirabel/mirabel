<?php

namespace controllers\editeur;

use CHttpException;
use Norm;
use Pays as PaysModel;
use models\searches\EditeurSearch;

class PaysAction extends \CAction
{
	public function run(string $paysId, string $paysNom = '')
	{
		$pays = PaysModel::model()->find(
			"code = :c OR id = :i OR nom = :n",
			[':c' => $paysId, ':i' => $paysId, ':n' => $paysId]
		);
		if (!$pays) {
			throw new CHttpException(404, "Pays inconnu");
		}
		if ($this->needsRedirection($pays, $paysNom)) {
			return;
		}

		$model = new EditeurSearch;
		$model->pays = $pays->id;
		$this->getController()->render(
			'pays',
			[
				'pays' => $pays,
				'model' => $model,
			]
		);
	}

	private function needsRedirection(PaysModel $pays, string $nom): bool
	{
		$expectedName = Norm::urlParam($pays->nom);
		if ($expectedName && (!$nom || $nom !== $expectedName)) {
			$url = ['pays', 'paysId' => $pays->code, 'paysNom' => $expectedName];
			$this->getController()->redirect($url);
			return true;
		}
		return false;
	}
}
