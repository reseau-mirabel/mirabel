<?php

namespace controllers\editeur;

use Controller;
use processes\completion\Editeur;

class AjaxCompleteAction extends \CAction
{
	public function run(string $term)
	{
		$excludes = self::parseInts($_GET['excludeIds'] ?? '');
		Controller::header('json');
		echo json_encode((new Editeur)->completeTerm($term, [], $excludes));
	}

	private static function parseInts($values): array
	{
		if (!$values) {
			return [];
		}
		if (!is_array($values)) {
			$values = explode(",", (string) $values);
		}
		$ids = array_filter(array_map('intval', $values));
		if (!$ids) {
			return [];
		}
		return ['id' => $values];
	}
}
