<?php

namespace controllers\editeur;

use CHtml;
use Editeur;
use Intervention;
use Yii;

class CreateAction extends \CAction
{
	/**
	 * Creates a new model.
	 *
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function run(): void
	{
		$model = new Editeur;
		$directAccess = Yii::app()->user->access()->toEditeur()->createDirect();
		$i = $model->buildIntervention($directAccess);

		if (isset($_POST['Editeur'])) {
			if ($this->save($model, $i, $directAccess)) {
				return;
			}
		}

		if (($_SERVER['HTTP_X_EMBEDDED_IN_HTML'] ?? '') === 'true') {
			$_SERVER['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest'; // Emulate AJAX, to stop web logs in debug mode
			$this->getController()->renderPartial(
				'_form',
				[
					'model' => $model,
					'direct' => $directAccess,
					'intervention' => $i,
					'embedded' => true,
				]
			);
			return;
		}

		$this->getController()->render(
			'create',
			[
				'model' => $model,
				'direct' => $directAccess,
				'intervention' => $i,
			]
		);
	}

	private function save(Editeur $model, Intervention $i, bool $directAccess): bool
	{
		$controller = $this->getController();
		assert($controller instanceof \EditeurController);

		if (isset($_POST['Intervention']['email'])) {
			$i->setAttributes($_POST['Intervention']);
		}
		$model->setAttributes($_POST['Editeur']);
		$model->statut = 'normal'; /// @TODO statut 'attente' parfois ?

		$prefill = (bool) Yii::app()->getRequest()->getPost('prefill', 0);
		if ($prefill) {
			if ($model->idref) {
				self::completeModelFromIdref($model);
			}
			return false;
		}

		if ($model->validate()) {
			$i->description = "Création de l'éditeur « {$model->nom} »";
			$i->contenuJson->create($model);
			$i->action = 'editeur-C';
			if (Yii::app()->getRequest()->isAjaxRequest) {
				// on AJAX, always save, ignoring $directAccess
				if ($controller->validateIntervention($i, true)) {
					\Controller::header('json');
					echo json_encode(
						[
							'id' => $i->editeurId,
							'result' => 'success',
							'label' => CHtml::encode($model->getFullName()),
							'message' => "L'éditeur <em>" . CHtml::encode($model->getFullName()) . "</em> a été créé.",
						]
					);
					return true;
				}
			} elseif ($controller->validateIntervention($i, $directAccess)) {
				if ($directAccess) {
					$model->id = $i->editeurId;
					$controller->redirect($model->getSelfUrl());
				} else {
					$controller->redirect(['/editeur/index']);
				}
				return true;
			}
		} elseif (Yii::app()->getRequest()->isAjaxRequest) {
			\Controller::header('json');
			echo json_encode([
				'result' => 'error',
				'html' => $controller->renderPartial(
					'_form',
					['model' => $model, 'embedded' => true, 'intervention' => null],
					true
				),
			]);
			return true;
		}
		return false;
	}

	private static function completeModelFromIdref(Editeur $e): void
	{
		try {
			$notice = \processes\idref\Notice::fetchByIdref($e->idref);
		} catch (\Throwable $ex) {
			Yii::app()->user->setFlash('warning', "Erreur de récupération des données sur idref.fr : {$ex->getMessage()}");
			return;
		}

		if ($notice->dateDebut && preg_match('/^(\d{4})(-\d\d){0,2}$/', $notice->dateDebut)) {
			$e->dateDebut = $notice->dateDebut;
		}
		if ($notice->dateFin && preg_match('/^(\d{4})(-\d\d){0,2}$/', $notice->dateFin)) {
			$e->dateFin = $notice->dateFin;
		}

		if ($notice->pays) {
			$pays = \Pays::model()->findByAttributes(['code2' => $notice->pays]);
			if ($pays) {
				$e->paysId = (int) $pays->id;
			}
		}
	}
}
