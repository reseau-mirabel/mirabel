<?php

namespace controllers\editeur;

use CHttpException;
use Editeur;
use Partenaire;
use Yii;

class CreatePartenaireAction extends \CAction
{
	public function run($id)
	{
		$controller = $this->getController();
		assert($controller instanceof \EditeurController);

		$model = \EditeurController::loadModel($id);
		$this->checkAccess($model);

		$partenaire = new Partenaire();
		$partenaire->editeurId = (int) $model->id;
		$partenaire->type = 'editeur';
		$partenaire->statut = 'actif';
		foreach (['nom', 'prefixe', 'sigle', 'description', 'url', 'paysId', 'geo'] as $attr) {
			$partenaire->{$attr} = $model->{$attr};
		}
		if (strpos($partenaire->nom, '=') > 0) {
			$partenaire->nom = trim(strtok($partenaire->nom, '='));
		}

		Yii::app()->session->add('editeur-partenaire', $partenaire->attributes);
		Yii::app()->user->setFlash(
			'success',
			"Le partenaire associé à cet éditeur est prêt à être enregistré dans la base. Vérifier et compléter (champ Courriel, etc)."
			. "<br />Un compte utilisateur sera ensuite créé automatiquement."
		);
		$controller->redirect(['/partenaire/create']);
	}

	private function checkAccess(Editeur $model): void
	{
		if (!Yii::app()->user->checkAccess('suiviPartenairesEditeurs')) {
			throw new CHttpException(403, "Cette page n'est accessible qu'aux comptes en charge du suivi des partenaires-éditeurs.");
		}
		if ($model->partenaire) {
			throw new CHttpException(500, "Cet éditeur a déjà le statut d'éditeur-partenaire.");
		}
		if ($model->statut !== 'normal') {
			throw new CHttpException(500, "Cet éditeur n'a pas le statut <i>normal</i>.");
		}
	}
}
