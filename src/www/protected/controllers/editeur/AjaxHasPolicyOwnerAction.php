<?php

namespace controllers\editeur;

use Controller;
use Yii;

class AjaxHasPolicyOwnerAction extends \CAction
{
	public function run(string $id)
	{
		Controller::header('json');
		echo json_encode(
			(bool) Yii::app()->db
				->createCommand("SELECT 1 FROM Utilisateur_Editeur WHERE editeurId = :id AND `role` = 'owner' AND confirmed = 1")
				->queryScalar([':id' => (int) $id])
		);
	}
}
