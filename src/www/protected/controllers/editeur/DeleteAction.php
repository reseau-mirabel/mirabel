<?php

namespace controllers\editeur;

use CHttpException;
use Editeur;
use Yii;
use components\FeedGenerator;

class DeleteAction extends \CAction
{
	/**
	 * Deletes a particular model.
	 *
	 * If deletion is successful, the browser will be redirected.
	 *
	 * @param string $id the ID of the model to be deleted
	 */
	public function run(string $id): void
	{
		$editeur = \EditeurController::loadModel($id);

		if (!Yii::app()->user->access()->toEditeur()->delete($editeur)) {
			throw new CHttpException(403, "Vous n'avez pas accès à la suppression d'éditeurs.");
		}

		if (!Yii::app()->request->getPost('confirm', false) || Yii::app()->request->getPost('annoying', '') !== 'Je confirme') {
			$this->getController()->render(
				'delete',
				[
					'editeur' => $editeur,
					'constraints' => self::findConstraints($editeur),
					'isRedirectionRequired' => (bool) Yii::app()->db
						->createCommand("SELECT 1 FROM RedirectionEditeur WHERE cibleId = {$editeur->id}")
						->queryScalar(),
				]
			);
			return;
		}

		// we only allow deletion via POST request
		if (!Yii::app()->request->isPostRequest) {
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}

		$redirection = (int) Yii::app()->request->getPost('redirection', 0);
		if ($redirection) {
			Yii::app()->db->createCommand("UPDATE RedirectionEditeur SET cibleId = :new WHERE cibleId = :old")
				->execute([':old' => $editeur->id, ':new' => $redirection]);
			Yii::app()->db->createCommand("INSERT IGNORE INTO RedirectionEditeur VALUES (:old, :new, :ts)")
				->execute([':old' => $editeur->id, ':new' => $redirection, ':ts' => $_SERVER['REQUEST_TIME']]);
		}

		$i = self::delete($editeur, $redirection);
		if ($i->hasErrors()) {
			$returnUrl = $editeur->getSelfUrl();
			$errors = $editeur->getErrors();
			if ($i->getErrors()) {
				foreach ($i->getErrors() as $k => $v) {
					$errors[$k] = $v[0];
				}
			}
			$msg = "<ul><li>" . join("</li><li>", $errors) . "</li></ul>";
			Yii::app()->user->setFlash('error', "Suppression de « {$editeur->nom} » impossible :" . $msg);
		} else {
			$returnUrl = Yii::app()->request->getPost('returnUrl', ['/editeur/index']);
			Yii::app()->user->setFlash('success', "Éditeur « {$editeur->nom} » supprimé.");
		}
		$this->getController()->redirect($returnUrl);
	}

	/**
	 * @param \Editeur $editeur
	 * @return array Each value is a HTML block describing the constraint.
	 */
	private static function findConstraints(\Editeur $editeur): array
	{
		$constraints = [];

		if ($editeur->idref || $editeur->sherpa || $editeur->politiques) {
			$helpUrl = Yii::app()->baseUrl . '/public/Procedure_Editeurs.pdf';
			$constraints["identifiants"] = <<<EOHTML
				Suppression impossible car cet éditeur a un identifiant Idref ou Sherpa, ou une politique de publication.
				<br>
				Veuillez consulter la <a href="$helpUrl" target="_blank" title="PDF dans une nouvelle page">documentation détaillée au format PDF</a>
				<br>
				Pour toute question ou besoin d’intervention : <a href="mailto:mirabel_editeurs@listes.sciencespo-lyon.fr">mirabel_editeurs@listes.sciencespo-lyon.fr</a>.
				EOHTML;
		}

		$interventionId = Yii::app()->db
			->createCommand("SELECT id FROM Intervention WHERE editeurId = :eid AND statut = 'attente' LIMIT 1")
			->queryScalar([':eid' => $editeur->id]);
		if ($interventionId) {
			$constraints["interventions"] = <<<EOHTML
				Suppression impossible car au moins <a href="/intervention/{$interventionId}">une intervention</a> est en attente sur cet éditeur.
				EOHTML;
		}

		$references = Yii::app()->db
			->createCommand("SELECT count(*) FROM Titre_Editeur WHERE editeurId = :eid")
			->queryScalar([':eid' => $editeur->id]);
		if ($references) {
			$urlRevues = \CHtml::encode(Yii::app()->createUrl('/revue/search', ['q' => ['editeurId' => $editeur->id]]));
			$constraints["titres"] = <<<EOHTML
				Suppression impossible car <a href="$urlRevues">$references titres</a> sont associés à cet éditeur.
				EOHTML;
		}

		return $constraints;
	}

	private static function delete(Editeur $editeur, int $redirect): \Intervention
	{
		$name = $editeur->nom;
		$i = $editeur->buildIntervention(true);
		$i->description = "Suppression de l'éditeur « {$editeur->nom} »";
		$i->contenuJson->delete($editeur, "Suppression de l'éditeur");
		$i->action = 'editeur-D';
		if ($redirect) {
			$i->commentaire .= sprintf("\nRedirection vers %d (%s)", $redirect, Editeur::model()->findByPk($redirect)->nom);
		}
		if ($i->validate()) {
			FeedGenerator::saveFeedsToFile('Editeur', (int) $editeur->id);
		}
		$success = $i->accept(false);
		$i->editeurId = null;
		if ($success) {
			$i->save(false);
			$i->emailForEditorChange($name, "suppression éditeur");
		}
		return $i;
	}
}
