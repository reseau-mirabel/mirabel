<?php

namespace controllers\editeur;

use CHttpException;
use Yii;

class VerifyAction extends \CAction
{
	public function run(string $id)
	{
		$controller = $this->getController();
		\assert($controller instanceof \EditeurController);

		$model = \EditeurController::loadModel($id);
		if (!Yii::app()->user->checkAccess('verify', $model)) {
			throw new CHttpException(403, "Vous n'avez pas les droits nécessaires pour cette opération.");
		}
		$model->hdateVerif = $_SERVER['REQUEST_TIME'];
		if ($model->save(false)) {
			Yii::app()->user->setFlash('success', "La date de vérification a été mise à jour.");
		}
		$controller->redirect($model->getSelfUrl());
	}
}
