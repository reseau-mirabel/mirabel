<?php

namespace controllers\editeur;

class CheckLinksAction extends \CAction
{
	public function run(string $id)
	{
		$controller = $this->getController();
		\assert($controller instanceof \EditeurController);

		$model = \EditeurController::loadModel($id);
		$controller->render(
			'checkLinks',
			[
				'editeur' => $model,
				'checks' => (new \processes\urlverif\UrlExtractor)->checkObjectLinks($model),
			]
		);
	}
}
