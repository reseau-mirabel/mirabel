<?php

namespace controllers\editeur;

class CreateByIdrefAction extends \CAction
{
	public function run(): void
	{
		$this->getController()->render('create-by-idref');
	}
}
