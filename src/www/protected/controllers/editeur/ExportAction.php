<?php

namespace controllers\editeur;

use models\searches\EditeurSearch;
use processes\editeur\Export as ExportProcess;

class ExportAction extends \CAction
{
	public function run(string $criteria)
	{
		$model = new EditeurSearch;
		$model->setAttributes(json_decode($criteria, true));
		$provider = $model->search(false);

		$exporter = new ExportProcess();
		$exporter->setProvider($provider);

		$this->sendHttpHeader();
		$out = fopen('php://output', 'w');
		$exporter->printCsv($out);
	}

	private function sendHttpHeader(): void
	{
		$controller = $this->getController();
		\assert($controller instanceof \Controller);
		\Controller::header('csv');
		$contentDisposition = sprintf(
			'Content-Disposition: inline; filename="%s_editeurs_%s.csv"',
			\Norm::urlParam(\Yii::app()->name),
			date('Y-m-d')
		);
		header($contentDisposition);
	}
}
