<?php

namespace controllers\editeur;

use models\searches\EditeurSearch;
use Yii;

class SearchAction extends \CAction
{
	public function run(): void
	{
		$model = new EditeurSearch;
		if (isset($_GET['q'])) {
			$model->setAttributes($_GET['q']);
		}

		$controller = $this->getController();
		\assert($controller instanceof \EditeurController);

		$controller->layout = (Yii::app()->user->checkAccess("avec-partenaire") || !$model->isEmpty())
			? '//layouts/column2'
			: '//layouts/column1';

		$controller->render(
			'search',
			['model' => $model]
		);
	}
}
