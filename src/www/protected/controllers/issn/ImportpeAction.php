<?php

namespace controllers\issn;

use CUploadedFile;
use models\forms\IssnImportpe;
use processes\issn\Importpe;
use Yii;

/**
 * Importe les fichiers csv d'équivalence Issn-p - Issn-e
 */
class ImportpeAction extends \CAction
{
	public function run(): void
	{
		$iform = new IssnImportpe();
		$import = new Importpe();

		if (Yii::app()->request->isPostRequest) {
			$iform->setAttributes($_POST['IssnImportpe']);
			$iform->csv = CUploadedFile::getInstance($iform, "csv");
			if ($iform->validate()) {
				$import->loadFile($iform->csv->tempName);
				if (!$iform->simulation) {
					$message = sprintf('%d ISSN-E ajoutés.', $import->insertMissing());
					Yii::app()->user->setFlash('success', $message);
				}
			}
		}
		$this->getController()->render(
			'importpe',
			[
				'model' => $iform,
				'import' => $import,
			]
		);
	}
}
