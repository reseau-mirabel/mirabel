<?php

namespace controllers\possession;

use CHttpException;
use CUploadedFile;
use Exception;
use Partenaire;
use PossessionImport;
use Yii;

/**
 * Import a CSV into Partenaire_Titre ("possession").
 */
class ImportAction extends \CAction
{
	public function run(string $partenaireId)
	{
		$partenaire = Partenaire::model()->findByPk($partenaireId);
		if (!$partenaire) {
			throw new CHttpException(404, "Ce partenaire n'existe pas.");
		}
		if (!Yii::app()->user->checkAccess('possession/update', ['Partenaire' => $partenaire])) {
			throw new CHttpException(403, "Vous n'avez pas la permission de gérer ces possessions.");
		}

		$model = new PossessionImport;
		$model->partenaireId = (int) $partenaire->id;

		if (isset($_POST['PossessionImport'])) {
			$model->attributes = $_POST['PossessionImport'];
			$model->importfile = CUploadedFile::getInstance($model, 'importfile');
			if ($model->validate()) {
				try {
					$model->import($model->importfile->tempName);
				} catch (Exception $e) {
					$model->addError('importfile', $e->getMessage());
				}
			}
		}

		$this->getController()->render(
			'import',
			['model' => $model, 'partenaire' => $partenaire]
		);
	}
}
