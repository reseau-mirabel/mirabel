<?php

namespace controllers\possession;

use CHttpException;
use Partenaire;
use PossessionImport;
use Yii;

/**
 * Exports the "possession" of the current "Partenaire" to CSV.
 */
class ExportAction extends \CAction
{
	public function run(string $partenaireId, string $format = 'csv')
	{
		$partenaire = Partenaire::model()->findByPk((int) $partenaireId);
		if (!$partenaire) {
			throw new CHttpException(404, "Ce partenaire n'existe pas.");
		}
		if (!Yii::app()->user->checkAccess('possession/update', ['Partenaire' => $partenaire])) {
			throw new CHttpException(403, "Vous n'avez pas la permission de gérer ces possessions.");
		}
		$sql = <<<EOSQL
			SELECT 1, pt.bouquet, t.titre, t.id, i1.issn, i2.issn AS issne, IF(i1.issnl <> '', i1.issnl, i2.issnl) AS issnl, t.revueId, pt.identifiantLocal
			FROM Partenaire_Titre pt JOIN Titre t ON pt.titreId = t.id
			  LEFT JOIN Issn i1 ON (i1.titreId = t.id AND i1.support = 'papier')
			  LEFT JOIN Issn i2 ON (i2.titreId = t.id AND i2.support = 'electronique')
			WHERE pt.partenaireId = {$partenaire->id}
			ORDER BY pt.bouquet ASC, t.titre ASC
			EOSQL;
		$cmd = Yii::app()->db->createCommand($sql);

		$filename = sprintf("%s_possessions_P%d_%s", \Norm::urlParam(\Yii::app()->name), (int) $partenaire->id, date('Y-m-d'));

		\Controller::header("csv");
		if ($format === 'txt') {
			header("Content-Disposition: attachment; filename=\"$filename.txt\"");
			echo "\xEF\xBB\xBF"; // BOM for Excel
		} else {
			header("Content-Disposition: attachment; filename=\"$filename.csv\"");
		}
		$out = fopen('php://output', 'w');
		fputcsv($out, PossessionImport::$columns, ';', '"', '\\');
		foreach ($cmd->queryAll(false) as $row) {
			fputcsv($out, $row, ';', '"', '\\');
		}
	}
}
