<?php

namespace controllers\possession;

use CHttpException;
use models\exporters\Titre;
use Partenaire;
use Yii;

class ExportMoreAction extends \CAction
{
	public function run(string $partenaireId, string $format = 'csv')
	{
		$partenaire = Partenaire::model()->findByPk($partenaireId);
		if (!$partenaire) {
			throw new CHttpException(404, "Ce partenaire n'existe pas.");
		}
		if (!Yii::app()->user->checkAccess('possession/update', ['Partenaire' => $partenaire])) {
			throw new CHttpException(403, "Vous n'avez pas la permission de gérer ces possessions.");
		}

		$filename = sprintf("%s_possessions-ext_P%d_%s", \Norm::urlParam(\Yii::app()->name), (int) $partenaire->id, date('Y-m-d'));

		\Controller::header('csv');
		if ($format === 'txt') {
			header("Content-Disposition: attachment;filename=\"$filename.txt\"");
			echo "\xEF\xBB\xBF"; // BOM for Excel
		} else {
			header("Content-Disposition: attachment;filename=\"$filename.csv\"");
		}
		$exporter = new Titre();
		$exporter->partenaireId = (int) $partenaire->id;
		$exporter->possessionsToCsv(";");
	}
}
