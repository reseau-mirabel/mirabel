<?php

namespace controllers\cms;

use models\cms\BreveSearchForm;

class BrevesAction extends \CAction
{
	public function run(): void
	{
		$model = new BreveSearchForm();
		if (isset($_GET['q'])) {
			$model->setAttributes($_GET['q']);
		}

		$this->controller->layout = '//layouts/column1';
		$this->controller->render(
			'breves',
			[
				'model' => $model,
				'categories' => BreveSearchForm::listCategories(),
				'partenaires' => BreveSearchForm::listPartenaires(),
				'ressources' => BreveSearchForm::listRessources(),
				'sources' => BreveSearchForm::listSources(),
			]
		);
	}
}
