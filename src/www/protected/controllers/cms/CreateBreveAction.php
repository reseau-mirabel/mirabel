<?php

namespace controllers\cms;

use Cms;
use models\cms\BreveForm;

class CreateBreveAction extends \CAction
{
	public function run(): void
	{
		$cms = new Cms();
		$model = new BreveForm($cms);

		if (\Yii::app()->request->isPostRequest) {
			$model->setAttributes($_POST['BreveForm']);
			if ($model->validate() && $model->save()) {
				$this->controller->redirect(['view', 'id' => $cms->id]);
				return;
			}
		}

		$this->controller->render(
			'create-breve',
			['model' => $model]
		);
	}
}
