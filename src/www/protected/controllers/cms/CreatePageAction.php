<?php

namespace controllers\cms;

use Cms;

class CreatePageAction extends \CAction
{
	public function run(): void
	{
		$model = new Cms;
		$model->singlePage = true;

		if (isset($_POST['Cms'])) {
			$model->setAttributes($_POST['Cms']);
			if ($model->save()) {
				$this->controller->redirect(['view', 'id' => $model->id]);
				return;
			}
		}

		$this->controller->render(
			'create-page',
			['model' => $model]
		);
	}
}
