<?php

namespace controllers\cms;

use Cms;

class AdminAction extends \CAction
{
	public function run(string $filter = '')
	{
		$model = new Cms('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Cms'])) {
			$model->setAttributes($_GET['Cms']);
		}

		switch ($filter) {
			case 'blocs':
				$model->singlePage = false;
				if (!$model->name) {
					$model->name = 'pas de brèves';
				}
				break;
			case 'breves':
				$model->singlePage = false;
				$model->name = 'brève';
				break;
			case 'pages':
				$model->singlePage = true;
				break;
			default:
				$filter = '';
		}

		$this->controller->render(
			'admin',
			[
				'model' => $model,
				'filter' => $filter,
			]
		);
	}
}
