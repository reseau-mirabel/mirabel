<?php

namespace controllers\cms;

use CHttpException;
use Cms;

class UpdatePageAction extends \CAction
{
	public function run(string $id)
	{
		$model = Cms::model()->findByPk((int) $id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		if ($model->isBreve()) {
			$this->controller->redirect(['update-breve', 'id' => $id]);
			return;
		}

		if (isset($_POST['Cms'])) {
			$model->setAttributes($_POST['Cms']);
			if ($model->save()) {
				$this->controller->redirect(['view', 'id' => $model->id]);
				return;
			}
		}

		$this->controller->render(
			'update',
			['model' => $model]
		);
	}
}
