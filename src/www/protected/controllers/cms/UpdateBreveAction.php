<?php

namespace controllers\cms;

use CHttpException;
use Cms;
use models\cms\BreveForm;
use Yii;

class UpdateBreveAction extends \CAction
{
	public function run(string $id)
	{
		$cms = Cms::model()->findByPk((int) $id);
		if ($cms === null || !$cms->isBreve()) {
			throw new CHttpException(404, "Aucune brève n'a cet ID");
		}
		$model = new BreveForm($cms);

		if (Yii::app()->request->isPostRequest) {
			$model->setAttributes($_POST['BreveForm']);
			if ($model->validate() && $model->save()) {
				$this->controller->redirect(['view', 'id' => $cms->id]);
				return;
			}
		}

		$this->controller->render(
			'update-breve',
			[
				'id' => (int) $cms->id,
				'model' => $model,
			]
		);
	}
}
