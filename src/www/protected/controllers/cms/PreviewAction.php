<?php

namespace controllers\cms;

use CHttpException;
use Cms;

class PreviewAction extends \CAction
{
	public function run(): void
	{
		if (empty($_POST['Cms'])) {
			throw new CHttpException(400, "Wrong call, missing POST data");
		}
		$model = new Cms();
		$model->setAttributes($_POST['Cms']);
		if (isset($_POST['Cms']['singlePage'])) {
			$model->singlePage = (bool) $_POST['Cms']['singlePage'];
		}
		if (!empty($_POST['content'])) {
			$model->setAttribute('content', (string) $_POST['content']);
		}
		echo $model->toHtml();
	}
}
