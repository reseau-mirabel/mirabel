<?php

namespace controllers\cms;

use CHttpException;
use Cms;
use Yii;

class DeleteAction extends \CAction
{
	public function run(string $id)
	{
		if (!Yii::app()->request->isPostRequest) {
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}
		$model = Cms::model()->findByPk((int) $id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		$model->delete();
		$this->controller->redirect($_POST['returnUrl'] ?? ['admin']);
	}
}
