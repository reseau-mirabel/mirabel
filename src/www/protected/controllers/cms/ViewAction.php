<?php

namespace controllers\cms;

use CHttpException;
use Cms;

class ViewAction extends \CAction
{
	public function run($id)
	{
		$model = Cms::model()->findByPk((int) $id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		$this->controller->render(
			'view',
			['model' => $model]
		);
	}
}
