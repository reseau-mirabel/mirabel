<?php

namespace controllers\cms;

use models\cms\BreveIndicateurs;

class BrevesIndicateursAction extends \CAction
{
	public function run(): void
	{
		$this->controller->render(
			'breves-indicateurs',
			['indicateurs' => new BreveIndicateurs()]
		);
	}
}
