<?php

namespace controllers\titre;

use processes\completion\Titre;

class AjaxCompleteAction extends \CAction
{
	public function run(string $term)
	{
		header('Content-Type: application/json; charset="UTF-8"');
		echo json_encode((new Titre)->completeTerm($term, [], [], 'text'));
	}
}
