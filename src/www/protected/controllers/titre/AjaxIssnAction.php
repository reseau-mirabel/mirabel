<?php

namespace controllers\titre;

use BootActiveForm;
use Issn;

class AjaxIssnAction extends \CAction
{
	public function run(string $position, string $titreId)
	{
		$newIssn = new Issn();
		$newIssn->support = Issn::SUPPORT_INCONNU;
		$newIssn->titreId = (int) $titreId;

		ob_start();
		$form = $this->getController()->beginWidget(
			'bootstrap.widgets.BootActiveForm',
			[
				'id' => 'titre-form',
				'enableAjaxValidation' => false,
				'enableClientValidation' => false,
				'type' => BootActiveForm::TYPE_HORIZONTAL,
			]
		);
		ob_end_clean();

		$this->getController()->renderPartial(
			'_issn',
			[
				'form' => $form,
				'position' => $position,
				'issn' => $newIssn,
			]
		);
	}
}
