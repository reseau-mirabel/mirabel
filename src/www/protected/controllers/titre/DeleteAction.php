<?php

namespace controllers\titre;

use CHttpException;
use processes\titres\Delete;
use Titre;
use Yii;

class DeleteAction extends \CAction
{
	public function run(string $id)
	{
		$titre = Titre::model()->findByPk((int) $id);
		if (!($titre instanceof Titre)) {
			throw new CHttpException(404, "Ce titre n'a pas été trouvé.");
		}

		if (!Yii::app()->user->access()->toTitre()->delete($titre)) {
			throw new CHttpException(403, "Vous n'avez pas la permission de supprimer ce titre.");
		}

		$process = new Delete($titre);
		if (!empty($_POST['returnUrl'])) {
			$process->setReturnUrl($_POST['returnUrl']);
		}

		if (!Yii::app()->request->getPost('confirm', false) || Yii::app()->request->getPost('annoying', '') !== 'Je confirme') {
			$this->getController()->render(
				'delete',
				[
					'model' => $titre,
					'process' => $process,
					'isRedirectionRequired' => (bool) Yii::app()->db
						->createCommand("SELECT 1 FROM RedirectionTitre WHERE cibleId = {$titre->id}")
						->queryScalar(),
				]
			);
			return;
		}

		// we only allow deletion via POST request
		if (!Yii::app()->request->isPostRequest) {
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}

		$process->setRedirection((int) (
			Yii::app()->request->getPost('redirection', 0)
			?: Yii::app()->request->getPost('redirection-autocomplete', 0)
		));
		if ($process->run()) {
			$message = "Titre « {$process->titre->fullTitle} » supprimé.";
			if ($process->getRedirection() !== null) {
				$message .= " L'ancienne page redirige désormais vers {$process->getRedirection()->getSelfLink()}.";
			} else {
				$message .= " Il n'y pas de redirection pour son ancienne page.";
			}
			Yii::app()->user->setFlash('success', $message);
		} else {
			Yii::app()->user->setFlash('info', "Échec de la suppression de « {$process->titre->fullTitle} » : " . $process->getErrorMessage());
		}
		$this->getController()->redirect($process->getReturnUrl());
	}
}
