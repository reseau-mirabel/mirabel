<?php

namespace controllers\titre;

use Exception;
use processes\titres\ChangeJournal;
use Yii;

class ChangeJournalAction extends \CAction
{
	public function run(string $id)
	{
		$controller = $this->getController();
		\assert($controller instanceof \TitreController);

		$model = $controller->loadModel($id);

		if (isset($_POST['revueId'])) {
			$process = new ChangeJournal($model);
			$result = $process->run((int) $_POST['revueId']);

			if ($result === ChangeJournal::RETURN_EMPTY) {
				Yii::app()->user->setFlash('error', "Impossible de déplacer un titre fils unique vers une revue vide.");
				$controller->redirect(['view', 'id' => $model->id]);
				return;
			}
			if ($result === ChangeJournal::RETURN_NOTFOUND) {
				Yii::app()->user->setFlash('error', "La revue avec cet ID n'a pas été trouvée.");
			} elseif ($result === ChangeJournal::RETURN_ERROR) {
				Yii::app()->user->setFlash('error', "Erreur en enregistrant l'intervention : " . $process->getErrorMessage());
			} elseif ($result === ChangeJournal::RETURN_OK) {
				$controller->redirect(['/revue/view', 'id' => $process->getRevueId()]);
				return;
			} else {
				throw new Exception("ChangeJournal : code de réponse inconnu.");
			}
		}

		$controller->render(
			'changeJournal',
			[
				'model' => $model,
			]
		);
	}
}
