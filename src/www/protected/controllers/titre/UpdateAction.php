<?php

namespace controllers\titre;

use Suivi;
use Yii;
use processes\titres\Update;

class UpdateAction extends \CAction
{
	public function run($id)
	{
		$controller = $this->getController();
		assert($controller instanceof \TitreController);

		$process = new Update($controller->loadModel($id));

		$directAccess = Yii::app()->user->access()->toTitre()->updateDirect($process->model);
		if ($directAccess) {
			$controller->layout = '//layouts/column2';
		}
		$forceProposition = false;
		if ($directAccess) {
			if (isset($_POST['force_proposition'])) {
				$forceProposition = (bool) $_POST['force_proposition'];
			} elseif (!Suivi::checkDirectAccessByIds((int) $process->model->revueId, null)) {
				$forceProposition = true;
			}
		}
		if ($directAccess && $forceProposition) {
			$directAccess = false;
		}
		$process->setDirectAccess($directAccess);

		if ($process->load($_POST)) {
			$validation = $process->validate();
			if ($validation === Update::RETURN_OK) {
				if ($process->hasAddedSherpaLink() && $directAccess) {
					Yii::app()->user->setFlash('info', "Un lien vers “Open policy finder” a été ajouté automatiquement au titre.");
				}
				if ($directAccess) {
					$process->intervention->contenuJson->confirm = true;
					if ($process->intervention->accept()) {
						Yii::app()->user->setFlash('success', "Modification enregistrée.");
						$process->intervention->emailForEditorChange();
					} else {
						Yii::app()->user->setFlash('error', "L'intervention n'a pu être enregistrée : " . \components\HtmlHelper::errorSummary($process->intervention));
					}
				} elseif ($process->intervention->save(false)) {
					Yii::app()->user->setFlash(
						'success',
						"Proposition enregistrée. Merci d'avoir contribué à Mir@bel."
					);
					$process->intervention->emailForEditorChange("", "Proposition");
				}
			}
			if ($validation !== Update::RETURN_ERROR) {
				$controller->redirect(['/revue/view', 'id' => $process->model->revueId]);
				return;
			}
		}

		$controller->render(
			'update',
			[
				'model' => $process->model,
				'editeurNew' => $controller->parseNewEditorLinks(),
				'direct' => $directAccess,
				'intervention' => $process->intervention,
				'forceProposition' => $forceProposition,
				'issns' => $process->issns,
			]
		);
	}
}
