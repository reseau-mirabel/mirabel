<?php

namespace controllers\titre;

use processes\titres\Identify;

/**
 * Updates all the Identification entries.
 */
class IdentificationAction extends \CAction
{
	public function run(string $id)
	{
		$controller = $this->getController();
		\assert($controller instanceof \TitreController);

		$process = new Identify($controller->loadModel($id));

		if (isset($_POST['Identification'])) {
			if ($process->load($_POST['Identification']) && $process->save()) {
				$controller->redirect(['view', 'id' => $process->titre->id]);
				return;
			}
		}

		$controller->render(
			'identification',
			[
				'process' => $process,
			]
		);
	}
}
