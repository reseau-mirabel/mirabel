<?php

namespace controllers\titre;

use Intervention;
use Titre;
use Yii;

class ViewAction extends \CAction
{
	public function run(string $id)
	{
		$controller = $this->getController();
		\assert($controller instanceof \TitreController);

		$model = Titre::model()->findByPk((int) $id);
		if ($model === null) {
			$url = (new \processes\redirection\Redirector('Titre'))->getRoute((int) $id);
			if ($url) {
				$this->getController()->redirect($url, true, 301);
				return null;
			}
			$model = null;
			throw new \CHttpException(404, "Le titre demandé n'existe pas.");
		}
		$model->id = (int) $model->id;

		if (Yii::app()->user->checkAccess("avec-partenaire")) {
			$controller->layout = '//layouts/column2';
		}
		$lastUpdate = Intervention::model()->findBySql(
			<<<EOSQL
			SELECT id, hdateProp, hdateVal
			FROM Intervention
			WHERE titreId = {$model->id} AND statut = 'accepté' AND ressourceId IS NULL
			  AND (`action` = 'revue-C' OR `action` IS NULL)
			ORDER BY hdateVal DESC
			LIMIT 1
			EOSQL
		);
		$controller->render(
			'view',
			[
				'model' => $model,
				'lastUpdate' => $lastUpdate,
				'autresTitres' => $model->revue->getTitres(),
				'variantes' => self::getTitreVariantes($model->id),
			]
		);
	}

	private static function getTitreVariantes(int $id): array
	{
		$rows = Yii::app()->db
			->createCommand(<<<SQL
				WITH Ranked AS (
					SELECT variante, lang, zoneSudoc, ROW_NUMBER() OVER(PARTITION BY lang,variante ORDER BY id ASC) AS rank
					FROM TitreVariante
					WHERE titreId = {$id}
				)
				SELECT variante, lang FROM Ranked WHERE rank = 1 ORDER BY lang, variante
				SQL
			)
			->queryAll(false);
		return array_map(
			fn ($x) => [$x[0], \models\lang\Codes::CODES3[$x[1]] ?? $x[1]],
			$rows
		);
	}
}
