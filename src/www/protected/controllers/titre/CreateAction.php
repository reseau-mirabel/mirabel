<?php

namespace controllers\titre;

use Suivi;
use Yii;
use processes\titres\Create;

class CreateAction extends \CAction
{
	public function run(string $revueId = '0', string $state = '')
	{
		$process = new Create((int) ($_POST['Titre']['revueId'] ?? $revueId));

		// if coming from create-by-issn, load the ISSN data.
		if ($state) {
			$stateData = Yii::app()->user->getState("createByIssn_{$state}");
			Yii::app()->user->setState("createByIssn_{$state}", null);
			if ($stateData) {
				if (!empty($stateData['sudocNotices'])) {
					$process->addSudocNotices($stateData['sudocNotices']);
					Yii::app()->user->setFlash(
						'info',
						"Ce nouveau titre est pré-rempli avec les données de l'ISSN."
						. " Complétez, et vérifiez notamment si les dates du titre coïncident réellement avec celles de son ISSN."
					);
				}
				if (!empty($stateData['extra'])) {
					$process->addExtraState($stateData['extra']);
				}
			}
		}

		$directAccess = Yii::app()->user->access()->toTitre()->createDirect((int) $process->model->revueId);
		$forceProposition = false;
		if ($directAccess) {
			if (isset($_POST['force_proposition'])) {
				$forceProposition = (bool) $_POST['force_proposition'];
			} elseif (!Suivi::checkDirectAccessByIds((int) $process->model->revueId, null)) {
				$forceProposition = true;
			}
		}
		if ($directAccess && $forceProposition) {
			$directAccess = false;
		}
		$process->setDirectAccess($directAccess);

		$controller = $this->getController();
		assert($controller instanceof \TitreController);

		if ($process->load($_POST) && $process->validate()) {
			if ($process->hasAddedSherpaLink() && $directAccess) {
				Yii::app()->user->setFlash('info', "Un lien vers “Open policy finder” a été ajouté automatiquement au nouveau titre.");
			}
			$process->fillIntervention();
			if ($controller->validateIntervention($process->intervention, $directAccess) && $process->save()) {
				return $controller->flashAndRedirect($process->getRedirection());
			}
			$errors = [];
			foreach ($process->intervention->getErrors() as $attr => $perAttr) {
				$errors[] = "<li><strong>$attr</strong><br>" . join("<br>", $perAttr) . "</li>";
			}
			Yii::app()->user->setFlash('error', "Erreur lors de l'enregistrement : <ul>" . join("", $errors) . "</ul>");
		}

		$controller->render(
			'create',
			[
				'model' => $process->model,
				'editeurNew' => $controller->parseNewEditorLinks(),
				'direct' => $directAccess,
				'intervention' => $process->intervention,
				'forceProposition' => $forceProposition,
				'issns' => $process->issns,
			]
		);
	}
}
