<?php

namespace controllers\titre;

use CHttpException;
use processes\titres\PolitiquePublication;
use Yii;

class PublicationAction extends \CAction
{
	private const SOURCELIEN_DOAJ = 1;

	public function run($id)
	{
		$controller = $this->getController();
		assert($controller instanceof \TitreController);

		$titre = $controller->loadModel($id);
		$polPub = new PolitiquePublication($titre);
		$source = $polPub->getSource();
		if ($source === 0) {
			// Pas dans Sherpa, ni dans Mirabel
			throw new CHttpException(404, "Ce titre n'a pas de données sur sa politique de publication (ni source Open policy finder, ni politique validée par Mir@bel).");
		}
		try {
			$publication = $polPub->getItem()->getPublication();
		} catch (\Exception $e) {
			Yii::log($e->getMessage(), 'error');
			throw new CHttpException(500, "Les données sont actuellement incomplètes. La correction est en cours.");
		}

		if ($source === PolitiquePublication::SOURCE_SHERPA) {
			$intro = preg_replace(
				'/{{SHERPA_LINK (.+?)}}/',
				"<a href=\"$publication->romeoUrl\">$1</a>",
				str_replace(
					['{{SHERPA_URL}}'],
					[$publication->romeoUrl],
					\Config::read("sherpa.intro")
				)
			);
		} else {
			$intro = \Config::read("sherpa.intro-politique-mirabel");
		}

		$controller->render(
			'publication',
			[
				'titre' => $titre,
				'doajUrl' => Yii::app()->db
					->createCommand("SELECT url FROM LienTitre WHERE titreId = :tid AND sourceId = :sid")
					->queryScalar([':tid' => $titre->id, ':sid' => self::SOURCELIEN_DOAJ]),
				'publication' => $publication,
				'intro' => $intro,
				'old' => ($source === PolitiquePublication::SOURCE_MIRABEL) ? $polPub->getItemSherpa() : null,
			]
		);
	}
}
