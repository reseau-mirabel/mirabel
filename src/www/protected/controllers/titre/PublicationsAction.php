<?php

namespace controllers\titre;

use CHttpException;
use processes\titres\PolitiquePublication;
use Yii;

class PublicationsAction extends \CAction
{
	private const SOURCELIEN_DOAJ = 1;

	public function run($id)
	{
		$controller = $this->getController();
		assert($controller instanceof \TitreController);

		$titre = $controller->loadModel($id);
		$polPub = new PolitiquePublication($titre);
		$publications = $polPub->getAllPublications();
		if (!$publications) {
			// Pas dans Sherpa, ni dans Mirabel
			throw new CHttpException(404, "Ce titre n'a pas de données sur sa politique de publication (ni source Open policy finder, ni politique dans Mir@bel).");
		}

		$controller->render(
			'publications',
			[
				'titre' => $titre,
				'doajUrl' => Yii::app()->db
					->createCommand("SELECT url FROM LienTitre WHERE titreId = :tid AND sourceId = :sid")
					->queryScalar([':tid' => $titre->id, ':sid' => self::SOURCELIEN_DOAJ]),
				'publications' => $publications,
			]
		);
	}
}
