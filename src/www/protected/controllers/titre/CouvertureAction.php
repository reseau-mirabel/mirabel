<?php

namespace controllers\titre;

use processes\titres\Cover;

/**
 * Download the cover image.
 */
class CouvertureAction extends \CAction
{
	public function run(string $id)
	{
		$controller = $this->getController();
		\assert($controller instanceof \TitreController);

		$model = $controller->loadModel($id);
		Cover::update($model);
		$controller->redirect(['/revue/view', 'id' => $model->revueId]);
	}
}
