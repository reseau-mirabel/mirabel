<?php

namespace controllers\titre;

use IssnForm;
use Revue;
use Throwable;
use Yii;

/**
 * Étape préliminaire à /titre/create pour interroger le Sudoc par ISSN et ainsi préremplir le formulaire de titre.
 *
 * Variantes :
 * /titre/create-by-issn : création d'une revue via un formulaire de saisie d'ISSN
 * /titre/create-by-issn?revueId=14 : création d'un titre dans la revue d'ID 14
 * /titre/create-by-issn?issn=1234-5678 : création d'un titre avec formulaire prérempli
 */
class CreateByIssnAction extends \CAction
{
	public function run(string $revueId = "0", $extra = "")
	{
		$model = new IssnForm();
		if ($revueId > 0) {
			$model->revueId = (int) $revueId;
			$revue = Revue::model()->findByPk($revueId);
		} else {
			$revue = null;
		}

		$extraDecoded = null;
		if ($extra) {
			$extraDecoded = json_decode($extra, true, 4);
		}

		if (isset($_POST['IssnForm'])) {
			$model->setAttributes($_POST['IssnForm']);
			if ($model->validate()) {
				$rng = substr(uniqid(), -8);
				try {
					Yii::app()->user->setState("createByIssn_{$rng}", ['sudocNotices' => $model->getSudocNotices(), 'extra' => $extraDecoded]);
					$url = ['create', 'state' => $rng];
					if ($model->revueId) {
						$url['revueId'] = $model->revueId;
					}
					return $this->getController()->redirect($url);
				} catch (Throwable $e) {
					$model->addError('issn', "Erreur en interrogeant le Sudoc : « {$e->getMessage()} ». Recommencer plus tard, ou créer sans ISSN.");
				}
			}
		} elseif (isset($_GET['issn'])) {
			$model->issn = $_GET['issn'];
		}

		$this->getController()->render(
			'create-by-issn',
			['model' => $model, 'revue' => $revue, 'extra' => $extraDecoded]
		);
	}
}
