<?php

namespace controllers\titre;

use CHttpException;
use processes\titres\Editeurs;
use Yii;

class EditeursAction extends \CAction
{
	public function run(string $id, string $e = '')
	{
		$controller = $this->getController();
		\assert($controller instanceof \TitreController);

		$titre = $controller->loadModel($id);
		if (!Yii::app()->user->access()->toTitre()->relationEditeurs($titre)) {
			throw new CHttpException(403);
		}
		$model = new Editeurs((int) Yii::app()->user->id, $titre, (int) $e);
		if (Yii::app()->getRequest()->isPostRequest) {
			$intervention = $model->createIntervention($_POST['TitreEditeur']);
			if ($intervention !== null && !$intervention->contenuJson->isEmpty()) {
				if (Yii::app()->user->access()->toTitre()->updateEditeursDirect($titre)) {
					$intervention->accept(false);
				}
				if ($intervention->save(false)) {
					$message = $intervention->statut === 'attente' ? " Elles seront appliquées après validation par les partenaires en charge de ce titre." : "";
					Yii::app()->user->setFlash('success', "Les modifications sont enregistrées.$message");
					if ($model->returnToPolitique) {
						$controller->redirect(['/politique/index', 'editeurId' => $model->returnToPolitique]);
					} else {
						$controller->redirect(['/revue/view', 'id' => $titre->revueId]);
					}
					return;
				}
				Yii::app()->user->setFlash('danger', "Erreur lors de l'enregistrement. Contactez l'équipe de Mir@bel.");
			}
		}
		$controller->render('editeurs', ['model' => $model]);
	}
}
