<?php

namespace controllers\titre;

use CHttpException;
use processes\titres\AjaxSudoc;
use const YII_DEBUG;

class AjaxSudocAction extends \CAction
{
	public function run(string $issn = '', string $issns = '', string $ppn = '', string $withHtml = '')
	{
		$controller = $this->getController();
		\assert($controller instanceof \Controller);
		\Controller::header('json');

		$_SERVER['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest';

		$process = new AjaxSudoc();
		if ($ppn) {
			$process->queryByPpn($ppn);
		} elseif ($issn) {
			$process->queryByIssn($issn);
		} elseif ($issns) {
			$process->queryByIssns($issns);
		} else {
			throw new CHttpException(402, "An issn|issns|ppn parameter is mandatory.");
		}
		if ($process->getErrorMessage() !== '') {
			throw new CHttpException(500, $process->getErrorMessage());
		}

		http_response_code($process->getResponseCode());
		echo json_encode(
			$process->getResponseBody((bool) $withHtml),
			JSON_UNESCAPED_UNICODE | (YII_DEBUG ? JSON_PRETTY_PRINT : 0)
		);
	}
}
