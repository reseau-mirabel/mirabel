<?php

namespace controllers\titre;

use CHttpException;
use processes\titres\ExportTitles;
use Yii;

/**
 * Export all.
 */
class ExportAction extends \CAction
{
	public function run(string $type = '')
	{
		if (!$this->checkAccess($type)) {
			return;
		}

		$controller = $this->getController();
		\assert($controller instanceof \TitreController);

		$basename = \Norm::urlParam(\Yii::app()->name);
		switch ($type) {
			case 'bnf':
				\Controller::header('csv');
				header(sprintf('Content-Disposition: attachment; filename="%s_%s_BnF.csv"', $basename, date('Y-m-d')));
				ExportTitles::printBnfCsv();
				break;
			case 'identifiants':
				\Controller::header('csv');
				header(sprintf('Content-Disposition: attachment; filename="%s_%s_identifiants.csv"', $basename, date('Y-m-d')));
				$all = Yii::app()->user->checkAccess("avec-partenaire");
				ExportTitles::printIdentifiersCsv($all);
				break;
			case 'maxi':
				\Controller::header('csv');
				header(sprintf('Content-Disposition: attachment; filename="%s_%s_maxi.csv"', $basename, date('Y-m-d')));
				ExportTitles::printMaxiCsv();
				break;
			case 'ppn':
				\Controller::header('csv');
				header(sprintf('Content-Disposition: attachment; filename="%s_%s_ppn_url-mirabel.csv"', $basename, date('Y-m-d')));
				ExportTitles::printPpnCsv();
				break;
			default:
				$controller->renderPartial('export');
		}
	}

	/**
	 * Selon le type demandé :
	 *  - ppn | bnf | identifiants : ouvert à tous
	 *  - *autres* : utilisateurs authentifiés
	 */
	private function checkAccess(string $type): bool
	{
		if ($type === 'ppn' || $type === 'bnf' || $type === 'identifiants') {
			return true;
		}

		$user = Yii::app()->user;
		if (!$user->checkAccess("avec-partenaire")) {
			$user->loginRequired();
			return false;
		}
		if (!$user->access()->toTitre()->admin()) {
			throw new CHttpException(403, "Accès restreint");
		}
		return true;
	}
}
