<?php

/**
 * Permet d'accéder aux statistiques de fréquentation générées par AWstats.
 */
class StatsaccesController extends Controller
{
	private const ROOT_DIR = '/../../statistiques/';

	private const ROOT_FILE_PREFIX = 'awstats.mirabel.sciencespo-lyon.fr';

	private const ROOT_FILE_SUFFIX = '.fr.html';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return ['accessControl'];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			['allow', 'actions' => ['liens'], 'users' => ['*']],
			['allow', 'actions' => ['index'], 'ips' => preg_split('/[\s,]+/', Config::read('server.ip'))],
			['allow', 'roles' => ['avec-partenaire'], ],
			['deny', 'users' => ['*']],
		];
	}

	/**
	 * Maps actions to dedicated classes.
	 */
	public function actions(): array
	{
		return [
			'index' => 'controllers\statsacces\IndexAction',
		];
	}

	/**
	 * @param string $actionID
	 */
	public function missingAction($actionID)
	{
		if (preg_match('/^\d{4}-\d\d$/', $actionID)) {
			$this->showMonthlyPage($actionID);
			return;
		}
		$m = [];
		if (preg_match('/^(\d{4}-\d\d)-(.+)$/', $actionID, $m)) {
			Yii::app()->session->add('statsaccess-date', $m[1]);
			$this->readStats($m[2]);
			return;
		}
		$this->readStats($actionID);
	}

	public static function getStatsPath(string $date, string $file): string
	{
		return realpath(Yii::app()->basePath . self::ROOT_DIR)
			. ($date ? '/' . str_replace('-', '/', $date) : '')
			. ($file ? '/' . $file : '');
	}

	private function showMonthlyPage(string $date)
	{
		Yii::app()->session->add('statsaccess-date', $date);
		$this->redirect(["/statsacces/$date-" . self::ROOT_FILE_PREFIX . self::ROOT_FILE_SUFFIX]);
	}

	private function readStats(string $name)
	{
		$date = Yii::app()->getSession()->get('statsaccess-date') ?? '';
		$filename = self::getStatsPath($date, $name);
		if (!file_exists($filename)) {
			Yii::log("Cannot read '{$filename}' for requested '{$name}'", CLogger::LEVEL_ERROR);
			throw new CHttpException(404, "Ce fichier des statistiques d'accès est introuvable.");
		}
		if (!Yii::app()->user->checkAccess("avec-partenaire")) {
			throw new CHttpException(404, "Les statistiques d'accès sont réservées aux utilisateurs authentifiés.");
		}
		readfile($filename);
	}
}
