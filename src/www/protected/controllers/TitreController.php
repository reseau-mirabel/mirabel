<?php

class TitreController extends Controller
{
	/**
	 * @var string The default layout for the views.
	 *   See 'protected/views/layouts/column1.php'.
	 */
	public $layout = '//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl',
			[
				// cache on "/titre/publication"
				'COutputCache + publication',
				'duration' => YII_DEBUG ? 5 : 3600,
				'varyByParam' => ['id'],
				'varyBySession' => false,
			],
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'actions' => ['ajax-complete', 'ajax-complete', 'ajax-issn', 'ajax-sudoc', 'create', 'create-by-issn', 'export', 'publication', 'update'],
				'users' => ['*'],
			],
			[
				'allow',
				'actions' => ['editeurs', 'publications'],
				'users' => ['@'],
			],
			[
				'allow',
				'actions' => ['couverture', 'delete', 'identification', 'view'],
				'roles' => ['avec-partenaire'],
			],
			[
				'allow',
				'actions' => ['change-journal'],
				'roles' => ['titre:admin'],
			],
			[
				'deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	/**
	 * Maps actions to dedicated classes.
	 */
	public function actions()
	{
		return [
			'ajax-complete' => 'controllers\titre\AjaxCompleteAction',
			'ajax-issn' => 'controllers\titre\AjaxIssnAction',
			'ajax-sudoc' => 'controllers\titre\AjaxSudocAction',
			'create' => 'controllers\titre\CreateAction',
			'create-by-issn' => 'controllers\titre\CreateByIssnAction',
			'change-journal' => 'controllers\titre\ChangeJournalAction',
			'couverture' => 'controllers\titre\CouvertureAction',
			'delete' => 'controllers\titre\DeleteAction',
			'export' => 'controllers\titre\ExportAction',
			'editeurs' => 'controllers\titre\EditeursAction',
			'identification' => 'controllers\titre\IdentificationAction',
			'publication' => 'controllers\titre\PublicationAction',
			'publications' => 'controllers\titre\PublicationsAction',
			'update' => 'controllers\titre\UpdateAction',
			'view' => 'controllers\titre\ViewAction',
		];
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 *
	 * If the data model is not found, an HTTP exception will be raised.
	 *
	 * @param string $id the ID of the model to be loaded
	 * @return Titre
	 */
	public function loadModel($id)
	{
		$model = Titre::model()->findByPk((int) $id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		return $model;
	}

	public function parseNewEditorLinks()
	{
		$editeurNew = [];
		if (!empty($_POST['TitreEditeurNew'])) {
			foreach ($_POST['TitreEditeurNew'] as $relation) {
				$editeurNew[] = Editeur::model()->findByPk($relation['editeurId'] ?? 0);
			}
		}
		return array_filter($editeurNew);
	}
}
