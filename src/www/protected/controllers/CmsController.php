<?php

class CmsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	public $defaultAction = 'admin';

	public function actions()
	{
		return [
			'admin' => \controllers\cms\AdminAction::class,
			'breves' => \controllers\cms\BrevesAction::class,
			'breves-indicateurs' => \controllers\cms\BrevesIndicateursAction::class,
			'create-breve' => \controllers\cms\CreateBreveAction::class,
			'create-page' => \controllers\cms\CreatePageAction::class,
			'delete' => \controllers\cms\DeleteAction::class,
			'preview' => \controllers\cms\PreviewAction::class,
			'update-breve' => \controllers\cms\UpdateBreveAction::class,
			'update-page' => \controllers\cms\UpdatePageAction::class,
			'view' => \controllers\cms\ViewAction::class,
		];
	}

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'actions' => ['admin', 'create-page', 'delete', 'preview', 'update', 'update-page', 'view'],
				'roles' => ['redaction'],
			],
			[
				'allow',
				'actions' => ['breves', 'breves-indicateurs', 'create-breve', 'update-breve'],
				'roles' => ['redaction/editorial'], // equals "admin" for now
			],
			[
				'deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	public function actionUpdate($id)
	{
		$model = Cms::model()->findByPk((int) $id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		if ($model->isBreve()) {
			$this->redirect(['update-breve', 'id' => $model->id]);
		}
		$this->redirect(['update-page', 'id' => $model->id]);
	}
}
