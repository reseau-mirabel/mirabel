<?php

class ServiceController extends Controller
{
	/**
	 * @var string the default layout for the views. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column1';

	public function filters(): array
	{
		return [
			'accessControl',
		];
	}

	/**
	 * This method is used by the 'accessControl' filter.
	 */
	public function accessRules(): array
	{
		return [
			[
				'allow',
				'actions' => ['contact', 'create', 'export', 'update'],
				'users' => ['*'],
			],
			[
				'allow',
				'roles' => ['avec-partenaire'],
			],
			[
				'deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	/**
	 * Maps actions to dedicated classes.
	 */
	public function actions(): array
	{
		return [
			'contact' => 'controllers\service\Contact',
			'create' => 'controllers\service\Create',
			'delete' => 'controllers\service\Delete',
			'export' => 'controllers\service\Export',
			'update' => 'controllers\service\Update',
		];
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 *
	 * If the data model is not found, an HTTP exception will be raised.
	 *
	 * @param string $id the ID of the model to be loaded
	 * @return Service
	 */
	public function loadModel($id)
	{
		$model = Service::model()->findByPk((int) $id);
		if ($model === null) {
			throw new CHttpException(404, "Cet accès en ligne n'a pas été trouvé.");
		}
		return $model;
	}
}
