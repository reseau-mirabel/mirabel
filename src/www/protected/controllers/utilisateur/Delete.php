<?php

namespace controllers\utilisateur;

use CHtml;
use CHttpException;
use Utilisateur;
use Yii;

class Delete extends \CAction
{
	/**
	 * Disable a record in the Utilisateur table.
	 *
	 * @param int $id the ID of the model to be deleted
	 */
	public function run($id)
	{
		$controller = $this->getController();
		assert($controller instanceof \UtilisateurController);

		$user = $controller->loadModel($id);
		$this->checkAccess($user);

		$policyRoles = $user->getEditeursRoles();
		$user->actif = false;
		if (!$user->partenaireId) {
			$user->nom = '';
			$user->prenom = '';
			$user->nomComplet = '';
			$user->email = preg_replace_callback(
				'/^(.)(.*)@/',
				function ($m) {
					return $m[1] . str_repeat('X', strlen($m[2])) . '@';
				},
				$user->email
			);
			$user->login = '_deleted_' . $user->email . uniqid('_');
			$user->save(false);
			if (Yii::app()->user->id == $user->id) {
				Yii::app()->user->setFlash('success', "Votre compte personnel a été effacé.");
				Yii::app()->user->logout();
				$controller->redirect('/');
				return;
			}
			Yii::app()->user->setFlash('success', "Les données personnelles du compte ont été effacées.");
			$controller->redirect('/admin/politique-utilisateurs');
			return ;
		}
		$user->save(false);
		$controller->warnOnRemovedPolicyRights($policyRoles);

		// if AJAX request (triggered via admin grid view), we should not redirect the browser
		if (!isset($_GET['ajax'])) {
			Yii::app()->user->setFlash(
				'success',
				"L'utilisateur " . CHtml::encode($user->nom) . " a été désactivé.<br />"
				. "Il peut être réactivé en modifiant son profil."
			);
			$controller->redirect($_POST['returnUrl'] ?? ['view', 'id' => $user->id]);
		}
	}

	private function checkAccess(Utilisateur $user): void
	{
		if (!Yii::app()->request->isPostRequest) {
			// we only allow disabling via POST request
			throw new CHttpException(400, 'Requête invalide. Il est possible que JavaScript soit désactivé dans votre navigateur. Activez-le et recommencez.');
		}
		if (!Yii::app()->user->access()->toUtilisateur()->disable($user)) {
			throw new CHttpException(403);
		}
	}
}
