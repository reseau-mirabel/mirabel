<?php

namespace controllers\utilisateur;

use Config;
use Utilisateur;
use Yii;
use components\email\Mailer;
use models\forms\Login;

class ResetPassword extends \CAction
{
	/**
	 * Reset the password of an user identified by login or email.
	 */
	public function run(): void
	{
		$controller = $this->getController();
		assert($controller instanceof \UtilisateurController);

		$model = new Login();
		$user = null;
		$misconfigured = !Mailer::checkConfig();
		$notfound = false;

		if (isset($_POST['LoginForm'])) {
			$model->attributes = $_POST['LoginForm'];
			if ($model->validate(['username'])) {
				$user = Utilisateur::findByName($model->username);
			}
			if (!$user || $user->authmethod == Utilisateur::AUTHMETHOD_LDAP || !$user->email) {
				$notfound = true;
			}
		}
		if ($user && !$misconfigured && !$notfound) {
			$password = Utilisateur::generateRandomPassword();
			$user->storePassword($password);
			if ($user->save(false)) {
				if ($this->mailPassword($user, $password)) {
					Yii::app()->user->setFlash('success', "Un nouveau mot de passe vous a été envoyé par mail.");
					$controller->redirect(['site/login']);
				} else {
					Yii::log("/utilisateur/reset-password: sending mail failed.", "error");
					Yii::app()->user->setFlash('error', "L'envoi par mail n'a pas fonctionné. L'erreur a été signalée aux administrateurs.");
				}
			} else {
				Yii::log("/utilisateur/reset-password: could NOT save new password '$password'", "error");
				Yii::app()->user->setFlash('error', "Un nouveau mot de passe n'a pu être généré. L'erreur a été signalée aux administrateurs.");
			}
		}
		$controller->render(
			'reset-password',
			[
				'model' => $model,
				'misconfigured' => $misconfigured,
				'notfound' => $notfound,
			]
		);
	}

	/**
	 * Send an e-mail with the new password.
	 *
	 * @param Utilisateur $user
	 * @param string $clearPassword
	 * @return bool Success?
	 */
	private function mailPassword(Utilisateur $user, $clearPassword)
	{
		$subject = sprintf("[%s] %s", \Yii::app()->name, Config::read('password.mailOnReset.subject') ?? "Nouveau mot de passe");
		$message = Mailer::newMail()
			->subject($subject)
			->from(Config::read('email.from'))
			->to($user->email)
			->text(
				str_replace(
					['%LOGIN%', '%PASSWORD%', '%URL%'],
					[$user->login, $clearPassword, Yii::app()->getBaseUrl(true)],
					(string) Config::read('password.mailOnReset.body')
				)
			);
		$replyTo = Config::read('email.replyTo');
		if ($replyTo) {
			$message->replyTo($replyTo);
		}
		return Mailer::sendMail($message);
	}
}
