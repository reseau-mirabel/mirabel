<?php

namespace controllers\utilisateur;

use Yii;
use models\forms\UtilisateurMultiForm;

class CreateMulti extends \CAction
{
	public function run(): void
	{
		if (!Yii::app()->params->itemAt('utilisateurs-par-lot')) {
			throw new \CHttpException(403, "Cette page n'est pas activée. Ajouter le paramètre 'utilisateurs-par-lot' au fichier de configuration.");
		}

		$model = new UtilisateurMultiForm();

		if (Yii::app()->request->isPostRequest) {
			$model->setAttributes($_POST['UtilisateurMultiForm']);
			if ($model->validate()) {
				$createdUsers = $model->save();
				if ($createdUsers) {
					$first = $createdUsers[0];
					$last = $createdUsers[count($createdUsers) - 1];
					Yii::app()->user->setFlash('success', "Ajout des {$model->nombre} utilisateurs, de '{$first->login}' à '{$last->login}'.");
					$this->getController()->redirect(['/utilisateur/admin', 'UtilisateurSearch' => ['partenaireId' => $first->partenaireId]]);
					return;
				}
				Yii::app()->user->setFlash('error', "Aucun des {$model->nombre} utilisateurs demandés n'a été créé.");
			}
		}

		$this->getController()->render(
			'createmulti',
			['model' => $model]
		);
	}
}
