<?php

namespace controllers\utilisateur;

use CHttpException;
use Utilisateur;
use models\forms\UserPolitique;
use Yii;

class PolitiquesEditeurs extends \CAction
{
	public function run($id = '')
	{
		$controller = $this->getController();
		assert($controller instanceof \UtilisateurController);

		if (!$id) {
			$id = Yii::app()->user->id;
		}
		$user = $controller->loadModel($id);
		$this->checkAccess($user);

		$model = new UserPolitique();
		if (isset($_POST['UserPolitique'])) {
			$model->setAttributes($_POST['UserPolitique']);
			if ($model->validate(['editeurIds'])) {
				if (!self::saveRoles((int) $user->id, $model)) {
					throw new CHttpException(500, "Erreur lors de l'enregistrement");
				}
				Yii::app()->user->setFlash('success', count($model->editeurIds) > 1 ? "Rôles ajoutés." : "Rôle ajouté.");
				$controller->redirect(['politiques-editeurs', 'id' => $id]);
				return;
			}
		}
		$controller->render(
			'politiques-editeurs',
			[
				'user' => $user,
				'model' => $model,
				'roles' => Yii::app()->db
					->createCommand("SELECT editeurId, role, lastUpdate FROM Utilisateur_Editeur WHERE utilisateurId = :uid ORDER BY lastUpdate DESC")
					->queryAll(true, [':uid' => $user->id]),
			]
		);
	}

	private function checkAccess(Utilisateur $user): void
	{
		if (!Yii::app()->user->access()->toPolitique()->addPublisher($user)) {
			throw new CHttpException(403);
		}
	}

	private function saveRoles(int $userId, UserPolitique $model): bool
	{
		$canSelectRole = Yii::app()->user->access()->toPolitique()->validate();
		$roles = $_POST['UserPolitique']['roles'] ?? [];
		$transaction = Yii::app()->db->beginTransaction();
		$insert = Yii::app()->db->createCommand("INSERT IGNORE INTO Utilisateur_Editeur (utilisateurId, editeurId, role, confirmed) VALUES ({$userId}, :eid, :role, :confirmed)");
		foreach ($model->editeurIds as $eid) {
			$role = $roles[$eid] ?? \Politique::ROLE_OWNER;
			if (!$insert->execute([':eid' => $eid, ':role' => $role, ':confirmed' => (int) $canSelectRole])) {
				$transaction->rollback();
				Yii::log("Erreur lors de l'enregistrement d'un rôle Utilisateur-Politique : ($eid, $role)", 'error');
				return false;
			}

			// Save a log a this role change
			if (!$canSelectRole) {
				$log = new \PolitiqueLog();
				$log->action = \PolitiqueLog::ACTION_ASK_FOR_ROLE;
				$log->editeurId = $eid;
				$log->politiqueId = 0;
				$log->publisher_policy = '{}';
				$log->titreId = 0;
				$log->utilisateurId = $userId;
				$log->save(false);
			}
		}
		$transaction->commit();
		return true;
	}
}
