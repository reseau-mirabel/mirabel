<?php

namespace controllers\utilisateur;

use CHttpException;
use models\forms\User;
use Yii;

class Update extends \CAction
{
	public function run(string $id)
	{
		$controller = $this->getController();
		assert($controller instanceof \UtilisateurController);

		$record = $controller->loadModel($id);
		$wasActive = (bool) $record->actif;
		$policyRoles = $record->getEditeursRoles();
		$model = new User($record);
		if (!Yii::app()->user->access()->toUtilisateur()->update($record)) {
			throw new CHttpException(403, "Vous n'avez pas la permission de modifier ce compte.");
		}
		if (!Yii::app()->user->access()->toUtilisateur()->admin() && Yii::app()->user->id == $model->id) {
			$model->setScenario('updateown');
		}

		if (isset($_POST['User'])) {
			$model->setAttributes($_POST['User']);
			if ($model->save()) {
				if ($wasActive && !$record->actif) {
					Yii::app()->user->setFlash('success', "Le compte de l'utilisateur a été désactivé.");
					$controller->warnOnRemovedPolicyRights($policyRoles);
				} else {
					Yii::app()->user->setFlash('success', "Le compte de l'utilisateur a été modifié.");
				}
				$controller->redirect(['view', 'id' => $model->id]);
				return;
			}
		}

		$controller->render(
			'update',
			['model' => $model]
		);
	}
}
