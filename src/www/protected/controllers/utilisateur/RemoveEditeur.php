<?php

namespace controllers\utilisateur;

use CHttpException;
use Utilisateur;
use Yii;

class RemoveEditeur extends \CAction
{
	public function run($id = '')
	{
		$controller = $this->getController();
		assert($controller instanceof \UtilisateurController);

		if (!$id) {
			$id = Yii::app()->user->id;
		}
		$user = $controller->loadModel($id);
		$editeurId = (int) Yii::app()->request->getPost('editeurId');
		$this->checkAccess($user, $editeurId);

		Yii::app()->db
			->createCommand("DELETE FROM Utilisateur_Editeur WHERE utilisateurId = :uid AND editeurId = :eid")
			->execute([':uid' => $user->id, ':eid' => $editeurId]);
		Yii::app()->user->setFlash('success', "Rôle supprimé.");
		$controller->redirect(['/utilisateur/politiques-editeurs', 'id' => $user->id]);
	}

	private function checkAccess(Utilisateur $user, int $editeurId): void
	{
		if (!Yii::app()->request->isPostRequest) {
			throw new CHttpException(400);
		}
		if (!Yii::app()->user->access()->toPolitique()->removePublisher($user, $editeurId)) {
			throw new CHttpException(403);
		}
	}
}
