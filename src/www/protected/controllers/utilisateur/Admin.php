<?php

namespace controllers\utilisateur;

use components\SqlHelper;
use UtilisateurSearch;

class Admin extends \CAction
{
	public function run(): void
	{
		$model = new UtilisateurSearch();
		$model->unsetAttributes();  // clear any default values
		$model->actif = true;

		if (isset($_GET['UtilisateurSearch'])) {
			$model->setAttributes($_GET['UtilisateurSearch']);
		}

		$this->getController()->render(
			'admin',
			[
				'model' => $model,
				'politiques' => SqlHelper::sqlToPairs("SELECT utilisateurId, count(*) FROM Utilisateur_Editeur GROUP BY utilisateurId"),
			]
		);
	}
}
