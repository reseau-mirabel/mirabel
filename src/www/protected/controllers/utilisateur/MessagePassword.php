<?php

namespace controllers\utilisateur;

use CHttpException;
use Config;
use Utilisateur;
use Yii;
use components\email\Mailer;
use models\forms\PasswordMailForm;

class MessagePassword extends \CAction
{
	/**
	 * Display a form to edit the email that will be sent. Then set a new password and send the email.
	 *
	 * @param int $id
	 */
	public function run($id)
	{
		$controller = $this->getController();
		assert($controller instanceof \UtilisateurController);

		$model = $controller->loadModel($id);
		$this->checkAccess($model);
		if (!$model->email) {
			Yii::app()->user->setFlash('error', "Cet utilisateur n'a pas d'adresse électronique à laquelle lui envoyer un message.");
			$controller->redirect(['view', 'id' => $model->id]);
		}

		$formData = new PasswordMailForm();
		if ($model->partenaire->type === "editeur") {
			$formData->body = Config::read('password.mailOnCreate.EditeurPartenaire.body');
			$formData->subject = Config::read('password.mailOnCreate.EditeurPartenaire.subject');
		} else {
			$formData->body = Config::read('password.mailOnCreate.body');
			$formData->subject = Config::read('password.mailOnCreate.subject');
		}

		if (isset($_POST['form'])) { // 'form' from Controller::init()
			$formData->attributes = $_POST['form'];
			$uncryptedPassword = Utilisateur::generateRandomPassword();
			$model->storePassword($uncryptedPassword);
			if ($formData->validate() && $model->save(false)) {
				$message = Mailer::newMail()
					->subject($formData->subject)
					->from(Config::read('email.from'))
					->setTo($model->email)
					->text($formData->fillBody($model, $uncryptedPassword));
				$replyTo = Config::read('email.replyTo');
				if ($replyTo) {
					$message->replyTo($replyTo);
				}
				if (Mailer::sendMail($message)) {
					Yii::app()->user->setFlash('success', "Un courriel a été envoyé avec le nouveau mot de passe.");
				} else {
					Yii::log("L'envoi du mail d'inscription (mdp) a échoué.", 'error', 'email');
					Yii::app()->user->setFlash('error', "L'envoi par mail n'a pas fonctionné. L'erreur a été signalée aux administrateurs.");
				}
				$controller->redirect(['view', 'id' => $model->id]);
			}
		}

		$controller->render(
			'messagePassword',
			['model' => $model, 'formData' => $formData]
		);
	}

	private function checkAccess(Utilisateur $model): void
	{
		if (!Yii::app()->user->access()->toUtilisateur()->messagePassword($model)) {
			throw new CHttpException(403, "Accès interdit.");
		}
		if ($model->authmethod == Utilisateur::AUTHMETHOD_LDAP) {
			throw new CHttpException(403, "Un utilisateur authentifié par LDAP ne peut changer de mot de passe.");
		}
	}
}
