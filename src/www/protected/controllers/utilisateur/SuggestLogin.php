<?php

namespace controllers\utilisateur;

use Utilisateur;

/**
 * Service that returns a login (that does not exist yet).
 */
class SuggestLogin extends \CAction
{
	/**
	 * @param string $nom
	 * @param string $prenom
	 */
	public function run($nom, $prenom)
	{
		$controller = $this->getController();
		assert($controller instanceof \UtilisateurController);

		\Controller::header('json');
		$nomC = self::cleanupName($nom);
		$prenomC = self::cleanupName($prenom);
		foreach (range(1, strlen($prenomC)) as $take) {
			$login = "m_{$nomC}_" . substr($prenomC, 0, $take);
			if (!Utilisateur::model()->findByAttributes(['login' => $login])) {
				echo json_encode($login, JSON_UNESCAPED_UNICODE);
				return;
			}
		}
		echo '""';
	}

	private static function cleanupName(string $x)
	{
		$translit = iconv("UTF-8", "ASCII//TRANSLIT", $x);
		$rm = [" " => "-", "'" => "-"];
		return strtolower(str_replace(array_keys($rm), array_values($rm), $translit));
	}
}
