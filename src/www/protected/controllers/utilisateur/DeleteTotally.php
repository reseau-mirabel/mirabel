<?php

namespace controllers\utilisateur;

use CHtml;
use CHttpException;
use Intervention;
use Utilisateur;
use Yii;

class DeleteTotally extends \CAction
{
	public function run(string $id)
	{
		$controller = $this->getController();
		assert($controller instanceof \UtilisateurController);

		$user = $controller->loadModel($id);
		$this->checkAccess($user);
		if ($this->hasObstacles($user)) {
			$controller->redirect(['/utilisateur/view', 'id' => $user->id]);
			return;
		}

		try {
			$user->delete();
		} catch (\Exception $e) {
			Yii::app()->user->setFlash(
				'error',
				"L'utilisateur n'a pu être supprimé, il est probablement utilisé dans les données : " . CHtml::encode($e->getMessage())
			);
			$controller->redirect(['/utilisateur/view', 'id' => $user->id]);
			return;
		}
		Yii::app()->user->setFlash('success', "L'utilisateur " . CHtml::encode($user->nom) . " a été supprimé définitivement.");
		$controller->redirect($_POST['returnUrl'] ?? ['admin']);
	}

	private function checkAccess(Utilisateur $user): void
	{
		if (!Yii::app()->request->isPostRequest) {
			// we only allow disabling via POST request
			throw new CHttpException(400, 'Requête invalide. Il est possible que JavaScript soit désactivé dans votre navigateur. Activez-le et recommencez.');
		}
		if (!Yii::app()->user->access()->toUtilisateur()->delete($user)) {
			throw new CHttpException(403);
		}
	}

	private function hasObstacles(Utilisateur $user): bool
	{
		$interv = Intervention::model()->findByAttributes(['utilisateurIdVal' => $user->id]);
		if (!$interv) {
			$interv = Intervention::model()->findByAttributes(['utilisateurIdProp' => $user->id]);
		}
		if ($interv) {
			Yii::app()->user->setFlash(
				'error',
				"L'utilisateur ne peut être supprimé, il est déjà intervenu au moins pour " . $this->getController()->createUrl('/intervention/view', ['id' => $interv->id])
				. ".<br />" . (
					$user->actif ?
					"Vous pouvez par contre désactiver cet utilisateur."
					: "Par contre cet utilisateur est bien désactivé. Il n'a donc plus accès à Mir@bel et n'est plus abonné à la liste de diffusion des partenaires."
				)
			);
			return true;
		}

		$hasPolicy = \Politique::model()->exists("utilisateurId = :id", [':id' => $user->id]);
		if ($hasPolicy) {
			Yii::app()->user->setFlash(
				'error',
				"L'utilisateur ne peut être supprimé, il est l'auteur d'au moins une politique."
			);
			return true;
		}

		return false;
	}
}
