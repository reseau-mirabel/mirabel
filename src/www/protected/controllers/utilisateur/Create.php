<?php

namespace controllers\utilisateur;

use CHttpException;
use Utilisateur;
use Yii;
use models\forms\User;

class Create extends \CAction
{
	public function run(string $partenaireId = "0")
	{
		$controller = $this->getController();
		assert($controller instanceof \UtilisateurController);

		$model = new User(null);
		$model->setPartenaireId($partenaireId > 0 ? (int) $partenaireId : null);
		$this->checkAccess($model);

		$partenaire = $model->getPartenaire();
		if (!$partenaire || $partenaire->type === 'normal') {
			$model->listeDiffusion = 1;
		}

		if (isset($_POST['User'])) {
			$model->setAttributes($_POST['User']);
			if ($model->save()) {
				if (!$model->authmethod == Utilisateur::AUTHMETHOD_LDAP && empty($model->motdepasse)) {
					Yii::app()->user->setFlash(
						'success',
						"<p>Le nouvel utilisateur a été créé.</p>
<p>Son champ <em>mot de passe</em> étant vide, il ne peut pas se connecter pour le moment.
Mir@bel propose de lui attribuer un mot de passe aléatoire qui lui sera envoyé par courriel.
</p>"
					);
					$controller->redirect(['message-password', 'id' => $model->id]);
				} else {
					Yii::app()->user->setFlash('success', "Le nouvel utilisateur a été créé.");
					$controller->redirect(['/utilisateur/view', 'id' => $model->id]);
				}
				return;
			}
		}

		$controller->render(
			'create',
			['model' => $model]
		);
	}

	private function checkAccess(User $model): void
	{
		$p = $model->partenaireId ? $model->getPartenaire() : null;
		if (!Yii::app()->user->access()->toUtilisateur()->create($p)) {
			throw new CHttpException(403, "Vous ne pouvez créer de compte" . ($p ? " pour ce partenaire." : "."));
		}
	}
}
