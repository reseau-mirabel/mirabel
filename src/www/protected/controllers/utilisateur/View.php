<?php

namespace controllers\utilisateur;

use CHttpException;
use Yii;

class View extends \CAction
{
	public function run(string $id)
	{
		$controller = $this->getController();
		assert($controller instanceof \UtilisateurController);

		$model = $controller->loadModel($id);

		if (!Yii::app()->user->checkAccess('avec-partenaire') && !Yii::app()->user->access()->toUtilisateur()->view($model)) {
			throw new CHttpException(403, "Vous ne pouvez consulter que les comptes du même établissement.");
		}

		if ($model->partenaireId) {
			$controller->render('view', ['model' => $model]);
		} else {
			$controller->render('view-politiques', ['model' => $model]);
		}
	}
}
