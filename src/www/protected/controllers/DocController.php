<?php

class DocController extends Controller
{
	/**
	 * @var string the default layout for the views. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'roles' => ['avec-partenaire'],
			],
			['deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	/**
	 * List pages.
	 */
	public function actionIndex()
	{
		$path = self::readDocPath();
		$pages = array_map(
			function ($v) {
				return preg_replace('/\.md$/', '', basename($v));
			},
			glob("$path/*.md")
		);
		$pdfs = array_map(
			'basename',
			glob("$path/*.pdf")
		);

		$this->render(
			'index',
			['pages' => $pages, 'pdfs' => $pdfs]
		);
	}

	public function actionPdf(string $file)
	{
		$pdf = self::readDocPath() . '/' . $file;
		if (!file_exists($pdf)) {
			throw new CHttpException(404, 'PDF not found');
		}
		$realPath = realpath($pdf);
		if (!preg_match('/\.pdf$/', $realPath)) {
			throw new CHttpException(404, 'PDF not found or not allowed');
		}
		\Controller::header('pdf');
		header("Content-Disposition: inline; filename=$realPath");
		readfile($realPath);
	}

	public function actionView(string $view)
	{
		$file = self::readDocPath() . "/$view.md";
		if (!file_exists($file)) {
			throw new CHttpException(404, "Fichier non trouvé.");
		}

		$md = Yii::app()->getComponent('markdown');
		assert($md instanceof \League\CommonMark\ConverterInterface);
		$html = '<div class="from-markdown">' . $md->convert(file_get_contents($file))->getContent() . '</div>';

		$this->breadcrumbs = [
			'Doc technique' => ['/doc'],
			(empty($_GET['view']) ? '*' : $_GET['view']),
		];

		// Load mermaid.js for graphs
		if (strpos($html, 'language-mermaid') > 0) {
			Yii::app()->clientScript->registerScript(
				'mermaid',
				<<<JS
					import mermaid from 'https://cdn.jsdelivr.net/npm/mermaid@10/dist/mermaid.esm.min.mjs';
					mermaid.initialize({ startOnLoad: false });
					await mermaid.run({
						querySelector: '.language-mermaid',
					});
					JS,
				CClientScript::POS_HEAD,
				['type' => 'module']
			);
		}

		// Low-level rendering: we need to wrap the HTML content into the layout.
		$layoutFile = $this->getLayoutFile($this->layout);
		echo $this->processOutput(
			$this->renderFile($layoutFile, ['content' => $html], true)
		);
	}

	private static function readDocPath(): string
	{
		$rawPath = Yii::app()->params->itemAt('doc-path') ?: 'doc/doc-interne';
		if ($rawPath[0] !== '/') {
			$rawPath = dirname(VENDOR_PATH) . '/' . $rawPath;
		}
		$path = realpath($rawPath);
		if ($path === false) {
			throw new \Exception("Le répertoire de la doc technique est introuvable. Un administrateur doit corriger params.doc-interne dans le fichier de configuration.");
		}
		return $path;
	}
}
