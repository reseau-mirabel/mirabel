<?php

use components\Tools;

/**
 * Pages des statistiques sur les données de M.
 */
class StatsController extends Controller
{
	public $localhostConnection = false;

	public function filters(): array
	{
		return [
			'accessControl',
			[
				// Cache outuput for every page but history and pdf
				'COutputCache - history, pdf',
				'duration' => \Yii::app()->user->checkAccess("avec-partenaire") ? (YII_DEBUG ? 5 : 3600) : 86400,
				'varyByParam' => ['domain', 'name', 'partenaireId', 'filter', 'sort'],
				'varyBySession' => true,
			],
		];
	}

	/**
	 * This method is used by the 'accessControl' filter.
	 */
	public function accessRules(): array
	{
		return [
			[
				'allow',
				'actions' => ['liens'],
				'users' => ['*'],
			],
			[
				// Some pages are periodically fetched by scripts, so acces from local IPs is allowed.
				'allow',
				'actions' => ['index', 'editeurs', 'interventions', 'politiques', 'science-ouverte', 'suivi'],
				'ips' => preg_split('/[\s,]+/', (string) Config::read('server.ip')),
			],
			[
				'allow',
				'actions' => ['activite', 'editeurs', 'history', 'index', 'interventions', 'liens', 'pdf', 'politiques', 'science-ouverte'],
				'roles' => ['avec-partenaire'],
			],
			[
				'allow',
				'actions' => ['suivi'],
				'roles' => ['stats/suivi'],
			],
			['deny', 'users' => ['*']],
		];
	}

	/**
	 * Maps actions to dedicated classes.
	 */
	public function actions(): array
	{
		return [
			'activite' => 'controllers\stats\ActiviteAction',
			'editeurs' => 'controllers\stats\EditeursAction',
			'history' => 'controllers\stats\HistoryAction',
			'index' => 'controllers\stats\IndexAction',
			'interventions' => 'controllers\stats\InterventionsAction',
			'liens' => 'controllers\stats\LiensAction',
			'pdf' => 'controllers\stats\PdfAction',
			'politiques' => 'controllers\stats\PolitiquesAction',
			'science-ouverte' => 'controllers\stats\ScienceOuverteAction',
			'suivi' => 'controllers\stats\SuiviAction',
		];
	}

	public function init()
	{
		$this->localhostConnection = Tools::matchIpFilter(
			Yii::app()->request->getUserHostAddress(),
			(string) Config::read('server.ip')
		);
		return parent::init();
	}
}
