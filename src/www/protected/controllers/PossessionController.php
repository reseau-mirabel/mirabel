<?php

class PossessionController extends Controller
{
	/**
	 * @var string the default layout for the views. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl',
		];
	}

	/**
	 * Specifies the rules for the 'accessControl' filter.
	 *
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'roles' => ['avec-partenaire'],
			],
			[
				'deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	/**
	 * Maps actions to dedicated classes.
	 */
	public function actions(): array
	{
		return [
			'export' => 'controllers\possession\ExportAction',
			'export-more' => 'controllers\possession\ExportMoreAction',
			'import' => 'controllers\possession\ImportAction',
		];
	}
}
