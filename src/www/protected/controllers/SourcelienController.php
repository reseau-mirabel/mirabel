<?php

class SourcelienController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column1';

	public $defaultAction = 'admin';

	private $model;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			['allow', 'roles' => ['admin']],
			['deny', 'users' => ['*']],
		];
	}

	/**
	 * Creates a new model.
	 */
	public function actionCreate()
	{
		$model = new Sourcelien();

		if (isset($_POST['Sourcelien'])) {
			$model->attributes = $_POST['Sourcelien'];
			if ($model->save()) {
				Yii::app()->user->setFlash('success', "La source de lien a été ajoutée.");
				$this->redirect(['admin']);
			}
		}

		$this->render(
			'create',
			['model' => $model]
		);
	}

	/**
	 * Updates a particular model.
	 *
	 * @param int $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		if (isset($_POST['Sourcelien'])) {
			$model->attributes = $_POST['Sourcelien'];
			if ($model->save()) {
				Yii::app()->user->setFlash('success', "La source de lien a été modifiée.");
				$this->redirect(['admin']);
			}
		}

		$this->render(
			'update',
			['model' => $model]
		);
	}

	/**
	 * Disables a particular model.
	 *
	 * @param int $id the ID of the model to be disabled
	 */
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			// we only allow disabling via POST request
			$model = $this->loadModel($id);
			if ($model->delete()) {
				Yii::app()->user->setFlash('success', "La source de lien {$model->nom} a été supprimée.");
				$this->redirect(
					$_POST['returnUrl'] ?? ['admin']
				);
			}
		} else {
			throw new CHttpException(400, 'Requête invalide. Il est possible que JavaScript soit désactivé dans votre navigateur. Activez-le et recommencez.');
		}
	}

	public function actionAdmin()
	{
		$model = new \models\searches\SourcelienSearch('search');
		if (isset($_GET['q'])) {
			$model->setAttributes($_GET['q']);
		}

		$this->render(
			'admin',
			['model' => $model]
		);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 *
	 * If the data model is not found, an HTTP exception will be raised.
	 *
	 * @param int $id the ID of the model to be loaded
	 * @return Sourcelien
	 */
	public function loadModel($id)
	{
		if (!$this->model) {
			$this->model = Sourcelien::model()->findByPk((int) $id);
		}
		if ($this->model === null) {
			throw new CHttpException(404, "La source demandée n'existe pas.");
		}
		return $this->model;
	}
}
