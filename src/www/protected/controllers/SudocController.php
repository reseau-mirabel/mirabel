<?php

class SudocController extends Controller
{
	public $layout = '//layouts/column1';

	public function filters()
	{
		return ['accessControl'];
	}

	public function accessRules()
	{
		return [
			// allow authenticated
			['allow', 'roles' => ['avec-partenaire'], ],
			// deny all users
			['deny', 'users' => ['*']],
		];
	}

	/**
	 * Maps actions to dedicated classes.
	 */
	public function actions(): array
	{
		return [
			'import' => 'controllers\sudoc\ImportAction',
		];
	}
}
