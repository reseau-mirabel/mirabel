<?php

namespace controllers\revue;

use processes\titres\TrierTitre;
use Yii;

class TrierTitreAction extends \CAction
{
	public function run($id)
	{
		$revue = \RevueController::loadModel($id);
		if (!Yii::app()->user->access()->toTitre()->createDirect((int) $revue->id)) {
			throw new \CHttpException(403, "L'accès est restreint aux revues auxquelles vous pouvez ajouter un titre directement.");
		}

		if (Yii::app()->request->isPostRequest) {
			$order = array_map('intval', $_POST['order'] ?? []);
			if (TrierTitre::isInputValid($revue, $order)) {
				// Le tri n'est accepté que pour modifier l'ordre ou pour corriger les problèmes d'obsoletePar s'il y en a (doublons ou obsoletePar hors Revue)
				if (TrierTitre::isProblemInObsoleteParValue($revue) || TrierTitre::isOrderChanged($revue, $order)) {
					if (TrierTitre::reorderTitreInDb($order)) {
						Yii::app()->user->setFlash('success', "L'ordre des titres a été enregistré.");
					} else {
						Yii::app()->user->setFlash('error', "La modification de l'ordre échoué.");
					}
					$this->getController()->refresh();
					return;
				}
				Yii::app()->user->setFlash('info', "L'ordre des titres de la revue est resté inchangé");

			} else {
				Yii::app()->user->setFlash('error', "L'ordre que vous avez fourni est invalide.");
			}
		}

		$obsoleteByIdsProblems = TrierTitre::verifyObsoleteByIds($revue);

		$this->getController()->render(
			'trier-titre',
			[
				'model' => $revue,
				'duplicate' => $obsoleteByIdsProblems['duplicate'],
				'notInRevue' => $obsoleteByIdsProblems['notInRevue'],
				'titreIdWithDateProblem' => TrierTitre::getTitresIdWithDateProblem($revue),
				'hasDateProblem' => TrierTitre::hasTitresWithDateProblem($revue),
				'hasObsoleteParProblem' => count($obsoleteByIdsProblems['duplicate']) > 0 || count($obsoleteByIdsProblems['notInRevue'])>0,
			]
		);
	}
}
