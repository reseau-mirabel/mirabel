<?php

namespace controllers\revue;

use CHttpException;
use models\exporters\Revue;
use SearchTitre;
use Yii;

/**
 * Export selected journals.
 * If 'extended' is not empty, add data related to the users's Partenaire.
 * If 'count' is not empty, return the number of journals instead of CSV data.
 */
class ExportAction extends \CAction
{
	public function run(array $q = [], string $extended = "", string $count = "")
	{
		if ($q) {
			$model = new SearchTitre(Yii::app()->user->getState('instituteId'));
			$model->setAttributes($q);
			if (!$model->validate()) {
				throw new CHttpException(400, "Bad query.\n" . print_r($model->getErrors(), true));
			}
			$revuesIds = $model->searchRevueIds();
			if (!$revuesIds) {
				throw new CHttpException(400, "no result");
			}
		} else {
			$revuesIds = [];
		}

		if ($count) {
			echo count($revuesIds);
			return;
		}

		$revueExporter = new Revue(';');
		if ($extended && Yii::app()->user->checkAccess('avec-partenaire')) {
			$revueExporter->partenaireId = (int) Yii::app()->user->getState('partenaireId');
		}

		self::sendHttpHeader();
		echo $revueExporter->csvHeader();
		echo $revueExporter->toCsv($revuesIds);
	}

	private static function sendHttpHeader(): void
	{
		\Controller::header('csv');
		header(
			sprintf('Content-Disposition: attachment;filename="%s_revues_%s.csv"', \Norm::urlParam(\Yii::app()->name), date('Y-m-d'))
		);
	}
}
