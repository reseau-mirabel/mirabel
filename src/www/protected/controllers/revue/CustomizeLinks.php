<?php

namespace controllers\revue;

use models\forms\CustomizeLinksForm;
use Yii;

class CustomizeLinks extends \CAction
{
	public function run(string $ajax = '')
	{
		$formData = new CustomizeLinksForm();
		if (Yii::app()->request->isPostRequest) {
			$formData->setAttributes($_POST['CustomizeLinksForm']);
			if ($formData->validate()) {
				$user = Yii::app()->user;
				$user->setCustomizedLinks($formData->selection);
				if ($ajax) {
					\Controller::header('json');
					echo json_encode(['html' => '', 'selection' => join(';', $formData->selection)]);
					return;
				}
				$user->setFlash('success', "Personnalisation enregistrée");
				$this->getController()->redirect($user->getReturnUrl());
				return;
			}
		}

		$selection = Yii::app()->user->getCustomizedLinks();
		if ($selection === null) {
			$formData->selection = \Sourcelien::DEFAULT_SEARCH_DISPLAY;
		} else {
			$formData->selection = $selection;
		}
		$params = [
			'formData' => $formData,
			'ajax' => (bool) $ajax,
		];

		if ($ajax) {
			\Controller::header('json');
			$html = $this->getController()->renderPartial('customize-links', $params, true);
			echo json_encode(['html' => $html], JSON_UNESCAPED_UNICODE);
			return;
		}
		$this->getController()->render('customize-links', $params);
	}
}
