<?php

namespace controllers\revue;

use processes\completion\Revue;

class AjaxCompleteAction extends \CAction
{
	public function run(string $term)
	{
		\Controller::header('json');
		echo json_encode((new Revue)->completeTerm($term, [], [], 'text'));
	}
}
