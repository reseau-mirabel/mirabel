<?php

namespace controllers\revue;

use models\wikidata\CompareContent;
use Wikidata;

/**
 * Check the links of the sub-objects.
 */
class CheckLinksAction extends \CAction
{
	public function run(string $id)
	{
		$controller = $this->getController();
		\assert($controller instanceof \RevueController);

		$model = \RevueController::loadModel($id);
		$hasWikidata = Wikidata::isRevuePresent((int) $model->id);
		if ($hasWikidata) {
			// M#4579 rafraîchissement des comparaisons WD/Mir@bel
			$wComp = new CompareContent(0);
			$wComp->compareProperties(false, $model->id); // for side effects on the Wikidata table, which a sub-view will read
		}
		$controller->render(
			'checkLinks',
			[
				'revue' => $model,
				'checks' => (new \processes\urlverif\UrlExtractor)->checkObjectLinks($model),
				'hasWikidata' => $hasWikidata,
			]
		);
	}
}
