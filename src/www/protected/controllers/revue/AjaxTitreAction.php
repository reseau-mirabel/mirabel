<?php

namespace controllers\revue;

use Titre;

class AjaxTitreAction extends \CAction
{
	public function run(string $id)
	{
		$this->getController()->renderPartial(
			'_titre-actif',
			[
				'activeTitle' => Titre::model()->findByPk((int) $id),
				'monoTitle' => false,
				'forceRefresh' => false,
				'isPopup' => true,
			]
		);
	}
}
