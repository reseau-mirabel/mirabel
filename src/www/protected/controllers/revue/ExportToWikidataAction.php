<?php

namespace controllers\revue;

use CHttpException;
use models\wikidata\Export;

/**
 * Export MirabelID to Wikidata (QuickStatements and logs)
 */
class ExportToWikidataAction extends \CAction
{
	public function run(string $target)
	{
		$WExport = new Export();
		$out = fopen('php://output', 'w');
		$controller = $this->getController();
		assert($controller instanceof \Controller);
		switch ($target) {
			case 'quickstatements':
				\Controller::header('text');
				$WExport->genQuickstatements($out);
				break;
			case 'log-multi-revues':
				\Controller::header('csv');
				$WExport->logHeaders($out);
				$WExport->logWarning($out, 'nbrevues');
				break;
			case 'log-multi-titres':
				\Controller::header('csv');
				$WExport->logHeaders($out);
				$WExport->logWarning($out, 'nbtitres');
				break;
			case 'log-multi-elements':
				\Controller::header('csv');
				$WExport->logHeaders($out);
				$WExport->logMultielements($out);
				break;
			default:
				throw new CHttpException(400, "paramètre incohérent");
		}
		fclose($out);
	}
}
