<?php

namespace controllers\revue;

use processes\revue\Delete as DeleteProcess;
use Yii;

class DeleteAction extends \CAction
{
	public function run(string $id)
	{
		$controller = $this->getController();
		assert($controller instanceof \RevueController);

		$revue = \RevueController::loadModel($id);
		$process = new DeleteProcess($revue);

		if (!$process->preValidate()) {
			$controller->flashAndRedirect($process->getRedirection());
			return;
		}

		if (!Yii::app()->request->getPost('confirm', false) || Yii::app()->request->getPost('annoying', '') !== 'Je confirme') {
			$controller->render('delete', ['model' => $revue]);
			return;
		}

		$process->run();
		$controller->flashAndRedirect($process->getRedirection());
	}
}
