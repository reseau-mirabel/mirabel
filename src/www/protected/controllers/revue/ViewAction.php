<?php

namespace controllers\revue;

use Revue;
use Yii;
use processes\revue\View as ViewProcess;

class ViewAction extends \CAction
{
	public function run(string $id, string $nom = '', string $public = '0')
	{
		$controller = $this->getController();
		assert($controller instanceof \RevueController);

		$revue = $this->loadModel((int) $id);
		if ($revue === null) {
			return;
		}

		$process = new ViewProcess($revue, (int) Yii::app()->user->getState('partenaireId'), (bool) $public);
		if (!$process->validateName($nom)) {
			$controller->flashAndRedirect($process->getRedirection());
			return;
		}
		$process->directUpdate = Yii::app()->user->checkAccess('revue/update-direct', ['Revue' => $revue]);
		$process->forceRefresh = Yii::app()->session->contains('force-refresh');
		if ($process->forceRefresh) {
			Yii::app()->session->remove('force-refresh');
		}
		if (!Yii::app()->user->checkAccess("avec-partenaire")) {
			$controller->layout = '//layouts/column1';
		}

		$controller->setCanonicalUrl($process->getCanonicalRoute());
		$controller->render(
			'view',
			[
				'process' => $process,
				'titres' => $revue->getTitresOrderedByObsolete(),
				'services' => $revue->getServices(),
				'partenaires' => $revue->getPartenairesSuivant(),
				'searchNavigation' => $controller->loadSearchNavigation(),
			]
		);
	}

	private function loadModel(int $id): ?Revue
	{
		$revue = Revue::model()->findByPk($id);
		if ($revue instanceof Revue) {
			$revue->id = (int) $revue->id;
			return $revue;
		}
		$url = (new \processes\redirection\Redirector('Revue'))->getRoute($id);
		if ($url) {
			$this->getController()->redirect($url, true, 301);
			return null;
		}
		throw new \CHttpException(404, "La revue demandée n'existe pas.");
	}
}
