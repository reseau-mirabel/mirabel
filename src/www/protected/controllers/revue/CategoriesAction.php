<?php

namespace controllers\revue;

use CHtml;
use Yii;
use models\forms\RevueCategoriesForm;

/**
 * List categories of a model and display a form to edit these links.
 */
class CategoriesAction extends \CAction
{
	public function run(string $id)
	{
		$controller = $this->getController();
		assert($controller instanceof \RevueController);

		$revue = \RevueController::loadModel($id);
		$form = new RevueCategoriesForm($revue);
		$directAccess = Yii::app()->user->checkAccess('revue/update-direct', ['Revue' => $revue]);

		if (Yii::app()->request->isPostRequest) {
			if ($this->update($form, $directAccess)) {
				return;
			}
		}
		$controller->render(
			'categories',
			[
				'formData' => $form,
				'categorieLinks' => $revue->categorieLinks,
				'categoriesPubliques' => $revue->getThemes(),
				'directAccess' => $directAccess,
				'propositions' => (bool) Yii::app()->db
					->createCommand("SELECT 1 FROM Intervention WHERE revueId = :rid AND action = 'revue-I' AND hdateVal IS NULL LIMIT 1")
					->queryScalar([':rid' => $revue->id]),
			]
		);
	}

	private function update(RevueCategoriesForm $form, bool $directAccess): bool
	{
		$form->setAttributes($_POST);
		if (!$form->validate()) {
			return false;
		}

		$i = $form->createIntervention();
		$message = $form->applyChanges($i->contenuJson);
		if ($i->contenuJson->isEmpty()) {
			Yii::app()->user->setFlash(
				'error',
				$message . "<div>Aucune modification à effectuer.</div>"
			);
			$this->getController()->refresh();
			return true;
		}

		if (!$directAccess || $form->isProposition()) {
			$i->save();
			Yii::app()->user->setFlash(
				($message ? 'warning' : 'success'),
				$message . "<div>Les affectations de thèmes ont bien été proposées "
				. " pour la revue <em>" . CHtml::encode($form->revue->activeTitle->titre) . "</em>.</div>"
			);
		} else {
			$i->accept();
			Yii::app()->user->setFlash(
				($message ? 'warning' : 'success'),
				$message . "<div>Les affectations de thèmes ont bien été modifiées "
				. " pour la revue <em>" . CHtml::encode($form->revue->activeTitle->titre) . "</em>.</div>"
			);
		}
		$this->getController()->redirect(['/revue/view', 'id' => $form->revue->id]);
		return true;
	}
}
