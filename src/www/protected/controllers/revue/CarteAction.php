<?php

namespace controllers\revue;

use CHtml;
use CJavaScriptExpression;
use Yii;

class CarteAction extends \CAction
{
	public function run(string $stamen = '', $vivantes = null)
	{
		if (!preg_match('/^[\w-]+$/', $stamen)) {
			$stamen = '';
		}
		$countries = [];
		$max = 0;
		$condVivantes = ($vivantes ? " WHERE t.dateFin = ''" : "");
		$query = Yii::app()->db->createCommand(<<<EOSQL
			SELECT p.code, p.id, p.nom, count(distinct e.id) AS editeurs, count(distinct t.revueId) AS revues
			FROM Editeur e
				JOIN Pays p ON e.paysId = p.id
				LEFT JOIN Titre_Editeur te ON te.editeurId = e.id AND te.ancien = 0
				LEFT JOIN Titre t ON te.titreId = t.id
			$condVivantes
			GROUP BY p.id, p.code
			EOSQL
		);
		foreach ($query->query() as $row) {
			if ($max < $row['editeurs']) {
				$max = $row['editeurs'];
			}
			$countries[$row['code']] = [(int) $row['revues'], self::renderPopup($row, (bool) $vivantes)];
		}
		$this->getController()->render(
			'carte',
			[
				'stamen' => $stamen,
				'vivantes' => (bool) $vivantes,
				'nbRevues' => (int) Yii::app()->db
					->createCommand("SELECT count(DISTINCT revueId) FROM Titre t $condVivantes")
					->queryScalar(),
				'mapConfig' => [
					'countries' => $countries,
					'countriesMax' => $max,
					'radius' => [
						'max' => 12, // pixels
						'method' => 'logarithmic',
					],
					'popupMessage' => new CJavaScriptExpression('function(x) { return x[1]; }'),
				],
			]
		);
	}

	private static function renderPopup(array $row, bool $vivantes): string
	{
		$html = CHtml::tag('div', [], CHtml::encode($row['nom']));

		$url = ['/revue/search', 'q' => ['paysId' => $row['id']]];
		if ($vivantes) {
			$url['q']['vivant'] = 1;
		}
		$html .= CHtml::tag(
			'strong',
			[],
			CHtml::link(
				$row['revues'] . " revue" . ($row['revues'] > 1 ? "s" : ""),
				$url
			)
		);

		if (!$vivantes) {
			$html .= "<br /> pour "
				. CHtml::link(
					$row['editeurs'] . " éditeur" . ($row['editeurs'] > 1 ? "s" : ""),
					['/editeur/pays', 'paysId' => $row['code'], 'paysNom' => $row['nom']]
				);
		}
		return $html;
	}
}
