<?php

namespace controllers\revue;

use CHttpException;
use components\sphinx\DataProvider;
use processes\search\titres\Params;
use SearchNavigation;
use SearchTitre;
use Yii;

class SearchAction extends \CAction
{
	public function run(): void
	{
		if (isset($_GET['SearchTitre'])) {
			$this->redirect($_GET['SearchTitre'], $_GET['q'] ?? []);
			return;
		}

		$cookies = \Yii::app()->request->getCookies();
		$customizePager = new \widgets\CustomizePager($cookies);

		$base = new SearchTitre(Yii::app()->user->getState('instituteId'));
		$refine = new SearchTitre(null);
		$facets = [];
		$results = null;
		$searchHash = '';

		if (isset($_GET['f'])) {
			$base->setAttributes((array) $_GET['q']);
			$refine->setAttributes((array) $_GET['f']);
			$refine->categoriesEt = true;
			$refine->languesEt = true;
			if ($base->validate() && $refine->validate()) {
				$results = self::getDataProvider($base->exportAttributes(), $refine->exportAttributes(), $customizePager->getPageSize());
			}
		} elseif (isset($_GET['q'])) {
			$base->setAttributes((array) $_GET['q']);
			// Backward compatibility on monitoredNot
			if (!empty($_GET['q']['monitoredNot'])) {
				$base->monitored = SearchTitre::MONIT_NO;
			}
			if ($base->validate()) {
				$results = $base->searchWithFacets($customizePager->getPageSize());
			}
		}
		if ($results !== null) {
			if ($refine->isEmpty() && $this->redirectIfSingleResult($results)) {
				return;
			}
			$facets = $results->getFacets();
			$searchHash = $this->updateNavigationCache($results, $refine->exportAttributes()->toArray());
		}

		$this->getController()->render(
			'search',
			[
				'base' => $base,
				'refine' => $refine,
				'results' => $results,
				'facets' => $facets,
				'searchHash' => $searchHash,
				'customizePager' => $customizePager,
			]
		);
	}

	private function redirect(array $query, array $base): void
	{
		$filtered = SearchTitre::removeEmptyCriteria((array) $query);
		$model = new SearchTitre(Yii::app()->user->getState('instituteId'));
		$model->setAttributes($base);
		$model->setAttributes($filtered);
		try {
			if ($model->validate()) {
				$this->getController()->redirect(['search', 'q' => $model->exportAttributes()->toArray()]);
			} else {
				$this->getController()->render('search', [
					'base' => $model,
					'refine' => new SearchTitre(null),
					'results' => null,
					'facets' => '',
					'searchHash' => '',
					'customizePager' => new \widgets\CustomizePager(Yii::app()->request->getCookies()),
				]);
			}
		} catch (\Exception $e) {
			throw new CHttpException(400, "Erreur dans la recherche de revues. " . $e->getMessage());
		}
	}

	private function redirectIfSingleResult(DataProvider $results): bool
	{
		if ($results->itemCount !== 1) {
			return false;
		}
		$data = $results->data;
		assert(count($data) === 1);
		$result = reset($data);
		$this->getController()->redirect(['view', 'id' => $result->revueid]);
		return true;
	}

	private function updateNavigationCache(DataProvider $results, array $refineParams): string
	{
		if ($results->itemCount === 0) {
			return '';
		}
		$params = array_filter([
			'q' => $_GET['q'] ?? $_GET['SearchTitre'] ?? [],
			'f' => $refineParams,
		]);
		if (!$params) {
			return '';
		}
		$searchCache = new SearchNavigation();
		return $searchCache->save(
			'revue/search',
			$params,
			'revueid',
			$results->getData(),
			$results->pagination
		);
	}

	private static function getDataProvider(Params $base, Params $facets, int $pageSize): DataProvider
	{
		$sort = new \CSort(\models\sphinx\Titres::class);
		$sort->defaultOrder = 'cletri ASC';

		$criteria = \processes\search\titres\Actor::createSqlCriteria($base);
		\processes\search\titres\Actor::addToCriteria($criteria, $facets);
		\SearchTitre::addFacets($criteria);

		return new DataProvider(
			\models\sphinx\Titres::model(),
			[
				'criteria' => $criteria,
				'pagination' => ['pageSize' => $pageSize],
				'sort' => $sort,
			]
		);
	}
}
