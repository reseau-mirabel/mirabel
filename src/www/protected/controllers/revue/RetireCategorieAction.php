<?php

namespace controllers\revue;

use CategorieRevue;
use CHttpException;
use Yii;

class RetireCategorieAction extends \CAction
{
	public function run(string $id, string $categorieId)
	{
		$controller = $this->getController();
		\assert($controller instanceof \RevueController);

		$revue = \RevueController::loadModel($id);
		$link = CategorieRevue::model()->findByPk(['revueId' => (int) $id, 'categorieId' => (int) $categorieId]);
		if ($link === null) {
			throw new CHttpException(404);
		}
		$categorie = $link->categorie;

		if ($link->delete()) {
			Yii::app()->user->setFlash(
				'success',
				"Le thème {$categorie->categorie} a été retiré de la revue {$revue->activeTitle->titre}."
			);
		} else {
			Yii::app()->user->setFlash(
				'Erreur',
				"Impossible de retirer le thème {$categorie->categorie} de la revue {$revue->activeTitle->titre} :"
				. print_r($link->getErrors(), true)
			);
		}
		$controller->redirect(['categories', 'id' => $revue->id]);
	}
}
