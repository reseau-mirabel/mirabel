<?php

namespace controllers\revue;

use CHttpException;
use components\paginationalpha\ListViewAlpha;
use components\sphinx\Criteria;
use components\sphinx\DataProvider;
use components\SqlHelper;
use CSort;
use models\sphinx\Revues;
use Revue;
use SearchNavigation;
use Yii;

/**
 * This page will read two parameters in the URL:
 * - lettre, *directly*  through the action, passed to PaginationAlpha
 * - page, *indirectly*  through CPagination, activated from the dataProvider.
 */
class IndexAction extends \CAction
{
	public static $pageSize = 50;

	public function run(string $lettre = 'A')
	{
		$controller = $this->getController();
		if (!Yii::app()->user->checkAccess("avec-partenaire")) {
			$controller->layout = '//layouts/column1';
		}

		$listView = self::createListView();
		$listView->getLetterPagination()->setCurrentPage($lettre);

		$letterRank = $listView->getLetterPagination()->getCurrentPage();
		$letter = $listView->getLetterPagination()->getPageLetter($letterRank);
		$dataProvider = self::createDataProvider($letter, $letterRank);
		try {
			$data = $dataProvider->getData();
		} catch (\Exception $e) {
			Yii::log($e->getMessage(), 'warning');
			throw new CHttpException(400, "Erreur dans la recherche de revues. Navigation au-delà de la taille des résultats ?");
		}
		$listView->dataProvider = $dataProvider;

		$searchCache = new SearchNavigation();
		$searchHash = $searchCache->save('revue/index', ['lettre' => $letter], 'revueid', $data, $dataProvider->getPagination());

		$controller->render(
			'index',
			[
				'lettre' => $letter,
				'listView' => $listView,
				'searchHash' => $searchHash,
				'totalRevues' => Revue::model()->count("statut = 'normal'"),
			]
		);
	}

	private static function createDataProvider(string $letter, int $letterRank): DataProvider
	{
		$criteria = new Criteria;
		$criteria->addColumnCondition(['lettre1' => $letterRank]);
		$criteria->addColumnCondition(['obsolete' => 0]);

		$sort = new CSort(Revues::class);
		$sort->defaultOrder = 'cletri ASC';

		return new DataProvider(
			Revues::model(),
			[
				'criteria' => $criteria,
				'pagination' => [
					'pageSize' => self::$pageSize,
					'params' => ['lettre' => $letter],
				],
				'sort' => $sort,
			]
		);
	}

	private static function createListView(): ListViewAlpha
	{
		$listView = new ListViewAlpha();
		$listView->setCountByLetter(
			SqlHelper::sqlToPairs(<<<EOSQL
				SELECT
					FIND_IN_SET(LEFT(titre, 1), 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z') AS rank,
					COUNT(*) AS num
				FROM Titre
				WHERE statut = 'normal' AND obsoletePar IS NULL
				GROUP BY rank
				EOSQL
			)
		);
		$listView->setLetterTitleTemplate("{count} revues");
		return $listView;
	}
}
