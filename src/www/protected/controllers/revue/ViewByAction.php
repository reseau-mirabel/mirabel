<?php

namespace controllers\revue;

use CHttpException;
use Revue;
use Yii;
use models\validators\IssnValidator;

class ViewByAction extends \CAction
{
	public function run(string $by, string $id)
	{
		$value = str_replace('_', ' ', $id);
		switch ($by) {
			case 'titre-id':
				$sql = "WHERE id = :id";
				$params = [':id' => (int) $id];
				break;
			case 'titre':
				$sql = "WHERE titre = :t OR sigle = :s OR CONCAT(prefixe,titre) = :ft";
				$params = [':t' => $value, ':s' => $value, ':ft' => $value];
				break;
			case 'sudoc':
				$sql = "JOIN Issn i ON i.titreId = t.id WHERE i.sudocPpn = :id";
				$params = [':id' => $id];
				break;
			case 'worldcat':
				$sql = "JOIN Issn i ON i.titreId = t.id WHERE i.worldcatOcn = :id";
				$params = [':id' => $id];
				break;
			case 'issn':
				$v = new IssnValidator();
				$v->allowEmpty = false;
				$v->allowEmptyChecksum = true;
				$v->allowMissingCaret = true;
				$v->specialValues = [];
				$v->validateString($id);
				$issn = $v->lastIssn;
				$sql = "JOIN Issn i ON t.id = i.titreId WHERE i.issn = :issn";
				$params = [':issn' => $issn];
				break;
			default:
				throw new CHttpException(400, "Paramètres non-valides.");
		}
		$revuesIds = Yii::app()->db
			->createCommand("SELECT DISTINCT t.revueId FROM Titre t $sql LIMIT 2")
			->queryColumn($params);
		if (count($revuesIds) === 0) {
			throw new CHttpException(404, "Cette revue n'a pu être trouvée.");
		}

		$controller = $this->getController();
		assert($controller instanceof \RevueController);
		if (count($revuesIds) > 1) {
			if ($by === 'titre') {
				Yii::app()->user->setFlash('info', "Plusieurs revues correspondent à ce critère.");
				$controller->redirect(['revue/search', "SearchTitre[$by]" => $value]);
			} else {
				Yii::app()->user->setFlash('info', "Plusieurs revues correspondent à ce critère. Veuillez passer par une recherche.");
				$controller->redirect(['revue/search']);
			}
			return;
		}

		// single result => redirection
		$revue = Revue::model()->findByPk($revuesIds[0]);
		$controller->redirect($revue->getSelfUrl());
	}
}
