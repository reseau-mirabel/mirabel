<?php

namespace controllers\revue;

use CHttpException;
use Yii;

class VerifyAction extends \CAction
{
	public function run(string $id)
	{
		if (!Yii::app()->request->isPostRequest) {
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}
		$controller = $this->getController();
		\assert($controller instanceof \RevueController);

		$model = \RevueController::loadModel($id);
		if (!Yii::app()->user->checkAccess('verify', $model)) {
			throw new CHttpException(403, "Vous n'avez pas les droits nécessaires pour cette opération.");
		}

		$pendingInterventions = (bool) Yii::app()->db
			->createCommand("SELECT 1 FROM Intervention WHERE revueId = :rid AND statut = :s")
			->queryScalar([':rid' => $model->id, ':s' => 'attente']);
		if ($pendingInterventions) {
			Yii::app()->user->setFlash('error', "Des interventions sur cette revue sont en attente. Merci de les accepter ou refuser.");
		} else {
			$model->hdateVerif = $_SERVER['REQUEST_TIME'];
			if ($model->save()) {
				Yii::app()->user->setFlash('success', "La date de vérification a été mise à jour.");
			}
		}
		$controller->redirect(['view', 'id' => $model->id]);
	}
}
