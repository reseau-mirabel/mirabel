<?php

namespace controllers\collection;

use Abonnement;
use processes\collection\CreateTmp;
use Service;

class CreateTmpAction extends \CAction
{
	public function run(string $ressourceId)
	{
		$controller = $this->getController();
		assert($controller instanceof \Controller);

		$process = new CreateTmp((int) $ressourceId);
		if (!$process->preValidate()) {
			$controller->flashAndRedirect($process->getRedirection());
			return;
		}

		if ($process->load($_POST['Collection'] ?? [])) {
			$process->save();
			if ($controller->flashAndRedirect($process->getRedirection())) {
				return;
			}
		}

		$collection = $process->collection;
		$controller->render(
			'create-tmp',
			[
				'model' => $collection,
				'nbAcces' => Service::model()->countByAttributes(['ressourceId' => $collection->ressourceId]),
				'nbAbos' => Abonnement::model()->countByAttributes(['ressourceId' => $collection->ressourceId]),
			]
		);
	}
}
