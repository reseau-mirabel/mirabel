<?php

namespace controllers\collection;

use processes\completion\Collection;

class AjaxCompleteAction extends \CAction
{
	public function run(string $term, string $ressourceId = '0')
	{
		\Controller::header('json');
		echo json_encode((new Collection)->completeTerm($term, (int) $ressourceId));
	}
}
