<?php

namespace controllers\collection;

use Yii;

class UpdateAction extends \CAction
{
	public function run(string $id)
	{
		$model = \CollectionController::loadModel($id);

		if (isset($_POST['Collection'])) {
			$i = $model->buildIntervention(true);
			$old = clone $model;
			$model->attributes = $_POST['Collection'];
			if ($model->validate()) {
				$i->description = "Modification de la collection « {$model->nom} »";
				$i->contenuJson->update($old, $model);
				if ($i->accept()) {
					Yii::app()->user->setFlash('success', "La collection a été modifiée.");
					$this->getController()->redirect(['/ressource/view', 'id' => $model->ressourceId]);
					return;
				}
			}
		}

		$this->getController()->render(
			'update',
			['model' => $model]
		);
	}
}
