<?php

namespace controllers\collection;

use CHtml;
use CHttpException;
use Collection;
use Yii;

class CreateAction extends \CAction
{
	public function run(string $ressourceId)
	{
		$model = new Collection;
		$model->ressourceId = (int) $ressourceId;
		if (!Yii::app()->user->checkAccess('collection/create', $model)) {
			throw new CHttpException(403, "Un non-admin ne peut pas créer une collection dans une ressource suivie par autrui.");
		}
		if (!$model->ressource->hasCollections() && $model->ressource->hasServices()) {
			$this->getController()->redirect(['create-tmp', 'ressourceId' => $model->ressourceId]);
			return;
		}

		if (isset($_POST['Collection'])) {
			$model->setAttributes($_POST['Collection']);
			$i = $model->buildIntervention(true);
			$success = false;
			if ($model->validate()) {
				$i->description = "Création de la collection « {$model->nom} »";
				$i->action = 'collection-C';
				$i->contenuJson->create($model);
				$success = $i->accept();
			}
			if ($success) {
				Yii::app()->user->setFlash('success', "Nouvelle collection « " . CHtml::encode($model->nom) . " » créée.");
				$this->getController()->redirect(['/ressource/view', 'id' => $model->ressourceId]);
				return;
			}
		}

		$this->getController()->render(
			'create',
			['model' => $model]
		);
	}
}
