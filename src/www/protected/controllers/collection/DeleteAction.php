<?php

namespace controllers\collection;

use CHtml;
use CLogger;
use processes\collection\Delete;
use Yii;

class DeleteAction extends \CAction
{
	public function run(string $id)
	{
		$model = \CollectionController::loadModel($id);
		$process = new Delete($model);
		$hasConfirmed = (int) Yii::app()->request->getParam('confirm');
		if (Yii::app()->request->isPostRequest && $process->isReady($hasConfirmed)) {
			$success = $process->delete();
			if (empty($_GET['ajax'])) {
				if ($success) {
					Yii::app()->user->setFlash(
						'success',
						"Collection « " . CHtml::encode($model->nom) . " » supprimée."
					);
					$this->getController()->redirect(['/ressource/view', 'id' => $model->ressourceId]);
					return;
				}
				Yii::app()->user->setFlash(
					'error',
					"La suppression a échoué. Contactez un administrateur qui consultera les logs techniques."
				);
				Yii::log("Erreur lors de la suppression de collection : " . $process->getErrorMessage(), CLogger::LEVEL_ERROR);
			} else {
				\Controller::header('json');
				echo json_encode(['success' => $success, 'error' => $process->getErrorMessage()]);
				return;
			}
		}

		/**
		 * @todo Prévoir une liste des titres liés à cette collection via des accès ?
		 */
		$this->getController()->render(
			'delete',
			['process' => $process]
		);
	}
}
