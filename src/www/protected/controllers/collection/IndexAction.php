<?php

namespace controllers\collection;

use CDbCriteria;
use CHttpException;
use Collection;
use Ressource;

class IndexAction extends \CAction
{
	public function run(string $ressourceId)
	{
		$ressource = Ressource::model()->findByPk($ressourceId);
		if (empty($ressource)) {
			throw new CHttpException(404, "La liste des ressources doit recevoir un paramètre ressourceId");
		}

		$criteria = new CDbCriteria(['order' => 'nom ASC']);
		$criteria->addColumnCondition(['ressourceId' => $ressource->id]);
		$collections = Collection::model()->findAll($criteria);

		$this->getController()->render(
			'index',
			[
				'collections' => $collections,
				'ressource' => $ressource,
			]
		);
	}
}
