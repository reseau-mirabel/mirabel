<?php

namespace controllers\collection;

use Collection;

/**
 * JSON list of collections.
 */
class AjaxListAction extends \CAction
{
	public function run(string $ressourceId, string $excludeImport = '')
	{
		/** @var Collection[] */
		$query = Collection::model()->findAllBySql(
			"SELECT * FROM Collection WHERE ressourceId = :ressourceId AND visible = 1"
				. ($excludeImport ? " AND importee = 0" : "")
				. " ORDER BY nom ASC",
			[':ressourceId' => (int) $ressourceId]
		);

		\Controller::header('json');
		$response = [];
		foreach ($query as $c) {
			$response[] = [
				'label' => "{$c->nom}   ({$c->type})" . ($c->importee ? " (importée)" : ""),
				'id' => (int) $c->id,
				'hint' => $c->description,
				'imported' => (bool) $c->importee,
				'visible' => (bool) $c->visible,
			];
		}
		echo json_encode($response);
	}
}
