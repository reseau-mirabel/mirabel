<?php

namespace controllers\collection;

class ViewAction extends \CAction
{
	public function run(string $id)
	{
		$model = \CollectionController::loadModel($id);
		$this->getController()->redirect(['/ressource/view', 'id' => $model->ressourceId]);
	}
}
