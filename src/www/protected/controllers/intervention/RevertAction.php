<?php

namespace controllers\intervention;

use CHttpException;
use Intervention;
use Yii;

class RevertAction extends \CAction
{
	public function run($id)
	{
		$model = Intervention::model()->findByPk((int) $id);
		if (!($model instanceof Intervention)) {
			throw new CHttpException(404, "Cette intervention n'existe plus.");
		}
		if (!Yii::app()->user->checkAccess('validate', $model)) {
			throw new CHttpException(403, "Vous n'avez pas la permission de valider sur cette intervention.");
		}

		try {
			$success = $model->revert();
		} catch (\Exception $e) {
			if (Yii::app()->request->isAjaxRequest) {
				Yii::app()->end();
			}
			Yii::app()->user->setFlash(
				'error',
				"L'intervention n'a pas pu être annulée, probablement parce que les données ont été modifiées : " . $e->getMessage()
			);
			$this->controller->redirect(['view', 'id' => $model->id]);
			return;
		}
		if ($success) {
			if (Yii::app()->request->isAjaxRequest) {
				return;
			}
			Yii::app()->user->setFlash('success', "Intervention annulée.");
			$this->controller->redirect(['view', 'id' => $model->id]);
		} else {
			Yii::app()->user->setFlash(
				'error',
				"L'intervention n'a pas pu être annulée. " . join(" ; ", $model->render()->errors())
			);
			$this->controller->redirect(['view', 'id' => $model->id]);
		}
	}
}
