<?php

namespace controllers\intervention;

use CHttpException;
use Intervention;
use InterventionDetail;
use models\sherpa\Hook;
use processes\titres\Cover;
use processes\titres\TrierTitre;
use Yii;

class AcceptAction extends \CAction
{
	/**
	 * Accepts and applies the changes.
	 */
	public function run(string $id, string $force = '')
	{
		$model = Intervention::model()->findByPk((int) $id);
		if (!($model instanceof Intervention)) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		if (!Yii::app()->user->checkAccess('validate', $model)) {
			throw new CHttpException(403, "Vous n'avez pas la permission de valider cette intervention.");
		}

		if ($force) {
			if (is_string($model->contenuJson)) {
				$model->contenuJson = new InterventionDetail(json_decode($model->contenuJson, true));
			}
			$model->contenuJson->confirm = true;
		}
		try {
			Hook::enrichIntervention($model);
			$success = $model->accept();
		} catch (\Throwable $e) {
			$this->crash($e, (int) $model->id);
			return;
		}

		if ($model->titreId && $model->contenuJson->hasUrlCoverChange()) {
			Cover::update($model->titre);
		}

		if ($success) {
			if (Yii::app()->request->isAjaxRequest) {
				Yii::app()->end();
			} else {
				Yii::app()->user->setFlash('success', "Intervention acceptée et appliquée.");
				if ($model->revue && TrierTitre::isRevueNeedReordering($model->revue)) {
					$this->getController()->redirect(['/revue/trier-titre', 'id' => $model->revueId]);
				} else {
					$this->getController()->redirect(['view', 'id' => $model->id]);
				}
			}
		} else {
			$errors = join("</li><li>", $model->render()->errors());
			Yii::app()->user->setFlash('error', <<<HTML
				<p>L'intervention n'a pas pu être acceptée :</p>
				<ul><li>$errors</li></ul>
				<p>Vous devez donc rejeter l'intervention et intervenir manuellement pour intégrer les modifications proposées que vous souhaitez prendre en compte.</p>
				HTML
			);
			$this->getController()->redirect(['view', 'id' => $model->id, 'statut' => 'warning']);
		}
	}

	private function crash(\Throwable $e, int $interventionId): void
	{
		if (Yii::app()->request->isAjaxRequest) {
			Yii::app()->end();
			return;
		}
		Yii::app()->user->setFlash('error', <<<HTML
			<p>
				L'intervention n'a pas pu être appliquée parce qu'une erreur s'est déclenchée,
				probablement à cause d'un conflit de données :
			</p>
			<code><pre>{$e->getMessage()}</pre></code>
			<p>
				Vous devez donc rejeter l'intervention et intervenir manuellement
				pour intégrer les modifications proposées que vous souhaitez prendre en compte.
			</p>
			HTML
		);
		$this->getController()->redirect(['view', 'id' => $interventionId]);
	}
}
