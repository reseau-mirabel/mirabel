<?php

namespace controllers\intervention;

use components\email\Mailer;
use CHttpException;
use Config;
use Yii;
use Suivi;
use Exception;

class SendReminderAction extends \CAction
{
	public function run($id)
	{
		if (!Yii::app()->user->checkAccess('utilisateur:admin')) {
			throw new CHttpException(403, "Vous n'avez pas la permission de valider sur cette intervention.");
		}
		$controller = $this->getController();
		assert($controller instanceof \InterventionController);
		$model = $controller->loadModel($id);

		$utilisateurToNotify = $this->loadUtilisateurToNotifyIfValid($model);
		$emails = self::listRecipients($utilisateurToNotify);
		if (Yii::app()->request->isPostRequest) {
			if (!$emails) {
				echo "<html><head><title>Erreur</title></head><body>Aucun destinataire. Revenez à la page précédente et complétez.</body></html>";
				return;
			}
			try {
				if (self::sendMail($emails)) {
					Yii::app()->user->setFlash('success', "Courriel de rappel envoyé.");
				} else {
					Yii::app()->user->setFlash('error', "Erreur lors de l'envoi du courriel");
				}
				$controller->redirect(["Intervention/view", 'id' => $id]);
				return;
			} catch (Exception $e) {
				Yii::app()->user->setFlash('error', "Une erreur de serveur de messagerie s'est produite. Erreur: " . $e->getMessage());
			}
		}

		$subject = Config::read("alerte.default.subject");
		if (!$subject) {
			$subject = "Proposition à traiter dans Mir@bel";
		}
		$subject = "[" . Yii::app()->name . "] " . $subject;

		$rawBody = Config::read("alerte.default.body");
		if (!$rawBody) {
			$rawBody = <<<EOTEXT
				Bonjour,

				une proposition de mise à jour a été faite sur une revue ou ressource que vous suivez dans Mir@bel.

				%INTERVENTIONS%

				Merci d'avance du suivi de cette proposition (validation ou rejet et réponse éventuelle).
				Ceci est un message automatique généré par Mir@bel. Pour toute question : contact@reseau-mirabel.info
				EOTEXT;
		}
		$body = str_replace('%INTERVENTIONS%', $model->renderAsText(), $rawBody);

		$controller->render(
			'mail-reminder',
			[
				'body' => $body,
				'id' => $id,
				'emails' => $emails,
				'subject' => $subject,
			]
		);
	}

	private static function listRecipients(\Partenaire $utilisateurToNotify): array
	{
		$destEmails = strpos($utilisateurToNotify->email, "\n")
			? array_filter(array_map('trim', explode("\n", $utilisateurToNotify->email)))
			: [$utilisateurToNotify->email];
		$destName = null;
		if (!empty($utilisateurToNotify->nomComplet)) {
			$destName = $utilisateurToNotify->nomComplet;
		} elseif (!empty($utilisateurToNotify->sigle)) {
			$destName = $utilisateurToNotify->sigle;
		} elseif (!empty($utilisateurToNotify->nom)) {
			$destName = $utilisateurToNotify->nom;
		}
		$emails = [];
		foreach ($destEmails as $e) {
			if ($destName) {
				$emails[$e] = $destName;
			} else {
				$emails[] = $e;
			}
		}
		return $emails;
	}

	private function loadUtilisateurToNotifyIfValid($model): ?\Partenaire
	{
		$utilisateurToNotify = Suivi::isTracked($model);
		if (!$utilisateurToNotify) {
			Yii::app()->user->setFlash('error', "Utilisateur suivant l'intervention introuvable.");
			$this->getController()->redirect(['admin']);
			return null;
		}
		return $utilisateurToNotify;
	}

	private static function sendMail(array $emails): bool
	{

		$subject = (string) $_POST['subject'];
		$body = (string) $_POST['body'];

		$from = Config::read('email.from.alertes');
		if (!$from) {
			$from = Config::read('email.from');
		}

		$message = Mailer::newMail()
			->subject($subject)
			->from(new \Symfony\Component\Mime\Address($from, "Utilisateur Mir@bel"))
			->setTo($emails)
			->text($body);
		return Mailer::sendMail($message);
	}
}
