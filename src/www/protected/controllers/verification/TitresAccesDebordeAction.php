<?php

namespace controllers\verification;

class TitresAccesDebordeAction extends \CAction
{
	public function run(): void
	{
		$this->getController()->render(
			'titre-acces-deborde',
			['data' => new \processes\verification\TitreAccesDeborde()]
		);
	}
}
