<?php

use components\YiiAccessToken;

class AnalyseController extends Controller
{
	public function filters(): array
	{
		return [
			'accessControl',
		];
	}

	/**
	 * This method is used by the 'accessControl' filter.
	 */
	public function accessRules(): array
	{
		$token = (new YiiAccessToken)->hasToken();
		if ($token === YiiAccessToken::RES_OK) {
			// Grant access to any user that has the right cookie for this route.
			return [['allow', 'users' => ['*']]];
		}
		if ($token === YiiAccessToken::RES_EXPIRED) {
			Yii::app()->user->setFlash('warning', "Le jeton d'accès à cette partie du site a expiré.");
			Yii::app()->request->getCookies()->remove(YiiAccessToken::COOKIE_NAME);
		}
		return [
			[
				'allow',
				'actions' => ['compare-issn', 'download', 'index', 'report'],
				'users' => ['@'],
			],
			[
				'allow',
				'actions' => ['create-token'],
				'roles' => ['admin'],
			],
			[
				'deny',  // otherwise, deny all users
				'users' => ['*'],
			],
		];
	}

	/**
	 * Maps actions to dedicated classes.
	 */
	public function actions()
	{
		return [
			'compare-issn' => \controllers\analyse\CompareIssnAction::class,
			'create-token' => \controllers\analyse\CreateTokenAction::class,
			'download' => \controllers\analyse\DownloadAction::class,
			'report' => \controllers\analyse\ReportAction::class,
		];
	}

	public function actionIndex()
	{
		$this->render('index');
	}
}
