<?php

class AdminController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl',
			'postOnly + deconnexion, fin-maintenance',
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'actions' => ['politique', 'politique-export', 'politique-utilisateurs', 'politique-validation'],
				'roles' => ['politiques'],
			],
			[
				'allow',
				'roles' => ['admin'],
			],
			[
				'deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	/**
	 * Maps actions to dedicated classes.
	 */
	public function actions(): array
	{
		return [
			'deconnexion' => 'controllers\admin\DeconnexionAction',
			'fin-maintenance' => 'controllers\admin\FinMaintenanceAction',
			'index' => 'controllers\admin\IndexAction',
			'politique' => 'controllers\admin\PolitiqueAction',
			'politique-export' => 'controllers\admin\PolitiqueExportAction',
			'politique-utilisateurs' => 'controllers\admin\PolitiqueUtilisateursAction',
			'politique-validation' => 'controllers\admin\PolitiqueValidationAction',
		];
	}
}
