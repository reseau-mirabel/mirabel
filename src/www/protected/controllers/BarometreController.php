<?php

class BarometreController extends Controller
{
	public function filters(): array
	{
		return [
			'accessControl',
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'actions' => ['index'],
				'roles' => ['admin'],
			],
			[
				'allow',
				'actions' => ['view-graph', 'viewGraph'],
				'users' => ['*'],
			],
			[
				'deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	public function actionViewGraph(string $graphName, string $nu = "")
	{
		// Access control: allow all if "nu" is set, allow all if nu is not set
		if ($nu === '' && !\Yii::app()->user->checkAccess('admin')) {
			if (\Yii::app()->user->isGuest) {
				\Yii::app()->user->loginRequired();
				return;
			}
			throw new CHttpException(403, "Réservé aux admins.");

		}

		if (!preg_match('/^\w+$/', $graphName)) {
			throw new CHttpException(400, "Nom de graphique invalide.");
		}
		$graphDirectory = self::getGraphDirectory();
		$graphPath = "{$graphDirectory}/{$graphName}.html";
		if (!file_exists($graphPath)) {
			throw new CHttpException(404, "Le graphique demandé n'existe pas.");
		}

		// Load the plotly assets.
		$am = \Yii::app()->assetManager;
		$cs = \Yii::app()->clientScript;
		$plotlyPath = VENDOR_PATH . '/npm-asset/plotly.js-dist/plotly.js';
		$cs->registerScriptFile($am->publish($plotlyPath));

		$graphContent = file_get_contents($graphPath);
		if ($nu !== '') {
			$this->layout = 'empty';
			Yii::app()->getEventHandlers('onEndRequest')->clear(); // Disable potential HTML loggers
		}
		$this->render('viewGraph', ['graphContent' => $graphContent, 'graphName' => $graphName]);
	}

	public function actionIndex()
	{
		$graphDirectory = self::getGraphDirectory();
		$graphFiles = glob("$graphDirectory/*.html");
		if (empty($graphFiles)) {
			$this->render('index', ['graphNames' => null, 'modificationTime' => null]);
			return;
		}
		$modificationTime = date('d-m-Y', filemtime($graphFiles[0]));
		$graphName = array_map(
			function ($file) {
				return basename($file, '.html');
			},
			$graphFiles
		);
		natsort($graphName);
		$this->render('index', ['graphNames' => $graphName, 'modificationTime' => $modificationTime]);
	}

	private static function getGraphDirectory(): string
	{
		$graphDirectory = Yii::app()->params['graphDir'];
		if (!$graphDirectory) {
			throw new \CHttpException(500, "Le paramètre 'graphDir' n'est pas défini alors qu'il est nécessaire pour accéder aux graphiques.");
		}
		if ($graphDirectory[0] !== '/') {
			$graphDirectory = DATA_DIR . "/public/$graphDirectory";
		}
		if (!is_dir($graphDirectory)) {
			throw new \CHttpException(500, "Le chemin '$graphDirectory' (config 'graphDir') n'existe pas.");
		}
		return rtrim($graphDirectory, '/');
	}
}
