<?php

class PolitiqueController extends Controller
{
	public $layout = '//layouts/column1';

	public function filters(): array
	{
		return [
			'accessControl',
		];
	}

	/**
	 * This method is used by the 'accessControl' filter.
	 */
	public function accessRules(): array
	{
		return [
			[
				'allow',
				'actions' => ['api', 'api-test', 'api-test-1', 'api-test-2', 'index', 'utilisateur'],
			],
			[
				'allow',
				'users' => ['@'],
			],
			[
				'deny',  // otherwise, deny all users
				'users' => ['*'],
			],
		];
	}

	/**
	 * Maps actions to dedicated classes.
	 */
	public function actions(): array
	{
		return [
			'ajax-cairn-url' => 'controllers\politique\AjaxCairnUrlAction',
			'ajax-candidates' => 'controllers\politique\AjaxCandidatesAction',
			'ajaxCandidates' => 'controllers\politique\AjaxCandidatesAction',
			'ajax-publish' => 'controllers\politique\PublishAction',
			'ajax-publishers' => 'controllers\politique\AjaxPublishersAction',
			'annotate' => 'controllers\politique\AnnotateAction', // access control inside class
			'api' => 'controllers\politique\ApiAction',
			'api-test' => 'controllers\politique\ApiTestAction',
			'api-test-1' => [
				'class' => \controllers\politique\ApiTestAction::class,
				'sqlitefile' => 'politique-test-1.sqlite',
			],
			'api-test-2' => [
				'class' => \controllers\politique\ApiTestAction::class,
				'sqlitefile' => 'politique-test-2.sqlite',
			],
			'assign' => 'controllers\politique\AssignAction',
			'assistant' => 'controllers\politique\AssistantAction',
			'delete' => 'controllers\politique\DeleteAction',
			'index' => 'controllers\politique\IndexAction',
			'jetons' => 'controllers\politique\JetonsAction',
			'export-utilisateurs' => \controllers\politique\ExportUtilisateursAction::class,
			'load-sherpa' => 'controllers\politique\LoadSherpaAction',
			'moderation' => 'controllers\politique\ModerationAction',
			'publish' => 'controllers\politique\PublishAction',
			'reject' => 'controllers\politique\RejectAction',
			'roles' => 'controllers\politique\RolesAction',
			'save' => 'controllers\politique\SaveAction',
			'titres' => 'controllers\politique\TitresAction',
			'unlink' => 'controllers\politique\UnlinkAction',
			'update' => 'controllers\politique\UpdateAction',
			'utilisateur' => 'controllers\politique\UtilisateurAction',
		];
	}
}
