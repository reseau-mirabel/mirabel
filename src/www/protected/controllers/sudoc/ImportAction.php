<?php

namespace controllers\sudoc;

use CUploadedFile;
use models\forms\SudocImport;
use models\sudoc\Import;
use Yii;

class ImportAction extends \CAction
{
	public function run(): void
	{
		$path = DATA_DIR . '/public/sudoc-import';
		if (!is_dir($path)) {
			mkdir($path, 0770);
		}
		$reportFile = "";

		$model = new SudocImport();
		if (Yii::app()->request->isPostRequest) {
			$model->setAttributes($_POST['SudocImport']);
			$model->xlsx = CUploadedFile::getInstance($model, "xlsx");
			if ($model->validate()) {
				$sudocImport = new Import();
				$sudocImport->verbosity = 0;
				$sudocImport->simulation = $model->simulation;
				$reportFile = preg_replace('/\.xlsx$/i', '_imported.xlsx', $model->xlsx->name);
				$sudocImport->file($model->xlsx->tempName, "$path/$reportFile");
			}
		}
		$this->getController()->render(
			'import',
			[
				'model' => $model,
				'reportFile' => $reportFile,
			]
		);
	}
}
