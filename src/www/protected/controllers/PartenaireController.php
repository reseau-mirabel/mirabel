<?php

use controllers\partenaire as actions;

class PartenaireController extends Controller
{
	/**
	 * @var string the default layout for the views. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl',
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'actions' => ['carte', 'grappes', 'index', 'view'],
				'users' => ['*'],
			],
			[
				'allow',
				'actions' => ['abonnements', 'ajax-refresh-hash', 'create', 'possession', 'suivi', 'tableau-de-bord', 'update', 'view-admin'],
				'roles' => ['avec-partenaire'], // finer controls are inside actions
			],
			[
				'allow',
				'roles' => [
					'partenaire:admin' => ['Partenaire' => Partenaire::model()->findByPk(Yii::app()->request->getQuery('id'))],
				],
			],
			[
				'deny',  // otherwise deny all users
				'users' => ['*'],
			],
		];
	}

	/**
	 * Maps actions to dedicated classes.
	 */
	public function actions(): array
	{
		return [
			'abonnements' => actions\AbonnementsAction::class,
			'admin' => actions\AdminAction::class,
			'ajax-refresh-hash' => actions\AjaxRefreshHashAction::class,
			'carte' => actions\CarteAction::class,
			'create' => actions\CreateAction::class,
			'delete' => actions\DeleteAction::class,
			'grappes' => actions\GrappesAction::class,
			'grappes-affichage' => actions\GrappesAffichageAction::class,
			'grappes-remove' => actions\GrappesRemoveAction::class,
			'grappes-selection' => actions\GrappesSelectionAction::class,
			'index' => actions\IndexAction::class,
			'logo' => actions\LogoAction::class,
			'personnaliser' => actions\CustomizeAction::class,
			'possession' => actions\PossessionAction::class,
			'tableau-de-bord' => actions\TableauDeBordAction::class,
			'update' => actions\UpdateAction::class,
			'suivi' => actions\SuiviAction::class,
			'view' => actions\ViewAction::class,
			'view-admin' => actions\ViewAdminAction::class,
		];
	}

	public function beforeRender($view)
	{
		if (!Yii::app()->user->checkAccess("avec-partenaire")) {
			$this->layout = '//layouts/column1';
		}
		return parent::beforeRender($view);
	}

	public static function loadModel(string $id): Partenaire
	{
		$model = Partenaire::model()->findByPk((int) $id);
		if ($model === null) {
			throw new \DetailedHttpException(
				404,
				"Cet établissement partenaire de Mir@bel n'a pas été trouvé",
				"Consultez la " . CHtml::link("liste des partenaires", ['index']) . "."
			);
		}
		$model->id = (int) $model->id;
		return $model;
	}
}
