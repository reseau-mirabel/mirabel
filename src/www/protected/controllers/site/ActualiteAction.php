<?php

namespace controllers\site;

class ActualiteAction extends \CAction
{
	public function run(string $id = '')
	{
		$q = \Yii::app()->request->getQuery('q', []);
		$search = new \models\cms\BreveSearchForm();
		if ($q) {
			$search->setAttributes($q);
			if (!$search->validate()) {
				$this->getController()->redirect('actualite');
				return;
			}
		}
		$search->startingId = (int) $id;

		$provider = $search->search([
			'pageVar' => 'p',
			'pageSize' => 100,
		]);

		$this->controller->render(
			'actualite',
			[
				'provider' => $provider,
				'search' => $search,
			]
		);
	}
}
