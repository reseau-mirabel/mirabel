<?php

namespace controllers\site;

use processes\politique\AlertesSuivi;
use Suivi;
use Yii;

class Index extends \CAction
{
	public function run(): void
	{
		$controller = $this->getController();
		assert($controller instanceof \Controller);
		$user = Yii::app()->user;

		$controller->backgroundL = true;
		$controller->backgroundR = true;
		$controller->pageTitle = "Mir@bel, le site qui facilite l'accès aux revues";

		$intvByCategory = [];
		$alertesPolitiques = null;
		if ($user->checkAccess("avec-partenaire")) {
			$intvByCategory = Suivi::listInterventionsTrackedByUser($user);
			$alertesPolitiques = new AlertesSuivi();
		}

		$controller->render(
			'index',
			[
				'intvByCategory' => $intvByCategory,
				'alertesPolitiques' => $alertesPolitiques,
			]
		);
	}
}
