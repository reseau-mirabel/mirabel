<?php

namespace controllers\site;

use CHttpException;
use Exception;
use SearchNavigation;
use Yii;
use models\searches\GlobalSearch;
use models\searches\GlobalSearchResult;

/**
 * Global Search.
 */
class Search extends \CAction
{
	public function run(string $global = '', string $type = '')
	{
		$model = new GlobalSearch();
		$search = new GlobalSearchResult();
		$totalCount = 0;
		if ($global !== "") {
			$model->q = trim($global);
			if (!in_array($type, GlobalSearch::INDEXES)) {
				$type = '';
			}
			if ($model->validate()) {
				if (empty($type)) {
					$search->titres = $model->search('titres');
					$search->ressources = $model->search('ressources');
					$search->editeurs = $model->search('editeurs');
				} else {
					$search->{$type} = $model->search($type);
				}

				$indexToKeyname = [
					'titres' => 'revueid',
					'ressources' => 'id',
					'editeurs' => 'id',
				];
				foreach (GlobalSearch::INDEXES as $index) {
					if (empty($search->{$index})) {
						continue;
					}
					$s = $search->{$index};
					$totalCount += $s->getItemCount();
					if ($s->itemCount > 1) {
						$searchCache = new SearchNavigation();
						$search->hashes[$index] = $searchCache->save(
							'site/search',
							['global' => $model->q],
							$indexToKeyname[$index],
							$s->getData(),
							$s->pagination
						);
					}
				}
				if ($totalCount === 1) {
					$this->redirect($search);
					return;
				}
			}
		}

		$this->getController()->render(
			'search',
			[
				'model' => $model,
				'search' => $search,
				'totalCount' => $totalCount,
			]
		);
	}

	private function redirect(GlobalSearchResult $searchResult): void
	{
		$controller = $this->getController();
		if (isset($searchResult->titres) && $searchResult->titres->itemCount == 1) {
			$data = $searchResult->titres->getData();
			assert(count($data) > 0);
			$first = reset($data);
			$controller->redirect(['revue/view', 'id' => $first->revueid]);
		} elseif (isset($searchResult->ressources) && $searchResult->ressources->itemCount == 1) {
			$data = $searchResult->ressources->getData();
			assert(count($data) > 0);
			$first = reset($data);
			$controller->redirect(['ressource/view', 'id' => $first->id]);
		} else {
			$data = $searchResult->editeurs->getData();
			assert(count($data) > 0);
			$first = reset($data);
			$controller->redirect(['editeur/view', 'id' => $first->id]);
		}
	}
}
