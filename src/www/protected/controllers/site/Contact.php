<?php

namespace controllers\site;

use Config;
use Yii;
use components\email\Mailer;
use models\forms\ContactForm;

/**
 * Global contact page.
 */
class Contact extends \CAction
{
	public function run(string $sent = '0')
	{
		$model = new ContactForm();
		if (isset($_POST['ContactForm'])) {
			$model->setAttributes($_POST['ContactForm']);
			if (!empty($_POST['nopost'])) {
				// do nothing
			} elseif ($model->validate()) {
				$this->sendMessage($model);
				return;
			} elseif ($model->hasErrors('email')) {
				Yii::log(
					"Contact, spam probable :\nMODEL: " . print_r($model->attributes, true)
					. "\nUSER-AGENT: " . $_SERVER['HTTP_USER_AGENT'],
					'warning',
					'contact'
				);
				// fake success message
				Yii::app()->user->setFlash(
					'info',
					"Thank you for contacting us. Message rejté par suspicion de S P A M..."
				);
				$this->getController()->redirect((['contact', 'sent' => 1]));
				return;
			}
		}
		$this->getController()->render('contact', ['model' => $model, 'sent' => $sent]);
	}

	private function sendMessage(ContactForm $model): void
	{
		$message = Mailer::newMail()
			->subject('[contact] ' . $model->sujet)
			->from(Config::read('email.from'))
			->replyTo(new \Symfony\Component\Mime\Address($model->courriel, $model->name))
			->setTo(Config::read('contact.email'))
			->text(<<<TXT
				[Message envoyé par le formulaire de contact]
				[Expéditeur : {$model->name} <{$model->courriel}>]

				{$model->body}
				--
				{$model->name} <{$model->courriel}>
				TXT
			);
		if (Mailer::sendMail($message)) {
			Yii::log(
				"Contact, envoi réussi :\n" . self::formatMessageAsLog($message),
				'warning',
				'contact'
			);
			Yii::app()->user->setFlash(
				'info',
				'Nous vous remercions de votre intérêt pour Mir@bel et vous répondrons dans les meilleurs délais.'
			);
			$this->getController()->redirect((['contact', 'sent' => 1]));
		} else {
			Yii::log("Contact, envoi raté :\n" . self::formatMessageAsLog($message), 'error', 'contact');
			Yii::app()->user->setFlash(
				'error',
				'Le message n\'a pu être envoyé. Le problème a été signalé aux administrateurs.'
			);
			$this->getController()->refresh();
		}
	}

	private static function formatMessageAsLog(\Symfony\Component\Mime\Message $message): string
	{
		return sprintf(
			"HEADERS: %s\nBODY: %s\nUSER-AGENT: %s\n",
			quoted_printable_decode($message->getHeaders()->toString()),
			quoted_printable_decode($message->getBody()->toString()),
			$_SERVER['HTTP_USER_AGENT']
		);
	}
}
