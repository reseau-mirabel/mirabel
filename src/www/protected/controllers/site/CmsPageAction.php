<?php

namespace controllers\site;

use CHtml;
use CHttpException;
use Cms;
use Controller;
use Norm;
use Yii;

/**
 * The CmsPageAction class reads its content from the DB through the Cms class.
 */
class CmsPageAction extends \CAction
{
	/**
	 * See UrlManager config.
	 */
	private const GET_PARAM = 'p';

	/**
	 * @var string name of the default view when {@link viewParam} GET parameter is not provided.
	 */
	public $defaultView = 'Actualité';

	/**
	 * @var mixed the name of the layout to be applied to the views.
	 * This will be assigned to CController::layout before the view is rendered.
	 * Defaults to null, meaning the controller's layout will be used.
	 * If false, no layout will be applied.
	 */
	public $layout;

	/**
	 * Displays the view requested by the user.
	 *
	 * @throws CHttpException if the view is invalid
	 */
	public function run(string $p = '')
	{
		if ($p === 'actualite') {
			$this->controller->redirect(['/site/actualite']);
			return;
		}

		$block = $this->getBlock($p);
		if ($block->private && !Yii::app()->user->checkAccess("avec-partenaire")) {
			Yii::app()->user->loginRequired();
			return;
		}

		$controller = $this->getController();
		assert($controller instanceof Controller);

		if ($this->layout !== null) {
			$layoutBackup = $controller->layout;
			$controller->layout = $this->layout;
		}
		$controller->pageTitle = $block->pageTitle;
		$controller->pageDescription = $block->dcdescription;
		$controller->breadcrumbs = [$block->pageTitle];
		$controller->setCanonicalUrl(['/site/page', self::GET_PARAM => $block->name]);
		$encodedName = CHtml::encode(preg_replace('/\s/', '-', Norm::unaccent($block->name)));
		$controller->renderText("<div id=\"page-{$encodedName}\">{$block->toHtml()}</div>");
		if (isset($layoutBackup)) {
			$controller->layout = $layoutBackup;
		}
	}

	private function getBlock(string $name): Cms
	{
		$viewName = $name ?: $_GET[self::GET_PARAM] ?? $this->defaultView;
		$block = Cms::model()->findByAttributes(
			['singlePage' => 1, 'name' => $viewName]
		);
		if (!$block) {
			$block = Cms::model()->findByAttributes(
				['singlePage' => 1, 'name' => str_replace('_', ' ', $viewName)]
			);
		}
		if (!($block instanceof Cms)) {
			throw new CHttpException(404, "Page non trouvée");
		}
		return $block;
	}
}
