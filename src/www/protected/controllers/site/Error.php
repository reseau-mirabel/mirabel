<?php

namespace controllers\site;

use Yii;

/**
 * This is the action to handle external exceptions.
 */
class Error extends \CAction
{
	public function run(): void
	{
		$error = Yii::app()->errorHandler->getError();
		if (!$error) {
			$this->getController()->redirect('/');
			return;
		}

		if (Yii::app()->request->isAjaxRequest) {
			echo $error['message'];
			return;
		}

		if (defined('YII_TEST') && YII_TEST) {
			fprintf(STDERR, "Fatal error %s : %s\n  in '%s' l. %d\n", $error['code'], $error['message'], $error['file'], $error['line']);
			return;
		}

		$ex = Yii::app()->errorHandler->getException();
		if ($ex instanceof \DetailedHttpException) {
			$error['details'] = $ex->details;
		}
		$this->getController()->render('/system/error', $error);
	}
}
