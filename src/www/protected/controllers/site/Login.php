<?php

namespace controllers\site;

use Yii;
use models\forms\Login as LoginForm;

class Login extends \CAction
{
	public function run(string $mode = '')
	{
		$controller = $this->getController();
		assert($controller instanceof \Controller);

		if (!Yii::app()->user->isGuest) {
			if ($mode === 'politique') {
				$controller->redirect('/politique');
			} else {
				$controller->redirect(Yii::app()->user->getReturnUrl());
			}
			return;
		}

		$model = new LoginForm;
		if (isset($_POST['LoginForm'])) {
			$model->attributes = $_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if ($model->validate() && $model->login()) {
				$this->redirect($mode, $model);
				return;
			}
		} elseif (
			!Yii::app()->user->getState('__returnUrl') && Yii::app()->request->urlReferrer
			&& strpos(Yii::app()->request->urlReferrer, Yii::app()->getBaseUrl(true)) === 0
		) {
			// When no return URL is set, use the referrer *if local to the site*.
			Yii::app()->user->setReturnUrl(Yii::app()->request->urlReferrer);
		}

		// display the login form
		$controller->render('login', ['mode' => $mode, 'model' => $model]);
	}

	private function redirect(string $mode, LoginForm $model): void
	{
		$cookies = Yii::app()->request->getCookies();
		$cookies->remove('bandeau-vu');

		$user = $model->getUtilisateur();

		$controller = $this->getController();
		if (!$user->partenaireId) {
			// utilisateur sans partenaire, donc politique des éditeurs
			$controller->redirect(['/politique']);
		}
		if ($user->getLoginMessages()) {
			// Yii1 looses flash messages upon login, so one more redirection
			Yii::app()->user->setReturnUrl(Yii::app()->user->getReturnUrl());
			$controller->redirect(['logged']);
		}
		if ($mode === 'politique') {
			$controller->redirect(['/politique']);
		} elseif (Yii::app()->user->checkAccess('indexation')) {
			$controller->redirect(Yii::app()->user->getReturnUrl('/categorie/candidates'));
		} else {
			$controller->redirect(Yii::app()->user->getReturnUrl());
		}
	}
}
