<?php

namespace controllers\site;

use Cms;

class Politiques extends \CAction
{
	public function run(): void
	{
		$this->getController()->render(
			'politiques',
			[
				'blocs' => Cms::model()->findAllBySql("SELECT * FROM Cms WHERE `name` LIKE 'page-politiques-_'"),
			]
		);
	}
}
