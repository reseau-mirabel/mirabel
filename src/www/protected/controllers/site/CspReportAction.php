<?php

namespace controllers\site;

/**
 * Add the CSP report to the logs.
 *
 * A web browser that encounters a forbidden resource, according to CSP rule,
 * (e.g. a JS script from an external URL while `script-src` is set to `'unsafe-inline' 'self'`)
 * will post to this action a JSON report about the infraction.
 */
class CspReportAction extends \CAction
{
	public function run()
	{
		if (!\Yii::app()->request->isPostRequest) {
			throw new \CHttpException(400);
		}
		$requestBody = file_get_contents('php://input');
		\Yii::log($requestBody, 'warning', 'csp');
	}
}
