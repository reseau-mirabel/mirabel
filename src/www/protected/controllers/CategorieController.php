<?php

use models\forms\IndexationImportForm;
use processes\categorie\IndexationImport;

class CategorieController extends Controller
{
	/**
	 * @var string the default layout for the views. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl',
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'actions' => ['index', 'ajaxChildrenNodes', 'ajaxNode', 'view', 'complete', 'revues'],
				'users' => ['*'],
			],
			[
				'allow',
				'actions' => ['admin', 'create', 'indexRefus'],
				'roles' => ['avec-partenaire'],
			],
			[
				'allow',
				'roles' => ['indexation'],
			],
			[
				'deny',  // deny all users by default
				'users' => ['*'],
			],
		];
	}

	public function beforeRender($view)
	{
		/** @var \components\WebSidebar */
		$sidebar = \Yii::app()->getComponent('sidebar');
		assert($sidebar instanceof \components\WebSidebar);
		$sidebar->menu = [
			['label' => "Thématique", 'itemOptions' => ['class' => 'nav-header']],
			['label' => 'Arbre thématique', 'url' => ['/categorie/index']],
			['label' => 'Administrer les thèmes', 'url' => ['/categorie/admin']],
			['label' => 'Nouveau thème', 'url' => ['/categorie/create']],
			['label' => 'Thèmes refusés', 'url' => ['/categorie/indexRefus']],
			['label' => "Import d'indexation", 'url' => ['/categorie/importIndexation'],
				'linkOptions' => ['title' => "Charger un CSV pour décrire des titres dans un vocabulaire, et affecter ainsi des thématiques aux revues."],
				'visible' => Yii::app()->user->checkAccess('indexation'),
			],
		];
		if (!empty($_GET['id'])) {
			$id = (int) $_GET['id'];
			array_push(
				$sidebar->menu,
				['label' => 'Ce thème', 'itemOptions' => ['class' => 'nav-header']],
				['label' => 'Détails', 'url' => ['view', 'id' => $id]],
				['label' => 'Modifier', 'url' => ['update', 'id' => $id]],
				[
					'label' => 'Supprimer', 'url' => '#',
					'linkOptions' => [
						'submit' => ['delete', 'id' => $id],
						'confirm' => 'Êtes-vous certain de vouloir supprimer ce thème ?',
					],
				]
			);
		}
		if (Yii::app()->user->checkAccess('indexation')) {
			array_push(
				$sidebar->menu,
				['label' => "Vocabulaires", 'itemOptions' => ['class' => 'nav-header']],
				['label' => 'Liste', 'url' => ['/vocabulaire/index'],
					'linkOptions' => ['title' => "Liste des vocabulaires."], ],
				['label' => 'Exporter les vocabulaires', 'url' => ['/vocabulaire/export'],
					'linkOptions' => ['title' => "Télécharger un CSV listant les thèmes et leurs éventuels alias dans chaque vocabulaire."], ],
				['label' => 'Importer des vocabulaires', 'url' => ['/vocabulaire/prepareImport'],
					'linkOptions' => ['title' => "Charger un CSV pour ajouter du contenu aux vocabulaires."], ]
			);
		}
		$this->breadcrumbs = ['Thématique' => ['index']];
		if (Yii::app()->user->isGuest) {
			$this->layout = '//layouts/column1';
		}
		return parent::beforeRender($view);
	}

	public function actionView($id, $name = '')
	{
		$model = $this->loadModel($id);
		if ($name !== $model->getSelfUrl()['name']) {
			$this->redirect($model->getSelfUrl());
		}
		$this->render(
			'view',
			['model' => $model]
		);
	}

	/**
	 * Creates a new model.
	 *
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($parentId = 0)
	{
		$model = new Categorie();
		if (!Yii::app()->user->checkAccess('indexation')) {
			$model->scenario = 'insert-sansperm';
		}

		if ($parentId) {
			$model->parentId = (int) $parentId;
		}
		if (isset($_POST['Categorie'])) {
			$model->attributes = $_POST['Categorie'];
			if (!Yii::app()->user->checkAccess('indexation')) {
				$model->role = 'candidat';
			}
			if ($model->save()) {
				Yii::app()->user->setFlash('success', "Nouveau thème <b>" . CHtml::encode($model->categorie) . "</b> créé.");
				$this->redirect(['admin']); // array('view', 'id' => $model->id)
				return;
			}
		}

		$this->render(
			'create',
			['model' => $model]
		);
	}

	/**
	 * Updates a particular model.
	 *
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param int $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		if (!Yii::app()->user->checkAccess('indexation')) {
			$model->scenario = 'update-sansperm';
		}

		if (
			!Yii::app()->user->checkAccess('indexation')
			&& ($model->role !== 'candidat' || $model->modifPar != Yii::app()->user->id)
		) {
			Yii::app()->user->setFlash('error', "Sans permission d'indexation, vous ne pouvez modifier que vos propres thèmes candidats.");
			$this->redirect('admin');
		}

		if (isset($_POST['Categorie'])) {
			$model->attributes = $_POST['Categorie'];
			if ($model->save()) {
				Yii::app()->user->setFlash('success', "Thème modifié.");
				$this->redirect(['admin']); // array('view', 'id' => $model->id)
				return;
			}
		}

		$this->render(
			'update',
			['model' => $model]
		);
	}

	/**
	 * Deletes a particular model.
	 *
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 *
	 * @param int $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if (!Yii::app()->request->isPostRequest) {
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}

		$result = $this->delete($id);
		Yii::app()->user->setFlash($result['success'], $result['message']);
		if ($result['success'] === 'success') {
			$this->redirect($_POST['returnUrl'] ?? ['admin']);
		} else {
			$this->redirect(['view', 'id' => $id]);
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		if (!Yii::app()->user->checkAccess('indexation')) {
			$this->layout = '//layouts/column1';
		}
		$this->render('index');
	}

	/**
	 * Lists all models with role='refus'.
	 */
	public function actionIndexRefus()
	{
		$dataProvider = new CActiveDataProvider('Categorie');
		$dataProvider->criteria = new CDbCriteria(['condition' => "role ='refus'"]);
		$this->render(
			'index-refus',
			['dataProvider' => $dataProvider]
		);
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		if (Yii::app()->user->checkAccess('indexation')) {
			Categorie::buildFlashForCandidates();
			$filterByUserId = 0;
		} else {
			$filterByUserId = Yii::app()->user->id;
		}

		$model = new Categorie('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Categorie'])) {
			$model->attributes = $_GET['Categorie'];
		}

		$this->render(
			'admin',
			['model' => $model, 'filterByUserId' => $filterByUserId]
		);
	}

	public function actionCandidates()
	{
		Categorie::buildFlashForCandidates();
		$this->redirect(Yii::app()->user->getReturnUrl());
	}

	public function actionComplete($term, $role = "", $minDepth = 2)
	{
		if (!Yii::app()->user->checkAccess("avec-partenaire")) {
			$role = 'public';
		}
		\Controller::header('json');
		$completer = new processes\completion\Categorie;
		echo json_encode($completer->completeTerm($term, $role, $minDepth));
		Yii::app()->end();
	}

	public function actionImportIndexation()
	{
		$import = new IndexationImport();
		$formData = new IndexationImportForm();
		if (isset($_POST['IndexationImportForm'])) {
			$formData->attributes = $_POST['IndexationImportForm'];
			$formData->csvfile = CUploadedFile::getInstance($formData, 'csvfile');
			if ($formData->validate()) {
				$import->vocabulaire = Vocabulaire::model()->findByPk($formData->vocabulaireId);
				$import->setSeparator($formData->separator);
				$import->removeOld = $formData->clean;
				$import->parseCsvFile($formData->csvfile->getTempName());
				if (!$formData->simulation) {
					$import->save(Yii::app()->user->id);
				}
			}
		}
		$this->render(
			'import-indexation',
			[
				'formData' => $formData,
				'import' => $import,
			]
		);
	}

	public function actionRevues($id, $name)
	{
		$model = $this->loadModel($id);
		$normalizedName = Norm::urlParam($model->categorie);
		if ($normalizedName !== $name) {
			$this->redirect($model->getSelfUrl());
			return;
		}

		$search = new SearchTitre(Yii::app()->user->getState('instituteId'));
		$search->categorie[] = $model->id;
		try {
			$results = $search->search(50);
		} catch (\Throwable $_) {
			throw new CHttpException(400, "Erreur dans la liste thématique. Navigation au-delà de la taille des résultats ?");
		}

		/* TODO enable this nav?
		if ($results) {
			if ($results->itemCount > 0) {
				$searchCache = new SearchNavigation();
				$searchHash = $searchCache->save(
					'revue/search', ['SearchTitre' => array_filter(['categorie' => [$model->id]])], 'SearchTitre', $results->getData(),
					$results->pagination
				);
			}
		}
		*/

		$this->render(
			'revues',
			[
				'model' => $model,
				'search' => $search,
				'results' => $results,
				//'searchHash' => $searchHash,
			]
		);
	}

	/**
	 * Return the JSON data of the Categorie tree.
	 *
	 * @param int $id categorieId
	 * @param int $depth (opt, 3) Max depth.
	 */
	public function actionAjaxNode($id = 1, $depth = 3, $extra = true, $role = '', $filterByUserId = 0)
	{
		if (!Yii::app()->user->checkAccess("avec-partenaire")) {
			$role = 'public';
		}
		$model = $this->loadModel($id);
		if ($model->profondeur > 0) {
			$nodes = $model->getNode($depth, $extra, $role, $filterByUserId);
		} else {
			$this->actionAjaxChildrenNodes($id, $depth, $extra, $role, $filterByUserId);
			return;
		}
		\Controller::header('json');
		echo json_encode($nodes, JSON_UNESCAPED_UNICODE);
	}

	/**
	 * Return the JSON data of the Categorie tree.
	 *
	 * @param int $id categorieId
	 * @param int $depth (opt, 3) Max depth.
	 */
	public function actionAjaxChildrenNodes($id = 1, $depth = 3, $extra = true, $role = '', $filterByUserId = 0)
	{
		if (!Yii::app()->user->checkAccess("avec-partenaire")) {
			$role = 'public';
		}
		$model = $this->loadModel($id);
		$nodes = array_filter(array_map(
			function (Categorie $x) use ($depth, $extra, $role, $filterByUserId) {
				if (!$role || $x->role === $role) {
					return $x->getNode($depth, (bool) $extra, $role, $filterByUserId);
				}
				return null;
			},
			$model->children
		));
		\Controller::header('json');
		echo json_encode($nodes, JSON_UNESCAPED_UNICODE);
	}

	/**
	 * Creates a new model (AJAX).
	 */
	public function actionAjaxCreate()
	{
		if (Yii::app()->request->isAjaxRequest && isset($_POST['Categorie'])) {
			$result = $this->create($_POST['Categorie']);
			if (isset($result['model'])) {
				unset($result['model']);
			}
		} else {
			$result = [
				'success' => 'error',
				'message' => "Données incomplètes.",
				'node' => null,
			];
		}

		\Controller::header('json');
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}

	/**
	 * Updates a model (AJAX).
	 */
	public function actionAjaxUpdate()
	{
		if (Yii::app()->request->isAjaxRequest && isset($_POST['Categorie']['id'])) {
			$result = $this->update($_POST['Categorie']['id'], $_POST['Categorie']);
		} else {
			$result = [
				'success' => 'error',
				'message' => "Données incomplètes.",
				'node' => null,
			];
		}

		\Controller::header('json');
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}

	/**
	 * Deletes a particular model (AJAX).
	 */
	public function actionAjaxDelete()
	{
		if (Yii::app()->request->isAjaxRequest && isset($_POST['id'])) {
			$result = $this->delete((int) $_POST['id']);
		} else {
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}

		\Controller::header('json');
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 *
	 * If the data model is not found, an HTTP exception will be raised.
	 *
	 * @param int $id the ID of the model to be loaded
	 */
	private function loadModel($id): Categorie
	{
		$model = Categorie::model()->findByPk((int) $id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		return $model;
	}

	private function create(array $data): array
	{
		$model = new Categorie();
		if (!Yii::app()->user->checkAccess('indexation')) {
			$model->scenario = 'insert-sansperm';
		}
		$model->attributes = $data;
		if ($model->save()) {
			$result = [
				'success' => 'success',
				'message' => "Le nouveau thème est enregistré.",
				'node' => $model->attributes,
				'model' => $model,
			];
		} else {
			$result = [
				'success' => 'error',
				'message' => "Le nouveau thème n'a pu être enregistré : " . print_r($model->getErrors(), true),
				'node' => $model->attributes,
				'model' => $model,
			];
		}
		return $result;
	}

	private function update($id, $data): array
	{
		$model = Categorie::model()->findByPk((int) $id);
		if (!Yii::app()->user->checkAccess('indexation')) {
			$model->scenario = 'update-sansperm';
		}
		if (!$model) {
			$result = [
				'success' => 'error',
				'message' => "Ce thème n'a pas été trouvé dans la base.",
				'node' => null,
			];
		} else {
			if (!Yii::app()->user->checkAccess('indexation') && $model->modifPar != Yii::app()->user->id) {
				$result = [
					'success' => 'error',
					'message' => "Vous n'avez pas la permission d'indexation. Vous ne pouvez modifier que les thèmes que vous avez créés.",
				];
			} else {
				$model->attributes = $data;
				if ($model->save()) {
					$result = [
						'success' => 'success',
						'message' => "Le thème est enregistré.",
						'node' => $model->attributes,
					];
				} else {
					$result = [
						'success' => 'error',
						'message' => "Le thème n'a pu être enregistré : " . print_r($model->getErrors(), true),
						'node' => $model->attributes,
					];
				}
			}
		}
		return $result;
	}

	private function delete(int $id): array
	{
		$model = Categorie::model()->findByPk((int) $id);
		if (!$model) {
			$result = [
				'success' => 'error',
				'message' => "Ce thème n'a pas été trouvé dans la base.",
			];
		} else {
			if (!Yii::app()->user->checkAccess('indexation') && $model->modifPar != Yii::app()->user->id) {
				$result = [
					'success' => 'error',
					'message' => "Vous n'avez pas la permission d'indexation. Vous ne pouvez supprimer que les thèmes que vous avez créés.",
				];
			} elseif ($model->children) {
				$result = [
					'success' => 'error',
					'message' => "Le thème n'a pu être supprimé car il a des sous-thèmes.",
				];
			} elseif ($model->categorieRevues) {
				$result = [
					'success' => 'error',
					'message' => "Le thème n'a pu être supprimé car il est associé à des revues.",
				];
			} else {
				try {
					$deleted = $model->delete();
				} catch (Exception $e) {
					$deleted = $e->getMessage();
				}
				if ($deleted == 1) {
					$result = [
						'success' => 'success',
						'message' => "Le thème a été supprimé.",
					];
				} else {
					$result = [
						'success' => 'error',
						'message' => "Le thème n'a pu être supprimé. A-t-il des sous-thèmes ? "
							. $deleted,
					];
				}
			}
		}
		return $result;
	}
}
