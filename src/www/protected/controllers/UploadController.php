<?php

use processes\upload\UploadDestination;

class UploadController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	public $defaultAction = 'index';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl', // perform access control
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'roles' => ['avec-partenaire'],
			],
			[
				'deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	/**
	 * Maps actions to dedicated classes.
	 */
	public function actions()
	{
		return [
			'index' => 'controllers\upload\IndexAction',
			'delete' => 'controllers\upload\DeleteAction',
			'view' => 'controllers\upload\ViewAction',
		];
	}

	/**
	 * Called before any action.
	 */
	public function init()
	{
		parent::init();
		Upload::$destinations = [
			'public' => UploadDestination::public(),
			'private' => UploadDestination::private(),
			'logos-p' => UploadDestination::logoPartenaire(),
			'videos' => UploadDestination::videos(),
			'editeurs' => UploadDestination::editeurs(),
			'conventions' => UploadDestination::conventions(),
		];
	}
}
