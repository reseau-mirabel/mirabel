<?php

namespace controllers\import;

use CHttpException;
use processes\kbart\Import;
use processes\kbart\logger\AggregateId;

/**
 * New KBART import, step 2/4
 */
class Kbart1 extends \CAction
{
	public function run(string $hash)
	{
		if (!preg_match('/^\w{32,40}$/', $hash)) {
			throw new CHttpException(400, 'Paramètre "hash" non valide.');
		}
		$import = Import::load($hash);

		if (\Yii::app()->request->isPostRequest) {
			if (!$import->hasDone(\processes\kbart\State::STEP_FILE)) {
				$import->readFile($hash);
			}
			if (!$import->hasDone(\processes\kbart\State::STEP_CONSISTENCY)) {
				$import->checkConsistency();
			}
			$import->save();
		}

		$model = new \processes\kbart\ImportForm();
		if ($import->getFileData()) {
			if ($import->getLogger()->getLocalSummary()->hasFamily(AggregateId::AccesstypeEmpty)) {
				$model->withAccessType = true;
			}
			if ($import->getLogger()->getLocalSummary()->hasFamily(AggregateId::CoveragedepthEmpty)) {
				$model->withCoverageDepth = true;
			}
		}

		$controller = $this->getController();
		$controller->layout = '//layouts/column1';
		$controller->render(
			'kbart1',
			[
				'import' => $import,
				'filedata' => $import->getFileData(),
				'logger' => $import->getLogger(),
				'model' => 	$model,
			]
		);
	}
}
