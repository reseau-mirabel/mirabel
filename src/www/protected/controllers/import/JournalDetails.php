<?php

namespace controllers\import;

use CHttpException;
use processes\kbart\Import;
use processes\kbart\noninteractive\Config;
use processes\kbart\work\ImportIdentifiedRows;

class JournalDetails extends \CAction
{
	public function run(string $hash, string $id, string $defaults)
	{
		$import = Import::load($hash);
		$config = new Config();
		foreach (json_decode($defaults, true, 4) as $k => $v) {
			$config->{$k} = $v;
		}

		$this->getController()->renderPartial(
			'journal-details',
			$this->getData($import, $config, (int) $id)
		);
	}

	public function getData(Import $import, Config $defaults, int $id): array
	{
		$state = $import->state;
		if (!isset($state->crossedData->known[$id])) {
			throw new CHttpException(500, "Pas de données sur ce titre pour cet import KBART");
		}
		$known = $state->crossedData->known[$id];

		$processor = new ImportIdentifiedRows($state->getRessource(), $state->collectionIds, $defaults);
		return [
			'analyzed' => $known,
			'diff' => $processor->computeDiff($known),
		];
	}
}
