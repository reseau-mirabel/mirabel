<?php

namespace controllers\import;

use CUploadedFile;
use models\forms\ImportKbart0;
use processes\kbart\Import;
use processes\kbart\State;
use Ressource;

/**
 * New KBART import, initial step (0/3 1/4)
 */
class Kbart0 extends \CAction
{
	public function run(): void
	{
		$controller = $this->getController();
		$controller->layout = '//layouts/column1';

		$model = new ImportKbart0();
		if (isset($_POST['ImportKbart0'])) {
			$model->setAttributes($_POST['ImportKbart0']);
			$model->kbartfile = CUploadedFile::getInstance($model, 'kbartfile');
			if ($model->validate() && $model->saveKbartFile()) {
				// upload OK, so we save the state and jump to the next step
				$import = $this->buildImport($model->getRessource(), $model->collectionIds);
				$import->state->title = $model->getUploadFilename();
				$filter = new \processes\kbart\data\Filter();
				$filter->accessType = (string) $model->filterAccessType;
				$import->state->filter = $filter->toArray();
				$hash = $model->getKbartFileHash();
				$import->save($hash);
				$controller->redirect(
					['kbart1', 'hash' => $hash],
					true,
					307
				);
				return;
			}
		}
		$controller->render(
			'kbart0',
			[
				'model' => $model,
				'collections' => $model->getPotentialCollections(),
			]
		);
	}

	private function buildImport(?Ressource $ressource, array $collectionIds): Import
	{
		if (!$ressource) {
			throw new \CHttpException(400, 'Paramètre "ressourceId" non valide.');
		}
		$state = new State($ressource, $collectionIds);
		return new Import($state);
	}
}
