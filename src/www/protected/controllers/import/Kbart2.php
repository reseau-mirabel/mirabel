<?php

namespace controllers\import;

use processes\kbart\Import;
use processes\kbart\logger\AggregateId;

/**
 * New KBART import, step 3/4
 */
class Kbart2 extends \CAction
{
	public function run(string $hash)
	{
		$import = Import::load($hash);
		$import->identifyTitles();
		$import->save();

		$model = new \processes\kbart\ImportForm();
		if ($import->getLogger()->getLocalSummary()->hasFamily(AggregateId::AccesstypeEmpty)) {
			$model->withAccessType = true;
		}
		if ($import->getLogger()->getLocalSummary()->hasFamily(AggregateId::CoveragedepthEmpty)) {
			$model->withCoverageDepth = true;
		}
		$model->setAttributes($_POST[\CHtml::modelName($model)]);

		$config = $model->exportConfig();
		$config->setRessourceAndCollections($import->state->getRessource()->id, $import->state->getCollectionIds());
		$diffs = $import->diff($config);

		$controller = $this->getController();
		$controller->layout = '//layouts/column1';
		$controller->render(
			'kbart2',
			[
				'import' => $import,
				'crossedData' => $import->state->crossedData,
				'diffs' => $diffs,
				'logger' => $import->getLogger(),
				'model' => $model,
			]
		);
	}
}
