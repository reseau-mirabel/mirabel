<?php

class IssnController extends Controller
{
	public $layout = '//layouts/column1';

	public function filters()
	{
		return ['accessControl'];
	}

	public function accessRules()
	{
		return [
			// allow authenticated
			['allow', 'roles' => ['admin'], ],
			// deny all users
			['deny', 'users' => ['*']],
		];
	}

	/**
	 * Maps actions to dedicated classes.
	 */
	public function actions(): array
	{
		return [
			'importpe' => 'controllers\issn\ImportpeAction',
		];
	}
}
