<?php

class ConfigController extends Controller
{
	public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl',
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'roles' => ['admin'],
			],
			[
				'deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model = new Config('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Config'])) {
			$model->setAttributes($_GET['Config']);
		}

		$this->render(
			'admin',
			[
				'model' => $model,
			]
		);
	}

	/**
	 * Creates a new model.
	 *
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new Config();

		if (isset($_POST['Config'])) {
			$model->attributes = $_POST['Config'];
			if ($model->save()) {
				$this->redirect(['view', 'id' => $model->id]);
			}
		}

		$this->render(
			'create',
			['model' => $model]
		);
	}

	/**
	 * Deletes a particular model.
	 *
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 *
	 * @param int $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		// we only allow deletion via POST request
		if (!Yii::app()->request->isPostRequest) {
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}

		$model = $this->loadModel($id);
		$success = $model->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if (!isset($_GET['ajax'])) {
			if ($success) {
				Yii::app()->user->setFlash('success', "Enregistrement supprimé.");
			} else {
				Yii::app()->user->setFlash('error', "La suppression a échoué.");
			}
			$this->redirect($_POST['returnUrl'] ?? ['admin']);
		} else {
			echo($success ? 'true' : 'false');
		}
	}

	public function actionIndex()
	{
		$this->render(
			'index',
			[
				'categories' => Yii::app()->db
					->createCommand("SELECT DISTINCT category FROM Config ORDER BY category")
					->queryColumn(),
			]
		);
	}

	/**
	 * Sets the value of an instance.
	 *
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param int $id the ID of the model to be updated
	 */
	public function actionSet($id)
	{
		$model = $this->loadModel($id);

		if (isset($_POST['Config'])) {
			$model->value = (string) $_POST['Config']['value'];
			if ($model->save()) {
				$this->redirect(['admin', 'Config' => ['category' => $model->category]]);
			}
		}

		$this->render(
			'set',
			['model' => $model]
		);
	}

	/**
	 * Updates a particular model.
	 *
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param int $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		if (isset($_POST['Config'])) {
			$model->attributes = $_POST['Config'];
			if ($model->save()) {
				$this->redirect(['view', 'id' => $model->id]);
			}
		}

		$this->render(
			'update',
			['model' => $model]
		);
	}

	/**
	 * Displays a particular model.
	 *
	 * @param int $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render(
			'view',
			['model' => $this->loadModel($id)]
		);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 *
	 * If the data model is not found, an HTTP exception will be raised.
	 *
	 * @param int $id the ID of the model to be loaded
	 * @throws CHttpException
	 * @return Config the loaded model
	 */
	public function loadModel($id): Config
	{
		$model = Config::model()->findByPk((int) $id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		return $model;
	}
}
