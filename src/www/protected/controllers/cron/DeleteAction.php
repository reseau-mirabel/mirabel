<?php

namespace controllers\cron;

use processes\cron\ar\Task;
use Yii;

class DeleteAction extends \CAction
{
	public function run(string $id)
	{
		if (!Yii::app()->request->isPostRequest) {
			throw new \CHttpException(405);
		}

		$task = Task::model()->findByPk((int) $id);
		if (!($task instanceof Task)) {
			throw new \CHttpException(404);
		}

		if ($task->delete()) {
			Yii::app()->user->setFlash('success', "La tâche « {$task->name} » est supprimée.");
			$this->getController()->redirect(['/cron/index']);
			return;
		}
		Yii::app()->user->setFlash('error', "La suppression a échoué.");
		$this->getController()->redirect(['/cron/view', 'id' => $task->id]);
	}
}
