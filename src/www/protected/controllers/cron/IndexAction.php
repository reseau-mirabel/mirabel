<?php

namespace controllers\cron;

use CHtml;
use processes\cron\ar\TaskSearch;
use Yii;

class IndexAction extends \CAction
{
	public function run(): void
	{
		CHtml::setModelNameConverter(function ($x) {
			if (is_string($x)) {
				if ($x === TaskSearch::class) {
					return 'q';
				}
			} else {
				if ($x instanceof TaskSearch) {
					return 'q';
				}
			}
		});

		$taskSearch = new TaskSearch();
		$taskSearch->setAttributes(Yii::app()->request->getQuery('q', []));

		$this->getController()->render(
			'index',
			[
				'taskSearch' => $taskSearch,
				'provider' => $taskSearch->search(),
			]
		);
	}
}
