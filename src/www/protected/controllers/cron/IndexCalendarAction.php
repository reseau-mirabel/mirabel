<?php

namespace controllers\cron;

use DateInterval;
use DateTimeImmutable;
use DateTimeInterface;
use processes\cron\ar\Task;
use processes\cron\Scheduler;

class IndexCalendarAction extends \CAction
{
	public function run(string $start, string $span)
	{
		if (!preg_match('/^20\d\d-\d\d-\d\d$/', $start)) {
			throw new \CHttpException(400, "Le paramètre 'start' n'est pas une date ISO YYYY-MM-DD.");
		}
		if (!preg_match('/^P\d+[DMY]$/', $span)) {
			throw new \CHttpException(400, "Le paramètre 'span' n'est pas un interval comme P7D ou P1M.");
		}

		$tasks = Task::model()->findAll();
		if (!$tasks) {
			\Yii::app()->user->setFlash('warning', "Aucune tâche, donc pas de vue calendaire.");
			$this->getController()->redirect(['/cron/index']);
			return;
		}

		$startDate = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', "$start 00:00:00");
		$endDate = $startDate->add(new DateInterval($span));
		$launches = self::listLaunches($tasks, $startDate, $endDate);

		$controller = $this->getController();
		assert($controller instanceof \Controller);
		$controller->containerClass = "container full-width";
		$controller->render(
			'index-calendar',
			[
				'launches' => $launches,
				'start' => $startDate,
				'end' => $endDate,
			]
		);
	}

	private function listLaunches(array $tasks, DateTimeImmutable $start, DateTimeInterface $end)
	{
		$scheduler = new Scheduler();
		$scheduler->start = $start;
		$launches = [];
		foreach ($tasks as $task) {
			assert($task instanceof Task);
			if ($task->getScheduleObject()->periodicity == "hourly") {
				$launchDate = $scheduler->listNextRuns($task->getScheduleObject(), 1)[0];
				$launches[] = ['time' => $launchDate, 'task' => $task];
			} else {
				foreach ($scheduler->listNextRunsUntil($task->getScheduleObject(), $end) as $launchDate) {
					$launches[] = ['time' => $launchDate, 'task' => $task];
				}
			}
		}
		usort(
			$launches,
			function (array $a, array $b) {
				return $a['time'] <=> $b['time'];
			}
		);
		return $launches;
	}
}
