<?php

namespace controllers\cron;

use processes\cron\ar\Task;
use Yii;

class ForceRunAction extends \CAction
{
	public function run(string $id, string $status)
	{
		if (!Yii::app()->getRequest()->isPostRequest) {
			throw new \CHttpException(400);
		}

		$record = \processes\cron\ar\Task::model()->findByPk((int) $id);
		if (!($record instanceof Task)) {
			throw new \CHttpException(404);
		}

		$record->forceRun = (bool) $status;
		$record->update(['forceRun']);
		$this->getController()->redirect(['/cron/view', 'id' => $record->id]);
	}
}
