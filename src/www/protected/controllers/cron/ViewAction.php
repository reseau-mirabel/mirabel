<?php

namespace controllers\cron;

use processes\cron\ar\Task;
use processes\cron\ar\History;

class ViewAction extends \CAction
{
	public function run(string $id)
	{
		$task = Task::model()->findByPk((int) $id);
		if (!($task instanceof Task)) {
			throw new \CHttpException(404);
		}

		if (\processes\cron\CronState::hasCrashed($task)) {
			\Yii::app()->user->setFlash('warning', "La dernière exécution s'est terminée sans signaler sa fin (plantage brutal).");
			\Yii::app()->db
				->createCommand("DELETE FROM CronLock WHERE crontaskId = {$task->id}")
				->execute();
		}

		$this->getController()->render(
			'view',
			[
				'task' => $task,
				'history' => History::model()->findAllBySql(
					"SELECT * FROM CronHistory WHERE crontaskId = {$task->id} ORDER BY id DESC LIMIT 5"
				),
				'historyNotEmpty' => self::findHistoryNotEmpty($task->id),
			]
		);
	}

	private static function findHistoryNotEmpty(int $taskId): ?History
	{
		$hasRecentContent = !\Yii::app()->db
			->createCommand("SELECT MIN(reportEmpty) FROM CronHistory WHERE crontaskId = {$taskId} ORDER BY id DESC LIMIT 5")
			->queryScalar();
		if ($hasRecentContent) {
			return null;
		}
		return History::model()->findBySql(
			"SELECT * FROM CronHistory WHERE crontaskId = {$taskId} AND reportEmpty = 0 ORDER BY id DESC LIMIT 1"
		);
	}
}
