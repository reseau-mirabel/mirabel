<?php

namespace controllers\cron;

use processes\cron\ar\Task;
use processes\cron\TaskForm;
use processes\cron\widgets\TaskFormWidget;
use Yii;

class CreateAction extends \CAction
{
	public function run(): void
	{
		$widget = new TaskFormWidget();
		$body = Yii::app()->getRequest()->getPost('body', null);
		if ($body) {
			$data = json_decode($body, true, 64, JSON_THROW_ON_ERROR);
			$formData = new TaskForm;
			$formData->setAttributes($data);
			if ($formData->validate()) {
				$record = new Task;
				$formData->updateRecord($record);
				if ($record->save()) {
					Yii::app()->user->setFlash('success', "La nouvelle tâche est enregistrée dans le cron.");
					$this->getController()->redirect(['/cron/view', 'id' => $record->id]);
					return;
				}
				Yii::log("Erreur en enregistrant la nouvelle tâche " . print_r($record->getErrors(), true), 'error');
				Yii::app()->user->setFlash('error', "Erreur en enregistrant la nouvelle tâche dans le cron.");
			} else {
				$widget->state = $formData->getAttributes();
				$widget->errors = $formData->getErrors();
				Yii::app()->user->setFlash('error', "Le formulaire est incomplet ou mal rempli.");
			}
		}
		$this->getController()->render(
			'create',
			[
				'widget' => $widget,
			]
		);
	}
}
