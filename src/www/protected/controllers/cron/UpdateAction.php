<?php

namespace controllers\cron;

use processes\cron\ar\Task;
use processes\cron\TaskForm;
use processes\cron\widgets\TaskFormWidget;
use Yii;

class UpdateAction extends \CAction
{
	public function run(string $id)
	{
		$record = \processes\cron\ar\Task::model()->findByPk((int) $id);
		if (!($record instanceof Task)) {
			throw new \CHttpException(404);
		}

		$widget = new TaskFormWidget();
		$widget->loadStateFromRecord($record);

		$body = Yii::app()->getRequest()->getPost('body', null);
		if ($body) {
			$data = json_decode($body, true, 64, JSON_THROW_ON_ERROR);
			$formData = new TaskForm;
			$formData->setAttributes($data);
			if ($formData->validate()) {
				$formData->updateRecord($record);
				if ($record->save()) {
					Yii::app()->user->setFlash('success', "La modification de la tâche est enregistrée.");
					$this->getController()->redirect(['/cron/view', 'id' => $record->id]);
					return;
				}
				Yii::log("Erreur en enregistrant la nouvelle tâche " . print_r($record->getErrors(), true), 'error');
				Yii::app()->user->setFlash('error', "Erreur en enregistrant la nouvelle tâche dans le cron.");
			} else {
				$widget->state = $formData->getAttributes();
				$widget->errors = $formData->getErrors();
			}
		}

		$this->getController()->render(
			'update',
			[
				'task' => $record,
				'widget' => $widget,
			]
		);
	}
}
