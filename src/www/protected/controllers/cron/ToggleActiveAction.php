<?php

namespace controllers\cron;

use processes\cron\ar\Task;

class ToggleActiveAction extends \CAction
{
	public function run(string $id)
	{
		\Controller::header('json');
		if (!\Yii::app()->getRequest()->isPostRequest) {
			throw new \CHttpException(405);
		}
		$record = \processes\cron\ar\Task::model()->findByPk((int) $id);
		if (!($record instanceof Task)) {
			throw new \CHttpException(404);
		}
		$record->active = !$record->active;
		$record->save(false);
		echo json_encode(['active' => $record->active]);
	}
}
