<?php

use components\HtmlHelper;

class UtilisateurController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	private $model;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'actions' => ['reset-password'],
				'users' => ['*'],
			],
			[
				'allow',
				'actions' => ['delete', 'politiques-editeurs', 'remove-editeur', 'update', 'view'],
				'users' => ['@'],
			],
			[
				'allow',
				'actions' => ['create', 'message-password', 'suggest-login'],
				'roles' => ['avec-partenaire'],
			],
			[
				'allow',
				'actions' => ['admin', 'create-multi', 'delete-totally'],
				'roles' => ['utilisateur:admin'],
			],
			['deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	/**
	 * Maps actions to dedicated classes.
	 */
	public function actions()
	{
		return [
			'admin' => 'controllers\utilisateur\Admin',
			'create' => 'controllers\utilisateur\Create',
			'create-multi' => 'controllers\utilisateur\CreateMulti',
			'delete' => 'controllers\utilisateur\Delete',
			'delete-totally' => 'controllers\utilisateur\DeleteTotally',
			'message-password' => 'controllers\utilisateur\MessagePassword',
			'politiques-editeurs' => 'controllers\utilisateur\PolitiquesEditeurs',
			'remove-editeur' => 'controllers\utilisateur\RemoveEditeur',
			'reset-password' => 'controllers\utilisateur\ResetPassword',
			'update' => 'controllers\utilisateur\Update',
			'suggest-login' => 'controllers\utilisateur\SuggestLogin',
			'view' => 'controllers\utilisateur\View',
		];
	}

	public function beforeRender($view)
	{
		parent::beforeRender($view);
		$isAdmin = Yii::app()->user->access()->toUtilisateur()->admin();
		if (isset($_GET['id']) && $_GET['id'] > 0) {
			$id = (int) $_GET['id'];
			$model = $this->loadModel($id);
		} else {
			$model = null;
		}
		$this->breadcrumbs = $this->createBreadrumbs($model, $isAdmin);
		$sidebar = \Yii::app()->getComponent('sidebar');
		assert($sidebar instanceof \components\WebSidebar);
		$sidebar->menu = $this->createMenu($model, $isAdmin);
		return true;
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 *
	 * If the data model is not found, an HTTP exception will be raised.
	 *
	 * @param int|string $id the ID of the model to be loaded
	 * @return Utilisateur
	 */
	public function loadModel($id)
	{
		if (!$this->model) {
			$this->model = Utilisateur::model()->findByPk((int) $id);
		}
		if ($this->model === null) {
			throw new CHttpException(404, "Le compte utilisateur demandé n'existe pas.");
		}
		return $this->model;
	}

	public function warnOnRemovedPolicyRights(array $policyRoles): void
	{
		if (!$policyRoles) {
			return;
		}
		$names = join(
			" ",
			array_map(
				function ($x) {
					return CHtml::encode($x['editeur']->nom);
				},
				$policyRoles
			)
		);
		Yii::app()->user->setFlash(
			'warning',
			"Les droits de cet utilisateur pour déclarer des politiques d'éditeurs ont été supprimés : $names."
			. " Veillez à les réattribuer si besoin."
		);
	}

	private function createBreadrumbs(?Utilisateur $model, bool $isAdmin): array
	{
		$breadcrumbs = ($isAdmin ? ['Utilisateurs' => ['admin']] : []);
		if ($model) {
			if ($model->partenaireId > 0) {
				$breadcrumbs['Partenaire ' . $model->partenaire->nom] = ['/partenaire/view', 'id' => $model->partenaireId];
			}
			$breadcrumbs[$model->login] = ['view', 'id' => $model->id];
		}
		return $breadcrumbs;
	}

	private function createMenu(?Utilisateur $model, bool $isAdmin): array
	{
		if (Yii::app()->user->isGuest) {
			return [];
		}
		$menu = [
			[
				'label' => 'Administrer',
				'url' => ['admin'],
				'visible' => $isAdmin,
			],
			[
				'label' => 'Créer un utilisateur',
				'url' => ['create'],
				'visible' => $isAdmin,
			],
			[
				'label' => 'Création par lot',
				'url' => ['create-multi'],
				'visible' => $isAdmin && Yii::app()->params->itemAt('utilisateurs-par-lot'),
			],
		];
		if ($model) {
			$isMe = ($model->id == Yii::app()->user->id);
			$isUpdateAllowed = Yii::app()->user->access()->toUtilisateur()->update($model);
			$hasPolitiqueValidate = Yii::app()->user->access()->toPolitique()->validate();
			array_push(
				$menu,
				[
					'label' => 'Créer un utilisateur',
					'url' => ['create', 'partenaireId' => Yii::app()->user->getState('partenaireId', 0)],
					'visible' => !$isAdmin && isset($model->partenaireId) && Yii::app()->user->access()->toUtilisateur()->create($model->partenaire),
				],
				[
					'label' => ($isMe ? 'Mon profil' : 'Cet utilisateur'),
					'itemOptions' => ['class' => 'nav-header'],
				],
				[
					'label' => 'Consulter',
					'url' => ['view', 'id' => $model->id],
				],
				[
					'label' => 'Modifier',
					'url' => ['update', 'id' => $model->id],
					'itemOptions' => ['title' => ($isMe ? "Modifier mon adresse électronique ou mon mot de passe" : "Modifier ce compte")],
					'visible' => $isUpdateAllowed,
				],
				[
					'label' => 'Politiques',
					'url' => ['politiques-editeurs', 'id' => $model->id],
					'itemOptions' => [
						'title' => ($hasPolitiqueValidate ? "Attribuer" : "Demander")
							. " des responsabilités de politiques d'éditeurs sur leurs titres",
					],
					'visible' => $hasPolitiqueValidate,
				],
				[
					'label' => 'Envoyer un mot de passe',
					'url' => ['message-password', 'id' => $model->id],
					'visible' => $isUpdateAllowed && !$isMe,
				],
				[
					'label' => HtmlHelper::postButton(
						"Désactiver",
						['/utilisateur/delete', 'id' => $model->id],
						[],
						['class' => "btn btn-link", 'style' => "padding: 0", 'onclick' => 'return confirm("Êtes vous certain de vouloir désactiver cet utilisateur ?")']
					),
					'encodeLabel' => false,
					'visible' => Yii::app()->user->access()->toUtilisateur()->disable($model),
				],
				[
					'label' => HtmlHelper::postButton(
						"Supprimer",
						['/utilisateur/delete-totally', 'id' => $model->id],
						[],
						['class' => "btn btn-link", 'style' => "padding: 0", 'onclick' => 'return confirm("Êtes vous certain de vouloir supprimer DÉFINITIVEMENT cet utilisateur ?")']
					),
					'encodeLabel' => false,
					'visible' => Yii::app()->user->access()->toUtilisateur()->delete($model),
				]
			);
		}
		return $menu;
	}
}
