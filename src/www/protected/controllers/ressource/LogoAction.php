<?php

namespace controllers\ressource;

use CUploadedFile;
use processes\images\Downloader as ImageDownloader;
use Upload;
use processes\upload\UploadDestination;
use Yii;

class LogoAction extends \CAction
{
	public function run(string $id)
	{
		$model = \RessourceController::loadModel($id);

		$imageTool = new ImageDownloader(
			path: DATA_DIR . '/images/ressources',
			pathRaw: DATA_DIR . '/public/images/ressources'
		);
		$imageTool->filePattern = '%06d';

		// remove the logo
		if (!empty($_POST['delete'])) {
			$imageTool->delete($model->id);
			$model->logoUrl = '';
			$model->save(false);
			Yii::app()->user->setFlash('success', "Le logo de la ressource a été mis à jour.");
			Yii::app()->session->add('force-refresh', true);
			$this->getController()->redirect(['ressource/view', 'id' => $model->id]);
			return;
		}

		$newImage = false;
		$filename = $imageTool->getRawImageFileName($model->id);
		$upload = new Upload(UploadDestination::logoRessource());
		$upload->overwrite = true;

		// logo by URL
		if (!empty($_POST['Ressource'])) {
			$logoUrl = $_POST['Ressource']['logoUrl'];
			if ($logoUrl) {
				try {
					if ($imageTool->download($model->id, $logoUrl)) {
						$model->logoUrl = $logoUrl;
						$model->save(false);
						$newImage = true;
					}
				} catch (\Exception $e) {
					Yii::app()->user->setFlash('error', $e->getMessage());
					$this->getController()->render(
						'logo',
						['model' => $model, 'upload' => $upload]
					);
					return;
				}
			}
		}

		// logo by file upload
		if (!empty($_POST['Upload'])) {
			$upload->attributes = $_POST['Upload'];
			$upload->file = CUploadedFile::getInstance($upload, 'file');
			if ($upload->file) {
				if ($upload->file->saveAs($filename)) {
					system("convert \"$filename\" \"$filename.png\"");
					unlink($filename);
					$newImage = true;
					// M#3422 remove the logoUrl when a upload superseeds it
					$model->logoUrl = '';
					$model->save(false);
				} else {
					Yii::app()->user->setFlash('error', "Erreur lors de la copie de ce fichier sur le serveur.");
				}
			} else {
				Yii::app()->user->setFlash('error', "Erreur lors du dépôt de ce fichier.");
			}
		}

		if ($newImage) {
			if (self::resize((int) $model->id)) {
				Yii::app()->user->setFlash('success', "Le logo de la ressource a été mis à jour.");
				Yii::app()->session->add('force-refresh', true);
				$this->getController()->redirect(['ressource/view', 'id' => $model->id]);
				return;
			}
			Yii::app()->user->setFlash('error', "Erreur lors du redimensionnement de l'image.");
		}

		$this->getController()->render(
			'logo',
			['model' => $model, 'upload' => $upload]
		);
	}

	private static function resize(int $identifier): bool
	{
		$resizer = new \processes\images\Resizer(
			finalPath: DATA_DIR . '/images/ressources',
			rawPath: DATA_DIR . '/images/ressources'
		);
		$resizer->filePattern = '%06d';
		$resizer->boundingbox = '450x110>';
		return $resizer->resizeByIdentifier($identifier);
	}
}
