<?php

namespace controllers\ressource;

use processes\completion\Ressource;

class AjaxCompleteAction extends \CAction
{
	public function run(string $term, string $noimport = '')
	{
		\Controller::header('json');
		echo json_encode((new Ressource)->completeTerm($term, [], (bool) $noimport));
	}
}
