<?php

namespace controllers\ressource;

class CheckLinksAction extends \CAction
{
	public function run(string $id)
	{
		$controller = $this->getController();
		\assert($controller instanceof \RessourceController);

		$model = \RessourceController::loadModel($id);
		$controller->render(
			'checkLinks',
			[
				'ressource' => $model,
				'checks' => (new \processes\urlverif\UrlExtractor)->checkObjectLinks($model),
			]
		);
	}
}
