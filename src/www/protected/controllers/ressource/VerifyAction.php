<?php

namespace controllers\ressource;

use CHttpException;
use Yii;

class VerifyAction extends \CAction
{
	public function run(string $id)
	{
		$model = \RessourceController::loadModel($id);
		if (!Yii::app()->user->checkAccess('verify', $model)) {
			throw new CHttpException(403, "Vous n'avez pas les droits nécessaires pour cette opération.");
		}
		$model->hdateVerif = $_SERVER['REQUEST_TIME'];
		if ($model->save()) {
			Yii::app()->user->setFlash('success', "La date de vérification a été mise à jour.");
		}
		$this->getController()->redirect(['view', 'id' => $model->id]);
	}
}
