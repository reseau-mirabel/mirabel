<?php

namespace controllers\ressource;

use CHttpException;
use components\paginationalpha\ListViewAlpha;
use components\sphinx\Criteria;
use components\sphinx\DataProvider;
use components\SqlHelper;
use CSort;
use models\sphinx\Ressources;
use processes\ressource\ListFilter;
use SearchNavigation;

class IndexAction extends \CAction
{
	public static $pageSize = 50;

	public function run(): void
	{
		$filter = ListFilter::load($_GET);
		if ($filter->lettre !== '') {
			$listView = self::createListView($filter);
		} else {
			$listView = null;
		}

		$dataProvider = self::createDataProvider($filter);
		try {
			$data = $dataProvider->getData();
		} catch (\Throwable $e) {
			if (isset($_GET['page']) && $_GET['page'] > 20) {
				throw new CHttpException(400, "Erreur dans la recherche de ressources. Navigation au-delà de la taille des résultats ?");
			}
			\Yii::log($e->getMessage() . $e->getTraceAsString(), 'error');
			throw new CHttpException(500, "Erreur dans la recherche de ressources. Elle a été signalée, nous ferons de notre mieux pour la corriger.");
		}
		if ($listView !== null) {
			$listView->dataProvider = $dataProvider;
		}

		$searchCache = new SearchNavigation();
		$searchHash = $searchCache->save(
			'ressource/index',
			['lettre' => $filter->lettre, 'suivi' => $filter->suivi, 'import' => $filter->import, 'noweb' => $filter->noweb,
				'exhaustif' => $filter->exhaustif, 'exhaustifcollections' => $filter->exhaustifcollections],
			'id',
			$data,
			$dataProvider->getPagination() ?: null
		);

		$this->getController()->render(
			'index',
			[
				'dataProvider' => $dataProvider,
				'filter' => $filter,
				'listView' => $listView,
				'searchHash' => $searchHash,
			]
		);
	}

	private static function createDataProvider(ListFilter $filter): DataProvider
	{
		if ($filter->suivi || $filter->import || $filter->exhaustifcollections || $filter->exhaustif) {
			$pagination = false;
		} else {
			$pagination = [
				'pageSize' => self::$pageSize,
				'params' => ['lettre' => $filter->lettre],
			];
		}

		$criteria = new Criteria();
		if ($filter->suivi > 0) {
			$criteria->addCondition("suivi = {$filter->suivi}");
		} elseif ($filter->suivi < 0) {
			$criteria->addCondition("suivi > 0");
		}
		if ($filter->exhaustif) {
			$criteria->addCondition("exhaustif = 1");
		} elseif ($filter->exhaustifcollections) {
			$criteria->addCondition("exhaustifcollections = 1");
		}
		if ($filter->import === 'ressource') {
			$criteria->addCondition("autoimport = 1");
		} elseif ($filter->import === 'collection') {
			$criteria->addCondition("autoimportcollections = 1");
		}
		if ($pagination !== false) {
			$criteria->addCondition("lettre1 = {$filter->letterRank}");
		}
		if ($filter->noweb) {
			if (!$filter->suivi) {
				$criteria->addCondition("web = 0");
			}
			if ($pagination) {
				$pagination['params']['noweb'] = 1;
			}
		}

		$sort = new CSort(Ressources::class);
		$sort->defaultOrder = 'cletri ASC';

		return new DataProvider(
			Ressources::model(),
			[
				'criteria' => $criteria,
				'pagination' => $pagination,
				'sort' => $sort,
			]
		);
	}

	private static function createListView(ListFilter $filter): ListViewAlpha
	{
		$listView = new ListViewAlpha();
		// List the initial letters where there is at least one result
		/**
		 * @todo Replace this SQL with a count-grouped query on Sphinx built from a clone of the search criteria.
		 */
		$extraSql = [];
		if ($filter->noweb) {
			$extraSql[] = "r.type <> 'SiteWeb'";
		}
		if ($filter->exhaustif) {
			$extraSql[] = "r.exhaustif = 1";
		}
		$where = "";
		if ($extraSql) {
			$where = "WHERE " . join(" AND ", $extraSql);
		}
		$listView->setCountByLetter(
			SqlHelper::sqlToPairs(<<<EOSQL
				SELECT
					FIND_IN_SET(LEFT(r.nom, 1), 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z') AS rank,
					COUNT(DISTINCT r.id) AS num
				FROM Ressource r
				$where
				GROUP BY rank
				EOSQL
			)
		);
		$listView->setLetterTitleTemplate("{count} ressources");
		$listView->getLetterPagination()->setCurrentPage($filter->lettre);

		// Update the filter by validating the letter,  computing its rank, incorporating filters.
		$letterPagination = $listView->getLetterPagination();
		$filter->letterRank = $letterPagination->getCurrentPage();
		$filter->lettre = $letterPagination->getPageLetter($filter->letterRank);
		if ($filter->import) {
			$letterPagination->params['import'] = $filter->import;
		}
		if ($filter->noweb) {
			$letterPagination->params['noweb'] = 1;
		}
		if ($filter->suivi) {
			$letterPagination->params['suivi'] = $filter->suivi;
		}

		return $listView;
	}
}
