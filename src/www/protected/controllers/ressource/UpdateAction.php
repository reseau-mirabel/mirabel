<?php

namespace controllers\ressource;

use RessourceController;
use Yii;

class UpdateAction extends \CAction
{
	public function run(string $id)
	{
		$controller = $this->getController();
		assert($controller instanceof \Controller);

		$model = RessourceController::loadModel($id);
		$directAccess = Yii::app()->user->checkAccess('ressource/update-direct', ['Ressource' => $model])
			&& empty($_POST['force_proposition']);
		$i = $model->buildIntervention($directAccess);

		if (isset($_POST['Ressource'])) {
			$old = clone $model;
			$model->setAttributes($_POST['Ressource']);
			if (isset($_POST['Intervention']['email'])) {
				$i->setAttributes($_POST['Intervention']);
			}
			$changedAttributes = array_keys(array_diff_assoc($model->getAttributes(), $old->getAttributes()));
			if ($model->validate($changedAttributes)) {
				$i->description = "Modification de la ressource « {$model->nom} »";
				$i->contenuJson->update($old, $model);
				unset($old);
				if ($controller->validateIntervention($i, $directAccess)) {
					$controller->redirect(['view', 'id' => $model->id]);
				}
			}
		}

		$controller->render(
			'update',
			['model' => $model, 'direct' => $directAccess, 'intervention' => $i]
		);
	}
}
