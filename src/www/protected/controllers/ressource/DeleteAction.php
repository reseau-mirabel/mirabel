<?php

namespace controllers\ressource;

use CHttpException;
use components\FeedGenerator;
use Yii;

class DeleteAction extends \CAction
{
	public function run(string $id)
	{
		$ressource = \RessourceController::loadModel($id);

		$hasIntervention = (bool) Yii::app()->db
			->createCommand("SELECT 1 FROM Intervention WHERE ressourceId = :rid AND statut = 'attente'")
			->queryScalar([':rid' => (int) $id]);
		if ($hasIntervention) {
			Yii::app()->user->setFlash('error', "Suppression impossible car au moins une intervention est en attente sur cette ressource.");
			$this->getController()->redirect(['view', 'id' => $ressource->id]);
			return;
		}

		if (!Yii::app()->request->isPostRequest) {
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}

		[$deletable, $deletableMsg] = $ressource->isDeletable();
		if (!$deletable && !Yii::app()->user->checkAccess('ressource/delete-force')) {
			Yii::app()->user->setFlash('error', $deletableMsg);
			$this->getController()->redirect(['view', 'id' => $ressource->id]);
			return;
		}

		if (!empty($_POST['confirm'])) {
			foreach ($ressource->services as $s) {
				$si = $s->buildIntervention(true);
				$si->action = 'service-D';
				$si->description = "Suppression d'un accès en ligne, revue « {$s->titre->titre} »"
					. " / « {$ressource->nom} »";
				$si->contenuJson->delete($s, "Suppression de l'accès en ligne");
				$si->accept();
			}
			$i = $ressource->buildIntervention(true);
			$i->description = "Suppression de la ressource « {$ressource->nom} »";
			$i->action = 'ressource-D';
			foreach ($ressource->collections as $c) {
				$i->contenuJson->delete($c, "Suppression de la collection « " . $c->nom . " »");
			}
			foreach ($ressource->identifications as $ident) {
				$i->contenuJson->delete($ident, "Suppression de l'identification entre « " . $ressource->nom . " » et « " . $ident->titre->titre . " »");
			}
			$i->contenuJson->delete($ressource, "Suppression de la ressource « " . $ressource->nom . " »");
			if ($i->validate()) {
				FeedGenerator::saveFeedsToFile('Ressource', (int) $ressource->id);
			}
			if (!$i->accept(false)) {
				Yii::app()->user->setFlash('error', print_r($i->errors, true));
			} else {
				$i->ressourceId = null;
				if (!$i->save(false)) {
					throw new CHttpException(500, "Erreur : " . print_r($i->errors, true));
				}
				Yii::app()->user->setFlash('success', "La ressource a été supprimée.");
				$this->getController()->redirect($_POST['returnUrl'] ?? ['index']);
				return;
			}
		}
		$this->getController()->render(
			'delete',
			[
				'model' => $ressource,
				'deletable' => $deletable,
				'msg' => $deletableMsg,
			]
		);
	}
}
