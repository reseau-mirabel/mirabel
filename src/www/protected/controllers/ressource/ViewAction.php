<?php

namespace controllers\ressource;

use AbonnementSearch;
use Controller;
use Partenaire;
use Ressource;
use Yii;

class ViewAction extends \CAction
{
	public const MAX_ACTUS = 3;

	public function run(string $id, string $nom = '')
	{
		$model = \RessourceController::loadModel($id);
		$controller = $this->getController();
		assert($controller instanceof \Controller);

		if ($this->needsRedirection($model, $nom)) {
			return;
		}

		$controller->setCanonicalUrl($model->getSelfUrl());
		$controller->appendToHtmlHead(
			Controller::linkEmbeddedFeed('rss2', "Ressource " . $model->nom, ['ressource' => $model->id])
		);

		$abonnements = null;
		if (Yii::app()->user->getState('instituteId') > 0) {
			$abonnements = new AbonnementSearch();
			$abonnements->partenaireId = Yii::app()->user->getState('instituteId');
		}
		$partenaireId = Yii::app()->user->getState('partenaireId');

		if (Yii::app()->session->contains('force-refresh')) {
			Yii::app()->session->remove('force-refresh');
			$forceRefresh = true;
		} else {
			$forceRefresh = false;
		}

		$controller->render(
			'view',
			[
				'model' => $model,
				'searchNavigation' => $controller->loadSearchNavigation(),
				'abonnements' => $abonnements,
				'partenaireCourant' => ($partenaireId ? Partenaire::model()->findByPk($partenaireId) : null),
				'forceRefresh' => $forceRefresh,
				'actus' => $this->getActus($model->id),
			]
		);
	}

	private function needsRedirection(Ressource $ressource, string $nom): bool
	{
		$selfUrl = $ressource->getSelfUrl();
		$expectedName = $selfUrl['nom'] ?? '';
		if ($expectedName && (!$nom || $nom !== $expectedName)) {
			$searchNav = $_GET['s'] ?? '';
			if ($searchNav) {
				$selfUrl['s'] = $searchNav;
			}
			$this->getController()->redirect($selfUrl);
			return true;
		}
		return false;
	}

	private function getActus(int $id): array
	{
		$actu = new \models\cms\BreveSearchForm();
		$actu->ressourceId = $id;
		$actuProvider = $actu->search();
		$actuProvider->getCriteria()->limit = self::MAX_ACTUS + 1;
		return $actuProvider->getData();
	}
}
