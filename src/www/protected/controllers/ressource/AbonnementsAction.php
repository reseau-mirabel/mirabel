<?php

namespace controllers\ressource;

class AbonnementsAction extends \CAction
{
	public function run(string $id)
	{
		$model = \RessourceController::loadModel($id);
		if ($model->hasCollections()) {
			$cibles = $model->getCollectionsDiffuseur();
		} else {
			$cibles = [$model];
		}

		$this->getController()->render(
			'abonnements',
			[
				'model' => $model,
				'cibles' => $cibles,
			]
		);
	}
}
