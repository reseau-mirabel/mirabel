<?php

namespace controllers\ressource;

use Intervention;
use Ressource;
use Yii;

class CreateAction extends \CAction
{
	public function run(): void
	{
		$controller = $this->getController();
		assert($controller instanceof \Controller);

		$model = new Ressource();
		$directAccess = Yii::app()->user->checkAccess('ressource/create-direct');

		if (isset($_POST['Ressource'])) {
			$i = $model->buildIntervention($directAccess);
			if (isset($_POST['Intervention']['email'])) {
				$i->setAttributes($_POST['Intervention']);
			}
			$model->setAttributes($_POST['Ressource']);
			if ($model->validate()) {
				$i->description = "Création de la ressource « {$model->nom} »";
				$i->action = 'ressource-C';
				$i->contenuJson->create($model);
				if ($controller->validateIntervention($i, $directAccess)) {
					if ($directAccess) {
						$controller->redirect(['view', 'id' => $i->ressourceId]);
					} else {
						$controller->redirect(['index']);
					}
					return;
				}
			}
		}
		if (!isset($i)) {
			$i = new Intervention;
		}

		$controller->render(
			'create',
			['model' => $model, 'direct' => $directAccess, 'intervention' => $i]
		);
	}
}
