<?php

class CronController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl',
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'roles' => ['admin'],
			],
			[
				'deny',  // otherwise deny all users
				'users' => ['*'],
			],
		];
	}

	/**
	 * Maps actions to dedicated classes.
	 */
	public function actions()
	{
		return [
			'create' => \controllers\cron\CreateAction::class,
			'delete' => \controllers\cron\DeleteAction::class,
			'force-run' => \controllers\cron\ForceRunAction::class,
			'index' => \controllers\cron\IndexAction::class,
			'index-calendar' => \controllers\cron\IndexCalendarAction::class,
			'toggle-active' => \controllers\cron\ToggleActiveAction::class,
			'update' => \controllers\cron\UpdateAction::class,
			'view' => \controllers\cron\ViewAction::class,
		];
	}

	public function beforeAction($action)
	{
		if (!Yii::app()->user->checkAccess('tester')) {
			throw new \CHttpException(403, "La fonctionnalité de cron-web est expérimentale, donc son accès est temporairement restreint.");
		}
		return parent::beforeAction($action);
	}
}
