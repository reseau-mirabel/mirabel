<?php

class RessourceController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	public $defaultAction = 'index';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'actions' => ['ajax-complete', 'create', 'index', 'update', 'view'],
				'users' => ['*'],
			],
			[
				'allow',
				'actions' => ['logo'],
				'roles' => ['ressource/logo'],
			],
			[
				'allow',
				'actions' => ['abonnements', 'checkLinks', 'delete', 'verify'],
				'roles' => ['avec-partenaire'],
			],
			[
				'deny',  // otherwise, deny all users
				'users' => ['*'],
			],
		];
	}

	/**
	 * Maps actions to dedicated classes.
	 */
	public function actions()
	{
		return [
			'abonnements' => 'controllers\ressource\AbonnementsAction',
			'ajax-complete' => 'controllers\ressource\AjaxCompleteAction',
			'checkLinks' => 'controllers\ressource\CheckLinksAction',
			'create' => 'controllers\ressource\CreateAction',
			'delete' => 'controllers\ressource\DeleteAction',
			'index' => 'controllers\ressource\IndexAction',
			'logo' => 'controllers\ressource\LogoAction',
			'update' => 'controllers\ressource\UpdateAction',
			'verify' => 'controllers\ressource\VerifyAction',
			'view' => 'controllers\ressource\ViewAction',
		];
	}

	public static function loadModel(string $id): Ressource
	{
		$model = Ressource::model()->findByPk((int) $id);
		if ($model === null) {
			throw new CHttpException(404, "La ressource demandée n'existe pas.");
		}
		$model->id = (int) $model->id;
		return $model;
	}
}
