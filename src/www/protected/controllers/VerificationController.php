<?php

use processes\verification\Couvertures;
use processes\verification\General;
use processes\verification\Issn;
use processes\verification\Ppn;
use processes\verification\Revues;
use processes\verification\Urls;
use processes\verification\TitresEditeurs;
use models\wikidata\CompareContent;

class VerificationController extends Controller
{
	protected $navLinks;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return ['accessControl'];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			['allow', 'roles' => ['avec-partenaire'], ],
			['deny',  'users' => ['*'], ],
		];
	}

	public function actions()
	{
		return [
			'titres-acces-deborde' => \controllers\verification\TitresAccesDebordeAction::class,
		];
	}

	public function actionAttributApc()
	{
		$incoherences = \Yii::app()->db
			->createCommand(<<<SQL
				WITH Apc AS (
				    SELECT
						at.titreId,
						json_arrayagg(json_object('id', sa.identifiant, 'nom', sa.nom, 'valeur', at.valeur)) AS attrValues,
						MAX(sa.identifiant LIKE 'apc-avec-%') AS avec,
						MAX(sa.identifiant LIKE 'apc-sans-%') AS sans
				    FROM Sourceattribut sa
				        JOIN AttributTitre at ON sa.id = at.sourceattributId
				    WHERE sa.identifiant LIKE 'apc-%'
					GROUP BY at.titreId
				)
				SELECT t.id, t.revueId, t.titre, t.sigle, Apc.attrValues
				FROM Apc
				    JOIN Titre t ON Apc.titreId = t.id
				WHERE avec > 0 AND sans > 0
				ORDER BY t.titre
				SQL
			)
			->queryAll();
		$this->render(
			'attribut-apc',
			['incoherences' => $incoherences]
		);
	}

	public function actionCouvertures()
	{
		$this->render(
			'couvertures',
			['verif' => new Couvertures()]
		);
	}

	public function actionData()
	{
		$this->render(
			'data',
			['verif' => new General()]
		);
	}

	public function actionRevues()
	{
		$this->render(
			'revues',
			['verif' => new Revues()]
		);
	}

	public function actionEditeurs()
	{
		$this->render(
			'editeurs',
			['verif' => new processes\verification\Editeurs()]
		);
	}

	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionIssn()
	{
		$this->render(
			'issn',
			['verif' => new Issn()]
		);
	}

	public function actionPpn()
	{
		$this->render(
			'ppn',
			['ppn' => new Ppn()],
		);
	}

	public function actionTitresEditeurs()
	{
		$this->render(
			'titres-editeurs',
			[
				'process' => new TitresEditeurs(),
			]
		);
	}

	public function actionLiens()
	{
		$this->render(
			'liens',
			['model' => new \processes\verification\Liens()]
		);
	}

	public function actionLiensCms()
	{
		$process = new Urls('cms');
		$process->load([]);
		$process->dates['dateMajMax'] = null;
		$this->render(
			'liens-cms',
			[
				'process' => $process,
				'verifs' => $process->getSearchCmd()->order('sourceId, id')->queryAll(),
			]
		);
	}

	public function actionLiensRevues()
	{
		$process = new Urls('revue');
		$process->load($_POST['VerifUrlForm'] ?? $_GET['VerifUrlForm'] ?? []);
		$this->render(
			'liens-revues',
			['process' => $process]
		);
	}

	public function actionLiensEditeurs()
	{
		$process = new Urls('editeur');
		$process->load($_POST['VerifUrlForm'] ?? $_GET['VerifUrlForm'] ?? []);
		$this->render(
			'liens-editeurs',
			['process' => $process]
		);
	}

	public function actionLiensRessources()
	{
		$process = new Urls('ressource');
		$process->load($_POST['VerifUrlForm'] ?? $_GET['VerifUrlForm'] ?? []);
		$this->render(
			'liens-ressources',
			['process' => $process]
		);
	}

	public function actionThematique($minImports = 5)
	{
		$this->render(
			'thematique',
			[
				'minImports' => (int) $minImports,
				'revues' => Yii::app()->db->createCommand(
					<<<EOSQL
						SELECT t.revueId AS id, t.titre, COUNT(DISTINCT ca.categorieId) AS importees
						FROM Titre t
						  LEFT JOIN Categorie_Revue cr ON cr.revueId = t.revueId
						 JOIN CategorieAlias_Titre ct ON ct.titreId = t.id
						  JOIN CategorieAlias ca ON ca.id = ct.categorieAliasId
						WHERE cr.categorieId IS NULL
						GROUP BY t.revueId
						  HAVING importees >= :min
						ORDER BY importees DESC
						EOSQL
				)->queryAll(true, [':min' => (int) $minImports]),
			]
		);
	}

	/**
	 * Manages Wikidata-cache records (index mainly)
	 */
	public function actionWikidata()
	{
		$model = new Wikidata('search');
		$model->unsetAttributes();  // clear any default values
		$model->setAttributes($_GET['Wikidata'] ?? []);

		$this->render('wikidata', [
			'model' => $model,
		]);
	}

	public function actionWikidataCompare()
	{
       $WComp = new CompareContent(0);
       $WComp->compareProperties(true);
	   Yii::app()->user->setFlash('info', 'La comparaison des propriétés Wikidata/Mir@bel a été relancée.');
	   $this->redirect(['/verification/wikidata']);
	}

	/**
	 * Manages Wikidata bugs and inconsistencies
	 */
	public function actionWikidatabugs(bool $export=false)
	{
		$model = new Wikidata('search');
		$model->unsetAttributes();  // clear any default values
		$model->setAttributes($_GET['Wikidata'] ?? []);

		if ($export) {
			\Controller::header("csv");
			\Controller::header("nocache");
			$name = \Norm::urlParam(\Yii::app()->name);
			header(
				sprintf('Content-Disposition: attachment; filename="%s_wikidatabugs_%s.csv"', $name, date('Y-m-d'))
			);
			$dp = $model->searchInconsistencies();
			$dp->setPagination(false);
			$columns = [
				'revueId',
				[
					'header' => 'Revue',
					'value' => function (\Wikidata $data) {
						return $data->revue->getNom();
					},
				],
				[
					'header' => 'Url Revue',
					'value' => function (\Wikidata $data) {
						return Yii::app()->getBaseUrl(true) . Yii::app()->createUrl('/revue/view', ['id' => $data->revueId]);
					},
				],
				'qId',
				[
					'header' => 'Url QID',
					'value' => function (\Wikidata $data) {
						return $data->getQidLink(false);
					},
				],
				[
					'header' => 'ISSN',
					'name' => 'value',
				],
				'comparison',
				'comparisonText',
				'compDetails',
			];
			$exporter = new \components\DataProviderExporter($dp, $columns);
			$exporter->writeCsv(fopen('php://output', 'w'));
			return;
		}

		$this->render('wikidatabugs', [
			'model' => $model,
		]);
	}
}
