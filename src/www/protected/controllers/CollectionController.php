<?php

class CollectionController extends Controller
{
	/**
	 * @var string the default layout for the views. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'actions' => ['ajax-list'],
				'users' => ['*'],
			],
			[
				'allow',
				'roles' => ['avec-partenaire'],
			],
			[
				'deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	/**
	 * Maps actions to dedicated classes.
	 */
	public function actions()
	{
		return [
			'ajax-complete' => 'controllers\collection\AjaxCompleteAction',
			'ajax-list' => 'controllers\collection\AjaxListAction',
			'create' => 'controllers\collection\CreateAction',
			'create-tmp' => 'controllers\collection\CreateTmpAction',
			'delete' => 'controllers\collection\DeleteAction',
			'index' => 'controllers\collection\IndexAction',
			'update' => 'controllers\collection\UpdateAction',
			'view' => 'controllers\collection\ViewAction',
		];
	}

	public static function loadModel(string $id): Collection
	{
		$model = Collection::model()->findByPk((int) $id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		$model->id = (int) $model->id;
		return $model;
	}
}
