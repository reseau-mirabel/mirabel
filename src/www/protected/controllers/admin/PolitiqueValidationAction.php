<?php

namespace controllers\admin;

use Controller;
use processes\admin\PolitiqueValidation;

class PolitiqueValidationAction extends \CAction
{
	public function run(): void
	{
		if (isset($_POST['action'])) {
			Controller::header('json');
			$uid = (int) $_POST['utilisateurId'];
			$eid = (int) $_POST['editeurId'];
			try {
				$result = self::apply((string) $_POST['action'], $uid, $eid);
			} catch (\Throwable $e) {
				$result = ['message' => $e->getMessage()];
				http_response_code(500);
			} finally {
				echo json_encode($result, JSON_UNESCAPED_UNICODE);
			}
			return;
		}
		$this->getController()->render(
			'politique-validation',
			['model' => new PolitiqueValidation()]
		);
	}

	private static function apply(string $action, int $uid, int $eid): array
	{
		switch ($action) {
			case 'delete':
				return PolitiqueValidation::deleteRole($uid, $eid);
			case 'guest':
				return PolitiqueValidation::setRole('guest', $uid, $eid);
			case 'owner':
				return PolitiqueValidation::setRole('owner', $uid, $eid);
			default:
				http_response_code(404);
				return ['message' => "Action inconnue."];
		}
	}
}
