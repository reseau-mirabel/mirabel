<?php

namespace controllers\admin;

class IndexAction extends \CAction
{
	public function run(): void
	{
		$this->getController()->render('index');
	}
}
