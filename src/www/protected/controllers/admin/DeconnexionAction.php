<?php

namespace controllers\admin;

use Config;
use Yii;

class DeconnexionAction extends \CAction
{
	public function run(): void
	{
		if (PHP_SAPI !== 'cli') {
			$dir = Yii::app()->session->getSavePath();
			$files = array_diff(glob("$dir/sess_*"), [self::getSessionFile()]);
			foreach ($files as $file) {
				unlink($file);
			}
			Yii::app()->user->setFlash('info', "Les autres sessions ont été supprimées.");
		}
		Config::writeRaw('maintenance.authentification.inactive', '1');
		$this->getController()->redirect(['index']);
	}

	private static function getSessionFile(): string
	{
		return sprintf(
			'%s/sess_%s',
			Yii::app()->session->getSavePath(),
			$_COOKIE[session_name()]
		);
	}
}
