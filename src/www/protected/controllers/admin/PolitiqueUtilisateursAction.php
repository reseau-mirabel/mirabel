<?php

namespace controllers\admin;

use models\searches\UtilisateurPolitique;

class PolitiqueUtilisateursAction extends \CAction
{
	public function run(): void
	{
		$model = new UtilisateurPolitique();
		if (isset($_GET['q'])) {
			$model->setAttributes($_GET['q']);
		}

		$this->getController()->render(
			'politique-utilisateurs',
			['model' => $model]
		);
	}
}
