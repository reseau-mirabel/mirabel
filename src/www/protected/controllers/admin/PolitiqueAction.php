<?php

namespace controllers\admin;

class PolitiqueAction extends \CAction
{
	public function run(): void
	{
		$this->getController()->render(
			'politique',
			['alertes' => new \processes\politique\AlertesSuivi()]
		);
	}
}
