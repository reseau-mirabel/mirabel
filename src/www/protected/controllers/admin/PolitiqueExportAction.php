<?php

namespace controllers\admin;

use Yii;

class PolitiqueExportAction extends \CAction
{
	public function run(): void
	{
		$sql = <<<EOSQL
            SELECT
                Politique_Titre.confirmed,
                FROM_UNIXTIME(Politique_Titre.createdOn) AS createdOn,
                Politique_Titre.titreid,
                Titre.revueId,
                Titre.titre,
                Politique_Titre.politiqueId,
                Politique.name,
                Politique.status,
                Politique.model,
                FROM_UNIXTIME(Politique.lastUpdate) AS lastUpdate,
                Politique.editeurId,
                Editeur.nom
            FROM
                Politique_Titre
                JOIN Politique ON Politique_Titre.politiqueId = Politique.id
                JOIN Titre ON Politique_Titre.titreId = Titre.id
                JOIN Editeur ON Politique.editeurId = Editeur.id
            ORDER BY Editeur.id, Titre.titre
            EOSQL;
		$dataProv = Yii::app()->db->createCommand($sql)->query();

		$csv = self::export($dataProv);
		\Controller::header('nocache');
		\Controller::header('csv');
		header('Content-Disposition: inline; filename=Mirabel_politiques-revues_' . date('Y-m-d') . '.csv');
		echo $csv;
	}

	private static function export(\Iterator $dataProv): string
	{
		$header = [
			'confirmed' => "Confirmée",
			'createdOn' => "Attribuée le",
			'titreid' => "ID de titre",
			'revueId' => "ID de revue",
			'titre' => "Titre",
			'politiqueId' => "ID de politique",
			'name' => "Nom de la politique",
			'status' => "Statut",
			'model' => 'Modèle',
			'lastUpdate' => "Der MàJ",
			'editeurId' => "ID de l'éditeur",
			'nom' => "Nom de l'éditeur",
		];
		$models = [
			"Revue en accès ouvert immédiat sans frais et sans embargo",
			"Revue en accès ouvert immédiat avec frais de type « auteur-payeur »",
			"Revue hybride : accès sur abonnement et frais optionnels OA aux choix des auteurs",
			"Revue sur abonnement avec barrière mobile : accès libre aux articles après un délai",
			"Revue sur abonnement sans accès libre aux numéros nouveaux ou anciens",
		];

		$headerToShow = true;
		$out = fopen('php://memory', 'w');
		foreach ($dataProv as $data) {
			if ($headerToShow) {
				fputcsv($out, array_values($header), ';', '"', '\\');
				$headerToShow = false;
			}
			$data['status'] = \Politique::translateStatus($data['status']);
			if (isset($data['model'])) {
				$data['model'] = $models[(int) $data['model']] ?? '';
			} else {
				$data['model'] = '';
			}
			fputcsv($out, $data, ';', '"', '\\');
		}
		return (string) stream_get_contents($out, -1, 0);
	}
}
