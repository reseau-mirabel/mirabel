<?php

namespace controllers\admin;

use Config;
use Yii;

class FinMaintenanceAction extends \CAction
{
	public function run(): void
	{
		Yii::app()->user->setFlash('info', "Connexion ré-ouverte à tous.");
		Config::writeRaw('maintenance.authentification.inactive', '0');
		$this->getController()->redirect(['index']);
	}
}
