<?php

namespace controllers\attribut;

use CHttpException;
use Sourceattribut;
use Yii;

class DeleteAction extends \CAction
{
	public function run(string $id)
	{
		if (!Yii::app()->user->checkAccess('admin')) {
			throw new CHttpException(403);
		}

		$sattribut = Sourceattribut::model()->findByPk($id);
		if (!($sattribut instanceof Sourceattribut)) {
			throw new \CHttpException(404, "Aucun attribut n'a cet identifiant");
		}

		$protected = $sattribut->isProtected();
		if ($protected) {
			Yii::app()->user->setFlash('warning', "Il est impossible de supprimer un attribut protégé.");
		} else {
			if ($sattribut->delete()) {
				\Yii::app()->user->setFlash('success', "Suppression enregistrée");
			} else {
				\Yii::app()->user->setFlash('error', "Erreur lors de la suppression");
			}
		}
		$this->getController()->redirect(['/attribut/declare']);
	}
}
