<?php

namespace controllers\attribut;

use CHttpException;
use components\SqlHelper;
use Sourceattribut;
use Yii;

class DeclareAction extends \CAction
{
	public function run(): void
	{
		if (!Yii::app()->user->checkAccess('admin')) {
			throw new CHttpException(403);
		}

		if (Yii::app()->request->getIsPostRequest()) {
			$input = Yii::app()->request->getPost('Sourceattribut', []);
			$id = (int) ($input['id'] ?? '');
			unset($input['id']);
			if ($id > 0) {
				$sattribut = Sourceattribut::model()->findByPk($id);
			} else {
				$sattribut = new Sourceattribut();
			}
			if ($sattribut instanceof Sourceattribut) {
				if ($this->applyChanges($sattribut, $input)) {
					$this->getController()->refresh();
					return;
				}
			}
		}

		$sattributs = Sourceattribut::model()->sorted()->findAll();
		$sattributs[] = new Sourceattribut();
		$numValeurs = SqlHelper::sqlToPairs("SELECT sourceattributId, count(*) FROM AttributTitre GROUP BY sourceattributId");
		$this->getController()->render(
			'declare',
			[
				'sattributs' => $sattributs,
				'numValeurs' => $numValeurs,
			]
		);
	}

	/**
	 * @return bool refresh?
	 */
	private function applyChanges(Sourceattribut $sattribut, array $input): bool
	{
		$protected = $sattribut->isProtected();
		$identifiant = $sattribut->identifiant;
		$sattribut->setAttributes($input);
		if ($sattribut->nom !== '' && $sattribut->identifiant !== '') {
			if ($protected && $identifiant && $sattribut->identifiant !== $identifiant) {
				Yii::app()->user->setFlash('warning', "Il est impossible de modifier l'identifiant d'un attribut protégé.");
				return false;
			}
			$sattribut->save();
			Yii::app()->user->setFlash('success', "Modification enregistrée");
			return true;
		}
		Yii::app()->user->setFlash('warning', "Le nom ou l'identifiant ne peut être vide");
		return false;
	}
}
