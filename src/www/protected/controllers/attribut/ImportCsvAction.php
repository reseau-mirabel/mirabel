<?php

namespace controllers\attribut;

use AttributImportLog;
use CHttpException;

/**
 * Construit le CSV contenant toutes les valeurs d'un import d'attribut passé.
 */
class ImportCsvAction extends \CAction
{
	public function run(string $id)
	{
		$log = AttributImportLog::model()->findByPk((int) $id);
		if (!($log instanceof AttributImportLog)) {
			throw new CHttpException(404, "L'historique des imports n'a pas cet enregistrement.");
		}

		$report = \processes\attribut\Report::loadByKey($log->reportKey);
		$attribut = \Sourceattribut::model()->findByPk($report->config->sourceattributId);
		if (!($attribut instanceof \Sourceattribut)) {
			throw new CHttpException(404, "L'attribut mentionné dans le rapport n'existe plus");
		}

		$filename = sprintf(
			'%s_attribut_%s_%s.csv',
			\Norm::urlParam(\Yii::app()->name),
			$attribut->identifiant,
			date('Y-m-d')
		);
		\Controller::header('csv');
		header("Content-Disposition: attachment;filename=\"$filename\"");

		$out = fopen('php://output', 'w');
		fputcsv($out, ["titreId", "valeur"], "\t", '"', '\\');
		foreach ($report->attributTitre as [$k, $v]) {
			fputcsv($out, [$k, $v], "\t", '"', '\\');
		}
	}
}
