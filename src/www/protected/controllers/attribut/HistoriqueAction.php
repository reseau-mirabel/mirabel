<?php

namespace controllers\attribut;

class HistoriqueAction extends \CAction
{
	public function run(string $id = "")
	{
		$attribut = null;
		$criteria = ['condition' => 'logtime > 0', 'order' => "logtime DESC"];
		if ($id > 0) {
			$attribut = \Sourceattribut::model()->findByPk($id);
			$criteria['condition'] .= " AND sourceattributId = {$attribut->id}";
		}
		$this->getController()->render(
			'historique',
			[
				'attribut' => $attribut,
				'logsProvider' => new \CActiveDataProvider(
					'AttributImportLog',
					['criteria' =>  $criteria, 'pagination' => false, ]
				),
			]
		);
	}
}
