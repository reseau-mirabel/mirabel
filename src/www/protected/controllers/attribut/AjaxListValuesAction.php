<?php

namespace controllers\attribut;

use AttributImportLog;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use Box\Spout\Reader\ReaderInterface;
use CAction;
use CFileHelper;
use components\Csv;
use components\FileStore;

class AjaxListValuesAction extends CAction
{
	private const MAX_ROWS = 1000;

	public function run(string $id, string $colNum)
	{
		\Controller::header('json');

		$file = self::getFile((int) $id);
		if (!$file) {
			http_response_code(404);
			echo "[]";
			return;
		}

		echo json_encode(self::readColumn($file, (int) $colNum));
	}

	private function getFile(int $id): string
	{
		$log = AttributImportLog::model()->findByPk((int) $id);
		if (!$log) {
			return '';
		}
		$store = new FileStore(\processes\attribut\FileImport::FILESTORE_ID);
		return $store->get($log->fileKey);
	}

	private static function readColumn(string $file, int $colNum): array
	{
		$reader = self::createReader($file);
		$reader->open($file);

		$values = [];
		$count = 0;
		foreach ($reader->getSheetIterator() as $worksheet) {
			assert($worksheet instanceof \Box\Spout\Reader\SheetInterface);
			foreach ($worksheet->getRowIterator() as $row) {
				assert($row instanceof \Box\Spout\Common\Entity\Row);
				$cell = $row->getCellAtIndex($colNum);
				if ($cell !== null && $count > 0) { // skip header row and missing cells
					$values[(string) $cell->getValueEvenIfError()] = 1;
				}
				$count++;
				if ($count > self::MAX_ROWS) {
					break;
				}
			}
			break; // Only the first worksheet
		}

		$reader->close();
		$distinctValues = array_keys($values);
		sort($distinctValues);
		return $distinctValues;
	}

	private static function createReader(string $file): ReaderInterface
	{
		switch (CFileHelper::getExtensionByMimeType($file)) {
			case 'ods':
				$reader = ReaderEntityFactory::createODSReader();
				$reader->setShouldFormatDates(true);
				break;
			case 'xlsx':
				$reader =  ReaderEntityFactory::createXLSXReader();
				$reader->setShouldFormatDates(true);
				break;
			default:
				$reader = ReaderEntityFactory::createCSVReader();
				$reader->setFieldDelimiter(Csv::guessCsvFieldDelimiter($file));
				break;
		}
		return $reader;
	}
}
