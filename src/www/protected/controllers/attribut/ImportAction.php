<?php

namespace controllers\attribut;

use AttributImportLog;
use CHttpException;
use components\curl\Downloader;
use Exception;
use processes\attribut\FileImport;
use processes\attribut\ImportParsingForm;
use processes\attribut\ImportSourceForm;
use Yii;

/**
 * Importe les valeurs d'un attribut de titres depuis un fichier csv ou ods.
 */
class ImportAction extends \CAction
{
	public function run(string $etape = "0")
	{
		switch ($etape) {
			case "0":
				$this->declareSource();
				break;
			case "1":
				$this->declareParsing();
				break;
			default:
				throw new Exception("Étape inconnue");
		}
	}

	private function declareSource()
	{
		$formData = new ImportSourceForm();
		if (Yii::app()->request->isPostRequest) {
			$formData->setAttributes($_POST['q'] ?? []);
			if ($formData->validate() && !$formData->hasErrors()) {
				$log = new AttributImportLog();
				$log->userId = (int) Yii::app()->user->id;
				if ($formData->url) {
					$downloader = new Downloader('tableur');
					try {
						$downloader->download($formData->url);
						$sourceFile = $downloader->getLocalFilepath();
						$log->url = $formData->url;
						$log->fileName = $downloader->getRemoteFilename();
						$log->fileKey = FileImport::storeFile($sourceFile);
					} catch (\Throwable $e) {
						$formData->addError('url', "Erreur lors du téléchargement : {$e->getMessage()}");
					}
				} else {
					$file = $formData->uploadedFile;
					$log->url = '';
					$log->fileName = $file->getName();
					$log->fileKey = FileImport::storeFile($file->tempName, $file->getExtensionName());
				}
				if ($log->fileKey) {
					$log->save(false);
					$this->getController()->redirect(['import', 'etape' => '1', 'id' => $log->id]);
					return;
				}
			}
		}

		$this->getController()->render(
			'import-0',
			[
				'formData' => $formData,
				// Assuming that PHP ini's config uses the same unit for both fields...
				'maxUpload' => (
					(int) ini_get('upload_max_filesize') > (int) ini_get('post_max_size')
					? ini_get('upload_max_filesize')
					: ini_get('post_max_size')
				),
			]
		);
	}

	private function declareParsing()
	{
		$log = AttributImportLog::model()->findByPk((int) Yii::app()->request->getParam('id', 0));
		if (!($log instanceof AttributImportLog)) {
			throw new Exception("L'historique des imports n'a pas cet enregistrement.");
		}
		if ($log->logtime > 0) {
			// The requested log is already complete, we need a new record for a new import.
			$newLog = $log->needNewLog();
			$this->getController()->redirect(['import', 'etape' => '1', 'id' => $newLog->id]);
			return;
		}

		$formData = new ImportParsingForm();
		if (Yii::app()->request->isPostRequest) {
			$formData->setAttributes($_POST['q'] ?? []);
			if ($formData->validate() && !$formData->hasErrors()) {
				try {
					$process = new FileImport($log, $formData->extractConfig());
				} catch (Exception $e) {
					throw new CHttpException(404, $e->getMessage());
				}
				if ($process->save()) {
					$this->getController()->redirect(['report', 'id' => $process->getLog()->id]);
					return;
				}
				Yii::app()->user->setFlash(
					'error',
					"Erreur, le fichier n'a pas pu être lu. Erreur de contenu ? {$process->getLastError()}"
				);
			}
		}
		$formData->sourceColumns = FileImport::readSourceHeader($log->getFilePath());

		$this->getController()->render(
			'import-1',
			[
				'formData' => $formData,
				'log' => $log,
			]
		);
	}
}
