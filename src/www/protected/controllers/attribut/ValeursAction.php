<?php

namespace controllers\attribut;

use Sourceattribut;
use Yii;

class ValeursAction extends \CAction
{
	/**
	 * @param string $id Sourceattribut.id
	 */
	public function run(string $id)
	{
		$attribut = Sourceattribut::model()->findByPk((int) $id);
		$valeurs = Yii::app()->db
			->createCommand(<<<EOSQL
				SELECT t.*, at.valeur AS attrValeur, unix_timestamp(at.hdate) AS attrHdate
				FROM AttributTitre at
					JOIN Titre t ON at.titreId = t.id
				WHERE at.sourceattributId = :id
				ORDER BY t.titre ASC
				EOSQL
			)->queryAll(true, [':id' => $attribut->id]);

		$this->getController()->render(
			'valeurs',
			[
				'attribut' => $attribut,
				'valeurs' => $valeurs,
			]
		);
	}
}
