<?php

namespace controllers\attribut;

use AttributImportLog;
use CHttpException;
use processes\attribut\Report;
use Sourceattribut;

/**
 * Affiche le rapport résultant d'un import d'attribut.
 */
class ReportAction extends \CAction
{
	/**
	 * @param string $id An ID of AttributImportLog!
	 */
	public function run(string $id)
	{
		$log = AttributImportLog::model()->findByPk((int) $id);
		if (!($log instanceof AttributImportLog)) {
			throw new CHttpException(404, "L'historique des imports d'attributs n'a pas cet enregistrement.");
		}
		try {
			$report = Report::loadByKey($log->reportKey);
		} catch (\Throwable $e) {
			throw new CHttpException(404, "Le rapport n'a pu être chargé : {$e->getMessage()}");
		}
		$this->getController()->render(
			'report',
			[
				'attribut' => Sourceattribut::model()->findByPk($log->sourceattributId),
				'log' => $log,
				'report' => $report,
			]
		);
	}
}
