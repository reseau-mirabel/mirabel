<?php

namespace controllers\attribut;

use CHttpException;
use Exception;
use processes\attribut\Report;

class JournalCreationAction extends \CAction
{
	public function run(string $id)
	{
		try {
			$report = Report::loadLast($id);
		} catch (Exception $ex) {
			throw new CHttpException(404, $ex->getMessage());
		}

		if (!$report->unknownTitles) {
			throw new CHttpException(404, "Le rapport de cet import ne contient pas de liste de titres inconnus.");
		}

		$this->getController()->render(
			'journal-creation',
			[
				'report' => $report,
			]
		);
	}
}
