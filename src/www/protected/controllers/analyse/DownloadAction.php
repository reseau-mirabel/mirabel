<?php

namespace controllers\analyse;

use components\FileStore;
use Yii;

/**
 * Récupérer le contenu d'un fichier du FileStore "compare-issn"
 */
class DownloadAction extends \CAction
{
	public function run(string $file, string $name)
	{
		$store = new FileStore('compare-issn');
		$path = $store->get($file);
		if (!$path) {
			throw new \CHttpException(404, "Fichier non trouvé (trop ancien ?)");
		}

		header('Content-Type: application/force-download');
		$encFile = rawurlencode(basename($name));
		header('Content-Disposition: attachment;filename=' . $encFile);

		// Workaround Yii1's debug mode where HTML is appended to the response.
		ob_start();
		Yii::app()->end(0, false);
		ob_end_clean();

		readfile($path);
	}
}
