<?php

namespace controllers\analyse;

use components\FileStore;
use CUploadedFile;
use processes\analyse\CompareForm;
use Yii;

/**
 * Comparer les revues de M avec un fichier tableur importé (jetable)
 */
class CompareIssnAction extends \CAction
{
	public const KEEP_FILE_DAYS = 7;

	public function run(): void
	{
		$formData = new CompareForm();
		if (Yii::app()->request->isPostRequest) {
			$formData->setAttributes($_POST['q'] ?? []);
			if ($formData->validate() && !$formData->hasErrors()) {
				$filename = self::storeFile($formData->uploadedFile);
				$this->getController()->redirect(['report', 'file' => $filename]);
			}
		}
		$this->getController()->render(
			'compare-issn',
			[
				'formData' => $formData,
				'maxUpload' => min(ini_get('upload_max_filesize'), ini_get('post_max_size')),
			]
		);
	}

	private static function getStore(): FileStore
	{
		$store = new FileStore('compare-issn');
		$store->flushOlderThan(self::KEEP_FILE_DAYS * 86400);
		return $store;
	}

	private static function storeFile(CUploadedFile $file): string
	{
		$hash = hash_file('md5', $file->tempName);

		// Store the file
		$filename =  "{$hash}." . $file->getExtensionName();
		$store = self::getStore();
		$store->set($filename, $file->tempName);

		// Store its metadata
		$store->setByContent("{$hash}.json", json_encode(['filename' => $file->name]));

		return $filename;
	}
}
