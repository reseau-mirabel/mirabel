<?php

namespace controllers\analyse;

class CreateTokenAction extends \CAction
{
	public function run(string $date)
	{
		$timestamp = strtotime($date);
		if ($timestamp > time() && $timestamp < time() + 8640000) {
			$tokenizer = new \components\AccessToken('analyse/', $timestamp);
			$url = $this->getController()->createAbsoluteUrl('analyse/compare-issn', ['access-token' => $tokenizer->createToken()]);
			\Yii::app()->user->setFlash('success', "Lien d'accès temporaire : " . \CHtml::encode($url));
		}
		$this->getController()->redirect('/analyse');
	}
}
