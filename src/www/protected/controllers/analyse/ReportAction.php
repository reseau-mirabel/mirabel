<?php

namespace controllers\analyse;

use CFileHelper;
use components\FileStore;
use Partenaire;
use processes\analyse\Compare;
use Yii;

class ReportAction extends \CAction
{
	public const KEEP_FILE_DAYS = 7;

	public function run($file)
	{
		$store = self::getStore();
		$path = $store->get($file);
		if (!$path) {
			throw new \CHttpException(404, "Fichier non trouvé (trop ancien ?)");
		}
		$comparer = self::analyzeFile($path);

		$metadataPath = $store->get(preg_replace('/\..+?$/', '.json', $file));
		$metadata = json_decode(file_get_contents($metadataPath), false);

		$partenaireId = (int) Yii::app()->user->getState('partenaireId');
		$this->getController()->render(
			'report',
			[
				'file' => $file,
				'matches' => $comparer->getMatches($partenaireId),
				'metadata' => $metadata,
				'partenaire' => Partenaire::model()->findByPk($partenaireId),
				'process' => $comparer,
			]
		);
	}

	private static function analyzeFile(string $path): Compare
	{
		$comparer = new Compare();
		$extension = CFileHelper::getExtension($path);
		try {
			if (in_array($extension, ['csv', 'tsv', 'txt'], true)) {
				$comparer->loadCsv($path);
			} else {
				$mimeType = CFileHelper::getMimeType($path);
				$comparer->loadSpreadsheet($path, $mimeType);
			}
		} catch (\Throwable $e) {
			Yii::log("Erreur de lecture du fichier : {$e->getMessage()}", 'warning');
			Yii::app()->user->setFlash(
				'error',
				"Erreur inconnue, le fichier n'a pas pu être lu. Il faut déposer un fichier textuel (csv, kbart, txt) ou un fichier de tableur (ods, xlsx)."
			);
		}
		return $comparer;
	}

	private static function getStore(): FileStore
	{
		$store = new FileStore('compare-issn');
		$store->flushOlderThan(self::KEEP_FILE_DAYS * 86400);
		return $store;
	}
}
