<?php

class SiteController extends Controller
{
	/**
	 * Maps actions to dedicated classes.
	 */
	public function actions()
	{
		return [
			'actualite' => \controllers\site\ActualiteAction::class,
			'captcha' => [
				'class' => 'CaptchaSumAction',
				'backColor' => 0xFFFFFF,
			],
			'contact' => 'controllers\site\Contact',
			'csp-report' => controllers\site\CspReportAction::class,
			'error' => 'controllers\site\Error',
			'index' => 'controllers\site\Index',
			'login' => 'controllers\site\Login',
			'politiques' => 'controllers\site\Politiques',
			'page' => \controllers\site\CmsPageAction::class, // /site/page executes CmsPageAction::run()
			'search' => 'controllers\site\Search',
		];
	}

	public function actionPresentation()
	{
		$this->render('presentation');
	}

	/**
	 * Web service for koha.
	 *
	 * @codeCoverageIgnore
	 * @deprecated
	 */
	public function actionService()
	{
		if (!getenv('REDIRECT_WEBSERVICE')) {
			throw new CHttpException(403, "Accès restreint par IP.");
		}
		\Controller::header('xml');
		$model = new WebService();
		$model->setAttributes($_GET);
		$output = $model->run();
		echo $output;
	}

	public function actionLogged()
	{
		$messages = Utilisateur::model()->findByPk(Yii::app()->user->id)->getLoginMessages();
		if ($messages) {
			Yii::app()->user->setFlash('info', $messages);
		}
		$this->redirect(Yii::app()->user->getReturnUrl());
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		$user = Yii::app()->user;
		$keep = [
			'instituteId' => $user->getState('instituteId'),
			'instituteName' => $user->getState('instituteName'),
			'instituteShortname' => $user->getState('instituteShortname'),
		];
		// Do not destroy the session, remove only the identity state.
		// This way, the 'institute*' states are kept in the session.
		$user->logout(false);
		if ($keep['instituteId'] !== null) {
			foreach ($keep as $k => $v) {
				$user->setState($k, $v);
			}
		}

		$this->redirect(Yii::app()->homeUrl);
	}
}
