<?php

namespace controllers\grappe;

use Grappe;

class AdminViewAction extends \CAction
{
	public function run(string $id)
	{
		$grappe = Grappe::model()->findByPk((int) $id);
		if (!($grappe instanceof Grappe)) {
			throw new \CHttpException(404, "Aucune grappe n'a cet identifiant");
		}
		$checkAccess = new \processes\grappe\Permissions(\Yii::app()->user);
		if (!$checkAccess->canWrite($grappe)) {
			$this->getController()->redirect(['/grappe/view', 'id' => $grappe->id]);
			return;
		}

		$titresDirects = $grappe->findTitres("gt.source = " . Grappe::SOURCE_NOTSEARCH, false);
		$titresInduits = $grappe->findTitres("gt.source > " . Grappe::SOURCE_NOTSEARCH, false);
		$this->getController()->render(
			'view-partenaire',
			[
				'grappe' => $grappe,
				'titresDirects' => $titresDirects,
				'titresInduits' => $titresInduits,
			]
		);
	}
}
