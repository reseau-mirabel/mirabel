<?php

namespace controllers\grappe;

use processes\grappe\FilteredList;
use Yii;

class PartenaireAction extends \CAction
{
	public function run(int $id = 0): void
	{
		$isAdmin = (new \processes\grappe\Permissions(Yii::app()->user))->canAdmin();
		if ($isAdmin) {
			$partenaireId = $id ?: (int) Yii::app()->user->partenaireId;
		} else {
			$partenaireId = (int) Yii::app()->user->partenaireId;
		}
		$partenaire = \Partenaire::model()->findByPk($partenaireId);

		$grappesPart = (new FilteredList)->requirePartenaire($partenaireId);

		$this->controller->layout = '//layouts/column2';
		$this->getController()->render(
			'index-partenaire',
			[
				'grappesPart' => $grappesPart,
				'partenaire' => $partenaire,
			]
		);
	}
}
