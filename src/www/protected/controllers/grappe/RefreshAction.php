<?php

namespace controllers\grappe;

use Grappe;

class RefreshAction extends \CAction
{
	public function run(string $id)
	{
		$grappe = Grappe::model()->findByPk((int) $id);
		if (!($grappe instanceof Grappe)) {
			throw new \CHttpException(404, "Aucune grappe n'a cet identifiant");
		}
		(new \processes\grappe\ContentManager($grappe))->refreshSearches();
		$this->getController()->redirect(['admin-view', 'id' => $grappe->id]);
	}
}
