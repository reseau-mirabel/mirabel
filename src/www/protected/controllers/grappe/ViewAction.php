<?php

namespace controllers\grappe;

use Grappe;

class ViewAction extends \CAction
{
	private const LIMIT = 100;

	public function run(string $id, string $nom = '', bool $full = false)
	{
		$grappe = Grappe::model()->findByPk((int) $id);
		if (!($grappe instanceof Grappe)) {
			throw new \CHttpException(404, "Aucune grappe n'a cet identifiant");
		}
		$checkAccess = new \processes\grappe\Permissions(\Yii::app()->user);
		if (!$checkAccess->canRead($grappe)) {
			throw new \CHttpException(403, "Vous n'avez pas le droit de consulter cette grappe.");
		}

		if ($this->needsRedirection($grappe, $nom)) {
			return;
		}

		if ($full) {
			self::renderSubPage($grappe);
			return;
		}

		$titres = $grappe->findTitres("", $grappe->niveau === Grappe::NIVEAU_REVUE, self::LIMIT);
		$this->getController()->render(
			'view-guest',
			['grappe' => $grappe, 'titres' => $titres, 'checkAccess' => $checkAccess]
		);
	}

	private function needsRedirection(Grappe $grappe, string $nom): bool
	{
		$selfUrl = $grappe->getSelfUrl();
		$expectedName = $selfUrl['nom'] ?? '';
		if ($expectedName && (!$nom || $nom !== $expectedName)) {
			$this->getController()->redirect($selfUrl);
			return true;
		}
		return false;
	}

	private function renderSubpage(Grappe $grappe): void
	{
		$titres = $grappe->findTitres("", $grappe->niveau === Grappe::NIVEAU_REVUE);
		foreach ($titres as $t) {
			echo "<li>{$t->getSelfLink()}</li>\n";
		}
		// Force AJAX detection, in case there's a web logger
		//  that output debug info at the bottom of the page.
		$_SERVER['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest';
	}
}
