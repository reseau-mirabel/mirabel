<?php

namespace controllers\grappe;

use Grappe;
use processes\grappe\AddJournalsForm;
use processes\grappe\ContentManager;
use Titre;
use Yii;

class AddTitlesAction extends \CAction
{
	public function run(string $id)
	{
		$grappe = Grappe::model()->findByPk((int) $id);
		if (!($grappe instanceof Grappe)) {
			throw new \CHttpException(404, "Aucune grappe n'a cet identifiant");
		}
		$checkAccess = new \processes\grappe\Permissions(\Yii::app()->user);
		if (!$checkAccess->canWrite($grappe)) {
			throw new \CHttpException(403, "Vous n'êtes pas propriétaire de cette grappe.");
		}

		$formData = new AddJournalsForm();
		if ($formData->load(Yii::app()->request)) {
			$titreIds = $formData->extractIds();
			if ($formData->save) {
				$content = new ContentManager($grappe);
				$count = $content->addJournals($titreIds);
				\Yii::app()->user->setFlash('success', "{$count} titres ont été ajoutés.");
				$this->getController()->redirect(['/grappe/admin-view', 'id' => $grappe->id]);
				return;
			}
		} else {
			$titreIds = [];
		}

		$titres = [];
		if ($titreIds) {
			$idsStr = join(',', array_map('intval', $titreIds));
			$titres = Titre::model()
				->findAllBySql("SELECT id, revueId, titre, prefixe, sigle FROM Titre WHERE id IN ($idsStr) ORDER BY titre");
		}

		$this->getController()->render(
			'add-titles',
			[
				'formData' => $formData,
				'grappe' => $grappe,
				'titres' => $titres,
			]
		);
	}
}
