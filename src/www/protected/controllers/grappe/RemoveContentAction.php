<?php

namespace controllers\grappe;

use Grappe;
use processes\grappe\ContentManager;
use processes\grappe\Permissions;
use processes\grappe\RemoveContentForm;
use Titre;
use Yii;

class RemoveContentAction extends \CAction
{
	public function run(string $id)
	{
		$grappe = Grappe::model()->findByPk((int) $id);
		if (!($grappe instanceof Grappe)) {
			throw new \CHttpException(404, "Aucune grappe n'a cet identifiant");
		}
		$checkAccess = new Permissions(\Yii::app()->user);
		if (!$checkAccess->canWrite($grappe)) {
			throw new \CHttpException(403, "Vous n'êtes pas propriétaire de cette grappe.");
		}

		$formData = new RemoveContentForm();
		if ($formData->load(Yii::app()->request)) {
			try {
				$content = new ContentManager($grappe);
				$content->removeJournals($formData->titreIds);
				$content->removeSearches($formData->searches);
				\Yii::app()->user->setFlash('success', "Le contenu de cette grappe a été modifié.");
				$this->getController()->redirect(['/grappe/admin-view', 'id' => $grappe->id]);
				return;
			} catch (\Throwable $e) {
				Yii::log($e->getMessage(), 'error', 'grappes');
				\Yii::app()->user->setFlash('error', "Erreur lors de l'enregistrement");
			}
		}

		$this->getController()->render(
			'remove-content',
			[
				'formData' => $formData,
				'grappe' => $grappe,
				'titres' => Titre::model()->findAllBySql(<<<EOSQL
					SELECT t.id, t.revueId, t.titre, t.prefixe, t.sigle
					FROM Titre t JOIN Grappe_Titre gt ON t.id = gt.titreId
					WHERE gt.grappeId = {$grappe->id} AND gt.source = :s
					ORDER BY t.titre
					EOSQL,
					[':s' => \Grappe::SOURCE_NOTSEARCH]
				),
			]
		);
	}
}
