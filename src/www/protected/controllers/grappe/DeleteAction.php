<?php

namespace controllers\grappe;

use Grappe;

class DeleteAction extends \CAction
{
	public function run(string $id)
	{
		$grappe = Grappe::model()->findByPk((int) $id);
		if (!($grappe instanceof Grappe)) {
			throw new \CHttpException(404, "Aucune grappe n'a cet identifiant");
		}
		$checkAccess = new \processes\grappe\Permissions(\Yii::app()->user);
		if (!$checkAccess->canWrite($grappe)) {
			throw new \CHttpException(403, "Vous n'êtes pas propriétaire de cette grappe.");
		}
		if (!\Yii::app()->request->isPostRequest) {
			throw new \CHttpException(400, "Only POST is allowed.");
		}
		if (!$checkAccess->canAdmin()) {
			$inLists = \processes\grappe\Selection::getListsReferences($grappe);
			if ($inLists['isInGlobalList'] || $inLists['inOtherLists'] > 0) {
				\Yii::app()->user->setFlash('error', "Seul un administrateur peut supprimer une grappe référencée dans les listes externes à l'établissement créateur.");
				$this->getController()->redirect(['/grappe/admin-view', 'id' => $grappe->id]);
			}
		}

		if ($grappe->delete()) {
			\Yii::app()->user->setFlash('success', "La grappe est supprimée.");
		} else {
			\Yii::app()->user->setFlash('error', "Erreur lors de la suppression");
		}
		$this->getController()->redirect(['/grappe/index']);
	}
}
