<?php

namespace controllers\grappe;

use processes\grappe\FilteredList;
use Yii;

class AdminAction extends \CAction
{
	public function run(): void
	{
		$isAdmin = (new \processes\grappe\Permissions(Yii::app()->user))->canAdmin();
		if (!$isAdmin) {
			$this->controller->redirect(['/grappe/partenaire']);
			return;
		}
		$partenaireId = (int) Yii::app()->user->partenaireId;
		$grappesOther = (new FilteredList)->excludePartenaire($partenaireId);
		$grappesOther->load($_GET);
		$grappesPart = (new FilteredList)->requirePartenaire($partenaireId);
		$this->getController()->render(
			'admin',
			[
				'grappesPart' => $grappesPart,
				'grappesAutres' => $grappesOther,
				'partenaire' => \Partenaire::model()->findByPk($partenaireId),
			]
		);
	}
}
