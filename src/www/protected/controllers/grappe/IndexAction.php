<?php

namespace controllers\grappe;

use processes\grappe\Selection;

class IndexAction extends \CAction
{
	public function run(): void
	{
		$c = $this->getController();
		assert($c instanceof \Controller);
		$c->setCanonicalUrl(['/grappe/index']);
		$c->render(
			'index-public',
			['grappes' => Selection::findGrappesPubliques()]
		);
	}
}
