<?php

namespace controllers\grappe;

use processes\partenaire\GrappesSelection;
use processes\partenaire\GrappeAffichage;
use Yii;

class AdminPubliqueAction extends \CAction
{
	public function run()
	{
		$permissions = new \processes\grappe\Permissions(Yii::app()->user);
		if (!$permissions->canAdmin()) {
			throw new \CHttpException(403);
		}
		$formData = new GrappesSelection(null);

		// POST for saving selected elements to the selection?
		$select = \Yii::app()->request->getPost('GrappesSelection', []);
		if ($formData->load($select) && $formData->addGrappesToSelection()) {
			Yii::app()->user->setFlash('success', "La liste publique a été mise à jour.");
			$this->getController()->redirect(['/grappe/admin-publique']);
			return;
		}
		// GET to search some elements and display them in a list?
		$search = \Yii::app()->request->getQuery('GrappesSelection', []);
		if (isset($search['nom'])) {
			$formData->load($search);
		}

		$selected = GrappeAffichage::loadFromDb(null);
		$updates = Yii::app()->request->getPost('GrappeAffichage', []);
		if ($updates) {
			self::writeUpdates($selected, $updates);
			Yii::app()->user->setFlash('success', "La liste a été mise à jour.");
			$this->getController()->refresh();
			return;
		}

		$this->getController()->render(
			'admin-publique',
			[
				'formData' => $formData,
				'selected' => $selected,
			]
		);
	}

	/**
	 * @param GrappeAffichage[] $records
	 */
	private static function writeUpdates(array $records, array $updates): bool
	{
		$valid = true;
		$transaction = \Yii::app()->db->beginTransaction();
		$position = 0;
		foreach ($updates as $id => $input) {
			$position++;
			if (isset($records[$id])) {
				$relation = $records[$id];
				$relation->setAttributes($input);
				$valid = $relation->validate() && $valid;
				if ($valid) {
					$relation->write($position);
				}
			}
		}
		if (!$valid) {
			Yii::app()->user->setFlash('erreur', "Erreur lors de l'enregistrement. Modifications annulées.");
			$transaction->rollback();
			return false;
		}
		$transaction->commit();
		return true;
	}
}
