<?php

namespace controllers\grappe;

use Grappe;

class UpdateAction extends \CAction
{
	public function run(string $id)
	{
		$grappe = Grappe::model()->findByPk((int) $id);
		if (!($grappe instanceof Grappe)) {
			throw new \CHttpException(404, "Aucune grappe n'a cet identifiant");
		}
		$checkAccess = new \processes\grappe\Permissions(\Yii::app()->user);
		if (!$checkAccess->canWrite($grappe)) {
			throw new \CHttpException(403, "Vous n'êtes pas propriétaire de cette grappe.");
		}

		$formData = new \processes\grappe\GrappeForm();
		$formData->setGrappe($grappe);

		if (\Yii::app()->request->isPostRequest) {
			$formData->setAttributes(\Yii::app()->request->getPost('Grappe', []));
			if ($formData->validate()) {
				$grappe = $formData->save();
				if ($grappe->hasErrors()) {
					\Yii::app()->user->setFlash('error', "Erreur lors de l'enregistrement");
				} elseif ($grappe->id > 0) {
					\Yii::app()->user->setFlash('success', "La modification de cette grappe est enregistrée.");
					$this->getController()->redirect(['/grappe/admin-view', 'id' => $grappe->id]);
					return;
				}
			}
		}

		$this->getController()->render(
			'update',
			[
				'formData' => $formData,
			]
		);
	}
}
