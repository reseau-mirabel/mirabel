<?php

namespace controllers\grappe;

use Grappe;
use Yii;

class AddSearchAction extends \CAction
{
	public function run(string $id)
	{
		$grappe = Grappe::model()->findByPk((int) $id);
		if (!($grappe instanceof Grappe)) {
			throw new \CHttpException(404, "Aucune grappe n'a cet identifiant");
		}
		$checkAccess = new \processes\grappe\Permissions(\Yii::app()->user);
		if (!$checkAccess->canWrite($grappe)) {
			throw new \CHttpException(403, "Vous n'êtes pas propriétaire de cette grappe.");
		}

		if (Yii::app()->request->isPostRequest) {
			$url = Yii::app()->request->getPost('url', '');
			$content = new \processes\grappe\ContentManager($grappe);
			$result = $content->addSearch($url);
			Yii::app()->user->setFlash($result['status'], $result['message']);
			if ($result['status'] === 'success') {
				$this->getController()->redirect(['/grappe/admin-view', 'id' => $grappe->id]);
				return;
			}
		}

		$this->getController()->render('add-search', ['grappe' => $grappe]);
	}
}
