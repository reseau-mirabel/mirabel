<?php

namespace controllers\grappe;

class CreateAction extends \CAction
{
	public function run(): void
	{
		$formData = new \processes\grappe\GrappeForm('insert');
		if (!\Yii::app()->user->checkAccess('admin')) {
			$formData->partenaireId = (int) \Yii::app()->user->partenaireId;
		}

		if (\Yii::app()->request->isPostRequest) {
			$formData->setAttributes(\Yii::app()->request->getPost('Grappe', []));
			if ($formData->validate()) {
				$grappe = $formData->save();
				if ($grappe->hasErrors()) {
					\Yii::app()->user->setFlash('error', "Erreur lors de l'enregistrement");
				} elseif ($grappe->id > 0) {
					\Yii::app()->user->setFlash('success', "La nouvelle grappe est enregistrée.");
					$this->getController()->redirect(['/grappe/admin-view', 'id' => $grappe->id]);
					return;
				}
			}
		}
		$this->getController()->render(
			'create',
			['formData' => $formData]
		);
	}
}
