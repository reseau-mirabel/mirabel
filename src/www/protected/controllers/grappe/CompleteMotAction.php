<?php

namespace controllers\grappe;

class CompleteMotAction extends \CAction
{
	public function run(string $term)
	{
		$searched = self::extractLastWord($term);
		$matchingWords = \Yii::app()->db
			->createCommand(<<<SQL
				SELECT DISTINCT REGEXP_SUBSTR(nom, :pattern) FROM Grappe WHERE diffusion = :d
				SQL
			)
			->queryColumn([
				':pattern' => '\\b\\Q' . $searched . '\\E[@\\p{L}]+',
				':d' => \Grappe::DIFFUSION_PARTAGE,
			]);
		$result = [];
		foreach ($matchingWords as $w) {
			if ($w) {
				$result[] = ['id' => $w, 'label' => $w, 'value' => $w];
			}
		}
		\Controller::header('json');
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}

	private static function extractLastWord(string $term): string
	{
		$words = preg_split('/[,;\s]+/', $term);
		return array_pop($words);
	}
}
