<?php

class AttributController extends Controller
{
	public $defaultAction = 'declare';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl',
		];
	}

	/**
	 * Specifies the access control rules.
	 *
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'roles' => ['admin'],
			],
			[
				'allow',
				'actions' => ['valeurs'],
				'users' => ['@'],
			],
			[
				'deny',  // otherwise deny all users
				'users' => ['*'],
			],
		];
	}

	/**
	 * Maps actions to dedicated classes.
	 */
	public function actions()
	{
		return [
			'ajax-list-values' => 'controllers\attribut\AjaxListValuesAction',
			'declare' => 'controllers\attribut\DeclareAction',
			'delete' => 'controllers\attribut\DeleteAction',
			'historique' => 'controllers\attribut\HistoriqueAction',
			'import' => 'controllers\attribut\ImportAction',
			'import-csv' => 'controllers\attribut\ImportCsvAction',
			'journal-creation' => 'controllers\attribut\JournalCreationAction',
			'report' => 'controllers\attribut\ReportAction',
			'valeurs' => 'controllers\attribut\ValeursAction',
		];
	}
}
