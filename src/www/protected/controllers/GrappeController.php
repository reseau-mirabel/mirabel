<?php

class GrappeController extends Controller
{
	public $layout = '//layouts/column1';

	public function filters()
	{
		return [
			'accessControl',
		];
	}

	/**
	 * Specifies the access control rules. This method is used by the 'accessControl' filter.
	 *
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'actions' => ['index', 'view'],
				'users' => ['*'],
			],
			[
				'allow',
				'actions' => ['admin-view', 'ajout-recherche', 'ajout-titres', 'complete-mot', 'delete', 'partenaire', 'refresh', 'suppression-contenu', 'update'],
				'roles' => ['avec-partenaire'], // Stricter checks are inside each action.
			],
			[
				'allow',
				'actions' => ['admin', 'admin-publique', 'admin-publique-suppr'],
				'expression' => function (\WebUser $user) {
					$perms = new \processes\grappe\Permissions($user);
					return $perms->canCreate() || $perms->canWrite(null);
				},
			],
			[
				'allow',
				'actions' => ['create'],
				'expression' => function (\WebUser $user) {
					return (new \processes\grappe\Permissions($user))->canCreate();
				},
			],
			[
				'deny',  // otherwise deny all users
				'users' => ['*'],
			],
		];
	}

	/**
	 * Maps actions to dedicated classes.
	 */
	public function actions()
	{
		return [
			'admin' => \controllers\grappe\AdminAction::class,
			'admin-publique' => \controllers\grappe\AdminPubliqueAction::class,
			'admin-publique-suppr' => \controllers\partenaire\GrappesRemoveAction::class,
			'admin-view' => \controllers\grappe\AdminViewAction::class,
			'ajout-recherche' => \controllers\grappe\AddSearchAction::class,
			'ajout-titres' => \controllers\grappe\AddTitlesAction::class,
			'create' => \controllers\grappe\CreateAction::class,
			'complete-mot' => \controllers\grappe\CompleteMotAction::class,
			'delete' => \controllers\grappe\DeleteAction::class,
			'index' => \controllers\grappe\IndexAction::class,
			'partenaire' => \controllers\grappe\PartenaireAction::class,
			'refresh' => \controllers\grappe\RefreshAction::class,
			'suppression-contenu' => \controllers\grappe\RemoveContentAction::class,
			'update' => \controllers\grappe\UpdateAction::class,
			'view' => \controllers\grappe\ViewAction::class,
		];
	}
}
