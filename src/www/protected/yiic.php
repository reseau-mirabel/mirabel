<?php

define('VENDOR_PATH', dirname(__DIR__, 3) . '/vendor');
require_once VENDOR_PATH . '/autoload.php';
spl_autoload_register(function (string $c): void { YiiBase::autoload($c); });

$localConfig = require __DIR__ . '/config/local.php';
if (defined('DATA_DIR')) {
	foreach (['', '/cache/yii', '/feeds', '/filestore', '/logs', '/sessions'] as $d) {
		if (!is_dir(DATA_DIR . $d)) {
			mkdir(DATA_DIR . $d, 0777, true);
		}
	}
} else {
	echo <<<TXT
		L'application n'a pas configuré le stockage de fichier.
		Ajoutez une ligne :

		defined('DATA_DIR') or define('DATA_DIR', dirname(__DIR__, 4) . '/data');

		en haut du fichier "src/www/proctected/config/local.php".

		TXT;
	exit(1);
}

defined('YII_ENV') or define('YII_ENV', APP_PRODUCTION_MODE ? 'prod' : 'dev');
$config = CMap::mergeArray(
	CMap::mergeArray(require __DIR__ . '/config/main.php', $localConfig),
	require __DIR__ . '/config/console.php'
);
unset($config['session'], $config['user']);

require __DIR__ . '/YiiConsole.php';
$app = new \components\app\ConsoleApplication($config);
$app->commandRunner->addCommands(YII_PATH . '/cli/commands');
if ($app->params->contains('baseUrl')) {
	$urlManager = $app->getComponent('urlManager');
	assert($urlManager instanceof CUrlManager);
	$urlManager->setBaseUrl($app->params->itemAt('baseUrl'));
	$app->getRequest()->setScriptUrl($app->params->itemAt('baseUrl'));
}
Yii::setPathOfAlias('approot', __DIR__);
Yii::setPathOfAlias('webroot', dirname(__DIR__));

$env = @getenv('YII_CONSOLE_COMMANDS');
if (!empty($env)) {
	$app->commandRunner->addCommands($env);
}

$app->run();
