<?php

// Tests store the application and framework files in a distinct directory, to avoid aside effects.
define('DATA_DIR', dirname(__DIR__, 3) . '/data-tests');
foreach (['', '/cache/yii', '/feeds', '/filestore', '/logs', '/sessions'] as $d) {
	if (!is_dir(DATA_DIR . $d)) {
		mkdir(DATA_DIR . $d, 0700, true);
	}
}

define('VENDOR_PATH', dirname(__DIR__, 3) . '/vendor');
require_once VENDOR_PATH . '/autoload.php';
spl_autoload_register(function (string $c): void { YiiBase::autoload($c); });

define('YII_TARGET', 'test');
defined('YII_ENV') or define('YII_ENV', 'dev');

$localConfig = require __DIR__ . '/config/local.php';
$config = CMap::mergeArray(
	CMap::mergeArray(require __DIR__ . '/config/main.php', $localConfig),
	CMap::mergeArray(require __DIR__ . '/config/console.php', require __DIR__ . '/config/test.php')
);

require __DIR__ . '/YiiConsole.php';
$app = new \components\app\ConsoleApplication($config);
$app->commandRunner->addCommands(YII_PATH . '/cli/commands');

Yii::setPathOfAlias('approot', __DIR__);
Yii::setPathOfAlias('webroot', dirname(__DIR__));

$app->run();
