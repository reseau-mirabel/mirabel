<?php

namespace processes\search\titres;

/**
 * Store the criteria for searching on Sphinx Titres.
 *
 * No validation, the data must be clean when loaded into this object.
 * The JSON pre-encoding is also suitable for URL params.
 */
class Params implements \JsonSerializable
{
	public const MONIT_IGNORE = 0;

	public const MONIT_YES = 1;

	public const MONIT_NO = 2;

	/**
	 * @var "NONE"|"ET"|"OU"
	 */
	public string $aboCombine = "NONE";

	public bool $abonnement = false;

	/**
	 * @var int[]
	 */
	public array $acces = [];

	public bool $accesLibre = false;

	/**
	 * @var int[]
	 */
	public array $aediteurId = [];

	/**
	 * @var int[]
	 */
	public array $attribut = [];

	/**
	 * @var int[]
	 */
	public array $categorie = [];

	public bool $categoriesEt = false;

	/**
	 * @var int[]
	 */
	public array $cNonRec = [];

	public ?int $collectionId = null;

	/**
	 * @var int[]
	 */
	public array $detenu = [];

	/**
	 * @var int[]
	 */
	public array $editeurId = [];

	public int $grappe = 0;

	public string $hdateModif = '';

	public string $hdateVerif = '';

	public string $issn = '';

	public int $issnpaysid = 0;

	/**
	 * @var string[]
	 */
	public array $langues = [];

	public bool $languesEt = false;

	/**
	 * @var int[]
	 */
	public array $lien = [];

	/**
	 * @var int 0|1|2
	 */
	public int $monitored = 0;

	public bool $monitoredByMe = false;

	public bool $owned = false;

	/**
	 * @var int[]
	 */
	public array $paysId = [];

	public int $pid = 0;

	/**
	 * @var int[]
	 */
	public array $ressourceId = [];

	/**
	 * @var int[]
	 */
	public array $suivi = [];

	public bool $sansAcces = false;

	public bool $sanscategorie = false;

	public string $titre = '';

	public bool $vivant = false;

	/**
	 * Converts an ISSN 1234-5678 into a number 1234567.
	 */
	public function getIssnAsInt(): int
	{
		if ($this->issn === '') {
			return 0;
		}
		$int = substr(str_replace('-', '', $this->issn), 0, 7);
		return (ctype_digit($int) ? (int) $int : 0);
	}

	public function getLangAsInts(): array
	{
		$langids = [];
		foreach ($this->langues as $lang) {
			if (preg_match('/^[A-Za-z]{3}$/', $lang)) {
				$langids[] = (int) base_convert($lang, 36, 10);
			}
		}
		return $langids;
	}

	/**
	 * Create a new instance, without validity checks: only for loading from safe sources.
	 */
	public static function loadFromArray(array $attr): self
	{
		$new = new self;

		// string
		foreach (['hdateModif', 'hdateVerif', 'issn', 'titre'] as $name) {
			if (!empty($attr[$name])) {
				$new->{$name} = (string) $attr[$name];
			}
		}
		// string, special case
		if (isset($attr['aboCombine']) && in_array($attr['aboCombine'], ['NONE', 'ET', 'OU'], true)) {
			$new->aboCombine = (string) $attr['aboCombine'];
		}

		// string[]
		if ($attr['langues'] ?? '') {
			if ($attr['langues'] === 'aucune' || $attr['langues'] === '!') {
				$new->langues = ['!'];
			} else {
				$new->langues = explode(',', $attr['langues']);
			}
		}

		// bool
		foreach (['abonnement', 'accesLibre', 'categoriesEt', 'languesEt', 'monitoredByMe', 'owned', 'sansAcces', 'sanscategorie', 'vivant'] as $name) {
			if (!empty($attr[$name])) {
				$new->{$name} = (bool) $attr[$name];
			}
		}

		// int
		foreach (['collectionId', 'grappe', 'issnpaysid', 'monitored'] as $name) {
			if (!empty($attr[$name])) {
				$new->{$name} = (int) $attr[$name];
			}
		}

		// int[]
		foreach (['acces', 'aediteurId', 'attribut', 'categorie', 'cNonRec', 'detenu', 'editeurId', 'lien', 'paysId', 'ressourceId', 'suivi'] as $name) {
			if (!empty($attr[$name])) {
				$values = is_array($attr[$name]) ? $attr[$name] : explode('_', (string) $attr[$name]);
				$new->{$name} = array_map('intval', $values);
			}
		}

		// Dependent fields
		if (!empty($attr['owned'])) {
			$new->detenu = [];
		}
		if (!empty($attr['monitored']) || !empty($attr['monitoredByMe'])) {
			$new->suivi = [];
		}
		if ((!empty($attr['abonnement']) || !empty($attr['monitoredByMe']) || !empty($attr['owned'])) && ($attr['pid'] ?? 0) > 0) {
			// partenaireId only if used
			$new->pid = (int) $attr['pid'];
		}

		return $new;
	}

	public function toArray(): array
	{
		$attr = [];

		// string
		foreach (['hdateModif', 'hdateVerif', 'issn', 'titre'] as $name) {
			if ($this->{$name}) {
				$attr[$name] = (string) $this->{$name};
			}
		}
		// string, special case
		if ($this->aboCombine !== 'NONE') {
			$attr['aboCombine'] = (string) $this->aboCombine;
		}

		// string[]
		if ($this->langues) {
			$attr['langues'] = join(',', $this->langues);
		}

		// bool
		foreach (['abonnement', 'accesLibre', 'categoriesEt', 'monitoredByMe', 'owned', 'sansAcces', 'sanscategorie', 'vivant'] as $name) {
			if ($this->{$name}) {
				$attr[$name] = (bool) $this->{$name};
			}
		}

		// int
		foreach (['collectionId', 'grappe', 'issnpaysid', 'monitored'] as $name) {
			if ($this->{$name}) {
				$attr[$name] = (int) $this->{$name};
			}
		}

		// int[]
		foreach (['acces', 'aediteurId', 'attribut', 'categorie', 'cNonRec', 'detenu', 'editeurId', 'lien', 'paysId', 'ressourceId', 'suivi'] as $name) {
			if ($this->{$name}) {
				$attr[$name] = join('_', $this->{$name});
			}
		}

		// Dependent fields
		if ($this->owned) {
			unset($attr['detenu']);
		}
		if ($this->monitored || $this->monitoredByMe) {
			unset($attr['suivi']);
		}
		if (($this->abonnement || $this->monitoredByMe || $this->owned) && $this->pid > 0) {
			// partenaireId only if used
			$attr['pid'] = (int) $this->pid;
		}

		return $attr;
	}

	#[\ReturnTypeWillChange]
	public function jsonSerialize()
	{
		return $this->toArray();
	}
}
