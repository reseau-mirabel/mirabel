<?php

namespace processes\search\titres;

use components\DateTimeHelper;
use components\sphinx\Criteria;
use components\sphinx\DataProvider;
use components\SqlHelper;
use Yii;

class Actor
{
	/**
	 * Where to search themes:
	 * - 'categorie' for the most precise search
	 * - 'revuecategorie' for always applying themes to the Revue level (manual theming, or if missing, themes imported on titles)
	 */
	private const SPHINX_THEME = 'revuecategorie';

	private const SPHINX_THEME_RECURSIVE = 'revuecategorierec';

	public static function getDataProvider(Params $params): DataProvider
	{
		$sort = new \CSort(\models\sphinx\Titres::class);
		$sort->defaultOrder = 'cletri ASC';

		return new DataProvider(
			\models\sphinx\Titres::model(),
			[
				'criteria' => self::createSqlCriteria($params),
				'pagination' => false,
				'sort' => $sort,
			]
		);
	}

	public static function createSqlCriteria(Params $searchParams): Criteria
	{
		$criteria = new Criteria();
		$criteria->group = "revueid WITHIN GROUP ORDER BY obsolete ASC";
		$criteria->order = "cletri ASC";
		return self::addToCriteria($criteria, $searchParams);
	}

	public static function addToCriteria(Criteria $criteria, Params $searchParams): Criteria
	{
		if ($searchParams->titre) {
			$fulltextPattern = preg_replace('/([A-Z])\.\s?(?=[A-Z]\b)/', '$1', $searchParams->titre);
			$q = Yii::app()->db->quoteValue(\components\sphinx\Schema::quoteFulltext($fulltextPattern));
			$criteria->addCondition("MATCH($q)");
		}

		if ($searchParams->vivant) {
			$criteria->addCondition("vivant = 1");
		}

		if ($searchParams->issn) {
			$criteria->addCondition("issn = {$searchParams->getIssnAsInt()}");
		}

		if ($searchParams->collectionId) {
			$criteria->addCondition("collectionid = {$searchParams->collectionId}");
		}

		if ($searchParams->grappe) {
			self::addSqlCriteriaByIds($criteria, $searchParams, ['grappe' => 'grappe']);
		}

		if ($searchParams->issnpaysid) {
			self::addSqlCriteriaByIds($criteria, $searchParams, ['issnpaysid' => 'issnpaysid']);
		}

		if ($searchParams->paysId) {
			// intersection
			self::inAll($criteria, 'paysid', $searchParams->paysId);
		}

		self::addSqlCriteriaOnAccess($criteria, $searchParams); // abonnement, accesLibre, …
		self::addSqlCriteriaOnCategorie($criteria, $searchParams);
		self::addSqlCriteriaOnLang($criteria, $searchParams->langues, $searchParams->languesEt);
		self::addSqlCriteriaOnMonitoring($criteria, $searchParams);

		// Date intervals
		self::addSqlCriteriaByDateInterval($criteria, $searchParams->hdateModif, 'hdatemodif');
		self::addSqlCriteriaByDateInterval($criteria, $searchParams->hdateVerif, 'hdateverif');

		// Lists that can require (union) and exclude, e.g. [3, 5, -4] => 3 & 5 & not(4)
		self::addSqlCriteriaByFilterList($criteria, $searchParams->attribut, 'attribut');
		self::addSqlCriteriaByFilterList($criteria, $searchParams->lien, 'lien');

		// Integer lists (for each, union)
		$byIds = [
			'editeurId' => 'editeuractuelid',
			'aediteurId' => 'editeurid',
			'ressourceId' => 'ressourceid',
			'detenu' => 'detenu',
		];
		self::addSqlCriteriaByIds($criteria, $searchParams, $byIds);

		return $criteria;
	}

	/**
	 * The criteria will require that the field's value is in the intersection of $values.
	 *
	 * @param int[] $values
	 */
	private static function inAll(Criteria $criteria, string $fieldName, array $values): void
	{
		foreach ($values as $v) {
			$criteria->addCondition("$fieldName = " . (int) $v);
		}
	}

	private static function addSqlCriteriaByDateInterval(Criteria $criteria, string $date, string $sphinxAttr): void
	{
		if ($date === '') {
			return;
		}
		if ($date === '!' || $date === '0') {
			// pas de date
			$criteria->addCondition("$sphinxAttr = 0");
		} else {
			$ts = DateTimeHelper::parseDateInterval($date);
			if (!$ts) {
				return;
			}
			if ($ts[1]) {
				$criteria->addCondition("$sphinxAttr BETWEEN {$ts[0]} AND {$ts[1]}");
			} else {
				$criteria->addCondition("$sphinxAttr >= {$ts[0]}");
			}
		}
	}

	/**
	 * Filter each field by a union of the required IDs.
	 *
	 * For each mapping K => V,
	 * the criteria will require that the sphinx value V is among the IDs of the search params for K.
	 */
	private static function addSqlCriteriaByIds(Criteria $criteria, Params $searchParams, array $mapping): void
	{
		foreach ($mapping as $attr => $sphinxAttr) {
			$ids = $searchParams->{$attr};
			if (is_scalar($ids)) {
				$ids = [(int) $ids];
			}

			$require = [];
			$exclude = [];
			foreach ($ids as $id) {
				if (!is_int($id) || $id === 0) {
					continue;
				}
				if ($id > 0) {
					$require[] = $id;
				} else {
					$exclude[] = 0 - $id;
				}
			}
			if ($require) {
				$criteria->addInCondition($sphinxAttr, $require);
			}
			if ($exclude) {
				// We need to declare an aggregated column, then filter on it
				$criteria->select .= ", MAX($sphinxAttr IN (" . join(',', $exclude) . ")) AS {$sphinxAttr}_exclude";
				$criteria->having = "{$sphinxAttr}_exclude = 0";
			}
		}
	}

	private static function addSqlCriteriaByFilterList(Criteria $criteria, array $filterList, string $sphinxAttr): void
	{
		if (!$filterList) {
			return;
		}
		// All of the positive IDs, and none of the negative IDS.
		$attrProhibited = [];
		foreach ($filterList as $a) {
			if (!is_int($a) || $a === 0) {
				continue;
			}
			if ($a < 0) {
				$attrProhibited[] = -1 * $a;
			} else {
				// Add a AND filter of this sourceattributId
				$criteria->addCondition("$sphinxAttr = $a");
			}
		}
		if ($attrProhibited) {
			$criteria->addNotInCondition($sphinxAttr, $attrProhibited);
		}
	}

	private static function addSqlCriteriaOnAccess(Criteria $criteria, Params $searchParams): void
	{
		$filterAbonnement = [];
		if ($searchParams->abonnement && $searchParams->pid) {
			$filterAbonnement = [$searchParams->pid];
		}
		if ($filterAbonnement && $searchParams->accesLibre && $searchParams->owned) {
			// abonnement OR possession OR libre
			$criteria->select = sprintf(
				"*, IF(IN(abonnement, %s)  OR IN(detenu, %d) OR IN (acceslibre, %s), 1, 0) AS abolibre",
				join(",", array_map('intval', $filterAbonnement)),
				$searchParams->detenu[0] ?? $searchParams->pid,
				join(",", empty($searchParams->acces) ? [1, 2, 3, 4, 5] : array_map('intval', $searchParams->acces))
			);
			$criteria->addCondition("abolibre = 1"); // texte intégral en accès libre
			$searchParams->detenu = []; // An ugly hack to disable this filter since we just applied it.
		} elseif ($filterAbonnement && $searchParams->accesLibre) {
			// abonnement OR libre
			$criteria->select = sprintf(
				"*, IF(IN(abonnement, %s) OR IN (acceslibre, %s), 1, 0) AS abolibre",
				join(",", array_map('intval', $filterAbonnement)),
				join(",", empty($searchParams->acces) ? [1, 2, 3, 4, 5] : array_map('intval', $searchParams->acces))
			);
			$criteria->addCondition("abolibre = 1"); // texte intégral en accès libre
		} else {
			if ($filterAbonnement && $searchParams->owned && $searchParams->aboCombine === 'OU') {
				// abonnement (établissement abonné en ligne) OU possession (dispo dans l'établissement)
				$criteria->select = sprintf(
					"*, IF(IN(abonnement, %s) OR IN(detenu, %d), 1, 0) AS possouabo",
					join(",", array_map('intval', $filterAbonnement)),
					$searchParams->pid
				);
				$criteria->addCondition("possouabo = 1");
				$searchParams->detenu = []; // An ugly hack to disable this filter since we just applied it.
			} else {
				if ($filterAbonnement) {
					$criteria->addInCondition('abonnement', array_map('intval', $filterAbonnement));
				}
				if ($searchParams->owned && $searchParams->pid) {
					$criteria->addInCondition('detenu', [$searchParams->pid]);
				}
			}
			if (!empty($searchParams->acces)) {
				if ($searchParams->accesLibre) {
					$criteria->addInCondition('acceslibre', array_map('intval', $searchParams->acces));
				} else {
					$criteria->addInCondition('acces', array_map('intval', $searchParams->acces));
				}
			} elseif ($searchParams->accesLibre) {
				// require whatever type of free access
				$criteria->addCondition("acceslibre IN (1, 2, 3, 4, 5)");
			}
		}

		if ($searchParams->sansAcces) {
			$criteria->addCondition("nbacces = 0");
		}
	}

	private static function addSqlCriteriaOnCategorie(Criteria $criteria, Params $searchParams): void
	{
		if ($searchParams->categorie) {
			$categories = SqlHelper::sqlToPairsObject(
				"SELECT * FROM Categorie WHERE id in (" . join(",", $searchParams->categorie) . ")",
				"Categorie"
			);
			if ($searchParams->categoriesEt) {
				foreach ($searchParams->categorie as $cid) {
					if (in_array($cid, $searchParams->cNonRec) || $categories[$cid]->profondeur == 3) {
						$criteria->addInCondition(self::SPHINX_THEME, [$cid]);
					} else {
						$criteria->addInCondition(self::SPHINX_THEME_RECURSIVE, [(int) $cid]);
					}
				}
			} else {
				// We cannot use the recursive index because we may have to mix in
				// recursive and non-recursive ids into a single IN condition.
				$rec = [];
				$nonRecIds = [];
				foreach ($searchParams->categorie as $cid) {
					$nonRecIds[] = $cid;
					if (!in_array($cid, $searchParams->cNonRec)) {
						$rec[] = $categories[$cid];
					}
				}
				$subids = array_merge($nonRecIds, \Categorie::listDescendantsIds($rec));
				$criteria->addInCondition(self::SPHINX_THEME, $subids);
			}
		} elseif ($searchParams->sanscategorie) {
			$criteria->select = "{$criteria->select}, LENGTH(revuecategorie) AS numcategories";
			$criteria->addCondition("numcategories = 0");
		}
	}

	private static function addSqlCriteriaOnLang(Criteria $criteria, array $langues, bool $intersection): void
	{
		if (!$langues) {
			return;
		}
		if (count($langues) === 1 && $langues[0] === '!') {
			$criteria->select .= ", LENGTH(langue) AS numlangue";
			$criteria->addCondition("numlangue = 0 AND obsolete = 0");
			return;
		}

		$langids = [];
		foreach ($langues as $lang) {
			if (preg_match('/^[A-Za-z]{3}$/', $lang)) {
				$langids[] = (int) base_convert($lang, 36, 10);
			}
		}
		if ($langids) {
			if ($intersection) {
				foreach ($langids as $id) {
					$criteria->addInCondition('langue', [$id]);
				}
			} else {
				$criteria->addInCondition('langue', $langids);
			}
		}
	}

	private static function addSqlCriteriaOnMonitoring(Criteria $criteria, Params $searchParams): void
	{
		if ($searchParams->monitoredByMe) {
			$criteria->addCondition("suivi = {$searchParams->pid}");
		} elseif ($searchParams->monitored === Params::MONIT_YES) {
			$criteria->select .= ", LENGTH(suivi) AS numsuivi";
			$criteria->addCondition("numsuivi > 0");
		} elseif ($searchParams->monitored === Params::MONIT_NO) {
			$criteria->select .= ", LENGTH(suivi) AS numsuivi";
			$criteria->addCondition("numsuivi = 0");
		} elseif ($searchParams->suivi) {
			$criteria->addInCondition('suivi', $searchParams->suivi);
		}
	}
}
