<?php

namespace processes\search\facets;

class CountryFacet extends WithExtraResults implements \components\sphinx\FacetInterface
{
	use WithExclude;

	public function getName(): string
	{
		return 'paysid';
	}

	public function loadResults(array $results): void
	{
		if (!$results) {
			return;
		}
		$ids = $this->extractIds($results);
		if (!$ids) {
			return;
		}
		$names = \Yii::app()->db
			->createCommand("SELECT id, nom FROM Pays WHERE id IN ($ids) ORDER BY nom")
			->setFetchMode(\PDO::FETCH_KEY_PAIR)
			->queryAll();
		if (!$names) {
			return;
		}
		foreach ($results as [$id, $count]) {
			if (isset($names[$id])) {
				$this->filters[$id] = [(string) $names[$id], (int) $count];
			}
		}
	}

	private function extractIds(array $results): string
	{
		$ids = [];
		foreach ($results as $r) {
			$ids[] = (int) $r[0];
		}
		return join(",", $ids);
	}
}
