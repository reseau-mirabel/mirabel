<?php

namespace processes\search\facets;

class DisciplineFacet extends WithExtraResults implements \components\sphinx\FacetInterface
{
	use WithExclude;

	public function getName(): string
	{
		return 'revuecategorierec';
	}

	public function loadResults(array $results): void
	{
		if (!$results) {
			return;
		}
		$ids = [];
		foreach ($results as $r) {
			$ids[] = (int) $r[0];
		}
		$ids = array_filter($ids);
		if (count($ids) === 0) {
			return;
		}
		$names = \Yii::app()->db
			->createCommand("SELECT id, categorie FROM Categorie WHERE id IN (" . join(",", $ids) . ")")
			->setFetchMode(\PDO::FETCH_KEY_PAIR)
			->queryAll();
		if (!$names) {
			return;
		}
		foreach ($results as [$id, $count]) {
			if (isset($names[$id])) {
				$this->filters[$id] = [(string) $names[$id], (int) $count];
			}
		}
	}
}
