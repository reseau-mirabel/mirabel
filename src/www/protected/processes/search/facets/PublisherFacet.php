<?php

namespace processes\search\facets;

class PublisherFacet extends WithExtraResults implements \components\sphinx\FacetInterface
{
	use WithExclude;

	public function getName(): string
	{
		return 'editeurid';
	}

	public function loadResults(array $results): void
	{
		if (!$results) {
			return;
		}
		$ids = [];
		foreach ($results as $r) {
			$ids[] = (int) $r[0];
		}
		$names = \Yii::app()->db
			->createCommand("SELECT id, nom FROM Editeur WHERE id IN (" . join(",", $ids) . ")")
			->setFetchMode(\PDO::FETCH_KEY_PAIR)
			->queryAll();
		foreach ($results as [$id, $count]) {
			if (isset($names[$id])) {
				$this->filters[$id] = [(string) $names[$id], (int) $count];
			}
		}
	}
}
