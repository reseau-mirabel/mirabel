<?php

namespace processes\search\facets;

use models\lang\Codes;

class LangFacet extends WithExtraResults implements \components\sphinx\FacetInterface
{
	use WithExclude;

	public function getName(): string
	{
		return 'langue';
	}

	public function loadResults(array $results): void
	{
		if (!$results) {
			return;
		}
		foreach ($results as [$langInt, $count]) {
			$code3 = strtolower(base_convert((string) $langInt, 10, 36));
			$this->filters[$code3] = [Codes::CODES3[$code3] ?? '???', $count];
		}
	}
}
