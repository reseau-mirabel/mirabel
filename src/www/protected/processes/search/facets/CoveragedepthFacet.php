<?php

namespace processes\search\facets;

use models\sphinx\Titres;

class CoveragedepthFacet implements \components\sphinx\FacetInterface
{
	private const NAMES = [
		Titres::ACCES_OTHER => "Autre",
		Titres::ACCES_INTEGRAL => "Texte intégral",
		Titres::ACCES_RESUME => "Résumés",
		Titres::ACCES_SOMMAIRE => "Sommaires",
		Titres::ACCES_INDEXATION => "Indexation",
	];

	private array $filters = [];

	public function exclude(array $values): self
	{
		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function getName(): string
	{
		return 'acces';
	}

	/**
	 * @inheritdoc
	 */
	public function getExtraSql(): string
	{
		return "DISTINCT revueid";
	}

	/**
	 * @inheritdoc
	 */
	public function listFilters(): array
	{
		return $this->filters;
	}

	/**
	 * @inheritdoc
	 */
	public function loadResults(array $results): void
	{
		if (!$results) {
			return;
		}
		$ids = [];
		$names = [];
		foreach ($results as $r) {
			$id = (int) $r[0];
			$ids[] = $id;
			$names[$id] = self::NAMES[$id];
		}
		$ids = array_filter($ids);
		if (count($ids) === 0) {
			return;
		}
		foreach ($results as [$id, $count]) {
			if (isset($names[$id])) {
				$this->filters[$id] = [$names[$id], (int) $count];
			}
		}
	}
}
