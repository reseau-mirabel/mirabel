<?php

namespace processes\search\facets;

trait WithExclude
{
	protected array $filters = [];

	/**
	 * Remove some values from the facet.
	 */
	public function exclude(array $values): self
	{
		foreach ($values as $id) {
			if (isset($this->filters[$id])) {
				unset($this->filters[$id]);
			}
		}
		return $this;
	}
}
