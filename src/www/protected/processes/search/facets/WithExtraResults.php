<?php

namespace processes\search\facets;

abstract class WithExtraResults
{
	public const MAX_RESULTS = 20;

	/**
	 * @var array Stores the actual facets as [value => [name, count]]
	 */
	protected array $filters = [];

	protected bool $hasMoreResults = false;

	public function getExtraSql(): string
	{
		$max = self::MAX_RESULTS + 1;
		return "DISTINCT revueid ORDER BY count(*) DESC, FACET() ASC LIMIT $max";
	}

	public function hasMoreResults(): bool
	{
		if (count($this->filters) > self::MAX_RESULTS) {
			$this->hasMoreResults = true;
			array_pop($this->filters);
		}
		return $this->hasMoreResults;
	}

	public function listFilters(): array
	{
		if (count($this->filters) > self::MAX_RESULTS) {
			$this->hasMoreResults = true;
			array_pop($this->filters);
		}
		return $this->filters;
	}

	abstract public function loadResults(array $results): void;
}
