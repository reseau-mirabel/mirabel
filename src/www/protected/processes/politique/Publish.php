<?php

namespace processes\politique;

use Config;
use Politique;
use PolitiqueLog;
use components\access\Politique as Access;
use components\email\Mailer;

class Publish
{
	private Politique $politique;

	private string $userRole;

	public function __construct(Politique $politique, string $userRole)
	{
		$this->politique = $politique;
		$this->userRole = $userRole;
	}

	/**
	 * @return array{status: int, location?: string, message: string}
	 */
	public function run(bool $multi): array
	{
		if ($this->userRole === Access::ROLE_EDITEUR_NONE || $this->userRole === Access::ROLE_EDITEUR_PENDING) {
			return [
				'status' => 403,
				'message' => "Vous n'avez pas la permission de publier cette politique.",
			];
		}

		$titres = $this->politique->getTitresAssigned();
		if (!$titres) {
			return [
				'status' => 403,
				'message' => "Une politique doit être associée à un titre pour être publiée.",
			];
		}
		if (!$multi && count($titres) > 1) {
			return [
				'status' => 302,
				'location' => "/politique/titres/{$this->politique->id}?mode=transmission",
				'message' => "Cette politique est associée à plusieurs titres, veuillez confirmer que vous voulez publier l'ensemble.",
			];
		}

		// At least one Titre is related, so we can publish the Politique.
		$action = new Workflow($this->politique, $this->userRole);
		if ($action->publish()) {
			$this->politique->save(false);
			if ($this->politique->status === Politique::STATUS_PUBLISHED) {
				self::logChanges($this->politique);
				$this->sendEmail();
				return [
					'status' => 200,
					'message' => "La transmission à Open policy finder est confirmée. Un courriel a été envoyé à l'auteur de la politique.",
				];
			}
			self::logChanges($this->politique);
			return [
				'status' => 200,
				'message' => "La transmission à Open policy finder est enclenchée.",
			];
		}
		return [
			'status' => 403,
			'message' => "Le statut actuel de cette politique ne permet pas sa publication.",
		];
	}

	private function sendEmail(): bool
	{
		$message = Mailer::newMail();
		$message
			->from(Config::read('email.from'))
			->setTo($this->politique->createur->email)
			->subject("[Mir@bel] politique transmise à Open policy finder")
			->text(
				<<<EOTEXT
				Bonjour.

				La politique « {$this->politique->name} » que vous aviez créée
				a été validée par l'équipe de Mir@bel et transmise à Open policy finder.

				Cordialement,
				l'équipe de Mir@bel.
				EOTEXT
			);
		return Mailer::sendMail($message);
	}

	private static function logChanges(Politique $politique): void
	{
		$ids = \Yii::app()->db
			->createCommand("SELECT titreId FROM Politique_Titre WHERE politiqueId = {$politique->id}")
			->queryColumn();
		foreach ($ids as $id) {
			$log = new PolitiqueLog();
			$log->action = (
				$politique->status == Politique::STATUS_PUBLISHED
				? PolitiqueLog::ACTION_PUBLISH
				: PolitiqueLog::ACTION_TOPUBLISH
			);
			$log->politiqueId = (int) $politique->id;
			$log->editeurId = (int) $politique->editeurId;
			$log->titreId = (int) $id;
			$log->publisher_policy = $politique->publisher_policy;
			$log->save(false);
		}
	}
}
