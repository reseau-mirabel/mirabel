<?php

namespace processes\politique;

use CHtml;

class Render
{
	public static function toHtmlBlock(\Cms $block, int $num): string
	{
		$images = [
			'',
			'undraw_add_information_j2wg',
			'undraw_profile_re_4a55',
			'undraw_referral_re_0aji',
			'undraw_scrum_board_re_wk7v',
		];
		$img = <<<EOHTML
			<div style="text-align: center">
			<picture>
				<source type="image/svg+xml" srcset="/images/politiques/{$images[$num]}.svg" />
				<source type="image/png" srcset="/images/politiques/{$images[$num]}.png" />
				<img style="max-width:100%" src="/images/politiques/{$images[$num]}.png" alt="" />
			</picture>
			</div>
			EOHTML;
		$html = preg_replace('#</a>#', " $img</a>", $block->toHtml(), 1);
		return CHtml::tag(
			'div',
			['id' => "bloc$num", 'class' => "politiques-bloc"],
			$html
		);
	}
}
