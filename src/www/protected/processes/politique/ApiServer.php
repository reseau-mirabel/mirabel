<?php

declare(strict_types=1);

namespace processes\politique;

use CHttpException;
use Editeur;
use Issn;
use Politique;
use Titre;

class ApiServer
{
	/**
	 * @var string[] Path in the URL following the main action.
	 */
	private array $pathinfo;

	private array $params;

	private \CDbConnection $db;

	public function __construct(array $pathinfo, array $params)
	{
		$this->pathinfo = $pathinfo;
		$this->params = $params;
		$this->db = \Yii::app()->db;
	}

	public function setDb(\CDbConnection $db): void
	{
		$this->db = $db;
	}

	public function run(string $action): array
	{
		switch ($action) {
			case 'changes':
				$since = $this->getRequestParamSince();
				return $this->actionChanges($since);
			case 'publication':
				return $this->actionPublication($this->getRequestId($action));
			case 'policy':
				return $this->actionPolicy($this->getRequestId($action));
			case 'publisher':
				return $this->actionPublisher($this->getRequestId($action));
			default:
				throw new CHttpException(400, "The path is not valid. The expected format is /politique/api/<action>.");
		}
	}

	public function actionChanges(int $since): array
	{
		return [
			'since' => self::formatDatetime($since),
			'new' => $this->findRelationsNew($since),
			'deleted' => $this->findRelationsDeleted($since),
		];
	}

	public function actionPublication(int $id): array
	{
		$row = $this->db->createCommand("SELECT * FROM Titre WHERE id = :id")->queryRow(true, [':id' => $id]);
		if (!$row) {
			throw new CHttpException(404, "Invalid ID, no matching record was found.");
		}
		$publication = Titre::model()->populateRecord($row);
		assert($publication instanceof Titre);
		$titles = explode(" = ", $publication->titre);



		$publishers = [];
		$te = $this->db
			->createCommand("SELECT te.*, e.role as erole FROM Titre_Editeur te JOIN Editeur e ON te.editeurId = e.id WHERE te.titreId = :id ORDER BY te.editeurId ASC")
			->queryAll(true, [':id' => $publication->id]);
		foreach ($te as $relation) {
			$publishers[] = [
				'publisher_id' => (int) $relation['editeurId'],
				'former' => (bool) $relation['ancien'],
				'role' => self::formatRole($relation['role'], $relation['erole']),
			];
		}

		return [
			'id' => (int) $publication->id,
			'group_id' => (int) $publication->revueId,
			'successor_publication_id' => (int) $publication->obsoletePar,
			'fulltitle' => "{$publication->prefixe}{$titles[0]}",
			'title' => array_map(
				function ($t) {
					return ['title' => $t, 'language' => ''];
				},
				$titles
			),
			'prefix' => $publication->prefixe,
			'shorttitle' => $publication->sigle,
			'start_date' => (string) $publication->dateDebut,
			'end_date' => (string) $publication->dateFin,
			'url' => $publication->url,
			'external_urls' => self::formatExternalLinks($publication->getLiens()),
			'languages' => json_decode($publication->langues),
			'identifiers' => $this->expandTitleIdentifiers($publication),
			'publishers' => $publishers,
			'last_update' => self::formatDatetime((int) $publication->hdateModif),
			'last_check' => self::formatDatetime((int) $publication->revue->hdateVerif),
		];
	}

	public function actionPolicy(int $id): array
	{
		$p = $this->db
			->createCommand("SELECT lastUpdate, publisher_policy FROM Politique WHERE id = :id AND status = :status")
			->queryRow(true, [':id' => $id, ':status' => Politique::STATUS_PUBLISHED]);
		if ($p) {
			$p['lastPublish'] = (int) $this->db
				->createCommand("SELECT actionTime FROM PolitiqueLog WHERE politiqueId = :id AND action = :action ORDER BY id DESC LIMIT 1")
				->queryScalar([':action' => \PolitiqueLog::ACTION_PUBLISH, ':id' => $id]);
		} else {
			$p = $this->db
				->createCommand(
					<<<EOSQL
					SELECT actionTime AS lastPublish, pl.publisher_policy
					FROM PolitiqueLog pl
						JOIN Politique p ON pl.politiqueId = p.id
					WHERE pl.action = :action AND p.id = :id AND p.status <> :status
					ORDER BY pl.actionTime DESC
					LIMIT 1
					EOSQL
				)
				->queryRow(true, [':action' => \PolitiqueLog::ACTION_PUBLISH, ':id' => $id, ':status' => Politique::STATUS_DELETED]);
			if (empty($p)) {
				throw new CHttpException(404, "Invalid ID, no matching record was found.");
			}
		}
		return [
			'id' => $id,
			'last_update' => max((int) ($p['lastPublish'] ?? 0), (int) ($p['lastUpdate'] ?? 0)),
			'content' => json_decode($p['publisher_policy']),
		];
	}

	public function actionPublisher(int $id): array
	{
		$row = $this->db->createCommand("SELECT * FROM Editeur WHERE id = :id")->queryRow(true, [':id' => $id]);
		if (!$row) {
			throw new CHttpException(404, "Invalid ID, no matching record was found.");
		}
		$e = Editeur::model()->populateRecord($row);
		assert($e instanceof Editeur);
		$names = explode(" = ", $e->nom);

		$publications = [];
		$te = $this->db->createCommand("SELECT * FROM Titre_Editeur WHERE editeurId = :id ORDER BY titreId ASC")
			->queryAll(true, [':id' => $e->id]);
		foreach ($te as $relation) {
			$publications[] = [
				'publication_id' => (int) $relation['titreId'],
				'former' => (bool) $relation['ancien'],
				'role' => self::formatRole($relation['role'], $e->role),
			];
		}

		$country = $e->pays;
		return [
			'id' => $id,
			'fullname' => "{$e->prefixe}{$names[0]}",
			'name' => array_map(
				function ($t) {
					return ['name' => $t, 'language' => ''];
				},
				$names
			),
			'prefix' => $e->prefixe,
			'shortname' => $e->sigle,
			'description' => $e->description,
			'location' => [
				'country2' => $e->paysId ? $country->code2 : null,
				'country3' => $e->paysId ? $country->code : null,
				'place' => $e->geo,
			],
			'url' => $e->url,
			'external_urls' => self::formatExternalLinks($e->getLiens()),
			'identifiers' => self::addToIdentifiers([], $e, ['idref', 'sherpa']),
			'publications' => $publications,
			'last_update' => self::formatDatetime((int) $e->hdateModif),
			'last_check' => self::formatDatetime((int) $e->hdateVerif),
		];
	}

	private static function formatRole(?string $relationRole, ?string $publisherRole): string
	{
		static $rolesMap = [];
		if (count($rolesMap) === 0) {
			$config = \Config::read('sherpa.publisher_role');
			if (!$config) {
				$config = [
					['primary_copublisher', "co-éditeur principal"],
					['copublisher', "co-éditeur"],
					['commercial_publisher', "éditeur commercial"],
					['university_publisher', "presses universitaires"],
					['university_publisher', "laboratoire de recherche"],
					['university_publisher', "université"],
					['society_publisher', "société académique"],
					['independent_journal', "collectif indépendant"],
					['governmental_publisher', "organisme gouvernemental"],
					['associate_organisation', "autre"],
				];
			}
			assert(is_array($config));
			foreach ($config as $row) {
				if (is_array($row) && count($row) > 1 && $row[0] && $row[1]) {
					$rolesMap[$row[0]] = $row[1];
				}
			}
		}
		return $rolesMap[$relationRole] ?? $rolesMap[$publisherRole] ?? "";
	}

	private function expandTitleIdentifiers(Titre $publication): array
	{
		$toMedium = [
			Issn::SUPPORT_ELECTRONIQUE => 'online',
			Issn::SUPPORT_INCONNU => '',
			Issn::SUPPORT_PAPIER => 'print',
		];
		$rows = $this->db
			->createCommand("SELECT * FROM Issn WHERE titreId = :tid AND statut = :statut ORDER BY dateDebut ASC, issn ASC")
			->queryAll(true, [':tid' => $publication->id, ':statut' => Issn::STATUT_VALIDE]);
		$records = Issn::model()->populateRecords($rows, false);
		$identifiers = [];
		foreach ($records as $issn) {
			$identifiers[] = array_filter([
				'medium' => $toMedium[$issn->support] ?? "",
				'bnf_ark' => $issn->bnfArk,
				'issn' => (string) $issn->issn,
				'issnl' => (string) $issn->issnl,
				'sudoc_ppn' => $issn->sudocPpn,
				'worldcat_ocn' => $issn->worldcatOcn,
			]);
		}
		return $identifiers;
	}

	private static function addToIdentifiers(array $identifiers, object $source, array $properties): array
	{
		foreach ($properties as $k => $v) {
			if (is_int($k)) {
				$k = $v;
			}
			if (!empty($source->{$k})) {
				$identifiers[] = [
					'name' => $v,
					'value' => $source->{$k},
				];
			}
		}
		return $identifiers;
	}

	private static function formatExternalLinks(\Liens $links): array
	{
		$baseUrl = \Yii::app()->params->itemAt('baseUrl');
		$otherLinks = [];
		foreach ($links as $l) {
			assert($l instanceof \Lien);
			$l->url = str_replace('\\/', '/', $l->url);
			$otherLinks[] = [
				'source' => $l->src,
				'url' => $l->isInternal() ? "{$baseUrl}{$l->url}" : $l->url,
			];
		}
		return $otherLinks;
	}

	/**
	 * List triplets created after $since, even some component has already been deleted.
	 */
	private function findRelationsNew(int $since): array
	{
		$greatestFunc = $this->db->driverName === 'sqlite' ? "MAX" : "GREATEST";
		$cmd = $this->db->createCommand(
			<<<EOSQL
			SELECT
			  pl.titreId AS publication_id,
			  pl.politiqueId AS policy_id,
			  pl.editeurId AS publisher_id,
			  $greatestFunc(IFNULL(pt.createdOn, 0), pl.actionTime) AS last_update
			FROM PolitiqueLog pl
			  JOIN Politique p ON pl.politiqueId = p.id AND p.status IN (:status1, :status2, :status3)
			  JOIN Politique_Titre pt ON pt.titreId = pl.titreId AND pt.politiqueId = pl.politiqueId
			WHERE
			  pl.action = 'publish'
			  AND $greatestFunc(IFNULL(pt.createdOn, 0), pl.actionTime) >= $since
			GROUP BY pl.politiqueId
			ORDER BY last_update DESC
			EOSQL
		);
		$query = $cmd->query([':status1' => Politique::STATUS_PUBLISHED, ':status2' => Politique::STATUS_UPDATED, ':status3' => Politique::STATUS_TOPUBLISH]);
		$result = [];
		foreach ($query as $row) {
			$result[] = [
				'publication_id' => (int) $row['publication_id'],
				'policy_id' => (int) $row['policy_id'],
				'publisher_id' => (int) $row['publisher_id'],
				'last_update' => self::formatDatetime((int) $row['last_update']),
			];
		}
		return $result;
	}

	private function findRelationsDeleted(int $since): array
	{
		$cmd = $this->db->createCommand(
			<<<EOSQL
			SELECT titreId AS publication_id, editeurId as publisher_id, politiqueId AS policy_id, actionTime AS last_update
			FROM PolitiqueLog
			WHERE action = :a AND actionTime >= :since
			ORDER BY id ASC
			EOSQL
		);
		$result = [];
		foreach ($cmd->query([':a' => \PolitiqueLog::ACTION_DELETE, ':since' => $since]) as $row) {
			$result[] = [
				'publication_id' => (int) $row['publication_id'],
				'policy_id' => (int) $row['policy_id'],
				'publisher_id' => (int) $row['publisher_id'],
				'last_update' => self::formatDatetime((int) $row['last_update']),
			];
		}
		return $result;
	}

	private function getRequestId(string $action): int
	{
		if (count($this->pathinfo) === 0) {
			throw new CHttpException(400, "No ID was given in the path.");
		}
		if (count($this->pathinfo) === 1) {
			$id = (int) $this->pathinfo[0];
			if ($id === 0) {
				throw new CHttpException(400, "The ID must be a positive integer pointing to an existing record.");
			}
			return $id;
		}
		if (count($this->pathinfo) === 2 && $this->pathinfo[0] === 'issn') {
			$id = 0;
			if ($action === 'publication') {
				$id = (int) $this->db
					->createCommand("SELECT titreId FROM Issn WHERE issn = :issn LIMIT 1")
					->queryScalar([':issn' => $this->pathinfo[1]]);
			} elseif ($action === 'policy') {
				$id = (int) $this->db
					->createCommand("SELECT pt.politiqueId FROM Issn i JOIN Politique_Titre pt USING(titreId) WHERE i.issn = :issn LIMIT 1")
					->queryScalar([':issn' => $this->pathinfo[1]]);
			} elseif ($action === 'publisher') {
				$id = (int) $this->db
					->createCommand("SELECT p.editeurId FROM Issn i JOIN Politique_Titre pt USING(titreId) JOIN Politique p ON p.id = pt.politiqueId WHERE i.issn = :issn LIMIT 1")
					->queryScalar([':issn' => $this->pathinfo[1]]);
			}
			if ($id === 0) {
				throw new CHttpException(400, "This ISSN was not found in Mir@bel's data.");
			}
			return $id;
		}
		throw new CHttpException(400, "No ID was given in the path.");
	}

	private function getRequestParamSince(): int
	{
		if (empty($this->params['since'])) {
			return 0;
		}
		$raw = $this->params['since'];
		if (ctype_digit((string) $raw)) {
			return (int) $raw;
		}
		$date = \DateTimeImmutable::createFromFormat(\DateTimeInterface::ATOM, str_replace(' ', '+', $raw));
		if ($date === false) {
			throw new CHttpException(400, "Wrong 'since' parameter. ISO8601 or UNIX timestamp expected");
		}
		return (int) $date->getTimestamp();
	}

	private static function formatDatetime(int $timestamp): string
	{
		if ($timestamp) {
			return date(\DateTimeInterface::ATOM, $timestamp);
		}
		return "";
	}
}
