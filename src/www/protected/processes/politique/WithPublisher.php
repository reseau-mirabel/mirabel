<?php

namespace processes\politique;

use components\SqlHelper;
use CHttpException;
use Editeur;

trait WithPublisher
{
	public ?Editeur $editeur;

	/**
	 * @var Editeur[]
	 */
	public array $editeurList = [];

	public int $userId = 0;

	private string $editeurUserRole = '';

	public function getRoleOnPublisher(): string
	{
		if ($this->editeur === null) {
			return '';
		}
		if ($this->editeurUserRole === '') {
			$roles = SqlHelper::sqlToPairs(
				"SELECT editeurId, IF(confirmed, role, 'pending') FROM Utilisateur_Editeur WHERE utilisateurId = {$this->userId}"
			);
			if (!isset($roles[$this->editeur->id])) {
				if (\Yii::app()->user->access()->hasPermission('politiques')) {
					$roles[$this->editeur->id] = 'owner';
				} else {
					throw new CHttpException(403, "Vous n'avez pas accès à cet éditeur.");
				}
			}
			$this->editeurUserRole = $roles[$this->editeur->id];
		}
		return $this->editeurUserRole;
	}

	protected function setEditeurById(int $id): bool
	{
		if ($id > 0) {
			if (!isset($this->editeurList[$id])) {
				// No specific permission on this publisher...
				if (\Yii::app()->user->access()->hasPermission('politiques')) { // but a global permission?
					$e = Editeur::model()->findByPk($id);
					if ($e instanceof Editeur) {
						$this->editeurList[$id] = $e;
					}
				} elseif (!empty($this->editeurList)) { // or another publisher?
					$id = 0;
				} else {
					throw new CHttpException(403, "Vous n'avez pas accès à cet éditeur.");
				}
			}
			if ($id) {
				$this->editeur = Editeur::model()->findByPk($id);
				if (!$this->editeur) {
					throw new CHttpException(404, "Cet identifiant ne correspond à aucun éditeur de Mir@bel.");
				}
				return true;
			}
		}
		if (count($this->editeurList) === 1) {
			$this->editeur = reset($this->editeurList);
		} else {
			$this->editeur = null;
		}
		return !empty($this->editeur);
	}

	protected function initPublisherList(): void
	{
		$this->editeurList = SqlHelper::sqlToPairsObject(
			"SELECT e.* FROM Editeur e JOIN Utilisateur_Editeur ue ON ue.editeurId = e.id WHERE ue.utilisateurId = {$this->userId} ORDER BY e.nom",
			'Editeur'
		);
	}
}
