<?php

namespace processes\politique;

use Politique;

/**
 * Transitions d'état.
 */
class Workflow
{
	private const ROLES = ["pending", "guest", "owner", "admin"];

	private Politique $politique;

	/**
	 * @var ""|"pending"|"guest"|"owner"|"admin"
	 */
	private string $role = '';

	public function __construct(Politique $p, string $role)
	{
		$this->politique = $p;
		$this->setRole($role);
	}

	public function setRole(string $role): void
	{
		if (!in_array($role, self::ROLES)) {
			throw new \Exception("Rôle non-valide.");
		}
		$this->role = $role;
	}

	public function create(): bool
	{
		if ($this->role === 'pending') {
			$this->politique->status = Politique::STATUS_PENDING;
		} else {
			$this->politique->status = Politique::STATUS_DRAFT;
		}
		return true;
	}

	public function delete(): bool
	{
		$politiqueStatus = $this->politique->status;
		switch ($this->role) {
			case 'admin':
				$this->politique->status = Politique::STATUS_DELETED;
				return true;
			case 'pending':
				if ($politiqueStatus === Politique::STATUS_PENDING) {
					$this->politique->status = Politique::STATUS_DELETED;
					return true;
				}
				return false;
			default:
				if (in_array($politiqueStatus, [Politique::STATUS_PENDING, Politique::STATUS_DRAFT])) {
					// A non-admin user can only directly delete a draft.
					$this->politique->status = Politique::STATUS_DELETED;
				} else {
					$this->politique->status = Politique::STATUS_TODELETE;
				}
		}
		return true;
	}

	public function update(): bool
	{
		if ($this->role === 'pending') {
			if ($this->politique->status === Politique::STATUS_PENDING) {
				return true;
			}
			return false;
		}
		if ($this->role === 'admin') {
			if ($this->politique->status === Politique::STATUS_PUBLISHED) {
				$this->politique->status = Politique::STATUS_UPDATED;
			}
			return true;
		}
		// role "guest"|"owner"
		if ($this->politique->status === Politique::STATUS_DRAFT || $this->politique->status === Politique::STATUS_PENDING) {
			// Same status.
			return true;
		}
		$this->politique->status = Politique::STATUS_UPDATED;
		return true;
	}

	public function confirmUser(): bool
	{
		if ($this->role == 'admin') {
			if ($this->politique->status === Politique::STATUS_PENDING) {
				$this->politique->status = Politique::STATUS_DRAFT;
				return true;
			}
		}
		return false;
	}

	public function publish(): bool
	{
		if ($this->role === 'pending') {
			return false;
		}
		if ($this->politique->status === Politique::STATUS_DRAFT || $this->politique->status === Politique::STATUS_UPDATED) {
			// role: "admin" | "guest" | "owner"
			$this->politique->status = Politique::STATUS_TOPUBLISH;
			return true;
		}
		if ($this->role === 'admin' && $this->politique->status === Politique::STATUS_TOPUBLISH) {
			$this->politique->status = Politique::STATUS_PUBLISHED;
			return true;
		}
		return false;
	}

	public function reject(): bool
	{
		if ($this->role !== 'admin') {
			return false;
		}
		if ($this->politique->status !== Politique::STATUS_TOPUBLISH) {
			return false;
		}
		$lastAction = (string) \Yii::app()->db
			->createCommand("SELECT action FROM PolitiqueLog WHERE politiqueId = :id AND action = :a ORDER BY id DESC LIMIT 1")
			->queryScalar([':id' => $this->politique->id, ':a' => \PolitiqueLog::ACTION_UPDATE]);
		if ($lastAction === \PolitiqueLog::ACTION_UPDATE) {
			$previousStatus = Politique::STATUS_UPDATED;
		} else {
			$previousStatus = Politique::STATUS_DRAFT;
		}
		$this->politique->status = $previousStatus;
		return true;
	}
}
