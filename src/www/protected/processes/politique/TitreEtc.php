<?php

namespace processes\politique;

use Politique;
use Titre;
use TitreEditeur;

class TitreEtc
{
	public ?Politique $politique;

	public ?bool $politiqueConfirmed;

	public Titre $titre;

	public TitreEditeur $titreEditeur;

	/**
	 * @var int How many active links apply to this Politique?
	 */
	public int $numLinks = 0;

	public function __construct(Titre $titre, ?Politique $p, ?bool $c, int $num)
	{
		$this->titre = $titre;
		$this->politique = $p;
		$this->politiqueConfirmed = $c;
		$this->numLinks = $num;
	}

	public function getLabel(): string
	{
		if (!$this->politique) {
			return '';
		}
		$pending = $this->politique->status === Politique::STATUS_PENDING;
		return \CHtml::link(
			\CHtml::tag(
				'span',
				['class' => "label label-success", 'title' => $pending ? "en attente de validation" : null],
				\CHtml::encode($this->politique->name)
					. ($pending ? ' <span class="glyphicon glyphicon-exclamation-sign"></span>' : '')
			),
			['/politique/update', 'id' => $this->politique->id]
		);
	}

	public function getActions(): string
	{
		return $this->buttonUpdate()
			. $this->buttonDelete()
			. $this->buttonPublish();
	}

	private function buttonUpdate(): string
	{
		if (!$this->politique) {
			return '';
		}
		return \CHtml::link(
			'<span class="glyphicon glyphicon-pencil"></span>',
			['/politique/update', 'id' => $this->politique->id],
			[
				'class' => 'btn btn-small btn-default',
				'title' => "Voir et modifier cette politique",
			]
		);
	}

	private function buttonDelete(): string
	{
		if (!$this->politique || !\Yii::app()->user->access()->toPolitique()->update($this->politique)) {
			return '';
		}
		return '<div style="display: inline-block">'
			. \components\HtmlHelper::postButton(
				'<span class="glyphicon glyphicon-trash" style="color:red"></span>',
				['/politique/unlink', 'titreId' => $this->titre->id],
				[],
				[
					'class' => 'btn btn-small btn-default',
					'onclick' => "return confirm('Êtes-vous certain de vouloir retirer la politique de ce titre ?')",
					'title' => $this->numLinks > 1 ? "Détacher cette politique" : "Supprimer cette politique",
				]
			)
			. '</div>';
	}

	private function buttonPublish(): string
	{
		if (!$this->politique) {
			return '';
		}
		if ($this->politique->status === \Politique::STATUS_PUBLISHED) {
			return '<span><span class="glyphicon glyphicon-share-alt" title="Politique transmise à Open policy finder (ex Sherpa Romeo)" /><img src="/images/liens/openpolicyfinder.png" alt="Open policy Finder" /></span>';
		}
		$permission = \Yii::app()->user->access()->toPolitique()->publish($this->politique);
		$button = \CHtml::htmlButton(
			'<span class="glyphicon glyphicon-share"></span>',
			[
				'type' => "button",
				'disabled' => !$permission,
				'class' => "btn btn-small btn-default policy-publish",
				'title' => "Transmettre la politique à Open policy finder",
				'data-politiqueid' => $this->politique->id,
			]
		);
		if ($this->politique->status === \Politique::STATUS_TOPUBLISH) {
			$icons = '<span class="glyphicon glyphicon-share-alt" style="color: #AAA" title="Politique en cours de transmission à Open policy finder" /><img src="/images/liens/openpolicyfinder.png" alt="Open policy finder" />';
			if ($permission) {
				return "<span>$icons$button</span>";
			}
			return "<span>$icons</span>";
		}
		return $button;
	}
}
