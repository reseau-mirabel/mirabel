<?php

namespace processes\politique;

use Politique;
use components\SqlHelper;

class Index
{
	use WithPublisher;

	/**
	 * @var Politique[]
	 */
	public array $politiques = [];

	/**
	 * @var TitreEtc[] Titres de l'éditeur, avec leur éventuelle politique et le statut confirmé
	 */
	public array $titresActuels = [];

	/**
	 * @var TitreEtc[]
	 */
	public array $titresAnciens = [];

	private array $cache = [];

	public function __construct(int $userId)
	{
		$this->userId = $userId;
		$this->initPublisherList();
	}

	public function countMissingCharacterizations(): int
	{
		$count = 0;
		foreach ($this->titresActuels as $t) {
			if (!$t->titreEditeur->isComplete()) {
				$count++;
			}
		}
		foreach ($this->titresAnciens as $t) {
			if (!$t->titreEditeur->isComplete()) {
				$count++;
			}
		}
		return $count;
	}

	public function hasManyJournals(): bool
	{
		if ($this->editeur === null) {
			return false;
		}
		$count = (int) \Yii::app()->db
			->createCommand(
				<<<EOSQL
				SELECT count(*)
				FROM Titre_Editeur te JOIN Titre t ON t.id = te.titreId
				WHERE (te.ancien IS NULL OR te.ancien = 0) AND (t.dateFin = '' OR dateFin IS NULL) AND te.editeurId = :eid
				EOSQL
			)
			->queryScalar([':eid' => $this->editeur->id]);
		return $count >= \Config::read('politique.seuil-affichage');
	}

	public function setEditeur(int $editeurId): void
	{
		if (!$this->setEditeurById($editeurId)) {
			$this->politiques = [];
			$this->titresActuels = [];
			$this->titresAnciens = [];
			return;
		}

		$this->politiques = Politique::model()->findAllByAttributes(['editeurId' => $this->editeur->id]);
		$this->fillTitres();
	}

	private function fillTitres(): void
	{
		$this->fillTitresAssigned();
		$this->fillTitresPending();

		$sorter = function ($t1, $t2) {
			return strcmp($t1->titre->titre, $t2->titre->titre);
		};
		uasort($this->titresActuels, $sorter);
		uasort($this->titresAnciens, $sorter);

		foreach ($this->editeur->titreEditeurs as $te) {
			if (isset($this->titresActuels[$te->titreId])) {
				$this->titresActuels[$te->titreId]->titreEditeur = $te;
			} elseif (isset($this->titresAnciens[$te->titreId])) {
				$this->titresAnciens[$te->titreId]->titreEditeur = $te;
			} else {
				\Yii::log("Lien titre-éditeur ({$te->titreId}, {$te->editeurId}) sans rattachement", 'warning');
			}
		}
	}

	/**
	 * Fill $this->titres* with Editeur.titres and their assigned Politique records.
	 */
	private function fillTitresAssigned(): void
	{
		$politiquesTitres = \Yii::app()->db
			->createCommand(
				<<<EOSQL
				SELECT
					p.*,
					pt.confirmed,
					te.titreId,
					count(*) OVER (PARTITION BY p.id) AS numLinks
				FROM
					Titre_Editeur te
					LEFT JOIN Politique_Titre pt USING(titreId)
					LEFT JOIN Politique p ON p.id = pt.politiqueId AND p.status NOT IN (:s1, :s2)
				WHERE
					te.editeurId = :eid
				EOSQL
			)
			->query([
				':eid' => $this->editeur->id,
				':s1' => Politique::STATUS_PENDING,
				':s2' => Politique::STATUS_DELETED,
			]);
		foreach ($politiquesTitres as $pt) {
			$titreId = (int) $pt['titreId'];
			if ($pt['id'] === null) {
				// Titre linked to the Editeur, without any active Politique
				$this->addTitre($titreId, null, null, 0);
				continue;
			}
			// Titre linked to the Editeur, with an active Politique
			$confirmed = (bool) $pt['confirmed'];
			$numLinks = (int) $pt['numLinks'];
			unset($pt['confirmed'], $pt['titreId'], $pt['numLinks']);
			$this->addTitre(
				$titreId,
				Politique::model()->populateRecord($pt, false),
				$confirmed,
				$numLinks,
			);
		}
	}

	/**
	 * Complete $this->titres* using Politique candidates (by Politique.initialTitreId)
	 */
	private function fillTitresPending(): void
	{
		foreach ($this->politiques as $p) {
			$tid = $p->initialTitreId;
			if (!$tid || $p->status !== \Politique::STATUS_PENDING) {
				continue;
			}
			$this->addTitre($tid, $p, false, 0);
		}
	}

	private function addTitre(int $titreId, ?\Politique $p, ?bool $c, int $num): void
	{
		if (!$this->cache) {
			$this->cache['titresActuels'] = SqlHelper::sqlToPairsObject(
				<<<EOSQL
				SELECT t.*
				FROM Titre t JOIN Titre_Editeur te ON te.titreId = t.id
				WHERE (te.ancien IS NULL OR te.ancien = 0) AND (t.dateFin = '' OR dateFin IS NULL) AND te.editeurId = {$this->editeur->id}
				ORDER BY t.titre
				EOSQL,
				'Titre'
			);
			$this->cache['titresAnciens'] = SqlHelper::sqlToPairsObject(
				<<<EOSQL
				SELECT t.*
				FROM Titre t JOIN Titre_Editeur te ON te.titreId = t.id
				WHERE (te.ancien = 1 OR t.dateFin <> '') AND te.editeurId = {$this->editeur->id}
				ORDER BY t.titre
				EOSQL,
				'Titre'
			);
		}

		if (isset($this->cache['titresActuels'][$titreId])) {
			if (isset($this->titresActuels[$titreId]->politique)) {
				return;
			}
			$this->titresActuels[$titreId] = new TitreEtc(
				$this->cache['titresActuels'][$titreId],
				$p,
				$c,
				$num,
			);
		} elseif (isset($this->cache['titresAnciens'][$titreId])) {
			if (isset($this->titresAnciens[$titreId]->politique)) {
				return;
			}
			$this->titresAnciens[$titreId] = new TitreEtc(
				$this->cache['titresAnciens'][$titreId],
				$p,
				$c,
				$num,
			);
		} else {
			\Yii::log("Titre $titreId non identifié pour l'éditeur {$this->editeur->id}", 'warning');
		}
	}
}
