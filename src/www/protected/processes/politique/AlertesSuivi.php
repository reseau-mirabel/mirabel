<?php

namespace processes\politique;

use Yii;

class AlertesSuivi
{
	public int $demandesPolitiques;

	public int $demandesUtilisateurs;

	public function __construct()
	{
		$this->demandesPolitiques = self::countPendingPolicies();
		$this->demandesUtilisateurs = self::countPendingRegistrations();
	}

	public function isEmpty(): bool
	{
		return $this->demandesPolitiques === 0 && $this->demandesUtilisateurs === 0;
	}

	private static function countPendingPolicies(): int
	{
		return (int) Yii::app()->db
			->createCommand("SELECT count(*) FROM Politique WHERE status = :s")
			->queryScalar([':s' => \Politique::STATUS_TOPUBLISH]);
	}

	private static function countPendingRegistrations(): int
	{
		return (int) Yii::app()->db
			->createCommand("SELECT count(*) FROM Utilisateur_Editeur WHERE confirmed = 0")
			->queryScalar();
	}
}
