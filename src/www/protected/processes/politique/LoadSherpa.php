<?php

namespace processes\politique;

use CHttpException;
use Politique;
use Titre;
use models\sherpa\Item;

class LoadSherpa
{
	use WithPublisher;

	private ?Titre $titre;

	private int $sherpaPolicyId = 0;

	public function __construct(int $userId)
	{
		$this->userId = $userId;
		$this->initPublisherList();
	}

	public function applyPolicyToJournal(Politique $politique): bool
	{
		return \Yii::app()->db
			->createCommand("INSERT INTO Politique_Titre (politiqueId, titreId, confirmed, createdOn) VALUES (?, ?, 0, ?)")
			->execute([$politique->id, $this->titre->id, time()]) === 1;
	}

	public function createPolitique(): Politique
	{
		if (!$this->titre || !$this->editeur) {
			throw new CHttpException(404, "Données incohérentes.");
		}

		$item = Item::loadFromCache($this->titre->id);
		if ($item === null) {
			throw new CHttpException(500, "Les données de “Open policy finder” sont absentes (trop récentes ?). Veuillez réessayer demain, puis contacter éventuellement un administrateur.");
		}
		$this->sherpaPolicyId = $item->publisher_policy[0]->id;
		$policy = self::cleanupSherpaPolicy($item->publisher_policy[0]);

		$politique = new Politique();
		$politique->editeurId = (int) $this->editeur->id;
		$politique->utilisateurId = (int) $this->userId;
		$politique->initialTitreId = (int) $this->titre->id;
		$politique->name = ($policy->internal_moniker === "Default Policy" ? "Politique de {$this->titre->titre}" : $policy->internal_moniker);
		$politique->lastUpdate = null;
		$politique->model = null;
		$politique->publisher_policy = json_encode($policy);
		return $politique;
	}

	/**
	 * List all the Titre.id that should have that same policy.
	 *
	 * @return int[]
	 */
	public function getTitleIds(): array
	{
		if (!$this->sherpaPolicyId || $this->editeur === null) {
			return [];
		}
		$result = [];
		$candidates = \Yii::app()->db
			->createCommand(<<<EOSQL
				SELECT sp.titreId, sp.content
				FROM Titre_Editeur te JOIN SherpaPublication sp ON sp.titreId = te.titreId
				WHERE te.editeurId = :eid
				EOSQL
			)
			->queryAll(true, [':eid' => $this->editeur->id]);
		foreach ($candidates as $row) {
			$data = json_decode($row['content'], true);
			if ($data['publisher_policy'][0]['id'] === $this->sherpaPolicyId) {
				$result[] = (int) $row['titreId'];
			}
		}
		return $result;
	}

	public function setEditeur(int $editeurId): void
	{
		$this->setEditeurById($editeurId);
	}

	public function setTitre(int $titreId): void
	{
		$this->titre = Titre::model()->findByPk($titreId);
		if (!$this->titre) {
			throw new CHttpException(404, "Ce titre n'a pas été trouvé.");
		}
		if (!$this->titre->getLiens()->containsSource(31)) { // 31 = Open policy finder
			throw new CHttpException(404, "Ce titre n'a pas de données sur sa politique de publication (source Open policy finder).");
		}
		$item = Item::loadFromCache((int) $this->titre->id);
		if (!$item) {
			throw new CHttpException(404, "La politique “Open policy finder” de ce titre n'est pas disponible. Contactez l'équipe Mir@bel ou réessayez la semaine prochaine.");
		}
	}

	public static function cleanupSherpaPolicy(\stdClass $policy): \stdClass
	{
		$rootKeys = [
			"permitted_oa",
			"internal_moniker",
			"open_access_prohibited",
			"urls",
		];
		$valid = [
			"additional_oa_fee" => true,
			"article_version" => true,
			"conditions" => true,
			"copyright_owner" => true,
			"embargo" => [
				"amount" => true,
				"units" => true,
			],
			"license" => [
				"license" => true,
				"version" => true,
			],
			"location" => [
				"location" => true,
				"named_repository" => true,
			],
			"prerequisites" => true,
			"public_notes" => true,
		];
		foreach ((array) $policy as $k => $v) {
			if ($k === "permitted_oa") {
				if (empty($v)) {
					unset($policy->permitted_oa);
				} else {
					$tmp = [];
					foreach ($v as $oa) {
						$tmp[] = self::filterSherpaItem($oa, $valid);
					}
					$policy->permitted_oa = array_filter($tmp);
				}
			} elseif (!in_array($k, $rootKeys)) {
				unset($policy->{$k});
			}
		}
		return $policy;
	}

	private static function filterSherpaItem($data, $valid): ?\stdClass
	{
		if ($valid === true) {
			return $data;
		}
		if (!($data instanceof \stdClass)) {
			return null;
		}
		foreach ((array) $data as $k => $v) {
			if (isset($valid[$k])) {
				if ($valid[$k] === true) {
					continue;
				}
				if (is_array($v) && isset($v[0])) {
					// numerical array
					$value = [];
					foreach ($v as $x) {
						$value[] = self::filterSherpaItem($x, $valid[$k]);
					}
				} else {
					// scalar or object
					$value = self::filterSherpaItem($v, $valid[$k]);
				}
				if ($value === null) {
					unset($data->{$k});
				} else {
					$data->{$k} = $value;
				}
			} else {
				unset($data->{$k});
			}
		}
		return $data;
	}
}
