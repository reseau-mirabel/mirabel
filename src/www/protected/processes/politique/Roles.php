<?php

namespace processes\politique;

use CHttpException;
use components\SqlHelper;
use Editeur;
use Intervention;

class Roles
{
	public Editeur $editeur;

	/**
	 * @var \TitreEditeur[]
	 */
	public array $roles = [];

	/**
	 * @var \Titre[]
	 */
	public array $titres = [];

	private int $userId;

	public function __construct(int $userId, int $editeurId)
	{
		$this->userId = $userId;
		$this->editeur = Editeur::model()->findByPk($editeurId);
		if (!($this->editeur instanceof Editeur)) {
			throw new CHttpException(404, "Aucun éditeur de Mir@bel n'a cet identifiant.");
		}
		if ($this->getUserRole() === '') {
			throw new CHttpException(403, "Vous n'avez pas la permission d'agir sur cet éditeur.");
		}

		$this->roles = SqlHelper::sqlToPairsObject(
			"SELECT * FROM Titre_Editeur WHERE editeurId = {$this->editeur->id}",
			'TitreEditeur',
			'titreId'
		);
		$this->titres = SqlHelper::sqlToPairsObject(
			"SELECT t.* FROM Titre t JOIN Titre_Editeur te ON te.titreId = t.id WHERE editeurId = {$this->editeur->id} ORDER BY t.titre",
			'Titre'
		);
	}

	public function createIntervention(array $post): ?Intervention
	{
		$i = new Intervention();
		$i->action = 'editeur-U';
		$i->description = "Rôles de l'éditeur sur ses titres";
		$i->editeurId = $this->editeur->id;
		$i->import = 0;
		$i->utilisateurIdProp = $this->userId;
		$i->statut = 'attente';
		$i->contenuJson = new \InterventionDetail();

		$valid = true;
		foreach ($this->roles as $te) {
			if (isset($post[$te->titreId])) {
				$old = clone $te;
				$te->setAttributes($post[$te->titreId]);
				if ($old->attributes === $te->attributes) {
					continue;
				}
				if ($te->validate()) {
					$i->contenuJson->update($old, $te);
				} else {
					$valid = false;
				}
			}
		}
		if (!$valid) {
			return null;
		}
		return $i;
	}

	public function getUserRole(): string
	{
		return (string) \Yii::app()->db
			->createCommand("SELECT role FROM Utilisateur_Editeur WHERE utilisateurId = {$this->userId} AND editeurId = {$this->editeur->id} LIMIT 1")
			->queryScalar();
	}
}
