<?php

namespace processes\cron;

use processes\cron\ar\Task;

/**
 * Manage cron locks and history.
 */
class CronState
{
	private const KEEP_OLD_RECORDS = 33;

	private \CDbConnection $db;

	public function __construct()
	{
		$this->db = \Yii::app()->db;
	}

	public function endForcedRun(Task $task): void
	{
		$this->db->createCommand("UPDATE CronTask SET forceRun = 0 WHERE id = {$task->id}")->execute();
	}

	public function hasLock(Task $task): bool
	{
		$row = $this->db
			->createCommand("SELECT id, crontaskId, startTime, pid FROM CronLock WHERE crontaskId = {$task->id}")
			->queryRow();
		if (!$row) {
			return false;
		}
		$pid = (int) $row['pid'];
		if ($pid > 0) {
			if (!file_exists("/proc/$pid")) {
				// The process ended without releasing the lock.
				\Yii::log(
					"The process started at timestamp {$row['startTime']} for Task.id={$row['crontaskId']} ended without releasing the lock.",
					'warning',
					'cron'
				);
				$this->db
					->createCommand("DELETE FROM CronLock WHERE id = {$row['id']}")
					->execute();
				return false;
			}
			return true;
		}
		// pid <= 0 should not happen except for legacy locks
		if ($row['startTime'] < time() - 3*86400) {
			// Started more than 3 days ago
			$this->db
				->createCommand("DELETE FROM CronLock WHERE id = {$row['id']}")
				->execute();
			return false;
		}
		return true;
	}

	/**
	 * Returns true if the pid cannot be found on the system.
	 *
	 * It means the task has crashed without releasing the lock.
	 */
	public static function hasCrashed(Task $task): bool
	{
		$pid = (int) \Yii::app()->db
			->createCommand("SELECT pid FROM CronLock WHERE crontaskId = {$task->id}")
			->queryScalar();
		if ($pid === 0) {
			return false;
		}
		return !file_exists("/proc/$pid");
	}

	public function lockStart(Task $task, int $startTime): int
	{
		$this->db
			->createCommand(<<<EOSQL
				INSERT INTO CronLock
				(crontaskId, identifier, startTime, pid)
				VALUES (:id, :unique, :start, :pid)
				EOSQL
			)
			->execute([
				':id' => $task->id,
				':unique' => $task->getTaskInstance()->getUniqueIdentifer(),
				':start' => $startTime,
				':pid' => posix_getpid(),
			]);
		return (int) $this->db->getLastInsertID();
	}

	public function lockEnd(Task $task): void
	{
		$this->db->createCommand("DELETE FROM CronLock WHERE crontaskId = {$task->id}")->execute();
	}

	public function writeHistory(Task $task, int $startTime, Report $report): void
	{
		$emailContent = "";
		if ($report->sendEmail) {
			$emailContent = $report->emailBody;
		}
		$this->db
			->createCommand(<<<EOSQL
				INSERT INTO
				CronHistory (crontaskId, config, startTime, endTime, emailTo, reportTitle, reportBody, reportFormat, reportEmailBody, error)
				VALUES (:id, :config, :start, :end, :emailTo, :title, :body, :format, :emailBody, :error)
				EOSQL
			)
			->execute([
				':id' => $task->id,
				':config' => $task->config,
				':start' => $startTime,
				':end' => time(),
				':emailTo' => $task->emailTo,
				':title' => trim($report->title),
				':body' => $report->body,
				':format' => $report->format,
				':emailBody' => $emailContent,
				':error' => $report->error,
			]);
		$this->cleanupHistory($task->id);
	}

	private function cleanupHistory(int $taskId): void
	{
		$ids = $this->db
			->createCommand("SELECT id FROM CronHistory WHERE crontaskId = {$taskId} ORDER BY id DESC LIMIT " . self::KEEP_OLD_RECORDS)
			->queryColumn();
		if (count($ids) === self::KEEP_OLD_RECORDS) {
			$this->db
				->createCommand("DELETE FROM CronHistory WHERE crontaskId = {$taskId} AND id < :minId")
				->execute([':minId' => $ids[self::KEEP_OLD_RECORDS - 1]]);
		}
	}
}
