<?php

namespace processes\cron;

use processes\cron\tasks;
use Yii;

/**
 * Convert from an external (system) cron to an internal (Mirabel) cron.
 */
class SystemConverter
{
	public int $verbose = 1;

	public function extractAttributesFromLine(string $row): array
	{
		if ($row === "" || $row[0] === '#') {
			return [];
		}

		$parts = preg_split('/\s+/', $row, 6);
		if (count($parts) < 6) {
			if ($this->verbose > 1) {
				echo "NOT A CRON COMMAND: {$row}\n";
			}
			return [];
		}
		$m = [];
		if (!preg_match('#\byii\s+(\w+)\s+(\w+)\b#', $parts[5], $m)) {
			if ($this->verbose > 1) {
				echo "NOT A YII COMMAND: {$parts[5]}\n";
			}
			return [];
		}
		$action = lcfirst($m[1]) . "/" . lcfirst($m[2]);

		switch ($action) {
			case "attribut/import":
				$result = $this->extractCmdAttributImport($parts[5]);
				break;
			case "attribut/unknown":
				$result = $this->extractCmdAttributUnknown($parts[5]);
				break;
			case "import/bmj":
			case "import/cairn":
			case "import/cairnMagazine":
				$name = ucfirst(substr($action, 7));
				$result = $this->extractCmdImportOther($name);
				break;
			case "import/delete":
				$result = $this->extractCmdImportDelete($parts[5]);
				break;
			case "import/kbart":
				$result = $this->extractCmdImportKbart($parts[5]);
				break;
			case "verify/linksCms":
			case "verify/linksEditeurs":
			case "verify/linksRessources":
			case "verify/linksRevues":
				$result = $this->extractCmdVerifyLinks(substr($action, 12), $parts[5]);
				break;
			case "wikidata/cronFetchCompare":
				return [];
			case "alert/all":
			case "alert/suivi":
			case "alert/nonSuivi":
			case "alert/partenairesEditeurs":
				$result = $this->extractCmdAlertAll(substr($action, 6), $parts[5]);
				break;
			default:
				if (preg_match('#^links/(\w+)$#', $action, $m)) {
					$result = $this->extractCmdLinks($m[1], $parts[5]);
				} else {
					$result = [];
				}
		}
		if (!$result) {
			if ($this->verbose > 0) {
				echo "NO MAPPING FOR: {$action}\n";
			}
			return [];
		}

		$result['schedule'] = json_encode($this->extractSchedule($parts));

		$email = self::extractEmail($parts[5]);
		$result = array_merge($result, $email);
		if ($email['emailTo'] === '' && $this->verbose > 1) {
			echo "NO EMAIL FOR: {$parts[5]}\n";
		}

		return $result;
	}

	private function extractCmdAttributImport(string $cmd): array
	{
		$parsed = [];
		$m = [];
		if (preg_match('/ --uri=(\S+) /', $cmd, $m)) {
			$parsed['url'] = trim($m[1], '"\'');
		} else {
			throw new \Exception("attribut/import has no --url=");
		}
		if (preg_match('/ --config=\'({.+?})\' /', $cmd, $m)) {
			$parsed['config'] = $m[1];
		} else {
			throw new \Exception("attribut/import has no --config='{...}'");
		}
		$importConfig = json_decode($parsed['config'], false);
		return [
			'name' => "Attribut " . Yii::app()->db->createCommand("SELECT nom FROM Sourceattribut WHERE id = {$importConfig->sourceattributId}")->queryScalar(),
			'fqdn' => '\\' . tasks\ModelAttributImport::class,
			'config' => json_encode($parsed, JSON_UNESCAPED_SLASHES),
		];
	}

	private function extractCmdAttributUnknown(string $cmd): array
	{
		$identifiant = "";
		$m = [];
		if (preg_match('/ --identifiant=(\S+?)(?: |\||$)/', $cmd, $m)) {
			$identifiant = trim($m[1], '"\'');
		} else {
			throw new \Exception("attribut/unknown has no --identifiant=");
		}
		$attributeName = Yii::app()->db
			->createCommand("SELECT nom FROM Sourceattribut WHERE identifiant = :i")
			->queryScalar([':i' => $identifiant]);
		return [
			'name' => "Attribut $attributeName : titres manquants dans Mir@bel",
			'fqdn' => '\\' . tasks\ModelAttributTitresInconnus::class,
			'config' => json_encode(['identifiant' => $identifiant], JSON_UNESCAPED_SLASHES),
		];
	}

	private function extractCmdImportKbart(string $command): array
	{
		$cmd = str_replace('$(date +"\%Y-\%m-\%d")', '{{TODAY}}', $command);
		$config = [];
		$m = [];
		if (preg_match('/ --collection=(\d+)\b/', $cmd, $m)) {
			$data = Yii::app()->db
				->createCommand("SELECT c.ressourceId, CONCAT(r.nom, ' [', c.nom, ']') AS nom FROM Collection c JOIN Ressource r ON c.ressourceId = r.id WHERE c.id = {$m[1]}")
				->queryRow();
			$config = [
				'ressourceId' => (int) $data['ressourceId'],
				'collectionIds' => $m[1],
			];
			$name = "KBART {$data['nom']}";
		} elseif (preg_match('/ --ressource=(\d+)\b/', $cmd, $m)) {
			$config = [
				'ressourceId' => (int) $m[1],
				'collectionIds' => "",
			];
			$name = "KBART " . Yii::app()->db->createCommand("SELECT nom FROM Ressource WHERE id = {$m[1]}")->queryScalar();
		} else {
			throw new \Exception("Unknown import/kbart format: no resource/collection.");
		}
		if (preg_match('#\b(https?://[^\s"]+)\b#', $cmd, $m)) {
			$config['url'] = str_replace('\\%', '%', trim($m[1], '"'));
		} elseif (preg_match('# "?(/[^\s"]+\.(?:csv|txt))"? #', $cmd, $m)) {
			$config['url'] = str_replace('\\%', '%', $m[1]);
		} else {
			throw new \Exception("Unknown import/kbart format: no URL in '$cmd'.");
		}
		$config['ssiConnu'] = (strpos($cmd, " --ssiConnu=1") !== false);
		$config['contenu'] = (preg_match('/--defautType=(\S+) /', $cmd, $m) ? $m[1] : "");
		$config['acces'] = (preg_match('/--acces=(\S+) /', $cmd, $m) ? $m[1] : "");
		$config['ignoreUrl'] = (strpos($cmd, " --ignoreUrl=1") !== false);
		$config['verbose'] = 1;

		return [
			'name' => $name,
			'fqdn' => '\\' . tasks\ModelServiceKbart::class,
			'config' => json_encode($config, JSON_UNESCAPED_SLASHES),
		];
	}

	private function extractCmdImportDelete(string $cmd): array
	{
		$since = '';
		$m = [];
		if (preg_match('/--since="([^"]+?)"/', $cmd, $m)) {
			$since = $m[1];
		}
		return [
			'name' => "Accès jamais importés ou disparus",
			'fqdn' => '\\processes\\cron\\tasks\\ModelServiceObsolete',
			'config' => json_encode(['since' => $since], JSON_UNESCAPED_SLASHES),
		];
	}

	private function extractCmdImportOther(string $srcName): array
	{
		return [
			'name' => "Accès importés de $srcName",
			'fqdn' => '\\processes\\cron\\tasks\\ModelService' . $srcName,
			'config' => '{}',
		];
	}

	private function extractCmdLinks(string $source, string $cmd): array
	{
		$parsed = [
			'source' => $source,
			'obsoleteDelete' => false,
			'obsoleteSince' => '',
			'url' => '',
		];
		if (preg_match('/ --obsoleteDelete=1 /', $cmd)) {
			$parsed['obsoleteDelete'] = true;
		}
		$m = [];
		if (preg_match('/ --obsoleteSince="(.+?)" /', $cmd, $m)) {
			$parsed['obsoleteSince'] = $m[1];
		}
		if (preg_match('/ --csv=(\S+) /', $cmd, $m)) {
			$parsed['url'] = trim($m[1], '"\'');
		}
		$sourceFullname = Yii::app()->db
			->createCommand("SELECT nom FROM Sourcelien WHERE nomcourt = :n")
			->queryScalar([':n' => $source]);
		return [
			'name' => "Liens {$sourceFullname}",
			'fqdn' => '\\' . tasks\ModelLinkImport::class,
			'config' => json_encode($parsed, JSON_UNESCAPED_SLASHES),
		];
	}

	private function extractCmdVerifyLinks(string $name, string $cmd): array
	{
		$config = [
			'source' => $name === 'Cms' ? 'Cms' : preg_replace('/s$/', '', $name),
			'partiel' => false,
		];
		$m = [];
		if (preg_match('/--partiel=(\d)/', $cmd, $m)) {
			$config['partiel'] = (bool) $m[1];
		}
		return [
			'name' => "Vérification " . ($config['partiel'] ? "partielle " : "") . "des liens / $name",
			'fqdn' => '\\processes\\cron\\tasks\\ModelCheckLinks',
			'config' => json_encode($config, JSON_UNESCAPED_SLASHES),
		];
	}

	private function extractCmdAlertAll(string $intervention, string $cmd): array
	{
		$config = [
			'intervention' => $intervention,
			'verbose' => '',
			'since' => '',
			'before' => '',
		];
		if (preg_match('#--verbose=(\d)#', $cmd, $m)) {
			$config['verbose'] = $m[1];
		}
		if (preg_match('#--since="([^"]+)"#', $cmd, $m)) {
			$config['since'] = $m[1];
		}
		if (preg_match('#--before="([^"]+)"#', $cmd, $m)) {
			$config['before'] = $m[1];
		}
		return [
			'name' => "Alerte {$intervention}" . ($config["since"] !== "" ? " since " . $config["since"] : "") . ($config["before"] !== "" ? " before " . $config["before"] : ""),
			'fqdn' => '\processes\cron\tasks\ModelAlert',
			'config' => json_encode($config, JSON_UNESCAPED_SLASHES),
		];
	}

	private static function extractEmail(string $cmd): array
	{
		$subject = '';
		$emailsTxt = '';
		$m = [];
		if (preg_match('#\bmail -E -s "([^"]+)"#', $cmd, $m)) {
			$subject = $m[1];
		} elseif (preg_match('#\bmail -E -s\s+(\S+)\s*$#', $cmd, $m)) {
			$subject = $m[1];
		}
		if (preg_match('#\bmail .* --\s+(\S+@.+)$#', $cmd, $m)) {
			$emailsTxt = $m[1];
		} elseif (preg_match('#\bmail\b[^@]+\s+(\S+@.+)$#', $cmd, $m)) {
			$emailsTxt = $m[1];
		}
		if ($emailsTxt === '' && preg_match_all('# --email="(\S+@.+?)"#', $cmd, $m, PREG_PATTERN_ORDER)) {
			$emailsTxt = join("\n", $m[1]);
		}
		if (!$emailsTxt) {
			return ['emailTo' => '', 'emailSubject' => ''];
		}
		return [
			'emailTo' => join("\n",
				array_filter(array_map(
					function ($x) {
						$email = trim($x, ' \'"');
						if (strcasecmp($email, 'mir@bel') === 0) {
							return '';
						}
						return $email;
					},
					preg_split('/\s+/', $emailsTxt)
				))
			),
			'emailSubject' => $subject,
		];
	}

	private function extractSchedule(array $parts): array
	{
		[$m, $h, $dom, $mon, $dow] = $parts;
		if ($m === '*') {
			throw new \Exception("Unsupported schedule format (every minute)");
		}
		if ($mon !== '*') {
			throw new \Exception("Unsupported schedule format (mon != *)");
		}
		if ($h === '*' && $dom === '*' && $dow === '*') {
			return [
				'periodicity' => 'hourly',
				'hours' => [sprintf("%02d", (int) $m)],
				'days' => [0, 1, 2, 3, 4, 5, 6],
			];
		}

		if ($dom !== '*') {
			return [
				'periodicity' => 'monthly',
				'hours' => [sprintf("%02d:%02d", (int) $h, (int) $m)],
				'days' => [(int) $dom],
			];
		}

		return [
			'periodicity' => 'daily',
			'hours' => [sprintf("%02d:%02d", (int) $h, (int) $m)],
			'days' => self::extractWeekDays($dow),
		];
	}

	private static function extractWeekDays(string $dow): array
	{
		if ($dow === '*') {
			return [0, 1, 2, 3, 4, 5, 6];
		}

		$days = [];
		$m = [];
		if (preg_match('/^(\d)-(\d)$/', $dow, $m)) {
			for ($i = (int) $m[1]; $i <= (int) $m[2]; $i++) {
				$days[] = $i % 7;
			}
		} else {
			$mapping = ['sun' => 0, 'mon' => 1, 'tue' => 2, 'wed' => 3, 'thu' => 4, 'fri' => 5, 'sat' => 6];
			foreach (explode(",", $dow) as $d) {
				if (ctype_digit($d)) {
					$days[] = (int) $d % 7;
				} else {
					$days[] = $mapping[$d];
				}
			}
		}
		sort($days, SORT_NUMERIC);
		return $days;
	}
}
