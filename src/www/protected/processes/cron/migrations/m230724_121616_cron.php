<?php

/**
 * @codeCoverageIgnore
 */
class m230724_121616_cron extends \CDbMigration
{
	public function up(): bool
	{
		if ($this->getDbConnection()->getSchema()->getTable("CronTask")) {
			echo "WARNING: Table 'CronTask' exists, aborting creation.\n";
			return true;
		}
		$sqls = explode(
			"\n\n",
			<<<EOSQL
			CREATE TABLE CronTask (
				id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
				name VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci,
				description TEXT NOT NULL COLLATE utf8mb4_bin,
				fqdn VARCHAR(255) NOT NULL COLLATE ascii_bin COMMENT 'fully qualified class name',
				config JSON NOT NULL COMMENT 'object sent to the class setConfig() as an associative array',
				schedule JSON NOT NULL COMMENT '',
				active BOOL NOT NULL COMMENT 'if 0, the task will not be launched',
				forceRun BOOL NOT NULL DEFAULT 0 COMMENT 'if 1, the task will be launched at the next cron run',
				emailTo TEXT NOT NULL COMMENT 'one email address per line',
				emailSubject VARCHAR(255) NOT NULL COLLATE utf8mb4_bin,
				timelimit INT NOT NULL COMMENT 'seconds',
				lastUpdate BIGINT COMMENT 'timestamp (epoch time)'
			)

			CREATE TABLE CronHistory (
				id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
				crontaskId INT NOT NULL,
				config JSON NOT NULL COMMENT 'config as it was at runtime',
				startTime BIGINT COMMENT 'timestamp as an epoch time',
				endTime BIGINT COMMENT 'timestamp as an epoch time',
				results LONGTEXT NOT NULL COMMENT 'email body if emailTo was set, else stdout',
				emailTo TEXT NULL COMMENT 'one email address per line',
				error TEXT NOT NULL COMMENT 'empty on success, else the exception message, the config errors, etc',
				CONSTRAINT `fk_cronhistory_task`
					FOREIGN KEY (crontaskId) REFERENCES CronTask (id)
					ON DELETE CASCADE
					ON UPDATE RESTRICT
			)

			CREATE TABLE CronLock (
				id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
				crontaskId INT NOT NULL,
				identifier VARCHAR(255) NOT NULL COLLATE ascii_bin,
				startTime BIGINT COMMENT 'timestamp (epoch time)',
				CONSTRAINT `fk_cronlock_task`
					FOREIGN KEY (crontaskId) REFERENCES CronTask (id)
					ON DELETE RESTRICT
					ON UPDATE RESTRICT
			)
			EOSQL,
		);
		foreach ($sqls as $sql) {
			$this->execute($sql);
		}
		return true;
	}

	public function down(): bool
	{
		$this->dropTable("CronLock");
		$this->dropTable("CronHistory");
		$this->dropTable("CronTask");
		return true;
	}
}
