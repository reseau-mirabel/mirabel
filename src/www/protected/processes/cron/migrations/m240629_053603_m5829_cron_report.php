<?php

class m240629_053603_m5829_cron_report extends \CDbMigration
{
	public function up(): bool
	{
		$this->addColumn("CronHistory", "reportEmpty", "BOOLEAN NOT NULL DEFAULT 0 AFTER results");
		$this->addColumn("CronHistory", "reportTitle", "TEXT NOT NULL DEFAULT '' COMMENT 'plain text summary' AFTER reportEmpty");
		$this->addColumn("CronHistory", "reportBody", "LONGTEXT NOT NULL DEFAULT '' COMMENT 'detailed report, formatted with reportFormat' AFTER reportTitle");
		$this->addColumn("CronHistory", "reportFormat", "TINYINT NOT NULL DEFAULT 1 COMMENT '1-plaintext 2-markdown 3-html' AFTER reportBody");
		$this->addColumn("CronHistory", "reportEmailBody", "LONGTEXT NOT NULL DEFAULT '' COMMENT 'if empty, will use reportTitle+reportBody' AFTER reportFormat");
		$this->execute(<<<'SQL'
			UPDATE CronHistory
				SET reportEmpty = 1
				WHERE results = ''
			SQL
		);
		$this->execute(<<<'SQL'
			UPDATE CronHistory
				SET reportTitle = TRIM(SUBSTRING_INDEX(results, '\n\n', 1))
					, reportBody = TRIM(SUBSTRING(results, INSTR(results, '\n\n') + 2))
				WHERE INSTR(results, '\n\n') > 10 AND LENGTH(results) > (100 + INSTR(results, '\n\n'))
			SQL
		);
		$this->dropColumn("CronHistory", "results");
		return true;
	}

	public function down(): bool
	{
		echo "m240629_053603_m5829_cron_report does not support migrating down.\n";
		return false;
	}
}
