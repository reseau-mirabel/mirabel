<?php

namespace processes\cron;

use DateInterval;
use DateTimeImmutable;

/**
 * A task should run when its expected start time is between now (included) and now+interval (excluded),
 * with now and interval defined in the constructor.
 */
class Scheduler
{
	/**
	 * @var DateTimeImmutable Should only get overwritten for testing purposes.
	 */
	public DateTimeImmutable $start;

	public function __construct()
	{
		$this->start = new DateTimeImmutable();
	}

	/**
	 * Return true if the schedule ticks between now and now+interval.
	 */
	public function isScheduledToRunNow(Schedule $s, int $secondsInterval): bool
	{
		[$next] = $this->listNextRuns($s, 1);
		$end = $this->start->add(new DateInterval("PT{$secondsInterval}S"));
		return ($next >= $this->start && $next < $end);
	}

	/**
	 * List the $num next runs of the schedule, starting at $this->start.
	 *
	 * @return DateTimeImmutable[]
	 */
	public function listNextRuns(Schedule $s, int $num): array
	{
		if ($s->periodicity === 'daily') {
			$runs = $this->listNextRunsDaily($s);
			return self::repeat($runs, "P7D", $num);
		}
		if ($s->periodicity === 'monthly') {
			$runs = $this->listNextRunsMonthly($s);
			return self::repeat($runs, "P1M", $num);
		}
		$runs = $this->listNextRunsHourly($s);
		return self::repeat($runs, "P1D", $num);
	}

	/**
	 * List the $num next runs of the schedule, starting at $this->start and until $end.
	 *
	 * @return DateTimeImmutable[]
	 */
	public function listNextRunsUntil(Schedule $s, \DateTimeInterface $end): array
	{
		if ($s->periodicity === 'daily') {
			$runs = $this->listNextRunsDaily($s);
			return self::repeatUntil($runs, "P7D", $end);
		}
		if ($s->periodicity === 'monthly') {
			$runs = $this->listNextRunsMonthly($s);
			return self::repeatUntil($runs, "P1M", $end);
		}
		$runs = $this->listNextRunsHourly($s);
		return self::repeatUntil($runs, "P1D", $end);
	}

	/**
	 * @return DateTimeImmutable[] Timestamps of every launch over the week starting now.
	 */
	private function listNextRunsDaily(Schedule $s): array
	{
		$dayOfWeek = (int) $this->start->format('w');
		$dateInterval = new DateInterval("P{$dayOfWeek}D");
		$sunday = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $this->start->format('Y-m-d') . ' 00:00:00')->sub($dateInterval);

		$later = [];
		$next = [];
		foreach ($s->days as $day) {
			foreach ($s->hours as $h) {
				$hour = str_replace(':', 'H', $h) . "M";
				$start = $sunday->add(new DateInterval("P{$day}DT{$hour}30S"));
				if ($start < $this->start) {
					$later[] = $start->add(new DateInterval("P7D"));
				} else {
					$next[] = $start;
				}
			}
		}
		return array_merge($next, $later);
	}

	/**
	 * @return DateTimeImmutable[] Timestamps Over the day starting now.
	 */
	private function listNextRunsHourly(Schedule $s): array
	{
		$firstDate = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $this->start->format('Y-m-d H:') . sprintf('%02d', $s->hours[0]) . ":30");
		if ($firstDate === false) {
			throw new \Exception("Failed to create DateTimeImmutable for the first date.");
		}
		$next = [];
		if ($firstDate < $this->start) {
			$next[] = $firstDate->add(new DateInterval("PT1H"));
		} else {
			$next[] = $firstDate;
		}
		for ($i = 1; $i < 24; $i++) {
			$next[] = $next[0]->add(new DateInterval("PT{$i}H"));
		}
		return $next;
	}

	/**
	 * @return DateTimeImmutable[] Timestamps
	 */
	private function listNextRunsMonthly(Schedule $s): array
	{
		$daysToMonthStart = (int) $this->start->format('j') - 1;
		$dateInterval = new \DateInterval("P{$daysToMonthStart}D");
		$first = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $this->start->format('Y-m-d') . ' 00:00:00')->sub($dateInterval);

		$later = [];
		$next = [];
		foreach ($s->days as $day) {
			$dayShift = $day - 1;
			foreach ($s->hours as $h) {
				$hour = str_replace(':', 'H', $h) . "M";
				$start = $first->add(new DateInterval("P{$dayShift}DT{$hour}30S"));
				if ($start < $this->start) {
					$later[] = $start->add(new DateInterval("P1M"));
				} else {
					$next[] = $start;
				}
			}
		}
		return array_merge($next, $later);
	}

	/**
	 * Repeat the runs (list of dates) until it can return a new list with $num elements.
	 */
	private static function repeat(array $runs, string $period, int $num): array
	{
		if (count($runs) >= $num) {
			return array_slice($runs, 0, $num);
		}
		$interval = new DateInterval($period);
		for ($i = 0; count($runs) < $num; $i++) {
			$runs[] = $runs[$i]->add($interval);
		}
		return $runs;
	}

	private static function repeatUntil(array $runs, string $period, \DateTimeInterface $end): array
	{
		if (!$runs) {
			return [];
		}
		$result = [];
		foreach ($runs as $dt) {
			if ($dt < $end) {
				$result[] = $dt;
			} else {
				return $result;
			}
		}
		$interval = new DateInterval($period);
		$i = 0;
		while (true) {
			$dt = $result[$i]->add($interval);
			assert($dt instanceof \DateTimeInterface);
			if ($dt < $end) {
				$result[] = $dt;
			} else {
				break;
			}
			if (count($result) > 500) {
				throw new \Exception("Planification : liste plafonnée à 500 résultats.");
			}
			$i++;
		}
		return $result;
	}
}
