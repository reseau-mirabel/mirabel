<?php

namespace processes\cron;

interface ModelInterface
{
	/**
	 * @return string HTML description of the model (ie what a task, instance of this model, would do).
	 */
	public static function getDescription(): string;

	/**
	 * @return string ASCII text that will prevent parallel runs. Empty if not needed.
	 */
	public function getUniqueIdentifer(): string;

	/**
	 * This will be used to display a form where users can configure a model's instance.
	 *
	 * @return array<string, ConfigParam> The config of a model is a set of string keys and mixed values.
	 */
	public static function getConfigHelp(): array;

	/**
	 * @param array<string, mixed> $config
	 */
	public function setConfig(array $config): void;

	/**
	 * @param array $config clé / valeur
	 * @return array clé / message d'erreur
	 */
	public static function checkConfig(array $config): array;

	/**
	 * @return array<string, string|string[]> For each key, an HTML human-readable description of the value
	 */
	public function getConfigSummary(): array;

	public function run(): Report;
}
