<?php

namespace processes\cron;

class ConfigParam
{
	/**
	 *
	 * @var "bool"|"integer"|"string"|"text"|"list"
	 */
	public string $format;

	/**
	 * @var string Plain text
	 */
	public string $label;

	/**
	 * @var string HTML
	 */
	public string $help;

	/**
	 * @var string Plain text
	 */
	public string $placeholder;

	/**
	 * @var array If ($format === "list"), the options of the dropdown as [key => value, ...] or [[key, value], ...].
	 *   If ($format === "integer"), keys are "min" or "max" (for the HTML input element).
	 */
	public array $values = [];

	public function __construct(string $format, string $label, string $help, array $values = [], $placeholder = "")
	{
		$this->format = $format;
		$this->label = $label;
		$this->help = $help;
		if ($values) {
			if ($format === 'list') {
				if (isset($values[''])) {
					throw new \Exception("Erreur dans le code : ConfigParam('list') ne peut recevoir un tableau avec '' pour clé.");
				}
			} elseif ($format === 'integer' && count(array_intersect(array_keys($values), ['max', 'min'])) !== count($values)) {
				throw new \Exception("Erreur dans le code : ConfigParam('integer') doit recevoir un tableau de clés 'max' ou 'min'.");
			}
			$this->values = $values;
		}
		$this->placeholder = $placeholder;
	}
}
