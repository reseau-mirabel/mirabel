<?php

namespace processes\cron;

use processes\cron\ar\History;

/**
 * The output of a cron task execution.
 *
 * A part of the report is for the web (displayed in the history of a task),
 * and an optional part stores the email body (just the body, without the potential attached parts).
 *
 * It will be stored inside the SQL table CronHistory, @see \processes\cron\ar\History.
 */
class Report
{
	/**
	 * @var string Empty iff the task was successful.
	 */
	public string $error = '';

	/**
	 * @var string Plain text
	 */
	public string $title = '';

	/**
	 * @var string The syntax of this text is given by $format.
	 */
	public string $body = '';

	public int $format = History::REPORTFORMAT_PLAINTEXT;

	/**
	 * @var bool If not set, no email will be sent, even if the body is filled.
	 */
	public bool $sendEmail = true;

	/**
	 * @var string Optional. Will be used iff the cron Task.subject is empty.
	 */
	public string $emailSubject = '';

	/**
	 * @var string Optional, title+body will be used if empty (@see $this->getEmailBody()).
	 */
	public string $emailBody = '';

	/**
	 * @var list<array{content: string, name: string, mimeType: string}>
	 */
	public array $emailAttachments = [];

	public function isEmpty(): bool
	{
		return $this->error === '' && $this->title === '' && $this->body === '' && $this->emailBody === '';
	}

	public function getEmailBody(): string
	{
		$body = "";
		if ($this->error) {
			if ($this->format === History::REPORTFORMAT_HTML) {
				// The error is not HTML, so we convert it.
				$body = "<div><strong>ERREUR</strong><br>" . htmlspecialchars($this->error) . "</div>\n\n";
			} else {
				$body =  "## ERREUR\n{$this->error}\n\n";
			}
		}
		if ($this->emailBody) {
			$body .= trim($this->emailBody, "\n");
		} elseif ($this->title === '') {
			$body .= trim($this->body, "\n");
		} elseif ($this->format === History::REPORTFORMAT_HTML) {
			// The title is not HTML, so we convert it.
			$body .= trim(sprintf("<h1>%s</h1>\n\n%s", htmlspecialchars($this->title), $this->body), "\n");
		} else {
			$body .= sprintf("%s\n\n%s", trim($this->title), trim($this->body, "\n"));
		}
		return $body;
	}

	public function attachToEmail(string $content, string $name, string $mimeType): void
	{
		$this->emailAttachments[] = ['content' => $content, 'name' => $name, 'mimeType' => $mimeType];
	}

	/**
	 * @return list<array{content: string, name: string, mimeType: string}>
	 */
	public function getAttachments(): array
	{
		return $this->emailAttachments;
	}
}
