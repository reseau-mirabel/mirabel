<?php

namespace processes\cron;

class TaskForm
{
	public string $fqdn = '';

	public string $name = '';

	public string $description = '';

	public bool $active = true;

	public array $config = [];

	public Schedule $schedule;

	public string $emailTo = '';

	public string $emailSubject = '';

	public int $timelimit = 0;

	/**
	 * @var array<string, string>
	 */
	private array $errors = [];

	public function __construct()
	{
		$this->schedule = new Schedule();
	}

	public function getAttributes(): array
	{
		$a = (array) $this; // ugly hack, but simpler than Reflection
		$attributes = [];
		foreach (array_keys($a) as $k) {
			if (!is_string($k)) {
				continue;
			}
			// Private properties have a special key starting with \0.
			if ($k !== '' && ord($k[0]) !== 0) {
				// $k is now the name of a public property of this instance
				$attributes[$k] = $a[$k];
			}
		}
		return $attributes;
	}

	public function getErrors(): array
	{
		return $this->errors;
	}

	public function setAttributes(array $data): void
	{
		foreach (['active'] as $k) {
			if (isset($data[$k])) {
				$this->{$k} = (bool) $data[$k];
			}
		}
		foreach (['timelimit'] as $k) {
			if (isset($data[$k])) {
				$this->{$k} = (int) $data[$k];
			}
		}
		foreach (['fqdn', 'name', 'description', 'emailTo', 'emailSubject'] as $k) {
			if (isset($data[$k])) {
				$this->{$k} = trim((string) $data[$k]);
			}
		}
		if (isset($data['config']) && is_array($data['config'])) {
			$this->config = $data['config'];
		}
		if (isset($data['schedule']) && is_array($data['schedule'])) {
			$this->schedule->days = $data['schedule']['days'] ?? [];
			$this->schedule->hours = $data['schedule']['hours'] ?? [];
			$this->schedule->periodicity = $data['schedule']['periodicity'] ?? [];
		}
	}

	public function validate(): bool
	{
		$this->errors = [];
		if (!$this->fqdn) {
			$this->errors['fqdn'] = "Ce champ est requis.";
			return false;
		}
		if (!is_a($this->fqdn, ModelInterface::class, true)) {
			$this->errors['fqdn'] = "N'est pas une classe de type ModelInterface.";
			return false;
		}
		if (strlen($this->name) > 255) {
			$this->errors['name'] = "Ce champ est trop long (maximum de 255 octets en UTF-8).";
		}
		$fqdn = $this->fqdn;
		$configErrors = $fqdn::checkConfig($this->config);
		if ($configErrors) {
			$this->errors['config'] = "<ul>";
			$fields = $fqdn::getConfigHelp();
			foreach ($configErrors as $k => $v) {
				$label = $fields[$k]->label;
				$this->errors['config'] .= "<li><strong>$label</strong> $v</li>";
			}
			$this->errors['config'] .= "</ul>";
		}
		$schError = $this->schedule->getError();
		if ($schError) {
			$this->errors['schedule'] = $schError;
		}
		if ($this->emailTo !== '' && strpos($this->emailTo, '@') === false) {
			$this->errors['emailTo'] = "Ce champ doit être vide, ou contenir au moins une adresse électronique.";
		} else {
			$emailValidator = new \CEmailValidator();
			$emails = preg_split('/\s+/', $this->emailTo, -1, PREG_SPLIT_NO_EMPTY);
			foreach ($emails as $e) {
				if (!$emailValidator->validateValue($e)) {
					$this->errors['emailTo'] = "Au moins une adresse électronique de ce champ n'est pas valide.";
				}
			}
		}
		if (strlen($this->emailSubject) > 255) {
			$this->errors['emailSubject'] = "Ce champ est trop long (maximum de 255 octets en UTF-8).";
		}
		return count($this->errors) === 0;
	}

	public function updateRecord(ar\Task $record): void
	{
		$record->setAttributes(
			[
				'name' => $this->name,
				'description' => $this->description,
				'fqdn' => $this->fqdn,
				'config' => json_encode($this->config, JSON_UNESCAPED_SLASHES),
				'schedule' => json_encode($this->schedule, JSON_UNESCAPED_SLASHES),
				'active' => $this->active,
				'emailTo' => $this->emailTo,
				'emailSubject' => $this->emailSubject,
				'timelimit' => $this->timelimit,
			],
			false
		);
	}
}
