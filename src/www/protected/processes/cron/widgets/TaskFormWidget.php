<?php

namespace processes\cron\widgets;

class TaskFormWidget
{
	public bool $debug;

	public array $errors = [];

	public array $state = [];

	public function __construct()
	{
		$this->debug = defined('YII_DEBUG') && YII_DEBUG;
	}

	public function loadStateFromRecord(\processes\cron\ar\Task $task): void
	{
		$this->state = $task->getAttributes();
		unset($this->state['id']);
		unset($this->state['lastUpdate']);
		$this->state['config'] = json_decode($this->state['config'], true);
		$this->state['schedule'] = $task->getScheduleObject();
	}

	public function run(): string
	{
		$am = \Yii::app()->assetManager;
		$cs = \Yii::app()->clientScript;

		// mithril
		$mithrilPath = VENDOR_PATH . '/npm-asset/mithril/' . ($this->debug ? 'mithril.js' : 'mithril.min.js');
		$cs->registerScriptFile($am->publish($mithrilPath));

		// js source code
		$encodeOptions = JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE;
		if ($this->debug) {
			$encodeOptions = $encodeOptions | JSON_PRETTY_PRINT;
		}
		$encoded = json_encode($this->readData(), $encodeOptions);
		$cs->registerScriptFile($am->publish(__DIR__ . '/assets/task-form.js'), \CClientScript::POS_HEAD, ['defer' => true]);

		// json data + root html
		return <<<EOHTML
			<script id="task-form-data" type="application/json">$encoded</script>
			<div id="task-form-content"></div>
			<noscript>Cette page ne fonctionne qu'avec JavaScript activé dans le navigateur.</noscript>
			EOHTML;
	}

	private function readData(): array
	{
		$result = [
			'errors' => $this->errors,
			'models' => self::readModelsConfig(),
		];
		if ($this->state) {
			if (empty($this->state['config'])) {
				// An empty array will be converted to [], because PHP (assoc) arrays are a mess.
				unset($this->state['config']);
			}
			$result['state'] = $this->state;
		}
		return $result;
	}

	/**
	 * Return an aggregate of the config of all the task models.
	 */
	private static function readModelsConfig(): array
	{
		$config = [];
		$classfiles = glob(dirname(__DIR__) . '/tasks/Model*.php');
		foreach ($classfiles as $c) {
			$className = substr(basename($c), 0, -4);
			$fqdn = "\\processes\\cron\\tasks\\$className";
			if (!is_a($fqdn, \processes\cron\ModelInterface::class, true)) {
				continue;
			}
			$config[] = [
				'name' => substr($className, 5),
				'fqdn' => $fqdn,
				'configHelp' => $fqdn::getConfigHelp(),
				'description' => $fqdn::getDescription(),
			];
		}
		return $config;
	}
}
