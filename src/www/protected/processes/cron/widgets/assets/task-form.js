/* global console, document, m, window */

const state = {
	model: null, // int
	name: "",
	description: "",
	fqdn: "",
	active: true,
	config: {},
	schedule: {
		periodicity: "",
		hours: [],
		days: [],
	},
	emailTo: "",
	emailSubject: "",
	timelimit: 0,
}
const context = JSON.parse(document.getElementById('task-form-data').text)
if (context.hasOwnProperty('state')) {
	for (let [k, v] of Object.entries(context.state)) {
		state[k] = v
	}
	for (let i = 0; i < context.models.length; i++) {
		if (state.fqdn === context.models[i].fqdn) {
			state.model = i
			break
		}
	}
}
if (window.history.state !== null) {
	for (let [k, v] of Object.entries(window.history.state)) {
		state[k] = v
	}
}
console.log("context", context)
console.log("state", state)

const SelectModel = {
	options: [['', "-"]],
	oninit() {
		for (let i=0; i < context.models.length; i++) {
			this.options.push([i, context.models[i].name])
		}
	},
	view() {
		return m('div.control-group', [
			m('label.control-label.required', { for: "modele" }, "Modèle"),
			m('div.controls', [
				m('select',
					{
						name: "modele",
						required: true,
						onchange: function() {
							if (this.value === "") {
								state.model = ""
								state.fqdn = ""
							} else {
								state.model = parseInt(this.value)
								state.fqdn = context.models[state.model].fqdn
							}
						}
					},
					this.options.map((o) => m('option', {key: o[0], value: o[0], selected: (o[0] === state.model)}, o[1]))
				),
				m('div.help-block', state.model === null ? ' ' : m.trust(context.models[state.model].description)),
			])
		])
	},
}

const TimeLimit = {
	getStateValue() {
		if (state.timelimit < 60) {
			return ""
		}
		const minutesLimit = Math.trunc(state.timelimit / 60)
		const minutes = minutesLimit % 60
		const hours = (minutesLimit - minutes) / 60
		return `${hours < 10 ? "0" + hours : hours}:${minutes < 10 ? "0" + minutes : minutes}`
	},
	setStateValue(txt) {
		if (txt.match(/^\d+$/)) {
			state.timelimit = parseInt(txt) * 60
		} else if (txt.match(/^\d+:\d\d$/)) {
			const t = txt.spit(':')
			state.timelimit = (t[0] * 60 + t[1]) * 60
		} else {
			state.timelimit = 0
		}
	},
	view() {
		return m('div.control-group', [
			m('label.control-label', { for: "timelimit" }, "Durée maximale"),
			m('div.controls', [
				m('input', {
					name: "timelimit",
					value: TimeLimit.getStateValue(),
					placeholder: "hh:mm",
					onchange: function() {
						TimeLimit.setStateValue(this.value)
					}
				}),
				m('div.help-block', "Si une durée (en heures:minutes) est déclarée, une tâche trop longue sera arrêtée."),
			])
		])
	},
}

const Checkbox = {
	view(vnode) {
		const label = vnode.attrs.label
		const value = vnode.attrs.value
		const extra = Object.assign({type: "checkbox"}, vnode.attrs.extra || {})
		return m('div.control-group', [
			m('div.controls',
				m('label.checkbox', [
					m('input', extra, value),
					" ",
					label,
					vnode.attrs.help ? m('span.help-block', vnode.attrs.help) : null,
				]),
			)
		])
	},
}

const Checkboxlist = {
	name: '',
	oninit() {
		this.name = 'l' + Math.random().toString(36).slice(-5);
	},
	view(vnode) {
		const label = vnode.attrs.label
		const name = this.name
		const valueToCheckbox = function (v, index) {
			let value, display
			if (v instanceof Array) {
				value = v[0]
				display = v.length > 1 ? v[1] : v[0].toString()
			} else {
				value = v
				display = v.toString()
			}

			return m("div",
				m("label.checkbox",
					m("input", {
						type: "checkbox",
						name: `${name}_${index}`,
						checked: vnode.attrs.checked.includes(value),
						value,
						onchange(event) {
							let result = []
							let checkboxes = event.target.closest("div.controls").querySelectorAll('input[type="checkbox"]')
							for (let c of checkboxes) {
								if (c.checked) {
									result.push(c.value)
								}
							}
							vnode.attrs.onchange(result)
						}
					}),
					" ",
					display,
				)
			)
		}
		return [
			m("div.control-group", vnode.attrs.error ? {class: 'error'} : {},
				m("label.control-label", label),
				m("div.controls", vnode.attrs.values.map(valueToCheckbox))
			)
		]
	},
}

const Dropdown = {
	name: '',
	oninit() {
		this.name = 'd' + Math.random().toString(36).slice(-5);
	},
	view(vnode) {
		const label = vnode.attrs.label
		const options = vnode.attrs.options
		const extra = Object.assign({name: this.name}, vnode.attrs.extra || {})
		return m('div.control-group', [
			m('label.control-label' + (extra.required ? '.required' : ''), { for: this.name }, label),
			m('div.controls',
				m('select', extra,
					[m('option', {value: ""}, "[Choisir une valeur]")]. concat(
						options.map(o => m('option', {value: o[0], selected: o[0] === vnode.attrs.selected}, o[1]))
					)
				),
				vnode.attrs.help ? m('div.help-block', vnode.attrs.help) : null,
			)
		])
	},
}

const HelpBlock = {
	view(vnode) {
		const error = vnode.attrs.error || ""
		const help = vnode.attrs.help || ""
		if (error === "" && help === "") {
			return null;
		}
		return m('div.help-block',
			error === "" ? null : m('div', m('strong', m.trust(error))),
			help === "" ? null : help,
		)
	},
}

const Text = {
	name: '',
	oninit() {
		this.name = 't' + Math.random().toString(36).slice(-5);
	},
	view(vnode) {
		const label = vnode.attrs.label
		const value = vnode.attrs.value
		const extra = Object.assign({type: "text", name: this.name, value}, vnode.attrs.extra || {})
		return m('div.control-group' + (vnode.attrs.error ? ".error" : ""), [
			m('label.control-label' + (extra.required ? '.required' : ''), { for: this.name }, label),
			m('div.controls.controls-row',
				m('input.span12', extra),
				m(HelpBlock, {error: vnode.attrs.error, help: vnode.attrs.help}),
			)
		])
	},
}

const Textarea = {
	name: '',
	oninit() {
		this.name = 'a' + Math.random().toString(36).slice(-5);
	},
	view(vnode) {
		const label = vnode.attrs.label
		const value = vnode.attrs.value
		const extra = Object.assign({name: this.name, rows: 3}, vnode.attrs.extra || {})
		return m('div.control-group' + (vnode.attrs.error ? ".error" : ""), [
			m('label.control-label' + (extra.required ? '.required' : ''), { for: this.name }, label),
			m('div.controls.controls-row',
				m('textarea.span12', extra, value),
				m(HelpBlock, {error: vnode.attrs.error, help: vnode.attrs.help}),
			)
		])
	},
}

const Schedule = {
	daysMonth: [],
	daysWeek: [
		[1, "lundi"],
		[2, "mardi"],
		[3, "mercredi"],
		[4, "jeudi"],
		[5, "vendredi"],
		[6, "samedi"],
		[0, "dimanche"],
	],
	oninit() {
		for (let i = 1; i <= 28; i++) {
			this.daysMonth.push(i)
		}
	},
	view() {
		return [
			context.errors.hasOwnProperty('schedule') ?
				m('div.alert.alert-error', m.trust(context.errors.schedule))
				: null,
			m(Checkbox, {
				label: "Tâche activée",
				help: "Cette tâche ne sera déclenchée par le cron que si cette case est cochée.",
				value: "1",
				extra: {
					checked: state.active,
					name: 'active',
					onchange: function() {
						state.active = this.checked
					},
				},
			}),
			m(Dropdown, {
				label: "Périodicité",
				options: [
					['', '-'],
					['hourly', "Chaque heure"],
					['daily', "Chaque jour ou chaque semaine"],
					['monthly', "Chaque mois"],
				],
				selected: state.schedule.periodicity,
				extra: {
					required: true,
					onchange() {
						if (this.value === 'hourly') {
							state.schedule.periodicity = 'hourly'
							state.schedule.days = []
							if (state.schedule.hours.length > 0 && !state.schedule.hours[0].match(/^\d\d$/)) {
								state.schedule.hours = []
							}
							return;
						}
						if (state.schedule.hours.length > 0 && state.schedule.hours[0].match(/^\d\d$/)) {
							state.schedule.hours = []
						}
						if (this.value === 'monthly') {
							state.schedule.periodicity = 'monthly'
							state.schedule.days = []
						} else {
							state.schedule.periodicity = 'daily'
							state.schedule.days = [0, 1, 2, 3, 4, 5, 6]
						}
					},
				},
			}),
			(state.schedule.periodicity === '' ? null :
				m(Text, {
					label: "Heure",
					value: state.schedule.hours.join(" "),
					extra: {
						placeholder: state.schedule.periodicity === 'hourly' ? "mm (minutes)" : "hh:mm (heure:minute, éventuellement plusieurs)",
						pattern: state.schedule.periodicity === 'hourly' ? "[0-5][0-9]( [0-5][0-9])*" : "[0-2][0-9]:[0-5][0-9]( [0-2][0-9]:[0-5][0-9])*",
						required: true,
						onchange: function() {
							state.schedule.hours = this.value.split(/\s+/)
						},
					},
				})
			),
			(state.schedule.periodicity === '' || state.schedule.periodicity === 'hourly' ? null :
				m(Checkboxlist, {
					label: "Jours " + (state.schedule.periodicity === 'monthly' ? "du mois" : "de la semaine"),
					values: (state.schedule.periodicity === 'monthly' ? this.daysMonth : this.daysWeek),
					checked: state.schedule.days,
					onchange: (values) => {
						state.schedule.days = values.map(x => parseInt(x));
					},
				})
			),
		]
	},
}

const ModelConfig = {
	view() {
		if (state.model === null) {
			return null
		}
		const result = [];
		if (context.errors.hasOwnProperty('config')) {
			result.push(m('div.alert.alert-error', m.trust(context.errors.config)))
		}
		return result.concat(
			Object.entries(context.models[state.model].configHelp).map(([name, field]) => {
				switch (field.format) {
					case "bool":
						return this.viewBool(name, field)
					case "integer":
						return this.viewInteger(name, field)
					case "string":
						return this.viewText(name, field)
					case "text":
						return this.viewTextarea(name, field)
					case "list":
						return this.viewDropdown(name, field)
					default:
						console.log("unknown " + field.format)
						return null
				}
			})
		)
	},
	viewBool(name, field) {
		return m(Checkbox, {
			label: field.label,
			help: field.help ? m.trust(field.help) : null,
			value: "1",
			extra: {
				checked: state.config[name],
				onchange: function() { state.config[name] = this.checked },
			},
		})
	},
	viewInteger(name, field) {
		return m(Text, {
			label: field.label,
			help: field.help ? m.trust(field.help) : null,
			value: state.config[name] || "",
			extra: Object.assign(field.values, {
				type: "number",
				onchange: function() {
					state.config[name] = parseInt(this.value)
				},
			}),
		})
	},
	viewText(name, field) {
		return m(Text, {
			label: field.label,
			help: field.help ? m.trust(field.help) : null,
			value: state.config[name] || "",
			extra: {
				onchange: function() {
					state.config[name] = this.value
				},
				placeholder: field.placeholder,
			},
		})
	},
	viewTextarea(name, field) {
		return m(Textarea, {
			label: field.label,
			help: field.help ? m.trust(field.help) : null,
			value: state.config[name] || "",
			extra: {
				onchange: function() { state.config[name] = this.value },
			},
			placeholder: field.placeholder,
		})
	},
	viewDropdown(name, field) {
		let options = [];
		if (field.values instanceof Array) {
			if (field.values[0] instanceof Array) {
				options = field.values;
			} else {
				options = field.values.map((x, index) => [index.toString(), x]);
			}
		} else {
			for (const x in field.values) {
				if (field.values.hasOwnProperty(x)) {
					options.push([x, field.values[x]]);
				}
			}
		}
		return m(Dropdown, {
			label: field.label,
			help: field.help ? m.trust(field.help) : null,
			options: options,
			selected: state.config[name] || "",
			extra: {
				onchange: function() { state.config[name] = this.value },
			},
			placeholder: field.placeholder,
		})
	},
}

const Main = {
	modelOptions: [],
	oninit() {
		this.modelOptions = context.models.map((x, i) => [i, x.name])
	},
	view() {
		return m('form.form-horizontal', {
			onsubmit: function(e) {
				e.preventDefault();
				window.history.pushState(state, "")

				const form = document.createElement('form')
				form.method = 'POST'
				const content = document.createElement('input')
				content.type = 'hidden'
				content.name = 'body'
				content.value = JSON.stringify(state)
				form.appendChild(content)
				document.body.appendChild(form)
				form.submit()
			},
		},
		[
			m(SelectModel),
			m(Text, {
				label: "Nom",
				value: state.name,
				error: context.errors.name,
				extra: {
					placeholder: "Affiché dans la liste des tâches",
					required: true,
					onchange: function() { state.name = this.value },
				},
			}),
			m(Textarea, {
				label: "Remarques",
				value: state.description,
				extra: {
					placeholder: "Uniquement affiché sur la page décrivant la tâche",
					onchange: function() {
						state.description = this.value
					},
				},
			}),
			m(TimeLimit),
			m('fieldset',
				m('legend', "Configuration spécifique à ce modèle"),
				m(ModelConfig),
			),
			m('fieldset',
				m('legend', "Planification"),
				m(Schedule),
			),
			m('fieldset',
				m('legend', "Courriels"),
				m(Textarea, {
					label: "Destinataires",
					value: state.emailTo,
					error: context.errors.emailTo,
					extra: {
						placeholder: "une adresse par ligne (éventuellement précédée du nom et prénom)",
						onchange: function() { state.emailTo = this.value },
					},
				}),
				m(Text, {
					label: "Sujet",
					value: state.emailSubject,
					error: context.errors.emailSubject,
					extra: {
						placeholder: "Sera préfixé par le nom d'instance [Mir@bel]. Si vide, le message par défaut du modèle sera utilisé, ou le nom de la tâche.",
						onchange: function() { state.emailSubject = this.value },
					},
				}),
			),
			m('div.form-actions',
				m('button.btn.btn-primary', {type: 'submit'}, "Enregistrer"),
			),
		])
	},
}

const root = document.getElementById('task-form-content');
m.mount(root, Main);
