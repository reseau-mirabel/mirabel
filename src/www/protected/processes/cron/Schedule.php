<?php

namespace processes\cron;

/**
 * Contains a CronTask schedule:
 * - submitted inside the task form,
 * - saved into the JSON field CronTask.schedule.
 */
class Schedule
{
	/**
	 * @var "hourly"|"daily"|"monthly"
	 */
	public string $periodicity;

	/**
	 * @var string[] E.g. ["15"] if hourly, else ["02:15", "16:48"].
	 */
	public array $hours = [];

	/**
	 * @var int[] Values in 0..6 if daily, 1..28 if monthly.
	 */
	public array $days = [];

	public static function fromJson(string $json): self
	{
		$o = new self;
		$data = json_decode($json, true, 5, JSON_THROW_ON_ERROR);
		foreach ($data as $k => $v) {
			if (!property_exists($o, $k)) {
				throw new \Exception("Unknown property in JSON");
			}
			$o->{$k} = $v;
		}
		return $o;
	}

	public function getError(): string
	{
		if (!in_array($this->periodicity, ["hourly", "daily", "monthly"])) {
			return "La périodicité choisie n'est pas une des valeurs prévues.";
		}
		if (!$this->hours) {
			return "Il faut déclarer au moins un horaire.";
		}
		if ($this->periodicity === "hourly") {
			foreach ($this->hours as $hour) {
				if (!preg_match('/^\d\d$/', $hour) || $hour > 59) {
					return "Chaque horaire doit être entre 00 et 59 (avec 2 chiffres).";
				}
			}
			$this->days = [];
		} elseif ($this->periodicity === "daily") {
			$m = [];
			foreach ($this->hours as $hour) {
				if (!self::validateHour($hour)) {
					return "Chaque horaire doit être entre 00:00 et 23:59 (avec 4 chiffres en tout).";
				}
			}
			if (!$this->days) {
				return "Choisir au moins un jour de la semaine.";
			}
			foreach ($this->days as $d) {
				if (!is_int($d) || $d < 0 || $d > 6) {
					return "Valeur incorrecte pour un jour de la semaine.";
				}
			}
		} else { // monthly
			$m = [];
			foreach ($this->hours as $hour) {
				if (!self::validateHour($hour)) {
					return "Chaque horaire doit être entre 00:00 et 23:59 (avec 4 chiffres en tout).";
				}
			}
			if (!$this->days) {
				return "Choisir au moins un jour du mois.";
			}
			foreach ($this->days as $d) {
				if (!is_int($d) || $d < 1 || $d > 28) {
					return "Valeur incorrecte pour un jour du mois.";
				}
			}
		}
		return '';
	}

	public function toString(): string
	{
		switch ($this->periodicity) {
			case 'daily':
				if (count($this->days) === 7) {
					return "Chaque jour, à " . join(", ", $this->hours);
				}
				$dayNames = ['dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
				$days = [];
				foreach ($this->days as $d) {
					$days[] = $dayNames[$d];
				}
				return "Chaque " . join(", ", $days) . ", à " . join(", ", $this->hours);
			case 'hourly':
				if (count($this->hours) > 1) {
					return "Chaque heure, aux minutes " . join(", ", $this->hours);
				}
				return "Chaque heure, à la minute " . join(", ", $this->hours);
			case 'monthly':
				$plural = count($this->days) > 1 ? "s" : "";
				return "Chaque mois, le$plural jour$plural " . join(", ", $this->days)
					. ", à " . join(", ", $this->hours);
			default:
				return "ERREUR : format non-valide";
		}
	}

	private static function validateHour(string $hour): bool
	{
		$m = [];
		if (!preg_match('/^(\d\d):(\d\d)$/', $hour, $m)) {
			return false;
		}
		if ((int) $m[1] > 23 || (int) $m[2] > 59) {
			return false;
		}
		return true;
	}
}
