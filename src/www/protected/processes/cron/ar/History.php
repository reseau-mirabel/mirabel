<?php

namespace processes\cron\ar;

/**
 * @property int $id
 * @property int $crontaskId
 * @property string $config
 * @property int $startTime timestamp
 * @property int $endTime timestamp
 * @property string $emailTo
 * @property string $reportTitle plain text summary
 * @property string $reportBody detailed report, formatted with reportFormat
 * @property int $reportFormat see constants REPORTFORMAT_*
 * @property string $reportEmailBody if empty, will use reportTitle+reportBody
 * @property string $error
 */
class History extends \CActiveRecord
{
	public const REPORTFORMAT_PLAINTEXT = 1;

	public const REPORTFORMAT_MARKDOWN = 2;

	public const REPORTFORMAT_HTML = 3;

	public function tableName()
	{
		return "CronHistory";
	}

	public function afterFind()
	{
		parent::afterFind();
		$this->id = (int) $this->id;
		$this->crontaskId = (int) $this->crontaskId;
		$this->reportFormat = (int) $this->reportFormat;
		$this->startTime = (int) $this->startTime;
		$this->endTime = (int) $this->endTime;
	}
}
