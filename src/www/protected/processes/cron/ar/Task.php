<?php

namespace processes\cron\ar;

use processes\cron\ModelInterface;
use processes\cron\Schedule;

/**
 * @property ?int $id
 * @property string $name
 * @property string $description
 * @property string $fqdn fully qualified class name
 * @property string $config JSON object sent to the class setConfig() as an associative array
 * @property string $schedule JSON,
 * @property bool $active the task will not be launched,
 * @property bool $forceRun if true, the task will be launched at the next cron run
 * @property string $emailTo email addresses, separated by line feeds,
 * @property string $emailSubject
 * @property int $timelimit seconds
 * @property int $lastUpdate timestamp (epoch time)
 */
class Task extends \CActiveRecord
{
	private ?ModelInterface $instance = null;

	public function tableName()
	{
		return "CronTask";
	}

	public function afterFind()
	{
		parent::afterFind();
		$this->id = (int) $this->id;
		$this->active = (bool) $this->active;
		$this->forceRun = (bool) $this->forceRun;
		$this->timelimit = (int) $this->timelimit;
		$this->lastUpdate = (int) $this->lastUpdate;
	}

	public function beforeSave()
	{
		$this->lastUpdate = (int) $_SERVER['REQUEST_TIME'];
		return parent::beforeSave();
	}

	public function getScheduleObject(): Schedule
	{
		return Schedule::fromJson($this->schedule);
	}

	public function getTaskInstance(): ModelInterface
	{
		if ($this->instance === null) {
			$fqdn = $this->fqdn;
			$this->instance = new $fqdn;
			assert($this->instance instanceof ModelInterface);

			$config = json_decode($this->config, true, JSON_THROW_ON_ERROR);
			$errors = $this->instance->checkConfig($config);
			if (count($errors) > 0) {
				throw new \Exception("Config fausse : " . json_encode($errors, JSON_UNESCAPED_UNICODE));
			}
			$this->instance->setConfig($config);
		}
		return $this->instance;
	}
}
