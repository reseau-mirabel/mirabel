<?php

namespace processes\cron\ar;

/**
 * @property int $id
 * @property string $name
 * @property string $fqdn fully qualified class name
 * @property string $active the task will not be launched,
 * @property string $emailTo email addresses, separated by line feeds
 * @property int $lastUpdate
 */
class TaskSearch extends \CActiveRecord
{
	public const FILTER_MESSAGE = ['erreur', 'texte', 'silencieux'];

	/**
	 * @var null|"0"|"1" Was there an error in the last message?
	 */
	public ?string $lastError = null;

	public ?int $lastRunTime = null;

	/**
	 * @var null|"0"|"1" Was there a text output in the last message?
	 */
	public $lastRunMessage = null;

	public function tableName()
	{
		return "CronTask";
	}

	public function rules()
	{
		return [
			['active, lastRunMessage', 'boolean'],
			[['name', 'fqdn', 'emailTo'], 'length', 'max' => 255],
			['lastRunMessage', 'in', 'range' => self::FILTER_MESSAGE, 'allowEmpty' => true],
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => 'Nom',
			'fqdn' => 'Modèle',
			'active' => 'Active',
			'emailTo' => 'Email à',
			'timelimit' => 'Durée maxi (s)',
			'lastRunMessage' => "Silencieux",
			'lastRunTime' => 'Dernier passage',
			'lastUpdate' => 'Dernière modification',
		];
	}

	public function isEmpty(): bool
	{
		if ((string) $this->active !== '') {
			return false;
		}
		return ($this->emailTo === '' && $this->fqdn === '' && $this->name === '' && $this->lastRunMessage === null);
	}

	public function search(): \CActiveDataProvider
	{
		$criteria = new \CDbCriteria;
		$criteria->join = "LEFT JOIN (SELECT crontaskId, MAX(id) AS lastId FROM CronHistory GROUP BY crontaskId) l ON l.crontaskId = t.id ";
		$criteria->join .= "LEFT JOIN CronHistory h ON l.lastId = h.id";
		$criteria->select = "t.*, (h.error <> '') AS lastError, (h.reportEmpty = 0) AS lastRunMessage, h.startTime AS lastRunTime";
		if ($this->active === '1') {
			$criteria->addCondition('active = 1');
		} elseif ($this->active === '0') {
			$criteria->addCondition('active = 0');
		}
		switch ($this->lastRunMessage) {
			case 'erreur':
				$criteria->addCondition("IFNULL(h.error <> '', 0) > 0");
				break;
			case 'silencieux':
				$criteria->addCondition("h.reportEmpty = 1 AND IFNULL(h.error <> '', 0) = 0");
				break;
			case 'texte':
				$criteria->addCondition("h.reportEmpty = 0 OR IFNULL(h.error <> '', 0) > 0");
				break;
		}
		$criteria->compare('emailTo', $this->emailTo, true);
		if ($this->fqdn) {
			$pattern = $this->dbConnection->quoteValue("%" . str_replace(['%', '_'], '', $this->fqdn));
			$criteria->addCondition("fqdn LIKE $pattern");
		}
		$criteria->compare('name', $this->name, true);

		$sort = new \CSort();
		$sort->attributes = [
			'name',
			'lastUpdate',
			'lastRunTime' => [
				'asc' => 'h.startTime ASC',
				'desc' => 'h.startTime DESC',
			],
		];
		$sort->defaultOrder = ['name' => \CSort::SORT_ASC];

		return new \CActiveDataProvider(
			\processes\cron\ar\TaskSearch::model(),
			[
				'model' => self::model(),
				'criteria' => $criteria,
				'pagination' => false,
				'sort' => $sort,
			]
		);
	}

	public function listClassNames(): array
	{
		$fqdns = $this->dbConnection->createCommand("SELECT DISTINCT fqdn FROM CronTask")->queryColumn();
		$classes = [];
		foreach ($fqdns as $f) {
			$classes[] = (string) substr($f, strrpos($f, '\\Model') + 6);
		}
		if (!$classes) {
			return [];
		}
		sort($classes);
		return array_combine($classes, $classes);
	}
}
