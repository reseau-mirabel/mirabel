<?php

namespace processes\cron;

use components\email\Mailer;
use processes\cron\ar\Task;
use Yii;

class Cron
{
	public CronState $state;

	private bool $isRunInTty;

	public function __construct()
	{
		$this->state = new CronState();
		$this->isRunInTty = defined('STDERR') && is_resource(STDERR) && stream_isatty(STDERR);
	}

	/**
	 * Run the tasks that are expected to start in the next minutes.
	 */
	public function run(int $minutes): void
	{
		$tasks = Task::model()->findAll("active = 1 OR forceRun = 1");
		$toRun = $this->findTasksToRun($tasks, $minutes);
		if (!$toRun) {
			return;
		}
		if ($this->isRunInTty) {
			echo count($toRun) . " tâches détectés :\n";
			foreach ($toRun as $t) {
				echo "    [ID {$t->id}] {$t->name}\n";
			}
		}
		if (count($toRun) === 1) {
			$this->runTask($toRun[0]);
			return;
		}
		$this->runParallelTasks($toRun);
	}

	/**
	 * This function is public because it has to be called from a shutdown function.
	 */
	public static function sendEmail(Task $task, Report $report): void
	{
		if ($report->isEmpty()) {
			return;
		}
		if ($report->error === '' && !$report->sendEmail) {
			return;
		}
		if ($task->emailTo === '') {
			return;
		}

		$body = $report->getEmailBody();
		if (!$body) {
			return;
		}

		$subject = "[" . Yii::app()->name . "] ";
		if ($report->error !== '') {
			$subject .= "(ERREUR) ";
		}
		if ($task->emailSubject) {
			$subject .= $task->emailSubject;
		} elseif ($report->emailSubject) {
			$subject .= $report->emailSubject;
		} else {
			$subject .= $task->name;
		}
		$message = Mailer::newMail()
			->subject($subject)
			->from(\Config::read('email.from'))
			->setTo($task->emailTo)
			->text($body);
		foreach ($report->emailAttachments as $a) {
			$message->attach($a['content'], $a['name'], $a['mimeType']);
		}
		Mailer::sendMail($message);
	}

	/**
	 * @param Task[] $tasks
	 * @return Task[]
	 */
	private function findTasksToRun(array $tasks, int $minutes): array
	{
		if (!$tasks) {
			return [];
		}
		$scheduler = new Scheduler();
		$toRun = [];
		foreach ($tasks as $task) {
			if ($task->forceRun) {
				$toRun[] = $task;
			} else {
				$schedule = $task->getScheduleObject();
				if ($scheduler->isScheduledToRunNow($schedule, 60 * $minutes)) {
					$toRun[] = $task;
				}
			}
		}
		return $toRun;
	}

	/**
	 * Fork for each of the tasks to run.
	 *
	 * @param Task[] $tasks
	 */
	private function runParallelTasks(array $tasks): void
	{
		$pids = [];
		// Close resources, so that they are not shared (and closed when the first fork ends).
		Yii::app()->db->setActive(false);
		Yii::app()->getComponent('sphinx')->setActive(false);
		foreach ($tasks as $task) {
			$pid = pcntl_fork();
			if ($pid == -1) {
				throw new \Exception("Could not fork.");
			}
			if ($pid === 0) {
				// child process starts the task
				$this->runTask($task);
				exit();
			}
			// parent process stores the child's PID then continues
			$pids[] = $pid;
		}
		// Wait for children
		$status = 0;
		foreach ($pids as $pid) {
			pcntl_waitpid($pid, $status);
		}
	}

	private function runTask(Task $task): void
	{
		if ($this->state->hasLock($task)) {
			Yii::log("Task {$task->id} ({$task->name}) is blocked by a lock", 'warning', 'cron');
			if ($this->isRunInTty) {
				echo "[ID {$task->id}] blocked by a lock\n";
			}
			return;
		}

		if ($this->isRunInTty) {
			echo "[ID {$task->id}] Starting {$task->name}\n";
		}
		if ($task->timelimit > 0) {
			set_time_limit($task->timelimit);
		}
		$startTime = time();
		$this->state->lockStart($task, $startTime);
		if ($task->forceRun) {
			$task->forceRun = false;
			$task->update(['forceRun']);
		}

		// End cleanly in case of interruption by the time limit.
		// Does not apply to SIGTERM signals (Ctrl-c).
		register_shutdown_function(
			function (Task $task, CronState $state, int $startTime) {
				if (!$state->hasLock($task)) {
					return; // The task ended normally.
				}
				if ($task->timelimit > 0 && time() - $startTime > $task->timelimit) {
					$errorTitle = "Timeout";
					$error = "ERREUR : la durée maximale d'exécution a été atteinte.";
					$stdout = "";
				} else {
					$errorTitle = "Unknown error";
					$error = "ERREUR inconnue : le processus ne s'est pas terminé normalement.";
					ob_start();
					debug_print_backtrace(0, 8);
					$stdout = (string) ob_get_contents();
					ob_end_clean();
				}

				// Resume the normal process.
				if (ob_get_level() > 0) {
					$stdout .= ob_get_contents();
					ob_end_clean();
				}
				$report = new Report();
				$report->error = "$errorTitle\n$error";
				$report->body = $stdout;

				if (defined('STDERR') && is_resource(STDERR) && stream_isatty(STDERR)) {
					echo "[ID {$task->id}] ended with an error: $errorTitle\n";
				}
				if ($task->forceRun) {
					$state->endForcedRun($task);
				}
				$state->lockEnd($task);
				$state->writeHistory($task, $startTime, $report);

				\processes\cron\Cron::sendEmail($task, $report);
			},
			$task,
			$this->state,
			$startTime
		);

		$error = "";
		ob_start();
		$report = new Report();
		try {
			$report = $task->getTaskInstance()->run();
		} catch (StopException $e) {
			$report->error .= $e->getMessage();
		} catch (\Throwable $e) {
			Yii::log($e->getMessage() . "\n" . $e->getTraceAsString(), 'error', 'cron');
			$error = "ERREUR FATALE : {$e->getMessage()}";
			$maxLines = 8;
			foreach ($e->getTrace() as $data) {
				if (preg_match('#/CronCommand.php$#', $data['file'])) {
					break;
				}
				$error .= "\n{$data['file']}:{$data['line']}";
				if (!empty($data['function'])) {
					$className = empty($data['class']) ? "" : "{$data['class']}::";
					$error .= "  $className{$data['function']}()";
				}
				$maxLines--;
				if ($maxLines <= 0) {
					break;
				}
			}
			$report->error .= $error;
		}
		$stdout = (string) ob_get_contents();
		ob_end_clean();
		if ($stdout) {
			Yii::log("Cron task {$task->id} wrote on STDOUT", 'warning', 'cron');
			$report->body .= "\n## Affichage supplémentaire\n\n$stdout";
		}

		if ($this->isRunInTty) {
			echo "[ID {$task->id}] ended with " . ($error === '' ? "SUCCESS" : "ERROR") . "\n";
		}
		if ($task->forceRun) {
			$this->state->endForcedRun($task);
		}
		$this->state->lockEnd($task);
		$this->state->writeHistory($task, $startTime, $report);

		self::sendEmail($task, $report);
	}
}
