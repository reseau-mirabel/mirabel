<?php

namespace processes\cron\tasks;

use commands\models\sudoc\CheckByIssn;
use models\sudoc\ApiClient;
use processes\cron;
use processes\cron\ConfigParam;
use processes\cron\Report;

class ModelSudocCheck implements cron\ModelInterface
{
	private int $verbose = 1;

	private bool $simulation = false;

	private int $limit = 0;

	public static function getDescription(): string
	{
		return "Vérifie pour chaque ISSN le ppn présent sur le SUDOC et ajoute ou met a jour la valeur locale";
	}

	public function getUniqueIdentifer(): string
	{
		return "sudoc-check";
	}

	public static function getConfigHelp(): array
	{
		return [
			'verbose' => new ConfigParam("bool", "Verbeux", "Signale les ISSN sans PPN."),
			'simulation' => new ConfigParam("bool", "Simulation", "N'effectue pas les modifications si cochée"),
			'limit' => new ConfigParam("integer", "Limite de requêtes", "Nombre de requêtes maximum à envoyer au SUDOC"),
		];
	}

	public function setConfig(array $config): void
	{
		$this->verbose = (int) ($config['verbose'] ? 2 : 1);
		$this->simulation = (bool) ($config['simulation'] ?? false);
		$this->limit = (int) ($config['limit'] ?? 0);
	}

	public static function checkConfig(array $config): array
	{
		return [];
	}

	public function getConfigSummary(): array
	{
		$info = [
			'verbeux' => $this->verbose > 1 ? "bavard" : "silencieux",
			'simulation' => $this->simulation ? "oui" : "non",
		];
		if ($this->limit > 0) {
			$info['limite'] = "$this->limit requêtes max";
		}
		return $info;
	}

	public function run(): Report
	{
		$check = new CheckByIssn(new ApiClient);
		$check->setConfig([
			'limit' => (int) $this->limit,
			'simulation' => (bool) $this->simulation,
			'verbose' => (int) $this->verbose,
		]);
		$out = fopen('php://memory', 'w');
		$check->run($out);
		$output = (string) stream_get_contents($out, -1, 0);

		$report = new Report();
		$report->body = $output;
		$report->emailSubject = 'Cron M sudoc check';
		$report->attachToEmail($output, 'Mirabel_issn_ppn-check.tsv', "text/tab-separated-values");
		return $report;
	}
}
