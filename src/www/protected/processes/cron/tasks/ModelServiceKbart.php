<?php

namespace processes\cron\tasks;

use processes\cron\ConfigParam;
use processes\cron\Report;
use Yii;

class ModelServiceKbart implements \processes\cron\ModelInterface
{
	private const VERBOSE = ["Silencieux", "Normal", "Bavard"];

	protected int $ressourceId;

	/**
	 * @var int[]
	 */
	protected array $collectionIds;

	protected string $url;

	protected bool $ssiConnu;

	/**
	 * @var ""|"Intégral"|"Sommaire"
	 */
	protected string $contenu;

	/**
	 * @var ""|"Libre"|"Restreint"
	 */
	protected string $acces;

	protected bool $ignoreUrl;

	protected int $verbose = 1;

	public static function getDescription(): string
	{
		return "Import des accès en ligne contenus dans un fichier KBART";
	}

	public function getUniqueIdentifer(): string
	{
		// Block parallel imports in the same resource, even for distinct collections.
		return "service-kbart-import-{$this->ressourceId}";
	}

	public static function getConfigHelp(): array
	{
		return [
			'ressourceId' => new ConfigParam("string", "Ressource", "Identifiant numérique."),
			'collectionIds' => new ConfigParam("string", "Collections", "Identifiants numériques séparés par des espaces. Facultatif si <em>Ressource</em> est rempli."),
			'url' => new ConfigParam("string", "URL du KBART", "URL débutant par 'http'/'https', ou chemin absolu débutant par '/'. Le texte '{{TODAY}}' sera remplacé par la date ISO du jour, par exemple '2024-03-25'."),
			'contenu' => new ConfigParam("list", "Contenu par défaut", "Si l'information est absente du KBART, l'import utilisera cette valeur, ou ignorera la ligne s'il n'a pas de valeur",
				[['', ""], ["Indexation", "Indexation"], ["Intégral", "Intégral"], ["Sommaire", "Sommaire"]]
			),
			'acces' => new ConfigParam("list", "Accès par défaut", "Si l'information est absente du KBART, l'import utilisera cette valeur, ou ignorera la ligne s'il n'a pas de valeur",
				[['', ""], ["Libre", "Libre"], ["Restreint", "Restreint"]]
			),
			'ssiConnu' => new ConfigParam("bool", "Ignorer les titres inconnus", "Par défaut, cette case est décochée et une proposition de création du titre est enregistrée."),
			'ignoreUrl' => new ConfigParam("bool", "Ignorer les URLs", "Si cochée, les URL des accès en ligne ne seront pas lues dans le KBART."),
			'verbose' => new ConfigParam("list", "Niveau de détails", "", self::VERBOSE),
		];
	}

	public function getConfigSummary(): array
	{
		$summary = array_filter([
			"Ressource" => \Ressource::model()->findByPk($this->ressourceId)->getSelfLink(),
			'Collections' => $this->listCollectionsLinks(),
			'URL' => \CHtml::link(\CHtml::encode($this->url), $this->url),
			'Ignore les titres inconnus' => $this->ssiConnu ? "oui" : "non",
			"Contenu par défaut" => $this->contenu,
			"Accès par défaut" => $this->acces,
		]);
		if ($this->verbose !== 1) {
			$summary["Niveau de détails"] = "{$this->verbose} (de 0 à 2)";
		}
		return $summary;
	}

	public function setConfig(array $config): void
	{
		$this->ressourceId = (int) $config['ressourceId'];
		$this->collectionIds = array_filter(array_map('intval', explode(' ', $config['collectionIds'] ?? '')));
		$this->url = $config['url'];
		$this->contenu = ($config['contenu'] ?? '');
		$this->acces = ($config['acces'] ?? '');
		$this->ssiConnu = (bool) ($config['ssiConnu'] ?? false);
		$this->ignoreUrl = (bool) ($config['ignoreUrl'] ?? false);
		if (empty($config['verbose'])) {
			$this->verbose = 1;
		} else {
			if ((int) $config['verbose'] > 0) {
				$this->verbose = (int) $config['verbose'];
			} else {
				$flipped = array_flip(self::VERBOSE);
				$this->verbose = $flipped[$config['verbose']] ?? 1;
			}
		}
	}

	public static function checkConfig(array $config): array
	{
		$errors = [];
		if (empty($config['ressourceId'])) {
			$errors['ressourceId'] = "Il faut sélectionner une ressource.";
		}
		if (!empty($config['collectionIds'])) {
			if (preg_match('/^[\d ]+$/', $config['collectionIds'])) {
				$collectionIds = array_filter(array_map('intval', explode(' ', $config['collectionIds'])));
				$ressourceIds = Yii::app()->db
					->createCommand("SELECT DISTINCT ressourceId FROM Collection WHERE id IN (" . join(",", $collectionIds) . ")")
					->queryColumn();
				if (count($ressourceIds) !== 1 || (int) $ressourceIds[0] !== (int) $config['ressourceId']) {
					$errors['collectionIds'] = "Les collections doivent toutes appartenir à la ressource.";
				}
			} else {
				$errors['collectionIds'] = "Écrire des identifiants numériques séparés par des espaces.";
			}
		}
		if (!empty($config['url'])) {
			if ($config['url'][0] === '/') {
				/*
				$path = str_replace('{{TODAY}}', date('Y-m-d'), $config['url']);
				if (!is_readable($path)) {
					$errors['url'] = "Le chemin '$path' n'est pas un fichier accessible en lecture.";
				}
				 */
			} elseif (strncmp($config['url'], 'http', 4) !== 0 && !is_readable($config['url'])) {
				$errors['url'] = "Ce champ doit contenir une URL ou un chemin vers un fichier lisible.";
			}
		}
		return $errors;
	}

	public function run(): Report
	{
		require_once \Yii::getPathOfAlias('application.models.import') . '/ImportKbart.php';
		$url = str_replace('{{TODAY}}', date('Y-m-d'), $this->url);
		$params = [
			'url' => $url,
			'ressourceId' => $this->ressourceId,
			'collectionId' => $this->collectionIds ? $this->collectionIds[0] : null,
			'defaultType' => $this->contenu,
			'acces' => $this->acces,
			'ignoreUnknownTitles' => (int) $this->ssiConnu,
			'ignoreUrl' => $this->ignoreUrl,
		];
		$import = new \ImportKbart($params);
		$import->verbose = $this->verbose;
		$import->importAll();

		$report = new Report();
		if ($this->verbose > 1 || !$import->log->isEmpty()) {
			$report->title = $import->log->getSummary();
			$report->body = (str_starts_with($this->url, 'http') ? "Source : {$this->url}\n\n" : "")
				. $import->log->format('string');
		}
		$report->emailSubject = "Import KBART " . Yii::app()->db->createCommand("SELECT nom FROM Ressource WHERE id = {$this->ressourceId}")->queryScalar();
		if ($this->collectionIds) {
			$report->emailSubject .= " [{$this->getCollectionNames()}]";
		}
		return $report;
	}

	/**
	 * @return string[] HTML links
	 */
	protected function listCollectionsLinks(): array
	{
		if (!$this->collectionIds) {
			return [];
		}
		$collections = [];
		foreach (\Collection::model()->findAllByPk($this->collectionIds) as $c) {
			$collections[] = $c->getSelfLink(false); // without the resource name
		}
		return $collections;
	}

	protected function getCollectionNames(): string
	{
		if (!$this->collectionIds) {
			return "";
		}
		return Yii::app()->db
			->createCommand("SELECT GROUP_CONCAT(nom) FROM Collection WHERE id IN (" . join(",", $this->collectionIds) . ")")
			->queryScalar();
	}
}
