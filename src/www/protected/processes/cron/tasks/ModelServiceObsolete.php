<?php

namespace processes\cron\tasks;

use processes\cron\ConfigParam;
use processes\cron\Report;
use processes\service\VanishedServicesNotify;

class ModelServiceObsolete implements \processes\cron\ModelInterface
{
	private string $since = '';

	private bool $verbose = false;

	public static function getDescription(): string
	{
		return <<<EOHTML
			Détecte les accès jamais importés alors qu'ils devraient l'être, ou importés mais pas récemment.
			S'applique à toutes les ressources.
			<br>
			Envoie des courriels aux partenaires-veilleurs suivant ces données,
			ou à une adresse par défaut si personne ne suit.
			EOHTML;
	}

	public function getUniqueIdentifer(): string
	{
		return "service-obsolete";
	}

	public static function getConfigHelp(): array
	{
		return [
			'since' => new ConfigParam("string", "Durée d'obsolescence", "Après quelle absence un lien devient-il obsolète ? Par exemple, <code>15 days ago</code> ou <code>-2 weeks</code>"),
			'verbose' => new ConfigParam("bool", "Rapport détaillé", "Affiche dans le rapport de la tâche le contenu de tous les messages envoyés"),
		];
	}

	public function getConfigSummary(): array
	{
		return [
			"Obsolète" => "Liste les liens obsolètes car absents depuis « {$this->since} »",
			"Verbeux" => ($this->verbose ? "rapport exhaustif" : "silencieux"),
		];
	}

	public function setConfig(array $config): void
	{
		$this->since = (string) ($config['since'] ?? '');
		$this->verbose = (bool) ($config['verbose'] ?? false);
	}

	public static function checkConfig(array $config): array
	{
		$errors = [];
		if (!empty($config['since'])) {
			$since = (int) strtotime($config['since']);
			if ($since === 0 || $since >= time()) {
				$errors['obsoleteSince'] = "Format non valide";
			}
		}
		return $errors;
	}

	public function run(): Report
	{
		$p = new VanishedServicesNotify($this->since);
		$p->setVerbose(1);
		$p->run(0); // Toutes les ressources

		$report = new Report();
		if ($this->verbose) {
			$report->format = \processes\cron\ar\History::REPORTFORMAT_HTML;
			$report->body = $p->getHtmlReport();
		}
		$p->sendEmails();
		return $report;
	}
}
