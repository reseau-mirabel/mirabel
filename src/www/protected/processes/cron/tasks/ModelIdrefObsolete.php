<?php

namespace processes\cron\tasks;

use components\Curl;
use Exception;
use processes\cron\ConfigParam;
use processes\cron\ModelInterface;
use processes\cron\Report;
use Editeur;
use Yii;

class ModelIdrefObsolete implements ModelInterface
{
	private const BATCH_SIZE = 50;

	private int $verbose = 0;

	private int $limit = 0;

	public static function getDescription(): string
	{
		return "Interroge l'API Idref et fournit la liste des éditeurs de Mirabel qui y sont fusionnés.";
	}

	public function getUniqueIdentifer(): string
	{
		return "idref-obsolete";
	}

	public static function getConfigHelp(): array
	{
		return [
			'limit' => new ConfigParam("integer", "Limite nombre éditeurs", "Nombre maximum d'éditeurs à vérifier."),
			'verbose' => new ConfigParam("bool", "Verbeux", "Affiche un message même s'il n'y a pas de fusion d'éditeurs."),
		];
	}

	public function setConfig(array $config): void
	{
		$this->limit = (int) ($config['limit'] ?? 0);
		$this->verbose = (int) ($config['verbose'] ?? 0);
	}

	public static function checkConfig(array $config): array
	{
		return [];
	}

	public function getConfigSummary(): array
	{
		$info = [
			'verbeux' => $this->verbose > 1 ? "bavard" : "silencieux",
		];
		if ($this->limit > 0) {
			$info['limite'] = "$this->limit éditeurs max";
		}
		return $info;
	}

	public function run(): Report
	{
		$report = new Report();
		$report->emailSubject = "'Cron M Vérification IdRef fusionnés";
		$merged = $this->getIdrefMerged($this->limit);
		if (empty($merged)) {
			if ($this->verbose) {
				$report->body = "Aucun éditeur fusionné.\n";
			}
			return $report;
		}
		$csv = fopen('php://memory', 'w');
		fputcsv($csv, ["PPN initial", "ID éditeur initial", "éditeur initial", "fusion vers le PPN", "éditeur fusionné"], ';', '"', '\\');
		foreach ($merged as $origin => $target) {
			$editeur = Editeur::model()->findByAttributes(['idref' => $origin]);
			$cible = Editeur::model()->findByAttributes(['idref' => $target]);
			$row = [$origin, $editeur->id, $editeur->nom, "fusionné dans " . $target, $cible->nom ?? 'INCONNU'];
			fputcsv($csv, $row, ';', '"', '\\');
		}
		$report->body = (string) stream_get_contents($csv, -1, 0);
		return $report;
	}

	private function getIdrefMerged(int $limit): array
	{
		$wsurl = 'https://www.idref.fr/services/merged/%s&format=text/json';

		$sql = "SELECT idref FROM Editeur WHERE idref IS NOT NULL" . ($limit > 0 ? " LIMIT $limit" : '');
		$records = Yii::app()->db->createCommand($sql)->queryColumn();
		$recordsbatches = array_chunk($records, self::BATCH_SIZE, false);
		$curl = new Curl;
		$curl->setopt(CURLOPT_TIMEOUT, 10); // seconds

		$res = [];
		foreach ($recordsbatches as $idrefs) {
			$idreflist = join(',', $idrefs);
			$qurl = sprintf($wsurl, $idreflist);
			$curlget = $curl->get($qurl);
			if ($curlget->getHttpCode() >= 400) {
				throw new Exception("Erreur de téléchargement pour '$qurl' : HTTP {$curl->getHttpCode()}");
			}
			$jdata = json_decode($curlget->getContent(), true);
			if ($jdata['sudoc'] == null) {
				continue;
			}
			if (count($jdata['sudoc']) == 1) {
				$data = ['query' => $jdata['sudoc']];
			} else {
				$data = $jdata['sudoc'];
			}
			foreach ($data as $item) {
				$res[$item['query']['ppn']] = $item['query']['result']['ppn'];
			}
		}
		return $res;
	}
}
