<?php

namespace processes\cron\tasks;

use processes\cron;
use processes\cron\ConfigParam;
use processes\cron\Report;
use processes\intervention\MailAlert;

class ModelAlert implements cron\ModelInterface
{
	private const INTERVENTION = [
		'all' => "TOUT",
		'nonSuivi' => "non-suivi",
		'suivi' => "suivi",
		'partenairesEditeurs' => "partenaires-éditeurs",
	];

	private const VERBOSITY = ["erreurs", "décompte des envois", "liste des destinataires"];

	private int $verbose = 0;

	private bool $simulation = false;

	private string $before = "";

	private string $since = "";

	/**
	 * @var "all"|"nonsuivi"|"partenaireEditeurs"
	 */
	private string $intervention = "all";

	private string $ccToPartEd;

	private string $ccToPartVe;

	private string $cc;

	public static function getDescription(): string
	{
		return "Envoi de courriel d'alerte pour les interventions en attente";
	}

	public function getUniqueIdentifer(): string
	{
		return "alert-{$this->intervention}"
			. ($this->since !== "" ? "-since-" . str_replace(" ", "-", $this->since) : "")
			. ($this->before !== "" ? "-before-" . str_replace(" ", "-", $this->before) : "");
	}

	public static function getConfigHelp(): array
	{
		return [
			'intervention' => new ConfigParam("list",
				"Interventions à notifier",
				"<b>suivi</b> : Interventions en attente liées à un objet suivi par un partenaire. Les messages sont envoyés aux adresses du champ contact du partenaire suiveur et au CC. Les champs \"CC editeur\" ou \"CC veilleur\" permettent de remplacer les CC par défaut, selon le type du partenaire suiveur.
				<br><b>nonSuivi</b> : Interventions en attente liées à des objets non suivis et proposées par des partenaires non-éditeur. Les messages sont envoyés aux utilisateurs avec 'Suivi des objets non suivis' et au CC.
				<br><b>partenaireEditeurs</b> : Interventions en attentes proposées par des partenaires éditeurs. Les messages sont envoyés aux utilisateurs avec 'Suivi des Editeurs' et au CC.",
				self::INTERVENTION
			),
			'since' => new ConfigParam("string", "Interventions postérieures à cette date", "", placeholder: "10 days ago"),
			'before' => new ConfigParam("string", "Interventions antérieures à cette date", "", placeholder: "2 months ago"),
			'verbose' => new ConfigParam("list", "Verbeux", "Niveau de détail du rapport (sans impact sur les envois)", self::VERBOSITY),
			'simulation' => new ConfigParam("bool", "Simulation", "N'envoie pas de courriels de suivi si cochée"),
			'cc' => new ConfigParam("string", "CC", "Destinataires en copie par défaut.", placeholder: "Adresses séparées par des virgules."),
			'ccToPartVe' => new ConfigParam("string", "CC si partenaire veilleur", "Remplace CC pour les interventions suivies par des partenaires veilleurs.", placeholder: "Adresses séparées par des virgules."),
			'ccToPartDe' => new ConfigParam("string", "CC si partenaire éditeur", "Remplace CC pour les interventions suivies par des partenaires éditeurs.", placeholder: "Adresses séparées par des virgules."),
		];
	}

	public function setConfig(array $config): void
	{
		$this->intervention = (string) $config['intervention'];
		$this->since = (string) ($config['since'] ?? "");
		$this->before = (string) ($config['before'] ?? "");
		$this->verbose = (int) ($config['verbose'] ?? 0);
		$this->simulation = (bool) ($config['simulation'] ?? false);
		$this->ccToPartVe = (string) ($config['ccToPartVe'] ?? "");
		$this->ccToPartEd = (string) ($config['ccToPartEd'] ?? "");
		$this->cc = (string) ($config['cc'] ?? "");
	}

	public static function checkConfig(array $config): array
	{
		$errors = [];
		if (empty($config['intervention'])) {
			$errors['intervention'] = "Il faut choisir les interventions à notifier";
			return $errors;
		}
		if (!isset(self::INTERVENTION[$config['intervention']])) {
			$errors['intervention'] = "Le type d'intervention à notifier doit être une des valeurs permises";
			return $errors;
		}
		return $errors;
	}

	public function getConfigSummary(): array
	{
		return [
			'intervention' => self::INTERVENTION[$this->intervention] ?? '???',
			'since' => $this->since,
			'before' => $this->before,
			'verbose' => self::VERBOSITY[$this->verbose] ?? "???",
			'simulation' => $this->simulation ? "oui" : "non",
			'ccToPartVe' => $this->ccToPartVe,
			'ccToPartEd' => $this->ccToPartEd,
			'cc' => $this->cc,
		];
	}

	public function run(): Report
	{
		$mailAlert = new MailAlert($this->since, $this->before, $this->simulation);
		$mailAlert->setRecipients($this->ccToPartEd, $this->ccToPartVe, $this->cc);
		switch ($this->intervention) { // "TOUT", "non-suivi", "suivi", "partenaires-éditeurs"
			case "suivi":
				$mailAlert->alertInterventionSuivi();
				break;
			case "nonSuivi":
				$mailAlert->alertInterventionNonSuivi();
				break;
			case "partenairesEditeurs":
				$mailAlert->alertInterventionPartenaireEditeurs();
				break;
			case "all":
				$mailAlert->alertInterventionSuivi();
				$mailAlert->alertInterventionNonSuivi();
				$mailAlert->alertInterventionPartenaireEditeurs();
				break;
			default:
				throw new \Exception("La valeur de 'intervention' dans la configuration n'est pas reconnue.");
		}

		$level = match ($this->verbose) {
			0 => \CLogger::LEVEL_WARNING,
			1 => \CLogger::LEVEL_INFO,
			2 => \CLogger::LEVEL_TRACE,
			default => \CLogger::LEVEL_WARNING,
		};

		$report = new Report();
		$report->body = $mailAlert->getLogString($level);
		$report->emailSubject = "interventions en attente";
		return $report;
	}
}
