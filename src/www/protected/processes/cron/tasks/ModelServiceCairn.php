<?php

namespace processes\cron\tasks;

use processes\cron\ConfigParam;
use processes\cron\Report;
use Yii;

class ModelServiceCairn implements \processes\cron\ModelInterface
{
	private const VERBOSE = ["Silencieux", "Normal", "Bavard"];

	protected string $url;

	protected int $verbose = 1;

	public static function getDescription(): string
	{
		return "Import des accès en ligne de Cairn";
	}

	public function getUniqueIdentifer(): string
	{
		// Block parallel imports in the same resource, even for distinct collections.
		return "service-cairn";
	}

	public static function getConfigHelp(): array
	{
		return [
			'url' => new ConfigParam("string", "URL du CSV", "URL débutant par 'http'/'https', ou chemin absolu débutant par '/'"),
			'verbose' => new ConfigParam("list", "Niveau de détails", "", self::VERBOSE),
		];
	}

	public function getConfigSummary(): array
	{
		$summary = array_filter([
			'URL' => \CHtml::link(\CHtml::encode($this->url), $this->url),
		]);
		if ($this->verbose !== 1) {
			$summary["Niveau de détails"] = "{$this->verbose} (de 0-silencieux à 2-bavard)";
		}
		return $summary;
	}

	public function setConfig(array $config): void
	{
		$this->url = $config['url'];
		if (empty($config['verbose'])) {
			$this->verbose = 1;
		} else {
			if ((int) $config['verbose'] > 0) {
				$this->verbose = (int) $config['verbose'];
			} else {
				$flipped = array_flip(self::VERBOSE);
				$this->verbose = $flipped[$config['verbose']] ?? 1;
			}
		}
	}

	public static function checkConfig(array $config): array
	{
		$errors = [];
		if (empty($config['url'])) {
			$errors['url'] = "Ce champ doit contenir une URL ou un chemin vers un fichier lisible.";
		} elseif (strncmp($config['url'], 'http', 4) !== 0 && !is_readable($config['url'])) {
			$errors['url'] = "Ce champ doit contenir une URL ou un chemin vers un fichier lisible.";
		}
		return $errors;
	}

	public function run(): Report
	{
		Yii::import('application.models.import.ImportCairn');
		$import = new \ImportCairn(['url' => $this->url]);
		$import->verbose = $this->verbose;
		$import->importAll();

		$report = new Report();
		if (!$import->log->isEmpty()) {
			$report->title = $import->log->getSummary();
			$report->body = $import->log->format('string');
		}
		return $report;
	}
}
