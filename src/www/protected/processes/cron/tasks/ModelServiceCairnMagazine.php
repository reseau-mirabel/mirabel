<?php

namespace processes\cron\tasks;

use processes\cron\Report;
use Yii;

class ModelServiceCairnMagazine implements \processes\cron\ModelInterface
{
	use NoConfig;

	public static function getDescription(): string
	{
		return "Import des accès en ligne de Cairn Magazine";
	}

	public function run(): Report
	{
		Yii::import('application.models.import.ImportCairnMagazine');
		$import = new \ImportCairnMagazine();
		$import->verbose = 1;
		$import->importAll();

		$report = new Report();
		if (!$import->log->isEmpty()) {
			$report->title = $import->log->getSummary();
			$report->body = $import->log->format('string');
		}
		return $report;
	}
}
