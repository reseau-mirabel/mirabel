<?php

namespace processes\cron\tasks;

use processes\cron\Report;
use processes\issn\IssnlImport;
use processes\issn\IssnOrgCheck;
use processes\issn\IssnOrgFromFile;
use processes\issn\IssnOrgZip;

class ModelIssnOrg implements \processes\cron\ModelInterface
{
	use NoConfig;

	public static function getDescription(): string
	{
		return <<<HTML
			<ol>
				<li>Télécharge le zip des ISSN et ISSN-L de issn.org.</li>
				<li>Pour chaque ISSN-L de M, liste les incohérences d'ISSN entre Mir@bel et issn.org</li>
				<li>Ajoute les données de issn.org dans Mir@bel : ISSN-P, ISSN-E et ISSN-L</li>
			</ol>
			HTML;
	}

	public function getUniqueIdentifer(): string
	{
		// Block parallel runs of this task.
		return "issnorg";
	}

	public function run(): Report
	{
		$report = new Report();
		$report->emailSubject = "Cron M ISSN-L";

		['level' => $level, 'msg' => $msg] = $this->download();
		if ($level === 'error') {
			$report->error = $msg;
			return $report;
		}
		if ($level === 'warning') {
			$report->body = "[Attention] $msg\n\n";
		}
		IssnlImport::rebuildIfMissing();

		$report->body .= "## Incohérences entre M et issn.org pour les ISSN valides de M (recherche par ISSN-L de M)\n";
		$checker = new IssnOrgCheck();
		$checker->addHeader();
		$checker->check();
		$checkerOutput = $checker->getOutput();
		$report->body .= $checkerOutput;
		$report->attachToEmail($checkerOutput, "Mirabel_issn-incoherents.tsv", "text/tab-separated-values");

		$report->body .= "\n\n## Ajout de données issn.org dans M\n";
		$issnorg = new IssnOrgFromFile();
		$issnorg->fillIssnl();
		$issnorg->fillIssn();
		$output = $issnorg->getOutput();
		$report->body .= $output;
		$report->attachToEmail($output, "Mirabel_issn-ajouts.tsv", "text/tab-separated-values");

		$report->title = sprintf(
			"%d incohérences détectées, %d ISSN manquants (ajoutés sauf conflit)",
			substr_count($checkerOutput, "\n") - 1, // TSV lines - header line
			substr_count($output, "\n"),
		);
		return $report;
	}

	/**
	 * @return array{level: "info"|"warning"|"error", msg: string}
	 */
	private function download(): array
	{
		\Yii::app()->db->setActive(false); // Do not hold on a SQL connection we don't need.
		$result = [
			'level' => 'info',
			'msg' => "",
		];
		$downloader = new IssnOrgZip();
		if ($downloader->getFileTime() > time() - 86400) {
			$result['msg'] = "Téléchargement annulé car le fichier existe et date de moins d'un jour.";
			return $result;
		}
		$downloaded = $downloader->downloadIssnorgZip(true);
		$tries = 1;
		while (!$downloaded && $tries < 3) {
			// wait 1 minute and download again
			sleep(60);
			$downloaded = $downloader->downloadIssnorgZip(false);
			$tries++;
		}
		if ($downloaded) {
			$i = new IssnlImport();
			$i->import();
			$result['msg'] = "Téléchargement réussi";
		} else {
			$message = "Le téléchargement du zip d'issn.org a échoué ($tries tentatives) :\n{$downloader->getLastError()}\n";
			if ($downloader->restoreZip()) {
				$result['level'] = 'warning';
				$result['msg'] = "$message Cet import utilise un ancien fichier provenant d'un précédant téléchargement.";
			} else {
				$result['level'] = 'error';
				$result['msg'] = "$message Arrêt faute de zip restant d'un précédent téléchargement.";
			}
		}
		\Yii::app()->db->setActive(true);
		return $result;
	}
}
