<?php

namespace processes\cron\tasks;

use processes\cron\Report;
use processes\cron\ConfigParam;
use processes\issn\IssnOrgWebImport;

class ModelIssnPortal implements \processes\cron\ModelInterface
{
	public bool $onlyEmpty = false;

	public static function getDescription(): string
	{
		return <<<HTML
			<p>Interroge le site web portal.issn.org pour chaque ISSN de Mir@bel,
			et met à jour les champs "pays", "support", "titre clé" s'ils diffèrent.</p>
			<p>Attention, cette tâche dure longtemps et envoie plus de 50k requêtes web.</p>
			HTML;
	}

	public function getUniqueIdentifer(): string
	{
		// Block parallel runs of this task.
		return "issnportal";
	}

	/**
	 * @return array<string, \processes\cron\ConfigParam>
	 */
	public static function getConfigHelp(): array
	{
		return [
			'onlyEmpty' => new ConfigParam('bool', "Uniquement les ISSN sans pays", "Si coché, le portail ISSN ne sera interrogé que pour les ISSN qui n'ont pas de pays dans Mir@bel."),
		];
	}

	/**
	 * @return array<string, string|string[]>
	 */
	public function getConfigSummary(): array
	{
		return [
			'Portée' => $this->onlyEmpty ? "Uniquement les ISSN sans pays" : "Tous les ISSN de Mir@bel",
		];
	}

	public function setConfig(array $config): void
	{
		$this->onlyEmpty = (bool) ($config['onlyEmpty'] ?? false);
	}

	public static function checkConfig(array $config): array
	{
		return [];
	}

	public function run(): Report
	{
		$import = new IssnOrgWebImport();
		$import->run($this->onlyEmpty);

		$report = new Report();
		if ($import->countErrors() > 0 || $import->countUpdates() > 0) {
			$csv = $import->getCsv();
			$report->title = "Liste des {$import->countUpdates()} modifications";
			if ($import->countErrors()) {
				$report->title .= " et {$import->countErrors()} erreurs";
			}
			$report->body = $csv;
			$report->attachToEmail($csv, "ISSN-Portal_country-updates.csv", "text/csv");
		}
		return $report;
	}
}
