<?php

namespace processes\cron\tasks;

use processes\cron;
use processes\cron\ConfigParam;
use processes\cron\Report;

class ModelSleep implements cron\ModelInterface
{
	private int $sleep = 0;

	public static function getDescription(): string
	{
		return "Ne fait rien. Utile pour tester le déclenchement des tâches, les verrous, l'historique.";
	}

	public function getUniqueIdentifer(): string
	{
		// Block parallel runs of this task.
		return "sleep";
	}

	public static function getConfigHelp(): array
	{
		return [
			'sleep' => new ConfigParam("integer", "Attente", "Durée d'exécution, en secondes. Utile pour tester qu'une instance de cette tâche ne peut démarrer quand une autre est en cours."),
		];
	}

	public function getConfigSummary(): array
	{
		return [
			"Attente" => "{$this->sleep} secondes",
		];
	}

	public function setConfig(array $config): void
	{
		$this->sleep = (int) $config['sleep'];
	}

	public static function checkConfig(array $config): array
	{
		$errors = [];
		if (!is_int($config['sleep']) || $config['sleep'] < 0) {
			$errors['sleep'] = "Indiquer une durée en secondes.";
		}
		return $errors;
	}

	public function run(): Report
	{
		if ($this->sleep > 0) {
			sleep($this->sleep);
		}
		$r = new Report();
		$r->emailSubject = "sleep";
		$r->emailBody = "Ce texte est fixé par la tâche 'sleep' dans Mir@bel.\nElle a pour but de tester le système de cron.\n";
		return $r;
	}
}
