<?php

namespace processes\cron\tasks;

use AttributImportLog;
use CHtml;
use components\curl\Downloader;
use processes\attribut\FileImport;
use processes\attribut\ImportConfig;
use processes\cron\ConfigParam;
use processes\cron\Report;
use processes\cron\StopException;
use Sourceattribut;
use Yii;

class ModelAttributImport implements \processes\cron\ModelInterface
{
	private string $url;

	private int $sourceattributId;

	private string $config;

	public static function getDescription(): string
	{
		return "Import d'attributs depuis une URL";
	}

	public function getUniqueIdentifer(): string
	{
		return "attribut-{$this->sourceattributId}";
	}

	public static function getConfigHelp(): array
	{
		return [
			'url' => new ConfigParam("string", "URL où lire les données", ""),
			'config' => new ConfigParam("text", "Config", "Bloc de texte (JSON) fourni par l'import web d'attributs"),
		];
	}

	public function getConfigSummary(): array
	{
		return array_filter([
			'URL' => CHtml::link(CHtml::encode($this->url), $this->url),
			'config' => "<pre>" . CHtml::encode(json_encode(json_decode($this->config), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)) . "</pre>",
		]);
	}

	public function setConfig(array $config): void
	{
		$this->url = $config['url'];
		$this->config = $config['config'];
		$dec = json_decode($this->config, false);
		$this->sourceattributId = $dec->sourceattributId;
	}

	public static function checkConfig(array $config): array
	{
		$errors = [];
		if (empty($config['url'])) {
			$errors['url'] = "Ce champ est nécessaire.";
		} elseif (strncmp($config['url'], 'http', 4) !== 0) {
			$errors['url'] = "Ce champ doit contenir une URL (commençant par 'http').";
		}
		if (empty($config['config'])) {
			$errors['config'] = "Ce champ est nécessaire.";
		} elseif (json_decode($config['config']) === null) {
			$errors['config'] = "Ce champ doit contenir un bloc JSON valide.";
		}
		return $errors;
	}

	public function run(): Report
	{
		$parsedConfig = json_decode($this->config, true, 512, JSON_THROW_ON_ERROR);
		$iconfig = new ImportConfig($parsedConfig);

		$attribut = Sourceattribut::model()->findByPk($this->sourceattributId);
		if (!($attribut instanceof Sourceattribut)) {
			throw new StopException("Aucun attribut n'a cet identifiant : {$this->sourceattributId}");
		}

		$log = new AttributImportLog();
		$log->sourceattributId = (int) $attribut->id;
		$log->userId = null;
		self::storeFile($this->url, $log);

		$process = new FileImport($log, $iconfig);
		if (!$process->save()) {
			throw new StopException("Erreur lors de l'enregistrement.");
		}
		$log->logtime = time();
		$log->save(false);

		$report = new Report();
		$report->emailSubject = "Attribut "
			. Yii::app()->db->createCommand("SELECT nom FROM Sourceattribut WHERE id = {$this->sourceattributId}")->queryScalar();
		$upstreamReport = $process->getReport();
		if ($upstreamReport->numChanges > 0 || $upstreamReport->numDeletes > 0) {
			$report->title = sprintf(
				"%d valeurs enregistrées, %d valeurs supprimées\n",
				$upstreamReport->numChanges,
				$upstreamReport->numDeletes
			);
			$report->body = sprintf(
				"%s/attribut/report/%d\n",
				Yii::app()->params->itemAt('baseUrl'),
				$process->getLog()->id
			);
		}
		return $report;
	}

	private static function storeFile(string $uri, AttributImportLog $log): bool
	{
		if (strncmp($uri, "http", 4) === 0) {
			$downloader = new Downloader('tableur');
			try {
				$downloader->download($uri);
				$sourceFile = $downloader->getLocalFilepath();
				$log->url = $uri;
				$log->fileName = $downloader->getRemoteFilename();
				$log->fileKey = FileImport::storeFile($sourceFile);
			} catch (\Throwable $e) {
				throw new StopException("Erreur lors du téléchargement : {$e->getMessage()}");
			}
		} else {
			$tmpFile = tempnam(sys_get_temp_dir(), 'mirabel_');
			copy($uri, $tmpFile);
			$log->url = '';
			$log->fileName = basename($uri);
			$log->fileKey = FileImport::storeFile($tmpFile, \CFileHelper::getExtension($uri));
		}
		if (!$log->fileKey) {
			throw new StopException("Erreur, données incomplètes.");
		}
		return true;
	}
}
