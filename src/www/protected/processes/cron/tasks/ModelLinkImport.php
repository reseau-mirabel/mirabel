<?php

namespace processes\cron\tasks;

use commands\models\linkimport;
use processes\cron\ConfigParam;
use processes\cron\Report;
use Yii;

class ModelLinkImport implements \processes\cron\ModelInterface
{
	/**
	 * Sourcelien.shortname => FQDN
	 */
	public const SOURCES = [
		"cairnint" => linkimport\CairnInt::class,
		"cairnmundo" => linkimport\CairnMundo::class,
		"doaj" => linkimport\Doaj::class,
		"erihplus" => linkimport\Erih::class,
		"ezb" => linkimport\Ezb::class,
		"hal" => linkimport\Hal::class,
		"latindex" => linkimport\Latindex::class,
		"miar" => linkimport\Miar::class,
		"openalex" => linkimport\Openalex::class,
		"road" => linkimport\Road::class,
		"scopus" => linkimport\Scopus::class,
		"sherpa" => linkimport\Sherpa::class,
		"wikipedia" => linkimport\Wikipedia::class,
		"wos" => linkimport\Wos::class,
	];

	private string $source;

	private bool $obsoleteDelete = false;

	private string $obsoleteSince = "";

	private int $cacheDuration = 0;

	private string $url = "";

	private int $verbose = 0;

	public static function getDescription(): string
	{
		return "Import des liens externes pour une des sources pré-définies (DOAJ, ROAD, etc)";
	}

	public function getUniqueIdentifer(): string
	{
		return "link-import-{$this->source}";
	}

	public static function getConfigHelp(): array
	{
		$sources = array_combine(array_keys(self::SOURCES), array_keys(self::SOURCES));
		return [
			'source' => new ConfigParam("list", "Source des liens", "Seules les sources ayant un code d'import automatique sont présentes.", $sources),
			'obsoleteDelete' => new ConfigParam("bool", "Supprimer les liens obsolètes à la fin de l'import", ""),
			'obsoleteSince' => new ConfigParam("string", "Durée d'obsolescence", "Après quelle absence un lien devient-il obsolète ? Par exemple, <code>15 days ago</code>"),
			'cacheDuration' => new ConfigParam("integer", "Durée cache fichiers", "Durée en jours du cache des fichiers téléchargés (temporisation). Si 0, le cache est de une heure.", ['min' => 0]),
			'url' => new ConfigParam("string", "URL", "Nécessaire uniquement pour MIAR et WOS."),
			'verbose' => new ConfigParam("list", "Niveau de détails", "", ["Normal", "Bavard", "Débogage"]),
		];
	}

	public function getConfigSummary(): array
	{
		$summary = [
			"Source des liens" => $this->source,
		];
		if ($this->obsoleteSince) {
			$summary["Liens obsolètes"] = "Signalement si absents depuis « {$this->obsoleteSince} »";
		}
		if ($this->obsoleteDelete) {
			$summary["Suppression /obsolète"] = "Suppression des liens obsolètes à la fin de l'import";
		}
		if ($this->cacheDuration > 0) {
			$summary["Temporisation"] = "{$this->cacheDuration} jours de conservation du fichier téléchargé";
		}
		$fqdn = self::SOURCES[$this->source];
		$lastDld = $fqdn::getLastDownloadDate();
		if ($lastDld) {
			$summary["(info) Dernier téléchargement"] = $lastDld;
		}
		if ($this->url) {
			$summary["URL"] = \CHtml::link(\CHtml::encode($this->url), $this->url);
		}
		if ($this->verbose > 0) {
			$summary["Niveau de détails"] = "{$this->verbose} / 0..2";
		}
		return $summary;
	}

	public function setConfig(array $config): void
	{
		$this->source = $config['source'] ?? '';
		$this->obsoleteDelete = (bool) ($config['obsoleteDelete'] ?? false);
		$this->obsoleteSince = (string) ($config['obsoleteSince'] ?? '');
		$this->cacheDuration = (int) ($config['cacheDuration'] ?? '');
		$this->url = (string) ($config['url'] ?? '');
		$verbosity = [
			'Normal' => 0,
			'Bavard' => 1,
			'Débogage' => 2,
		];
		if (isset($config['verbose'])) {
			$this->verbose = $verbosity[$config['verbose']] ?? (int) $config['verbose'];
		}
	}

	public static function checkConfig(array $config): array
	{
		$errors = [];
		if (empty($config['source'])) {
			$errors['source'] = "Il faut sélectionner la source des liens.";
		}
		if (!isset(self::SOURCES[$config['source']])) {
			$errors['source'] = "Cette source des liens n'est pas (encore) possible.";
		}
		if (($config['source'] === 'miar' ||  $config['source'] === 'wos') && empty($config['url'])) {
			$errors['url'] = "L'import de liens MIAR ou WOS a besoin d'une URL.";
		}
		if (!empty($config['obsoleteDelete']) && empty($config['obsoleteSince'])) {
			$errors['obsoleteSince'] = "Requis puisque la suppression des liens obsolètes est active.";
		}
		if (!empty($config['obsoleteSince'])) {
			$since = (int) strtotime($config['obsoleteSince']);
			if ($since === 0 || $since >= time()) {
				$errors['obsoleteSince'] = "Format non valide";
			}
		}
		if (!empty($config['cacheDuration'])) {
			$d = $config['cacheDuration'];
			if ($d < 0) {
				$errors['cacheDuration'] = "Entier positif ou nul, ou vide";
			}
		}
		return $errors;
	}

	public function run(): Report
	{
		$config = [
			'obsoleteDelete' => $this->obsoleteDelete,
			'obsoleteSince' => $this->obsoleteSince,
			'verbose' => $this->verbose,
			'cacheDuration' => $this->cacheDuration,
		];
		$fqdn = self::SOURCES[$this->source];
		$importer = new $fqdn($config);
		assert($importer instanceof linkimport\DefaultImport);
		if ($this->url !== ''  && property_exists($importer, 'csvFile')) {
			/** @psalm-suppress UndefinedPropertyAssignment */
			$importer->csvFile = $this->url;
		}

		$report = new Report();
		$sourceName = Yii::app()->db->createCommand("SELECT nom FROM Sourcelien WHERE nomcourt = :n")->queryScalar([':n' => $this->source]);
		$report->emailSubject = "Liens $sourceName";
		$report->body = $importer->import();
		return $report;
	}
}
