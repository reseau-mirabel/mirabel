<?php

namespace processes\cron\tasks;

use Editeur;
use processes\cron\ConfigParam;
use processes\cron\ModelInterface;
use processes\cron\Report;
use processes\idref\Notice;
use processes\idref\UpdateEditeur;

class ModelUpdateByIdrefApi implements ModelInterface
{
	private int $verbose = 0;

	public static function getDescription(): string
	{
		return <<<HTML
			<strong>Mise à jour des données d'éditeurs via l'API de idref.fr.</strong>
			<br>
			Modifie chaque éditeur avec :
			<ul>
				<li>la date de début et la date de fin venant d'idref.fr, sauf en cas de divergence ;</li>
				<li>le pays venant d'idref.fr, sauf en cas de divergence ;</li>
				<li>l'identifiant ROR venant d'idref.fr.</li>
			</ul>
			Si un envoi par courriel est activé, le CSV sera dans le corps du texte et aussi en pièce jointe.
			HTML;
	}

	public function getUniqueIdentifer(): string
	{
		return "update-by-idref-api";
	}

	public static function getConfigHelp(): array
	{
		return [
			'verbose' => new ConfigParam("bool", "Verbeux", "Signale les éditeurs inchangés."),
		];
	}

	public function setConfig(array $config): void
	{
		$this->verbose = (int) ($config['verbose'] ?? 0);
	}

	public static function checkConfig(array $config): array
	{
		return [];
	}

	public function getConfigSummary(): array
	{
		return [
			'verbeux' => $this->verbose > 0 ? "bavard" : "silencieux",
		];
	}

	public function run(): Report
	{
		$writer = new UpdateEditeur();
		$writer->verbose = $this->verbose;

		$maxId = (int) \Yii::app()->db->createCommand("SELECT max(id) FROM Editeur WHERE idref IS NOT NULL")->queryScalar();
		$batchSize = 5000;
		$start = 0;
		while ($start <= $maxId) {
			$end = $start + $batchSize - 1;
			$editeurs = Editeur::model()->findAll("idref IS NOT NULL AND id BETWEEN {$start} AND {$end} ORDER BY id");
			foreach ($editeurs as $e) {
				assert($e instanceof Editeur);
				$notice = Notice::fetchByIdref($e->idref);
				$writer->update($e, $notice);
			}
			$start += $batchSize;
		}

		$report = new Report();
		$csv = $writer->getCsv();
		$report->body = $csv;
		$report->attachToEmail($csv, "Mirabel_Editeur-par-idref.fr.csv", "text/csv");
		$report->emailSubject = "Cron M données d'éditeurs par idref.fr";
		return $report;
	}
}
