<?php

namespace processes\cron\tasks;

use CHtml;
use processes\attribut\Report as AttributeReport;
use processes\cron\ConfigParam;
use processes\cron\Report;
use Sourceattribut;
use Yii;

class ModelAttributTitresInconnus implements \processes\cron\ModelInterface
{
	private string $identifiant;

	public static function getDescription(): string
	{
		return "Liste les titres inconnus de Mir@bel dans le dernier import des valeurs d'un attribut.";
	}

	public function getUniqueIdentifer(): string
	{
		return "attribut-{$this->identifiant}";
	}

	public static function getConfigHelp(): array
	{
		$attrList = Yii::app()->db
			->createCommand("SELECT identifiant FROM Sourceattribut ORDER BY identifiant")
			->queryColumn();
		return [
			'identifiant' => new ConfigParam("list", "Attribut", "", array_combine($attrList, $attrList)),
		];
	}

	public function getConfigSummary(): array
	{
		$attribute = Sourceattribut::model()->find("identifiant = :i", [':i' => $this->identifiant]);
		assert($attribute instanceof Sourceattribut);
		return [
			'identifiant' => CHtml::link(
				CHtml::encode("{$this->identifiant} ({$attribute->nom})"),
				['/attribute/view', 'id' => $attribute->id]
			),
		];
	}

	public function setConfig(array $config): void
	{
		$this->identifiant = trim($config['identifiant']);
	}

	public static function checkConfig(array $config): array
	{
		$errors = [];
		if (empty($config['identifiant'])) {
			$errors['identifiant'] = "Ce champ est nécessaire.";
			return $errors;
		}
		$exists = (bool) Yii::app()->db
			->createCommand("SELECT 1 FROM Sourceattribut WHERE identifiant = :i")
			->queryScalar([':i' => $config['identifiant']]);
		if (!$exists) {
			$errors['identifiant'] = "Aucun attribut n'a cet identifiant.";
		}
		return $errors;
	}

	public function run(): Report
	{
		$upstreamReport = AttributeReport::loadLast($this->identifiant);

		if (!$upstreamReport->unknownTitles) {
			return new Report();
		}

		$csv = fopen('php://memory', 'w');
		fputcsv($csv, ["Ligne", "Titre", "URL de création"], "\t", '"', '\\');
		foreach ($upstreamReport->unknownTitles as $position => [$issns, $titre]) {
			$url = "";
			if ($issns) {
				$url = Yii::app()->getParams()->itemAt('baseUrl') . "/titre/create-by-issn?issn=" . str_replace(" ", "+", $issns);
			}
			fputcsv($csv, [$position, $titre, $url], "\t", '"', '\\');
		}

		$report = new Report();
		$report->body = stream_get_contents($csv, -1, 0);
		$attributeName = Yii::app()->db
			->createCommand("SELECT nom FROM Sourceattribut WHERE identifiant = :i")
			->queryScalar([':i' => $this->identifiant]);
		$report->emailSubject = "Attribut $attributeName : titres manquants dans Mir@bel";
		return $report;
	}
}
