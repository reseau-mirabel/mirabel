<?php

namespace processes\cron\tasks;

use processes\cron\ConfigParam;
use processes\cron\Report;
use processes\urlverif\UrlMassCheck;

class ModelCheckLinks implements \processes\cron\ModelInterface
{
	private const SOURCES = ["Cms" => "Textes rédigés", "Editeur" => "Éditeurs", "Ressource" => "Ressources", "Revue" => "Revues"];

	private bool $partiel = false;

	private bool $verbose = false;

	/**
	 * @var ""|"Cms"|"Editeur"|"Ressource"|"Revue"
	 */
	private string $source = '';

	public static function getDescription(): string
	{
		return "Vérification des URLs contenues dans Mir@bel";
	}

	public function getUniqueIdentifer(): string
	{
		return "checklinks-{$this->source}";
	}

	public static function getConfigHelp(): array
	{
		return [
			'source' => new ConfigParam("list", "Source des URLs", "", self::SOURCES),
			'partiel' => new ConfigParam("bool", "Vérification partielle", "Limite aux URLs ayant des erreurs au précédent passage"),
			'verbose' => new ConfigParam("bool", "Verbeux", "Affiche un rapport en CSV listant les URLs en erreur."),
		];
	}

	public function getConfigSummary(): array
	{
		return [
			'source' => $this->source,
			'partiel' => $this->partiel ? "OUI, uniquement les URLs déja en erreur" : "NON, vérification complète de toutes les URLs",
			'verbose' => $this->verbose ? "bavard" : "silencieux",
		];
	}

	public function setConfig(array $config): void
	{
		$this->source = (string) $config['source'];
		$this->partiel = (bool) $config['partiel'];
		$this->verbose = (bool) ($config['verbose'] ?? false);
	}

	public static function checkConfig(array $config): array
	{
		$errors = [];
		if (empty($config['source'])) {
			$errors['source'] = "Il faut choisir une source.";
			return $errors;
		}
		if (!in_array($config['source'], array_keys(self::SOURCES))) {
			$errors['source'] = "La source doit être une des valeurs permises.";
		}

		if (isset($config['partiel'])) {
			if ((string) $config['partiel'] !== "0" && (string) $config['partiel'] !== "1") {
				$errors['partiel'] = "La valeur doit être un booléen.";
			} elseif ($config['source'] === 'Cms') {
				$errors['partiel'] = "Pas de vérification partielle possible pour le contenu rédactionnel.";
			}
		}
		return $errors;
	}

	public function run(): Report
	{
		/**
		 * @var false|resource Set to a resource in order to create a CSV log: fopen('php://temp', 'w');
		 */
		$output = fopen('php://temp', 'w');
		if ($output === false) {
			throw new \Exception("Failed to open/create a file!");
		}

		$className = $this->source;
		$urlCheck = new UrlMassCheck($this->partiel); // Slower checks when only broken URLs are tested.
		$urlCheck->verbose = 0;
		if ($this->verbose) {
			$urlCheck->setCsvOutput($output);
		}

		if ($this->partiel) {
			$urlCheck->removeObsoleteLinks($className);
			$urlCheck->checkBrokenLinks($className);
		} else {
			$records = $className::model()->findAll();
			$urlCheck->checkAllLinks($className, UrlMassCheck::extractLinks($records));
		}

		$report = new Report();
		$report->emailSubject = "Vérification des URLs";
		if ($this->verbose) {
			$report->body = stream_get_contents($output, -1, 0);
			if ($report->body) {
				$report->title = $urlCheck->getStats($className);
			}
		}
		return $report;
	}
}
