<?php

namespace processes\cron\tasks;

use processes\cron\ConfigParam;
use processes\cron\Report;
use processes\kbart\noninteractive\Config;
use processes\kbart\noninteractive\ImportAll;
use processes\kbart\noninteractive\WritingStatsRenderer;
use Yii;

class ModelServiceKbart2 extends ModelServiceKbart
{
	protected bool $lacunaire = false;

	protected bool $selection = false;

	protected bool $overrideDatesWithEmbargo = false;

	/**
	 * @override
	 */
	public static function getConfigHelp(): array
	{
		return parent::getConfigHelp() + [
			'lacunaire' => new ConfigParam("bool", "Accès lacunaires", "Si cochée, tous les accès seront lacunaires."),
			'selection' => new ConfigParam("bool", "Sélections d'articles", "Si cochée, tous les accès seront marqués ainsi."),
			'overrideDatesWithEmbargo' => new ConfigParam("bool", "Dates par embargo", "Si cochée, les dates déclarés seront remplacées par les dates calculées par l'embargo de l'accès."),
			'ssiConnu' => new ConfigParam("bool", "Ignorer les titres inconnus", "Par défaut, cette case est décochée tous les titres inconnus sont listés."),
		];
	}

	/**
	 * @override
	 */
	public function getConfigSummary(): array
	{
		return parent::getConfigSummary() + [
			'Lacunaire' => $this->lacunaire ? "oui" : "non",
			"Sélection" => $this->selection ? "oui" : "non",
			"Dates par embargo" => $this->overrideDatesWithEmbargo ? "oui" : "non",
		];
	}

	/**
	 * @override
	 */
	public function setConfig(array $config): void
	{
		parent::setConfig($config);
		$this->lacunaire = (bool) ($config['lacunaire'] ?? false);
		$this->selection = (bool) ($config['selection'] ?? false);
		$this->overrideDatesWithEmbargo = (bool) ($config['overrideDatesWithEmbargo'] ?? false);
	}

	/**
	 * @override
	 */
	public function run(): Report
	{
		/**
		 * @todo Gérer ignoreUrl (efface l'URL de l'accès), nécessaire pour Europresse
		 */

		$config = new Config();
		$config->setRessourceAndCollections($this->ressourceId, $this->collectionIds);
		$config->setDefaultAccess($this->acces);
		$config->setDefaultCoveragedepth($this->contenu);
		$config->overrideDatesWithEmbargo = $this->overrideDatesWithEmbargo;
		$config->lacunaire = $this->lacunaire;
		$config->selection = $this->selection;
		$config->verbose = $this->verbose;

		$importAuto = new ImportAll($config);
		$url = $url = str_replace('{{TODAY}}', date('Y-m-d'), $this->url);
		$importAuto->store($url);
		$importer = $importAuto->createImporter();
		$stats = $importAuto->write($importer);

		$writer = new WritingStatsRenderer();
		$writer->titreInconnus = !$this->ssiConnu;
		$writer->url = $importAuto->getUrl();

		$report = new Report();
		$report->emailSubject = "Import KBART " . Yii::app()->db->createCommand("SELECT nom FROM Ressource WHERE id = {$this->ressourceId}")->queryScalar();
		if ($this->collectionIds) {
			$report->emailSubject .= " [{$this->getCollectionNames()}]";
		}
		$report->title = $writer->getSummary($stats);
		$report->body = $writer->renderAsText($stats, $config);
		$report->format = \processes\cron\ar\History::REPORTFORMAT_MARKDOWN;
		return $report;
	}
}
