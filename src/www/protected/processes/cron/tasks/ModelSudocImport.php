<?php

namespace processes\cron\tasks;

use commands\models\sudoc\ImportByPpn;
use models\sudoc\ApiClient;
use processes\cron;
use processes\cron\ConfigParam;
use processes\cron\Report;

class ModelSudocImport implements cron\ModelInterface
{
	private bool $verbose = false;

	private bool $simulation = false;

	private int $limit = 0;

	public static function getDescription(): string
	{
		return <<<HTML
			<strong>Interroge l'API du SUDOC pour tous les PPN de Mir@bel.</strong>
			Pour chacun de ces PPN, cette tâche :
			<ol>
				<li>met à jour les informations du titre s'il y a un changement (table <em>Issn</em>, par une Intervention) ;</li>
				<li>récupére les états de collections du titre.</li>
			</ol>
			Les champs mis à jour sont :
			<ul>
				<li>BNF-ark</li>
				<li>dates de début et fin</li>
				<li>noholding</li>
				<li>support, si aucun ISSN n'est associé au PPN</li>
				<li>worldcat OCN</li>
			</ul>
			HTML;
	}

	public function getUniqueIdentifer(): string
	{
		return "sudoc-import";
	}

	public static function getConfigHelp(): array
	{
		return [
			'verbose' => new ConfigParam("bool", "Verbeux", "Si actif, le CSV signale aussi les lignes sans changement."),
			'simulation' => new ConfigParam("bool", "Simulation", "Si cochée, n'enregistre pas les modifications."),
			'limit' => new ConfigParam("integer", "Limite de requêtes", "Nombre maximum de requêtes à envoyer au SUDOC (pour tester)"),
		];
	}

	public function setConfig(array $config): void
	{
		$this->verbose = (bool) ($config['verbose'] ?? false);
		$this->simulation = (bool) ($config['simulation'] ?? false);
		$this->limit = (int) ($config['limit'] ?? 0);
	}

	public static function checkConfig(array $config): array
	{
		return [];
	}

	public function getConfigSummary(): array
	{
		$info = [
			'verbeux' => $this->verbose ? "bavard" : "silencieux",
		];
		if ($this->simulation) {
			$info['simulation'] = "oui (aucun enregistrement)";
		}
		if ($this->limit > 0) {
			$info['limite'] = "$this->limit requêtes max";
		}
		return $info;
	}

	public function run(): Report
	{
		$import = new ImportByPpn(new ApiClient);
		$import->setConfig([
			'limit' => $this->limit,
			'onlyDubious' => false,
			'simulation' => $this->simulation,
			'verbose' => $this->verbose ? 1 : 0,
		]);
		$out = fopen('php://memory', 'w');
		$import->run($out);
		$output = (string) stream_get_contents($out, -1, 0);

		$report = new Report();
		$report->body = $output;
		$report->emailSubject = 'Cron M sudoc import';
		$report->attachToEmail($output, "Mirabel_issn-modif.tsv", "text/tab-separated-values");
		return $report;
	}
}
