<?php

namespace processes\cron\tasks;

trait NoConfig
{
	public function getUniqueIdentifer(): string
	{
		return static::class;
	}

	/**
	 * @return array<string, \processes\cron\ConfigParam>
	 */
	public static function getConfigHelp(): array
	{
		return [];
	}

	/**
	 * @return array<string, string|string[]>
	 */
	public function getConfigSummary(): array
	{
		return [];
	}

	public function setConfig(array $config): void
	{
		// NOP
	}

	public static function checkConfig(array $config): array
	{
		return [];
	}
}
