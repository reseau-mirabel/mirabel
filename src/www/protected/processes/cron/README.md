Cron PHP / Mir@bel
==================

- local time only (no GMT)
- tasks define a unique identifier that prevent parallel runs of the same tasks
- time limit for each task


Glossary compared to crontab
----------------------------

A *Task* is like a complete row in the crontab.
Tasks are saved in a table in the DB.

Each task has several components :

- a serialized *Schedule* which declares when to run.
  It's less powerful but simpler than cron's expressions.
- a reference to the *Model* which is the PHP code that is linked to the task.
- a few settings of the task,
  like the config for the model,
  or the email addresses to send reports to.

FAQ
---

### How to install?

Add the following line to the crontab, for instance with `crontab -e`:
```
* *    *   *   *   /path/to/mirabel/yii cron 1
```

It's possible to space the cron tasks by more than a minute.
E.g. for 5 minutes intervals:
```
*/5 *    *   *   *   /path/to/mirabel/yii cron 5
```

That should be the only command line operation,
everything else is done through the web interface.

## How to migrate the current crontab?

Create the tasks with:
```
crontab -l | ./yii cron convert
```
You should then remove the migrated system tasks with `crontab -e`.

A second pass will not create duplicates,
so the list of not-migrated system tasks is given by:
```
crontab -l | ./yii cron convert | sort
```

### Why models?

We want to run cron tasks for dozens of similar imports.
For instance, for various ressources and collections, load KBART files.
The web users must be able to add a new task like that to the cron,
without any access to the code.
This behaviour requires that the web user can declare the resource, the KBART URL, etc
into a pre-defined *task model*.

Each task model declares its own available settings
that the web user can fill when creating the task. 

### Start time

All tasks scheduled will start (in parallel) at the begining of the cron interval
that contain their expected starting time.

E.g. for a cron running every 5 minutes (command `yii cron 5`),
at 08:00 it will start all the tasks scheduled for starting between 08:00 and 08:05.

### Parallel tasks

When multiple tasks must start during the same run,
they will run in parallel through forked processes.

The PHP API `\parallel\Runtime` was not suitable
because it's meant for tasks that don't share any data with the caller.
Bootstrapping threads (autoloader, Yii singleton, etc) would be a heavy and error-prone work.
