<?php

namespace processes\titres;

use Config;
use Issn;
use models\sherpa\Item;
use Politique;
use Titre;

/**
 * Croise les données de Sherpa et les politiques publiées de Mirabel
 * pour fournir une instance Item (directement de Sherpa ou déduite d'une politique locale).
 */
class PolitiquePublication
{
	public const SOURCE_MIRABEL = 1;

	public const SOURCE_SHERPA = 2;

	private const SOURCELIEN_SHERPAROMEO = 31;

	public ?Item $itemSherpa = null;

	private ?Politique $politique = null;

	private Titre $titre;

	private int $source;

	public function __construct(Titre $titre)
	{
		$this->titre = $titre;
		$this->source = $this->detectSource();
	}

	public function getItem(): Item
	{
		switch ($this->source) {
			case self::SOURCE_MIRABEL:
				$item = self::createItemFromPolitique($this->titre, $this->politique);
				break;
			case self::SOURCE_SHERPA:
				$item = $this->getItemSherpa();
				break;
			default:
				throw new \Exception("Wrong source");
		}
		if ($item === null) {
			throw new \Exception("Cannot create a Sherpa Item when no Politique is set.");
		}
		return $item;
	}

	/**
	 * Create an Item for each available policy: Sherpa or Politique (even unpublished);
	 *
	 * @return array{source: "Sherpa"|Politique, publication: \models\sherpa\Publication}[]
	 */
	public function getAllPublications(): array
	{
		$items = [];

		$hasSherpaLink = $this->titre->getLiens()->containsSource(self::SOURCELIEN_SHERPAROMEO);
		if ($hasSherpaLink) {
			$items[] = [
				'source' => 'Sherpa',
				'publication' => self::findSherpaItem($this->titre)->getPublication(),
			];
		}

		$p = \Politique::model()->findBySql(
			"SELECT p.* FROM `Politique` p JOIN `Politique_Titre` pt ON pt.`politiqueId` = p.id WHERE pt.`titreId` = :id",
			[':id' => $this->titre->id]
		);
		if ($p) {
			$items[] = [
				'source' => $p,
				'publication' => self::createItemFromPolitique($this->titre, $p)->getPublication(),
			];
		}

		return $items;
	}

	public function getItemSherpa(): ?Item
	{
		return $this->itemSherpa;
	}

	public function getSource(): int
	{
		return $this->source;
	}

	public static function hasInternalPolicy(int $titreId): bool
	{
		$row = \Yii::app()->db
			->createCommand(
				"SELECT p.id, p.status FROM `Politique` p JOIN `Politique_Titre` pt ON pt.`politiqueId` = p.id WHERE pt.`titreId` = :id AND p.status IN (:s1, :s2, :s3) LIMIT 1"
			)
			->queryRow(
				true,
				[':id' => $titreId, ':s1' => \Politique::STATUS_PUBLISHED, ':s2' => \Politique::STATUS_UPDATED, ':s3' => \Politique::STATUS_TOPUBLISH]
			);
		if (!$row) {
			return false;
		}
		if ($row['status'] === \Politique::STATUS_PUBLISHED) {
			return true;
		}
		return (bool) \Yii::app()->db
			->createCommand("SELECT 1 FROM `PolitiqueLog` WHERE `politiqueId` = :id AND `action` = 'publish' ORDER BY id DESC LIMIT 1")
			->queryScalar([':id' => (int) $row['id']]);
	}

	private function detectSource(): int
	{
		$hasSherpaLink = $this->titre->getLiens()->containsSource(self::SOURCELIEN_SHERPAROMEO);
		if ($hasSherpaLink) {
			$this->itemSherpa = self::findSherpaItem($this->titre);
		}
		$useInternalPolicies = Config::read('politique.publication-si-validee');
		if ($useInternalPolicies) {
			$this->politique = self::findPolitique($this->titre);
			if ($this->politique !== null) {
				/**
				 * @todo Si Sherpa et Mirabel, détecter si l'identifiant de M est dans Sherpa. (M#5433 cas 4)
				 */
				return self::SOURCE_MIRABEL;
			}
		}
		if ($hasSherpaLink && $this->itemSherpa !== null) {
			return self::SOURCE_SHERPA;
		}
		return 0;
	}

	private static function createItemFromPolitique(Titre $titre, \Politique $politique): Item
	{
		$data = new \stdClass();
		$lastUpdate = $politique->lastUpdate;
		$jsonPolicy = $politique->publisher_policy; // Already replaced by the last *published* policy, if current policy is updated/topublish
		$data->system_metadata = (object) [
			"date_modified" => date('Y-m-d', $lastUpdate),
			"publicly_visible" => 'no',
			"uri" => "mirabel-{$titre->id}",
		];
		$data->url = $titre->url;
		try {
			$policy = json_decode($jsonPolicy, false, 512, JSON_THROW_ON_ERROR);
		} catch (\Exception $e) {
			throw new \Exception("JSON incohérent dans la politique publiée pour Titre {$titre->id}, Politique {$politique->id}, JSON '{$jsonPolicy}' : {$e->getMessage()}");
		}
		$data->publisher_policy = [$policy];
		$data->publisher_policy[0]->uri = "";
		return new Item($data, (int) $lastUpdate);
	}

	private static function findSherpaItem(Titre $titre): ?Item
	{
		// cache by Titre.id
		$item = Item::loadFromCache((int) $titre->id);
		if ($item !== null) {
			return $item;
		}

		// cache by ISSN
		$issns = array_filter(array_map(
			function (Issn $i): ?string {
				return $i->issn;
			},
			$titre->issns
		));
		$itemByIssn = Item::loadFromCacheByIssn((int) $titre->id, $issns);
		if ($itemByIssn !== null) {
			return $itemByIssn;
		}

		return null;
	}

	/**
	 * Return a Politique that was published for this Titre, or null.
	 */
	private static function findPolitique(Titre $titre): ?\Politique
	{
		$p = \Politique::model()->findBySql(
			"SELECT p.* FROM `Politique` p JOIN `Politique_Titre` pt ON pt.`politiqueId` = p.id WHERE pt.`titreId` = :id AND p.status IN (:s1, :s2, :s3)",
			[':id' => $titre->id, ':s1' => \Politique::STATUS_PUBLISHED, ':s2' => \Politique::STATUS_UPDATED, ':s3' => \Politique::STATUS_TOPUBLISH]
		);
		if (($p instanceof \Politique) && $p->status !== \Politique::STATUS_PUBLISHED) {
			$published = \Yii::app()->db
				->createCommand("SELECT actionTime, publisher_policy FROM `PolitiqueLog` WHERE `politiqueId` = {$p->id} AND `action` = 'publish' ORDER BY id DESC LIMIT 1")
				->queryRow();
			if ($published) {
				$p->lastUpdate = (int) $published['actionTime'];
				$p->publisher_policy = $published['publisher_policy'];
			} else {
				$p = null;
			}
		}
		return $p;
	}
}
