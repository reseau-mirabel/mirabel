<?php

namespace processes\titres;

use Yii;

class Cover
{
	public static function update(\Titre $model): void
	{
		$tc = new TitreCouverture();

		if (!$model->urlCouverture) {
			$tc->delete($model->id);
			return;
		}

		try {
			if ($tc->download($model->id, $model->urlCouverture)) {
				Yii::app()->user->setFlash('info', "La couverture du titre a été téléchargée et redimensionnée.");
			}
			$tc->getResizer()->resizeByIdentifier($model->id);
			Yii::app()->session->add('force-refresh', true);
		} catch (\Exception $e) {
			Yii::app()->user->setFlash('warning', "Erreur de téléchargement de la couverture du titre : " . $e->getMessage());
		}
	}
}
