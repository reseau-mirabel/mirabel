<?php

namespace processes\titres;

use Intervention;
use Issn;
use Suivi;
use Revue;
use Titre;
use models\sherpa\Hook;

/**
 * /titre/create
 */
class Create
{
	public Titre $model;

	/**
	 * @var Issn[]
	 */
	public array $issns = [];

	public ?Intervention $intervention;

	private bool $directAccess = false;

	private bool $hasAddedSherpaLink = false;

	private array $issnChanges = [];

	private array $redirection = [];

	public function __construct(int $revueId)
	{
		$this->model = new Titre();
		$this->issns = [];

		if ($revueId > 0) {
			$this->model->revueId = $revueId;
			if ($this->model->revue === null) {
				throw new \CHttpException(500, "Cette revue id={$revueId} n'existe pas.");
			}
		} else {
			$this->model->revueId = 0;
		}
	}

	public function addExtraState(array $extra): void
	{
		if (!empty($extra['publishers'])) {
			// Ugly hack
			if (empty($_POST['TitreEditeurNew'])) {
				$_POST['TitreEditeurNew'] = [];
			}
			foreach (array_keys($extra['publishers']) as $p) {
				if (ctype_digit($p) && !in_array($p, $_POST['TitreEditeurNew'])) {
					$_POST['TitreEditeurNew'][] = ['editeurId' => (int) $p];
				}
			}
		}
		if (!empty($extra['issnSupport'])) {
			foreach ($extra['issnSupport'] as $i => $support) {
				$found = false;
				$pos = 0;
				foreach ($this->issns as $issnRecord) {
					if ($issnRecord->issn === $i) {
						$issnRecord->support = preg_replace('/[^a-z]/', '', $support);
						$found = true;
					}
					$pos++;
				}
				if (!$found) {
					$record = new Issn();
					$record->issn = preg_replace('/[^0-9X-]/', '', $i);
					$record->support = (string) $support;
					$this->issns["new$pos"] = $record;
				}
			}
		}
	}

	public function addSudocNotices(array $notices): void
	{
		\processes\titres\TitreUpdater::fillBySudocNotices($this->model, $notices);

		foreach ($notices as $k => $sudocNotice) {
			$issn = new Issn();
			$issn->fillBySudocNotice($sudocNotice);
			$this->issns["new$k"] = $issn;
		}
	}

	public function fillIntervention(): void
	{
		$this->createRevue();

		// main object
		$this->intervention->contenuJson->create($this->model);
		// related issn records
		$this->intervention->addChanges($this->issnChanges);
		// relations
		$relations = new Publishers(0, $this->intervention);
		$relations->linkAndUnlink($_POST['TitreEditeur'] ?? [], $_POST['TitreEditeurNew'] ?? []);
	}

	public function getRedirection(): array
	{
		return $this->redirection;
	}

	public function hasAddedSherpaLink(): bool
	{
		return $this->hasAddedSherpaLink;
	}

	public function load(array $post): bool
	{
		if (!$this->model->load($post)) {
			$this->intervention = new Intervention();
			$this->intervention->contenuJson = new \InterventionDetail();
			if ($this->model->revueId > 0) {
				$this->intervention->revueId = $this->model->revueId;
				$this->intervention->suivi = (Suivi::isTracked($this->intervention->revue) !== null);
			}
			return false;
		}
		$this->model->obsoletePar = $this->getDefaultObsoletePar();
		$this->issnChanges = Issn::loadMultiple($this->issns, $post);
		$this->hasAddedSherpaLink = Hook::addSherpaLink($this->model, $this->issnChanges);
		$this->intervention = $this->model->buildIntervention($this->directAccess);
		if (isset($post['Intervention']['email'])) {
			$this->intervention->setAttributes($post['Intervention']);
		}
		return true;
	}

	public function save(): bool
	{
		if ($this->directAccess) {
			$this->model->id = $this->intervention->titreId;
		}
		if (!$this->model->revueId && !empty($this->intervention->contenuJson[0]['id'])) {
			$this->model->revueId = $this->intervention->contenuJson[0]['id'];
			TitreSuivi::update($this->model);

			$this->redirection['status'] = 'success';
			$this->redirection['message'] = "La revue a été créée. Il vous reste à :
<ol>
	<li>ajouter les autres titres de la revue s'il y a un historique de titres et
		qu'il est simple (pas de fusions/scissions, simplement des <em>devient</em> ou <em>suite de</em>),
	</li>
	<li>ajouter les accès en ligne,</li>
	<li>définir la thématique,</li>
	<li>puis, une fois que tout est terminé, indiquer que la revue a été vérifiée.</li>
</ol>";
		}
		if ($this->directAccess && (int) $this->model->revueId === 0) {
			return false;
		}
		if ($this->directAccess) {
			Cover::update($this->model);
		}
		if ($this->model->revueId) {
			if ($this->directAccess && Titre::model()->countByAttributes(['revueId' => $this->model->revueId]) > 1 && TrierTitre::isRevueNeedReordering($this->model->revue)) {
				$this->redirection['url'] = ['/revue/trier-titre', 'id' => $this->model->revueId];
			} else {
				$this->redirection['url'] = ['/revue/view', 'id' => $this->model->revueId];
			}
		} else {
			$this->redirection['url'] = ['/revue/index'];
		}
		return true;
	}

	public function setDirectAccess(bool $mode): void
	{
		$this->directAccess = $mode;
	}

	public function validate(): bool
	{
		return !$this->model->getErrors()
			&& $this->model->validate()
			&& Issn::validateMultiple($this->issns, $this->model);
	}

	/**
	 * Create a Revue record before the Titre record?
	 */
	private function createRevue(): void
	{
		if ($this->model->revueId > 0) {
			$this->intervention->description = "Création du titre « {$this->model->titre} »";
			return;
		}
		$this->intervention->description = "Création de la revue « {$this->model->titre} »";
		$this->intervention->action = 'revue-C';
		$revue = new Revue();
		$revue->statut = 'normal';
		$this->intervention->contenuJson->create($revue);
	}

	private function getDefaultObsoletePar(): ?int
	{
		if (!$this->model->revueId || $this->model->obsoletePar || !$this->model->dateFin) {
			return null;
		}
		$sql = "SELECT * FROM Titre
			WHERE revueId = :revueId
			AND dateDebut >= :dateFin
			ORDER BY dateDebut
			LIMIT 1";
		$obsoleteParId = (int) \Yii::app()->db->createCommand($sql)->queryScalar([
			':revueId' => $this->model->revueId,
			':dateFin' => $this->model->dateFin,
		]);
		if (!$obsoleteParId) {
			return null;
		}
		return $obsoleteParId;
	}
}
