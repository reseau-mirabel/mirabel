<?php

namespace processes\titres;

use CHtml;
use Titre;
use Yii;
use components\FeedGenerator;

/**
 * Utilisé par /titre/delete
 */
class Delete
{
	public Titre $titre;

	private string $errorMessage = '';

	private string|array $returnUrl = '';

	private ?Titre $redirection = null;

	public function __construct(Titre $titre)
	{
		$this->titre = $titre;
	}

	public function getErrorMessage(): string
	{
		return $this->errorMessage;
	}

	public function getReturnUrl(): string|array
	{
		if ($this->returnUrl) {
			return $this->returnUrl;
		}
		if ($this->isRevueDeleted()) {
			if ($this->redirection) {
				return ['/revue/view', 'id' => $this->redirection->revueId];
			}
			return ['/revue/index'];
		}
		return ['/revue/view', 'id' => $this->titre->revueId];
	}

	/**
	 * @return string[]
	 */
	public function getConstraints(): array
	{
		$deleteErrors = [];

		$services = $this->titre->services;
		if (!empty($services)) {
			foreach ($services as $s) {
				$deleteErrors[] = "Ce titre est lié à l'accès en ligne #"
					. CHtml::link((string) $s->id, ['/service/update', 'id' => $s->id]);
			}
		}

		$previous = Titre::model()->findByAttributes(['obsoletePar' => $this->titre->id]);
		if ($previous) {
			$deleteErrors[] = "Ce titre est le successeur du titre " . $previous->getSelfLink();
		}

		$partenaires = \Partenaire::model()->findAllBySql(
			"SELECT p.* FROM Partenaire p JOIN Partenaire_Titre pt ON pt.partenaireId=p.id "
			. "WHERE pt.titreId = " . $this->titre->id
		);
		if (!empty($partenaires)) {
			foreach ($partenaires as $p) {
				$deleteErrors[] = "Ce titre est détenu par le partenaire " . $p->getSelfLink();
			}
		}

		$interventions = $this->getInterventions();
		foreach ($interventions as $intv) {
			$deleteErrors[] = "Intervention en attente : " . CHtml::link(CHtml::encode($intv->description), ['/intervention/view', 'id' => $intv->id]);
		}

		$politique = $this->getPolitique();
		if ($politique) {
			$deleteErrors[] = "Politique de publication : "
				. CHtml::link(CHtml::encode($politique->name), ['/politique/update', 'id' => $politique->id])
				. ($politique->editeurId ? " par l'éditeur <em>" . CHtml::encode($politique->editeur->nom) . "</em>" : "");
		}

		return $deleteErrors;
	}

	public function getRedirection(): ?\Titre
	{
		return $this->redirection;
	}

	public function isRevueDeleted(): bool
	{
		$numTitres = (int) Titre::model()->countByAttributes(['revueId' => $this->titre->revueId]);
		return ($numTitres === 1);
	}

	public function run(): bool
	{
		if ($this->getInterventions()) {
			$this->errorMessage = "Suppression impossible car au moins une intervention est en attente sur ce titre.";
			return false;
		}
		if ($this->getPolitique()) {
			$this->errorMessage = "Suppression impossible car une politique de publication est déclarée sur ce titre.";
			return false;
		}

		$isRevueDeleted = $this->isRevueDeleted();

		$i = $this->createIntervention($isRevueDeleted);
		$success = $i->accept(false);
		$i->revueId = null;
		$i->titreId = null;
		if ($success) {
			$i->save(false);
			if ($this->redirection) {
				$this->writeRedirection($isRevueDeleted);
			}
			return true;
		}

		// deletion failed!
		$this->returnUrl = ['view', 'id' => $this->titre->id];
		if ($isRevueDeleted) {
			FeedGenerator::removeFeedsFile('Revue', $this->titre->revueId);
		}
		$errors = $this->getConstraints();
		$this->errorMessage = ($errors ? "<ul><li>" . join("</li><li>", $errors) . "</li></ul>"
			: json_encode($i->getErrors(), JSON_PRETTY_PRINT)
		);
		return false;
	}

	public function setRedirection(int $targetId): bool
	{
		$this->redirection = Titre::model()->findByPk($targetId);
		return ($this->redirection instanceof Titre);
	}

	public function setReturnUrl(string $url): void
	{
		$this->returnUrl = $url;
	}

	private function createIntervention(bool $isRevueDeleted): \Intervention
	{
		$i = $this->titre->buildIntervention(true);
		$i->contenuJson->delete($this->titre, "Suppression du titre " . $this->titre->titre);
		if ($isRevueDeleted) {
			$i->description = "Suppression de la revue « {$this->titre->titre} »";
			$i->contenuJson->delete($this->titre->revue);
			$i->action = 'revue-D';
			$this->returnUrl = ['/revue/index'];
			FeedGenerator::saveFeedsToFile('Revue', (int) $this->titre->revueId);
		} else {
			$i->description = "Suppression du titre « {$this->titre->titre} »";
			$i->action = 'revue-U';
			if (!$this->returnUrl) {
				$this->returnUrl = ['/revue/view', 'id' => $this->titre->revueId];
			}
		}
		return $i;
	}

	/**
	 * @return \Intervention[]
	 */
	private function getInterventions(): array
	{
		return \Intervention::model()->findAllBySql(
			"SELECT * FROM Intervention WHERE titreId = :tid AND statut = 'attente'",
			[':tid' => $this->titre->id]
		);
	}

	private function getPolitique(): ?\Politique
	{
		return \Politique::model()->findBySql(
			"SELECT p.* FROM Politique p JOIN Politique_Titre pt ON pt.politiqueId = p.id WHERE pt.titreId = :tid",
			[':tid' => $this->titre->id]
		);
	}

	private function writeRedirection(bool $isRevueDeleted): void
	{
		Yii::app()->db->createCommand("UPDATE RedirectionTitre SET cibleId = :new WHERE cibleId = :old")
			->execute([':old' => $this->titre->id, ':new' => $this->redirection->id]);
		Yii::app()->db->createCommand("INSERT IGNORE INTO RedirectionTitre VALUES (:old, :new, :ts)")
			->execute([':old' => $this->titre->id, ':new' => $this->redirection->id, ':ts' => $_SERVER['REQUEST_TIME']]);
		if ($isRevueDeleted) {
			Yii::app()->db->createCommand("UPDATE RedirectionRevue SET cibleId = :new WHERE cibleId = :old")
				->execute([':old' => $this->titre->revueId, ':new' => $this->redirection->revueId]);
			Yii::app()->db->createCommand("INSERT IGNORE INTO RedirectionRevue VALUES (:old, :new, :ts)")
				->execute([':old' => $this->titre->revueId, ':new' => $this->redirection->revueId, ':ts' => $_SERVER['REQUEST_TIME']]);
		}
	}
}
