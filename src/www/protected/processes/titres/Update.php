<?php

namespace processes\titres;

use Issn;
use Titre;
use components\FeedGenerator;
use models\sherpa\Hook;

/**
 * /titre/update
 */
class Update
{
	public const RETURN_ERROR = 0;

	public const RETURN_OK = 1;

	public const RETURN_COVER = 2;

	public Titre $model;

	/**
	 * @var Issn[]
	 */
	public $issns = [];

	public ?\Intervention $intervention;

	private bool $directAccess = false;

	private bool $hasAddedSherpaLink = false;

	public function __construct(Titre $titre)
	{
		$this->model = $titre;
		if ($this->model->liensJson) {
			// normalize
			$this->model->liensJson = json_encode($this->model->getLiens(), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
		}
		$this->issns = $this->model->issns;
	}

	public function hasAddedSherpaLink(): bool
	{
		return $this->hasAddedSherpaLink;
	}

	public function load(array $post): bool
	{
		$oldModel = clone $this->model;
		if (!$this->model->load($post)) {
			// default values when no form is received
			$this->model->suivre = $this->model->monitoredBy();
			$this->model->posseder = $this->model->belongsTo();
			return false;
		}

		$issnChanges = Issn::loadMultiple($this->issns, $post);
		$this->model->setAttributes($post['Titre']);
		if (isset($post['Intervention']['email'])) {
			$this->intervention->setAttributes($post['Intervention']);
		}
		$changedAttributes = array_keys(array_diff_assoc($this->model->getAttributes(), $oldModel->getAttributes()));
		if ($this->model->validate($changedAttributes) && Issn::validateMultiple($this->issns, $this->model)) {
			$this->hasAddedSherpaLink = Hook::addSherpaLink($this->model, $issnChanges);

			$this->intervention->description = "Modification du titre « {$this->model->titre} »";
			$this->intervention->contenuJson->update($oldModel, $this->model, '', false);
			unset($oldModel);

			// related issn records
			$this->intervention->addChanges($issnChanges);
			// relations
			$relations = new Publishers((int) $this->model->id, $this->intervention);
			$relations->linkAndUnlink($post['TitreEditeur'] ?? [], $post['TitreEditeurNew'] ?? []);
			if ($relations->getError()) {
				$this->model->addError('id', $relations->getError());
				return false;
			}
			return true;
		}
		return false;
	}

	public function setDirectAccess(bool $mode): void
	{
		$this->directAccess = $mode;
		$this->intervention = $this->model->buildIntervention($mode);
	}

	public function validate(): int
	{
		if ($this->intervention->validate()) {
			if ($this->directAccess) {
				FeedGenerator::removeFeedsFile('Revue', $this->model->revueId);
				if ($this->model->urlCouvertureDld) {
					Cover::update($this->model);
				}
			}
			return self::RETURN_OK;
		}
		if ($this->model->urlCouvertureDld && $this->directAccess && count($this->intervention->errors) === 1 && $this->intervention->errors['commentaire']) {
			Cover::update($this->model);
			return self::RETURN_COVER;
		}
		return self::RETURN_ERROR;
	}
}
