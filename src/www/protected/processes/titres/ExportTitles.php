<?php

namespace processes\titres;

use Issn;
use Yii;

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class ExportTitles
{
	public static function printIdentifiersCsv(bool $allColumns): void
	{
		$sql = <<<EOSQL
			SELECT
				t.id, t.revueId, t.titre, t.prefixe, t.sigle, t.obsoletePar, t.dateDebut, t.dateFin, t.url, t.periodicite, t.langues, t.hdateModif,
				GROUP_CONCAT(DISTINCT i1.issn SEPARATOR '|') AS ISSN,
				MAX(i1.sudocPpn) AS `PPN Sudoc (ISSN)`,
				MAX(i1.worldcatOcn) AS `OCN Worldcat (ISSN)`,
				GROUP_CONCAT(DISTINCT i2.issn SEPARATOR '|') AS ISSNE,
				MAX(i2.sudocPpn) AS `PPN Sudoc (ISSNE)`,
				MAX(i2.worldcatOcn) AS `OCN Worldcat (ISSNE)`,
				GROUP_CONCAT(DISTINCT i3.issnl SEPARATOR '|') AS issnl
			FROM Titre t
				LEFT JOIN Issn i1 ON t.id = i1.titreId AND i1.support = :sp AND i1.issn IS NOT NULL
				LEFT JOIN Issn i2 ON t.id = i2.titreId AND i2.support = :se AND i2.issn IS NOT NULL
				LEFT JOIN Issn i3 ON t.id = i3.titreId AND i3.issnl IS NOT NULL
			WHERE t.statut = 'normal'
			GROUP BY t.id
			ORDER BY revueId, t.titre
			EOSQL;
		$cmd = Yii::app()->db->createCommand($sql)->query([
			':sp' => Issn::SUPPORT_PAPIER,
			':se' => Issn::SUPPORT_ELECTRONIQUE,
		]);
		$needsHeader = true;
		Yii::app()->urlManager->setBaseUrl(Yii::app()->getBaseUrl(true));
		$out = fopen('php://output', 'w');
		foreach ($cmd as $row) {
			$row["URL revue"] = Yii::app()->urlManager->createUrl('/revue/view', ['id' => $row['revueId']]);
			if ($allColumns) {
				$row['URL id titre'] = Yii::app()->urlManager->createUrl('/titre/view', ['id' => $row['id']]);
				$row['langues'] = join(", ", json_decode($row['langues']));
				$row['hdateModif'] = date('Y-m-d', $row['hdateModif']);
			} else {
				// UrlManager fails to build the correct URL:
				//$row['URL id titre'] = Yii::app()->urlManager->createUrl('/revue/view-by', ['by' => 'titre-id', 'id' => $row['id']]);
				$row['URL id titre'] = Yii::app()->urlManager->getBaseUrl() . "/revue/titre-id/{$row['id']}";

				// uniquement id (titre), revueId, titre, prefixe, sigle, obsoletePar, ISSN, ISSNE, ISSNL, URL revue
				unset($row['dateDebut']);
				unset($row['dateFin']);
				unset($row['url']);
				unset($row['periodicite']);
				unset($row['langues']);
				unset($row['hdateModif']);
			}
			if ($needsHeader) {
				$header = array_keys($row);
				fputcsv($out, $header, ';', '"', '\\');
				$needsHeader = false;
			}
			fputcsv($out, $row, ';', '"', '\\');
		}
	}

	public static function printMaxiCsv(): void
	{
		$sql = <<<EOSQL
			SELECT
				t.id, t.titre, t.dateDebut, t.dateFin, MAX(i.sudocPpn) AS sudoc, MAX(i.worldcatOcn) AS worldcat, GROUP_CONCAT(i.issn) AS issn, GROUP_CONCAT(i.issnl) AS issnl, t.periodicite, t.langues
				, GROUP_CONCAT(DISTINCT CONCAT(r.nom, ' (', s.acces, ' : ', s.dateBarrDebut, '...', s.dateBarrFin, ')') ORDER BY r.nom SEPARATOR ' | ') accès
				, IFNULL(categories.nom, aliases.nom) thématique
			FROM
				Titre t
				LEFT JOIN Issn i ON t.id = i.titreId
				LEFT JOIN `Titre_Editeur` te ON te.titreId = t.id
				LEFT JOIN `Editeur` e ON e.id = te.editeurId
				LEFT JOIN Pays p ON e.paysId = p.id
				LEFT JOIN Service s ON s.titreId = t.id AND s.type = 'Intégral'
				LEFT JOIN Ressource r ON r.id = s.ressourceId
				LEFT JOIN (SELECT catr.revueId, GROUP_CONCAT(c.categorie ORDER BY c.categorie SEPARATOR ' | ') AS nom FROM Categorie_Revue catr JOIN Categorie c ON catr.categorieId = c.id GROUP BY catr.revueId) categories USING (revueId)
				LEFT JOIN (SELECT catt.titreId, GROUP_CONCAT(c.categorie ORDER BY c.categorie SEPARATOR ' | ') AS nom FROM CategorieAlias_Titre catt JOIN CategorieAlias cata ON cata.id = catt.categorieAliasId JOIN Categorie c ON cata.categorieId = c.id GROUP BY catt.titreId) aliases ON aliases.titreId = t.id
			WHERE
				t.statut = 'normal'
				AND JSON_CONTAINS(langues, '"fre"', '$')
				-- AND paysId = 62 -- France
			GROUP BY t.id
			ORDER BY t.revueId, t.titre
			EOSQL;
		$titres = Yii::app()->db->createCommand($sql)->queryAll();
		$findEditeurs = Yii::app()->db->createCommand(
			"SELECT
	e.nom, p.nom AS pays
FROM
	Titre_Editeur te
	JOIN Editeur e ON e.id = te.editeurId
	LEFT JOIN Pays p ON e.paysId = p.id
WHERE te.titreId = :titreId
LIMIT 3
"
		);
		$out = fopen('php://output', 'w');
		fputcsv($out, array_merge(array_keys($titres[0]), ['editeur1', 'pays1', 'editeur2', 'pays2', 'editeur3', 'pays3']), "\t", '"', '\\');
		foreach ($titres as $titre) {
			$editeurs = $findEditeurs->queryAll(true, [':titreId' => $titre['id']]);
			$pos = 1;
			foreach ($editeurs as $e) {
				$titre["editeur$pos"] = $e['nom'];
				$titre["pays$pos"] = $e['pays'];
				$pos++;
			}
			while ($pos < 4) {
				$titre["editeur$pos"] = '';
				$titre["pays$pos"] = '';
				$pos++;
			}
			$titre['langues'] = join(", ", json_decode($titre['langues']));
			fputcsv($out, $titre, "\t", '"', '\\');
		}
	}

	/**
	 * Print the CSV for all titles: {ppn};htpps://mirabel/revue/titre-id/{titreId}
	 */
	public static function printPpnCsv(): void
	{
		$prefix = Yii::app()->params->itemAt('baseUrl') . "/revue/titre-id/";

		$out = fopen('php://output', 'w');
		fputcsv($out, ["PPN", "URL Mir@bel"], ';', '"', '\\');

		$sql = "SELECT sudocPpn, titreId FROM Issn WHERE sudocPpn != '' GROUP BY sudocPpn";
		$cmd = Yii::app()->db->createCommand($sql)->query();
		$cmd->setFetchMode(\PDO::FETCH_NUM);
		while ($row = $cmd->read()) {
			fputcsv($out, [$row[0], $prefix . $row[1]], ';', '"', '\\');
		}
	}

	/**
	 * Print the CSV for all titles: {ppn};htpps://mirabel/revue/titre-id/{titreId}
	 */
	public static function printBnfCsv(): void
	{
		$out = fopen('php://output', 'w');
		fputcsv($out, ["ISSN", "ISSN-L", "ISSN support", "bnfArk", "ID titre Mirabel", "ID revue Mirabel", "URL Mirabel", "Titre", "Debut Mirabel", "Fin Mirabel"], ';', '"', '\\');

		$sql = <<<EOSQL
			SELECT i.issn, i.issnl, i.support, i.bnfArk, t.id as idTitre, t.revueId as idRevue,
			    CONCAT('https://reseau-mirabel.info/revue/issn/',i.issn) AS URL, t.titre,
			    t.dateDebut as DebutMirabel, t.dateFin as FinMirabel
			FROM Issn i
				JOIN Titre t ON i.titreid = t.id
				JOIN Service s ON i.titreId = s.titreId WHERE s.type = 'Intégral'
			GROUP BY i.issn
			ORDER BY t.id ASC
			EOSQL;
		$cmd = Yii::app()->db->createCommand($sql)->query();
		$cmd->setFetchMode(\PDO::FETCH_NUM);
		while ($row = $cmd->read()) {
			fputcsv($out, $row, ';', '"', '\\');
		}
	}
}
