<?php

namespace processes\titres;

use components\SqlHelper;
use Intervention;
use Titre;

class Editeurs
{
	/**
	 * @var int 0 or Editeur.id
	 */
	public int $returnToPolitique = 0;

	public Titre $titre;

	/**
	 * @var \TitreEditeur[]
	 */
	public array $roles = [];

	/**
	 * @var \Editeur[]
	 */
	public array $editeurs = [];

	private int $userId;

	/**
	 * @param int $userId ID for current user
	 * @param Titre $titre Titre to set up
	 * @param int $editeurId If X > 0, post success will return to /politique/index?editeurId=X
	 */
	public function __construct(int $userId, Titre $titre, int $editeurId)
	{
		$this->userId = $userId;
		$this->titre = $titre;
		$this->returnToPolitique = $editeurId;

		$this->roles = SqlHelper::sqlToPairsObject(
			"SELECT * FROM Titre_Editeur WHERE titreId = {$this->titre->id}",
			'TitreEditeur',
			'editeurId'
		);
		$this->editeurs = SqlHelper::sqlToPairsObject(
			"SELECT e.* FROM Editeur e JOIN Titre_Editeur te ON te.editeurId = e.id WHERE titreId = {$this->titre->id} ORDER BY e.nom",
			'Editeur'
		);
	}

	public function createIntervention(array $post): ?Intervention
	{
		$i = new Intervention();
		$i->action = 'revue-U';
		$i->description = "Rôles des éditeurs du titre « {$this->titre->titre} »";
		$i->titreId = $this->titre->id;
		$i->revueId = $this->titre->revueId;
		$i->import = 0;
		$i->utilisateurIdProp = $this->userId;
		$i->statut = 'attente';
		$i->contenuJson = new \InterventionDetail();
		$i->hdateProp = time();
		$i->ip = $_SERVER['REMOTE_ADDR'];

		$valid = true;
		foreach ($this->roles as $te) {
			if (isset($post[$te->editeurId])) {
				$newValues = array_intersect_key(
					$post[$te->editeurId],
					['ancien' => 1, 'commercial' => 1, 'intellectuel' => 1, 'role' => 1]
				);
				$old = clone $te;
				$te->setAttributes($newValues);
				if ($old->attributes === $te->attributes) {
					continue;
				}
				if ($te->validate()) {
					$i->contenuJson->update($old, $te);
				} else {
					$valid = false;
				}
			}
		}
		if (!$valid) {
			return null;
		}
		return $i;
	}
}
