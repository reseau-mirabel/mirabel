<?php

namespace processes\titres;

use CDbCriteria;
use components\SqlHelper;
use Identification;
use Ressource;
use Titre;

/**
 * /titre/identification.
 */
class Identify
{
	public Identification $creation;

	/**
	 * @var Identification[][]  Ressource.id => [...identifications...]
	 */
	public array $identifications = [];

	/**
	 * @var Ressource[]
	 */
	public array $ressources;

	public Titre $titre;

	/**
	 * @var Identification[]
	 */
	private array $toDelete = [];

	/**
	 * @var Identification[]
	 */
	private array $toSave = [];

	public function __construct(Titre $titre)
	{
		$this->titre = $titre;

		// add the Identification records from the Ressources linked by a Service
		$this->ressources = $this->getRessourcesByServices();
		$ids = [];
		foreach ($this->ressources as $ressource) {
			$attributes = [
				'titreId' => $this->titre->id,
				'ressourceId' => $ressource->id,
			];
			$idents = Identification::model()->findAllByAttributes($attributes);
			if ($idents) {
				$this->identifications[$ressource->id] = $idents;
				foreach ($idents as $i) {
					$ids[] = $i->id;
				}
			} else {
				$empty = new Identification();
				$empty->titreId = $this->titre->id;
				$empty->ressourceId = $ressource->id;
				$this->identifications[$ressource->id] = [$empty];
			}
		}

		// add the other Identification records
		$criteria = new CDbCriteria();
		$criteria->addColumnCondition(['titreId' => $this->titre->id]);
		$criteria->addNotInCondition('id', $ids);
		$criteria->order = 'ressourceId';
		$idents = Identification::model()->findAll($criteria);
		if ($idents) {
			foreach ($idents as $ident) {
				assert($ident instanceof Identification);
				assert($ident->ressourceId > 0);
				if (isset($this->identifications[$ident->ressourceId])) {
					$this->identifications[$ident->ressourceId][] = $ident;
				} else {
					$r = Ressource::model()->findByPk($ident->ressourceId);
					if ($r instanceof Ressource) {
						$this->identifications[$ident->ressourceId] = [$ident];
						$this->ressources[$ident->ressourceId] = $r;
					}
				}
			}
		}
		unset($idents);

		$this->creation = new Identification();
		$this->creation->titreId = $this->titre->id;
	}

	public function load(array $data): bool
	{
		$valid = true;
		$this->toSave = [];

		foreach ($this->identifications as $rid => $idents) {
			foreach ($idents as $num => $ident) {
				$ident->setAttributes($data[$rid][$num]);
				if ($ident->idInterne) {
					$valid = $ident->validate() && $valid;
					if ($valid) {
						$this->toSave[] = $ident;
					}
				} else {
					if (!$ident->isNewRecord && $ident->id) {
						$this->toDelete[] = $ident;
						unset($this->identifications[$rid][$num]);
					}
				}
			}
		}

		$this->creation->setAttributes($data[0]);
		if ($this->creation->idInterne) {
			$valid = $this->creation->validate() && $valid;
			if ($valid) {
				$this->toSave[] = $this->creation;
			}
		}

		return $valid;
	}

	public function save(): bool
	{
		foreach ($this->toDelete as $ident) {
			$ident->delete();
		}
		foreach ($this->toSave as $ident) {
			$ident->save(false);
		}
		return true;
	}

	/**
	 * Returns the list of Ressource that have a Service on this Titre.
	 *
	 * @return Ressource[]
	 */
	private function getRessourcesByServices(): array
	{
		if (empty($this->titre->id)) {
			return [];
		}
		return SqlHelper::sqlToPairsObject(
			"SELECT r.* FROM Ressource r JOIN Service s ON r.id=s.ressourceId "
			. "WHERE s.titreId = " . $this->titre->id . " ORDER BY r.nom ASC",
			'Ressource'
		);
	}
}
