<?php

namespace processes\titres;

use Yii;
use Revue;
use Titre;

class ChangeJournal
{
	public const RETURN_OK = 0;

	public const RETURN_NOTFOUND = 1;

	public const RETURN_EMPTY = 2;

	public const RETURN_ERROR = 3;

	private Titre $titre;

	private int $revueId = 0;

	private string $errorMessage = '';

	public function __construct(Titre $titre)
	{
		$this->titre = $titre;
	}

	public function run(int $revueId): int
	{
		$revue = $this->prepareRevue($revueId);
		if ($revue === null) {
			if ($revueId > 0) {
				return self::RETURN_NOTFOUND;
			}
			return self::RETURN_EMPTY;
		}

		$i = $this->titre->buildIntervention(true);
		$new = clone $this->titre;
		$new->revueId = $revue->id;
		$previousTitle = Titre::model()->findByAttributes(['revueId' => $this->titre->revueId, 'obsoletePar' => $this->titre->id]);
		if ($previousTitle) {
			$previousTitle->updateByPk($previousTitle->id, ['obsoletePar' => $this->titre->obsoletePar]);
		}

		$oldestTitle = Titre::model()->findBySql("SELECT * FROM Titre WHERE revueId = :rid ORDER BY (dateFin = ''), dateFin LIMIT 1", [':rid' => $revue->id]);
		if ($oldestTitle) {
			$new->obsoletePar = $oldestTitle->id;
		} else {
			$new->obsoletePar = null;
		}
		$i->description = "Déplacement du titre « {$new->titre} »";
		$i->contenuJson->update($this->titre, $new);
		$i->revueId = $revue->id;
		$this->revueId = (int) $revue->id;
		if (Titre::model()->countByAttributes(['revueId' => $this->titre->revueId]) < 2) {
			$i->contenuJson->delete($this->titre->revue);
			$this->redirectRevue($this->titre->revueId, $revue->id);
		}
		$i->contenuJson->confirm = true;
		if ($i->accept()) {
			return self::RETURN_OK;
		}
		$this->errorMessage = print_r($i->getErrors(), true);
		return self::RETURN_ERROR;
	}

	public function getErrorMessage(): string
	{
		return $this->errorMessage;
	}

	public function getRevueId(): int
	{
		return (int) $this->revueId;
	}

	private function redirectRevue(int $oldid, int $newid): void
	{
		Yii::app()->db->createCommand()->insert('RedirectionRevue', [
			'sourceId' => $oldid,
			'cibleId' => $newid,
			'hdateModif' => time(),
		]);
		$sql = "UPDATE RedirectionRevue SET cibleId = :new WHERE cibleId = :old";
		Yii::app()->db->createCommand($sql)->execute([':new' => $newid, ':old' => $oldid]);
	}

	private function prepareRevue(int $revueId): ?Revue
	{
		if ($revueId > 0) {
			return Revue::model()->findByPk($revueId);
		}

		if (Titre::model()->countByAttributes(['revueId' => $this->titre->revueId]) == 1) {
			return null;
		}

		$revue = new Revue();
		$revue->statut = 'normal';
		$revue->save();
		return $revue;
	}
}
