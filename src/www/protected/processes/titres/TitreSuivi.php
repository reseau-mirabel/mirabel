<?php

namespace processes\titres;

use Partenaire;
use Titre;
use Yii;

class TitreSuivi
{
	public static function update(Titre $titre): bool
	{
		$user = Yii::app()->user;
		if (!$user->checkAccess("avec-partenaire")) {
			return false;
		}
		if ($titre->suivre && $titre->revueId && !$titre->monitoredBy($user->partenaireId)) {
			$titre->dbConnection
				->createCommand(
					"INSERT IGNORE INTO Suivi (partenaireId, cible, cibleId) VALUES "
					. "(:partenaireId, 'Revue', :id)"
				)
				->execute([':partenaireId' => $user->partenaireId, ':id' => $titre->revueId]);
			// Hook to refresh the Manticore index of these Titre records.
			$titre->dbConnection
				->createCommand(
					"INSERT IGNORE INTO IndexingRequiredTitre (id, updated, deleted)"
					. " VALUES ({$titre->id}, unix_timestamp(), 0)"
				)
				->execute();
			Yii::app()->user->setState(
				'suivi',
				Partenaire::model()->findByPk($user->partenaireId)->getRightsSuivi()
			);
			Yii::app()->user->setFlash('info suivi', "Votre partenaire suit désormais cette revue.");
		}
		return true;
	}
}
