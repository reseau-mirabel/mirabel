<?php

namespace processes\titres;

use processes\images\Downloader as ImageDownloader;

class TitreCouverture extends ImageDownloader
{
	public function __construct()
	{
		$public = DATA_DIR . '/public';
		if (!is_dir("$public/images")) {
			throw new \Exception("Chemin vers les images introuvable");
		}
		$dest = DATA_DIR . '/images/titres-couvertures';
		if (!is_dir($dest)) {
			mkdir($dest, 0777, true);
		}
		parent::__construct(
			"$public/images/titres-couvertures",
			DATA_DIR . '/images/titres-couvertures'
		);
	}

	public function getResizer(): \processes\images\Resizer
	{
		return new \processes\images\Resizer($this->path, $this->pathRaw);
	}
}
