<?php

namespace processes\titres;

use Issn;
use models\sudoc\ApiClient;
use models\sudoc\Notice;
use Titre;

/**
 * /titre/ajax-sudoc
 */
class AjaxSudoc
{
	private ApiClient $api;

	private string $errorMessage = '';

	/**
	 * @var Notice[]
	 */
	private array $notices = [];

	public function __construct(?ApiClient $api = null)
	{
		$this->api = ($api === null ? new ApiClient() : $api);
	}

	public function getErrorMessage(): string
	{
		return $this->errorMessage;
	}

	/**
	 * @return array<string, mixed>
	 */
	public function getResponseBody(bool $withHtml): array
	{
		$issnportalUpdates = self::updateWithIssnportal($this->notices);
		$response = [
			'notices' => $this->notices,
		];
		if ($issnportalUpdates) {
			$response['divergences_sudoc_issn'] = [];
			foreach ($issnportalUpdates as $issn => $x) {
				$response['divergences_sudoc_issn'][$issn] = $x['changes'];
			}
		}

		if ($withHtml) {
			$data = $this->getHtmlData();
			$response['html'] = preg_replace('/\s+/s', ' ', $this->renderHtmlContent($data));
			$response['duplicates'] = $this->findDuplicates();
		}
		return $response;
	}

	public function getResponseCode(): int
	{
		if (!$this->notices) {
			return 404;
		}
		return 200;
	}

	public function queryByIssn(string $issn): bool
	{
		if (!$issn) {
			return false;
		}
		try {
			$this->add($this->api->getNoticeByIssn($issn));
		} catch (\Exception $e) {
			$this->errorMessage = $e->getMessage();
			return false;
		}
		return true;
	}

	public function queryByIssns(string $issns): bool
	{
		if (!$issns) {
			return false;
		}
		try {
			foreach (preg_split('/[\s,]+/', $issns) as $issn) {
				$this->add($this->api->getNoticeByIssn($issn));
			}
		} catch (\Exception $e) {
			$this->errorMessage = $e->getMessage();
			return false;
		}
		return true;
	}

	public function queryByPpn(string $ppn): bool
	{
		if (!$ppn) {
			return false;
		}
		try {
			$this->add($this->api->getNotice($ppn));
		} catch (\Exception $e) {
			$this->errorMessage = $e->getMessage();
			return false;
		}
		return true;
	}

	private function add(?Notice $notice): void
	{
		if ($notice) {
			$this->notices[] = $notice;
		}
	}

	private function findDuplicates(): array
	{
		$issns = [];
		foreach ($this->notices as $notice) {
			$issns[] = $notice->issn;
		}
		if (!$issns) {
			return [];
		}
		$rows = \Yii::app()->db
			->createCommand("SELECT issn, titreId, revueId FROM Issn JOIN Titre t ON t.id = Issn.titreId WHERE issn IN('" . join("','", $issns) . "')")
			->queryAll();
		foreach ($rows as $i => $row) {
			$row[$i]['titreId'] = (int) $row['titreId'];
			$row[$i]['revueId'] = (int) $row['revueId'];
		}
		return $rows;
	}

	private function getHtmlData(): array
	{
		if (!$this->notices) {
			return [];
		}

		$titre = new Titre();
		TitreUpdater::fillBySudocNotices($titre, $this->notices);
		$issnRecords = [];
		$notice = null;
		foreach ($this->notices as $notice) {
			$issnR = new Issn();
			$issnR->fillBySudocNotice($notice);
			$issnRecords[] = $issnR;
		}
		return [
			'notice' => $notice, // last notice
			'titre' => $titre,
			'issns' => $issnRecords,
		];
	}

	private function renderHtmlContent(array $data): string
	{
		if (!$data) {
			return
				<<<EOHTML
				<div class="alert alert-warning">
				Cet ISSN est introuvable dans le Sudoc. Vérifiez que votre ISSN est correctement saisi.

				Si vous préférez créer le titre sans passer par l'import Sudoc, utilisez le bouton « Créer un titre sans ISSN »
				</div>
				EOHTML;
		}
		return (string) \Yii::app()->controller->renderPartial(
			'_sudoc-info',
			$data,
			true
		);
	}

	/**
	 * Update each record with data fetched from ISSN Portal.
	 */
	private static function updateWithIssnportal(array $records): array
	{
		if (!$records) {
			return [];
		}
		$updater = new \processes\issn\IssnOrgPortal(['issnl', 'name', 'pays', 'support']);
		return array_filter($updater->updateRecords($records), fn ($r) => !empty($r['changes']));
	}
}
