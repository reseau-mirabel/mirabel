<?php

namespace processes\titres;

use Intervention;
use Politique;
use TitreEditeur;

/**
 * Update an Intervention according to the Titre-Editeur relations sent by a form.
 */
class Publishers
{
	private string $error = '';

	private Intervention $interv;

	private int $titreId;

	public function __construct(int $titreId, Intervention $interv)
	{
		$this->titreId = $titreId;
		$this->interv = $interv;
	}

	public function getError(): string
	{
		return $this->error;
	}

	public function linkAndUnlink(array $dataOld, array $dataNew): void
	{
		// detach old relations and update existing relations
		if ($this->titreId && !empty($dataOld)) {
			$this->updateExisting($dataOld);
		}

		// attach new relations
		if (isset($dataNew[0])) {
			unset($dataNew[0]); // template for JS extensions
		}
		if (!empty($dataNew)) {
			$this->attachPublishers($dataNew);
		}
	}

	private function updateExisting(array $data): void
	{
		foreach ($data as $eId => $rel) {
			$relation = TitreEditeur::model()->findByPk(['titreId' => $this->titreId, 'editeurId' => (int) $eId]);
			if ($relation === null) {
				continue;
			}
			if ($rel['editeurId'] === '0') {
				$this->detachPublisher($relation);
			} else {
				$relation->commercial = (int) $relation->commercial;
				$relation->intellectuel = (int) $relation->intellectuel;
				$this->interv->contenuJson->updateByAttributes(
					$relation,
					[
						'ancien' => isset($rel['ancien']) ? (int) $rel['ancien'] : null,
						'commercial' => (int) ($rel['commercial'] ?? 0),
						'intellectuel' => (int) ($rel['intellectuel'] ?? 0),
					]
				);
			}
		}
	}

	private function attachPublishers(array $edData): void
	{
		foreach ($edData as $data) {
			if (empty($data['editeurId'])) {
				// unchecked
				continue;
			}
			$relation = new TitreEditeur;
			$relation->titreId = $this->titreId;
			$relation->editeurId = (int) $data['editeurId'];
			$relation->ancien = isset($data['ancien']) ? (int) $data['ancien'] : null;
			$relation->commercial = (int) ($data['commercial'] ?? 0);
			$relation->intellectuel = (int) ($data['intellectuel'] ?? 0);
			if ($relation->editeurId > 0 && !empty($relation->editeur)) {
				$this->interv->contenuJson->create(
					$relation,
					"Attache l'éditeur « {$relation->editeur->nom} »"
				);
			}
		}
	}

	private function detachPublisher(TitreEditeur $relation): void
	{
		$nomEditeur = $relation->editeur->nom;
		$politique = Politique::model()->findBySql(<<<EOSQL
			SELECT p.*
			FROM Politique_Titre pt
				JOIN Politique p ON pt.politiqueId = p.id
			WHERE pt.titreId = :tid AND p.editeurId = :eid
			LIMIT 1
			EOSQL,
			[':tid' => $relation->titreId, ':eid' => $relation->editeurId]
		);
		if ($politique instanceof Politique) {
			$this->error = "Une politique de publication existe pour l'éditeur « {$nomEditeur} »."
				. " Seuls les modérateurs habilités peuvent détacher cette politique puis cet éditeur.";
			return;
		}
		$this->interv->contenuJson->delete(
			$relation,
			"Détache l'éditeur « {$nomEditeur} »"
		);
	}
}
