<?php

namespace processes\titres;

use Yii;
use Titre;

/**
 * Process used to make modification in the obsoletePar order of the Titre from a Revue.
 */
class TrierTitre
{
	/**
	 * Reorder the titles of a given review by updating their 'obsolete by' attribute.
	 *
	 * @return bool Returns true if the operation was successful, false otherwise
	 */
	public static function reorderTitreInDb(array $orderIds): bool
	{
		$titre = Titre::model()->findByPk($orderIds[0]);
		$intervention = $titre->buildIntervention(true);
		$intervention->action = 'revue-U';
		$intervention->description = "Tri des titres de la revue {$titre->titre}";
		$intervention->setImport(-1);

		$previousId = null;
		foreach ($orderIds as $id) {
			$titre = Titre::model()->findByPk($id);
			$intervention->contenuJson->updateByAttributes($titre, ['obsoletePar' => $previousId]);
			$previousId = $id;
		}
		if (!$intervention->accept()) {
			$errors = json_encode($intervention->getErrors());
			Yii::app()->user->setFlash('warning', "Erreur lors de la validation de l'intervention, Erreurs : {$errors}");
			return false;
		}
		return true;
	}

	/**
	 * Take a Revue and return the list of the 'obsoletePar' IDs who are duplicate or not belonging at this Revue, it'.
	 *
	 * @return array{"duplicate": array, "notInRevue": array} Return an associative array with two keys :
	 *        'duplicate' contains the ids that mark multiple Titre as obsolete
	 *        'notInRevue' contains the ids that mark a Titre of the Revue but do not belong to the Revue
	 */
	public static function verifyObsoleteByIds(\Revue $revue): array
	{
		$obsoleteByIds = array_filter(array_map(
			fn ($titre) => $titre->obsoletePar,
			$revue->titres
		));

		$occurences = array_count_values($obsoleteByIds);
		$duplicate = [];
		foreach ($occurences as $id => $occurence) {
			if ($occurence > 1) {
				$duplicate[] = $id;
			}
		}

		$titresIds = array_map(fn ($titre) => $titre->id, $revue->titres);
		$idNotInRevue = array_diff($obsoleteByIds, $titresIds);

		return [
			'duplicate' => $duplicate,
			'notInRevue' => $idNotInRevue,
		];
	}

	/**
	 * Return true if the ordering is complete, i.e. all the Titre.id are present.
	 */
	public static function isInputValid(\Revue $revue, array $newOrderIds): bool
	{
		if (!$newOrderIds) {
			return false;
		}
		$titresIds = array_map(fn ($titre) => $titre->id, $revue->titres);
		$common = array_intersect($newOrderIds, $titresIds);
		return count($common) === count($titresIds);
	}

	/**
	 * Return true if the new ordering based on the obsolescence chain is different from the original one.
	 */
	public static function isOrderChanged(\Revue $revue, array $newOrderIds): bool
	{
		$titreOrderedByObsoleteParChain = $revue->getTitresOrderedByObsolete();
		$originalOrderIds = array_map(fn ($item) => $item['id'], $titreOrderedByObsoleteParChain);
		return $newOrderIds != $originalOrderIds;
	}

	/**
	 * Return true if there is a duplicate/not from revue obsoletePar in the revue.
	 */
	public static function isProblemInObsoleteParValue(\Revue $revue): bool
	{
		$obsoleteparProblemList = self::verifyObsoleteByIds($revue);
		return !empty($obsoleteparProblemList["duplicate"]) || !empty($obsoleteparProblemList["notInRevue"]);
	}

	/**
	 * Returns true if there are any titles with date problems in the revue:
	 * - Non-active titles without end dates
	 * - End date of title is strictly after the start date of its successor
	 */
	public static function hasTitresWithDateProblem(\Revue $revue): bool
	{
		$hasTitres = Yii::app()->db->createCommand(<<<EOSQL
			WITH TitresOrdered AS (
				SELECT
					titrePredecesseur.id,
					titrePredecesseur.dateFin,
					titrePredecesseur.obsoletePar,
					titreSuccesseur.dateDebut as dateDebutSuccesseur
				FROM Titre titrePredecesseur
					LEFT JOIN Titre titreSuccesseur ON titrePredecesseur.obsoletePar = titreSuccesseur.id
				WHERE titrePredecesseur.revueId = :revueId
			)
			SELECT 1
			FROM TitresOrdered
			WHERE
				obsoletePar IS NOT NULL -- Titre avec successeur
				AND
				(dateFin = '' OR LEFT(dateFin, LENGTH(dateDebutSuccesseur)) > dateDebutSuccesseur) -- Sans dateFin, ou Datefin > DateDebutSuccesseur
			LIMIT 1
			EOSQL
		)->queryScalar([':revueId' => $revue->id]);
		return (bool) $hasTitres;
	}

	/**
	 * Return a list on titreId with date problem.
	 */
	public static function getTitresIdWithDateProblem(\Revue $revue): array
	{
		$titres = $revue->getTitresOrderedByObsolete();
		if (count($titres) <= 1) {
			return [];
		}

		$titresIdWithInconsistencies = [];
		for ($i = 0; $i < count($titres) - 1; $i++) {
			$titreSuccesseur = $titres[$i];
			$titrePredecesseur = $titres[$i + 1];

			# titre non actif sans date de fin
			if ($titrePredecesseur['dateFin'] === '') {
				$titresIdWithInconsistencies[] = $titrePredecesseur["id"];
			}
			# dateFin titres précédent > dateDébut titre en gérant les différents format de date (AAAA, AAAA-MM, AAAA-MM-DD)
			elseif (substr($titrePredecesseur['dateFin'], 0, strlen($titreSuccesseur['dateDebut'])) > $titreSuccesseur['dateDebut']) {
				$titresIdWithInconsistencies[] = $titreSuccesseur["id"];
				$titresIdWithInconsistencies[] = $titrePredecesseur["id"];
			}
		}
		return $titresIdWithInconsistencies;
	}

	/**
	 * return true in the revue has a obsoletePar or a date problem.
	 */
	public static function isRevueNeedReordering(\Revue $revue): bool
	{
		return self::isProblemInObsoleteParValue($revue) || self::hasTitresWithDateProblem($revue);
	}

	public static function getRevueIdWithObsoleteParProblemsByPartenaire(\Partenaire $partenaire): array
	{
		// Récupération de tous les revueId suivis par un partenaire qui ont un problème de doublons de "obsoletePar" ou d'incohérence de date.
		return \Yii::app()->db
			->createCommand(<<<EOSQL
				WITH RevuePartenaire AS (
					SELECT t.revueId
					FROM Suivi s
						JOIN Titre t ON s.cible = 'Revue' AND s.cibleId = t.revueId
					WHERE s.partenaireId = {$partenaire->id}
					GROUP BY t.revueId
					HAVING count(*) > 1
				)
				-- Revue Id avec des problèmes de dates
				SELECT DISTINCT titre_predecesseur.revueId
				FROM Titre titre_predecesseur
					JOIN RevuePartenaire USING(revueId)
					LEFT JOIN Titre titre_successeur ON titre_predecesseur.obsoletePar =titre_successeur.id
				WHERE
					titre_predecesseur.obsoletePar IS NOT NULL -- Titre with successor
					AND
					(titre_predecesseur.dateFin = '' OR LEFT(titre_predecesseur.dateFin, LENGTH(titre_successeur.dateDebut)) > titre_successeur.dateDebut) -- Without dateFin, or Datefin > DateDebutSuccessor
				UNION
				-- RevueId avec des doublons d'obsoletePar
				SELECT DISTINCT revueId
				FROM Titre
					JOIN RevuePartenaire USING(revueId)
				WHERE obsoletePar IS NOT NULL
				GROUP BY obsoletePar
				HAVING count(*) > 1
				EOSQL
			)
			->queryColumn();
	}
}
