<?php

namespace processes\titres;

use Issn;
use Titre;

class TitreUpdater
{
	/**
	 * Update the Titre record.
	 *
	 * @param array<?\models\sudoc\Notice> $notices
	 */
	public static function fillBySudocNotices(Titre $titre, array $notices): void
	{
		if (!$notices) {
			return;
		}
		$synthesis = [
			'dateDebut' => null,
			'dateFin' => null,
			'langues' => null,
			'titre' => null,
		];
		foreach ($notices as $notice) {
			if (!$notice) {
				continue;
			}
			if ($notice->dateDebut && (!$synthesis['dateDebut'] || $notice->dateDebut < $synthesis['dateDebut'])) {
				$synthesis['dateDebut'] = $notice->dateDebut;
			}
			if ($synthesis['dateFin'] !== '') {
				if ($notice->dateFin === '' && $notice->dateDebut) {
					$synthesis['dateFin'] = '';
				} elseif ($notice->dateFin && ($synthesis['dateFin'] === null || $notice->dateFin > $synthesis['dateFin'])) {
					$synthesis['dateFin'] = $notice->dateFin;
				}
			}
			if ($notice->langues && !$synthesis['langues']) {
				$synthesis['langues'] = $notice->langues;
			}
			if ($notice->titre && !$synthesis['titre']) {
				$synthesis['titre'] = $notice->titre;
			}
		}
		$titre->setAttributes(
			array_filter(
				$synthesis,
				function ($x) {
					return $x !== null;
				}
			),
			false
		);
	}

	/**
	 * @param Titre $titre Updated by this method
	 * @param Issn[] $issns
	 * @return array changed attributes [name => [before, after]]
	 */
	public static function updateDatesFromIssn(Titre $titre, array $issns): array
	{
		if (!$issns) {
			return [];
		}
		$changed = [];

		$min = null;
		foreach ($issns as $issn) {
			if (strpos($issn->dateDebut, 'X') !== false) {
				return [];
			}
			if ($issn->dateDebut && (!$min || $issn->dateDebut < $min)) {
				$min = $issn->dateDebut;
			}
		}
		if ($min !== null && $titre->dateDebut !== $min) {
			$changed['dateDebut'] = [$titre->dateDebut, $min];
			$titre->dateDebut = $min;
		}

		$max = null;
		foreach ($issns as $issn) {
			if (strpos($issn->dateFin, 'X') !== false) {
				return [];
			}
			if ($issn->dateDebut) {
				if ($issn->dateFin === '') {
					$max = '';
					break;
				}
				if ($issn->dateFin > $max) {
					$max = $issn->dateFin;
				}
			}
		}
		if ($max !== null && $titre->dateFin !== $max) {
			$changed['dateFin'] = [$titre->dateFin, $max];
			$titre->dateFin = $max;
		}

		return $changed;
	}
}
