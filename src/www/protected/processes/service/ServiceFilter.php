<?php

namespace processes\service;

use Service;

class ServiceFilter extends \CModel
{
	public const BACON_DOAJ = 'doaj';

	public const BACON_EPISCIENCES = 'episciences';

	public const BACON_GLOBAL = 'global';

	public const BACON_LIBRE = 'libre';

	public const BACON_REPERES = 'reperes';

	public $acces;

	public $bacon = '';

	public $ressourceId;

	public $type;

	public $lacunaire;

	public $selection;

	public $hdateModif;

	/**
	 * @return string[]
	 */
	public function attributeNames(): array
	{
		return ['acces', 'bacon', 'ressourceId', 'type', 'lacunaire', 'selection', 'hdateModif'];
	}

	public function rules(): array
	{
		return [
			['acces', 'in', 'range' => array_keys(Service::$enumAcces)],
			['type', 'safe'], // 'in', 'range' => array_keys(Service::$enumType)],
			['ressourceId', 'numerical', 'integerOnly' => true],
			['selection, lacunaire', 'boolean'],
			['bacon', 'in', 'range' => ['', self::BACON_DOAJ, self::BACON_EPISCIENCES, self::BACON_GLOBAL, self::BACON_LIBRE, self::BACON_REPERES]],
			['hdateModif', 'date', 'format' => 'yyyy-MM-dd'],
		];
	}

	public function afterValidate(): void
	{
		// when searching, validate type as an array of types
		if (!empty($this->type)) {
			if (!is_array($this->type)) {
				$this->type = [$this->type];
			}
			foreach ($this->type as $type) {
				if (!isset(Service::$enumType[$type])) {
					$this->addError('type', "Une valeur sélectionnée est inconnue.");
				}
			}
		}
		parent::afterValidate();
	}

	public function isBacon(): bool
	{
		return $this->bacon !== '';
	}

	public function onUnsafeAttribute($name, $value)
	{
		// do nothing
	}
}
