<?php

namespace processes\service;

use components\email\Mailer;
use Ressource;
use Yii;

/**
 * See VanishedServices to know what will we displayed and to who.
 *
 * Usage:
 * $x = new VanishedServicesNotify("1 week ago");
 * $x->run(0); // or $x->run($ressourceId);
 * $x->sendEmails(); // or $x->displayReport();
 */
class VanishedServicesNotify
{
	private const EMAIL_DEFAULT = 'mirabel_donnees@listes.sciencespo-lyon.fr';

	private VanishedServices $finder;

	private int $verbose = 1;

	public function __construct(string $since)
	{
		$finder = new VanishedServices();
		$finder->defaultTo = self::EMAIL_DEFAULT;
		$finder->verbose = $this->verbose;
		$finder->setSince($since);
		$this->finder = $finder;
	}

	public function setVerbose(int $level): void
	{
		$this->verbose = $level;
		$this->finder->verbose = $level;
	}

	public function run(int $ressourceId)
	{
		if ($ressourceId > 0) {
			$ressources = [Ressource::model()->findByPk($ressourceId)];
		} else {
			$ressources = Yii::app()->db->createCommand(
				<<<EOSQL
				SELECT r.*, GROUP_CONCAT(c.id) AS collectionIds
				FROM Ressource r
					LEFT JOIN Collection c ON c.ressourceId = r.id
				WHERE r.autoImport = 1 OR c.importee = 1
				GROUP BY r.id
				ORDER BY r.nom ASC
				EOSQL
			)->queryAll();
		}
		foreach ($ressources as $row) {
			$ressource = Ressource::model()->populateRecord($row);
			$collectionIds = (string) $row['collectionIds'];
			$this->finder->find($ressource, $collectionIds);
		}
	}

	public function getHtmlReport(): string
	{
		$html = "";
		foreach ($this->finder->getEmails() as $to => $bodies) {
			$htmlBody = htmlspecialchars(join("\n", $bodies));
			$html .= sprintf(
				"<details>\n<summary>import et accès supprimés → %s</summary>\n<div>\n%s\n</div></details>",
				htmlspecialchars($to),
				nl2br($htmlBody)
			);
		}
		return $html;
	}

	public function getTextReport(): string
	{
		$text = "";
		foreach ($this->finder->getEmails() as $to => $bodies) {
			$text .= Yii::app()->name . " : import et accès supprimés\n\n# To: $to\n" . join("\n", $bodies) . "\n-----\n\n";
		}
		return $text;
	}

	public function sendEmails(): void
	{
		foreach ($this->finder->getEmails() as $toStr => $bodies) {
			$toList = array_filter(array_map('trim', explode("\n", $toStr)));
			if (count($toList) > 1) {
				$to = $toList;
			} elseif (count($toList) === 1) {
				$to = $toList[0];
			} else {
				$to = self::EMAIL_DEFAULT;
			}
			if ($this->verbose > 1) {
				fprintf(STDERR, "To: %s\n", join(", ", $toList));
			}
			$message = Mailer::newMail()
				->subject(Yii::app()->name . " : import et accès supprimés")
				->from(self::EMAIL_DEFAULT)
				->setTo($to)
				->text(join("\n", $bodies));
			if ($to !== self::EMAIL_DEFAULT) {
				$message->addCc(self::EMAIL_DEFAULT);
			}
			Mailer::sendMail($message);
		}
	}
}
