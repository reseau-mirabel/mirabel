<?php

namespace processes\service;

use Service;

/**
 * Détecte les accès jamais importés alors qu'ils devraient l'être, ou importés mais pas récemment.
 * Prépare des courriels destinés aux partenaires-veilleurs suivant ces données,
 * ou destinés à une adresse par défaut si personne ne suit.
 */
class VanishedServices
{
	public string $defaultTo = '';

	public int $verbose = 0;

	private string $baseUrl = '';

	private array $emailTo = [];

	private int $sinceTs = 0;

	public function __construct()
	{
		$this->emailTo = [];
		if (\Yii::app()->params->contains('baseUrl')) {
			$this->baseUrl = \Yii::app()->params->itemAt('baseUrl');
		}
	}

	public function find(\Ressource $r, string $collectionIds): void
	{
		if (!preg_match('/^[\d,]*$/', $collectionIds)) {
			throw new \Exception("Le paramètre collectionIds est faux.");
		}
		$body = "";
		if ($this->sinceTs) {
			$fromDate = $this->sinceTs;
		} else {
			$fromDate = Service::getLastImportTimestamp($r->id);
			$body .= "  Avant-dernière intervention d'import : " . date('Y-m-d H:s', $fromDate) . "\n";
		}
		$services = $this->findDeletedFromImport($r->id, $collectionIds, $fromDate);
		if ($services) {
			foreach ($services as $s) {
				$body .= sprintf("  * %s  —  %s/titre/view/%d\n\taccès %d\n", $s->titre->titre, $this->baseUrl, $s->titreId, $s->id);
			}
		} elseif ($this->verbose > 1) {
			$body .= "  - tous les accès ont été actualisés.\n";
		}
		if ($this->verbose) {
			$list = $this->findNeverImported((int) $r->id, $collectionIds);
			if ($list) {
				$body .= " ### Accès jamais importés\n";
				foreach ($list as $s) {
					\Yii::log(
						sprintf(
							"Accès jamais importé)\nRessource ID=%d\nService (ID=%d, import='%d', hdateImport=%s)\n",
							$r->id,
							$s->id,
							$s->import,
							$s->hdateImport === null ? "NULL" : $s->hdateImport
						),
						\CLogger::LEVEL_INFO,
						'import'
					);
					$body .= sprintf("  * %s\n\t%s/titre/view/%d  accès %d\n", $s->titre->titre, $this->baseUrl, $s->titreId, $s->id);
				}
			}
		}
		if ($this->verbose > 1) {
			$list = $this->findNew((int) $r->id, $collectionIds, $fromDate);
			if ($list) {
				$body .= " ### Accès nouveaux\n";
				foreach ($list as $s) {
					$body .= sprintf("  * %s\n\t%s/titre/view/%d  accès %d\n", $s->titre->titre, $this->baseUrl, $s->titreId, $s->id);
				}
			}
		}
		if ($body !== '') {
			$this->email($r, $body);
		}
	}

	/**
	 * Renvoie pour chaque adresse électronique une liste de corps de textes.
	 *
	 * @return array<string, string[]>
	 */
	public function getEmails(): array
	{
		return $this->emailTo;
	}

	public function setSince(string $since): void
	{
		$this->sinceTs = $this->toTimestamp($since);
	}

	private function email(\Ressource $r, string $body): void
	{
		$emailBody = "\n## Ressource {$r->nom}\n$body";
		$importEmails = array_filter(
			array_map(
				function ($x) {
					return trim($x->importEmail);
				},
				$r->getPartenairesSuivant()
			)
		);
		if (!$importEmails && $this->defaultTo) {
			$importEmails = [$this->defaultTo];
		}
		foreach ($importEmails as $e) {
			if (isset($this->emailTo[$e])) {
				$this->emailTo[$e][] = $emailBody;
			} else {
				$this->emailTo[$e] = [$emailBody];
			}
		}
	}

	/**
	 * @return Service[]
	 */
	private function findDeletedFromImport(int $ressourceId, string $collectionIds, int $since): array
	{
		$sql = "SELECT s.*
FROM
    Service s
    JOIN Titre t ON s.titreId = t.id
	" . ($collectionIds ? "JOIN Service_Collection sc ON sc.serviceId = s.id " : "") . "
WHERE
    s.statut = 'normal' AND s.ressourceId = :rid
	" . ($collectionIds ? "AND sc.collectionId IN ($collectionIds) " : "") . "
    AND s.hdateCreation < :since
    AND s.hdateImport IS NOT NULL AND s.hdateImport < :since
ORDER BY t.titre";
		return Service::model()->findAllBySql($sql, [':rid' => $ressourceId, ':since' => $since]);
	}

	/**
	 * @return Service[]
	 */
	private function findNeverImported(int $ressourceId, string $collectionIds): array
	{
		$sql = "SELECT s.*
FROM
    Service s
    JOIN Titre t ON s.titreId = t.id
    " . ($collectionIds ? "JOIN Service_Collection sc ON sc.serviceId = s.id " : "") . "
WHERE
    s.statut = 'normal' AND s.ressourceId = :rid
	" . ($collectionIds ? "AND sc.collectionId IN ($collectionIds) " : "") . "
	AND s.hdateImport IS NULL
ORDER BY t.titre";
		return Service::model()->findAllBySql($sql, [':rid' => $ressourceId]);
	}

	/**
	 * @return Service[]
	 */
	private function findNew(int $ressourceId, ?string $collectionIds, int $since): array
	{
		$sql = "SELECT s.* FROM Service s "
			. ($collectionIds ? "JOIN Service_Collection sc ON sc.serviceId = s.id " : "")
			. "WHERE s.statut = 'normal' AND s.ressourceId = :rid "
			. ($collectionIds ? "AND sc.collectionId IN ($collectionIds) " : "")
			. " AND s.hdateCreation > :since"
			. " GROUP BY s.id";
		return Service::model()->findAllBySql($sql, [':rid' => $ressourceId, ':since' => $since]);
	}

	/**
	 * Converts a string date to a timestamp at 00:00 (defaults to 0).
	 */
	private function toTimestamp(string $since): int
	{
		if ($since === '') {
			return 0;
		}
		if (!ctype_digit($since)) {
			$since = strtotime($since);
			if ($since === false) {
				throw new \Exception("Mauvais format de date (YYYY-MM-AA attendu), ou mauvais 'timestamp'.");
			}
			if ($this->verbose > 1) {
				echo "Accès supprimés depuis : " . date("Y-m-d H:i", $since) . "\n";
			}
		}
		return (int) $since;
	}
}
