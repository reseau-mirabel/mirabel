<?php

namespace processes\service;

use CDbDataReader;
use Issn;
use Yii;

class ExportServices
{
	public int $time;

	private CDbDataReader $services;

	/**
	 * Constructor.
	 *
	 * @param array $ids of revueId
	 * @param ServiceFilter $filter
	 */
	public function __construct($ids, ServiceFilter $filter)
	{
		$safeIds = $ids ? array_map('intval', $ids) : [];
		Yii::app()->db->pdoInstance->setAttribute(\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, false);
		ini_set("memory_limit", "256M");
		$this->services = $this->findServices($safeIds, $filter);
		$this->time = time();
	}

	/**
	 * Print the CSV lines for the matching services.
	 * @param resource $output
	 * @throws \Exception
	 * @return int octets écrits
	 */
	public function exportToCsv($output, bool $extraColumns): int
	{
		$separator = ';';
		$header = [
			'titre', 'ISSN', 'ISSN-E', 'ISSN-L', 'URL Mir@bel', 'id de la revue', "URL de l'accès", 'ressource', 'collection',
			"contenu", "type d'accès",
			'date barrière de début', 'début', 'début (vol)', 'début (numéro)',
			'date barrière de fin', 'fin', 'fin (vol)', 'fin (numéro)',
			'id', 'dernière modification dans Mir@bel',
		];
		if ($extraColumns) {
			array_push($header, 'lacunaire', 'sélection');
		}
		$bytes = (int) fputcsv($output, $header, $separator, '"', '\\');
		if ($bytes === 0) {
			throw new \Exception("Could not write CSV header.");
		}

		foreach ($this->services as $row) {
			$written = fputcsv($output, self::toCsvArray($row, $extraColumns), $separator, '"', '\\');
			if ($written === false) {
				throw new \Exception("Impossible d'écrire le CSV. Manque de mémoire sur le serveur ?");
			}
			$bytes += $written;
		}
		return $bytes;
	}

	/**
	 * Print the KBART II lines for the matching services.
	 *
	 * @param resource $output
	 * @param bool $customFields Add some Mirabel-only fields to the header and content.
	 * @throws \Exception
	 * @return int octets écrits
	 */
	public function exportToKbartV2($output, bool $customFields = true): int
	{
		$groupedServices = [];
		foreach ($this->services as $row) {
			$id = $row['revueId'] . ',' . $row['ressourceId'];
			if (isset($groupedServices[$id])) {
				$groupedServices[$id][] = $row;
			} else {
				$groupedServices[$id] = [$row];
			}
		}

		$header = [
			"publication_title", "print_identifier", "online_identifier",
			"date_first_issue_online", "num_first_vol_online", "num_first_issue_online",
			"date_last_issue_online", "num_last_vol_online", "num_last_issue_online",
			"title_url", "first_author", "title_id",
			"embargo_info", "coverage_depth", "notes", "publisher_name",
			"publication_type",
			"date_monograph_published_print", "date_monograph_published_online", // empty fields
			"monograph_volume", "monograph_edition", "first_editor", // empty fields
			"parent_publication_title_id", "preceding_publication_title_id", // empty fields
			"access_type",
		];
		if ($customFields) {
			array_push($header, "resource", "collections", "Mir_title_url", "Mir_title_id");
		}
		$bytes = (int) fputcsv($output, $header, "\t", '"', '\\');
		if ($bytes === 0) {
			throw new \Exception("Could not write CSV header.");
		}

		foreach ($groupedServices as $rowGroup) {
			foreach ($rowGroup as $row) {
				$kbartRow = self::toKbartv2Array($row, $customFields);
				$written = fputcsv($output, $kbartRow, "\t", '"', '\\');
				if ($written === false) {
					throw new \Exception("Impossible d'écrire le CSV. Manque de mémoire sur le serveur ?");
				}
				$bytes += $written;
			}
		}
		return $bytes;
	}

	/**
	 * Finds the records in Service matching the ids and the filter.
	 *
	 * @param array $ids of revueId (safe)
	 */
	protected function findServices($ids, ServiceFilter $filter): CDbDataReader
	{
		$cond = [];
		if ($filter->bacon === ServiceFilter::BACON_DOAJ) {
			$cond = [
				"s.acces = 'Libre'",
				"s.type = 'Intégral'",
				"s.lacunaire = 0",
				"s.selection = 0",
				"t.liensJson LIKE '%\"src\":\"DOAJ\"%'",
			];
		} elseif ($filter->bacon === ServiceFilter::BACON_EPISCIENCES) {
			$cond = [
				"s.type = 'Intégral'",
				"s.ressourceId = 931",
			];
		} elseif ($filter->bacon === ServiceFilter::BACON_GLOBAL) {
			$cond = [
				"s.type = 'Intégral'",
				"s.selection = 0",
			];
		} elseif ($filter->bacon === ServiceFilter::BACON_LIBRE) {
			$cond = [
				"s.acces = 'Libre'",
				"s.type = 'Intégral'",
				"s.lacunaire = 0",
				"s.selection = 0",
				// Cairn (3), Erudit (7), OpenEdition Journals (4), Persée (22)
				// "s.ressourceId NOT IN (3, 7, 4, 22)",
			];
		} elseif ($filter->bacon === ServiceFilter::BACON_REPERES) {
			$cond = [
				"s.acces = 'Libre'",
				"s.type = 'Intégral'",
				'Grappe_Titre gt' => "gt.titreId = s.titreId",
				"gt.grappeId = 2",
			];
		} else {
			if ($filter->acces) {
				$cond[] = "s.acces = '{$filter->acces}'";
			}
			if ($filter->type) {
				$quoter = function ($v) {
					return \Yii::app()->db->quoteValueWithType($v, \PDO::PARAM_STR);
				};
				$quotedTypes = array_map($quoter, $filter->type);
				$cond[] = "s.type IN (" . join(",", $quotedTypes) . ")";
			}
			if (!$filter->lacunaire) { // filter out by default
				$cond[] = "s.lacunaire = 0";
			}
			if (!$filter->selection) { // filter out by default
				$cond[] = "s.selection = 0";
			}
		}
		if ($filter->ressourceId) {
			$cond[] = "s.ressourceId = {$filter->ressourceId}";
		}
		if ($filter->hdateModif) {
			$timestamp = strtotime($filter->hdateModif);
			if ($timestamp) {
				$cond[] = "s.hdateModif > " . $timestamp;
			}
		}

		$joins = "";
		foreach ($cond as $k => $v) {
			if (!is_int($k)) {
				$joins .= "JOIN $k ON $v";
				unset($cond[$k]);
			}
		}

		$sql =
			<<<EOSQL
			SELECT
			  t.titre
			  , GROUP_CONCAT(DISTINCT i1.issn SEPARATOR '|') AS issn
			  , GROUP_CONCAT(DISTINCT i1.issnl SEPARATOR '|') AS issnl
			  , t.revueId
			  , s.id, s.titreId, s.ressourceId, s.type, s.acces, s.url, s.lacunaire, s.selection
			  , s.volDebut, s.noDebut, s.numeroDebut, s.volFin, s.noFin, s.numeroFin
			  , s.dateBarrDebut, dateBarrFin, s.dateBarrInfo, s.embargoInfo, s.notes
			  , s.hdateModif
			  , i.description
			  , GROUP_CONCAT(DISTINCT i2.issn SEPARATOR '|') AS issne
			  , idf.idInterne
			  , GROUP_CONCAT(DISTINCT CONCAT(e.prefixe, e.nom)) AS editeur
			  , CONCAT(r.prefixe, r.nom) AS ressource
			  , GROUP_CONCAT(DISTINCT c.nom SEPARATOR ' | ') AS collections
			  , r.url AS ressourceUrl
			FROM Titre t
			  JOIN Service s ON t.id=s.titreId
			  JOIN Ressource r ON r.id=s.ressourceId
			  LEFT JOIN Issn i1 ON t.id = i1.titreId AND i1.support = :sp AND i1.statut = :valide1
			  LEFT JOIN Issn i2 ON t.id = i2.titreId AND i2.support = :se AND i2.statut = :valide2
			  LEFT JOIN Identification idf ON (t.id=idf.titreId AND s.ressourceId=idf.ressourceId)
			  LEFT JOIN Intervention i ON (i.titreId=t.id AND i.hdateVal=s.hdateModif)
			  LEFT JOIN Titre_Editeur te ON t.id=te.titreId AND te.ancien = 0
			  LEFT JOIN Editeur e ON te.editeurId=e.id
			  LEFT JOIN Service_Collection sc ON sc.serviceId=s.id
			  LEFT JOIN Collection c ON sc.collectionId=c.id
			  $joins
			WHERE
			  s.statut = 'normal'
			EOSQL
			. ($ids ? "AND t.revueId IN (" . join(',', $ids) . ") " : '')
			. ($cond ? " AND " . join(" AND ", $cond) : '')
			. " GROUP BY s.id"
			. " ORDER BY t.titre, r.nom, s.dateBarrDebut";
		return Yii::app()->db->createCommand($sql)->query([
			':sp' => Issn::SUPPORT_PAPIER,
			':se' => Issn::SUPPORT_ELECTRONIQUE,
			':valide1' => Issn::STATUT_VALIDE,
			':valide2' => Issn::STATUT_VALIDE,
		]);
	}

	private static function toCsvArray(array $row, bool $extraColumns): array
	{
		$res = [
			$row['titre'],
			$row['issn'],
			$row['issne'],
			$row['issnl'],
			PHP_SAPI === 'cli' ? '' : Yii::app()->createAbsoluteUrl('revue/view', ['id' => $row['revueId']]),
			$row['revueId'],
			$row['url'],
			$row['ressource'],
			$row['collections'],
			\Service::$enumType[$row['type']],
			$row['acces'],
			$row['dateBarrDebut'],
			$row['numeroDebut'],
			$row['volDebut'],
			$row['noDebut'],
			$row['dateBarrFin'],
			$row['numeroFin'],
			$row['volFin'],
			$row['noFin'],
			$row['id'],
			$row['hdateModif'] ? date('Y-m-d', $row['hdateModif']) : '',
		];
		if ($extraColumns) {
			$res['lacunaire'] = (int) $row['lacunaire'];
			$res['selection'] = (int) $row['selection'];
		}
		return $res;
	}

	/**
	 * Returns a list of KBART II values for a given row.
	 *
	 * @param array $row
	 * @param bool $customFields (opt, true) Add some Mirabel-only fields (not inBacon mode).
	 * @return array
	 */
	private static function toKbartv2Array(array $row, bool $customFields = true): array
	{
		[$coverage, $coverageNotes] = self::buildCoverage($row);
		$revueUrl = Yii::app()->params->itemAt('baseUrl') . '/revue/' . $row['revueId'];
		$kb = [
			$row['titre'],
			$row['issn'],
			$row['issne'],
			$row['dateBarrDebut'],
			$row['volDebut'],
			$row['noDebut'],
			$row['dateBarrFin'],
			$row['volFin'],
			$row['noFin'],
			$row['url'] ?: $row['ressourceUrl'],
			'',
			$customFields ? $row['idInterne'] : $revueUrl,
			$row['embargoInfo'],
			$coverage,
			trim($coverageNotes),
			$row['editeur'],
			'serial',
			// empty fields
			'', '', '', '', '', '', '',
			($row['acces'] === 'Libre' ? 'F' : 'P'),
		];
		if ($customFields) {
			array_push(
				$kb,
				$row['ressource'],
				$row['collections'],
				$revueUrl,
				$row['titreId']
			);
		}
		return $kb;
	}

	private static function buildCoverage(array $row): array
	{
		$coverage = '';
		$coverageNotes = '';
		switch ($row['type']) {
			case 'Intégral':
				if ($row['selection']) {
					$coverage = 'selected articles';
				} else {
					$coverage = 'fulltext';
				}
				break;
			case 'Sommaire':
			case 'Résumé':
				$coverage = 'abstracts';
				$coverageNotes = $row['type'] . ' ';
				break;
			default:
				$coverageNotes = $row['type'] . ' ';
		}
		if ($row['lacunaire']) {
			$coverageNotes .= '(lacunaire)';
		}
		if ($row['selection']) {
			$coverageNotes .= "(sélection d'articles)";
		}
		if ($row['notes']) {
			$coverageNotes = $row['notes'];
		}
		if ($row['dateBarrInfo']) {
			$coverageNotes = ($coverageNotes ? "$coverageNotes|{$row['dateBarrInfo']}" : $row['dateBarrInfo']);
		}
		return [$coverage, $coverageNotes];
	}
}
