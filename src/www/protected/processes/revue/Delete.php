<?php

namespace processes\revue;

use Editeur;
use Intervention;
use InterventionDetail;
use Revue;
use Titre;
use Yii;
use components\FeedGenerator;

class Delete
{
	public Revue $revue;

	/**
	 * @var Editeur[]
	 */
	private array $editeurs = [];

	private array $redirection = [];

	/**
	 * @var Titre[]
	 */
	private array $titres;

	public function __construct(Revue $r)
	{
		$this->revue = $r;
		$this->titres = $this->revue->getTitres(false);
	}

	public function getRedirection(): array
	{
		return $this->redirection;
	}

	public function preValidate(): bool
	{
		return !$this->hasPendingIntervention() && !$this->hasManyTitles() && !$this->hasPolitique();
	}

	public function run(): bool
	{
		$i = $this->createIntervention();
		if ($i->validate()) {
			// Before applying, find which publishers will loose a title.
			$this->editeurs = Editeur::model()->findAllBySql(
				"SELECT e.* FROM Editeur e JOIN Titre_Editeur te ON te.editeurId = e.id JOIN Titre t ON t.id = te.titreID WHERE t.revueId = :rid",
				[':rid' => $this->revue->id]
			);
			// Update the RSS feed.
			FeedGenerator::saveFeedsToFile('Revue', (int) $this->revue->id);
		} else {
			return false;
		}
		if ($i->accept(false)) {
			$i->save(false);
			$this->checkForOrphanPublishers();
			Yii::app()->user->setFlash('success', "Revue « {$this->getName()} » supprimée.");
			$this->redirection['url'] = ['/revue/index'];
			return true;
		}

		FeedGenerator::removeFeedsFile('Revue', $this->revue->id);
		$errors = (new \processes\titres\Delete($this->titres[0]))->getConstraints();
		if (!$errors && !empty($i->errors['contenuJson'])) {
			$errors = $i->errors['contenuJson'];
		}
		$this->redirection = [
			'status' => 'info',
			'message' => "Suppression de « {$this->getName()} » impossible : <ul><li>" . join("</li><li>", $errors) . "</li></ul>",
			'url' => ['view', 'id' => $this->revue->id],
		];
		return false;
	}

	private function checkForOrphanPublishers(): void
	{
		$warning = "";
		foreach ($this->editeurs as $e) {
			if ($e->countRevues(false) === 0) {
				$warning .= "<br>L'éditeur « {$e->getSelfLink()} » n'a plus aucune revue. Vous devez peut-être le supprimer.";
			}
		}
		if ($warning) {
			$this->redirection['status'] = 'danger';
			$this->redirection['message'] = "<strong>Éditeur orphelin !</strong> $warning";
		}
	}

	private function createIntervention(): Intervention
	{
		$i = new Intervention();
		$i->statut = 'attente';
		$i->action = 'revue-D';
		$i->utilisateurIdProp = Yii::app()->user->id;
		$i->import = 0;
		$i->ip = $_SERVER['REMOTE_ADDR'];
		$i->contenuJson = new InterventionDetail();
		if ($this->titres) {
			$fullTitle = $this->getName();
			$i->contenuJson->delete($this->titres[0], "Suppression de la revue " . $fullTitle);
			$i->description = "Suppression de la revue " . $fullTitle;
		}
		$i->contenuJson->delete($this->revue);
		return $i;
	}

	private function getName(): string
	{
		if ($this->titres) {
			return $this->titres[0]->titre;
		}
		return 'SansTitre';
	}

	private function hasManyTitles(): bool
	{
		if (count($this->titres) < 2) {
			return false;
		}
		$this->redirection = [
			'status' => 'error',
			'message' => "On ne peut supprimer une revue ayant plusieurs titres.",
			'url' => ['view', 'id' => $this->revue->id],
		];
		return true;
	}

	private function hasPendingIntervention(): bool
	{
		$hasPending = (bool) Yii::app()->db
			->createCommand("SELECT 1 FROM Intervention WHERE revueId = :rid AND statut = 'attente' LIMIT 1")
			->queryScalar([':rid' => $this->revue->id]);
		if ($hasPending) {
			$this->redirection = [
				'status' => 'error',
				'message' => "Suppression impossible car au moins une intervention est en attente sur cette revue.",
				'url' => ['view', 'id' => $this->revue->id],
			];
			return true;
		}
		return false;
	}

	private function hasPolitique(): bool
	{
		$exists = (bool) Yii::app()->db
			->createCommand("SELECT 1 FROM `Politique_Titre` pt JOIN Titre t ON pt.titreId = t.id WHERE t.revueId = :id LIMIT 1")
			->queryScalar([':id' => $this->revue->id]);
		if ($exists) {
			$this->redirection = [
				'status' => 'danger',
				'message' => "<strong>Un des titres de cette revue a une politique</strong>, donc la suppression est impossible.",
				'url' => ['view', 'id' => $this->revue->id],
			];
		}
		return $exists;
	}
}
