<?php

namespace processes\revue;

use AbonnementSearch;
use CHtml;
use Norm;
use Partenaire;
use processes\titres\TrierTitre;
use Revue;
use Titre;

class View
{
	public bool $directUpdate = false;

	public bool $forceRefresh = false;

	public Revue $revue;

	private ?AbonnementSearch $abonnement;

	private string $fullTitle;

	private ?Partenaire $partenaire;

	private bool $publicMode;

	private array $redirection = [];

	/**
	 * @var Titre[]
	 */
	private array $titres;

	public function __construct(Revue $r, ?int $partenaireId, bool $public)
	{
		$this->revue = $r;
		$this->publicMode = $public;
		if ($partenaireId > 0) {
			$this->partenaire = Partenaire::model()->findByPk($partenaireId);
		} else {
			$this->partenaire = null;
		}

		$this->titres = $r->getTitresOrderedByObsolete();
		$this->fullTitle = $this->titres[0]->getFullTitle();
		if ($this->partenaire) {
			$this->abonnement = new AbonnementSearch('search');
			$this->abonnement->partenaireId = (int) $this->partenaire->id;
		} else {
			$this->abonnement = null;
		}
	}

	public function getAbonnement(): ?AbonnementSearch
	{
		return $this->abonnement;
	}

	public function getActiveTitle(): Titre
	{
		assert(count($this->titres) > 0);
		return reset($this->titres);
	}

	public function getCanonicalRoute(): array
	{
		$nomEnc = Norm::urlParam($this->fullTitle);
		$url = ['/revue/view', 'id' => $this->revue->id, 'nom' => $nomEnc];
		if ($this->publicMode) {
			$url['public'] = 1;
		}
		return $url;
	}

	public function getRedirection(): array
	{
		return $this->redirection;
	}

	public function getSidebarMenu(bool $isMonitoring, bool $canVerify): array
	{
		$revueId = (int) $this->revue->id;

		$reorderLabel = "Trier les titres";
		if (TrierTitre::isRevueNeedReordering($this->revue)) {
			$reorderLabel = '<span title="Tri nécessaire" class="glyphicon glyphicon-warning-sign"></span> ' . $reorderLabel;
		}

		$menu = [
			['label' => 'Liste des revues', 'url' => ['index']],
			['label' => 'Sur cette revue', 'itemOptions' => ['class' => 'nav-header']],
			['label' => 'Vérifier les liens' . (\Wikidata::isRevuePresent($revueId) ? ' et données Wikidata' : ''),
				'url' => ['checkLinks', 'id' => $revueId],
			],
			['label' => 'Ajouter un titre à cette revue', 'url' => ['/titre/create-by-issn', 'revueId' => $revueId]],
			['label' => 'Ajouter un accès en ligne', 'url' => ['/service/create', 'revueId' => $revueId]],
			['label' => 'Définir la thématique', 'url' => ['categories', 'id' => $revueId]], // indexation-catégorisation
			[
				'label' => $reorderLabel,
				'encodeLabel' => false,
				'url' => ['trier-titre', 'id' => $revueId],
				'visible' => \Yii::app()->user->access()->toTitre()->createDirect($this->revue->id) && (count($this->titres) > 1),
			],
			[
				'label' => "Supprimer…",
				'url' => ['/revue/delete', 'id' => $revueId],
				'visible' => $this->directUpdate,
			],
			['label' => 'Autre revue', 'itemOptions' => ['class' => 'nav-header']],
			['label' => 'Créer une revue', 'url' => ['/titre/create-by-issn', 'revueId' => 0]],
			['label' => ''],
			['label' => 'Suivi', 'itemOptions' => ['class' => 'nav-header']],
			['label' => "Voir l'historique", 'url' => ['/intervention/admin', 'q[revueId]' => $revueId]],
		];
		if (!$isMonitoring) {
			$label = CHtml::form(['/partenaire/suivi', 'id' => $this->partenaire->id, 'target' => 'Revue'])
				. CHtml::hiddenField('Suivi[cibleId][]', (string) $revueId)
				. CHtml::hiddenField('returnUrl', \Yii::app()->controller->createUrl('view', ['id' => $revueId]), ['id' => 'dqoshfo'])
				. (\Yii::app()->user->access()->toPartenaire()->suivi($this->partenaire) ? CHtml::htmlButton("Suivre cette revue", ['type' => 'submit', 'class' => "btn btn-primary btn-small"]) : "")
				. CHtml::endForm();
			$menu[] = ['encodeLabel' => false, 'label' => $label];
		}
		if ($canVerify && $this->directUpdate) {
			array_push(
				$menu,
				[
					'label' => 'Vérification',
					'itemOptions' => ['class' => 'nav-header'],
				],
				[
					'encodeLabel' => false,
					'label' => \components\HtmlHelper::postButton(
						"Indiquer que la revue a été vérifiée",
						['/revue/verify', 'id' => $revueId],
						[],
						['class' => 'btn btn-primary btn-small', 'style' => "padding: 0;"]
					),
				]
			);
		}
		return $menu;
	}

	/**
	 * @return Titre[]
	 */
	public function getTitles(): array
	{
		return $this->titres;
	}

	public function isMonoTitle(): bool
	{
		return count($this->titres) === 1;
	}

	public function isPublic(): bool
	{
		return $this->publicMode;
	}

	/**
	 * Check that the name is the same as the encoded Revue title expected in the URL.
	 */
	public function validateName(string $name): bool
	{
		$nomEnc = Norm::urlParam($this->fullTitle);
		if (!$nomEnc || $name === $nomEnc) {
			return true;
		}
		$url = $this->getCanonicalRoute();
		if (!empty($_GET['s'])) {
			$url['s'] = $_GET['s'];
		}
		$this->redirection = [
			'url' => $url,
		];
		return false;
	}
}
