<?php

namespace processes\revue;

use CHtml;
use Issn;
use Partenaire;
use Titre;
use Yii;

/**
 * Each attribute contains the HTML to be displayed.
 */
class RevueTitreTableContent
{
	public int $titreId;

	public string $titre;

	public string $issn;

	public string $issne;

	public ?string $local;

	public string $dates;

	public string $editeurs;

	public string $actions;

	/**
	 * @var bool Global state, updated when parsing input.
	 */
	private static bool $hasNonEmptyEtatcollection = false;

	public function __construct(
		Titre $titre,
		Titre $titreActif,
		array $revueIssnGroups,
		bool $isGuest,
		bool $direct,
		?Partenaire $institute
	) {
		$this->titreId = (int) $titre->id;
		$this->titre = self::generateTitreColumn($titre);
		$this->issn = self::generateIssnColumn($titre, $revueIssnGroups, $isGuest);
		$this->issne = self::generateIssneColumn($titre, $revueIssnGroups);
		$this->local = self::generateLocalColumn($titre, $revueIssnGroups, $institute, $isGuest);
		$this->dates = self::generateDatesColumn($titre, $revueIssnGroups);
		$this->editeurs = self::generateEditeursColumn($titre);
		$this->actions = self::generateActionsColumn($titre, $titreActif, $direct, $isGuest);
	}

	public static function resetEtatCollection(): void
	{
		self::$hasNonEmptyEtatcollection = false;
	}

	public static function hasNonEmptyEtatcollection(): bool
	{
		return self::$hasNonEmptyEtatcollection;
	}

	private static function generateTitreColumn(Titre $titreObject): string
	{
		$titre = '<span itemprop="name">' . CHtml::encode($titreObject->getPrefixedTitle()) . '</span>';
		if ($titreObject->sigle) {
			$titre .= '<span class="titre-sigle">' . CHtml::encode($titreObject->sigle) . '</span>';
		}
		return $titre;
	}

	private static function generateIssnColumn(Titre $titre, array $revueIssnGroups, bool $isGuest): string
	{
		$issn = "";
		if ($revueIssnGroups['issnp']) {
			if (isset($revueIssnGroups['issnp'][$titre->id])) {
				foreach ($revueIssnGroups['issnp'][$titre->id] as $issnObject) {
					/** @var Issn $issnObject */
					$printableIssn = $issnObject->issn ? $issnObject->getHtml(true) : "";
					$issn .= CHtml::tag(
						'div',
						['style' => 'white-space: nowrap;'],
						($issnObject->sudocPpn ? $issnObject->getSudocLink() . " " : "")
						. $printableIssn
						. (
							$isGuest || $issnObject->statut != Issn::STATUT_VALIDE || $issnObject->support !== Issn::SUPPORT_INCONNU ?
								''
								: CHtml::tag('i', ['class' => 'icon-warning-sign', 'title' => "Support indéterminé : modifier l'ISSN pour préciser le support papier ou électronique"], '')
						)
					);
				}
			}
		}
		return $issn;
	}

	private static function generateIssneColumn(Titre $titre, array $revueIssnGroups): string
	{
		$issne = "";
		if ($revueIssnGroups['issne']) {
			if (isset($revueIssnGroups['issne'][$titre->id])) {
				foreach ($revueIssnGroups['issne'][$titre->id] as $issn) {
					/** @var Issn $issn */
					$printableIssn = $issn->issn ? $issn->getHtml(true) : "";
					$issne .= CHtml::tag(
						'div',
						['style' => 'white-space: nowrap;'],
						($issn->sudocPpn ? $issn->getSudocLink() . " " : "") . $printableIssn
					);
				}
			}
		}
		return $issne;
	}

	private static function generateLocalColumn(Titre $titre, array $revueIssnGroups, ?Partenaire $institute, bool $isGuest): ?string
	{
		if ($institute == null || (!$titre->belongsTo($institute->id) && !self::isEtatcollectionToDisplayToUser($titre, $institute, $isGuest))) {
			return null;
		}

		self::$hasNonEmptyEtatcollection = true;
		$local = "";
		$possessionMsg = "Disponible dans votre bibliothèque";
		$url = $institute->buildPossessionUrl($titre);
		$divTitle = "Lien direct sur le catalogue de la bibliothèque de {$institute->nom}";
		if ($url) {
			$local .= " " . CHtml::link($possessionMsg, $url, ['title' => $divTitle]);
		} else {
			$local .= " <div>$possessionMsg</div>";
		}

		if ($titre->belongsTo($institute->id) && $isGuest && !$institute->rcrPublic) {
			return $local;
		}

		$etatscollectionHtml = "<div>";
		if (isset($revueIssnGroups['issne'][$titre->id])) {
			foreach ($revueIssnGroups['issne'][$titre->id] as $issn) {
				/** @var Issn $issn */
				if (empty($issn->sudocPpn) || $issn->sudocNoHolding) {
					continue;
				}
				$etatscollection = $issn->getEtatcollection($institute);
				foreach ($etatscollection as $etat) {
					$safeEtat = CHtml::encode($etat);
					$etatscollectionHtml .= <<<EOF
						<span title="État de collection (version en ligne)[source : Sudoc]" class="glyphicon glyphicon-globe" style="color:#eb6907"></span>
						{$safeEtat}
						<br/>
						EOF;
				}
			}
		}
		if (isset($revueIssnGroups['issnp'][$titre->id])) {
			foreach ($revueIssnGroups['issnp'][$titre->id] as $issn) {
				/** @var Issn $issn */
				if (empty($issn->sudocPpn) || $issn->sudocNoHolding) {
					continue;
				}
				$etatscollection = $issn->getEtatcollection($institute);
				foreach ($etatscollection as $etat) {
					$safeEtat = CHtml::encode($etat);
					$etatscollectionHtml .= <<<EOF
							<span title="État de collection (version imprimée)[source : Sudoc]" class="glyphicon glyphicon-book" style="color:#eb6907"></span>
							{$safeEtat}
							<br/>
							EOF;
				}
			}
		}
		$etatscollectionHtml .= "</div>";
		if (strlen($etatscollectionHtml) > 1000) {
			$local .= <<<EOF
				<details>
					<summary style="color:#0088cc">Afficher l'état de collection</summary>
					{$etatscollectionHtml}
				</details>
				EOF;
		} elseif ($etatscollectionHtml != "<div></div>") {
			$local .= $etatscollectionHtml;
		}
		return $local;
	}

	private static function generateDatesColumn(Titre $titre, array $revueIssnGroups): string
	{
		if (isset($revueIssnGroups['dates'][$titre->id])) {
			$dates = CHtml::tag('abbr', ['title' => $revueIssnGroups['dates'][$titre->id]], $titre->getPeriode());
		} else {
			$dates = $titre->getPeriode();
		}
		return $dates;
	}

	private static function generateEditeursColumn(Titre $titre): string
	{
		$links = [];
		$editeursActifs = \Editeur::model()->findAllBySql(
			"SELECT e.* FROM Editeur e JOIN Titre_Editeur te ON te.editeurId = e.id AND IFNULL(te.ancien, 0) = 0 WHERE te.titreId = :tid",
			[':tid' => $titre->id]
		);
		foreach ($editeursActifs as $editeur) {
			$links[] = $editeur->getSelfLink(\Editeur::NAME_SHORT);
		}
		$editeurs = join(', ', $links);
		if (($editeursActifs || $titre->editeurs) && Yii::app()->user->access()->toTitre()->relationEditeurs($titre)) {
			$editeurs .= '  ' . CHtml::link(
				'<span class="glyphicon glyphicon-retweet" title="Modifier les relations du titre à ses éditeurs"></span>',
				['/titre/editeurs', 'id' => $titre->id]
			);
		}
		return $editeurs;
	}

	private static function generateActionsColumn(Titre $titre, Titre $activeTitle, bool $direct, bool $isGuest): string
	{
		$imgUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets'))
			. '/gridview';
		if ($titre->id == $activeTitle->id) {
			$actions = "";
		} else {
			$actions = CHtml::tag(
				'span',
				[
					'class' => 'titre-details-popover',
					'data-title' => $titre->titre,
					'data-titreid' => $titre->id,
					'title' => "Vue détaillée de cet ancien titre",
				],
				'<span class="glyphicon glyphicon-zoom-in" style="font-size:80%" title="Détails"></span> '
			);
		}
		if (!$isGuest) {
			$actions .= CHtml::link(
				CHtml::image($imgUrl . '/view.png', 'Détails'),
				['/titre/view', 'id' => $titre->id],
				['title' => 'Détails de ce titre']
			);
		}
		$actions .= CHtml::link(
			CHtml::image($imgUrl . '/update.png', 'Modifier'),
			['/titre/update', 'id' => $titre->id],
			['title' => $direct ? 'Modifier' : 'Proposer une modification']
		);
		return $actions;
	}

	/*
	 * Vrai s'il y a des etatcollection associés au titre et qu'ils sont accessibles pour l'utilisateur
	 */
	private static function isEtatcollectionToDisplayToUser(Titre $titre, Partenaire $institute, bool $isGuest): bool
	{
		$canAccesRcrCollection = $institute->rcrHorsPossession
			&& !empty(json_decode($institute->rcr))
			&& (!$isGuest || $institute->rcrPublic);
		if (!$canAccesRcrCollection) {
			return false;
		}
		foreach ($titre->issns as $issn) {
			if (!empty($issn->getEtatcollection($institute))) {
				return true;
			}
		}
		return false;
	}
}
