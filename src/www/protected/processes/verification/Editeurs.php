<?php

namespace processes\verification;

use Editeur;
use Titre;
use components\HtmlTable;
use components\HtmlHelper;
use CHtml;

class Editeurs
{
	public int $relationIncomplete = 0;

	public int $sansIdref = 0;

	/**
	 * @var Editeur[]
	 */
	public array $sansPays = [];

	public int $sansSherpa = 0;

	/**
	 * @var Titre[]
	 */
	public array $titresRelationIncomplete = [];

	/**@var Editeur[] */
	public array $avecDateDansNom = [];

	/** @var string[][] */
	public array $mortsMaisActuels;

	/** @var string[][] */
	public array $paysIncoherents;

	public function __construct()
	{
		$this->relationIncomplete = (int) \Yii::app()->db
			->createCommand(
				"SELECT count(DISTINCT te.editeurId) FROM Titre_Editeur te JOIN Editeur e ON te.editeurId = e.id WHERE e.paysId = 62 AND te.commercial = 0 AND te.intellectuel = 0"
			)->queryScalar();
		$this->titresRelationIncomplete = Titre::model()->findAllBySql(
			<<<EOSQL
			SELECT DISTINCT t.*
			FROM Titre t
				JOIN Titre_Editeur te ON t.id = te.titreId AND te.commercial = 0 AND te.intellectuel = 0
				JOIN Editeur e ON te.editeurId = e.id AND e.paysId = 62
			WHERE t.obsoletePar IS NULL
			ORDER BY t.titre
			LIMIT 500
			EOSQL
		);

		$this->sansPays = Editeur::model()->findAllBySql(
			"SELECT * FROM Editeur WHERE paysId IS NULL ORDER BY nom"
		);
		$this->sansIdref = (int) \Yii::app()->db
			->createCommand(
				"SELECT count(*) FROM Editeur WHERE idref IS NULL AND paysId = 62" // France
			)->queryScalar();
		$this->sansSherpa = (int) \Yii::app()->db
			->createCommand(
				"SELECT count(*) FROM Editeur WHERE sherpa IS NULL AND paysId = 62" // France
			)->queryScalar();

		// Dates de début et fin
		$this->avecDateDansNom = Editeur::model()->findAllBySql(<<<'EOSQL'
			SELECT e.* FROM Editeur e WHERE e.nom REGEXP '[[:<:]][12][0-9]{3}\\)$| \\([[:<:]][12][0-9]{3} ' ORDER BY e.nom
			EOSQL
		);

		$this->mortsMaisActuels = array_map(
			fn ($x) => [
				"ID" => $x['id'],
				"Suivi" => $x['suivi'] ? 'oui' : '',
				"Pays" => $x['pays'] ?? '',
				"Titres" => $x['titres'],
				"Éditeur" => Editeur::model()->populateRecord($x, false)->getSelfLink(),
			],
			self::getEditeursMortsMaisActuels()
		);

		$this->paysIncoherents = array_map(
			fn ($x) => [
				"ISSN-P" => $x['issnp'] ?? "",
				"ISSN-E" => $x['issne'] ?? "",
				"TitreId" => CHtml::link($x['titreId'], ['/titre/view', 'id' => $x['titreId']]),
				"RevueId" => $x['revueId'],
				"Revue" => CHtml::link(CHtml::encode($x['titre']), ['/revue/view', 'id' => $x['revueId']]),
				"Pays ISSN" => $x['paysI'],
				"Pays éditeur" => $x['paysE'],
			],
			self::getDiscordingCountries()
		);
	}

	public static function displayExporTable(string $tableName, array $verifData): string
	{
		if (!$verifData) {
			return '<p>Aucun.</p>';
		}
		$table = HtmlTable::build($verifData)
			->enableNumberedColumn()
			->addClass('exportable export-href')
			->setHtmlAttributes(['data-exportable-filename' => HtmlHelper::exportableName($tableName)]);
		return $table->toHtml();
	}

	/**
	 * Éditeur mort (avec une date de fin) MAIS *actuel* (pas ancien) de titres vivants (sans date de fin).
	 */
	private static function getEditeursMortsMaisActuels(): array
	{
		return \Yii::app()->db->createCommand(
			<<<EOSQL
			WITH NumTitres AS (
				SELECT te.editeurId, COUNT(DISTINCT te.titreId) AS titres
				FROM Titre_Editeur te
					JOIN Titre t ON t.id = te.titreId
				WHERE te.ancien = 0 AND t.obsoletePar IS NULL AND t.dateFin = ''
				GROUP BY te.editeurId
			)
			SELECT e.*, n.titres, p.nom AS pays, count(s.partenaireId) AS suivi
			FROM Editeur e
				JOIN Titre_Editeur te ON te.editeurId = e.id AND te.ancien = 0
				JOIN Titre t ON t.id = te.titreId AND t.dateFin = ''
				JOIN NumTitres n ON n.editeurId = e.id
				LEFT JOIN Pays p ON e.paysId = p.id
				LEFT JOIN Suivi s ON s.cible = 'Editeur' AND s.cibleId = e.id
			WHERE e.dateFin <> ''
			GROUP BY e.id
			ORDER BY e.nom
			EOSQL
		)
			->queryAll(true);
	}

	/**
	 * Incohérences entre pays d'ISSN (référence) et pays de l'éditeur
	 */
	private static function getDiscordingCountries(): array
	{
		return \Yii::app()->db->createCommand(
			<<<EOSQL
			WITH Ipe AS (
				SELECT
					t.id AS titreId,
					ip.issn AS issnp,
					ip.pays AS paysp,
					ie.issn AS issne,
					ie.pays AS payse
				FROM Titre t
					LEFT JOIN Issn ip ON ip.titreId = t.id AND ip.issn IS NOT NULL AND ip.statut = :valide AND ip.support = 'papier'
					LEFT JOIN Issn ie ON ie.titreId = t.id AND ie.issn IS NOT NULL AND ie.statut = :valide AND ie.support = 'electronique'
			),
			PaysParEditeur AS (
				SELECT
					te.titreId,
					GROUP_CONCAT(DISTINCT p.code2 ORDER BY p.code2) AS pays
				FROM Titre_Editeur te
					JOIN Editeur e ON te.editeurId = e.id
					JOIN Pays p ON p.id = e.paysId
				GROUP BY te.titreId
			)
			SELECT
				t.id AS titreId, t.revueId,
				IF(LENGTH(t.titre) > 79, CONCAT(LEFT(t.titre, 80), '…'), t.titre) AS titre,
				issnp, issne,
				IF(i.paysp = i.payse OR i.paysp IS NULL,
					payse,
					IF(i.payse IS NULL,
						i.paysp,
						CONCAT(
							IF(i.paysp = '', '?', IFNULL(paysp, '')),
							',',
							IF(i.payse = '', '?', IFNULL(payse, ''))
					))) AS paysI,
				e.pays AS paysE
			FROM Titre t
				JOIN Ipe i ON i.titreId = t.id
				LEFT JOIN PaysParEditeur e ON e.titreId = t.id
			WHERE
				(i.paysp IS NOT NULL AND LOCATE(i.paysp, e.pays) = 0)
				OR (i.payse IS NOT NULL AND LOCATE(i.payse, e.pays) = 0)
			ORDER BY t.titre
			EOSQL
		)->queryAll(true, [':valide' => \Issn::STATUT_VALIDE]);
	}
}
