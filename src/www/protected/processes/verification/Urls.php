<?php

namespace processes\verification;

use CSqlDataProvider;
use DateTime;
use processes\urlverif\UrlMassCheck;
use VerifUrlForm;
use Yii;

class Urls
{
	public array $dates = [
		'dateMajMin' => '',
		'dateMajMax' => '',
	];

	public VerifUrlForm $input;

	public CSqlDataProvider $provider;

	private string $nameU;

	private \CDbCommand $searchCmd;

	/**
	 * @var string[] ID => nomPartenaire
	 */
	private array $suivi;

	public function __construct(string $name)
	{
		$this->nameU = ucfirst($name);
		$this->input = new VerifUrlForm();
		$this->provider = new CSqlDataProvider('', ['pagination' => false]);

		if ($name === 'cms') {
			$this->searchCmd = Yii::app()->db->createCommand()
				->select('v.*')
				->from('VerifUrl v')
				->join("Cms c", "c.id = v.sourceId");
			$this->suivi = [];
		} elseif ($name === 'editeur') {
			$this->searchCmd = Yii::app()->db->createCommand()
				->select('v.*, e.nom, e.hdateVerif')
				->from('VerifUrl v')
				->join("Editeur e", "e.id = v.sourceId");
			$this->suivi = [];
		} elseif ($name === 'revue') {
			$this->searchCmd = Yii::app()->db->createCommand()
				->select('v.*, t.titre, r.hdateVerif')
				->from('VerifUrl v')
				->join("Titre t", "t.revueId = v.sourceId")
				->join("Revue r", "t.revueId = r.id")
				->andWhere("t.obsoletePar IS NULL");
			$this->suivi = Yii::app()->db->pdoInstance
				->query("SELECT s.cibleId, IF(p.sigle='',p.nom,p.sigle) FROM Suivi s JOIN Partenaire p ON p.id = s.partenaireId WHERE s.cible = 'Revue'")
				->fetchAll(\PDO::FETCH_KEY_PAIR);
		} elseif ($name === 'ressource') {
			$this->searchCmd = Yii::app()->db->createCommand()
				->select('v.*, r.nom AS ressource, r.hdateVerif')
				->from('VerifUrl v')
				->join("Ressource r", "r.id = v.sourceId");
			$this->suivi = Yii::app()->db->pdoInstance
				->query("SELECT s.cibleId, IF(p.sigle='',p.nom,p.sigle) FROM Suivi s JOIN Partenaire p ON p.id = s.partenaireId WHERE s.cible = 'Ressource'")
				->fetchAll(\PDO::FETCH_KEY_PAIR);
		} else {
			throw new \Exception("Invalid parameter 'name'");
		}
		$this->searchCmd->andWhere("v.success = 0 AND v.source = " . UrlMassCheck::SOURCES[$this->nameU]);
		$this->searchCmd->order("v.id ASC");
	}

	public function load(array $data): void
	{
		$this->input->setAttributes($data);
		$this->provider->sql = $this->input->addSearchCriteria($this->searchCmd, $this->nameU);

		if ($this->provider->getItemCount() > 0) {
			$source = UrlMassCheck::SOURCES[$this->nameU];
			$this->dates['dateMajMin'] = DateTime::createFromFormat(
				"!Y-m-d H:i:s",
				Yii::app()->db->createCommand("SELECT hdate FROM VerifUrl WHERE source = $source ORDER BY hdate ASC LIMIT 1")->queryScalar()
			);
			$this->dates['dateMajMax'] = DateTime::createFromFormat(
				"!Y-m-d H:i:s",
				Yii::app()->db->createCommand("SELECT hdate FROM VerifUrl WHERE source = $source ORDER BY hdate DESC LIMIT 1")->queryScalar()
			);
		}
	}

	public function getSearchCmd(): \CDbCommand
	{
		return $this->searchCmd;
	}

	public function getSuivi(int $id): string
	{
		return $this->suivi[(string) $id] ?? '-';
	}
}
