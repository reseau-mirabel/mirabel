<?php

namespace processes\verification;

use Collection;
use Ressource;
use Yii;

class General
{
	public array $collectionsSansAcces;

	/**
	 * @var Collection[]
	 */
	public array $collectionsTemporaires;

	public array $doajInactif;

	public array $doublonsIdentification;

	/**
	 * @var Ressource[]
	 */
	public array $ressources;

	public array $revuesSansAccesLibre;

	public function __construct()
	{
		$this->ressources = Ressource::model()->findAllBySql(
			"SELECT r.* FROM Ressource r LEFT JOIN Service s ON s.ressourceId = r.id "
			. "WHERE s.id IS NULL ORDER BY r.nom"
		);

		$this->collectionsTemporaires = Collection::model()->with('ressource')
			->findAll("t.`type` = :type", [':type' => Collection::TYPE_TEMPORAIRE]);

		$this->collectionsSansAcces = Yii::app()->db->createCommand(
			<<<EOSQL
				SELECT r.*, c.nom AS collection
				FROM Collection c
				    JOIN Ressource r ON c.ressourceId = r.id
				    LEFT JOIN Service_Collection sc ON (sc.collectionId = c.id)
				WHERE sc.serviceId IS NULL
				GROUP BY c.id
				ORDER BY r.nom, collection
				EOSQL
		)->queryAll();

		$this->doublonsIdentification = Yii::app()->db->createCommand(
			<<<EOSQL
				SELECT GROUP_CONCAT(t.id) AS titreIds, GROUP_CONCAT(t.titre SEPARATOR ' / ') AS titres, i.ressourceId, r.nom as ressource, i.idInterne
				FROM Identification i
				    JOIN Titre t ON t.id=i.titreId
				    JOIN Ressource r ON r.id=i.ressourceId
				WHERE idinterne <> ''
				GROUP BY idinterne, ressourceId
				    HAVING count(*) > 1
				EOSQL
		)->queryAll();

		$this->revuesSansAccesLibre = [];
		foreach (['road', 'doaj'] as $src) {
			$sourceId = (int) Yii::app()->db
				->createCommand("SELECT id FROM Sourcelien WHERE nomcourt = :nom")
				->queryScalar([':nom' => $src]);
			$sql = <<<EOSQL
				SELECT t.revueId, MAX(t.titre) AS titre
				FROM Titre t
				    JOIN LienTitre l ON l.titreId = t.id AND l.sourceId = :sid
				    JOIN Titre t2 ON t2.revueId = t.revueId
				    LEFT JOIN Service s ON s.titreId = t2.id AND s.type = 'Intégral' AND s.acces = 'Libre'
				GROUP BY t.revueId
				    HAVING count(s.id) = 0
				EOSQL;
			$this->revuesSansAccesLibre[$src] = Yii::app()->db->createCommand($sql)->queryAll(true, [':sid' => $sourceId]);
		}

		$currentYear = date('Y');
		$year = $currentYear - 3;
		$this->doajInactif = Yii::app()->db
			->createCommand(<<<EOSQL
				WITH DoajLastYear AS (
					SELECT
						t.revueId,
						MAX(t.titre) AS titre,
						LEFT(MAX(IF(s.dateBarrFin = '', '$currentYear', s.dateBarrFin)), 4) AS lastYear
					FROM Service s
						JOIN LienTitre l USING(titreId)
						JOIN Titre t ON t.id = l.titreId
					WHERE l.sourceId = 1 -- DOAJ
						AND s.type = 'Intégral' AND s.acces = 'Libre'
					GROUP BY t.revueId
				)
				SELECT t.revueId, y.titre, y.lastYear
				FROM Titre t
					JOIN DoajLastYear y ON t.revueId = y.revueId
				WHERE t.obsoletePar IS NULL AND y.lastYear < $year
				EOSQL
			)
			->queryAll();
	}
}
