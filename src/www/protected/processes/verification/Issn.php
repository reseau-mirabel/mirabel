<?php

namespace processes\verification;

use CHtml;
use Norm;
use Titre;
use TitreWithExtra;
use Yii;
use components\HtmlTable;
use components\HtmlHelper;

class Issn
{
	private const SUIVI_COL = ['suivi-none' => '', 'suivi-other' => 'oui', 'suivi-self' => 'soi'];

	/**
	 * @var string[][]
	 */
	public array $titresIssneSansAcces;

	/**
	 * @var string[][]
	 */
	public array $titresIntegralSansIssne;

	/**
	 * @var string[][]
	 */
	public array $issnSansIssnl;

	/**
	 * @var string[][]
	 */
	public array $issnlSansIssn;

	/**
	 * @var string[][]
	 */
	public array $issnlAbsents;

	/**
	 * @var string[][]
	 */
	public array $issnlAutreRevue;

	/**
	 * @var string[][]
	 */
	public array $titresElectroMultiIssne;

	/**
	 * @var string[][]
	 */
	public array $supportInconnu;

	/**
	 * @var string[][]
	 */
	public array $conflictingIssnl;

	/**
	 * @var string[][]
	 */
	public array $sharedIssnl;

	/**
	 * @var string[][]
	 */
	public array $multipleIssne;

	/**
	 * @var string[][]
	 */
	public array $multipleIssnp;

	/**
	 * @var string[][]
	 */
	public array $discordingDates;

	/**
	 * @var string[][]
	 */
	public $sansIssn;

	/**
	 * @var string[][]
	 */
	public $issnEncours;

	/**
	 * Constructor that computes all the values in the properties.
	 */
	public function __construct()
	{
		$this->titresIssneSansAcces = array_map(
			function ($x) {
				static $k = 1;
				$titre = Titre::model()->populateRecord($x, false);
				$suivi = $titre->getSuiviType();
				return [
					'#' => (string) $k++,
					"ID Titre" => CHtml::link($x['id'], ['/titre/view', 'id' => $x['id']]),
					"Suivi" => CHtml::tag('span', ['class' => $suivi], self::SUIVI_COL[$suivi]),
					"Revue" => CHtml::link(CHtml::encode($x['titre']), ['/revue/view', 'id' => $x['revueId']]),
				];
			},
			self::getTitresIssneSansAcces()
		);

		$this->titresIntegralSansIssne = array_map(
			function ($x) {
				static $k = 1;
				$journalName = Norm::shortenText($x['titre'], 50);
				$titre = Titre::model()->populateRecord($x, false);
				$suivi = $titre->getSuiviType();
				return [
					'#' => (string) $k++,
					"ID Titre" => CHtml::link($x['id'], ['/titre/view', 'id' => $x['id']]),
					"†" => ($titre->dateFin ? '<span class="label" title="Ce titre a une date de fin.">†</span>' : ''),
					"Suivi" => CHtml::tag('span', ['class' => $suivi], self::SUIVI_COL[$suivi]),
					"Revue" => CHtml::link(CHtml::encode($journalName), ['/revue/view', 'id' => $x['revueId']]),
					"Pays de l'ISSN-P" => $x['pays'],
					"ISSN-P" => $x['issnp'],
					"ISSN-E même revue" => $x['issnrel'],
					'Accès' => $x['accesagr'],
					'PPN-E' => CHtml::link($x['PPNe'], sprintf(Titre::URL_SUDOC, $x['PPNe'])),
				];
			},
			self::getTitresIntegralSansIssne()
		);

		$this->issnSansIssnl = array_map(
			function ($x) {
				static $k = 1;
				$titre = TitreWithExtra::model()->populateRecord($x, false);
				$suivi = $titre->getSuiviType();
				return [
					'#' => (string) $k++,
					"ID Titre" => CHtml::link($x['id'], ['/titre/view', 'id' => $x['id']]),
					"Suivi" => CHtml::tag('span', ['class' => $suivi], self::SUIVI_COL[$suivi]),
					"Issn" => $x['issn'],
					"Statut" => ($x['issnStatut'] == \Issn::STATUT_VALIDE ? "" : \Issn::$enumStatut[$x['issnStatut']]),
					"Revue" => CHtml::link(CHtml::encode($x['titre']), ['/revue/view', 'id' => $x['revueId']]),
				];
			},
			self::getIssnSansIssnl()
		);

		$this->issnlSansIssn = array_map(
			function ($x) {
				static $k = 1;
				$titre = TitreWithExtra::model()->populateRecord($x, false);
				$suivi = $titre->getSuiviType();
				return [
					'#' => (string) $k++,
					"ID Titre" => CHtml::link($x['id'], ['/titre/view', 'id' => $x['id']]),
					"Suivi" => CHtml::tag('span', ['class' => $suivi], self::SUIVI_COL[$suivi]),
					"Issn-L" => $x['issnl'],
					"Revue" => CHtml::link(CHtml::encode($x['titre']), ['/revue/view', 'id' => $x['revueId']]),
				];
			},
			self::getIssnlSansIssn()
		);

		$this->issnlAutreRevue= array_map(
			function ($x) {
				static $k = 1;
				$titre = TitreWithExtra::model()->populateRecord($x, false);
				$suivi = $titre->getSuiviType();
				return [
					'#' => (string) $k++,
					"ID Titre" => CHtml::link($x['id'], ['/titre/view', 'id' => $x['id']]),
					"Suivi" => CHtml::tag('span', ['class' => $suivi], self::SUIVI_COL[$suivi]),
					"Issn-L" => CHtml::link($x['issnl'], ['/revue/search', 'q' => ['issn' => $x['issnl']]]),
					"Revue Issn-L" => $titre->getSelfLink(),
					"Revue Issn-(P|E)" => CHtml::link(CHtml::encode($x['titre2']), ['/revue/view', 'id' => $x['revueId2']]),
				];
			},
			self::getIssnlAutreRevue()
		);

		$this->issnlAbsents = array_map(
			function ($x) {
				static $k = 1;
				$titre = TitreWithExtra::model()->populateRecord($x, false);
				$suivi = $titre->getSuiviType();
				return [
					'#' => (string) $k++,
					"ID Titre" => CHtml::link($x['id'], ['/titre/view', 'id' => $x['id']]),
					"Suivi" => CHtml::tag('span', ['class' => $suivi], self::SUIVI_COL[$suivi]),
					"Issn-L" => CHtml::encode($x['issnl']),
					"Revue Issn-L" => $titre->getSelfLink(),
				];
			},
			self::getIssnlAbsents()
		);

		$this->titresElectroMultiIssne = array_map(
			function ($x) {
				static $k = 1;
				$titre = TitreWithExtra::model()->populateRecord($x, false);
				$suivi = $titre->getSuiviType();
				return [
					'#' => (string) $k++,
					"ID Titre" => CHtml::link($x['id'], ['/titre/view', 'id' => $x['id']]),
					"Suivi" => CHtml::tag('span', ['class' => $suivi], self::SUIVI_COL[$suivi]),
					"Issn" => CHtml::encode($x['extra']),
					"Revue Issn-L" => $titre->getSelfLink(),
				];
			},
			self::gettitresElectroMultiIssne()
		);

		$this->supportInconnu = array_map(
			function ($x) {
				static $k = 1;
				$titre = TitreWithExtra::model()->populateRecord($x, false);
				$suivi = $titre->getSuiviType();
				return [
					'#' => (string) $k++,
					"ID Titre" => CHtml::link($x['id'], ['/titre/view', 'id' => $x['id']]),
					"Suivi" => CHtml::tag('span', ['class' => $suivi], self::SUIVI_COL[$suivi]),
					"Issn" => CHtml::encode($x['extra']),
					"Revue" => $titre->getSelfLink(),
				];
			},
			self::getSupportInconnu()
		);

		$this->sharedIssnl = array_map(
			function ($x) {
				static $k = 1;
				$titre = TitreWithExtra::model()->populateRecord($x, false);
				$suivi = $titre->getSuiviType();
				return [
					'#' => (string) $k++,
					"Suivi" => CHtml::tag('span', ['class' => $suivi], self::SUIVI_COL[$suivi]),
					"Issn-L" => CHtml::encode($x['extra']),
					"Revue" => $titre->getSelfLink(),
				];
			},
			self::getSharedIssnl()
		);

		$this->multipleIssnp = self::findMultipleIssnSupport(\Issn::SUPPORT_PAPIER);
		$this->multipleIssne = self::findMultipleIssnSupport(\Issn::SUPPORT_ELECTRONIQUE);

		$this->discordingDates = array_map(
			function ($x) {
				static $k = 1;
				$titre = Titre::model()->populateRecord($x, false);
				$suivi = $titre->getSuiviType();
				return [
					"#" => (string) $k++,
					"ID Titre" => CHtml::link($x['id'], ['/titre/view', 'id' => $x['id']]),
					"Suivi" => CHtml::tag('span', ['class' => $suivi], self::SUIVI_COL[$suivi]),
					"Revue" => CHtml::link(CHtml::encode($x['titre']), ['/revue/view', 'id' => $x['revueId']]),
					"Date de début" => $x['dateDebut'],
					"ISSN début" => $x['ISSN début'],
					"Date de fin" => $x['dateFin'],
					"ISSN fin" => $x['ISSN fin'],
				];
			},
			self::getDiscordingDates()
		);

		$this->sansIssn = array_map(
			function ($x) {
				static $k = 1;
				$journalName = Norm::shortenText($x['titre'], 55);
				$titre = Titre::model()->populateRecord($x, false);
				$suivi = $titre->getSuiviType();
				return [
					'#' => (string) $k++,
					"ID Titre" => CHtml::link($x['id'], ['/titre/view', 'id' => $x['id']]),
					"†" => ($titre->dateFin ? '<span class="label" title="Ce titre a une date de fin.">†</span>' : ''),
					"Suivi" => CHtml::tag('span', ['class' => $suivi], self::SUIVI_COL[$suivi]),
					"Revue" => CHtml::link(CHtml::encode($journalName), ['/revue/view', 'id' => $x['revueId']]),
					'PPN' => join(' | ', array_map(
						function ($y) {
							return CHtml::link($y, sprintf(Titre::URL_SUDOC, $y));
						}, explode(',', $x['sudocPpn'] ?? '')
					)),
					"Pays des éditeurs actuels" => $x['pays'],
				];
			},
			self::getSansIssn()
		);

		$this->issnEncours = array_map(
			function ($x) {
				static $k = 1;
				$journalName = Norm::shortenText($x['titre'], 55);
				$titre = Titre::model()->populateRecord($x, false);
				$suivi = $titre->getSuiviType();
				return [
					'#' => (string) $k++,
					"ID Titre" => CHtml::link($x['id'], ['/titre/view', 'id' => $x['id']]),
					"†" => ($titre->dateFin ? '<span class="label" title="Ce titre a une date de fin.">†</span>' : ''),
					"Suivi" => CHtml::tag('span', ['class' => $suivi], self::SUIVI_COL[$suivi]),
					"Revue" => CHtml::link(CHtml::encode($journalName), ['/revue/view', 'id' => $x['revueId']]),
					'PPN' => CHtml::link($x['sudocPpn'], sprintf(Titre::URL_SUDOC, $x['sudocPpn'])),
					'Support' => \Issn::$enumSupport[$x['support']],
					"Pays des éditeurs actuels" => $x['pays'],
				];
			},
			self::getIssnEncours()
		);

		$this->conflictingIssnl = array_map(
			function ($x) {
				static $k = 1;
				$titre = Titre::model()->populateRecord($x, false);
				$suivi = $titre->getSuiviType();
				return [
					"#" => (string) $k++,
					"ID Titre" => CHtml::link($x['titreId'], ['/titre/view', 'id' => $x['titreId']]),
					"Suivi" => CHtml::tag('span', ['class' => $suivi], self::SUIVI_COL[$suivi]),
					"ISSN-L 1" => CHtml::tag('span', ['class' => 'issn-number'], $x['issnl1']),
					"ISSN-L 2" => CHtml::tag('span', ['class' => 'issn-number'], $x['issnl2']),
					"ISSN-P" => self::wrapIssnNotInList([$x['issnl1'], $x['issnl2']], $x['issnp']),
					"ISSN-E" => self::wrapIssnNotInList([$x['issnl1'], $x['issnl2']], $x['issne']),
					"Titre" => CHtml::link(CHtml::encode($x['titre']), ['/titre/view', 'id' => $x['titreId']]),
				];
			},
			self::getConflictingIssnl()
		);

	}

	public static function displayExporTable(string $tableName, array $verifData): string
	{
		if (!$verifData) {
			return '<p>Aucun.</p>';
		}
		$table = HtmlTable::build($verifData)->addClass('exportable export-href');
		$table->htmlAttributes['data-exportable-filename'] = HtmlHelper::exportableName($tableName);
		return $table->toHtml();
	}

	private static function findMultipleIssnSupport(string $support): array
	{
		return array_map(
			function ($x) {
				static $k = 1;
				$titre = TitreWithExtra::model()->populateRecord($x, false);
				$suivi = $titre->getSuiviType();
				return [
					'#' => (string) $k++,
					"Suivi" => CHtml::tag('span', ['class' => $suivi], self::SUIVI_COL[$suivi]),
					"Issn" => CHtml::encode($x['extra']),
					"Revue" => $titre->getSelfLink(),
				];
			},
			self::getMultipleIssn($support)
		);
	}

	private static function getMultipleIssn(string $support): array
	{
		return TitreWithExtra::model()->findAllBySql(
			<<<EOSQL
			SELECT t.*, i1.issn AS extra
			FROM Titre t
				JOIN Issn i1 ON t.id = i1.titreId AND i1.issn IS NOT NULL AND i1.support = :support1
				JOIN Issn i2 ON t.id = i2.titreId AND i2.issn IS NOT NULL AND i2.support = :support2
			WHERE i1.id < i2.id AND i1.statut = :st1 AND i2.statut = :st2
			GROUP BY t.id
			ORDER BY t.titre
			EOSQL,
			[
				':support1' => $support,
				':support2' => $support,
				':st1' => \Issn::STATUT_VALIDE,
				':st2' => \Issn::STATUT_VALIDE,
			]
		);
	}

	private static function getTitresIssneSansAcces(): array
	{
		return Titre::model()->findAllBySql(
			<<<EOSQL
			WITH RevueWithIssne AS (
				SELECT t.revueId
				FROM Issn i
					JOIN Titre t ON t.id = i.titreId
				WHERE i.issn IS NOT NULL AND i.support = :support
				GROUP BY t.revueId
			), RevueWithAccess AS (
				SELECT t.revueId
				FROM Service s
					JOIN Titre t ON t.id = s.titreId
				WHERE s.`type` = :stype
				GROUP BY t.revueId
			)
			SELECT
				t.id, t.revueId, t.titre, t.sigle, t.obsoletePar, t.dateFin
			FROM Titre t
				JOIN RevueWithIssne USING(revueId)
				LEFT JOIN RevueWithAccess r ON t.revueId = r.revueId
			WHERE
				t.obsoletePar IS NULL
				AND r.revueId IS NULL
			ORDER BY titre
			EOSQL,
			[':support' => \Issn::SUPPORT_ELECTRONIQUE, ':stype' => \Service::TYPE_INTEGRAL]
		);
	}

	private static function getTitresIntegralSansIssne(): array
	{
		return Yii::app()->db->createCommand(
			<<<EOSQL
			WITH TitreAccesIntegral AS (
				SELECT
					s.titreId,
					IF(COUNT(DISTINCT s.acces) = 1, s.acces, 'Mixte') AS accesagr
				FROM Service s
				WHERE s.type = :sintegral AND s.selection = 0
				GROUP BY s.titreId
			), TitreAccesIntegralSansIssne AS (
				SELECT
					t.id, t.revueId, t.titre, t.sigle, t.obsoletePar, t.dateFin,
					ti.accesagr
				FROM Titre t
					JOIN TitreAccesIntegral ti ON ti.titreId = t.id
					LEFT JOIN Issn ie ON ie.titreId = t.id AND ie.support = :elec AND ie.issn IS NOT NULL
				WHERE ie.id IS NULL
			)
			SELECT
				t.*,
				GROUP_CONCAT(DISTINCT ip.issn SEPARATOR ' ') AS issnp,
				GROUP_CONCAT(DISTINCT Pays.nom) AS pays,
				GROUP_CONCAT(DISTINCT i.issn SEPARATOR ' ') AS issnrel,
				GROUP_CONCAT(DISTINCT ie.sudocPpn SEPARATOR ' ') AS PPNe
			FROM TitreAccesIntegralSansIssne t
				LEFT JOIN Issn ie ON ie.titreId = t.id AND ie.support = :elec AND ie.issn IS NULL
				LEFT JOIN Issn ip ON ip.titreId = t.id AND ip.support = :pap  AND ip.issn IS NOT NULL
				LEFT JOIN Pays ON ip.pays = Pays.code2
				LEFT JOIN Titre t2 ON t2.revueId = t.revueId AND t2.id <> t.id
				LEFT JOIN Issn i ON i.titreId = t2.id AND i.support = :elec AND i.issn IS NOT NULL
			GROUP BY t.id
			ORDER BY t.titre
			EOSQL
		)
			->queryAll(true, [':elec' => \Issn::SUPPORT_ELECTRONIQUE, ':pap' => \Issn::SUPPORT_PAPIER, ':sintegral' => \Service::TYPE_INTEGRAL]);
	}

	private static function getSansIssn(): array
	{
		return Yii::app()->db->createCommand(<<<EOSQL
			WITH TitreLackingIssn AS (
				SELECT t.id, t.revueId, t.titre, t.sigle, t.obsoletePar, t.dateFin
				FROM Titre t
					LEFT JOIN Issn i ON t.id = i.titreId AND i.issn IS NOT NULL
				WHERE i.id IS NULL
			)
			SELECT
				t.*,
				GROUP_CONCAT(DISTINCT i.sudocPpn) AS sudocPpn,
				GROUP_CONCAT(DISTINCT p.nom ORDER BY p.nom SEPARATOR ' | ') AS pays
			FROM TitreLackingIssn t
				LEFT JOIN Issn i ON t.id = i.titreId AND i.sudocPpn <> ''
				LEFT JOIN Titre_Editeur te ON te.titreId = t.id AND te.ancien = 0
				LEFT JOIN Editeur e ON te.editeurId = e.id
				LEFT JOIN Pays p ON e.paysId = p.id
			GROUP BY t.id
			ORDER BY t.titre
			EOSQL
		)
			->queryAll();
	}

	private static function getIssnEncours(): array
	{
		return Yii::app()->db->createCommand(<<<EOSQL
			SELECT
				t.id, t.revueId, t.titre, t.sigle, t.obsoletePar, t.dateFin,
				i.support,
				i.sudocPpn,
				GROUP_CONCAT(DISTINCT p.nom ORDER BY p.nom SEPARATOR ' | ') AS pays
			FROM Titre t
			    JOIN Issn i ON t.id = i.titreId
			    LEFT JOIN Titre_Editeur te ON te.titreId = t.id AND te.ancien = 0
			    LEFT JOIN Editeur e ON te.editeurId = e.id
				LEFT JOIN Pays p ON e.paysId = p.id
			WHERE i.statut = :st
			GROUP BY i.id
			ORDER BY t.titre
			EOSQL
		)
			->queryAll(true, [':st' => \Issn::STATUT_ENCOURS]);
	}

	private static function getConflictingIssnl(): array
	{
		return Yii::app()->db->createCommand(<<<EOSQL
			WITH IssnSupport AS (
				SELECT titreId,
					GROUP_CONCAT(IF(support='papier', issn, NULL)) AS issnp,
					GROUP_CONCAT(IF(support='electronique', issn, NULL)) AS issne
				FROM Issn
				GROUP BY titreId
			)
			SELECT t.revueId, i1.issnl AS issnl1, i2.issnl AS issnl2, t.id AS titreId, t.titre, issnp, issne
			FROM Issn i1
			  JOIN Issn i2 ON i1.titreId = i2.titreId AND i1.id < i2.id
			  JOIN Titre t ON t.id = i1.titreId
			  LEFT JOIN IssnSupport ist ON (ist.titreId = i1.titreId)
			WHERE i1.issnl IS NOT NULL AND i2.issnl IS NOT NULL AND i1.issnl <> i2.issnl
			GROUP BY i1.titreId
			ORDER BY t.titre
			EOSQL
		)
			->queryAll();
	}

	private static function getDiscordingDates(): array
	{
		return Yii::app()->db->createCommand(
			<<<EOSQL
			SELECT t.id, t.revueId, t.titre, t.dateDebut, i1.dateDebut AS 'ISSN début', t.dateFin, i2.dateFin AS 'ISSN fin'
			FROM Titre t
			  JOIN (SELECT titreId, MIN(dateDebut) AS dateDebut FROM Issn WHERE dateDebut <> '' GROUP BY titreId) i1 ON t.id = i1.titreId
			  JOIN (SELECT titreId, MAX(dateFin) AS dateFin FROM Issn WHERE dateFin <> '' GROUP BY titreId) i2 ON t.id = i2.titreId
			WHERE (t.dateDebut <> '' AND RIGHT(i1.dateDebut, 1) <> 'X' AND i1.dateDebut <> LEFT(t.dateDebut, 4))
			  OR (t.dateFin <> '' AND RIGHT(i2.dateFin, 1) AND i2.dateFin <> LEFT(t.dateFin, 4))
			ORDER BY t.titre
			EOSQL
		)
			->queryAll();
	}

	private static function getIssnSansIssnl(): array
	{
		return Yii::app()->db->createCommand(
			<<<EOSQL
			SELECT t.*, i.issn, i.statut AS issnStatut
			FROM Titre t
			  JOIN Issn i ON t.id = i.titreId
			WHERE i.issnl IS NULL AND i.issn IS NOT NULL
			GROUP BY t.id, i.issn
			ORDER BY t.titre
			EOSQL
		)
			->queryAll();
	}

	private static function getIssnlSansIssn(): array
	{
		return Yii::app()->db->createCommand(
			<<<EOSQL
			SELECT t.*, i.issnl
			FROM Titre t
			  JOIN Issn i ON t.id = i.titreId
			WHERE i.issn IS NULL AND i.issnl IS NOT NULL
			GROUP BY t.id, i.issnl
			ORDER BY t.titre
			EOSQL
		)
			->queryAll();
	}

	private static function getIssnlAutreRevue(): array
	{
		return Yii::app()->db->createCommand(
			<<<EOSQL
			WITH JournalsSharingIssnl AS (
				SELECT i1.issnl, i1.titreId AS titre1Id, i2.titreId AS titre2Id
				FROM Issn i1
					JOIN Issn i2 ON i1.issnl = i2.issn AND i1.titreId <> i2.titreId
				WHERE i1.issnl IS NOT NULL
			)
			SELECT j.issnl, t1.id, t1.titre, t1.revueId, t2.titre AS titre2, t2.revueId AS revueId2
			FROM Titre t1
				JOIN JournalsSharingIssnl j ON t1.id = j.titre1Id
				JOIN Titre t2 ON t2.id = j.titre2Id
			WHERE t1.revueId <> t2.revueId
			GROUP BY t1.id
			ORDER BY t1.titre
			EOSQL
		)
			->queryAll();
	}

	private static function getIssnlAbsents(): array
	{
		return Yii::app()->db->createCommand(
			<<<EOSQL
			WITH IssnlWithNoMatch AS (
				SELECT DISTINCT i1.titreId, i1.issnl
				FROM Issn i1
					LEFT JOIN Issn i2 ON i1.issnl = i2.issn
				WHERE i1.issnl IS NOT NULL AND i2.id IS NULL
			)
			SELECT t.*, i.issnl
			FROM Titre t
				JOIN IssnlWithNoMatch i ON t.id = i.titreId
			GROUP BY t.revueId
			ORDER BY t.titre
			EOSQL
		)
			->queryAll();
	}

	private static function getSupportInconnu(): array
	{
		return Yii::app()->db->createCommand(
			<<<EOSQL
			SELECT t.*, i.issn AS extra
			FROM Titre t
				JOIN Issn i ON t.id = i.titreId
			WHERE i.support = :support AND i.statut = :statut AND i.issn IS NOT NULL
			ORDER BY t.titre
			EOSQL
		)
			->queryAll(true, [':support' => \Issn::SUPPORT_INCONNU, ':statut' => \Issn::STATUT_VALIDE]);
	}

	private static function getSharedIssnl(): array
	{
		return Yii::app()->db->createCommand(
			<<<EOSQL
			SELECT t1.*, i1.issnl AS extra
			FROM Titre t1
				JOIN Issn i1 ON t1.id = i1.titreId AND i1.issnl IS NOT NULL
				JOIN Titre t2 ON t1.revueId = t2.revueId
				JOIN Issn i2 ON t2.id = i2.titreId AND i2.issnl IS NOT NULL
			WHERE t1.id <> t2.id AND i1.issnl = i2.issnl
			GROUP BY t1.id
			ORDER BY t1.titre
			EOSQL
		)
			->queryAll();
	}

	private static function getTitresElectroMultiIssne(): array
	{
		return Yii::app()->db->createCommand(
			<<<EOSQL
			WITH TitreElectro AS (
				SELECT t.id
				FROM Titre t
					JOIN Issn i ON i.titreId = t.id AND i.issn IS NOT NULL
				GROUP BY t.id, t.obsoletePar
					HAVING MIN(i.support = :support) = 1 AND t.obsoletePar IS NULL
			)
			SELECT t1.*, i1.issn AS extra
			FROM Titre t1
			  JOIN TitreElectro AS te ON te.id = t1.id
			  JOIN Issn i1 ON t1.id = i1.titreId AND i1.issn IS NOT NULL
			  JOIN Titre t2 ON t1.revueId = t2.revueId
			  JOIN Issn i2 ON t2.id = i2.titreId AND i2.issn IS NOT NULL
			WHERE t1.id <> t2.id AND i1.issn = i2.issn
			GROUP BY t1.id
			ORDER BY t1.titre
			EOSQL
		)
			->queryAll(true, [':support' => \Issn::SUPPORT_ELECTRONIQUE]);
	}

	private static function wrapIssnNotInList(array $issnls, string|null $issntxt): string
	{
		if ($issntxt === null || $issntxt === '') {
			return '';
		}
		$issndisplay = [];
		$issnlist = explode(',', $issntxt);
		foreach ($issnlist as $issn) {
			if (!in_array($issn, $issnls)) {
				$issndisplay[] = CHtml::tag('strong', ['class' => 'issn-number'], $issn);
			} else {
				$issndisplay[] = CHtml::tag('span', ['class' => 'issn-number'], $issn);
			}
		}
		return join(' ', $issndisplay);
	}
}
