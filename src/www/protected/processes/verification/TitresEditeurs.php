<?php

namespace processes\verification;

use Titre;

class TitresEditeurs
{
	/**
	 * @var Titre[]
	 */
	private array $titresMorts;

	/**
	 * @var Titre[]
	 */
	private array $titresVivants;

	public function __construct()
	{
		$this->titresVivants = Titre::model()->findAllBySql(
			<<<EOSQL
			SELECT t.*
			FROM Titre t
			  LEFT JOIN Titre_Editeur te ON t.id = te.titreId
			WHERE te.editeurId IS NULL AND t.obsoletePar IS NULL AND t.dateFin = ''
			ORDER BY t.titre
			EOSQL
		);
		$this->titresMorts = Titre::model()->findAllBySql(
			<<<EOSQL
			SELECT t.*
			FROM Titre t
			  LEFT JOIN Titre_Editeur te ON t.id = te.titreId
			WHERE te.editeurId IS NULL AND (t.obsoletePar IS NOT NULL OR t.dateFin > 0)
			ORDER BY t.titre
			EOSQL
		);
	}

	/**
	 * @return Titre[]
	 */
	public function getTitresMortsSansEditeurs(): array
	{
		return $this->titresMorts;
	}

	/**
	 * @return Titre[]
	 */
	public function getTitresVivantsSansEditeurs(): array
	{
		return $this->titresVivants;
	}
}
