<?php

namespace processes\verification;

use Titre;
use Yii;
use components\HtmlTable;
use components\HtmlHelper;

class Revues
{
	private const SUIVI_COL = ['suivi-none' => '', 'suivi-other' => 'oui', 'suivi-self' => 'soi'];

	/**
	 * @var string[][]
	 */
	public array $titresMorts;

	public array $revuesDebordementDebut;

	public array $revuesDebordementFin;

	public array $revuesWithObsoleteDuplicate;

	public array $revuesWithDateProblem;

	public function __construct()
	{
		$rowsTitresMorts = Titre::model()->findAllBySql(
			"SELECT t.* FROM Titre t WHERE dateFin = '' AND obsoletePar IS NOT NULL ORDER BY titre"
		);
		$this->titresMorts = array_map(
			function (Titre $t) {
				static $k = 1;
				$suivi = $t->getSuiviType();
				return [
					'#' => (string) $k++,
					"ID titre" => (string) $t->id,
					"ID revue" => (string) $t->revueId,
					"Suivi" => \CHtml::tag('span', ['class' => $suivi], self::SUIVI_COL[$suivi]),
					"Titre" => $t->getSelfLink(),
					"Successeur" => (string) $t->obsoletePar,
				];
			},
			$rowsTitresMorts
		);

		$this->revuesDebordementDebut = self::findRevuesDebordementDebut();
		$this->revuesDebordementFin = self::findRevuesDebordementFin();

		$this->revuesWithObsoleteDuplicate = [];
		$k = 1;
		foreach (self::getWithObsoleteDuplicate() as $row) {
			$titre = Titre::model()->populateRecord($row, false);
			$suivi = $titre->getSuiviType();
			$this->revuesWithObsoleteDuplicate[] = [
				'#' => (string) $k++,
				"ID revue" => (string) $row['revueId'],
				"Suivi" => \CHtml::tag('span', ['class' => $suivi], self::SUIVI_COL[$suivi]),
				"Revue" => \CHtml::link(\CHtml::encode($row['titre']), ['/revue/view', 'id' => $row['revueId']]),
				"Trier" => Yii::app()->user->access()->toTitre()->createDirect($row['revueId']) ?
					\CHtml::link("Trier", ['revue/trier-titre', 'id' => $row['revueId']])
					: '',
			];
		}

		$this->revuesWithDateProblem = [];
		$k = 1;
		foreach (self::getWithDateProblem() as $row) {
			$titre = Titre::model()->populateRecord($row, false);
			$suivi = $titre->getSuiviType();
			$this->revuesWithDateProblem[] = [
				'#' => (string) $k++,
				"ID revue" => (string) $row['revueId'],
				"Suivi" => \CHtml::tag('span', ['class' => $suivi], self::SUIVI_COL[$suivi]),
				"Revue" => \CHtml::link(\CHtml::encode($row['titre']), ['/revue/view', 'id' => $row['revueId']]),
				"Trier" => Yii::app()->user->access()->toTitre()->createDirect((int) $row['revueId']) ?
					\CHtml::link("Trier", ['revue/trier-titre', 'id' => $row['revueId']])
					: '',
			];
		}
	}

	public static function displayExporTable($tableName, $verifData): string
	{
		$table = HtmlTable::build($verifData)->addClass('exportable export-href');
		$table->htmlAttributes['data-exportable-filename'] = HtmlHelper::exportableName($tableName);
		return $table->toHtml();
	}

	private static function sqlDebordementWith(): string
	{
		return <<<EOSQL
			WITH ServiceDate AS (
				SELECT
					t.revueId,
					s.dateBarrDebut AS debut,
					IF(dateBarrFin = '', CURDATE(), dateBarrFin) AS fin,
					IF(s.import > 0 AND r.autoImport > 0, s.ressourceId, 0) AS ressourceId,
					IF(s.import > 0 AND r.autoImport > 0, r.nom, '') AS ressourceNom
				FROM Service s
					JOIN Titre t ON t.id = s.titreId
					JOIN Ressource r ON r.id = s.ressourceId
			),
			RevueDate AS (
				SELECT
					revueId,
					MAX(IF(obsoletePar IS NULL, titre, '')) AS titre,
					min(dateDebut) AS debut,
					MAX(IF(dateFin = '', CURDATE(), dateFin)) AS fin
				FROM Titre
				GROUP BY revueId
			)
			EOSQL;
	}

	private static function findRevuesDebordementDebut(): array
	{
		$sql = <<<EOSQL
			SELECT
				r.revueId,
				r.titre,
				json_objectagg(s.ressourceId, s.ressourceNom) AS ressource,
				r.debut AS debutRevue,
				s.debut AS debutService
			FROM RevueDate r
				JOIN ServiceDate s USING(revueId)
			WHERE s.debut < r.debut
			GROUP BY r.revueId
			ORDER BY r.titre
			EOSQL;
		$k = 1;
		$result = [];
		$debordementDebut = Yii::app()->db->createCommand(self::sqlDebordementWith() . " $sql")->queryAll();
		foreach ($debordementDebut as $row) {
			$titre = Titre::model()->populateRecord($row, false);
			$suivi = $titre->getSuiviType();
			$result[] = [
				'#' => (string) $k++,
				"ID revue" => (string) $row['revueId'],
				"Suivi" => \CHtml::tag('span', ['class' => $suivi], self::SUIVI_COL[$suivi]),
				"Importé" => self::displayRessourceList($row['ressource']),
				"Revue" => \CHtml::link(\CHtml::encode($row['titre']), ['/revue/view', 'id' => $row['revueId']]),
				"Début de revue" => $row['debutRevue'],
				"Début de l'accès" => $row['debutService'],
			];
		}
		return $result;
	}

	private static function findRevuesDebordementFin(): array
	{
		$sql = <<<EOSQL
			SELECT
				r.revueId,
				r.titre,
				json_objectagg(s.ressourceId, s.ressourceNom) AS ressource,
				r.fin AS finRevue,
				s.fin AS finService
			FROM RevueDate r
				JOIN ServiceDate s USING(revueId)
			WHERE s.fin > CONCAT(r.fin, '-99-99')
			GROUP BY r.revueId
			ORDER BY r.titre
			EOSQL;
		$today = date('Y-m-d');
		$k = 1;
		$result = [];
		$debordementFin = Yii::app()->db->createCommand(self::sqlDebordementWith() . " $sql")->queryAll();
		foreach ($debordementFin as $row) {
			$titre = Titre::model()->populateRecord($row, false);
			$suivi = $titre->getSuiviType();
			$result[] = [
				'#' => (string) $k++,
				"ID revue" => (string) $row['revueId'],
				"Suivi" => \CHtml::tag('span', ['class' => $suivi], self::SUIVI_COL[$suivi]),
				"Importé" => self::displayRessourceList($row['ressource']),
				"Revue" => \CHtml::link(\CHtml::encode($row['titre']), ['/revue/view', 'id' => $row['revueId']]),
				"Fin de revue" => $row['finRevue'],
				"Fin de l'accès" => $row['finService'] === $today ? '…' : $row['finService'],
			];
		}
		return $result;
	}

	private static function getWithObsoleteDuplicate(): array
	{
		// Revue à problème si plus de titres obsolètes que de valeurs distinctes de obsoletePar.
		return Yii::app()->db->createCommand(
			<<<EOSQL
				SELECT revueId, MAX(titre) AS titre
				FROM Titre
				GROUP BY revueId
				HAVING count(obsoletePar) > count(distinct obsoletePar)
				EOSQL
		)->queryAll();
	}

	/**
	 * Return a list of revue with date problem
	 * @return array{revueId: string, titre: string}[]
	 */
	private static function getWithDateProblem(): array
	{
		return Yii::app()->db->createCommand(<<<EOSQL
			SELECT DISTINCT titre_predecesseur.revueId, tActif.titre
			FROM Titre titre_predecesseur
				JOIN Titre tActif ON tActif.revueId = titre_predecesseur.revueId AND tActif.obsoletePar IS NULL
				LEFT JOIN Titre titre_successeur ON titre_predecesseur.obsoletePar = titre_successeur.id
			WHERE
					titre_predecesseur.obsoletePar IS NOT NULL
					AND
					(titre_predecesseur.dateFin = '' OR LEFT(titre_predecesseur.dateFin, LENGTH(titre_successeur.dateDebut)) > titre_successeur.dateDebut)
			EOSQL
		)->queryAll();
	}

	private static function displayRessourceList(string $json): string
	{
		$list = array_unique(array_filter(
			json_decode($json, true) ?? []
		));
		if (!$list) {
			return '';
		}
		$html = [];
		foreach ($list as $k => $v) {
			$html[] = sprintf('<abbr title="Accès importé automatiquement dans %s">%d</abbr>', \CHtml::encode($v), $k);
		}
		return join(' ', $html);
	}
}
