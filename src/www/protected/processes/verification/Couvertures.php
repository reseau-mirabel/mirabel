<?php

namespace processes\verification;

use Titre;
use Yii;

class Couvertures
{
	/**
	 * @var list{string, int, string, string}[]
	 */
	public array $titresSansCouvertureAvecUrl;

	/**
	 * @var Titre[]
	 */
	public array $titresSansCouverture;

	public function __construct()
	{
		$this->titresSansCouverture = $this->getTitlesMissingCover();
		$this->titresSansCouvertureAvecUrl = $this->getTitlesWithUrlMissingCover();
	}

	private function getTitlesWithUrlMissingCover(): array
	{
		$db = Yii::app()->db;

		// List the images into a temp table.
		$db->createCommand("CREATE TEMPORARY TABLE couverture (id INT NOT NULL) ENGINE=MEMORY")->execute();
		$path = dirname(Yii::app()->getBasePath()) . '/images/titres-couvertures';
		if (!is_dir($path)) {
			@mkdir($path);
		}
		$ids = array_map('intval', scandir($path, \SCANDIR_SORT_NONE));
		$transaction = $db->beginTransaction();
		for ($i = 0; $i < count($ids); $i += 1000) {
			$size = ($i + 1000 > count($ids)) ? count($ids) - $i : 1000;
			$db->createCommand("INSERT INTO couverture VALUES (" . join("),(", array_slice($ids, $i, $size)) . ")")->execute();
		}
		$transaction->commit();
		$db->createCommand("ALTER TABLE couverture ADD INDEX idx_id (id)")->execute();

		$couvertures = [];
		$titles = $db->createCommand(
			"SELECT t.* FROM Titre t LEFT JOIN couverture c ON t.id = c.id WHERE t.urlCouverture <> '' AND c.id IS NULL ORDER BY t.id LIMIT 100"
		)->queryAll();
		foreach ($titles as $t) {
			$title = Titre::model()->populateRecord($t, false);
			$couvertures[] = [$title->getSuiviType(), (int) $title->id, $title->getSelfLink(), $title->urlCouverture];
		}
		$db->createCommand("DROP TEMPORARY TABLE couverture")->execute();
		return $couvertures;
	}

	private function getTitlesMissingCover(): array
	{
		return Titre::model()->findAllBySql(<<<EOSQL
			SELECT * FROM Titre
			WHERE urlCouverture = '' AND obsoletePar IS NULL
			ORDER BY titre
			EOSQL
		);
	}
}
