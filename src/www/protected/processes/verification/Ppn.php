<?php

namespace processes\verification;

use TitreWithExtra;

class Ppn
{
	/**
	 * @return TitreWithExtra[]
	 */
	public function getIssnSansPpn(): array
	{
		return TitreWithExtra::model()->findAllBySql(
			<<<EOSQL
			SELECT t.*, GROUP_CONCAT(i.issn) AS extra
			FROM Titre t
				JOIN Issn i ON t.id = i.titreId
			WHERE i.sudocPpn IS NULL AND i.issn IS NOT NULL
			GROUP BY t.id
			ORDER BY t.titre
			EOSQL
		);
	}

	/**
	 * @return TitreWithExtra[]
	 */
	public function getPpnNoHolding(): array
	{
		return TitreWithExtra::model()->findAllBySql(
			<<<EOSQL
			SELECT t.*, GROUP_CONCAT(i.sudocPpn) AS extra
			FROM Titre t
				JOIN Issn i ON t.id = i.titreId
			WHERE i.sudocPpn IS NOT NULL AND i.sudocNoHolding = 1
			GROUP BY t.id
			ORDER BY t.titre
			EOSQL
		);
	}

	/**
	 * @return TitreWithExtra[]
	 */
	public function getPpnSansIssn(): array
	{
		return TitreWithExtra::model()->findAllBySql(
			<<<EOSQL
			SELECT t.*, GROUP_CONCAT(i.sudocPpn) AS extra
			FROM Titre t
				JOIN Issn i ON t.id = i.titreId
			WHERE i.sudocPpn IS NOT NULL AND i.issn IS NULL
			GROUP BY t.id
			ORDER BY t.titre
			EOSQL
		);
	}
}
