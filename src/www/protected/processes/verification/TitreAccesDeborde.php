<?php

namespace processes\verification;

class TitreAccesDeborde
{
	/**
	 * @var array<int, array{ressource: string, table: array}>
	 */
	public readonly array $byResource;

	/**
	 * @var list<array{ressource: string, titreId: int, revueId: int, titre: string, services: int}>
	 */
	public readonly array $singleErrors;

	public function __construct()
	{
		$all = self::fetchData();
		$byResource = [];
		$singleErrors = [];
		foreach ($all as $id => $x) {
			if (count($x['table']) === 1) {
				$first = reset($x['table']);
				$singleErrors[] = array_merge(['ressource' => $x['ressource']], $first);
			} else {
				$byResource[$id] = $x;
			}
		}
		unset($all);

		$this->singleErrors = $singleErrors;

		// Sort
		usort($byResource, fn ($a, $b) => count($b['table']) <=> count($a['table']));

		$this->byResource = $byResource;
	}

	private static function fetchData(): array
	{
		$cmd = \Yii::app()->db
			->createCommand(<<<SQL
			SELECT
				s.id, s.ressourceId, s.titreId,
				r.nom AS ressource,
				t.titre, t.revueId,
				JSON_OBJECTAGG(c.id, c.nom) AS collections,
				GREATEST(
					IF (
						t.dateDebut = '' OR s.dateBarrDebut = '',
						0,
						LEFT(t.dateDebut, 4) - LEFT(s.dateBarrDebut,4)
					),
					IF (
						t.dateFin = '',
						0,
						IF(s.dateBarrFin = '', LEFT(CURRENT_DATE(), 4), LEFT(s.dateBarrFin, 4)) - LEFT(t.dateFin,4)
					)
					) AS ecart
			FROM Service s
				JOIN Titre t ON s.titreId = t.id
				JOIN Ressource r ON s.ressourceId = r.id
				LEFT JOIN Service_Collection sc ON s.id = sc.serviceId
				LEFT JOIN Collection c ON sc.collectionId = c.id
			WHERE (t.dateDebut <> '' AND s.dateBarrDebut <> '' AND rpad(s.dateBarrDebut, 10, '-01') < t.dateDebut)
				OR (t.dateFin <> '' AND s.dateBarrFin = '')
				OR (t.dateFin <> '' AND s.dateBarrFin <> '' AND s.dateBarrFin > rpad(t.dateFin, 10, '-31-31'))
			GROUP BY s.id
			ORDER BY r.nom, t.titre, t.id, s.id
			SQL
			);
		$byResource = [];
		foreach ($cmd->query() as $row) {
			$ressourceId = (int) $row['ressourceId'];
			if (!isset($byResource[$ressourceId])) {
				$byResource[$ressourceId] = [
					'ressource' => $row['ressource'],
					'table' => [],
				];
			}
			$id = (int) $row['id'];
			$byResource[$ressourceId]['table'][$id] = [
				'titreId' => (int) $row['titreId'],
				'revueId' => (int) $row['revueId'],
				'titre' => \CHtml::link(\CHtml::encode($row['titre']), ['/titre/view', 'id' => $row['titreId']]),
				'serviceId' => (int) $row['id'],
				'ecart' => (int) $row['ecart'],
				'collections' => self::formatCollections($row['collections']),
			];
		}

		return $byResource;
	}

	private static function formatCollections(?string $cs): string
	{
		if (!$cs) {
			return '';
		}
		$collections = [];
		$dec = json_decode($cs, true);
		foreach ($dec as $cid => $cname) {
			$collections[] = sprintf('<abbr title="%s">%d</abbr>', \CHtml::encode($cname), $cid);
		}
		return join(" ", $collections);
	}
}
