<?php

namespace processes\verification;

class Liens
{
	/**
	 * @var array<string, string> Sourcelien.nom => Sourcelien.domain
	 */
	public array $domains;

	/**
	 * @var array<string, string> Sourcelien.nom => Sourcelien.domain
	 */
	public array $regexp;

	public array $urlsInvalidesEditeurs;

	public array $urlsInvalidesTitres;

	public array $nomsFauxEditeurs;

	public array $nomsFauxTitres;

	public function __construct()
	{
		$this->nomsFauxEditeurs= self::getNomsFaux('Editeur');
		$this->nomsFauxTitres = self::getNomsFaux('Titre');

		$this->urlsInvalidesEditeurs = array_merge(
			self::getDomainesFaux('Editeur'),
			self::getUrlDoesNotMatchPattern('Editeur')
		);
		$this->urlsInvalidesTitres = array_merge(
			self::getDomainesFaux('Titre'),
			self::getUrlDoesNotMatchPattern('Titre')
		);
		$this->domains = \components\SqlHelper::sqlToPairs("SELECT nom, url FROM Sourcelien WHERE url <> ''");
		$this->regexp = \components\SqlHelper::sqlToPairs("SELECT nom, urlRegex FROM Sourcelien WHERE urlRegex <> ''");
	}

	/**
	 * Records where the field `domain` does not end with the Sourcelien.domain value.
	 *
	 * @param "Editeur"|"Titre" $table
	 */
	private static function getDomainesFaux(string $table): array
	{
		$keyName = strtolower($table) . "Id";
		if ($table === 'Titre') {
			$columns = "t.id, t.revueId, t.titre, t.prefixe, t.sigle";
			$order = "t.titre";
		} else {
			$columns = "t.id, t.nom, t.prefixe, t.sigle";
			$order = "t.nom";
		}
		return \Yii::app()->db
			->createCommand(<<<EOSQL
				SELECT
				  $columns,
				  lt.url AS linkUrl,
				  s.url AS expectedDomain
				FROM Lien{$table} lt
				  JOIN Sourcelien s ON lt.sourceId = s.id
				  JOIN {$table} t ON lt.{$keyName} = t.id
				WHERE s.urlRegex = '' AND RIGHT(lt.`domain`, LENGTH(s.url)) <> s.url
				ORDER BY $order
				EOSQL
			)->queryAll();
	}

	/**
	 * @param "Editeur"|"Titre" $table
	 */
	private static function getNomsFaux(string $table): array
	{
		$keyName = strtolower($table) . "Id";
		if ($table === 'Titre') {
			$columns = "t.id, t.revueId, t.titre, t.prefixe, t.sigle";
			$order = "t.titre";
		} else {
			$columns = "t.id, t.nom, t.prefixe, t.sigle";
			$order = "t.nom";
		}
		return \Yii::app()->db
			->createCommand(<<<EOSQL
				SELECT
				  $columns,
				  lt.`name` AS linkName,
				  s.nom AS sourceName
				FROM Lien{$table} lt
				  JOIN Sourcelien s ON lt.sourceId = s.id
				  JOIN {$table} t ON lt.{$keyName} = t.id
				WHERE lt.`name` <> s.nom
				ORDER BY $order
				EOSQL
			)->queryAll();
	}

	/**
	 * Records where the field `url` does not match the `urlRegex` pattern.
	 *
	 * @param "Editeur"|"Titre" $table
	 */
	private static function getUrlDoesNotMatchPattern(string $table): array
	{
		$keyName = strtolower($table) . "Id";
		if ($table === 'Titre') {
			$columns = "t.id, t.revueId, t.titre, t.prefixe, t.sigle";
			$order = "t.titre";
		} else {
			$columns = "t.id, t.nom, t.prefixe, t.sigle";
			$order = "t.nom";
		}
		return \Yii::app()->db
			->createCommand(<<<EOSQL
				SELECT
				  $columns,
				  lt.url AS linkUrl,
				  REGEXP_REPLACE(lt.url, '^https?://([^/]+)/.*$', '\\\\1') AS domain,
				  s.nom AS sourceName
				FROM Lien{$table} lt
				  JOIN Sourcelien s ON lt.sourceId = s.id
				  JOIN {$table} t ON lt.{$keyName} = t.id
				WHERE s.urlRegex <> '' AND lt.url NOT RLIKE s.urlRegex
				ORDER BY $order
				EOSQL
			)->queryAll();
	}
}
