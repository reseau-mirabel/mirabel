<?php

namespace processes\partenaire;

class GrappeResult extends \Grappe
{
	public ?string $partenaireNom = null;

	/**
	 * @var bool In the Partenaire's selection?
	 */
	public $selected;

	/**
	 * @var int Number of Titre|Revue
	 */
	public $size;

	public function attributeLabels()
	{
		return parent::attributeNames() + [
			'partenaireNom' => "Gestionnaire",
			'selected' => "Sélectionnée",
			'size' => "Taille",
		];
	}
}
