<?php

namespace processes\partenaire;

use Config;
use Partenaire;
use Suivi;
use Yii;
use components\email\Mailer;

class Follow
{
	/**
	 * @var Suivi[]
	 */
	public array $existing = [];

	public Partenaire $partenaire;

	public Suivi $suivi;

	private array $redirection = [];

	public function __construct(Partenaire $p, string $target)
	{
		$this->partenaire = $p;

		$this->suivi = new Suivi();
		$this->suivi->partenaireId = (int) $p->id;
		$this->suivi->cible = $target;
		if (!in_array($target, ['Revue', 'Ressource', 'Editeur'])) {
			throw new \CHttpException(400, "L'argument 'target' n'est pas valide.");
		}
	}

	public function add(array $data): bool
	{
		if (!$data) {
			return false;
		}

		$added = 0;
		if (isset($data['cibleId'])) {
			$countToAdd = count($data['cibleId']);
			$cmd = Yii::app()->db->createCommand("INSERT IGNORE INTO Suivi (partenaireId, cible, cibleId) VALUES (:pid, :c, :cid)");
			foreach ($data['cibleId'] as $cibleId) {
				if ($cmd->execute([':pid' => $this->suivi->partenaireId, ':c' => $this->suivi->cible, ':cid' => (int) $cibleId]) > 0) {
					$added++;
					if ($this->suivi->cible === 'Revue') {
						Yii::app()->db
							->createCommand(<<<SQL
								INSERT IGNORE INTO IndexingRequiredTitre (id, updated, deleted)
								SELECT id, unix_timestamp(), 0 FROM Titre WHERE revueId = {$cibleId}
								SQL
							)
							->execute();
					}
				}
			}
		} else {
			$countToAdd = 0;
		}

		if ($added === $countToAdd) {
			$this->redirection = [
				'status' => 'success',
				'message' => "Suivi : $added ajout" . ($added > 1 ? 's' : ''),
			];
		} else {
			$this->redirection = [
				'status' => 'error',
				'message' => "Suivi : certains suivis étaient en doublons ($added ajouts / $countToAdd prévus).",
			];
		}
		return true;
	}

	public function addToPartenaireEditeur(array $data): bool
	{
		if (!$data) {
			return false;
		}

		$objects = [];
		/** @var \CActiveRecord $model */
		$model = call_user_func(["\\{$this->suivi->cible}", 'model']);
		foreach ($data['cibleId'] as $id) {
			$object = $model->findByPk($id);
			if ($object && isset($object->nom)) {
				$objects[] = $object->nom . " ({$object->getPrimaryKey()})";
			}
		}
		if (count($objects) === 1 && $model instanceof \Revue && $this->partenaire->editeurId) {
			$object = $model->findByPk((int) $data['cibleId'][0]);
			$sameEditeur = (boolean) Yii::app()->db
				->createCommand(
					"SELECT 1 FROM Titre t JOIN Titre_Editeur te ON te.titreId = t.id"
					. " WHERE te.editeurId = " . (int) $this->partenaire->editeurId . " AND t.revueId = " . (int) $object->id
				)->queryScalar();
			if ($sameEditeur) {
				$this->suivi->cibleId = (int) $object->id;
				if ($this->suivi->save()) {
					$this->redirection = [
						'status' => 'success',
						'message' => "Vous suivez désormais : " . $object->getSelfLink(),
					];
					return true;
				}
				$this->redirection = [
					'status' => 'error',
					'message' => "Erreur lors de l'enregistrement du suivi",
				];
				return true;
			}
		}

		$body = "Bonjour\nLe partenaire « {$this->partenaire->fullName} » de type {$this->partenaire->type} souhaite suivre les objets « {$this->suivi->cible} » suivant :\n - "
			. join("\n - ", $objects)
			. "\n\nPage du partenaire : " . Yii::app()->controller->createAbsoluteUrl('/partenaire/view', ['id' => $this->partenaire->id]);
		$destinataires = array_filter(
			Yii::app()->db->createCommand("SELECT email FROM Utilisateur WHERE suiviPartenairesEditeurs = 1")->queryColumn()
		);
		if (!$destinataires) {
			// contact
			$destinataires = array_filter(array_map('trim', explode("\n", Config::read('contact.email'))));
		}
		$message = Mailer::newMail()
			->subject(Yii::app()->name . " : demande de suivi pour un partenaire {$this->partenaire->type}")
			->from(Config::read('email.from'))
			->setTo($destinataires)
			->text($body);
		Mailer::sendMail($message);
		$this->redirection = [
			'status' => 'info',
			'message' => "Votre demande de suivi a été transmise aux administrateurs de Mir@bel.",
		];
		return true;
	}

	public function findExisting(): array
	{
		$existingAll = $this->partenaire->getRightsSuivi();
		if (empty($existingAll[$this->suivi->cible])) {
			return [];
		}
		$model = call_user_func(["\\{$this->suivi->cible}", 'model']);
		return $model->sorted()->findAllByPk($existingAll[$this->suivi->cible]);
	}

	public function getRedirection(): array
	{
		return $this->redirection;
	}

	public function updateExisting(array $data): bool
	{
		if (!$data) {
			return false;
		}

		$deleted = 0;
		foreach ($data as $id => $on) {
			if (!$on) {
				$deleted += Suivi::model()
					->deleteAllByAttributes(
						['partenaireId' => $this->partenaire->id, 'cible' => $this->suivi->cible, 'cibleId' => (int) $id]
					);
				if ($this->suivi->cible === 'Revue') {
					// Hook to refresh the Manticore index of these Titre records.
					Yii::app()->db
						->createCommand(<<<SQL
							INSERT IGNORE INTO IndexingRequiredTitre (id, updated, deleted)
							SELECT id, unix_timestamp(), 0 FROM Titre WHERE revueId = {$id}
							SQL
						)
						->execute();
				}
			}
		}
		if ($deleted) {
			$this->redirection = [
				'status' => 'success',
				'message' => "Suivi : $deleted suppression" . ($deleted > 1 ? 's' : ''),
			];
		} else {
			$this->redirection = [
				'status' => 'info',
				'message' => "Suivi : aucune suppression",
			];
		}
		return true;
	}
}
