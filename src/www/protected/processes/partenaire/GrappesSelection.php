<?php

namespace processes\partenaire;

class GrappesSelection extends \CFormModel
{
	public ?string $nom = null;

	public $add = [];

	private ?int $partenaireId;

	public function __construct(?\Partenaire $p = null)
	{
		parent::__construct('');
		$this->partenaireId = ($p === null ? null : (int) $p->id);
	}

	public function rules()
	{
		return [
			['nom', 'length', 'max' => 255],
			['add', 'ext.validators.ArrayOfIntValidator'],
		];
	}

	public function attributeLabels()
	{
		return [
			'nom' => 'Chercher',
		];
	}

	public function load(array $data): bool
	{
		if (!$data) {
			return false;
		}
		$this->setAttributes($data);
		return $this->validate();
	}

	public function addGrappesToSelection(): bool
	{
		if (!$this->add) {
			return false;
		}
		$inserts = [];
		$now = time();
		$position = 1;
		foreach ($this->add as $id) {
			$inserts[] = sprintf("(%s, %d, %d, %d, %d)",
				$this->partenaireId ?? "NULL",
				$id,
				$this->partenaireId ? 0 : 4, // page: for a Partenaire, not visible unless assigned to a page
				$position,
				$now // timestamp
			);
			$position++;
		}
		$sql = "INSERT INTO Partenaire_Grappe (partenaireId, grappeId, pages, position, hdateCreation) VALUES "
			. join(", ", $inserts)
			. " ON DUPLICATE KEY UPDATE position = values(position)";
		return (bool) \Yii::app()->db->createCommand($sql)->execute();
	}

	/**
	 * Each result is an instance of GrappeResult.
	 */
	public function search(): \CActiveDataProvider
	{
		$criteria = new \CDbCriteria;
		$criteria->with = 'partenaire';
		$criteria->join = <<<SQL
			JOIN Grappe_Titre gt ON gt.grappeId = t.id
			JOIN Titre ON gt.titreId = Titre.id
			LEFT JOIN Partenaire_Grappe pg ON pg.partenaireId <=> :pid AND pg.grappeId = t.id
			LEFT JOIN Partenaire p ON pg.partenaireId = p.id
			SQL;
		$criteria->params[':pid'] = $this->partenaireId;
		$criteria->group = "t.id";
		$criteria->select = [
			't.*',
			"IF(p.sigle ='', p.nom, p.sigle) AS partenaireNom",
			"pg.grappeId IS NOT NULL AS selected",
			"IF(t.niveau = 1, count(*), count(DISTINCT Titre.revueId)) AS size",
		];
		$criteria->addCondition("t.diffusion = " . \Grappe::DIFFUSION_PARTAGE);
		if ($this->partenaireId > 0) {
			// Grappes étrangères et partagées (les grappes de ce partenaires sont déjà dans sa liste)
			$criteria->addCondition("NOT(t.partenaireId <=> {$this->partenaireId})");
		}

		// Apply the filters.
		$nom = trim($this->nom);
		if ($nom && $nom !== '*') {
			if (ctype_digit($nom)) {
				$criteria->addCondition("t.id = {$nom} OR t.nom RLIKE '\\\\b{$nom}\\\\b'");
			} elseif ($nom[0] === '=') {
				$criteria->addColumnCondition(['t.nom' => substr($nom, 1)]);
			} elseif (strpos($nom, " ") > 0) {
				$words = preg_split('/[,;\s]+/', $nom);
				foreach ($words as $word) {
					$criteria->addSearchCondition('t.nom', $word);
				}
			} else {
				$criteria->addSearchCondition('t.nom', $nom);
			}
		}

		$sort = new \CSort();
		$sort->defaultOrder = ['t.nom' => \CSort::SORT_ASC];
		$sort->attributes = [
			'id' => [
				'asc' => 't.id ASC',
				'desc' => 't.id DESC',
			],
			'nom' => [
				'asc' => 't.nom ASC',
				'desc' => 't.nom DESC',
			],
			'partenaireNom' => [
				'asc' => 'partenaireNom ASC, t.nom ASC',
				'desc' => 'partenaireNom DESC, t.nom ASC',
			],
		];
		return new \CActiveDataProvider(
			GrappeResult::model(),
			[
				'criteria' => $criteria,
				'pagination' => ['pageSize' => 20],
				'sort' => $sort,
			]
		);
	}
}
