<?php

namespace processes\partenaire;

use Grappe;
use Partenaire;
use Yii;

/**
 * Each instance represents a relation between Partenaire and Grappe.
 */
class GrappeAffichage extends \CFormModel
{
	public const PAGE_PARTENAIRE = 1;

	public const PAGE_SELECTION = 2;

	/**
	 * @var int Cf \Grappe::DIFFUSION_*
	 */
	public $diffusion = Grappe::DIFFUSION_PARTENAIRE;

	/**
	 * @var bool Page partenaire?
	 */
	public $pp = false;

	/**
	 * @var bool Page sélection?
	 */
	public $ps = false;

	public readonly int $grappeId;

	public readonly string $grappeName;

	public readonly int $grappePartenaireId;

	private ?int $partenaireId;

	private function __construct(?Partenaire $p, int $grappeId, int $grappePartenaireId, string $name)
	{
		parent::__construct('');
		$this->partenaireId = ($p === null ? null : (int) $p->id);
		$this->grappeId = $grappeId;
		$this->grappeName = $name;
		$this->grappePartenaireId = $grappePartenaireId;
	}

	public function rules()
	{
		return [
			['diffusion', 'numerical', 'integerOnly' => true, 'min' => 0, 'max' => Grappe::DIFFUSION_PARTENAIRE],
			[['pp', 'ps'], 'boolean'],
		];
	}

	public function getPartenaireCourt(): string
	{
		if (!$this->grappePartenaireId) {
			return 'Mir@bel';
		}
		$partenaire = Partenaire::model()->findByPk($this->grappePartenaireId);
		if (!($partenaire instanceof Partenaire)) {
			return '???';
		}
		return $partenaire->getShortName();
	}

	/**
	 * Return a list of relations, read from the DB table "Partenaire_Grappe".
	 *
	 * @return array<int, self> Indexed by Grappe.id
	 */
	public static function loadFromDb(?Partenaire $partenaire): array
	{
		$records = Yii::app()->db
			->createCommand(<<<SQL
				SELECT g.nom, g.partenaireId AS grappePartenaireId, g.diffusion AS diffusion, pg.*
				FROM Partenaire_Grappe pg
					JOIN Grappe g ON pg.grappeId = g.id
				WHERE pg.partenaireId <=> :pid
				ORDER BY position ASC, g.nom ASC
				SQL
			);
		$results = [];
		foreach ($records->query([':pid' => $partenaire?->id]) as $record) {
			$id = (int) $record['grappeId'];
			$relation = new self($partenaire, $id, (int) $record['grappePartenaireId'], $record['nom']);
			$relation->diffusion = (int) $record['diffusion'];
			if (in_array($relation->diffusion, [Grappe::DIFFUSION_PARTAGE, Grappe::DIFFUSION_PUBLIC], true)) {
				$relation->pp = (bool) ((int) $record['pages'] & self::PAGE_PARTENAIRE);
				$relation->ps = (bool) ((int) $record['pages'] & self::PAGE_SELECTION);
			}
			$results[$id] = $relation;
		}
		return $results;
	}

	public function write(int $position): void
	{
		if ($this->partenaireId !== $this->grappePartenaireId && (int) $this->diffusion !== Grappe::DIFFUSION_PARTAGE) {
			$this->pp = false;
			$this->ps = false;
		}
		Yii::app()->db
			->createCommand("UPDATE Partenaire_Grappe SET pages = :p, position = {$position} WHERE partenaireId <=> :pid AND grappeId = :gid")
			->execute([
				':p' => ($this->pp ? self::PAGE_PARTENAIRE : 0) + ($this->ps ? self::PAGE_SELECTION : 0),
				':pid' => $this->partenaireId,
				':gid' => $this->grappeId,
			]);
		if ($this->partenaireId === $this->grappePartenaireId) {
			Yii::app()->db
				->createCommand("UPDATE Grappe SET diffusion = {$this->diffusion} WHERE id = {$this->grappeId}")
				->execute();
		}
	}
}
