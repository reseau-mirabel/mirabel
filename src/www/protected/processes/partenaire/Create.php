<?php

namespace processes\partenaire;

use Partenaire;
use Suivi;

class Create
{
	/**
	 * @var Partenaire
	 */
	public $model;

	/**
	 * @var array
	 */
	private $redirection = [];

	public function __construct()
	{
		$this->model = new Partenaire();
	}

	public function getRedirection(): array
	{
		return $this->redirection;
	}

	public function load(array $data): bool
	{
		if (!$data) {
			return false;
		}
		$this->model->setAttributes($data);
		return $this->model->validate();
	}

	public function save(): bool
	{
		if (!$this->model->save(false)) {
			return false;
		}
		if ($this->model->editeurId > 0) {
			$this->partenaireEditeurSuivi($this->model);
		}
		$message = "<p>Le partenaire a été créé.</p>
	<ul>
	<li>Vérifiez les informations ci-dessous.</li>
	<li>L'adresse électronique de contact de ce partenaire est celle qui sera utilisée pour l'envoi des alertes de propositions de modifications.</li>
	<li>Ajoutez au moins un utilisateur principal, administrateur de ce partenaire (cf menu latéral).</li>
	<li>Tous les utilisateurs actifs seront automatiquement abonnés à la liste de diffusion partenaire (veilleur ou éditeur).</li>
	<li>Vous devez également ajouter le logo du partenaire (cf menu latéral).</li>
	</ul>
	";
		$this->redirection = [
			'status' => 'success',
			'message' => $message,
			'url' => ['view', 'id' => $this->model->id],
		];
		return true;
	}

	public function setEditeur(array $editeurPartenaire): void
	{
		$this->model->scenario = 'partenaire-editeur';
		$this->model->setAttributes($editeurPartenaire);
		$this->model->editeurId = (int) $editeurPartenaire['editeurId'];
	}

	private function partenaireEditeurSuivi(Partenaire $model)
	{
		$defined = Suivi::model()->findByAttributes(['partenaireId' => $model->id, 'cible' => 'Editeur', 'cibleId' => $model->editeurId]);
		if (!$defined) {
			$suivi = new Suivi();
			$suivi->attributes = ['partenaireId' => $model->id, 'cible' => 'Editeur', 'cibleId' => $model->editeurId];
			$suivi->save(false);
		}
	}
}
