<?php

namespace processes\partenaire;

use Partenaire;
use Possession;
use SearchNavigation;
use Yii;

class Owning
{
	/**
	 * @var Possession[]
	 */
	public $existing = [];

	/**
	 * @var Partenaire
	 */
	public $partenaire;

	/**
	 * @var Possession
	 */
	public $possession;

	/**
	 * @var SearchNavigation
	 */
	public $searchNavRevues;

	/**
	 * @var SearchNavigation
	 */
	public $searchNavTitres;

	/**
	 * @var array
	 */
	private $redirection = [];

	public function __construct(Partenaire $p)
	{
		$this->partenaire = $p;

		$this->possession = new Possession();
		$this->possession->partenaireId = (int) $p->id;
	}

	public function add(array $data): bool
	{
		if (!$data) {
			return false;
		}
		$this->possession->setAttributes($data);
		if (empty($this->possession->titreId)) {
			$t = (new \processes\completion\Titre)->findExactTerm($data['titreIdComplete']);
			if ($t && count($t) === 1) {
				$this->possession->titreId = $t[0];
			}
		}
		if (!empty($data['nosave'])) {
			return false;
		}
		$code = $this->possession->importAdd(false);
		$this->redirection = [
			'status' => ($code >= Possession::IMPORT_NOTFOUND) ? 'error' : 'success',
			'message' => "Possession : " . Possession::getMsgFromCode($code),
			'url' => ['possession', 'id' => $this->partenaire->id],
		];
		return true;
	}

	public function findExisting(string $filter): void
	{
		$criteria = ['order' => 'titre.titre'];
		if ($filter) {
			if ($filter === 'bouquet') {
				$this->existing = Possession::groupByBouquet((int) $this->partenaire->id);
			} elseif ($filter === 'identifiant') {
				$criteria =  ['order' => 't.identifiantLocal'];
			} elseif ($filter === 'idnum') {
				$criteria =  ['order' => 'CAST(t.identifiantLocal AS UNSIGNED INTEGER)'];
			}
		}
		if (!$this->existing) {
			$this->existing = Possession::model()->with('titre')
				->findAllByAttributes(['partenaireId' => $this->partenaire->id], $criteria);
		}
	}

	public function getRedirection(): array
	{
		return $this->redirection;
	}

	public function getSearchNav(): array
	{
		$revues = [];
		$titres = [];
		foreach ($this->existing as $e) {
			$titres[] = (object) ['id' => $e->titreId, 'titre' => $e->titre->titre];
			$revues[] = (object) ['id' => $e->titre->revueId, 'titrecomplet' => $e->titre->titre];
		}

		$r = new SearchNavigation();
		$r->save(
			'partenaire/possession',
			['id' => $this->partenaire->id],
			'id',
			$revues
		);

		$t = new SearchNavigation();
		$t->save(
			'partenaire/possession',
			['id' => $this->partenaire->id],
			'id',
			$titres
		);

		return ['revues' => $r, 'titres' => $t];
	}

	public function updateExisting(array $data): bool
	{
		if (!$data) {
			return false;
		}
		$ids = [];
		foreach ($data as $tid => $on) {
			$tid = ltrim($tid, 't');
			if ($on && $tid > 0) {
				$ids[] = (int) $tid;
			}
		}
		if ($ids) {
			$deleted = Yii::app()->db
				->createCommand("DELETE FROM Partenaire_Titre WHERE partenaireId = :pid AND titreId IN (" . join(',', $ids) . ")")
				->execute([':pid' => $this->partenaire->id]);
			$this->redirection = [
				'status' => 'success',
				'message' => "Possessions : $deleted suppression" . ($deleted > 1 ? 's' : '') . " pour " . count($ids) . " désélections.",
				'url' => ['possession', 'id' => $this->partenaire->id],
			];
		} else {
			$this->redirection = [
				'status' => 'info',
				'message' => "Possesssions : aucune suppression",
				'url' => ['possession', 'id' => $this->partenaire->id],
			];
		}
		return true;
	}
}
