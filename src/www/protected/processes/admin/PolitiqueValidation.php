<?php

namespace processes\admin;

use components\SqlHelper;
use Editeur;
use Utilisateur;
use processes\politique\Workflow;

class PolitiqueValidation
{
	public const GHOST_DAYS = 3;

	/**
	 * @var array
	 */
	public $ghostUsers;

	/**
	 * @var Utilisateur[] Indexed by id.
	 */
	public $pendingUsers;

	/**
	 * @var array Array of {utilisateurId, editeur}.
	 */
	public $pendingRoles = [];

	public function __construct()
	{
		$ghostTime = time() - self::GHOST_DAYS*86400;
		$this->ghostUsers = \Yii::app()->db
			->createCommand(
				<<<EOSQL
				SELECT t.* FROM (
					-- old user that never connected
					SELECT
						u.id, u.nom, u.prenom, u.nomComplet, u.email, u.actif, u.derConnexion, u.hdateCreation, u.hdateModif,
						GROUP_CONCAT(e.nom) AS editeurs
					FROM Utilisateur u
						JOIN Utilisateur_Editeur ue ON ue.utilisateurId = u.id
						JOIN Editeur e ON ue.editeurId = e.id
					WHERE partenaireId IS NULL AND derConnexion IS NULL AND hdateCreation < $ghostTime
					GROUP BY u.id
					UNION
					-- user with no Partenaire and no Editeur
					SELECT u.id, u.nom, u.prenom, u.nomComplet, u.email, u.actif, u.derConnexion, u.hdateCreation, u.hdateModif, ''
					FROM Utilisateur u
						LEFT JOIN Utilisateur_Editeur ue ON ue.utilisateurId = u.id
					WHERE partenaireId IS NULL AND ue.editeurId IS NULL
				) t
				ORDER BY nom, prenom, nomComplet
				EOSQL
			)->queryAll();

		$this->pendingUsers = SqlHelper::sqlToPairsObject(
			<<<EOSQL
			SELECT u.*
			FROM Utilisateur u
				JOIN Utilisateur_Editeur ue ON ue.utilisateurId = u.id AND ue.confirmed = 0
			WHERE u.actif = 1
			GROUP BY u.id
			ORDER BY u.nom, u.prenom, u.nomComplet, ue.lastUpdate
			EOSQL,
			Utilisateur::class
		);

		$this->fillPendingRoles();
	}

	public static function deleteRole(int $uid, int $eid): array
	{
		$transaction = \Yii::app()->db->beginTransaction();
		try {
			$changes = \Yii::app()->db
				->createCommand("DELETE FROM Utilisateur_Editeur WHERE utilisateurId = :uid AND editeurId = :eid LIMIT 1")
				->execute([':uid' => $uid, ':eid' => $eid]);
			if ($changes > 0) {
				\Yii::app()->db
					->createCommand("DELETE FROM Politique WHERE utilisateurId = :uid AND editeurId = :eid")
					->execute([':uid' => $uid, ':eid' => $eid]);
				\Yii::app()->db
					->createCommand("DELETE u FROM Utilisateur u LEFT JOIN Utilisateur_Editeur ue ON ue.utilisateurId = u.id WHERE partenaireId IS NULL AND ue.editeurId IS NULL")
					->execute();
				$transaction->commit();
				return ['success' => true];
			}
			$transaction->commit();
			return ['success' => false, 'message' => "L'élément a déjà été supprimé."];
		} catch (\Exception $e) {
			$transaction->rollback();
			\Yii::log($e->getMessage(), 'error');
			return ['success' => false, 'message' => "Erreur de base de données. " . (YII_DEBUG ? $e->getMessage() : '')];
		}
	}

	public static function setRole(string $name, int $uid, int $eid): array
	{
		if (!in_array($name, [\Politique::ROLE_GUEST, \Politique::ROLE_OWNER])) {
			return ['message' => "rôle non valide"];
		}

		// Set the user role.
		try {
			\Yii::app()->db
				->createCommand("UPDATE Utilisateur_Editeur SET role = :r, confirmed = 1 WHERE utilisateurId = :uid AND editeurId = :eid")
				->execute([':r' => $name, ':uid' => $uid, ':eid' => $eid]);
		} catch (\Exception $e) {
			\Yii::log($e->getMessage(), 'error');
			return ['message' => "Erreur de base de données. " . (YII_DEBUG ? $e->getMessage() : '')];
		}

		// Add the pending links Politique_Titre.
		$actorRole = \Yii::app()->user->access()->toPolitique()->getRole($eid);
		$politiques = \Politique::model()->findAllByAttributes(['utilisateurId' => $uid, 'editeurId' => $eid]);
		foreach ($politiques as $p) {
			/** @var \Politique $p */
			if ($p->status !== \Politique::STATUS_PENDING) {
				continue;
			}
			if (!(new Workflow($p, $actorRole))->confirmUser()) {
				throw new \Exception("Vous n'avez pas la permission requise pour valider ce compte.");
			}
			$p->save(false);

			if ($p->initialTitreId > 0) {
				try {
					\Yii::app()->db
						->createCommand("REPLACE INTO Politique_Titre (politiqueId, titreId, confirmed, createdOn) VALUES (:p, :t, 0, :now)")
						->execute([
							':p' => (int) $p->id,
							':t' => (int) $p->initialTitreId,
							':now' => time(),
						]);
				} catch (\Throwable $_) {
				}
			}
		}
		return ['success' => true];
	}

	private function fillPendingRoles(): void
	{
		$query = \Yii::app()->db
			->createCommand(
				<<<EOSQL
				SELECT
					e.*,
					ue.utilisateurId,
					count(p.id) AS politiques,
					ue.role AS userRole,
					Pays.nom AS pays,
					pt.politiqueId AS overId,
					ue.lastUpdate
				FROM Utilisateur_Editeur ue
					JOIN Editeur e ON ue.editeurId = e.id
					LEFT JOIN Pays ON e.paysId = Pays.id
					LEFT JOIN Politique p ON (p.editeurId, p.utilisateurId) = (ue.editeurId, ue.utilisateurId)
					LEFT JOIN Politique_Titre pt ON pt.titreId = p.initialTitreId
					JOIN Utilisateur u ON ue.utilisateurId = u.id AND u.actif = 1
				WHERE ue.confirmed = 0
				GROUP BY ue.editeurId, ue.utilisateurId
				ORDER BY ue.utilisateurId, e.nom
				EOSQL
			)->query();
		$this->pendingRoles = [];
		foreach ($query as $row) {
			$uId = (int) $row['utilisateurId'];
			unset($row['utilisateurId']);

			$politiques = (int) $row['politiques'];
			unset($row['politiques']);

			$overwrites = [];
			if ($row['overId']) {
				$overwrites = \Yii::app()->db
					->createCommand(
						<<<EOSQL
						SELECT
							p2.id AS id,
							p2.name AS name,
							t.id AS titreId,
							t.titre AS titreName
						FROM
							Politique p
							JOIN Politique_Titre pt ON pt.titreId = p.initialTitreId
							JOIN Politique p2 ON pt.politiqueId = p2.id
							JOIN Titre t ON t.id = p.initialTitreId
						WHERE p.utilisateurId = :uid AND p1.id <> p2.id
						ORDER BY t.titre
						EOSQL
					)
					->queryAll(true, [':uid' => $uId]);
			}

			$e = Editeur::model()->populateRecord($row);

			$this->pendingRoles[] = [
				'utilisateurId' => $uId,
				'politiques' => $politiques,
				'role' => $row['userRole'],
				'pays' => $row['pays'],
				'editeur' => $e,
				'overwrites' => $overwrites,
				'lastUpdate' => $row['lastUpdate'],
			];
		}
	}
}
