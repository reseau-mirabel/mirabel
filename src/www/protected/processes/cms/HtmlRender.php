<?php

namespace processes\cms;

use Yii;

class HtmlRender
{
	/**
	 * Returns the HTML listing the last validated interventions.
	 *
	 * @return string HTML list.
	 */
	public static function listLastInterventions(int $num): string
	{
		$limit = (int) $num;
		if (!$limit) {
			return '';
		}
		$floorTime = mktime(0, 0, 0) - 5184000; // 60 last days
		$list = \Intervention::model()->findAllBySql(<<<EOSQL
			WITH IntAcceptRevue AS (
				SELECT titreId, editeurId, ressourceId, max(hdateVal) as hdateVal
					FROM Intervention i
					WHERE i.statut = 'accepté'
						AND (i.action IS NULL OR i.action <> 'revue-C')
						AND hdateVal > $floorTime
					GROUP BY titreId, editeurId, ressourceId
					ORDER BY hdateVal DESC
					LIMIT $limit
			)
			SELECT i.*
			FROM Intervention i
				JOIN IntAcceptRevue AS t
					ON (i.titreId <=> t.titreId AND i.editeurId <=> t.editeurId AND i.ressourceId <=> t.ressourceId AND i.hdateVal = t.hdateVal)
			WHERE i.statut = 'accepté'
			ORDER BY i.hdateVal DESC, id DESC
			EOSQL
		);
		if (!$list) {
			$titles = ['<li>Aucune modification</li>'];
		} else {
			$avecPartenaire = Yii::app()->user->checkAccess("avec-partenaire");
			$titles = [];
			foreach ($list as $i) {
				$titles[] = '<li class="' . ($i->suivi ? 'suivi-other' : 'suivi-none') . '">'
					. $i->render()->link($avecPartenaire)
					. "</li>\n";
			}
		}
		return '<ul>' . join('', $titles) . '</ul>';
	}

	/**
	 * Returns the HTML listing the last validated interventions that created a Revue.
	 *
	 * @return string HTML list.
	 */
	public static function listLastJournalCreations(int $num): string
	{
		$limit = (int) $num;
		if (!$limit) {
			return '';
		}
		$floorTime = mktime(0, 0, 0) - 10368000; // 120 last days
		$list = \Titre::model()->findAllBySql(<<<EOSQL
			SELECT t.*, i.suivi AS suivre
			FROM Intervention i
				JOIN Titre t ON t.revueId = i.revueId AND t.obsoletePar IS NULL
			WHERE i.statut = 'accepté'
				AND i.action <=> 'revue-C'
				AND i.hdateVal > $floorTime
			GROUP BY i.titreId
			ORDER BY i.hdateVal DESC
			LIMIT $limit
			EOSQL
		);
		if (!$list) {
			$titles = ['Aucune création de revue'];
		} else {
			$titles = [];
			foreach ($list as $t) {
				$titles[] = '<li class="' . ($t->suivre ? 'suivi-other' : 'suivi-none') . '">'
					. $t->getSelfLink()
					. "</li>\n";
			}
		}
		return '<ul>' . join('', $titles) . '</ul>';
	}
}
