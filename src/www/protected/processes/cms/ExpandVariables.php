<?php

namespace processes\cms;

use CHtml;
use CMarkdown;
use Cms;
use components\info\Composer;
use components\SqlHelper;
use Partenaire;
use Yii;

class ExpandVariables
{
	private const STR_TO_SQL = [
		'%NB_COMPTES_TYPE_PARTENAIRES%' => <<<EOSQL
			SELECT count(*)
			FROM Utilisateur u
				JOIN Partenaire p ON p.id = u.partenaireId
			WHERE p.type='normal' AND p.statut IN ('actif', 'provisoire') AND u.actif = 1
			EOSQL,
		'%NB_COMPTES_TYPE_EDITEUR%' => <<<EOSQL
			SELECT count(*)
			FROM Utilisateur u
				JOIN Partenaire p ON p.id = u.partenaireId
			WHERE p.type='editeur' AND p.statut IN ('actif', 'provisoire') AND u.actif = 1
			EOSQL,
		'%NB_COMPTES_TYPE_TOUT%' => <<<EOSQL
			SELECT count(*)
			FROM Utilisateur u
				JOIN Partenaire p ON p.id = u.partenaireId
			WHERE p.type IN ('normal', 'editeur') AND p.statut IN ('actif', 'provisoire') AND u.actif = 1
			EOSQL,
		'%NB_EDITEURS%' => "SELECT count(*) FROM Editeur",
		'%NB_ISSN%' => <<<EOSQL
			SELECT COUNT(DISTINCT issnw)
			FROM (
				SELECT issn AS issnw FROM Issn WHERE statut = 0
				UNION
				SELECT issnl AS issnw FROM Issn WHERE statut = 0
				) IM
			EOSQL,
		'%NB_PARTENAIRES%' => "SELECT count(*) FROM Partenaire WHERE type = 'normal' AND statut = 'actif'",
		'%NB_PARTENAIRES_STATUT_INACTIF%' => "SELECT count(*) FROM Partenaire WHERE statut != 'actif'",
		'%NB_PARTENAIRES_STATUT_PROVISOIRE%' => "SELECT count(*) FROM Partenaire WHERE statut = 'provisoire'",
		'%NB_PARTENAIRES_TOUT%' => "SELECT count(*) FROM Partenaire",
		'%NB_PARTENAIRES_TYPE_EDITEUR%' => "SELECT count(*) FROM Partenaire WHERE type = 'editeur' AND statut = 'actif'",
		'%NB_PARTENAIRES_TYPE_IMPORT%' => "SELECT count(*) FROM Partenaire WHERE type = 'import' AND statut = 'actif'",
		'%NB_PARTENAIRES_TYPE_TOUT%' => "SELECT count(*) FROM Partenaire WHERE statut = 'actif'",
		'%NB_RESSOURCES%' => "SELECT count(*) FROM Ressource",
		'%NB_RESSOURCES_IMPORT%' => "SELECT count(DISTINCT ressourceId) FROM Service WHERE import > 0",
		'%NB_RESSOURCES_SUIVIES%' => "SELECT count(DISTINCT s.cibleId) FROM Suivi s JOIN Revue r ON s.cibleId = r.id WHERE s.cible = 'Ressource'",
		'%NB_REVUES%' => "SELECT count(DISTINCT revueId) FROM Titre",
		'%NB_REVUES_IMPORT%' => "SELECT count(DISTINCT t.revueId) FROM Service s JOIN Titre t ON s.titreId = t.id WHERE s.import > 0",
		'%NB_REVUES_IMPORT_AUTOMATISE%' => <<<EOSQL
			SELECT count(DISTINCT t.revueId)
			FROM Service s
				JOIN Titre t ON s.titreId = t.id
				JOIN Ressource r ON s.ressourceId = r.id
				LEFT JOIN Service_Collection sc ON sc.serviceId = s.id
				LEFT JOIN Collection c ON sc.collectionId = c.id
			WHERE s.import > 0 AND (r.autoImport + c.importee) > 0
			EOSQL,
		'%NB_REVUES_SUIVIES%' => "SELECT count(DISTINCT s.cibleId) FROM Suivi s JOIN Revue r ON s.cibleId = r.id WHERE s.cible = 'Revue'",
		'%NB_TITRES%' => "SELECT count(*) FROM Titre",
	];

	private const STR_TO_FUNCTION = [
		'%ABCD_REVUES%' => 'listAbcdRevues',
		'%BIBLIOS_LOGICIELLES%' => 'listBiblios',
		'%DATE_DER_MAJ%' => 'dateDerMaj',
		'%NB_LIENS%' => 'countLinks',
		'%RATIO_LIENS%' => 'getRatioLiens',
		'%RECHERCHE%' => 'displaySearchForm',
	];

	private const PATTERN_TO_FUNCTION = [
		'/%DER_ACTU_(\d+)%/' => 'getNews', // compat
		'/%DER_ACTU\((\d+)\)%/' => 'getNews',
		'/%DER_ACTU\((\d+),\s*(\{.+?\})\)%/' => 'getNews',
		'/%DER_ACTU_TITRES_(\d+)%/' => 'getNewsTitles', // compat
		'/%DER_ACTU_TITRES\((\d+)\)%/' => 'getNewsTitles',
		'/%DER_ACTU_TITRES\((\d+),\s*(\{.+?\})\)%/' => 'getNewsTitles',
		'/%DER_CREA_(\d+)%/' => 'listLastJournalCreations', // compat
		'/%DER_CREA\((\d+)\)%/' => 'listLastJournalCreations',
		'/%DER_MAJ_(\d+)%/' => 'listLastInterventions', // compat
		'/%DER_MAJ\((\d+)\)%/' => 'listLastInterventions',
	];

	private bool $authentified;

	private ?Partenaire $partenaire = null;

	public function __construct(?Partenaire $partenaire = null)
	{
		$this->setPartenaire($partenaire);
	}

	public function setPartenaire(?Partenaire $p): void
	{
		$this->authentified = ($p !== null);
		$this->partenaire = $p;
	}

	public function expand(string $text): string
	{
		if (!preg_match('/%[A-Z_]{3}.*%/', $text)) {
			return $text;
		}
		return $this->expandHtml($this->expandNumbers($text));
	}

	public function expandNumbers(string $text): string
	{
		$strings = [];

		$db = Yii::app()->db;
		foreach (self::STR_TO_SQL as $k => $v) {
			if (strpos($text, $k) !== false) {
				$strings[$k] = $db->createCommand($v)->queryScalar();
			}
		}

		// Special cases

		if ($this->partenaire) {
			$strings['%PARTENAIRE.ID%'] = (string) $this->partenaire->id;
		} else {
			$strings['%PARTENAIRE.ID%'] = '';
		}

		return str_replace(array_keys($strings), array_values($strings), $text);
	}

	public function expandHtml(string $text): string
	{
		$strings = [];

		foreach (self::STR_TO_FUNCTION as $k => $func) {
			if (strpos($text, $k) !== false) {
				$strings[$k] = self::{$func}();
			}
		}

		$matches = [];
		foreach (self::PATTERN_TO_FUNCTION as $p => $f) {
			if (preg_match_all($p, $text, $matches, PREG_SET_ORDER) > 0) {
				foreach ($matches as $m) {
					$strings[$m[0]] = self::{$f}($m);
				}
			}
		}

		// Special cases

		if ($this->partenaire) {
			$strings['%PARTENAIRE%'] = (string) $this->partenaire->nom;
		} else {
			$strings['%PARTENAIRE%'] = '';
		}

		if (strpos($text, '%SI_CONNECTE_DEB%') !== false) {
			if ($this->authentified) {
				$text = str_replace(['%SI_CONNECTE_DEB%', '%SI_CONNECTE_FIN%'], '', $text);
			} else {
				$text = preg_replace('/%SI_CONNECTE_DEB%.+?%SI_CONNECTE_FIN%/s', '', $text);
			}
		}

		return str_replace(array_keys($strings), array_values($strings), $text);
	}

	/**
	 * Returns the HTML for the titles of the last $num news.
	 *
	 * @param Cms[] $blocks
	 * @return string HTML.
	 */
	public static function formatNewsTitles(array $blocks): string
	{
		$html = '';
		foreach ($blocks as $block) {
			[$date, $image, $title] = self::extractTitle($block);
			$url = CHtml::normalizeUrl(['site/page', 'p' => 'actualite']) . "#actu{$block->id}";
			$html .= "<tr id=\"actu{$block->id}\">"
				. '<td class="image">' . ($image ? CHtml::link($image, $url) : '') . '</td>'
				. '<td>'
				. ($date ? "<div class=\"date\">$date</div> " : '')
				. '<div class="actu-titre">' . CHtml::link($title, $url) . "</div>"
				. "</td>"
				. "</tr>\n";
		}
		return "<table class=\"breves-titres\">$html</table>";
	}

	/**
	 * Extract info from a block, notably a "Brève".
	 *
	 * @return array [$date, $image, $title]
	 */
	protected static function extractTitle(Cms $cms): array
	{
		$date = '';
		$image = '';
		switch ($cms->type) {
			case 'text':
				do {
					$first = strtok($cms->content, "\n");
				} while (trim($first) === '');
				$title = CHtml::encode($first);
				break;
			case 'html':
				/**
				 * @todo parse the DOM instead of regexps ?
				 */
				$m = [];
				if (preg_match('/<h(\d)[^>]*>(.+?)<\1>/', $cms->content, $m)) {
					$title = $m[2];
				} elseif (preg_match('/<(b|strong)[^>]*>(.+?)<\1>/', $cms->content, $m)) {
					$title = $m[2];
				} else {
					$title = $cms->content;
				}
				break;
			case 'markdown':
			default:
				$md = new CMarkdown();
				$m = [];
				if (preg_match('/(?:^|\n)([^\n]+)\n(?:={3,}|-{3,})/', $cms->content, $m)) {
					$title = $md->transform($m[1]);
				} elseif (preg_match('/(?:^|\n)#+\s+(.+?)/', $cms->content, $m)) {
					$title = $md->transform($m[1]);
				} elseif (preg_match('/^\|\s*([^|]+?)\s*\|\s*([^|]+?)\s*\|/', $cms->content, $m)) {
					$date = $m[1];
					$title = strip_tags($md->transform($m[2]), '<b><i><em><strong><br>');
					if (preg_match('/\n\s*\|\s*!\[(.+?)\]\(([^)]+?)\)\s*\|/', $cms->content, $m)) {
						// image seule
						$image = CHtml::image(Yii::app()->baseUrl . $m[2], $m[1]);
					} elseif (preg_match('/\n\s*\|\s*\[\s*!\[(.+?)\]\(([^)]+?)\)\s*\]\s*\(([^)|]+)\)\s*\|/', $cms->content, $m)) {
						// image dans un lien
						$image = CHtml::link(CHtml::image(Yii::app()->baseUrl . $m[2], $m[1]), $m[3]);
					}
				} else {
					$title = substr(
						strip_tags($md->transform($cms->content)),
						0,
						30
					);
				}
		}
		return [$date, $image, $title];
	}

	private static function countLinks(): int
	{
		return Cms::countLinks();
	}

	private static function dateDerMaj(): string
	{
		$date = Yii::app()->db->createCommand("SELECT MAX(hdateVal) FROM Intervention")->queryScalar();
		if (!$date) {
			return "???";
		}
		return (string) Yii::app()->dateFormatter->format(' d MMMM yyyy', $date);
	}

	/**
	 * @return string HTML
	 */
	private static function displaySearchForm(): string
	{
		return '<div id="main-search-form"><h2>Rechercher une revue</h2>'
			. Yii::app()->controller->getSearchForm('')
			. CHtml::tag(
				'div',
				['style' => 'display: inline-block; vertical-align: middle; padding-top: 2px;'],
				CHtml::link('Recherche avancée', ['/revue/search'], ['class' => 'advanced-search'])
				. '<br />'
				. CHtml::link('Exploration thématique', ['/categorie/index'], ['class' => 'advanced-search'])
			)
			. '</div>';
	}

	private static function getRatioLiens(): string
	{
		$nbRevues = (int) Yii::app()->db->createCommand("SELECT count(DISTINCT revueId) FROM Titre")->queryScalar();
		if ($nbRevues === 0) {
			return "0";
		}
		return (string) (round(self::countLinks() / (double) $nbRevues));
	}

	/**
	 * @return string HTML
	 */
	private static function listAbcdRevues(): string
	{
		$letters = array_filter(SqlHelper::sqlToPairs(<<<EOSQL
			SELECT
				FIND_IN_SET(LEFT(titre, 1), 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z') AS rank,
				COUNT(*) AS num
			FROM Titre
			WHERE statut = 'normal' AND obsoletePar IS NULL
			GROUP BY rank
			EOSQL
		));
		$abcd = '<div class="abcd-revues"><ul>';
		foreach ($letters as $i => $count) {
			$l = ($i > 0 ? strtoupper(chr((int) $i + 64)) : '#0-9');
			$abcd .= "<li>"
				. CHtml::link(strtoupper($l), ['revue/index', 'lettre' => $l], ['title' => $count . ' revues'])
				. "</li>\n";
		}
		$abcd .= "</ul></div>\n";
		return $abcd;
	}

	/**
	 * @return string HTML
	 */
	private static function listBiblios(): string
	{
		$composer = new Composer(dirname(VENDOR_PATH));
		$libs = $composer->getLibraries();
		$libsHtml = [];
		foreach ($libs as $lib) {
			$name = htmlspecialchars($lib->name);
			$attr = empty($lib->description) ? [] : ['title' => $lib->description];
			$libsHtml[$lib->name] = "<li>"
				. (empty($lib->homepage) ? CHtml::tag('em', $attr, $name) : CHtml::link($name, $lib->homepage, $attr))
				. (empty($lib->license) ? '' : " (" . join(", ", $lib->license) . ")")
				. "</li>";
		}
		asort($libsHtml);
		return "<ul>" . join("", $libsHtml) . "</ul>";
	}

	/**
	 * @return \Cms[]
	 */
	private static function search(array $matches): array
	{
		$search = new \models\cms\BreveSearchForm();
		$q = Yii::app()->request->getQuery('q', []);
		if (is_scalar($q)) {
			throw new \CHttpException(400);
		}
		$get = array_filter($q, is_string(...));
		if ($get) {
			$search->setAttributes($get);
		}
		if (isset($matches[2])) {
			$filters = json_decode($matches[2], true);
			$search->setAttributes($filters);
		}
		$provider = $search->search();
		$provider->criteria->limit = isset($matches[1]) ? (int) $matches[1] : 5;
		return $provider->getData();
	}

	/**
	 * Returns the HTML for the last $num news.
	 * @todo build the HTML from the selected news.
	 *
	 * @return string HTML.
	 */
	private static function getNews(array $matches): string
	{
		$blocks = self::search($matches);
		$html = '<ul class="breves">';
		foreach ($blocks as $block) {
			$html .= '<li id="actu' . $block->id . '">' . $block->toHtml(false) . "</li>\n";
		}
		return $html . "</ul>";
	}

	/**
	 * Returns the HTML for the titles of the last $num news.
	 *
	 * @return string HTML.
	 */
	private static function getNewsTitles(array $matches): string
	{
		$blocks = self::search($matches);
		return self::formatNewsTitles($blocks);
	}

	/**
	 * @return string HTML
	 */
	private static function listLastInterventions(array $matches): string
	{
		return HtmlRender::listLastInterventions((int) $matches[1]);
	}

	/**
	 * @return string HTML
	 */
	private static function listLastJournalCreations(array $matches): string
	{
		return HtmlRender::listLastJournalCreations((int) $matches[1]);
	}
}
