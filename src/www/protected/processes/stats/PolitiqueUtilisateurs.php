<?php

namespace processes\stats;

use Yii;

class PolitiqueUtilisateurs
{
	public function counts(): array
	{
		$result = [
			"Comptes validés" => 0,
			"Comptes en attente de validation" => 0,
		];
		$rows = Yii::app()->db
			->createCommand("SELECT confirmed, count(DISTINCT utilisateurId) FROM Utilisateur_Editeur GROUP BY confirmed")
			->queryAll(false);
		foreach ($rows as $row) {
			if ($row[0]) {
				$result["Comptes validés"] = (int) $row[1];
			} else {
				$result["Comptes en attente de validation"] = \CHtml::link($row[1], ['admin/politique-validation']);
			}
		}
		return $result;
	}

	public function byInstitute(): array
	{
		$result = [];
		$rows = Yii::app()->db
			->createCommand(
				<<<EOSQL
				SELECT Partenaire.type, count(DISTINCT utilisateurId) as numUtilisateurs
				FROM Utilisateur u
					LEFT JOIN Partenaire ON u.partenaireId = Partenaire.id
					JOIN Utilisateur_Editeur ue ON ue.utilisateurId = u.id
				GROUP BY Partenaire.type
				ORDER BY count(*) DESC, Partenaire.type ASC
				EOSQL
			)
			->queryAll(false);
		$toFr = [
			"" => "Sans partenaire",
			"editeur" => "Partenaire-éditeur",
			"normal" => "Partenaire veilleur",
		];
		foreach ($rows as $row) {
			$name = $toFr[(string) $row[0]] ?? $row[0];
			$result[] = [$name, (int) $row[1]];
		}
		return $result;
	}

	public function byPublisher(): array
	{
		$result = [];
		$rows = Yii::app()->db
			->createCommand(
				<<<EOSQL
				SELECT e.*, count(DISTINCT utilisateurId) as numUtilisateurs
				FROM Editeur e
					JOIN Utilisateur_Editeur ue ON ue.editeurId = e.id
				GROUP BY e.id
				ORDER BY count(*) DESC, e.nom ASC
				EOSQL
			)
			->queryAll(true);
		foreach ($rows as $row) {
			$editeur = (new \Editeur())->populateRecord($row, false);
			$result[] = [$editeur->getSelfLink(), (int) $row['numUtilisateurs']];
		}
		return $result;
	}
}
