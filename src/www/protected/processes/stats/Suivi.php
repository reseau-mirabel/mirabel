<?php

declare(strict_types=1);

namespace processes\stats;

use Abonnement;
use components\SqlHelper;
use CSqlDataProvider;
use Yii;

class Suivi
{
	public CSqlDataProvider $provider;

	/**
	 * @var int[]
	 */
	public array $possessions;

	/**
	 * @var int[]
	 */
	public array $abonnements;

	/**
	 * @var int[]
	 */
	public array $masques;

	public function __construct()
	{
		$cmd = Yii::app()->db->createCommand(<<<EOSQL
			SELECT
				p.id
			  , p.nom
			  , p.type
			  , p.statut
			  , u.nomComplet
			  , u.derConnexion
			  , SuiviRevue.verifRecente
			  , SuiviRevue.verifAncienne
			  , SuiviRevue.verifNon
			  , SuiviRevue.revuesSuivies
			  , SuiviRessource.ressourcesSuivies
			FROM `Partenaire` p
			  LEFT JOIN (
				  SELECT partenaireId, MAX(`derConnexion`) AS maxDerConnexion FROM `Utilisateur` GROUP BY `partenaireId`
				) m ON m.partenaireId = p.id
			  LEFT JOIN Utilisateur u ON u.partenaireId = p.id AND u.derConnexion = m.maxDerConnexion
			  LEFT JOIN (
				  SELECT
					s.partenaireId
					, MAX(r.hdateVerif) AS verifRecente
					, MIN(IF(r.hdateVerif > 0, r.hdateVerif, NULL)) AS verifAncienne
					, COUNT(r.id) - COUNT(r.hdateVerif) AS verifNon
					, COUNT(DISTINCT s.cibleId) AS revuesSuivies
				  FROM Suivi s
					LEFT JOIN Revue r ON r.id = s.cibleId
				  WHERE s.cible='Revue'
				  GROUP BY s.partenaireId
				) AS SuiviRevue ON SuiviRevue.partenaireId = p.id
			  LEFT JOIN (
				  SELECT partenaireId, COUNT(*) AS ressourcesSuivies FROM Suivi WHERE cible = 'Ressource' GROUP BY partenaireId
				) AS SuiviRessource ON SuiviRessource.partenaireId = p.id
			GROUP BY p.id
			EOSQL
		);
		$this->provider = new CSqlDataProvider(
			$cmd,
			[
				'sort' => [
					'attributes' => [
						'nom' => 'p.nom',
						'derConnexion' => 'u.derConnexion',
						'verifAncienne' => 'verifAncienne',
						'type' => "IF(p.type <> 'normal', p.type, p.statut)",
					],
					'defaultOrder' => [
						'nom' => \CSort::SORT_ASC,
					],
				],
				'pagination' => false,
			]
		);

		$this->possessions = SqlHelper::sqlToPairs(
			"SELECT partenaireId, COUNT(*) FROM Partenaire_Titre GROUP BY partenaireId"
		);

		$abo = Abonnement::ABONNE;
		$this->abonnements = SqlHelper::sqlToPairs(
			"SELECT partenaireId, COUNT(*) FROM Abonnement WHERE mask = {$abo} GROUP BY partenaireId"
		);

		$mask = Abonnement::MASQUE;
		$this->masques = SqlHelper::sqlToPairs(
			"SELECT partenaireId, COUNT(*) FROM Abonnement WHERE mask = {$mask} GROUP BY partenaireId"
		);
	}
}
