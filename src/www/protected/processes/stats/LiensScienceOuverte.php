<?php

namespace processes\stats;

use Pays;
use processes\stats\scienceouverte\ResultatParPays;

class LiensScienceOuverte extends Liens
{
	/**
	 * @param \CDbConnection $db
	 * @param int[] $sourceids restriction du calcul à ces Sourceliens (LienTitre.sourceId)
	 */
	public function __construct(\CDbConnection $db, array $sourceids)
	{
		parent::__construct($db);
		$this->listFrequenciesScienceOuverte($sourceids);
	}

	/**
	 * @return array [ $name => ['total' => int , 'ratio' => string ] ]
	 */
	public function getFrequenciesTitresRatio(): array
	{
		$sql = "SELECT nom, nbimport FROM Sourcelien WHERE nbimport > 0";
		$records = $this->db->createCommand($sql)->queryAll();
		foreach ($records as $record) {
			$count[$record['nom']] = (int) $record['nbimport'];
		}

		$res = [];
		foreach ($this->names as $curname) {
			$name = $curname['name'];
			if (isset($count[$name])) {
				$res[$name] = [
					'total' => $count[$name],
					'ratio' => sprintf("%5.2f %%", 100 * $curname['num'] / $count[$name]),
				];
			} else {
				$res[$name] = [
					'total' => '?',
					'ratio' => '? %',
				];
			}
		}
		return $res;
	}

	/**
	 * @return ResultatParPays[]
	 */
	public function listFrequenciesCountry(int $sourceid): array
	{
		return [
			$this->listFrequenciesPaysIssnBySource($sourceid),
			$this->listFrequenciesPaysEditeursBySource($sourceid),
		];
	}

	private function listFrequenciesScienceOuverte(array $sourceids): void
	{
		$value = $this->getFrequenciesTitresFromDb($sourceids);
		$this->names = $value['names'];
		$this->domains = $value['domains'];
	}

	private function listFrequenciesPaysIssnBySource(int $sourceid): ResultatParPays
	{
		$res = new ResultatParPays();
		$res->source = "Par pays d'ISSN";

		$this->db
			->createcommand(<<<SQL
				CREATE TEMPORARY TABLE TitrePays (
					SELECT
						i.titreId AS id,
						IFNULL(MAX(i.pays = 'FR'), 0) AS francais,
						MAX(i.pays) AS pays
					FROM Issn i
						JOIN LienTitre lt ON lt.titreId = i.titreId AND lt.sourceId = $sourceid
					WHERE i.issn IS NOT NULL AND i.statut = :valide
					GROUP BY i.titreId
				)
				SQL)
			->execute([':valide' => \Issn::STATUT_VALIDE]);
		$sql1 = <<<EOSQL
			SELECT
				count(*) AS numTitres,
				count(DISTINCT Titre.revueId) AS numRevues
			FROM Titre
				JOIN TitrePays p USING(id)
			WHERE p.francais = 1
			EOSQL;
		$row1 = $this->db->createCommand($sql1)->queryRow();
		$res->frTitres = (int) $row1['numTitres'];
		$res->frRevues = (int) $row1['numRevues'];

		$row2 = $this->db->createCommand("$sql1 AND Titre.dateFin = ''")->queryRow();
		$res->frVivantTitres = (int) $row2['numTitres'];
		$res->frVivantRevues = (int) $row2['numRevues'];

		$res->inconnus = (int) $this->db->createCommand("SELECT count(*) FROM TitrePays WHERE pays = ''")->queryScalar();
		$res->autres = (int) $this->db->createCommand("SELECT count(*) FROM TitrePays WHERE pays <> '' AND francais = 0")->queryScalar();

		$this->db->createCommand("DROP TEMPORARY TABLE TitrePays")->execute();

		return $res;
	}

	private function listFrequenciesPaysEditeursBySource(int $sourceid): ResultatParPays
	{
		$res = new ResultatParPays();
		$res->source = "Par pays d'éditeur";

		$frid = (int) Pays::model()->findByAttributes(['code2' => 'FR'])->id;
		$this->db
			->createcommand(<<<SQL
				CREATE TEMPORARY TABLE TitrePays (
					SELECT
						l.titreId AS id,
						IFNULL(MAX(e.paysId = $frid), 0) AS francais,
						MAX(e.paysId) AS paysId
					FROM LienTitre l
						LEFT JOIN Titre_Editeur te ON l.titreId = te.titreId AND te.ancien = 0
						LEFT JOIN Editeur e ON te.editeurId = e.id
					WHERE l.sourceId = {$sourceid}
					GROUP BY l.titreId
				)
				SQL)
			->execute();

		$row = $this->db
			->createCommand("SELECT COUNT(*), COUNT(DISTINCT t.revueId) FROM Titre t JOIN TitrePays p USING(id) WHERE p.francais = 1")
			->queryRow(false);
		[$res->frTitres, $res->frRevues] = [(int) $row[0], (int) $row[1]];

		$rowVivant = $this->db
			->createCommand("SELECT COUNT(*), COUNT(DISTINCT t.revueId) FROM Titre t JOIN TitrePays p USING(id) WHERE p.francais = 1 AND t.dateFin = ''")
			->queryRow(false);
		[$res->frVivantTitres, $res->frVivantRevues] = [(int) $rowVivant[0], (int) $rowVivant[1]];

		$res->inconnus = (int) $this->db
			->createCommand("SELECT count(*) FROM TitrePays WHERE paysId IS NULL")
			->queryScalar();
		$res->autres = (int) $this->db
			->createCommand("SELECT count(*) FROM TitrePays WHERE paysId IS NOT NULL AND francais = 0")
			->queryScalar();

		$this->db->createCommand("DROP TEMPORARY TABLE TitrePays")->execute();

		return $res;
	}
}
