<?php

declare(strict_types=1);

namespace processes\stats;

use CDbConnection;
use CDbExpression;
use components\HtmlTable;
use Intervention;
use Partenaire;

/**
 * Indicateurs d'activité
 */
class Activite extends \CComponent
{
	public ?Partenaire $partenaire = null;

	/**
	 * @var int Timestamp
	 */
	protected int $startTs;

	/**
	 * @var int Timestamp
	 */
	protected int $endTs;

	private CDbConnection $db;

	public function __construct(CDbConnection $db)
	{
		$this->db = $db;
		$this->startTs = mktime(0, 0, 0, (int) date('n'), (int) date('j'), (int) date('Y') - 1);
		$this->endTs = time();
	}

	public function setInterval($startTs, $endTs): void
	{
		$this->startTs = $startTs;
		$this->endTs = $endTs;
	}

	public function getIntervalMessage(): string
	{
		if (!$this->startTs || !$this->endTs) {
			return ' (période non valide)';
		}
		return " sur la période	<em>"
			. \Yii::app()->dateFormatter->format('d MMMM yyyy', $this->startTs) . " — "
			. \Yii::app()->dateFormatter->format('d MMMM yyyy', $this->endTs)
			. "</em>";
	}

	public function getSuivi(): HtmlTable
	{
		$table = new HtmlTable();
		if (!$this->partenaire) {
			return $table;
		}
		$table->header = ["Suivis", "Total", "dont revues mortes", "dont (partiellement) importées", "Plus ancienne vérification"];
		$revuesSuivies = $this->partenaire->countRevuesSuivies();
		$oldestRessourceCheck = (int) $this->db
			->createCommand(
				"SELECT min(hdateVerif) FROM Suivi s JOIN Ressource r ON (r.id = s.cibleId AND cible = 'Ressource')"
				. " WHERE s.partenaireId = {$this->partenaire->id}"
			)->queryScalar();
		$oldestRevueCheck = (int) $this->db
			->createCommand(
				"SELECT min(hdateVerif) FROM Suivi s JOIN Revue r ON (r.id = s.cibleId AND cible = 'Revue')"
				. " WHERE s.partenaireId = {$this->partenaire->id}"
			)->queryScalar();

		$numRevuesSuiviesVivantes = (int) $this->db
			->createCommand(
				"SELECT count(DISTINCT r.id) FROM Suivi s JOIN Revue r ON (r.id = s.cibleId AND cible = 'Revue')"
				. " JOIN Titre t ON t.revueId = r.id"
				. " WHERE s.partenaireId = {$this->partenaire->id} AND t.obsoletePar IS NULL AND dateFin = ''"
			)->queryScalar();
		$numRevuesSuiviesImportees = (int) $this->db
			->createCommand(<<<EOSQL
				SELECT count(DISTINCT r.id)
				FROM Suivi s JOIN Revue r ON (r.id = s.cibleId AND cible = 'Revue')
					JOIN Titre t ON t.revueId = r.id
					JOIN Service se ON se.titreId = t.id
				WHERE s.partenaireId = {$this->partenaire->id} AND se.import > 0
				EOSQL
			)->queryScalar();
		$table->data = [
			[
				'Revues',
				$revuesSuivies,
				$revuesSuivies - $numRevuesSuiviesVivantes,
				$numRevuesSuiviesImportees,
				($oldestRevueCheck ? \Yii::app()->dateFormatter->format(' d MMMM yyyy', $oldestRevueCheck) : "aucune vérification"),
			],
			[
				'Ressources',
				(int) $this->db
					->createCommand(
						"SELECT count(*) FROM Suivi s JOIN Ressource r ON (r.id = s.cibleId AND cible = 'Ressource')"
						. " WHERE s.partenaireId = {$this->partenaire->id}"
					)->queryScalar(),
				'-',
				(int) $this->db
					->createCommand(
						"SELECT count(*) FROM Suivi s JOIN Ressource r ON (r.id = s.cibleId AND cible = 'Ressource')"
						. " WHERE s.partenaireId = {$this->partenaire->id} AND r.autoImport > 0"
					)->queryScalar(),
				($oldestRessourceCheck ? \Yii::app()->dateFormatter->format(' d MMMM yyyy', $oldestRessourceCheck) : "aucune vérification"),
			],
		];
		return $table;
	}

	public function getProp(): HtmlTable
	{
		if (!$this->startTs || !$this->endTs) {
			return new HtmlTable;
		}
		$sql = <<<EOSQL
			SELECT
				count(utilisateurIdProp) AS toutInterne,
				count(*) AS tout,
				SUM(utilisateurIdProp IS NOT NULL AND (utilisateurIdVal IS NULL OR utilisateurIdVal != utilisateurIdProp)) AS propInterne,
				SUM(IFNULL(utilisateurIdVal, -2) <> IFNULL(utilisateurIdProp, -1)) AS prop
			FROM Intervention
			WHERE import = 0 AND ip <> ''
				AND hdateProp BETWEEN :begin AND :end
			EOSQL;
		$params = [':begin' => $this->startTs, ':end' => $this->endTs];
		$data = $this->db->createCommand($sql)->queryRow(true, $params);

		$table = new HtmlTable();
		$table->header = ['', 'Interventions externes', 'Interventions internes', 'Ensemble des interventions'];
		$table->rowHeaders = ['Tout type', 'Dont propositions'];
		$table->data = [
			[$data['tout'] - $data['toutInterne'], (int) $data['toutInterne'], (int) $data['tout']],
			[$data['prop'] - $data['propInterne'], (int) $data['propInterne'], (int) $data['prop']],
		];
		return $table;
	}

	public function getPropParStatut(): HtmlTable
	{
		if (!$this->startTs || !$this->endTs) {
			return new HtmlTable;
		}
		$params = [':begin' => $this->startTs, ':end' => $this->endTs];
		$cmd = $this->db->createCommand()
			->select('statut, count(*) AS interventions')
			->from('Intervention i')
			->where("import = 0 AND ip <> '' AND hdateProp BETWEEN :begin AND :end")
			->group([new CDbExpression('statut WITH ROLLUP')]);
		if ($this->partenaire) {
			$cmd->join("Utilisateur u", "u.id = i.utilisateurIdProp")
				->andWhere("u.partenaireId = " . $this->partenaire->id);
		}
		return new HtmlTable(
			$cmd->queryAll(false, $params),
			["Statut", "Interventions"]
		);
	}

	public function getValParType(): HtmlTable
	{
		if (!$this->startTs || !$this->endTs) {
			return new HtmlTable;
		}
		$params = [':begin' => $this->startTs, ':end' => $this->endTs];
		$cmd = $this->db->createCommand()
			->select('action, count(*) AS interventions')
			->from('Intervention i')
			->where("import = 0 AND ip <> '' AND hdateVal BETWEEN :begin AND :end")
			->group("action");
		if ($this->partenaire) {
			$cmd->join("Utilisateur u", "u.id = i.utilisateurIdProp")
				->andWhere("u.partenaireId = " . $this->partenaire->id);
		}
		$data = $cmd->queryAll(false, $params);
		foreach ($data as $num => $row) {
			if ($row[0]) {
				$data[$num][0] = Intervention::ACTIONS[$row[0]];
			} else {
				$data[$num][0] = "Modification";
			}
		}
		return new HtmlTable(
			$data,
			["Type d'action", "Interventions"]
		);
	}

	public function countInterventions(): HtmlTable
	{
		if (!$this->startTs || !$this->endTs) {
			return new HtmlTable;
		}
		$params = [':begin' => $this->startTs, ':end' => $this->endTs];
		$cmd = $this->db->createCommand()
			->select('sum(i.import > 0), sum(i.import <= 0), count(*)')
			->from('Intervention i')
			->where("hdateVal BETWEEN :begin AND :end");
		if ($this->partenaire) {
			$cmd->join("Utilisateur u", "u.id = i.utilisateurIdVal")
				->andWhere("u.partenaireId = " . $this->partenaire->id);
		}
		return new HtmlTable(
			[
				$cmd->queryRow(false, $params),
			],
			["Interventions d'import", "Autres interventions validées", "Total"]
		);
	}

	public function countPropPerUser(): HtmlTable
	{
		if (!$this->partenaire || !$this->startTs || !$this->endTs) {
			return new HtmlTable;
		}
		$params = [
			':beginp' => $this->startTs, ':endp' => $this->endTs,
			':beginv' => $this->startTs, ':endv' => $this->endTs,
		];
		$cmd = $this->db->createCommand(
			<<<EOSQL
				WITH propositions AS (
				    SELECT up.id, count(i.id) as countProp
				    FROM Utilisateur up JOIN Intervention i ON up.id = i.utilisateurIdProp
				    WHERE up.partenaireId = {$this->partenaire->id} AND import = 0 AND hdateProp BETWEEN :beginp AND :endp
				    GROUP BY up.id
				),
				validations AS (
				    SELECT uv.id, count(i.id) as countVal
				    FROM Utilisateur uv
				    JOIN Intervention i ON uv.id = i.utilisateurIdVal
				    WHERE uv.partenaireId = {$this->partenaire->id} AND import = 0 AND hdateVal BETWEEN :beginv AND :endv
				    GROUP BY uv.id
				)

				SELECT
				    CONCAT(u.nomcomplet, ' (', u.login, ')'), IF(actif, '', 'désactivé'),
				    propositions.countProp,
				    validations.countVal
				FROM Utilisateur u
				    LEFT JOIN propositions USING (id)
				    LEFT JOIN validations USING (id)
				WHERE u.partenaireId = {$this->partenaire->id}
				ORDER BY u.nom ASC
				EOSQL
		);
		return new HtmlTable(
			$cmd->queryAll(false, $params),
			["Utilisateur", "Compte activé ?", "Interventions proposées", "Interventions validées"]
		);
	}
}
