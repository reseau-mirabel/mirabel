<?php

namespace processes\stats;

use CDbConnection;
use Cms;
use components\HtmlTable;
use Ressource;

class Contenu extends \CComponent
{
	private CDbConnection $db;

	public function __construct(CDbConnection $db)
	{
		$this->db = $db;
	}

	public function getMainInfoRevues(): HtmlTable
	{
		return new HtmlTable(
			[
				[(int) $this->db->createCommand("SELECT count(DISTINCT revueId) FROM Titre WHERE statut = 'normal'")->queryScalar()],
				[(int) $this->db->createCommand("SELECT count(DISTINCT revueId) FROM Titre WHERE statut = 'normal' AND dateFin = ''")->queryScalar()],
				[(int) $this->db->createCommand("SELECT count(DISTINCT s.cibleId) FROM Suivi s JOIN Revue r ON s.cibleId = r.id WHERE s.cible = 'Revue'")->queryScalar()],
				[(int) $this->db->createCommand("SELECT count(DISTINCT t.revueId) FROM Service s JOIN Titre t ON s.titreId = t.id WHERE s.import > 0")->queryScalar()],
				[(int) $this->db->createCommand(<<<EOSQL
					SELECT count(DISTINCT t.revueId)
					FROM Service s JOIN Titre t ON s.titreId = t.id JOIN Ressource r ON s.ressourceId = r.id
						LEFT JOIN Service_Collection sc ON sc.serviceId = s.id LEFT JOIN Collection c ON sc.collectionId = c.id
					WHERE s.import > 0 AND (r.autoImport > 0 OR c.importee > 0)
					EOSQL)
					->queryScalar()],
				[(int) $this->db->createCommand("SELECT count(*) FROM Titre WHERE statut = 'normal'")->queryScalar()],
				[(int) $this->db->createCommand(<<<EOSQL
					SELECT COUNT(DISTINCT issnw)
					FROM (
						SELECT issn AS issnw FROM Issn WHERE statut = 0
						UNION
						SELECT issnl AS issnw FROM Issn WHERE statut = 0
						) IM
					EOSQL)
					->queryScalar()],
			],
			null,
			[
				"Revues",
				"dont revues vivantes",
				"Revues suivies",
				"Revues mises à jour par import",
				"Revues mises à jour par import automatique",
				"Titres",
				"Nombre d'ISSN valides distincts",
			]
		);
	}

	public function getMainInfoRessources(): HtmlTable
	{
		return new HtmlTable(
			[
				[(int) $this->db->createCommand("SELECT count(*) FROM Ressource")->queryScalar()],
				[(int) $this->db->createCommand("SELECT count(DISTINCT s.cibleId) FROM Suivi s JOIN Revue r ON s.cibleId = r.id WHERE s.cible = 'Ressource'")->queryScalar()],
				[(int) $this->db->createCommand("SELECT count(DISTINCT s.ressourceId) FROM Service s WHERE s.import > 0")->queryScalar()],
				[(int) $this->db->createCommand("SELECT count(*) FROM Ressource WHERE autoImport = 1")->queryScalar()],
				[(int) $this->db->createCommand("SELECT count(*) FROM Collection")->queryScalar()],
				[(int) $this->db->createCommand("SELECT count(*) FROM Collection WHERE importee = 1")->queryScalar()],
			],
			null,
			["Ressources", "Ressources suivies", "Ressources ayant des accès importés", "Ressources importées", "Collections", "Collections importées"]
		);
	}

	public function getMainInfo(): HtmlTable
	{
		return new HtmlTable(
			[
				[(int) $this->db->createCommand("SELECT count(*) FROM Partenaire WHERE type='normal' AND statut='actif'")->queryScalar()],
				[(int) $this->db->createCommand("SELECT count(*) FROM Partenaire WHERE type='editeur' AND statut='actif'")->queryScalar()],
				[(int) $this->db->createCommand("SELECT count(*) FROM Editeur")->queryScalar()],
				[(int) $this->db->createCommand("SELECT count(*) FROM Service WHERE statut = 'normal'")->queryScalar()],
				[Cms::countLinks()],
				[(int) $this->db->createCommand("SELECT count(*) FROM Utilisateur u JOIN Partenaire p ON p.id = u.partenaireId WHERE p.type='normal' AND p.statut IN ('actif', 'provisoire') AND u.actif = 1")->queryScalar()],
				[(int) $this->db->createCommand("SELECT count(*) FROM Utilisateur u JOIN Partenaire p ON p.id = u.partenaireId WHERE p.type='editeur' AND p.statut IN ('actif', 'provisoire') AND u.actif = 1")->queryScalar()],
			],
			null,
			["Partenaires veilleurs", "+ Partenaires éditeurs", "Éditeurs", "Accès en ligne", "Liens extérieurs", "Veilleurs actifs", "+ Veilleurs-éditeurs actifs"]
		);
	}

	public function getRessourcesParType(): array
	{
		$sql = <<<EOSQL
			SELECT type, count(*) AS 'Ressources'
			FROM Ressource
			GROUP BY type
			ORDER BY count(*) DESC, type
			EOSQL;
		$rows = $this->db->createCommand($sql)->queryAll();
		$res = [];
		foreach ($rows as $row) {
			$res[] = [
				'Type' => Ressource::$enumType[$row['type']],
				'Ressources' => (int) $row['Ressources'],
			];
		}
		return $res;
	}

	public function getRessourcesImportees(): array
	{
		$sql = "SELECT nom, type FROM Ressource WHERE autoImport = 1 ORDER BY nom";
		return $this->db->createCommand($sql)->queryAll();
	}

	public function getRessourcesParAcces(int $limit = 20): array
	{
		$sql = <<<EOSQL
			SELECT r.nom, count(DISTINCT t.revueId) AS 'Revues', count(DISTINCT t.id) AS 'Titres', count(*) AS 'Accès en ligne'
			FROM Ressource r
				JOIN Service s ON r.id=s.ressourceId JOIN Titre t ON t.id=s.titreId
			GROUP BY r.id
			ORDER BY count(DISTINCT t.revueId) DESC, count(DISTINCT t.id) DESC, count(*) DESC
			LIMIT $limit
			EOSQL;
		return $this->db->createCommand($sql)->queryAll();
	}

	public function getAccesParType(): array
	{
		$sql = <<<EOSQL
			SELECT IFNULL(`type`, 'Tout type') AS `type`, IFNULL(acces, 'Total') AS acces, count(*) AS num
			FROM Service s
			WHERE statut = 'normal'
			GROUP BY `type`, acces
			WITH ROLLUP
			EOSQL;
		$query = $this->db->createCommand($sql)->queryAll(false);
		$result = [];
		$libre = 0;
		$restreint = 0;
		foreach ($query as $row) {
			$type = $row[0];
			if (empty($result[$type])) {
				$result[$type] = ['type' => $type];
			}
			$result[$type][$row[1]] = (int) $row[2];
			if ($row[1] === 'Total' && $type !== 'Tout type') {
				$result[$type]['Taux'] = round(100.0 * $result[$type]['Libre'] / $result[$type]['Total']);
			}
			if ($row[1] === 'Libre') {
				$libre += $row[2];
			} elseif ($row[1] === 'Restreint') {
				$restreint += $row[2];
			}
		}
		$result['Tout type']['Libre'] = $libre;
		$result['Tout type']['Restreint'] = $restreint;
		$result['Tout type']['Taux'] = round(100.0 * $libre / ($libre + $restreint));
		return $result;
	}

	public function getRevuesParAcces(): array
	{
		$sql = <<<EOSQL
			SELECT
				SUM(integralLibre) AS 'Intégral libre',
				SUM(integralRestreint) AS 'Intégral restreint',
				SUM(sommaire) AS 'Sommaire',
				SUM(sommaireSansIntegral) AS 'Sommaire sans accès intégral',
				SUM(indexation) AS 'Indexation',
				SUM(sansAcces) AS 'Sans accès',
				COUNT(*) AS 'Total'
			FROM (
			SELECT revueId
			, MAX(`type`='Intégral' AND acces='Libre') AS integralLibre
			, MAX(`type`='Intégral' AND acces='Restreint') AS integralRestreint
			, MAX(`type`='Sommaire') AS sommaire
			, (MAX(`type`='Sommaire') AND NOT MAX(`type`='Intégral')) AS sommaireSansIntegral
			, MAX(`type`='Indexation') AS indexation
			, MIN(s.id IS NULL) AS sansAcces
			FROM Titre t LEFT JOIN Service s ON (s.titreId = t.id)
			WHERE s.statut = 'normal' OR s.statut IS NULL
			GROUP BY t.revueId
			) AS sub LIMIT 1
			EOSQL;
		return array_map(
			'intval',
			$this->db->createCommand($sql)->queryRow()
		);
	}

	public function countLangGroups(): int
	{
		return (int) $this->db->createCommand("SELECT count(DISTINCT langues) FROM Titre")->queryScalar();
	}

	public function countLangGroupsUnordered(): int
	{
		$langs = $this->db->createCommand("SELECT DISTINCT langues FROM Titre WHERE langues <> ''")->queryColumn();
		$counter = [];
		foreach ($langs as $l) {
			$sorted = json_decode($l);
			sort($sorted);
			$counter[join(' ', $sorted)] = 1;
		}
		return count($counter);
	}

	public function getLangNb(): HtmlTable
	{
		$empty = $this->db->createCommand(<<<EOSQL
			SELECT count(DISTINCT revueId) FROM Titre WHERE obsoletePar IS NULL AND langues = ''
			EOSQL
		);
		$count = $this->db->createCommand(<<<EOSQL
			SELECT count(DISTINCT revueId)
			FROM Titre
			WHERE obsoletePar IS NULL
				AND NOT JSON_CONTAINS(langues, '"mul"', '$')
				AND JSON_LENGTH(langues) = :count
			EOSQL
		);
		$many = $this->db->createCommand(<<<EOSQL
			SELECT count(DISTINCT revueId)
			FROM Titre
			WHERE obsoletePar IS NULL
				AND NOT JSON_CONTAINS(langues, '"mul"', '$')
				AND JSON_LENGTH(langues) > 3
			EOSQL
		);
		$data = [
			['Sans langue précisée', (int) $empty->queryScalar()],
			['Une langue', (int) $count->queryScalar([':count' => 1])],
			['Deux langues', (int) $count->queryScalar([':count' => 2])],
			['Trois langues', (int) $count->queryScalar([':count' => 3])],
			['Plus de langues', (int) $many->queryScalar()],
		];
		return new HtmlTable(
			$data,
			["Langue de la revue (dernier titre)", "Nombre de revues"]
		);
	}

	public function getLangInfo(): HtmlTable
	{
		$exact = $this->db->createCommand(<<<EOSQL
			SELECT count(DISTINCT revueId)
			FROM Titre
			WHERE obsoletePar IS NULL
				AND JSON_LENGTH(langues) = 1
				AND JSON_VALUE(langues, '$[0]') = :lang
			EOSQL
		);
		$exactDuo = $this->db->createCommand(<<<EOSQL
			SELECT count(DISTINCT revueId)
			FROM Titre
			WHERE obsoletePar IS NULL
				AND JSON_LENGTH(langues) = 2
				AND JSON_CONTAINS(langues, JSON_QUOTE(:langa), '$')
				AND JSON_CONTAINS(langues, JSON_QUOTE(:langb), '$')
			EOSQL
		);
		$contains = $this->db->createCommand(<<<EOSQL
			SELECT count(DISTINCT revueId)
			FROM Titre
			WHERE obsoletePar IS NULL
				AND JSON_CONTAINS(langues, JSON_QUOTE(:lang), '$')
			EOSQL
		);
		$containsDuo = $this->db->createCommand(<<<EOSQL
			SELECT count(DISTINCT revueId)
			FROM Titre
			WHERE obsoletePar IS NULL
				AND JSON_CONTAINS(langues, JSON_QUOTE(:langa), '$')
				AND JSON_CONTAINS(langues, JSON_QUOTE(:langb), '$')
			EOSQL
		);
		$data = [
			['Français', (int) $contains->queryScalar([':lang' => 'fre']), (int) $exact->queryScalar([':lang' => 'fre'])],
			['Anglais', (int) $contains->queryScalar([':lang' => 'eng']), (int) $exact->queryScalar([':lang' => 'eng'])],
			['Espagnol', (int) $contains->queryScalar([':lang' => 'spa']), (int) $exact->queryScalar([':lang' => 'spa'])],
			['Allemand', (int) $contains->queryScalar([':lang' => 'ger']), (int) $exact->queryScalar([':lang' => 'ger'])],
			['Bilingue français-anglais', (int) $containsDuo->queryScalar([':langa' => 'fre', ':langb' => 'eng']), (int) $exactDuo->queryScalar([':langa' => 'fre', ':langb' => 'eng'])],
		];
		return new HtmlTable(
			$data,
			["Langue", "Parmi les langues de la revue", "Langue unique de la revue"]
		);
	}

	public function getEditeursNb(): HtmlTable
	{
		$query = $this->db->createCommand(<<<EOSQL
			SELECT nbed, count(*) AS nbtitres
			FROM (
				SELECT t.id, count(*) AS nbed
				FROM Titre t
					LEFT JOIN Titre_Editeur te ON te.titreId = t.id
				WHERE t.obsoletePar IS NULL
				GROUP BY t.id
				) AS tmp
			GROUP BY nbed
			EOSQL
		);
		$data = [
			["Sans éditeur", 0],
			["Un éditeur", 0],
			["Deux éditeurs", 0],
			["Trois éditeurs", 0],
			["Plus de 3 éditeurs", 0],
		];
		$nums = $query->queryAll(false);
		foreach ($nums as $num) {
			if ($num[0] > 3) {
				$data[4][1] += $num[1];
			} else {
				$data[(int) $num[0]][1] += $num[1];
			}
		}
		if (!$data[0][1]) {
			array_shift($data);
		}
		return new HtmlTable(
			$data,
			["Nombre d'éditeurs (dernier titre)", "Nombre de revues"]
		);
	}
}
