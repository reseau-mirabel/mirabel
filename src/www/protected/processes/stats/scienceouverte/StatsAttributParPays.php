<?php

namespace processes\stats\scienceouverte;

use Exception;
use Pays;
use Yii;

class StatsAttributParPays
{
	private string $strAttrIds;  // eg '12,15,42'

	private string $condition = "";

	private array $parameters = [];

	/**
	 * @param string $attrIdentifier accepte les % pour utiliser LIKE par ex. 'apc-avec-%'
	 * @param array $attrValues Si vide, teste uniquement la présence de l'attribut. Sinon filtre par cette liste de valeurs.
	 * @throws Exception
	 */
	public function __construct(string $attrIdentifier, array $attrValues)
	{
		// Init $strAttrIds
		$sql = "SELECT GROUP_CONCAT(id SEPARATOR ',') FROM Sourceattribut WHERE identifiant LIKE :idfr";
		$attributeIds = Yii::app()->db->createCommand($sql)->queryScalar([':idfr' => $attrIdentifier]);
		if (!$attributeIds) {
			throw new Exception("L'attribut de titre nommé '$attrIdentifier' n'existe pas.");
		}
		$this->strAttrIds = $attributeIds;

		// Init $condition and $parameters
		if ($attrValues) {
			foreach ($attrValues as $k => $v) {
				$this->parameters[":v$k"] = $v;
			}
			$this->condition = " AND at.valeur IN (" . join(',', array_keys($this->parameters)) . ")";
		}
	}

	/**
	 * @return ResultatParPays[]
	 */
	public function listFrequencies(): array
	{
		return [
			$this->listUsingIssn(),
			$this->listUsingEditeur(),
		];
	}

	private function listUsingIssn(): ResultatParPays
	{
		$res = new ResultatParPays();
		$res->source = "Par pays d'ISSN";

		$parameters = $this->parameters;
		$parameters[':valide'] = \Issn::STATUT_VALIDE;

		Yii::app()->db
			->createcommand(<<<SQL
				CREATE TEMPORARY TABLE TitrePays (
					SELECT
						i.titreId AS id,
						IFNULL(MAX(i.pays = 'FR'), 0) AS francais,
						MAX(i.pays) AS pays
					FROM Issn i
						JOIN AttributTitre at ON at.titreId = i.titreId AND at.sourceattributId IN ({$this->strAttrIds}) {$this->condition}
					WHERE i.issn IS NOT NULL AND i.statut = :valide
					GROUP BY i.titreId
				)
				SQL)
			->execute($parameters);

		$sql1 = <<<EOSQL
			SELECT
				count(*) AS numTitres,
				count(DISTINCT Titre.revueId) AS numRevues
			FROM Titre
				JOIN TitrePays p USING(id)
			WHERE p.francais = 1
			EOSQL;
		$row1 = Yii::app()->db->createCommand($sql1)->queryRow();
		$res->frTitres = (int) $row1['numTitres'];
		$res->frRevues = (int) $row1['numRevues'];
		$row2 = Yii::app()->db->createCommand("$sql1 AND Titre.dateFin = ''")->queryRow();
		$res->frVivantTitres = (int) $row2['numTitres'];
		$res->frVivantRevues = (int) $row2['numRevues'];

		$res->inconnus = (int) Yii::app()->db
			->createCommand("SELECT count(*) FROM TitrePays WHERE pays = ''")
			->queryScalar();
		$res->autres = (int) Yii::app()->db
			->createCommand("SELECT count(*) FROM TitrePays WHERE pays <> '' AND francais = 0")
			->queryScalar();

		Yii::app()->db->createCommand("DROP TEMPORARY TABLE TitrePays")->execute();

		return $res;
	}

	private function listUsingEditeur(): ResultatParPays
	{
		$res = new ResultatParPays();
		$res->source = "Par pays d'éditeur";

		$frid = (int) Pays::model()->findByAttributes(['code2' => 'FR'])->id;
		Yii::app()->db
			->createcommand(<<<SQL
				CREATE TEMPORARY TABLE TitrePays (
					SELECT
						at.titreId AS id,
						IFNULL(MAX(e.paysId = $frid), 0) AS francais,
						MAX(e.paysId) AS paysId
					FROM AttributTitre at
						LEFT JOIN Titre_Editeur te ON at.titreId = te.titreId AND te.ancien = 0
						LEFT JOIN Editeur e ON te.editeurId = e.id
					WHERE at.sourceattributId IN ({$this->strAttrIds}) {$this->condition}
					GROUP BY at.titreId
				)
				SQL)
			->execute($this->parameters);

		$row = Yii::app()->db
			->createCommand("SELECT COUNT(*), COUNT(DISTINCT t.revueId) FROM Titre t JOIN TitrePays p USING(id) WHERE p.francais = 1")
			->queryRow(false);
		[$res->frTitres, $res->frRevues] = [(int) $row[0], (int) $row[1]];

		$rowVivant = Yii::app()->db
			->createCommand("SELECT COUNT(*), COUNT(DISTINCT t.revueId) FROM Titre t JOIN TitrePays p USING(id) WHERE p.francais = 1 AND t.dateFin = ''")
			->queryRow(false);
		[$res->frVivantTitres, $res->frVivantRevues] = [(int) $rowVivant[0], (int) $rowVivant[1]];

		$res->inconnus = (int) Yii::app()->db
			->createCommand("SELECT count(*) FROM TitrePays WHERE paysId IS NULL")
			->queryScalar();
		$res->autres = (int) Yii::app()->db
			->createCommand("SELECT count(*) FROM TitrePays WHERE paysId IS NOT NULL AND francais = 0")
			->queryScalar();

		Yii::app()->db->createCommand("DROP TEMPORARY TABLE TitrePays")->execute();

		return $res;
	}
}
