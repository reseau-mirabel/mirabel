<?php

namespace processes\stats\scienceouverte;

class ResultatParPays
{
	public string $source;

	public int $frRevues;

	public int $frTitres;

	public int $frVivantRevues;

	public int $frVivantTitres;

	public int $inconnus;

	public int $autres;

	public function getFrTitresRevues(): string
	{
		return "{$this->frTitres} ⇔ {$this->frRevues} revues";
	}

	public function getFrVivantTitresRevues(): string
	{
		return "{$this->frVivantTitres} ⇔ {$this->frVivantRevues} revues";
	}
}
