<?php

declare(strict_types=1);

namespace processes\stats;

use CDbConnection;
use Yii;

class Liens
{
	private const CACHE_DURATION = 3600;

	/**
	 * @var array [ {name: "hal", num: Y, numRevues: Z} ]
	 */
	public array $names = [];

	/**
	 * @var array [ {domain: "hal.fr", num: Y, numRevues: Z} ]
	 */
	public array $domains = [];

	protected CDbConnection $db;

	public function __construct(CDbConnection $db)
	{
		$this->db = $db;
	}

	public function run(): void
	{
		$this->listFrequenciesTitres();
	}

	protected function listFrequenciesTitres(): void
	{
		$cacheName = "Sourcelien::listFrequencies_Titres";
		$value = Yii::app()->cache->get($cacheName);
		if ($value === false) {
			$value = $this->getFrequenciesTitresFromDb([]);
			Yii::app()->cache->set($cacheName, $value, self::CACHE_DURATION);
		}
		$this->names = $value['names'];
		$this->domains = $value['domains'];
	}

	protected function getFrequenciesTitresFromDb(array $sourceids): array
	{
		$result = [];
		$result['names'] = $this->createCommand($sourceids, false)
			->select(<<<EOSQL
				l.name,
				count(*) AS num,
				count(DISTINCT t.revueId) AS numRevues
				EOSQL
			)->queryAll();
		$result['domains'] = $this->createCommand($sourceids, true)
			->group("IF(LEFT(l.url, 1)='/', 'LIEN INTERNE', l.domain)")
			->select(<<<EOSQL
				IF(LEFT(l.url, 1)='/', 'LIEN INTERNE', l.domain) AS domain,
				count(*) AS num,
				count(DISTINCT t.revueId) AS numRevues,
				count(l.sourceId) AS numIdentif
				EOSQL
			)->queryAll();
		return $result;
	}

	private function createCommand(array $sourceIds, bool $includeInternalLinks): \CDbCommand
	{
		$cmd = $this->db->createCommand()
			->from("LienTitre l")
			->join("Titre t", "l.titreId = t.id")
			->group("l.name")
			->having("count(*) > 1")
			->order("count(*) DESC, l.name");
		if (!$includeInternalLinks) {
			$cmd = $cmd->where("CAST(domain AS CHAR) NOT LIKE '%interne%'");
		}
		if ($sourceIds) {
			$idsStr = implode(', ', array_map('intval', $sourceIds));
			$cmd->where("l.sourceId IN ($idsStr)");
		}
		return $cmd;
	}
}
