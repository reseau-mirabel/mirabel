<?php

namespace processes\stats;

use Politique;
use Yii;

class PolitiqueEditeurs
{
	public function byJournal(): array
	{
		$result = [];
		$rows = Yii::app()->db
			->createCommand(
				<<<EOSQL
				WITH byjournal AS (
					SELECT p.id, count(*) AS numJournals
					FROM Politique p JOIN Politique_Titre pt ON pt.politiqueId = p.id
					WHERE p.status NOT IN (:del1, :del2)
					GROUP BY p.id
				)
				SELECT numJournals, count(*) as numPol
				FROM byjournal
				GROUP BY numJournals
				ORDER BY count(*) DESC
				EOSQL
			)
			->queryAll(false, [':del1' => Politique::STATUS_DELETED, ':del2' => Politique::STATUS_TODELETE]);
		foreach ($rows as $row) {
			$result[] = [(int) $row[0], (int) $row[1]];
		}
		return $result;
	}

	public function byPublisher(): array
	{
		$result = [];
		$revuesParEditeur = \components\SqlHelper::sqlToPairs(
			"SELECT te.editeurId, count(DISTINCT t.revueId) FROM Titre_Editeur te JOIN Titre t ON t.id = te.titreId GROUP BY te.editeurId"
		);
		$rows = Yii::app()->db
			->createCommand(
				<<<EOSQL
				SELECT e.*, count(DISTINCT p.id) as numPol, count(DISTINCT t.revueId) AS numTitresPol
				FROM Editeur e
					JOIN Politique p ON p.editeurId = e.id
					LEFT JOIN Politique_Titre pt ON pt.politiqueId = p.id
					LEFT JOIN Titre t ON pt.titreId = t.id
				WHERE p.status NOT IN (:del1, :del2)
				GROUP BY e.id
				ORDER BY count(DISTINCT p.id) DESC
				EOSQL
			)
			->queryAll(true, [':del1' => Politique::STATUS_DELETED, ':del2' => Politique::STATUS_TODELETE]);
		foreach ($rows as $row) {
			$editeur = (new \Editeur())->populateRecord($row, false);
			$misses = $revuesParEditeur[$editeur->id] ?? 0;
			$misses -= $row['numTitresPol'];
			if ($misses < 0) {
				$misses = 0;
			}
			$result[] = [$editeur->getSelfLink(), (int) $row['numPol'], (int) $row['numTitresPol'], $misses];
		}
		return $result;
	}

	public function byStatus(): array
	{
		$result = [];
		$rows = Yii::app()->db
			->createCommand("SELECT status, count(*) FROM Politique GROUP BY status")
			->queryAll(false);
		foreach ($rows as $row) {
			$result[] = [Politique::translateStatus($row[0]), (int) $row[1]];
		}
		return $result;
	}
}
