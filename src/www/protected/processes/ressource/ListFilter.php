<?php

namespace processes\ressource;

use Partenaire;

class ListFilter
{
	public string $import;

	public string $lettre;

	public int $letterRank;

	public bool $noweb;

	public int $suivi;

	public bool $exhaustif = false;

	public bool $exhaustifcollections = false;

	public ?Partenaire $suiviPartenaire;

	public static function load(array $data): self
	{
		$filter = new self;
		$filter->import = $data['import'] ?? '';
		$filter->lettre = empty($data['lettre']) ? "A" : strtoupper(substr($data['lettre'], 0, 1));
		$filter->noweb = !empty($data['noweb']);
		$filter->suivi = empty($data['suivi']) ? 0 : (int) $data['suivi'];

		$filter->exhaustif = !empty($data['exhaustif']);

		$filter->exhaustifcollections = !empty($data['exhaustifcollections']);

		if (!in_array($filter->import, ['ressource', 'collection'], true)) {
			$filter->import = '';
		}
		if ($filter->suivi > 0) {
			$filter->suiviPartenaire = Partenaire::model()->findByPk($filter->suivi);
			if ($filter->suiviPartenaire === null) {
				$filter->suivi = 0;
			}
		}
		if ($filter->suivi || $filter->import || $filter->exhaustif || $filter->exhaustifcollections) {
			// no pagination
			$filter->lettre = '';
		}
		return $filter;
	}
}
