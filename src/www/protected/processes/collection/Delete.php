<?php

namespace processes\collection;

use Collection;
use Service;
use ServiceCollection;
use Yii;

class Delete
{
	/**
	 * @var Collection
	 */
	public $collection;

	/**
	 * @var int Count of all the Service records linked to this Collection.
	 */
	public $servicesCount;

	/**
	 * @var Service[]
	 */
	public $servicesToDelete;

	/**
	 * @var int Count of Abonnement records.
	 */
	public $abosCount;

	/**
	 * @var string
	 */
	private $errorMessage = '';

	public function __construct(Collection $c)
	{
		if (!$c->id || !$c->ressourceId) {
			throw new \Exception("Invalid Collection.");
		}
		$this->collection = $c;

		$this->servicesCount = (int) ServiceCollection::model()->countByAttributes(['collectionId' => $c->id]);
		$this->servicesToDelete = Service::model()->with('titre', 'ressource')->findAllBySql(
			"SELECT s.* FROM Service s LEFT JOIN Service_Collection sc ON s.id = sc.serviceId AND sc.collectionId != :cid "
			. " WHERE sc.collectionId IS NULL AND s.ressourceId = :rid",
			[':rid' => $c->ressourceId, ':cid' => $c->id]
		);
		$this->abosCount = (int) Yii::app()->db
			->createCommand("SELECT count(*) FROM Abonnement WHERE collectionId = :cid")
			->queryScalar([':cid' => $c->id]);
	}

	public function delete(): bool
	{
		return $this->deleteOrphanServices() && $this->deleteCollection();
	}

	public function getErrorMessage(): string
	{
		return $this->errorMessage;
	}

	public function isReady(int $confirmed): bool
	{
		return $this->abosCount === 0
			&& (count($this->servicesToDelete) === 0 || $confirmed);
	}

	private function deleteCollection(): bool
	{
		$i = $this->collection->buildIntervention(true);
		$i->description = "Suppression de la collection « {$this->collection->nom} »";
		$i->action = 'collection-D';
		// delete the collection
		$i->contenuJson->delete(
			$this->collection,
			"Suppression de la collection « " . $this->collection->nom . " »"
			. ($this->servicesToDelete ? " après suppression des " . count($this->servicesToDelete) . " accès orphelins" : "")
		);
		if (!$i->accept()) {
			$this->errorMessage = "intervention/collection : " . print_r($i->getErrors(), true);
			return false;
		}
		return true;
	}

	private function deleteOrphanServices(): bool
	{
		foreach ($this->servicesToDelete as $s) {
			$iS = $s->buildIntervention(true);
			$iS->action = 'service-D';
			$iS->description = "Suppression d'un accès en ligne, revue « {$s->titre->titre} »"
				. " / « {$s->ressource->nom} » pour la suppression de la collection « {$this->collection->nom} »";
			$link = new ServiceCollection();
			$link->collectionId = $this->collection->id;
			$link->serviceId = $s->id;
			$link->isNewRecord = false;
			$iS->contenuJson->delete($s, "Suppression de l'accès en ligne");
			if (!$iS->accept()) {
				$this->errorMessage = "erreur de suppression d'accès en ligne: " . print_r($iS->getErrors(), true);
				return false;
			}
		}
		return true;
	}
}
