<?php

namespace processes\collection;

use CHtml;
use Collection;
use Config;
use components\email\Mailer;
use Partenaire;
use Yii;

class CreateTmp
{
	/**
	 * @var Collection
	 */
	public $collection;

	/**
	 * @var array
	 */
	private $redirection = [];

	public function __construct(int $ressourceId)
	{
		$this->collection = new Collection;
		$this->collection->ressourceId = (int) $ressourceId;
		$this->collection->type = Collection::TYPE_TEMPORAIRE; // par défaut
		$this->collection->nom = "par défaut";
	}

	public function getRedirection(): array
	{
		return $this->redirection;
	}

	public function load(array $data): bool
	{
		if (!$data) {
			return false;
		}
		$this->collection->attributes = $data;
		if ($this->collection->type === Collection::TYPE_TEMPORAIRE) {
			$this->collection->visible = false;
		}
		return true;
	}

	public function preValidate(): bool
	{
		if ($this->collection->ressource === null) {
			throw new \Exception("Invalid ressource ID");
		}
		if (Collection::model()->countByAttributes(['ressourceId' => $this->collection->ressourceId]) > 0) {
			$this->redirection = [
				'status' => 'error',
				'message' => "Cette ressource a déjà des collections, donc une collection temporaire ne peut y être ajoutée.",
				'url' => ['create', 'ressourceId' => $this->collection->ressourceId],
			];
			return false;
		}
		return true;
	}

	public function save(): bool
	{
		$i = $this->collection->buildIntervention(true);
		$i->description = "Création de la collection « {$this->collection->nom} »";
		$i->action = 'collection-C';
		$i->contenuJson->create($this->collection);
		if (!$this->collection->validate()) {
			return false;
		}
		if (!$i->accept()) {
			Yii::log("Erreur de création de collection temporaire : " . print_r($i->getErrors(), true), 'error');
			$this->redirection = [
				'status' => 'error',
				'message' => "Erreur interne lors de l'enregistrement de la collection.",
			];
			return false;
		}

		$this->collection->id = (int) $i->contenuJson->lastInsertId['Collection'];

		$numAcces = Yii::app()->db
			->createCommand("INSERT INTO Service_Collection SELECT id, {$this->collection->id} FROM Service WHERE ressourceId = :rid")
			->execute([':rid' => $this->collection->ressourceId]);
		$numAbos = Yii::app()->db
			->createCommand("UPDATE Abonnement SET collectionId = {$this->collection->id}, ressourceId = NULL WHERE ressourceId = :rid")
			->execute([':rid' => $this->collection->ressourceId]);
		$this->redirection = [
			'status' => 'success',
			'message' => "La collection initiale « " . CHtml::encode($this->collection->nom) . " »  de type {$this->collection->type} est créée."
				. " $numAcces accès en ligne de la ressource y ont été liés."
				. " $numAbos abonnements y ont été migrés, avec courriels de signalement aux partenaires.",
			'url' => ['create', 'ressourceId' => $this->collection->ressourceId],
		];

		if ($this->collection->type === Collection::TYPE_TEMPORAIRE && $numAbos > 0) {
			$partenaires = Partenaire::model()->findAllBySql(
				"SELECT p.* FROM Abonnement a JOIN Partenaire p ON a.partenaireId = p.id WHERE a.collectionId = {$this->collection->id}"
			);
			foreach ($partenaires as $p) {
				$this->emailCollectionTemporaire($p->email, $this->collection);
			}
		}
		return true;
	}

	/**
	 * Send an e-mail with a warning about the tmp collection.
	 */
	private function emailCollectionTemporaire(string $email, Collection $collection): bool
	{
		$url = Yii::app()->controller->createAbsoluteUrl('/ressource/view', ['id' => $collection->ressourceId]);
		$message = Mailer::newMail()
			->subject("[" . Yii::app()->name . "] Abonnement à une collection temporaire")
			->from(Config::read('email.from'))
			->setTo($email)
			->addBcc("mirabel_donnees@listes.sciencespo-lyon.fr")
			->text(
				"Bonjour,

vous étiez abonnés dans Mir@bel à une ressource qui a été migrée dans une
collection temporaire. Cette collection a vocation à disparaître
prochainement, vous devriez sans doute vous abonner à une autre
collection de cette ressource.

- Ressource {$collection->ressource->nom}
- Collection {$collection->nom}
- URL $url

--
Mir@bel
"
			);
		$replyTo = Config::read('email.replyTo');
		if ($replyTo) {
			$message->replyTo($replyTo);
		}
		return Mailer::sendMail($message);
	}
}
