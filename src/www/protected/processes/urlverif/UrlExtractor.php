<?php

namespace processes\urlverif;

use LinkChecker;

class UrlExtractor
{
	private LinkChecker $linkChecker;

	public function __construct(?LinkChecker $linkChecker = null)
	{
		$this->linkChecker = ($linkChecker === null ? new LinkChecker() : $linkChecker);
	}

	/**
	 * Check the links of the object and its sub-objects.
	 *
	 * @return array{checked: array, urls: array}
	 */
	public function checkObjectLinks(\CActiveRecord $r): array
	{
		$links = self::extract($r);
		return [
			'checked' => $links['checked'],
			'urls' => $this->linkChecker->checkLinks($links['urls']),
		];
	}

	/**
	 * Return an structure with the URLs present in the object and its related sub-objects.
	 *
	 * The key "checked" contains a flat list of URLs.
	 * The key "urls" is an associative array that groups URLs.
	 *
	 * @return array{checked: array, urls: array}
	 */
	public static function extract(\CActiveRecord $r): array
	{
		if ($r instanceof \Cms) {
			return self::extractFromCms($r);
		}
		if ($r instanceof \Editeur) {
			return self::extractFromEditeur($r);
		}
		if ($r instanceof \Ressource) {
			return self::extractFromRessource($r);
		}
		if ($r instanceof \Revue) {
			return self::extractFromRevue($r);
		}
		throw new \Exception("Cannot extract links from class " . get_class($r));
	}

	private static function extractFromCms(\Cms $c): array
	{
		$urls = [];
		$baseUrl = \Yii::app()->params->itemAt('baseUrl');
		if ($c->content === '') {
			// empty
		} elseif ($c->type === 'html') {
			$dom = new \DOMDocument;
			$dom->loadHTML($c->content);
			foreach ($dom->getElementsByTagName('a') as $node) {
				$u = $node->getAttribute("href");
				if (strncmp($u, 'http', 4) === 0) {
					$urls[] = $u;
				} elseif ($u[0] === '/') {
					$urls[] = "{$baseUrl}$u";
				}
			}
		} else {
			// markdown
			$md = \Yii::app()->getComponent('markdown');
			assert($md instanceof \League\CommonMark\ConverterInterface);
			$document = $md->convert($c->content)->getDocument();
			$matchingNodes = (new \League\CommonMark\Node\Query())
				->where(\League\CommonMark\Node\Query::type(\League\CommonMark\Extension\CommonMark\Node\Inline\Link::class))
				->findAll($document);
			foreach ($matchingNodes as $node) {
				assert($node instanceof \League\CommonMark\Extension\CommonMark\Node\Inline\Link);
				$u = $node->getUrl();
				if (strncmp($u, 'http', 4) === 0) {
					$urls[] = $u;
				} elseif ($u[0] === '/') {
					$urls[] = "{$baseUrl}$u";
				}
			}
		}
		return [
			'urls' => $urls,
			'checked' => $urls,
		];
	}

	private static function extractFromEditeur(\Editeur $e): array
	{
		$urls = [];
		$checks = [
			'Editeur' => [],
			'Collection' => [],
		];

		$urls[] = $e->url;
		$urls[] = $e->logoUrl;
		if ($e->liensJson) {
			foreach ($e->getLiens() as $link) {
				$urls[] = $link->url;
			}
		}
		$checks['Editeur'][$e->id] = $e;

		return [
			'urls' => array_filter($urls),
			'checked' => $checks,
		];
	}

	private static function extractFromRessource(\Ressource $r): array
	{
		$urls = [];
		$checks = [
			'Ressource' => [],
			'Collection' => [],
		];

		$urls[] = $r->url;
		foreach ($r->getCollectionsDiffuseur() as $c) {
			/** @var \Collection $c */
			$urls[] = $c->url;
		}
		$checks['Ressource'][$r->id] = $r;

		return [
			'urls' => array_filter($urls),
			'checked' => $checks,
		];
	}

	private static function extractFromRevue(\Revue $r): array
	{
		$urls = [];
		$checks = [
			'Titres' => [],
			'Accès en ligne' => [],
		];

		foreach ($r->titres as $titre) {
			/** @var \Titre $titre */
			$urls[] = $titre->url;
			if ($titre->liensJson) {
				foreach ($titre->getLiens() as $link) {
					$urls[] = $link->url;
				}
			}
			$urls[] = $titre->getUrlSudoc();
			$checks['Titres'][$titre->id] = $titre;
		}

		foreach ($r->getServices() as $service) {
			/** @var \Service $service */
			$urls[] = $service->url;
			$urls[] = $service->alerteMailUrl;
			$urls[] = $service->alerteRssUrl;
			$urls[] = $service->derNumUrl;
			$checks['Accès en ligne'][] = $service;
		}

		return [
			'urls' => array_filter($urls),
			'checked' => $checks,
		];
	}
}
