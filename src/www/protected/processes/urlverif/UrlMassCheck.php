<?php

namespace processes\urlverif;

use CActiveRecord;
use components\db\InsertBuffer;
use Exception;
use LinkChecker;
use Yii;

/**
 * Update the VerifUrl* tables with bad URLs.
 */
class UrlMassCheck
{
	public const SOURCES = [
		'Cms' => 1,
		'Editeur' => 2,
		'Ressource' => 3,
		'Revue' => 4,
	];

	private const URL_MAX_LENGTH = 511;

	public LinkChecker $linkChecker;

	public int $verbose = 0;

	/**
	 * @var ?resource
	 */
	private $csvOutput = null;

	/**
	 * @param bool $slow If set, will reduce the parallel processing and wait after each request.
	 */
	public function __construct(bool $slow)
	{
		$this->linkChecker = $this->initLinkChecker($slow);
	}

	/**
	 * @param null|resource $resource
	 */
	public function setCsvOutput($resource): void
	{
		$this->csvOutput = $resource;
	}

	public function checkAllLinks(string $sourceName, array $linksData): void
	{
		if (!$linksData) {
			return;
		}
		$source = self::SOURCES[$sourceName] ?? 0;
		if ($source === 0) {
			throw new \Exception("Invalid URL source");
		}

		Yii::app()->db->createCommand("DELETE FROM VerifUrl WHERE source = $source")->execute();
		$insertBuffer = new InsertBuffer("INSERT INTO VerifUrl (source, sourceId, url, success, msg) VALUES ");
		$insertBuffer->setBatchSize(100);
		$insertBuffer->stopDbConnection();

		foreach ($linksData as $links) {
			if ($this->verbose > 1) {
				fprintf(STDERR, "%s %5d...\n", $sourceName, $links['pk']);
			}
			$links['urls'] = $this->linkChecker->checkLinks($links['urls']);

			// analyze and save the results for this source
			foreach ($links['urls'] as $url => $result) {
				[$status, $statusMessage] = $result;
				$statusMessage = $statusMessage?: '';
				$success = in_array($status, [LinkChecker::RESULT_SUCCESS, LinkChecker::RESULT_IGNORED], true);
				if ($this->csvOutput) {
					if ($this->verbose > 1) {
						fputcsv($this->csvOutput, [$links['pk'], $url, ($status === LinkChecker::RESULT_SUCCESS ? 'OK' : $statusMessage)], "\t", '"', '\\');
					} elseif (!$success) {
						fputcsv($this->csvOutput, [$links['pk'], $url, $statusMessage], "\t", '"', '\\');
					}
				}
				$insertBuffer->add([
					$source,
					$links['pk'],
					substr($url, 0, self::URL_MAX_LENGTH),
					(int) $success,
					($success ? "" : $statusMessage),
				]);
			}
		}
		$insertBuffer->close();
	}

	/**
	 * @return int Number of checked URLs
	 */
	public function checkBrokenLinks(string $sourceName): int
	{
		$source = self::SOURCES[$sourceName] ?? 0;
		if ($source === 0) {
			throw new \Exception("Invalid URL source");
		}
		$db = Yii::app()->db;

		$this->linkChecker->setTimeout(12);
		$toCheckCmd = $db
			->createCommand(
				"SELECT id, sourceId, url FROM VerifUrl WHERE source = {$source} AND success = 0 AND length(url) < " . self::URL_MAX_LENGTH
			);
		$toCheck = [];
		foreach ($toCheckCmd->query() as $row) {
			if (strncmp($row['url'], 'http', 4) === 0) {
				$toCheck[$row['url']] = ['id' => (int) $row['id'], 'sourceId' => (int) $row['sourceId']];
			}
		}
		unset($toCheckCmd);
		if (!$toCheck) {
			return 0;
		}

		$results = $this->linkChecker->checkLinks(array_keys($toCheck));

		$transaction = $db->beginTransaction();
		$update = $db->createCommand("UPDATE VerifUrl SET success = :success, msg = :msg, hdate = NOW() WHERE id = :id");
		$batch = 0;
		foreach ($results as $url => $result) {
			$id = (int) $toCheck[$url]['id'];
			$sourceId = (int) $toCheck[$url]['sourceId'];

			[$status, $statusMessage] = $result;
			$statusMessage = $statusMessage?: '';
			$success = in_array($status, [LinkChecker::RESULT_SUCCESS, LinkChecker::RESULT_IGNORED], true);
			if ($this->csvOutput) {
				if ($this->verbose > 1) {
					fputcsv($this->csvOutput, [$sourceId, $url, ($status === LinkChecker::RESULT_SUCCESS ? 'OK' : $statusMessage)], "\t", '"', '\\');
				} elseif (!$success) {
					fputcsv($this->csvOutput, [$sourceId, $url, $statusMessage], "\t", '"', '\\');
				}
			}
			$update->execute([
				':success' => (int) $success,
				':msg' => $success ? '' : $statusMessage,
				':id' => $id,
			]);
			$batch++;
			if ($batch >= 200) {
				$batch = 0;
				$transaction->commit();
				$transaction = $db->beginTransaction();
			}
		}
		$transaction->commit();
		return count($toCheck);
	}

	/**
	 * @param \CActiveRecord[] $sources
	 * @return list<array{pk: int, urls: array}>
	 */
	public static function extractLinks(array $sources): array
	{
		$linksData = [];
		foreach ($sources as $source) {
			$row = UrlExtractor::extract($source);
			$row['pk'] = (int) $source->getPrimaryKey();
			unset($row['checked']);
			$linksData[] = $row;
		}
		return $linksData;
	}

	public function getStats(string $source): string
	{
		$out = "";
		$stats = LinkChecker::getStats();
		if ($this->verbose > 1) {
			$out .= sprintf(" stats:\n%s\n", print_r($stats, true));
		}
		$out .= sprintf("Liens ($source) : %3d liens testés, %3d erreurs\n", $stats['urlsNum'], $stats['errorsNum']);
		return $out;
	}

	public function removeObsoleteLinks(string $sourceName): void
	{
		$source = self::SOURCES[$sourceName] ?? 0;
		if ($source === 0) {
			throw new \Exception("Invalid URL source");
		}

		$data = Yii::app()->db
			->createCommand(<<<EOSQL
				SELECT t.*, GROUP_CONCAT(v.url SEPARATOR ' | ') AS brokenUrls
				FROM $sourceName t JOIN VerifUrl v ON v.source = $source AND t.id = v.sourceId
				WHERE v.success = 0
				GROUP BY t.id
				EOSQL
			)->queryAll();

		$remove = Yii::app()->db->createCommand("DELETE FROM VerifUrl WHERE source = :s AND sourceId = :id AND url = :url");
		$model = call_user_func([$sourceName, 'model']);
		foreach ($data as $d) {
			$brokenUrls = explode(' | ', $d['brokenUrls']);
			unset($d['brokenUrls']);

			$record = $model->populateRecord($d, false);
			if (!($record instanceof CActiveRecord)) {
				throw new Exception("Programming error.");
			}

			$existingUrls = UrlExtractor::extract($record)['urls'];
			$diff = array_diff($brokenUrls, $existingUrls);
			foreach ($diff as $url) {
				$id = (int) $record->getPrimaryKey();
				if ($this->csvOutput) {
					fputcsv($this->csvOutput, [$id, $url, "Cette URL n'est plus dans Mir@bel"], "\t", '"', '\\');
				}
				$remove->execute([':s' => $source, ':id' => $id, ':url' => $url]);
			}
		}
	}

	private function initLinkChecker(bool $slow): LinkChecker
	{
		$linkChecker = new LinkChecker();
		if ($slow) {
			$linkChecker->maxThreads = 2;
			$linkChecker->wait = 100; // ms
		}
		$linkChecker->setTimeout(5);
		return $linkChecker;
	}
}
