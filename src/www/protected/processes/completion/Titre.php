<?php

namespace processes\completion;

use CHtml;
use models\validators\IssnValidator;
use Yii;

class Titre implements CompletionInterface
{
	/**
	 * Returns a list of assoc (id =>, value=> , label=>) matching the requested term.
	 *
	 * @param array $filters  array(field => array(values,to,require))
	 * @param array $excludes array(field => array(values,to,exclude))
	 * @return array{id: int, value: string, label: string}[]
	 */
	public function completeTerm(string $term, array $filters = [], array $excludes = [], string $more = 'html'): array
	{
		$cmd = static::createCommand($term, $filters, $excludes);
		$results = [];
		foreach ($cmd->query() as $row) {
			if (count($results) >= 20) {
				$label = "…";
				if ($more === 'html') {
					$label = CHtml::link(
						'…',
						Yii::app()->createUrl('revue/search', ['SearchTitre[titre]' => $term]),
						['target' => '_blank']
					);
				}
				if ($more !== '') {
					$results[] = ['id' => 0, 'value' => $term, 'label' => $label];
				}
				break;
			}
			$results[] = static::formatRow($row);
		}
		if (ctype_digit($term)) {
			$exact = \Titre::model()->findByPk((int) $term);
			if ($exact instanceof \Titre) {
				array_unshift($results, [
					'id' => (int) $exact->id,
					'value' => $exact->getFullTitle(),
					'label' => "[ID=$term] " . $exact->getFullTitleWithPerio(),
				]);
			}
		}
		return $results;
	}

	/**
	 * Find the instances ID that have this exact name or ISSN.
	 *
	 * @param string $term Title or ISSN
	 * @return int[]
	 */
	public function findExactTerm(string $term): array
	{
		$v = new IssnValidator();
		if ($v->validateString($term)) {
			return [
				(int) Yii::app()->db
					->createCommand("SELECT titreId FROM Issn WHERE issn = :issn LIMIT 1")
					->queryScalar([':issn' => $term]),
			];
		}
		$ids = Yii::app()->db
			->createCommand("SELECT id FROM Titre t WHERE titre = :term")
			->queryColumn([':term' => $term]);
		return array_map('intval', $ids);
	}

	protected static function createCommand(string $term, array $filters = [], array $excludes = []): \CDbCommand
	{
		$db = Yii::app()->getComponent('sphinx');
		assert($db instanceof \components\sphinx\Connection);
		$cmd = $db->createCommand()
			->from('{{titres}}')
			->select('*')
			->order('cletri ASC')
			->limit(21);
		assert($cmd instanceof \CDbCommand);

		if ($term) {
			$issn = \SearchTitre::issnToInt($term);
			if ($issn) {
				$cmd->andWhere("issn = $issn");
			} else {
				$qterm = $db->quoteValue(\components\sphinx\Schema::quoteFulltext($term));
				$cmd->where("MATCH($qterm)");
			}
		}
		if ($filters) {
			foreach ($filters as $key => $values) {
				$cmd->andWhere(['in', $key, $values]);
			}
		}
		if ($excludes) {
			foreach ($excludes as $key => $values) {
				$cmd->andWhere(['not in', $key, $values]);
			}
		}
		return $cmd;
	}

	protected static function formatRow(array $row): array
	{
		return [
			'id' => (int) $row['id'],
			'value' => $row['titrecomplet'],
			'label' => $row['titrecomplet'],
		];
	}
}
