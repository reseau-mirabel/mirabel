<?php

namespace processes\completion;

use Yii;

class Categorie implements CompletionInterface
{
	/**
	 * Returns a list of assoc (id =>, value=> , label=>) matching the requested term.
	 *
	 * @param string $role ""|"public"|"candidat"|"refus" Defaults ""exclude 'refus'.
	 * @param int $minDepth (opt, 2)
	 * @return array[]
	 */
	public function completeTerm(string $term, string $role = "", int $minDepth = 2): array
	{
		$andWhere = " AND c.role " . ($role ? "=" : "<>") . " :role"
			. ($minDepth > 0 ? " AND c.profondeur >= $minDepth" : "");
		$cmd = Yii::app()->db->createCommand(<<<EOSQL
			SELECT
				c.id
				, c.categorie AS value
				, IF(c.role = 'candidat', CONCAT('[candidat] ',c.categorie), c.categorie) AS label
				, c.role, c.description, c.profondeur
				, IF(p.parentId IS NULL, '', p.categorie) AS parent
			FROM Categorie c
				JOIN Categorie p ON c.parentId = p.id
			WHERE (c.categorie LIKE :term OR c.description LIKE :descr)
				$andWhere
			ORDER BY c.categorie
			EOSQL
		);
		$eterm = str_replace(['%', '_'], ['\\%', '\\_'], $term);
		$params = [
			':term' => "%$eterm%",
			':descr' => "%$eterm%",
			':role' => $role ?: 'refus',
		];
		return $cmd->queryAll(true, $params);
	}

	public function findExactTerm(string $term): array
	{
		throw new \Exception("Not implemented");
	}
}
