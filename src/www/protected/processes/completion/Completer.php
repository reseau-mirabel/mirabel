<?php

namespace processes\completion;

class Completer
{
	public static function existsFor(string $className): bool
	{
		$path = __DIR__ . "/$className.php";
		return (preg_match('/^[A-Z][a-z]+$/', $className) && file_exists($path));
	}

	public static function type(string $className): CompletionInterface
	{
		if (!self::existsFor($className)) {
			throw new \Exception("Completing wrong object $className");
		}

		// Optional
		$path = __DIR__ . "/$className.php";
		require_once $path;

		$fqdn = "\\processes\\completion\\$className";
		return new $fqdn;
	}
}
