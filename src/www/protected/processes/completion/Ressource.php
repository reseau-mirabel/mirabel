<?php

namespace processes\completion;

use Ressource as Source;
use Yii;

class Ressource implements CompletionInterface
{
	/**
	 * Returns a list of assoc (id =>, value=> , label=>) matching the requested term.
	 *
	 * @param array $filters  array(field => array(values,to,require))
	 * @return array{id: int, value: string, label: string, forbid?: string}[]
	 */
	public function completeTerm(string $term, array $filters = [], bool $forbidAutoimport = false): array
	{
		$results = [];
		if (ctype_digit($term)) {
			$exact = array_filter([Source::model()->findByPk((int) $term)]);
		} else {
			$exact = Source::model()->findAllByAttributes(['nom' => $term]);
		}
		if ($exact && count($exact) === 1 && (!$filters || $exact[0]->autoImport == 0)) {
			assert($exact[0] instanceof Source);
			$results[] = [
				'id' => (int) $exact[0]->id,
				'value' => $exact[0]->getFullName(),
				'label' => $exact[0]->getFullName(),
				'forbid' => $forbidAutoimport && $exact[0]->autoImport
					? "Cette ressource est gérée automatiquement et ne peut pas être sélectionnée."
					: "",
			];
		}

		$cmd = self::createCommand($term, $filters);
		foreach ($cmd->query() as $row) {
			if (count($results) >= 20) {
				$results[] = ['id' => 0, 'value' => $term, 'label' => '…'];
				break;
			}
			if ($exact && $row['id'] === $exact[0]->id) {
				continue;
			}
			$results[] = [
				'id' => (int) $row['id'],
				'value' => (string) $row['nomcomplet'],
				'label' => (string) $row['nomcomplet'],
				'forbid' => $forbidAutoimport && $row['autoimport']
					? "Cette ressource est gérée automatiquement et ne peut pas être sélectionnée."
					: "",
			];
		}
		return $results;
	}

	/**
	 * Find the instances ID that have this exact name.
	 *
	 * @return int[]
	 */
	public function findExactTerm(string $term): array
	{
		$ids =  Yii::app()->db
			->createCommand("SELECT id FROM Ressource WHERE nom = :term")
			->queryColumn([':term' => $term]);
		return array_map('intval', $ids);
	}

	protected static function createCommand(string $term, array $filters = []): \CDbCommand
	{
		$db = Yii::app()->getComponent('sphinx');
		assert($db instanceof \CDbConnection);

		$cmd = $db->createCommand()
			->from('{{ressources}}')
			->select('*')
			->order('cletri ASC')
			->limit(21);
		assert($cmd instanceof \CDbCommand);

		if ($term) {
			$qterm = $db->quoteValue(\components\sphinx\Schema::quoteFulltext($term));
			$cmd->where("MATCH($qterm)");
		}
		if ($filters) {
			foreach ($filters as $key => $values) {
				$cmd->andWhere(['in', $key, $values]);
			}
		}
		return $cmd;
	}
}
