<?php

namespace processes\completion;

use Editeur as Source;
use Yii;

class Editeur implements CompletionInterface
{
	private const RESULT_SIZE = 20;

	/**
	 * Returns a list of assoc (id =>, value=> , label=>) matching the requested term.
	 *
	 * @param array $filters  array(field => array(values,to,require))
	 * @param array $excludes array(field => array(values,to,exclude))
	 * @return array{id: int, value: string, label: string}[]
	 */
	public function completeTerm(string $term, array $filters = [], array $excludes = []): array
	{
		$results = [];
		$cmd = self::createCommand($term, $filters, $excludes);
		foreach ($cmd->query() as $row) {
			if (count($results) >= self::RESULT_SIZE) {
				$results[] = ['id' => 0, 'value' => $term, 'label' => '…'];
				break;
			}
			$results[$row['id']] = [
				'id' => (int) $row['id'],
				'value' => $row['nomcomplet'],
				'label' => $row['nomcomplet'],
			];
		}

		usort($results, [self::class, 'compareResults']);

		if (!$filters) {
			if (preg_match("/^\d{8}[\dxX]$/", $term)) {
				$exact = Source::model()->findByAttributes(['idref' => $term]);
				if ($exact instanceof Source) {
					array_unshift($results, [
						'id' => (int) $exact->id,
						'value' => $exact->getFullName(),
						'label' => $exact->getFullName(Source::NAME_DATES),
					]);
				}
			} elseif (ctype_digit($term)) {
				$exact = Source::model()->findByPk((int) $term);
				if ($exact) {
					array_unshift($results, [
						'id' => (int) $exact->id,
						'value' => $exact->getFullName(),
						'label' => "[ID=$term] " . $exact->getFullName(Source::NAME_DATES),
					]);
				}
			} else {
				$exact = Source::model()->findAllByAttributes(['nom' => $term]);
				/** @var Source[] $exact */
				if ($exact && count($exact) === 1) {
					array_unshift($results, [
						'id' => (int) $exact[0]->id,
						'value' => $exact[0]->getFullName(),
						'label' => $exact[0]->getFullName(Source::NAME_DATES),
					]);
				}
			}
		}

		return $results;
	}

	/**
	 * Find the instances ID that have this exact name.
	 *
	 * @return int[]
	 */
	public function findExactTerm(string $term): array
	{
		$ids = Yii::app()->db
			->createCommand("SELECT id FROM Editeur WHERE nom = :term")
			->queryColumn([':term' => $term]);
		return array_map('intval', $ids);
	}

	public static function compareResults(array $a, array $b): int
	{
		if ($a['id'] === 0) {
			return 1;
		}
		if ($b['id'] === 0) {
			return -1;
		}
		return strcmp($a['label'], $b['label']);
	}

	private static function createCommand(string $term, array $filters = [], array $excludes = []): \CDbCommand
	{
		$db = Yii::app()->getComponent('sphinx');
		assert($db instanceof \components\sphinx\Connection);

		$cmd = $db->createCommand()
			->from('{{editeurs}}')
			->select('*, weight() as w')
			->order('weight() DESC')
			->limit(self::RESULT_SIZE + 1)
			->addOption('field_weights', '(nomcomplet=10, sigle=10, description=3, geo=1)');
		assert($cmd instanceof \CDbCommand);

		if ($term) {
			$qterm = $db->quoteValue(\components\sphinx\Schema::quoteFulltext($term));
			$cmd->andWhere("MATCH($qterm)");
		}
		if ($filters) {
			foreach ($filters as $key => $values) {
				$cmd->andWhere(['in', $key, $values]);
			}
		}
		if ($excludes) {
			foreach ($excludes as $key => $values) {
				// The Yii1 syntax ['not in', $key, $values] sometimes converts values to strings,
				// which fails with Sphinx which cares a bit about types.
				$ids = join(',', array_map('intval', $values));
				$cmd->andWhere("`$key` NOT IN ($ids)");
			}
		}
		return $cmd;
	}
}
