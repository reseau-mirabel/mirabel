<?php

namespace processes\completion;

use Collection as Source;

class Collection implements CompletionInterface
{
	/**
	 * Returns a list of assoc (id =>, value=> , label=>) matching the requested term.
	 *
	 * @return array Array of assoc (id => , value=> , label=>).
	 */
	public function completeTerm(string $term, int $ressourceId = 0): array
	{
		$escapedTerm = str_replace(['%', '_'], ['\\%', '\\_'], $term);
		$collections = Source::model()->findAll(
			[
				'condition' => "nom LIKE :term AND visible = 1"
					. ($ressourceId > 0 ? " AND ressourceId = " . (int) $ressourceId : ''),
				'order' => 'nom ASC',
				'params' => [ ':term' => "%$escapedTerm%" ],
			]
		);
		$results = [];
		foreach ($collections as $c) {
			$results[] = [
				'id' => (int) $c->id,
				'label' => $c->nom,
				'value' => $c->nom,
			];
		}
		return $results;
	}

	public function findExactTerm(string $term): array
	{
		throw new \Exception("Not implemented");
	}
}
