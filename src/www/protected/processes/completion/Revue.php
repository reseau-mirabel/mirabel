<?php

namespace processes\completion;

class Revue extends Titre implements CompletionInterface
{
	public function findExactTerm(string $term): array
	{
		throw new \Exception("Not implemented");
	}

	protected static function createCommand(string $term, $filters = [], $excludes = []): \CDbCommand
	{
		$cmd = parent::createCommand($term, $filters, $excludes);
		$cmd->select('revueid, titrecomplet');
		$cmd->group(['revueid' => "WITHIN GROUP ORDER BY obsolete ASC"]);
		$cmd->order('obsolete ASC');
		return $cmd;
	}

	protected static function formatRow(array $row): array
	{
		return [
			'id' => (int) $row['revueid'],
			'value' => $row['titrecomplet'],
			'label' => $row['titrecomplet'],
		];
	}
}
