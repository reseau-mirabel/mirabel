<?php

namespace processes\completion;

interface CompletionInterface
{
	public function completeTerm(string $term): array;

	public function findExactTerm(string $term): array;
}
