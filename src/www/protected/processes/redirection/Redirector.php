<?php

namespace processes\redirection;

class Redirector
{
	private const CLASSES = ['Titre', 'Revue', 'Editeur'];

	private \CActiveRecord $target;

	public function __construct(string $className)
	{
		if (!in_array($className, self::CLASSES)) {
			throw new \Exception("Wrong class name");
		}
		$this->target = $className::model();
	}

	public function getRoute(int $id): array
	{
		try {
			$redirected = $this->target->findBySql(<<<EOSQL
				SELECT t.*
				FROM {$this->target->tableName()} t
					JOIN Redirection{$this->target->tableName()} r ON r.cibleId = t.id
				WHERE r.sourceId = :id
				EOSQL,
				[':id' => $id]
			);
		} catch (\Throwable $_) {
			return [];
		}
		if ($redirected !== null && $redirected instanceof \WithSelfUrl) {
			return $redirected->getSelfUrl();
		}
		return [];
	}
}
