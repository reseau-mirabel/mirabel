<?php

namespace processes\redirection;

class Display
{
	private const OBJECTS = ['Titre', 'Revue', 'Editeur'];

	/**
	 * @var "Editeur"|"Revue"|"Titre"
	 */
	public string $object;

	public int $sourceId;

	public int $cibleId;

	public int $hdateModif;

	/**
	 * @param "Editeur"|"Revue"|"Titre" $object
	 */
	final public function __construct(string $object, int $old, int $new, int $timestamp)
	{
		if (!in_array($object, self::OBJECTS)) {
			throw new \Exception("Wrong object type");
		}
		$this->object = $object;
		$this->sourceId = $old;
		$this->cibleId = $new;
		$this->hdateModif = $timestamp;
	}

	public function getUrl(): string
	{
		return \Yii::app()->createAbsoluteUrl(
			strtolower($this->object) . '/view',
			['id' => $this->cibleId]
		);
	}

	public function getName(): string
	{
		$model = $this->object::model()->findByPk($this->cibleId);

		switch ($this->object) {
			case 'Titre':
				return $model->titre;
			case 'Editeur':
				return $model->nom;
			case 'Revue':
				return $model->getNom();
			default:
				throw new \Exception("Wrong class");
		}
	}

	/**
	 * @return self[]
	 */
	public static function fetchList(string $object): array
	{
		if (!in_array($object, self::OBJECTS)) {
			return [];
		}
		$sql = "SELECT * FROM Redirection{$object} ORDER BY sourceId";
		$redirections = \Yii::app()->db->createCommand($sql)->queryAll(true);
		$res = [];
		foreach ($redirections as $r) {
			$res[] = new static($object, (int) $r['sourceId'], (int) $r['cibleId'], (int) $r['hdateModif']);
		}
		return $res;
	}
}
