<?php

namespace processes\idref;

use Editeur;
use components\SqlHelper;
use models\import\ImportType;

class UpdateEditeur
{
	public int $verbose = 0;

	private array $countries;

	private array $countryNormalize;

	private int $numLogs = 0;

	private array $logs = [
		'OK' => [],
		"ERREUR d'enregistrement" => [],
		'IGNORÉ' => [],
		'DIVERGENCES' => [],
		'INCHANGÉ' => [],
	];

	public function __construct()
	{
		$this->countries = SqlHelper::sqlToPairs("SELECT code2, id FROM Pays");
		$this->countryNormalize = \Config::read('pays.correspondance');
	}

	public function getCsv(): string
	{
		if ($this->numLogs === 0) {
			return "";
		}
		$out = fopen('php://memory', 'w');
		fputcsv($out, ['ID', 'opération', 'nom', 'type', 'debut', 'ancien début', 'fin', 'ancienne fin', 'pays', 'alertes-erreurs'], ';', '"', '\\');
		foreach ($this->logs as $byType) {
			foreach ($byType as $row) {
				fputcsv($out, $row, ';', '"', '\\');
			}
		}
		return (string) stream_get_contents($out, -1, 0);
	}

	/**
	 * Update the DB (record matching the Editeur parameter) through an intervention.
	 */
	public function update(Editeur $e, Notice $notice): void
	{
		if ($notice->pays && isset($this->countryNormalize[$notice->pays])) {
			$notice->pays = $this->countryNormalize[$notice->pays];
		}

		$warning = $this->checkNoticeContent($notice);
		if ($warning) {
			$this->fputCsv($e, $notice, "IGNORÉ", "ERREUR, $warning");
			return;
		}

		if (!$notice->dateDebut && !$notice->dateFin && !$notice->pays && !$notice->ror) {
			// Pas de données en amont, on n'efface rien dans M.
			return;
		}

		$updatedFields = $this->compareFieldsDate($e, $notice);
		if (!$updatedFields) {
			if ($this->verbose > 0) {
				$this->fputCsv($e, $notice, "INCHANGÉ", "");
			}
			return;
		}

		if (($e->dateDebut && $notice->dateDebut && $notice->dateDebut !== $e->dateDebut)
			|| ($e->dateFin && $notice->dateFin && $notice->dateFin !== $e->dateFin)
		) {
			$this->fputCsv($e, $notice, "DIVERGENCE", "les dates M et idref.fr diffèrent");
			return;
		}
		if ($e->paysId && isset($updatedFields['paysId']) && (int) $e->paysId !== (int) $updatedFields['paysId']) {
			$paysM = array_flip($this->countries)[$e->paysId];
			$paysI = $notice->pays;
			$this->fputCsv($e, $notice, "DIVERGENCE", "les pays M et idref.fr diffèrent ($paysM / $paysI)");
			return;
		}
		if ($e->ror && $notice->ror && $notice->ror !== $e->ror) {
			$this->fputCsv($e, $notice, "DIVERGENCE", "Le ror M et idref.fr diffèrent ($e->ror / $notice->ror)");
			return;
		}

		// Pas de modification de date, mais au moins un remplissage.
		$i = $e->buildIntervention(true);
		$i->setScenario('import');
		$i->commentaire = "script d'interrogation de www.idref.fr";
		$i->import = ImportType::getSourceId('idref.fr');
		$i->description = "Modification de l'éditeur « {$e->nom} »";
		$i->action = 'editeur-U';
		$i->contenuJson->updateByAttributes($e, $updatedFields);
		$i->contenuJson->confirm = true;
		if ($i->contenuJson->isEmpty()) {
			if ($this->verbose === 0) {
				return;
			}
			$status = "INCHANGÉ";
			$updatedFields = [];
		} elseif ($i->accept(true)) {
			$status = "OK";
		} else {
			$status = "ERREUR d'enregistrement";
		}
		$this->fputCsv($e, $notice, $status, "modifications : " . join(" ", array_keys($updatedFields)));
	}

	private function compareFieldsDate(Editeur $e, Notice $notice): array
	{
		$updateData = [
			'dateDebut' => $notice->dateDebut,
			'dateFin' => $notice->dateFin,
			'paysId' => ($notice->pays && isset($this->countries[$notice->pays]) ? (int) $this->countries[$notice->pays] : null),
			'ror' => $notice->ror,
		];

		// Which fields have changed?
		$updatedFields = [];
		if ($updateData['dateDebut'] && $updateData['dateDebut'] !== $e->dateDebut) {
			$updatedFields['dateDebut'] = $updateData['dateDebut'];
		}
		if ($updateData['dateFin'] && $updateData['dateFin'] !== $e->dateFin) {
			$updatedFields['dateFin'] = $updateData['dateFin'];
		}
		if ($updateData['paysId'] && (int) $updateData['paysId'] !== (int) $e->paysId) {
			$updatedFields['paysId'] = $updateData['paysId'];
		}
		if ($updateData['ror'] && $updateData['ror'] !== $e->ror) {
			$updatedFields['ror'] = $updateData['ror'];
		}
		return $updatedFields;
	}

	private function fputCsv(Editeur $e, Notice $n, string $op, string $err): void
	{
		$this->numLogs += 1;
		$types = [
			Notice::TYPE_COLLECTIVITE => 'coll.',
			Notice::TYPE_PERSONNE => 'pers.',
		];
		$type = $types[$n->type] ?? $n->type;
		$this->logs[$op][] = [$e->id, $op, $e->nom, $type, $n->dateDebut, $e->dateDebut, $n->dateFin, $e->dateFin, $n->pays, $err];
	}

	private function checkNoticeContent(Notice $notice): string
	{
		if ($notice->getErrors()) {
			return join(", ", $notice->getErrors());
		}
		if ($notice->dateDebut && !preg_match('/^(\d{4})(-\d\d){0,2}$/', $notice->dateDebut)) {
			return "date de début incorrecte";
		}
		if ($notice->dateFin && !preg_match('/^(\d{4})(-\d\d){0,2}$/', $notice->dateFin)) {
			return "date de fin incorrecte";
		}
		if ($notice->pays && !isset($this->countries[$notice->pays])) {
			return "code alpha2 de pays incorrect";
		}
		return "";
	}
}
