<?php

namespace processes\idref;

use CHttpException;
use components\Curl;
use models\marcxml\MarcEditeur;
use models\marcxml\Parser;

class Notice
{
	public const TYPE_COLLECTIVITE = 'TB';

	public const TYPE_PERSONNE = 'TP';

	public string $type = '';

	public string $dateDebut = '';

	public string $dateFin = '';

	/**
	 * @var string 2 letters code our empty string
	 */
	public string $pays = '';

	public string $ror = '';

	private static $curl = null;

	private array $errors = [];

	public static function fetchByIdref(string $idref): self
	{
		if (!preg_match('/^[\dxX]+$/', $idref)) {
			throw new \Exception("L'idref n'est pas un PPN valide.");
		}

		if (self::$curl === null) {
			self::$curl = new Curl();
		}
		self::$curl->get("https://www.idref.fr/$idref.xml");
		$code = self::$curl->getHttpCode();
		if ($code !== 200) {
			throw new CHttpException($code);
		}

		$parser = new Parser();
		$marc = new MarcEditeur();
		$parser->parse(self::$curl->getContent(), $marc);

		$notice = new Notice();
		$notice->fromMarcXml($marc);
		return $notice;
	}

	public function fromMarcXml(MarcEditeur $marc): void
	{
		$this->type = strtoupper(substr($marc->getControl(8), 0, 2));

		$dates = $marc->getDataField('103', [' '], ' ');
		if ($this->type === self::TYPE_COLLECTIVITE) {
			$this->dateDebut = $this->extractDate($dates, 'a');
			$this->dateFin = $this->extractDate($dates, 'b');
		} elseif ($this->type === self::TYPE_PERSONNE) {
			$this->dateDebut = $this->extractDate($dates, 'c');
			$this->dateFin = $this->extractDate($dates, 'd');
		}

		$pays = $marc->getDataField('102', [" "])[0]['a'] ?? '';
		$this->pays = is_string($pays) ? $pays : ($pays[0] ?? '');
		$this->ror = self::extractRor($marc);
	}

	public function getErrors(): array
	{
		return $this->errors;
	}

	private static function extractRor(MarcEditeur $marc)
	{
		$dataFields = $marc->readFields('035');
		foreach ($dataFields as $field) {
			if (isset($field[2]) && $field[2] === 'ROR') {
				return $field['a'];
			}
		}
		return '';
	}

	private function extractDate(array $values, string $zone): string
	{
		$currentYear = (int) date('Y');
		foreach ($values as $a) {
			if (isset($a[$zone])) {
				$raw = trim($a[$zone], ' ');
				if ($raw === '') {
					continue;
				}
				$year = (int) substr($raw, 0, 4);
				if ($year < 1000 || $year > $currentYear) {
					$this->errors[] = "date '$raw'";
					continue;
				}
				$m = [];
				if (preg_match('/^(\d{4})(\d\d)(\d\d)$/', $raw, $m)) {
					return "{$m[1]}-{$m[2]}-{$m[3]}";
				}
				if (preg_match('/^(\d{4})(\d\d)$/', $raw, $m)) {
					return "{$m[1]}-{$m[2]}";
				}
				if (preg_match('/^(\d{4})$/', $raw, $m)) {
					return $raw;
				}
				$this->errors[] = "date '$raw'";
			}
		}
		return '';
	}
}
