<?php

namespace processes\images;

use CLogger;
use Exception;
use Yii;

class Downloader
{
	private const MIME_TO_EXT = [
		'text/html' => 'html',
		'image/jpeg' => 'jpg',
		'image/jpg' => 'jpg',
		'image/png' => 'png',
		'image/gif' => 'gif',
		'image/svg+xml' => 'svg',
		'image/webp' => 'webp',
	];

	/**
	 * @var string For sprintf use, without the file extension.
	 */
	public string $filePattern = '%09d';

	protected string $path;

	protected string $pathRaw;

	protected $curl;

	/**
	 * @param string $path Where to put the final image (after resizing).
	 * @param string $pathRaw Where to put the raw image (before resizing).
	 */
	public function __construct(string $path, string $pathRaw)
	{
		$this->path = $path;
		$this->pathRaw = $pathRaw;

		$this->checkAccess($this->path);
		$this->checkAccess($this->pathRaw);

		$this->curl = curl_init();
		curl_setopt_array(
			$this->curl,
			[
				CURLOPT_FOLLOWLOCATION => true,    // follow redirection (301, etc)
				CURLOPT_MAXREDIRS => 4,
				CURLOPT_TIMEOUT => 5,              // 5 seconds max
				CURLOPT_SSL_VERIFYHOST => false,
				CURLOPT_SSL_VERIFYPEER => false,
				CURLOPT_COOKIEFILE => "",          // store cookies
				CURLOPT_USERAGENT => 'Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0',
			]
		);
	}

	public function __destruct()
	{
		curl_close($this->curl);
	}

	/**
	 * Get the name of the file where to download the raw image, before resizing.
	 *
	 * @param int|string $identifier
	 * @return string full path
	 */
	public function getRawImageFileName($identifier): string
	{
		return $this->pathRaw . '/' . sprintf($this->filePattern, $identifier);
	}

	/**
	 * Download the image from a distant source.
	 *
	 * @param int|string $identifier
	 * @param string $url
	 * @return bool Image downloaded?
	 */
	public function download($identifier, string $url): bool
	{
		$filename = $this->getRawImageFileName($identifier);
		$oldFiles = glob($filename . ".*");
		$filenameTmp = $filename . ".tmp";
		$this->downloadWithCurl($url, $filenameTmp);

		if (!@chmod($filenameTmp, 0664)) {
			Yii::log("Could not chmod on '$filenameTmp' in download().", CLogger::LEVEL_WARNING);
		}
		$size = filesize($filenameTmp);
		Yii::log("ImageDownloader: téléchargement de l'image $filenameTmp de $size octets", "info");
		if ($size < 400) {
			Yii::log("ImageDownloader: suppression de l'image vide $filenameTmp", "error");
			unlink($filenameTmp);
			throw new Exception("Image de $size octets, forcément vide.");
		}
		if ($oldFiles) {
			if ($size === filesize($oldFiles[0]) && md5_file($filenameTmp) === md5_file($oldFiles[0])) {
				Yii::log("ImageDownloader: image $filename identique à l'ancienne", "info");
				unlink($filenameTmp);
				return true;
			}
		}
		$ext = $this->getImageExtension($filenameTmp, curl_getinfo($this->curl, CURLINFO_CONTENT_TYPE) ?: '', $url);
		$newname = "$filename.$ext";
		//Supression des anciens fichiers
		if ($oldFiles) {
			foreach ($oldFiles as $oldFile) {
				Yii::log("ImageDownloader: suppression de l'ancienne image $oldFile", "info");
				unlink($oldFile);
			}
		}
		if (!rename($filenameTmp, $newname)) {
			throw new Exception("Erreur de permission de fichiers, impossible de renommer.");
		}
		if (!@chmod($newname, 0664)) {
			Yii::log("Could not chmod on '$newname'.", CLogger::LEVEL_WARNING);
		}
		return true;
	}

	/**
	 * @param int|string $identifier
	 * @return bool Success?
	 */
	public function delete($identifier): bool
	{
		$globPattern = $this->pathRaw . sprintf('/' . $this->filePattern, $identifier) . '.{jpg,gif,png,svg,webp}';
		$filenames = glob($globPattern, GLOB_BRACE);
		if ($filenames) {
			foreach ($filenames as $f) {
				Yii::log("ImageDownloader: suppression de l'ancienne image $f", "info");
				unlink($f);
			}
		}
		$final = glob($this->path . '/' . sprintf($this->filePattern, $identifier) . '.*');
		if ($final) {
			Yii::log("ImageDownloader: suppression de l'ancienne image {$final[0]}", "info");
			unlink($final[0]);
		}
		return true;
	}

	protected function checkAccess(string $dir)
	{
		if (!is_dir($dir)) {
			if (!mkdir($dir, 0777, true)) {
				throw new Exception("Impossible de créer le répertoire où les images seront écrites.");
			}
		}
		if (!is_writable($dir) || !is_executable($dir)) {
			$user = posix_getpwuid(posix_geteuid())['name'];
			throw new Exception("Erreur de droit d'accès pour '$user' sur le répertoire où les images seront écrites: $dir");
		}
	}

	/**
	 * Return the file image extension. If an error occur remove the temp file.
	 *
	 * @param string $filenameTmp
	 * @param string $mimeType cURL will set it to false if no MIME header in the HTTP response
	 * @param string $url (only used if non mime-type is given)
	 * @return string New file name
	 */
	protected function getImageExtension(string $filenameTmp, string $mimeType, string $url = ''): string
	{
		$ext = self::findImageExtension($mimeType, $url, $filenameTmp);
		if (!$ext) {
			unlink($filenameTmp);
			throw new Exception("Mime-type vide ou inconnu: '$mimeType'");
		}
		if ($ext === 'html') {
			unlink($filenameTmp);
			throw new Exception("Erreur : reçu du HTML et non une image");
		}
		return $ext;
	}

	/**
	 * Encode an URL that may contain non-ASCII characters (e.g. http://www.openedition.org/iss_600×281.png).
	 *
	 * @param string $url
	 * @return string
	 */
	protected function encodeUrl(string $url): string
	{
		if (strpos($url, '%') !== false) {
			return $url; // no double-encoding
		}
		$parts = parse_url($url);
		if (!empty($parts['path'])) {
			$parts['path'] = join('/', array_map('rawurlencode', explode('/', $parts['path'])));
		}
		$query = [
			isset($parts['scheme']) ? $parts['scheme'] . '://' : '',
			$parts['host'] ?? '',
			isset($parts['port']) ? ':' . $parts['port'] : '',
			$parts['path'] ?? '',
			isset($parts['query']) ? '?' . $parts['query'] : '',
		];
		return implode('', array_filter($query));
	}

	protected static function findImageExtension(string $mimeType, string $url, string $file): string
	{
		// some websites (Persée) append a charset to images!
		$mime = strtok((string) $mimeType, ';');
		if (!$mime || !isset(self::MIME_TO_EXT[$mime])) {
			$mime = \CFileHelper::getMimeType($file);
		}

		if (!$mime) {
			// no mime-type, guess from extension
			$m = [];
			if (preg_match('/\.(png|jpg|gif|svg|webp)(\?|$)/i', $url, $m)) {
				return strtolower($m[1]);
			}
			if (preg_match('/\.jpeg(\?|$)/i', $url, $m)) {
				return 'jpg';
			}
			// No mime-type in the headers, no extension in the URL...
			return '';
		}
		return self::MIME_TO_EXT[$mime] ?? '';
	}

	private function downloadWithCurl(string $url, string $filenameTmp)
	{
		$fh = @fopen($filenameTmp, "w");
		if (!$fh) {
			throw new Exception("Impossible de créer ou modifier le fichier en écriture !");
		}
		curl_setopt($this->curl, CURLOPT_FILE, $fh);
		curl_setopt($this->curl, CURLOPT_URL, $this->encodeUrl($url));
		if (curl_exec($this->curl) === false) {
			throw new Exception("Erreur de téléchargement : " . curl_error($this->curl));
		}
		$code = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);
		if ($code < 200 || $code >= 300) {
			throw new Exception("Permission refusée ? Code HTTP " . $code);
		}
		fclose($fh);
	}
}
