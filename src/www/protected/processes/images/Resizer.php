<?php

namespace processes\images;

use CFileHelper;
use CLogger;
use Error;
use Exception;
use Yii;

class Resizer
{
	public string $boundingbox = "256x256>";

	/**
	 * @var string For sprintf use, without the file extension.
	 */
	public string $filePattern = '%09d';

	private string $finalPath;

	private string $rawPath;

	/**
	 * @param string $finalPath Where to put the final image (after resizing).
	 * @param string $rawPath Where to put the raw image (before resizing).
	 */
	public function __construct(string $finalPath, string $rawPath)
	{
		$this->finalPath = $finalPath;
		$this->rawPath = $rawPath;
	}

	/**
	 * [width, height] to fit into a bounding box.
	 *
	 * @return list{int, int} [width, height], where [0, 0] means an error occured.
	 */
	public static function computeSize(int $width, int $height, int $maxWidth, int $maxHeight): array
	{
		if (($width <= 0) || ($height <= 0)) {
			return [0, 0];
		}
		if ($width <= $maxWidth && $height <= $maxHeight) {
			return [$width, $height];
		}
		$newWidth = (int) round($width * $maxHeight / $height);
		if ($newWidth > $maxWidth) {
			$newWidth = $maxWidth;
			$newHeight = (int) round($height * $maxWidth/ $width);
		} else {
			$newHeight = $maxHeight;
		}
		return [$newWidth, $newHeight];
	}

	/**
	 * Resize all the downloaded bitmap image files.
	 */
	public function resizeAll(): array
	{
		$images = glob($this->rawPath . "/*.{jpg,gif,png,svg,webp}", GLOB_BRACE);
		printf("Parcourt %d images pour traiter les changements...\n", count($images));
		$actions = [
			"vignette à jour" => 0,
			"vignette créée" => 0,
			"vignette mise à jour" => 0,
			"erreurs" => 0,
		];
		foreach ($images as $source) {
			try {
				$a = $this->resizeFile($source);
				$actions[$a]++;
			} catch (Error $e) {
				Yii::log($e->getMessage(), 'error');
				$actions['erreurs']++;
			} catch (Exception $e) {
				Yii::log($e->getMessage(), 'error');
				$actions['erreurs']++;
			}
		}
		return $actions;
	}

	/**
	 * Resize a bitmap file into PNG, or copies SVG vectorial files.
	 *
	 * @param string $sourcePath
	 * @param bool $force Force a resize, even if the source file is older.
	 * @return string Description of the operation
	 */
	public function resizeFile(string $sourcePath, bool $force = false): string
	{
		if (preg_match('/\.svg$/', $sourcePath)) {
			return $this->resizeSvgFile($sourcePath, $force);
		}

		$name = basename($sourcePath);
		$oldFiles = glob(preg_replace('/\.\w+$/', '.*', "{$this->finalPath}/$name"));
		if ($oldFiles) {
			foreach ($oldFiles as $f) {
				if (!preg_match('/\.png$/', $f)) {
					unlink($f);
				}
			}
		}
		$out = $this->finalPath . "/" . preg_replace('/\.\w+$/', '', $name) . '.png';
		if (!$force && file_exists($out) && filemtime($out) > filemtime($sourcePath)) {
			return "vignette à jour";
		}
		$maj = file_exists($out);
		$size = filesize($sourcePath);
		if ($size < 400) {
			copy($sourcePath, $out);
		} else {
			if (!$this->resizeImageFile($sourcePath, $out)) {
				throw new Exception("Erreur de traitement d'image. Imagemagick est-il installé ?");
			}
			$multiple = glob(preg_replace('/\.png$/', '-*.png', $out));
			if ($multiple) {
				rename(array_shift($multiple), $out);
				array_map('unlink', $multiple);
			}
		}
		if (!@chmod($out, 0664)) {
			Yii::log("Could not chmod on '$out'.", CLogger::LEVEL_WARNING);
		}
		return ($maj ? "vignette mise à jour" : "vignette créée");
	}

	/**
	 * Resize gif|jpg|png, copies svg.
	 *
	 * @param int|string $identifier Identifier of the image (for filePattern).
	 * @param bool $force Force a resize, even if the source file is older.
	 * @return bool Success?
	 */
	public function resizeByIdentifier($identifier, bool $force = false): bool
	{
		$globPattern = $this->rawPath . '/' . sprintf($this->filePattern, $identifier) . '.{jpg,gif,png,svg}';
		$filename = glob($globPattern, GLOB_BRACE);
		if ($filename) {
			$this->resizeFile($filename[0], $force);
			return true;
		}
		return false;
	}

	protected function resizeImageFile(string $in, string $out): bool
	{
		$escIn = escapeshellarg($in);
		$escOut = escapeshellarg($out);
		system("convert -quiet $escIn -thumbnail '{$this->boundingbox}' -unsharp 0x.5 $escOut");
		if (file_exists($out)) {
			return true;
		}

		// If ImageMagick failed, we use the PHP internal tools.
		return $this->resizeWithPurePhp($in, $out);
	}

	protected function resizeSvgFile(string $sourcePath, bool $force): string
	{
		$name = basename($sourcePath);
		$oldFiles = glob(preg_replace('/\.\w+$/', '.*', "{$this->finalPath}/$name"));
		if ($oldFiles) {
			foreach ($oldFiles as $f) {
				if (!preg_match('/\.svg$/', $f)) {
					unlink($f);
				}
			}
		}
		$out = "{$this->finalPath}/$name";
		if (!$force && file_exists($out) && filemtime($out) > filemtime($sourcePath)) {
			return "vignette à jour";
		}
		$maj = file_exists($out);
		copy($sourcePath, $out);
		if (!@chmod($out, 0664)) {
			Yii::log("Could not chmod on '$out'.", CLogger::LEVEL_WARNING);
		}
		return ($maj ? "vignette mise à jour" : "vignette créée");
	}

	/**
	 * @codeCoverageIgnore
	 */
	private function resizeWithPurePhp(string $in, string $out): bool
	{
		$type = CFileHelper::getMimeType($in);
		switch ($type) {
			case "image/png":
				$image = imagecreatefrompng($in);
				break;
			case "image/jpeg":
				$image = imagecreatefromjpeg($in);
				break;
			case "image/gif":
				$image = imagecreatefromgif($in);
				break;
			default:
				throw new Exception("Le type MIME '$type' est inconnu.");
		}

		$oldWidth = (int) imagesx($image);
		if ($oldWidth < 4) { // min input width
			return false;
		}
		$oldHeight = (int) imagesy($image);
		[$maxWidth, $maxHeight] = explode("x", $this->boundingbox);
		[$width, $height] = self::computeSize($oldWidth, $oldHeight, (int) $maxWidth, (int) $maxHeight);
		if ($width === 0) {
			return false;
		}
		$resized = imagescale($image, $width, $height, \IMG_BICUBIC_FIXED);
		if ($resized === false) {
			$resized = imagescale($image, $width, $height, \IMG_BILINEAR_FIXED);
		}
		if ($resized === false) {
			throw new Exception("Erreur en redimmensionnant l'image '{$in}' en {$width}x{$height}");
		}
		return imagepng($resized, $out);
	}
}
