<?php

namespace processes\kbart\convert;

use processes\kbart\read\Header;
use processes\kbart\read\ReaderCsv;

class FromErudit
{
	private string $tmpFile;

	public function __construct()
	{
		$this->tmpFile = tempnam(sys_get_temp_dir(), 'mirabel_kbart-erudit_');
	}

	public function __destruct()
	{
		if (is_file($this->tmpFile)) {
			unlink($this->tmpFile);
		}
	}

	public function load(string $path): void
	{
		if (!is_readable($path)) {
			throw new \Exception("Le fichier Érudit '$path' est soit introuvable, soit interdit à la lecture.");
		}
		$output = fopen($this->tmpFile, 'w');
		if ($output === false) {
			throw new \Exception("Impossible de créer un fichier temporaire pour KBART-Erudit.");
		}

		$csvReader = new ReaderCsv($path, "\t");
		$firstRow = $csvReader->readSkippingEmpty();

		$header = new Header();
		$header->read($firstRow);

		// Write the header
		$header->setRowData($firstRow);
		$outHeaderCells = self::createDefaultAccess($header); // Copy non-mirabel columns.
		$outHeaderCells['notes'] = "notes";
		fputcsv($output, $outHeaderCells, "\t", '"', '\\');

		while ($row = $csvReader->readSkippingEmpty()) {
			$header->setRowData($row);
			if ($header->getColumn('access_type') === '') {
				// Pas encore en ligne, on passe.
				continue;
			}
			$converted = [];
			if ($header->getColumn('access_type') === 'F') {
				$converted[] = self::createDefaultAccess($header);
			} else {
				if ($header->getOptionalColumn('mirabel_date_first_issue_open')) {
					$converted[] = self::createOpenAccess($header);
				}
				if ($header->getOptionalColumn('mirabel_date_first_issue_restricted')) {
					$converted[] = self::createRestrictedAccess($header);
				} else {
					$converted[] = self::createDefaultAccess($header);
				}
			}
			foreach ($converted as $c) {
				fputcsv($output, array_values($c), "\t", '"', '\\');
			}
		}
		$header->unsetRowData();
		fclose($output);
	}

	public function writeIntoFile(string $path): bool
	{
		return copy($this->tmpFile, $path);
	}

	/**
	 * @param resource $h
	 */
	public function writeIntoHandler($h): bool
	{
		return fwrite($h, file_get_contents($this->tmpFile)) > 0;
	}

	private static function createDefaultAccess(Header $header): array
	{
		$r = [];
		foreach ($header->listColumns() as $col) {
			if (!str_starts_with($col, 'mirabel_')) {
				$r[$col] = $header->getColumn($col);
			}
		}
		$r['notes'] = trim($r['notes'] . " " . $header->getOptionalColumn('mirabel_warning'));
		return $r;
	}

	private static function createOpenAccess(Header $header): array
	{
		$r = self::createDefaultAccess($header);
		$r['access_type'] = 'F';
		$r['date_first_issue_online'] = $header->getOptionalColumn('mirabel_date_first_issue_open') ?? '';
		$r['num_first_vol_online'] = $header->getOptionalColumn('mirabel_num_first_vol_open') ?? '';
		$r['num_first_issue_online'] = $header->getOptionalColumn('mirabel_num_first_issue_open') ?? '';
		$r['date_last_issue_online'] = $header->getOptionalColumn('mirabel_date_last_issue_open') ?? '';
		$r['num_last_vol_online'] = $header->getOptionalColumn('mirabel_num_last_vol_open') ?? '';
		$r['num_last_issue_online'] = $header->getOptionalColumn('mirabel_num_last_issue_open') ?? '';
		$r['embargo_info'] = $header->getColumn('embargo_info') ?: $header->getOptionalColumn('mirabel_embargo_info') ?? '';
		return $r;
	}

	private static function createRestrictedAccess(Header $header): array
	{
		$r = self::createDefaultAccess($header);
		$r['access_type'] = 'P';
		$r['date_first_issue_online'] = $header->getOptionalColumn('mirabel_date_first_issue_restricted') ?? '';
		$r['num_first_vol_online'] = $header->getOptionalColumn('mirabel_num_first_vol_restricted') ?? '';
		$r['num_first_issue_online'] = $header->getOptionalColumn('mirabel_num_first_issue_restricted') ?? '';
		$r['embargo_info'] = $header->getColumn('embargo_info') ?: $header->getOptionalColumn('mirabel_embargo_info') ?? '';
		return $r;
	}
}
