<?php

namespace processes\kbart\convert;

class Convert
{
	private string $path;

	public function __construct(string $path)
	{
		$this->path = $path;
	}

	public function apply(): string
	{
		$header = $this->readHeader();
		if (strpos($header, "\tmirabel_date_first_issue_open\t") !== false) {
			$converter = new FromErudit();
			$converter->load($this->path);
			$path = sprintf('%s/%s_conv.txt', sys_get_temp_dir(), preg_replace('/\.\w+$/', '', basename($this->path)));
			if (!$converter->writeIntoFile($path)) {
				throw new \Exception("Écriture impossible dans '{$path}'.");
			}
			return $path;
		}
		return $this->path;
	}

	private function readHeader(): string
	{
		$fh = fopen($this->path, "r");
		if ($fh === false) {
			throw new \Exception("Lecture impossible pour '{$this->path}'.");
		}
		$header = fgets($fh);
		fclose($fh);
		if ($header === false) {
			return '';
		}
		return $header;
	}
}
