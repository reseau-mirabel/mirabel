<?php

namespace processes\kbart;

use processes\kbart\logger\LogEvent;
use processes\kbart\logger\LogStore;

/**
 * Logger for import events, either local (on the current row) or global (the whole file or process).
 */
class Logger
{
	private LogStore $globalStore;

	/**
	 * @var array<int, LogStore> Index by row number.
	 */
	private array $localStores;

	/**
	 * @var LogStore Contains only the aggregated events over all the rows.
	 */
	private LogStore $localSummary;

	/**
	 * @var array Context for the logged events.
	 */
	private array $context = [
		'file' => '', // global + local
		'row' => 0, // local
		'startTime' => null, // global + local
	];

	public function __construct()
	{
		$this->reset();
	}

	public function init(): void
	{
		$this->context['startTime'] = time();
	}

	/**
	 * Add a global event.
	 */
	public function addGlobal(LogEvent $event): void
	{
		$this->globalStore->addEvent($event);
	}

	/**
	 * Add an event relative to the current row.
	 * @see setRowContext
	 */
	public function addLocal(LogEvent $event): void
	{
		if ($event->family) {
			$this->localSummary->addEvent(clone $event);
		}

		$context = (int) $this->context['row'];
		if ($context <= 0) {
			// No context, so we do not store the local event
			\Yii::log("Local event without context: $event->message", 'warning', 'kbart');
			return;
		}
		if (!isset($this->localStores[$context])) {
			$this->localStores[$context] = new LogStore();
		}
		$this->localStores[$context]->addEvent($event);
	}

	public function getGlobalLog(): LogStore
	{
		return $this->globalStore;
	}

	/**
	 * Return the number of row which have events.
	 */
	public function getLocalCount(): int
	{
		return count($this->localStores);
	}

	public function getLocalSummary(): LogStore
	{
		return $this->localSummary;
	}

	public function getRowLog(int $row): LogStore
	{
		if (!isset($this->localStores[$row])) {
			return new LogStore();
		}
		return $this->localStores[$row];
	}

	public function reset(): void
	{
		$this->globalStore = new LogStore();
		$this->localStores = [];
		$this->localSummary = new LogStore();
		$this->localSummary->mergeByFamily = true;
		$this->context = [
			'startTime' => time(), // global + local
			'file' => '', // global + local
			'row' => 0, // local
		];
	}

	public function setFileContext(string $filename): void
	{
		$this->context['file'] = $filename;
	}

	public function setRowContext(int $position): void
	{
		$this->context['row'] = $position;
	}
}
