<?php

namespace processes\kbart\data;

class DiffCollection
{
	private array $statsByStatus = [
		Diff::STATUS_EMPTY => 0,
		Diff::STATUS_ERROR => 0,
		Diff::STATUS_JOURNAL_UPDATE => 0,
		Diff::STATUS_UPDATE => 0,
		Diff::STATUS_CREATE => 0,
		Diff::STATUS_DELETE => 0,
	];

	/**
	 * @var Diff[]
	 */
	private array $diffs = [];

	public function addDiff(int $titreId, Diff $diff): void
	{
		$this->statsByStatus[$diff->status]++;
		$this->diffs[$titreId] = $diff;
	}

	public function getDiff(int $titreId): ?Diff
	{
		return $this->diffs[$titreId] ?? null;
	}

	/**
	 * @return Diff[]
	 */
	public function getDiffs(): array
	{
		return $this->diffs;
	}

	public function getStatsByStatus(): array
	{
		return $this->statsByStatus;
	}
}
