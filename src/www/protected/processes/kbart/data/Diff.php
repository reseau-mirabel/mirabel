<?php

namespace processes\kbart\data;

use processes\kbart\work\DiffServices;

/**
 * All the potential changes on a journal (a Titre record and its related data, like Service...).
 */
class Diff
{
	public const STATUS_EMPTY = 0;

	public const STATUS_ERROR = 1;

	public const STATUS_JOURNAL_UPDATE = 2; // May have also an access change

	public const STATUS_UPDATE = 4;

	public const STATUS_CREATE = 8; // Only create

	public const STATUS_DELETE = 16; // Only delete

	public int $status = 0;

	public ?DiffServices $services = null;

	public string $erreur = "";

	/**
	 * @var array If not changed, []. If creation, ["", new]. If update, [old, new, id-in-db].
	 */
	public array $identification = [];

	/**
	 * @var array attribute => newValue (if not a creation, only date fields?)
	 */
	public array $titre = [];

	/**
	 * @var array<string, int> ISSN => revueId
	 */
	public array $issn = [];

	/**
	 * @var array Only when creating a new Titre record.
	 *     Values are an int `Editeur.id` or an array [attribute => value].
	 */
	public array $publishers = [];
}
