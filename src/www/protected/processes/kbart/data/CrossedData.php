<?php

namespace processes\kbart\data;

use processes\kbart\read\RowData;
use Titre;

class CrossedData
{
	/**
	 * @var RowData[] KBART raw data for unknown titles, without index.
	 */
	public array $unknown = [];

	/**
	 * @var Analyzed[] For each title ID, an Analyzed structure holds:
	 *  - the Title record,
	 *  - the raw KBART data (array since several rows may match this title),
	 *  - the existings Service records.
	 * The Service changes will be computed later on.
	 */
	public array $known = [];

	/**
	 * @var null|Titre[]
	 */
	private ?array $missing = null;

	public function addUnknown(RowData $row): void
	{
		$this->unknown[] = $row;
	}

	/**
	 * Store this Title among those known of Mirabel.
	 *
	 * @param RowData $row
	 * @param \Titre $title
	 * @param \Service[] $services
	 */
	public function addKnown(RowData $row, \Titre $title, array $services): void
	{
		if (isset($this->known[$title->id])) {
			$this->known[$title->id]->addRow($row);
		} else {
			$this->known[$title->id] = new Analyzed($row, $title, $services);
		}
	}

	/**
	 * @param int[] $collectionIds
	 * @return Titre[]
	 */
	public function findMissingJournals(int $ressourceId, array $collectionIds): array
	{
		if (!isset($this->missing)) {
			$cmd = \Yii::app()->db->createCommand()
				->from("Service s")
				->where("s.ressourceId = $ressourceId")
				->andWhere("s.import = " . \models\import\ImportType::getSourceId('KBART'))
				->select('s.titreId')
				->group('s.titreId');
			if ($collectionIds) {
				$cmd->join("Service_Collection sc", "sc.serviceId = s.id");
				$cmd->andWhere(['in', 'sc.collectionId', $collectionIds]);
			}
			$ids = [];
			foreach ($cmd->queryColumn() as $id) {
				if (!isset($this->known[$id])) {
					$ids[] = (int) $id;
				}
			}
			if ($ids) {
				$idsStr = join(",", $ids);
				$this->missing = Titre::model()->findAllBySql("SELECT * FROM Titre WHERE id IN ($idsStr) ORDER BY titre");
			} else {
				$this->missing = [];
			}
		}
		return $this->missing;
	}
}
