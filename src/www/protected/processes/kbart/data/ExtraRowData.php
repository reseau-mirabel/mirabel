<?php

namespace processes\kbart\data;

class ExtraRowData
{
	public int $position;

	public ?\Titre $title;

	/**
	 * @var \Service[]
	 */
	public array $services = [];
}
