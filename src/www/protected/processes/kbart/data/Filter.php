<?php

namespace processes\kbart\data;

class Filter
{
	/**
	 * @var ""|"F"|"P"
	 */
	public string $accessType = '';

	public function __construct(array $f = [])
	{
		foreach ($f as $k => $v) {
			$this->{$k} = $v;
		}
	}

	public function accept(\processes\kbart\read\RowData $row): bool
	{
		if ($this->accessType) {
			if ($row->accessType !== $this->accessType) {
				return false;
			}
		}
		return true;
	}

	public function summary(): string
	{
		$text = [];
		if ($this->accessType === 'F') {
			$text[] = "Accès libres";
		} elseif ($this->accessType === 'P') {
			$text[] = "Accès restreints";
		}
		return join(", ", $text);
	}

	public function toArray(): array
	{
		$a = [];
		if ($this->accessType) {
			$a['accessType'] = $this->accessType[0];
		}
		return $a;
	}
}
