<?php

namespace processes\kbart\data;

use processes\kbart\read\RowData;
use Titre;

class Analyzed
{
	/**
	 * @var RowData[] Rows that match this journal
	 */
	public array $rows = [];

	public Titre $title;

	/**
	 * @var \Service[] Existing services
	 */
	public array $services = [];

	public function __construct(RowData $row, Titre $title, array $services)
	{
		$this->rows = [$row];
		$this->title = $title;
		$this->services = $services;
	}

	public function addRow(RowData $row): void
	{
		$this->rows[] = $row;
	}
}
