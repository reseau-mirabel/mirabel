<?php

namespace processes\kbart\data;

use CHtml;
use components\HtmlHelper;
use Intervention;
use Issn;
use models\import\ImportType;
use processes\kbart\read\RowData;
use Titre;
use Yii;

class WritingStats
{
	/**
	 * @var array[] Unchanged journals: [Titre.id => Info about the journal]
	 */
	public array $ignored = [];

	/**
	 * @var array[] Each item is an associative array with info about the intervention and journal.
	 */
	public array $interventionFailures = [];

	/**
	 * @var array[] Each item is an associative array with info about the intervention and journal.
	 */
	public array $interventionSuccesses = [];

	public int $interventionTotal = 0;

	/**
	 * @var array<string, int> The keys also are keys of Intervention::ACTIONS.
	 */
	public array $interventionTypes = [];

	public int $numIdentifiedJournals = 0;

	/**
	 * @var array[] Unchanged journals: [Titre.id => Info about the journal]
	 */
	public array $unchanged = [];

	public array $unknown = [];

	private int $ressourceId;

	private int $writingStart = 0;

	private int $writingEnd = 0;

	public function __construct(int $ressourceId)
	{
		$this->ressourceId = $ressourceId;
		$this->writingStart = time();
	}

	public function addFailure(Intervention $i): void
	{
		$info = $this->addIntervention($i);
		$info['errors'] = HtmlHelper::errorSummary($i, "");
		$this->interventionFailures[] = $info;
	}

	public function addIgnored(Titre $t, string $error): void
	{
		$revueUrl = self::toAbsoluteUrl($t->id ? $t->getRevueUrl() : "");
		$this->ignored[$t->id] = [
			'description' => "",
			'errors' => $error,
			'revueUrl' => $revueUrl,
			'titre' => $t->titre,
			'titreLink' => $t->id ? CHtml::link(CHtml::encode($t->titre), $revueUrl) : '',
			'url' => '',
		];
	}

	public function addSuccess(Intervention $i): void
	{
		$this->interventionSuccesses[] = $this->addIntervention($i);
	}

	public function addUnchanged(Titre $t): void
	{
		$revueUrl = self::toAbsoluteUrl($t->getRevueUrl());
		$this->unchanged[$t->id] = [
			'description' => "Aucun changement",
			'errors' => '',
			'revueUrl' => $revueUrl,
			'titre' => $t->titre,
			'titreLink' => CHtml::link(CHtml::encode($t->titre), $revueUrl),
			'url' => '',
		];
	}

	public function addUnknown(RowData $row): void
	{
		$hash = md5($row->title);
		if (isset($this->unknown[$hash])) {
			return;
		}

		$issns = trim("{$row->issn} {$row->issne}");
		$extra = [
			'issnSupport' => [],
			'publishers' => [],
		];
		if ($row->issn) {
			$extra['issnSupport'][$row->issn] = Issn::SUPPORT_PAPIER;
		}
		if ($row->issne) {
			$extra['issnSupport'][$row->issne] = Issn::SUPPORT_ELECTRONIQUE;
		}
		if ($row->publisherName) {
			$names = preg_split('/\s*;\s*/', $row->publisherName);
			foreach ($names as $n) {
				$publishers = Yii::app()->db->createCommand("SELECT id FROM Editeur WHERE nom = :n")
					->queryColumn([':n' => $n]);
				if ($publishers && count($publishers) === 1) {
					$extra['publishers'][$publishers[0]] = $n;
				}
			}
		}
		$e = array_filter($extra);
		if ($issns) {
			$url = ['/titre/create-by-issn', 'issn' => $issns];
			if ($e) {
				$url['extra'] = json_encode($e);
			}
		} else {
			$url = [];
		}
		$this->unknown[$hash] = [
			'title' => $row->title,
			'url' => $url,
		];
	}

	public function getInterventionsUrl(): array
	{
		if ($this->writingEnd === 0) {
			$this->writingEnd = time();
		}
		return [
			'/intervention/admin',
			'q' => [
				'ressourceId' => $this->ressourceId,
				'import' => ImportType::getSourceId('KBART'),
				'hdateVal' => "{$this->writingStart}-{$this->writingEnd}",
			],
		];
	}

	private function addIntervention(Intervention $i): array
	{
		$this->interventionTotal++;

		if (isset($this->interventionTypes[$i->action])) {
			$this->interventionTypes[$i->action]++;
		} else {
			$this->interventionTypes[$i->action] = 1;
		}

		$description = $i->description;
		if ($i->commentaire) {
			$description .= "\n    " . trim(str_replace("\n", "\n    ", $i->commentaire));
		}

		$result = [
			'description' => $description,
			'errors' => "",
			'revueUrl' => '',
			'titre' => "à créer",
			'titreLink' => "à créer",
			'url' => Yii::app()->createAbsoluteUrl('/intervention/view', ['id' => $i->id]),
		];
		if ($i->titreId > 0) {
			$titre = $i->titre;
			$url = self::toAbsoluteUrl($titre->getRevueUrl());
			$result['revueUrl'] = $url;
			$result['titre'] = $titre->getFullTitle();
			$result['titreLink'] = CHtml::link(CHtml::encode($result['titre']), $url);
		}
		return $result;
	}

	/**
	 * Converts a (local) relative URL into an absolute URL.
	 */
	private static function toAbsoluteUrl(string $url): string
	{
		static $hostPrefix = null;
		if ($url === '' || strncmp($url, 'https://', 8) === 0 || strncmp($url, 'http://', 7) === 0) {
			return $url;
		}
		if ($hostPrefix === null) {
			$hostPrefix = Yii::app()->getRequest()->getHostInfo('');
		}
		return $hostPrefix . $url;
	}
}
