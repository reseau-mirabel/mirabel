<?php

namespace processes\kbart\work;

use Service;

class DiffServices
{
	private const SKIPPED_SERVICE_ATTRIBUTES = [
		'id' => true,
		'hdateCreation' => true,
		'hdateModif' => true,
		'hdateImport' => true,
		'statut' => true,
	];

	/**
	 * @var int[] $collectionIds The collections into which the services are imported.
	 */
	private array $collectionIds;

	/**
	 * @var Service[]
	 */
	private array $create = [];

	/**
	 * @var Service[] Records present in the DB by a previous import,
	 *     without a match in this import,
	 *     and with a key (e.g. "Libre Intégral") that had a match.
	 */
	private array $delete = [];

	/**
	 * @var int[] Array of Service.id.
	 */
	private array $unchangedIds = [];

	/**
	 * @var Service[] Records present in the DB but without a match in the import.
	 */
	private array $unmatched = [];

	/**
	 * @var array where each value is ['before' => Service $before, 'after' => Service $after]
	 */
	private array $update = [];

	/**
	 * @var array<int, int[]> Keys are Service.id, values are a list of Collection.id.
	 */
	private array $needsCollectionsReset = [];

	public function __construct(array $collectionsIds)
	{
		$this->collectionIds = $collectionsIds;
	}

	/**
	 * Detect the services that need an *extension* their collection memberships.
	 *
	 * The import wil never remove a link between a Service and a Collection.
	 * So we fill $this->needsCollectionsReset with the IDs of Service records,
	 * that miss some of the collections where we import.
	 */
	public function detectCollectionChanges(): void
	{
		if (!$this->collectionIds) {
			return;
		}

		sort($this->collectionIds, SORT_NUMERIC);
		$cidsStr = join(',', $this->collectionIds);
		$numCollections = count($this->collectionIds);

		$ids = array_merge(
			$this->unchangedIds,
			array_map(
				function ($x) { return (int) $x['before']->id; },
				$this->update
			)
		);
		if (!$ids) {
			// No Service is detected (unchanged or updated).
			return;
		}
		$idsStr = join(',', $ids);

		$needing = \Yii::app()->db
			->createCommand(<<<EOSQL
				SELECT sc.serviceId, GROUP_CONCAT(sc.collectionId ORDER BY sc.collectionId SEPARATOR ',') AS colls
				FROM Service_Collection sc
				WHERE sc.serviceId IN ({$idsStr}) AND sc.collectionId IN ($cidsStr)
				GROUP BY sc.serviceId
					HAVING count(*) < $numCollections
				EOSQL
			)->queryAll();
		$this->needsCollectionsReset = [];
		foreach ($needing as $row) {
			$serviceId = (int) $row['serviceId'];
			$inDb = array_map(intval(...), explode(',', $row['colls']));
			$this->needsCollectionsReset[$serviceId] = array_diff($this->collectionIds, $inDb);
		}
	}

	/**
	 * @return array<int, int[]> Keys are Service.id, values are a list of Collection.id.
	 */
	public function needsCollectionChanges(): array
	{
		return $this->needsCollectionsReset;
	}

	/**
	 * @param Service[] $oldServices
	 * @param Service[] $newServices
	 */
	public function compare(array $oldServices, array $newServices): void
	{
		if (!$oldServices) {
			$this->create = $newServices;
			return;
		}

		if (!$newServices) {
			$this->unmatched = $oldServices;
			return;
		}

		$oldByKey = [];
		$unmatched = [];
		foreach ($oldServices as $old) {
			$key = self::getServiceUniqueKey($old);
			if (isset($oldByKey[$key])) {
				$oldByKey[$key][$old->id] = $old;
			} else {
				$oldByKey[$key] = [$old->id => $old];
			}
			// unmatched: by default, everything.
			$unmatched[$old->id] = $old;
		}
		$deleted = [];
		foreach ($newServices as $new) {
			$key = self::getServiceUniqueKey($new);
			if (isset($oldByKey[$key])) {
				$updated = self::updateService($oldByKey[$key], $new);
				if (count($updated) === 2) {
					$this->update[] = ['before' => $updated[0], 'after' => $updated[1]];
				} else {
					$this->unchangedIds[] = (int) $updated[0]->id;
				}
				unset($oldByKey[$key][$updated[0]->id]);
				if (empty($oldByKey[$key])) {
					unset($oldByKey[$key]);
				}
				// unmatched: one service has matched
				unset($unmatched[$updated[0]->id]);
				// Service to delete?
				if (isset($oldByKey[$key])) {
					foreach ($oldByKey[$key] as $s) {
						$deleted[$s->id] = $s; // Everything remaining in the group is candidate for deletion
					}
				}
				if (isset($deleted[$updated[0]->id])) {
					unset($deleted[$updated[0]->id]); // What was updated is no longer to be deleted
				}
			} else {
				$this->create[] = $new;
			}
		}

		if ($this->collectionIds) {
			$this->unmatched = [];
			$idsInCollections = \Yii::app()->db
				->createCommand("SELECT serviceId FROM Service_Collection WHERE collectionId IN (" . join(",", $this->collectionIds) . ")")
				->queryColumn();
			foreach ($unmatched as $s) {
				if (in_array($s->id, $idsInCollections)) {
					$this->unmatched[] = $s;
				}
			}
		} else {
			$this->unmatched = array_values($unmatched);
		}

		/*
		 * Deleted services, e.g. those
		 *  - that were imported
		 *  - that were not matched
		 *  - contained in groups that had another element matched.
		 * The deletion of unmatched services is less certain...
		 */
		foreach (array_values($deleted) + $this->unmatched as $s) {
			if ((int) $s->import > 0) {
				$this->delete[] = $s;
			}
		}
	}

	/**
	 * @return Service[]
	 */
	public function getCreate(): array
	{
		return $this->create;
	}

	/**
	 * @return Service[]
	 */
	public function getDelete(): array
	{
		return $this->delete;
	}

	public function getTextSummary(bool $showDeletes): string
	{
		$summary = [];
		if ($this->create) {
			$summary[] = count($this->create) . "C";
		}
		if ($this->update) {
			$summary[] = count($this->update) . "M";
		}
		if ($showDeletes && $this->delete) {
			$summary[] = count($this->delete) . "S";
		}
		if ($this->needsCollectionsReset) {
			$summary[] = count($this->needsCollectionsReset) . "coll";
		}
		$summaryText = join(" ", $summary);
		if (!$this->isValid()) {
			$summaryText = <<<HTML
				<div class="alert-error">
					<span class="glyphicon glyphicon-warning-sign" title="Certains accès sont invalides, donc l'intervention ne sera pas appliquée, et aucun changement ne sera enregistré."></span>
					$summaryText
				</div>
				HTML;
		}
		return $summaryText;
	}

	/**
	 * @return Service[]
	 */
	public function getUnmatched(): array
	{
		return $this->unmatched;
	}

	/**
	 * Array where each value is [Service $before, Service $after].
	 */
	public function getUpdate(): array
	{
		return $this->update;
	}

	/**
	 * Validates each od the Service records that will be created or updated.
	 */
	public function isValid(): bool
	{
		if ($this->create) {
			foreach ($this->create as $s) {
				if (!$s->validate()) {
					return false;
				}
			}
		}
		if ($this->update) {
			foreach ($this->update as $x) {
				if (!$x['after']->validate()) {
					return false;
				}
			}
		}
		return true;
	}

	public function getValidationErrors(): array
	{
		$errors = [];
		if ($this->create) {
			foreach ($this->create as $s) {
				foreach ($s->getErrors() as $byAttr) {
					foreach ($byAttr as $e) {
						$errors[] = "(création) $e";
					}
				}
			}
		}
		if ($this->update) {
			foreach ($this->update as $s) {
				foreach ($s['after']->getErrors() as $byAttr) {
					foreach ($byAttr as $e) {
						$errors[] = "(modification) $e";
					}
				}
			}
		}
		return array_unique($errors);
	}

	/**
	 * Returns a pair [old Service, new Service], or [old Service] if there is no change.
	 *
	 * @param Service[] $existing
	 * @param Service $to
	 * @return Service[]
	 */
	public static function updateService(array $existing, Service $to): array
	{
		$from = self::findNearestService($existing, $to);
		if (self::isSame($from, $to)) {
			return [$from];
		}
		$to->id = $from->id;
		$to->hdateCreation = $from->hdateCreation;
		$to->setIsNewRecord(false);
		unset($from->hdateImport);
		unset($to->hdateImport);
		unset($from->hdateModif);
		unset($to->hdateModif);
		return [$from, $to];
	}

	/**
	 * @param Service[] $existing
	 * @param Service $to
	 * @throws \Exception
	 * @return Service
	 */
	public static function findNearestService(array $existing, Service $to): Service
	{
		if (!$existing) {
			throw new \Exception("Cannot update a non-existing Service.");
		}
		if (count($existing) === 1) {
			return reset($existing);
		}

		$scores = [];
		foreach (array_keys($existing) as $pos) {
			$scores[$pos] = 0;
		}

		foreach ($existing as $pos => $s) {
			/** @var Service $s */
			// Already: same type (e.g. Intégral) & same access (e.g. Libre)

			// Same URL?
			if ($s->url === $to->url) {
				$scores[$pos] += 20;
			}

			// New coverage contains old coverage? (dates)
			if ($s->dateBarrDebut && $s->dateBarrFin) {
				if ($s->dateBarrDebut >= $to->dateBarrDebut && $s->dateBarrFin <= $to->dateBarrFin) {
					$scores[$pos] += 10;
				}
			} elseif ($s->dateBarrDebut) {
				if ($s->dateBarrDebut >= $to->dateBarrDebut) {
					$scores[$pos] += 10;
				}
			} elseif ($s->dateBarrFin) {
				if ($s->dateBarrFin >= $to->dateBarrFin) {
					$scores[$pos] += 10;
				}
			}

			// New coverage contains old coverage? (volumes)
			if ($s->volDebut && $s->volFin) {
				if ($s->volDebut >= $to->volDebut && $s->volFin <= $to->volFin) {
					$scores[$pos] += 5;
				}
			} elseif ($s->volDebut) {
				if ($s->volDebut >= $to->volDebut) {
					$scores[$pos] += 5;
				}
			} elseif ($s->volFin) {
				if ($s->volFin >= $to->volFin) {
					$scores[$pos] += 10;
				}
			}
		}
		$max = (int) max($scores);
		foreach ($scores as $pos => $score) {
			if ($score === $max) {
				return $existing[$pos];
			}
		}
		throw new \Exception("Max score was not found among scores!");
	}

	private static function getServiceUniqueKey(Service $s): string
	{
		return "{$s->acces} {$s->type}";
	}

	private static function isSame(Service $from, Service $to): bool
	{
		foreach ($to->getAttributes() as $k => $v) {
			if (isset(self::SKIPPED_SERVICE_ATTRIBUTES[$k])) {
				continue;
			}
			if ($from->getAttribute($k) != $v) {
				return false;
			}
		}
		return true;
	}
}
