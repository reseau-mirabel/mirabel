<?php

namespace processes\kbart\work;

use processes\kbart\Logger;
use processes\kbart\logger\LogEvent;
use processes\kbart\logger\LogFamily;
use processes\kbart\Normalize;
use processes\kbart\read\FileData;

class CheckConsistency
{
	/**
	 * @var array<string, int>
	 */
	private array $accesstypeValues = [];

	/**
	 * @var array<string, int>
	 */
	private array $coveragedepthValues = [];

	private FileData $fileData;

	public function __construct(FileData $d)
	{
		$this->fileData = $d;
	}

	public function run(Logger $logger): void
	{
		$this->checkValues($logger);
		$this->findDuplicates(['title', 'accessType'], $logger);
	}

	public function getStats(): string
	{
		$result = "Valeurs lues\n";
		$result .= "access_type (diffusion) : ";
		$a = [];
		foreach ($this->accesstypeValues as $k => $v) {
			$a[] = "$v '$k'";
		}
		$result .= join(", ", $a);
		$result .= "\ncoverage_depth (contenu) : ";
		$c = [];
		foreach ($this->coveragedepthValues as $k => $v) {
			$c[] = "$v '$k'";
		}
		$result .= join(", ", $c);
		return $result;
	}

	/**
	 * Checks various criteria on each row.
	 */
	private function checkValues(Logger $logger): void
	{
		$issnv = new \models\validators\IssnValidator();
		foreach ($this->fileData->getRows() as $row) {
			$logger->setRowContext($row->position);
			if (!$row->accessType) {
				$logger->addLocal(new LogEvent(LogFamily::accesstypeEmpty()));
			} elseif (isset($this->accesstypeValues[strtoupper($row->accessType)])) {
				$this->accesstypeValues[strtoupper($row->accessType)]++;
			} else {
				$this->accesstypeValues[strtoupper($row->accessType)] = 1;
			}
			if (isset($this->coveragedepthValues[strtolower($row->coverageDepth)])) {
				$this->coveragedepthValues[strtolower($row->coverageDepth)]++;
			} else {
				$this->coveragedepthValues[strtolower($row->coverageDepth)] = 1;
			}

			if (!$row->title) {
				$logger->addLocal(new LogEvent(LogFamily::error(), "Le champ 'title' est vide !"));
			}
			if (!$row->url) {
				$logger->addLocal(new LogEvent(LogFamily::error(), "Le champ 'url' est vide !"));
			}

			// Coverage (full text, abstract, ...)
			if ($row->coverageDepth === '') {
				$logger->addLocal(new LogEvent(LogFamily::coveragedepthEmpty()));
			} elseif (!in_array(strtolower($row->coverageDepth), ['fulltext', 'full text', 'abstract', 'abstracts', 'selected articles'], true)) {
				$logger->addLocal(new LogEvent(LogFamily::coveragedepthNotValid($row->coverageDepth)));
			}

			// ISSN
			if ($row->issn && !$issnv->validateString($row->issn)) {
				$logger->addLocal(new LogEvent(LogFamily::issnNotValid('issnp', $row->issn, $issnv->getErrors())));
				$row->issn = "";
			}
			if ($row->issne && !$issnv->validateString($row->issne)) {
				$logger->addLocal(new LogEvent(LogFamily::issnNotValid('issne', $row->issne, $issnv->getErrors())));
				$row->issne = "";
			}
			if (!$row->issn && !$row->issne && !$row->id) {
				$logger->addLocal(new LogEvent(LogFamily::unidentified()));
			}

			// Accès
			if ($row->accessEnd->isEmpty() && $row->accessBegin->isEmpty()) {
				$logger->addLocal(new LogEvent(LogFamily::accessDateError("L'accès n'a ni date de fin ni date de début.")));
			} elseif (!$row->accessEnd->isEmpty() && $row->accessBegin->isEmpty()) {
				$logger->addLocal(new LogEvent(LogFamily::accessDateError("L'accès a une date de fin mais pas de date de début.")));
			} elseif ($row->accessEnd->date && $row->accessBegin->date > $row->accessEnd->date) {
				$logger->addLocal(new LogEvent(LogFamily::accessDateError("L'accès a une date de début postérieure à la date de fin : {$row->accessBegin->date} > {$row->accessEnd->date}.")));
			} elseif (($row->accessBegin->vol || $row->accessBegin->issue) && !$row->accessBegin->date) {
				$logger->addLocal(new LogEvent(LogFamily::info(), "L'accès n'a pas de date de début alors qu'un numéro/volume est présent."));
			} elseif (($row->accessEnd->vol || $row->accessEnd->issue) && !$row->accessEnd->date) {
				$logger->addLocal(new LogEvent(LogFamily::info(), "L'accès n'a pas de date de fin alors qu'un numéro/volume est présent."));
			} elseif ($row->accessType === '') {
				$logger->addLocal(new LogEvent(LogFamily::info(), "Le type d'accès (access_type='F' pour 'Libre', etc) est absent."));
			}

			// Embargo
			if ($row->embargoInfo !== '') {
				$dateBarr = Normalize::convKbartToDate($row->embargoInfo);
				$embargoDescr = "l'embargo ({$row->embargoInfo}) calculé à '{$dateBarr['date']}'.";
				if ($dateBarr['type'] === 'P') {
					if (empty($row->accessEnd->date)) {
						$logger->addLocal(new LogEvent(LogFamily::info(), "La date de fin vide sera remplacée par $embargoDescr"));
					} elseif ((int) $row->accessEnd->date !== (int) $dateBarr['date']) {
						// Année différente => niveau avertissement
						$logger->addLocal(new LogEvent(LogFamily::warning(), "La date de fin déclarée '{$row->accessEnd->date}' diffère de $embargoDescr"));
					} elseif ($row->accessEnd->date !== $dateBarr['date']) {
						// Même année => niveau info
						$logger->addLocal(new LogEvent(LogFamily::info(), "La date de fin déclarée '{$row->accessEnd->date}' diffère de $embargoDescr"));
					}
				} elseif ($dateBarr['type'] === 'R') {
					if (empty($row->accessBegin->date)) {
						$logger->addLocal(new LogEvent(LogFamily::info(), "La date de début vide sera remplacée par $embargoDescr"));
					} elseif ((int) $row->accessBegin->date !== (int) $dateBarr['date']) {
						$logger->addLocal(new LogEvent(LogFamily::warning(), "La date de début déclarée '{$row->accessBegin->date}' diffère de $embargoDescr"));
					} elseif ($row->accessBegin->date !== $dateBarr['date']) {
						$logger->addLocal(new LogEvent(LogFamily::info(), "La date de début déclarée '{$row->accessBegin->date}' diffère de $embargoDescr"));
					}
				}
			}
		}
	}

	/**
	 * List the groups of rows where these columns have the same value.
	 *
	 * @param string[] $columns List of RowData properties, e.g. ["issne", "accessType"]
	 */
	private function findDuplicates(array $columns, Logger $logger): void
	{
		$colNames = join(", ", $columns);
		$indexes = (new Dedup())->find($this->fileData->getRows(), $columns);
		foreach ($indexes as $positions) {
			$first = null;
			foreach ($positions as $p) {
				$logger->setRowContext($p);
				if ($first === null) {
					$first = $p;
					$logger->addLocal(new LogEvent(LogFamily::info(), "Doublons pour les colonnes '$colNames' en lignes " . join(", ", $positions)));
				} else {
					$logger->addLocal(new LogEvent(LogFamily::info(), "Doublon de la ligne $first pour les colonnes '$colNames'"));
				}
			}
		}
	}
}
