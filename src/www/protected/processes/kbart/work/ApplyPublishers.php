<?php

namespace processes\kbart\work;

use Editeur;
use Intervention;
use InterventionDetail;
use Titre;
use TitreEditeur;

class ApplyPublishers
{
	public static function createIntervention(Titre $titre, array $publishers): Intervention
	{
		$i = self::getBaseIntervention($titre);
		foreach ($publishers as $p) {
			if (is_int($p)) {
				$editeurId = $p;
			} else {
				$editeurId = 0;
				$e = new Editeur();
				$e->setAttributes($p, true);
				$i->contenuJson->create($e);
			}
			$te = new TitreEditeur();
			$te->titreId = (int) $titre->id;
			$te->editeurId = (int) $editeurId;
			$i->contenuJson->create($te);
		}
		return $i;
	}

	public static function getBaseIntervention(Titre $titre): Intervention
	{
		$i = $titre->buildIntervention(true);
		$i->contenuJson = new InterventionDetail();
		$i->contenuJson->isImport = true;
		return $i;
	}
}
