<?php

namespace processes\kbart\work;

class Dedup
{
	/**
	 * Return an array of duplicates (same values in all the columns given as parameter).
	 *
	 * @param \processes\kbart\read\RowData[] $content
	 * @param string[] $columns
	 * @return int[][] duplicates, each as a list of positions, e.g. [ [2,3] ].
	 */
	public function find(array $content, array $columns): array
	{
		if (empty($content)) {
			return [];
		}
		$first = reset($content);
		foreach ($columns as $v) {
			if (!property_exists($first, $v)) {
				throw new \Exception("'$v' is not a valid property of RowData.");
			}
		}

		$dups = [];
		foreach ($content as $row) {
			$values = [];
			foreach ($columns as $c) {
				if (!empty($row->{$c})) {
					$values[] = $row->{$c};
				}
			}
			if (empty($values)) {
				continue;
			}
			$key = join("//", $values);
			if (isset($dups[$key])) {
				$dups[$key][] = $row->position;
			} else {
				$dups[$key] = [$row->position];
			}
		}
		return array_values(
			array_filter(
				$dups,
				function ($a) {
					return count($a) > 1;
				}
			)
		);
	}
}
