<?php

namespace processes\kbart\work;

class ExceptionMissingDefault extends \Exception
{
	public const CODE_DIFFUSION = 1;

	public const CODE_CONTENU = 2;
}
