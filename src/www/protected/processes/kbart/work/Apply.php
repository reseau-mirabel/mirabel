<?php

namespace processes\kbart\work;

use Intervention;
use processes\kbart\data\Diff;
use Ressource;
use Titre;

class Apply
{
	private Ressource $ressource;

	private array $collectionIds;

	private bool $delete = false;

	public function __construct(Ressource $ressource, array $collectionIds)
	{
		$this->ressource = $ressource;
		$this->ressource->id = (int) $this->ressource->id;
		$this->collectionIds = $collectionIds;
	}

	public function enableDelete(bool $delete): void
	{
		$this->delete = $delete;
	}

	public function createIntervention(?Titre $oldTitre, Diff $diff): Intervention
	{
		$i = $this->getBaseIntervention();

		if ($oldTitre === null) {
			$i->action = 'revue-C';
			$titre = new Titre();
			$titre->setAttributes($diff->titre, true);
			$i->contenuJson->create($titre);
			$i->description = "Création de la revue « {$titre->titre} »";
		} else {
			$i->titreId = (int) $oldTitre->id;
			$i->revueId = (int) $oldTitre->revueId;
			if ($diff->status === Diff::STATUS_JOURNAL_UPDATE) {
				$i->action = 'revue-U';
			} elseif ($diff->status === Diff::STATUS_CREATE) {
				$i->action = 'service-C';
			} elseif ($diff->status === Diff::STATUS_UPDATE) {
				$i->action = 'service-U';
			} elseif ($diff->status === Diff::STATUS_DELETE) {
				$i->action = 'service-D';
			}

			if ($diff->titre) {
				$titre = new Titre();
				$titre->setAttributes($diff->titre, true);
				$i->contenuJson->update($oldTitre, $titre);
			}
		}

		// Pas de modification des ISSN, $diff->issn est purement informatif.

		if ($diff->identification) {
			$identNew = new \Identification();
			$identNew->ressourceId = $this->ressource->id;
			$identNew->titreId = (int) ($oldTitre === null ? 0 : $oldTitre->id);
			$identNew->idInterne = $diff->identification[1];
			if ($diff->identification[0] === '') {
				$i->contenuJson->create($identNew);
			} else {
				$identNew->isNewRecord = false;
				$identNew->id = $diff->identification[2];
				$identOld = clone $identNew;
				$identOld->idInterne = $diff->identification[0];
				$i->contenuJson->update($identOld, $identNew);
			}
		}

		if ($diff->services instanceof DiffServices) {
			// Create
			foreach ($diff->services->getCreate() as $s) {
				$i->contenuJson->create($s);
				foreach ($this->collectionIds as $cid) {
					$sc = new \ServiceCollection('import');
					$sc->collectionId = $cid;
					$sc->serviceId = 0;
					$i->contenuJson->create($sc);
				}
			}
			// Update
			foreach ($diff->services->getUpdate() as ['before' => $before, 'after' => $after]) {
				assert($before instanceof \Service);
				$i->contenuJson->update($before, $after);
			}
			// Collections
			$collChanges = $diff->services->needsCollectionChanges();
			foreach ($collChanges as $sid => $cids) {
				foreach ($cids as $cid) {
					$sc = new \ServiceCollection('import');
					$sc->serviceId = (int) $sid;
					$sc->collectionId = $cid;
					$i->contenuJson->create($sc);
				}
			}
			// Delete
			if ($this->delete) {
				foreach ($diff->services->getDelete() as $s) {
					$i->contenuJson->delete($s);
				}
			}
			if ($oldTitre !== null) {
				if ($diff->services->getCreate()) {
					$i->description =  "Création d'accès en ligne, revue « {$oldTitre->titre} » / « {$this->ressource->nom} »";
				} else {
					$i->description =  "Modification d'accès en ligne, revue « {$oldTitre->titre} » / « {$this->ressource->nom} »";
				}
			}
		}

		if (empty($i->description) && $oldTitre !== null) {
			$i->description = "Modification de la revue « {$oldTitre->titre} »";
		}

		return $i;
	}

	public function createRemovalIntervention(?Titre $titre): Intervention
	{
		$i = $this->getBaseIntervention();
		$i->titreId = (int) $titre->id;
		$i->revueId = (int) $titre->revueId;
		$i->action = 'service-D';
		$i->description =  "Suppression d'accès en ligne, revue « {$titre->titre} » / « {$this->ressource->nom} »";

		$services = \Service::model()->findAllByAttributes([
			'titreId' => $titre->id,
			'ressourceId' => $this->ressource->id,
			'import' => \models\import\ImportType::getSourceId('KBART'),
		]);
		foreach ($services as $s) {
			$i->contenuJson->delete($s);
		}

		return $i;
	}

	public function getBaseIntervention(): \Intervention
	{
		$i = new \Intervention('insert');
		$i->ressourceId = $this->ressource->id;
		$i->contenuJson = new \InterventionDetail();
		$i->contenuJson->isImport = true;
		$i->statut = 'attente';
		$i->import = \models\import\ImportType::getSourceId('KBART');
		return $i;
	}
}
