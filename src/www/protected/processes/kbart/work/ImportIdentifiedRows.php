<?php

namespace processes\kbart\work;

use models\import\ImportType;
use processes\kbart\data\Analyzed;
use processes\kbart\data\Diff;
use processes\kbart\logger\LogEvent;
use processes\kbart\logger\LogFamily;
use processes\kbart\LoggerTrait;
use processes\kbart\noninteractive\Config;
use processes\kbart\Normalize;
use processes\kbart\read\RowData;

class ImportIdentifiedRows
{
	use LoggerTrait;

	private Config $defaults;

	private array $collectionIds;

	private \Ressource $ressource;

	public function __construct(\Ressource $ressource, array $collectionIds, Config $defaults)
	{
		$this->collectionIds = $collectionIds;
		$this->ressource = $ressource;
		$this->defaults = $defaults;
	}

	public function computeDiff(Analyzed $data): Diff
	{
		$this->getLogger()->setRowContext($data->title->id);
		$diff = new Diff();
		try {
			$diff->services = $this->computeDiffServices($data);
		} catch (ExceptionMissingDefault $e) {
			if ($e->getCode() === ExceptionMissingDefault::CODE_CONTENU) {
				$diff->erreur = "Type de contenu absent, sans valeur par défaut";
			} elseif ($e->getCode() === ExceptionMissingDefault::CODE_DIFFUSION) {
				$diff->erreur = "Type de diffusion absent, sans valeur par défaut";
			} else {
				$diff->erreur = "Erreur inconnue";
			}
			$diff->status = Diff::STATUS_ERROR;
			return $diff;
		}

		$diff->issn = self::findMissingIssnRecords($data->title, $data->rows);

		if ($data->rows[0]->id !== '') {
			$localId = $data->rows[0]->id;
			$row = \Yii::app()->db
				->createCommand("SELECT id, idInterne FROM Identification WHERE titreId = {$data->title->id} AND ressourceId = {$this->ressource->id} LIMIT 1")
				->queryRow();
			if (!$row) {
				$diff->identification = ['', $localId];
			} elseif ($localId !== $row['idInterne']) {
				$diff->identification = [$row['idInterne'], $localId, (int) $row['id']];
			}
		}

		self::setDiffStatus($diff);

		if (!$diff->services->isValid()) {
			$diff->erreur = "Accès en ligne invalides : " . join(" | ", $diff->services->getValidationErrors());
		}
		$localLog = $this->logger->getRowLog($data->title->id);
		if ($localLog->hasEvents()) {
			foreach ($localLog->getEvents() as $event) {
				if ($event->level === \processes\kbart\logger\LogLevel::Error->value) {
					$diff->erreur .= $event->message . " ";
				}
			}
		}

		return $diff;
	}

	/**
	 * Return an array of new instances of \Service.
	 *
	 * @param int $titreId
	 * @param RowData $row
	 * @return \Service[]
	 */
	public function buildNewServices(int $titreId, RowData $row): array
	{
		$s = new \Service('import');
		$s->statut = 'normal';
		$s->import = ImportType::getSourceId('KBART');
		$s->hdateImport = time();
		$s->statut = 'normal';

		$s->ressourceId = (int) $this->ressource->id;
		$s->titreId = $titreId;

		$s->type = $this->getType($row);
		$s->acces = $this->getAccess($row);
		$s->url = $row->url;
		$s->alerteMailUrl = '';
		$s->alerteRssUrl = '';
		$s->derNumUrl = '';
		$s->notes = $row->notes;
		$s->embargoInfo = trim($row->embargoInfo, ' ;');

		if (strtolower($row->coverageDepth) === 'selected articles') {
			$s->selection = 1;
		} else {
			$s->selection = (int) $this->defaults->selection;
		}
		$s->lacunaire= (int) $this->defaults->lacunaire;

		// début
		$s->volDebut = $row->accessBegin->vol;
		$s->noDebut = $row->accessBegin->issue;
		$s->numeroDebut = $row->accessBegin->numbering ?? '';
		$s->dateBarrDebut = $row->accessBegin->date ?? '';
		// fin
		$s->volFin = $row->accessEnd->vol;
		$s->noFin = $row->accessEnd->issue;
		$s->numeroFin = $row->accessEnd->numbering ?? '';
		$s->dateBarrFin = $row->accessEnd->date ?? '';

		/*
		 * Si date absente, la calculer à partir de l'embargo.
		 * La configuration détermnine la priorité entre "date déclarée" et "date calculée par l'embargo".
		 */
		$s->dateBarrInfo = '';
		$row->embargoInfo = trim($row->embargoInfo, ' ;');
		$canOverrideDates = $row->embargoInfo
			&& ($this->defaults->overrideDatesWithEmbargo || (!$s->dateBarrDebut || !$s->dateBarrFin));
		if ($canOverrideDates) {
			$this->overrideDatesWithEmbargo($s, $row->embargoInfo);
		}

		return [$s];
	}

	/**
	 * @param RowData[] $rows
	 * @return array<string, int> ISSN => 0 if not in M, ISSN => revueId if linked to another Titre record.
	 */
	private static function findMissingIssnRecords(\Titre $t, array $rows): array
	{
		$inDb = \components\SqlHelper::sqlToPairs("SELECT issn, id FROM Issn WHERE titreId = {$t->id} AND issn <> ''");
		$missingInM = [];
		foreach ($rows as $row) {
			if ($row->issn && !isset($inDb[$row->issn])) {
				$missingInM[] = $row->issn;
			}
			if ($row->issne && !isset($inDb[$row->issne])) {
				$missingInM[] = $row->issne;
			}
		}
		$missing = [];
		if ($missingInM) {
			$params = [];
			foreach ($missingInM as $issn) {
				$missing[$issn] = 0;
				$params[] = "'{$issn}'";
			}
			$param = join(",", $params); // ISSN are validated, no SQL injection is possible.
			$data = \Yii::app()->db
				->createCommand("SELECT i.issn, t.revueId FROM Issn i JOIN Titre t ON i.titreId = t.id WHERE i.issn IN ($param)")
				->queryAll();
			if ($data) {
				foreach ($data as $issnRows) {
					$missing[$issnRows['issn']] = (int) $issnRows['revueId'];
				}
			}
		}
		return $missing;
	}

	private static function setDiffStatus(Diff $diff): void
	{
		if (($diff->titre || $diff->issn) && $diff->status !== Diff::STATUS_ERROR) {
			$diff->status = Diff::STATUS_JOURNAL_UPDATE;
		} else {
			$c = count($diff->services->getCreate());
			$d = count($diff->services->getDelete());
			$u = count($diff->services->getUpdate());
			if (!$c && !$d && !$u) {
				$diff->status = Diff::STATUS_EMPTY;
			} elseif ($u > 0 || ($c > 0 && $d > 0)) {
				$diff->status = Diff::STATUS_UPDATE;
			} elseif ($c > 0) {
				$diff->status = Diff::STATUS_CREATE;
			} elseif ($d > 0) {
				$diff->status = Diff::STATUS_DELETE;
			}
		}
		if ($diff->status === Diff::STATUS_EMPTY && $diff->identification) {
			$diff->status = Diff::STATUS_JOURNAL_UPDATE;
		}
	}

	private function computeDiffServices(Analyzed $data): DiffServices
	{
		$newServices = [];
		foreach ($data->rows as $row) {
			foreach ($this->buildNewServices((int) $data->title->id, $row) as $s) {
				$newServices[] = $s;
			}
		}

		$diff = new DiffServices($this->collectionIds);
		$diff->compare($data->services, $newServices);
		$diff->detectCollectionChanges();
		return $diff;
	}

	/**
	 * Update the dates in the Service record according to the embargo and config.
	 */
	private function overrideDatesWithEmbargo(\Service $s, string $embargoInfo): void
	{
		$canOverride = $this->defaults->overrideDatesWithEmbargo;

		// Complex embargo with 2 components?
		if (strpos($embargoInfo, ';') !== false) {
			// service entre ces dates, séparées par ";"
			$split = explode(';', $embargoInfo);
			$embargoBefore = Normalize::convKbartToDate($split[0]);
			$embargoAfter = Normalize::convKbartToDate($split[1]);
			if (!$s->dateBarrDebut || ($canOverride && $embargoBefore['fulldate'] > $s->dateBarrDebut)) {
				$s->dateBarrDebut = $embargoBefore['date'];
			}
			if (!$s->dateBarrFin || ($canOverride && $embargoAfter['fulldate'] < $s->dateBarrFin)) {
				$s->dateBarrFin = $embargoAfter['date'];
			}
			return;
		}

		// Simple embargo
		$dateBarr = Normalize::convKbartToDate($s->embargoInfo);
		if ($dateBarr['type'] === 'P') {
			// service avant cette date
			if (!$s->dateBarrFin || $canOverride) {
				if ($s->dateBarrFin !== $dateBarr['date']) {
					$this->getLogger()->addLocal(
						new LogEvent(LogFamily::dateReplaced("fin", $s->dateBarrFin, $dateBarr['date']))
					);
				}
				$s->dateBarrFin = $dateBarr['date'];
				$s->noFin = 0;
				$s->numeroFin = '';
				$s->volFin = 0;
			} elseif ($dateBarr['fulldate'] < $s->dateBarrFin) {
				$this->getLogger()->addLocal(
					new LogEvent(LogFamily::dateOutofbounds("Attention, la date de fin d'accès {$s->dateBarrFin} est postérieure à la date barrière {$dateBarr['date']}."))
				);
			}
		} elseif ($dateBarr['type'] === 'R') {
			// service après cette date
			if (!$s->dateBarrDebut || $canOverride) {
				if ($s->dateBarrDebut !== $dateBarr['date']) {
					$this->getLogger()->addLocal(
						new LogEvent(LogFamily::dateReplaced("début", $s->dateBarrDebut, $dateBarr['date']))
					);
				}
				$s->dateBarrDebut = $dateBarr['date'];
				$s->noDebut = 0;
				$s->numeroDebut = '';
				$s->volDebut = 0;
			} elseif ($dateBarr['fulldate'] > $s->dateBarrDebut) {
				$this->getLogger()->addLocal(
					new LogEvent(LogFamily::dateOutofbounds("Attention, la date de début d'accès {$s->dateBarrDebut} est antérieure à la date barrière {$dateBarr['date']}."))
				);
			}
		}
	}

	private function getType(RowData $row): string
	{
		switch (strtolower($row->coverageDepth)) {
			case 'fulltext':
			case 'full text':
				return \Service::TYPE_INTEGRAL;
			case 'abstract':
			case 'abstracts':
				return \Service::TYPE_RESUME;
			case 'selected articles':
				return \Service::TYPE_INTEGRAL;
			case '':
				if (!empty($this->defaults->contenu)) {
					return $this->defaults->contenu;
				}
				throw new ExceptionMissingDefault("Données KBART, 'coverage_depth' vide.", ExceptionMissingDefault::CODE_CONTENU);
			default:
				if (!empty($this->defaults->contenu)) {
					return $this->defaults->contenu;
				}
				$this->logger->addLocal(new LogEvent(LogFamily::coveragedepthNotValid($row->coverageDepth)));
				throw new ExceptionMissingDefault("Données KBART, 'coverage_depth' non reconnu.", ExceptionMissingDefault::CODE_CONTENU);
		}
	}

	private function getAccess(RowData $row): string
	{
		if ($row->accessType === '') {
			if (!empty($this->defaults->diffusion)) {
				return $this->defaults->diffusion;
			}
			throw new ExceptionMissingDefault("Données KBART, 'access_type' vide.", ExceptionMissingDefault::CODE_DIFFUSION);
		}
		switch (strtoupper($row->accessType[0])) { // first letter
			case 'F':
				return 'Libre';
			case 'P':
				return 'Restreint';
			default:
				$this->logger->addLocal(new LogEvent(LogFamily::accesstypeNotValid($row->accessType)));
				if (!empty($this->defaults->diffusion)) {
					return $this->defaults->diffusion;
				}
				throw new \Exception("Données KBART, 'access_type' non reconnu : {$row->accessType}.");
		}
	}
}
