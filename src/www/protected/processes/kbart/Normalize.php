<?php

namespace processes\kbart;

class Normalize
{
	public static function getReadableKbartEmbargo(string $embargo): string
	{
		$date = (new self)->convKbartToDate($embargo);
		return $date['info'];
	}

	/**
	 * @return array{date: string, fulldate: string, info: string, type: ""|"P"|"R"}
	 */
	public static function convKbartToDate(string $input, ?int $time = null): array
	{
		if (!$time) {
			$time = $_SERVER['REQUEST_TIME'];
		}
		if (!$input) {
			return ['date' => '', 'fulldate' => '', 'info' => '', 'type' => ''];
		}
		$matches = [];
		if (!preg_match('/^\s*([PR])(\d+)([DMY])\s*$/', $input, $matches)) {
			throw new \Exception("Champ date barrière invalide (K-Bart attendu) : $input");
		}
		$date = [
			'date' => '',
			'fulldate' => '',
			'info' => 'Accès ' . (($matches[1] === 'P') ? 'avant ' : 'après ') . 'une date barrière mobile de ',
			'type' => $matches[1],
		];
		$plural = $matches[2] > 1 ? 's' : '';
		$step = ($matches[1] === 'P') ? 1 : 0;
		switch ($matches[3]) {
			case 'M':
				$yesterday = $time - 24*3600;
				$date['info'] .= "{$matches[2]} mois calendaire{$plural}";
				$step += (int) $matches[2] - 1;
				if ($step) {
					$date['date'] = date('Y-m', strtotime("-$step month", $yesterday));
				} else {
					$date['date'] = date('Y-m', $yesterday);
				}
				break;
			case 'D':
				$years = (int) $matches[2] % 365 ? 0 : (int) $matches[2] / 365;
				$months = (int) $matches[2] % 30 ? 0 : (int) $matches[2] / 30;
				if ($years) {
					$plural = $years > 1 ? 's' : '';
					$date['info'] .= "$years année{$plural} glissante{$plural}";
					$date['date'] = date('Y-m-d', strtotime("-{$years} year{$plural}", $time));
				} elseif ($months) {
					$plural = $months > 1 ? 's' : '';
					$date['info'] .= "$months mois glissant{$plural}";
					$date['date'] = date('Y-m-d', strtotime("-{$months} month{$plural}", $time));
				} else {
					$date['info'] .= "{$matches[2]} jours";
					$date['date'] = date('Y-m-d', strtotime("-{$matches[2]} days", $time));
				}
				break;
			case 'Y':
				$date['info'] .= "{$matches[2]} année{$plural} calendaire{$plural}";
				$step += (int) $matches[2] - 1;
				if ($step) {
					$date['date'] = date('Y', strtotime("-$step years", $time));
				} else {
					$date['date'] = date('Y', $time);
				}
				break;
		}
		switch (strlen($date['date'])) {
			case 10:
				$date['fulldate'] = $date['date'];
				break;
			case 7:
				$date['fulldate'] = $date['date'] . (($matches[1] === 'P') ? '-30' : '-01');
				break;
			case 4:
				$date['fulldate'] = $date['date'] . (($matches[1] === 'P') ? '-12-31' : '-01-01');
				break;
		}
		return $date;
	}

	/**
	 * Convert a string to an ISO date, as much as possible.
	 */
	public static function toIsoDate(string $strDate): string
	{
		$m = [];
		if (preg_match('/^\d{4}-\d\d-\d\d$|^\d{4}$/', $strDate)) {
			// ISO or year
			return $strDate;
		}
		if (preg_match('#^(\d{4})\W?(\d\d)\W?(\d\d)$#', $strDate, $m)) {
			// ISO, except for separators
			return sprintf("%d-%02d-%02d", $m[1], $m[2], $m[3]);
		}
		if (preg_match('#^(\d{4})[-/](\d\d)$#', $strDate, $m)) {
			// incomplete ISO
			return $m[1] . "-" . $m[2];
		}
		if (preg_match('#^(\d\d)\W(\d\d)\W(\d{4})$#', $strDate, $m)) {
			// european or US, can't guess
			return $m[3];
		}
		return "";
	}

	public static function arabizeRomanNumber(string $text): ?int
	{
		$patterns = [
			'M' => 1000,
			'D' => 500,
			'CM' => 900,
			'CD' => 400,
			'C' => 100,
			'L' => 50,
			'XC' => 90,
			'XL' => 40,
			'X' => 10,
			'V' => 5,
			'IX' => 9,
			'IV' => 4,
			'I' => 1,
		];
		$result = 0;
		foreach ($patterns as $p => $value) {
			while (str_starts_with($text, $p)) {
				$result += $value;
				$text = substr($text, strlen($p));
			}
		}
		if (strlen($text) > 0) {
			return null;
		}
		return $result;
	}
}
