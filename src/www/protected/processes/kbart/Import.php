<?php

namespace processes\kbart;

use processes\kbart\data\CrossedData;
use processes\kbart\data\DiffCollection;
use processes\kbart\identify\Batch;
use processes\kbart\logger\LogEvent;
use processes\kbart\logger\LogFamily;
use processes\kbart\noninteractive\Config;
use processes\kbart\read\File;
use processes\kbart\work\Apply;
use processes\kbart\work\ImportIdentifiedRows;

class Import
{
	use LoggerTrait;

	public State $state;

	final public function __construct(State $state)
	{
		$this->state = $state;
	}

	public static function load(string $hash): self
	{
		$path = DATA_DIR . "/cache/kbart/$hash.serialized";
		return new static(State::load($path));
	}

	public function save(string $hash = ''): bool
	{
		if ($hash === '') {
			if ($this->state->fileData === null) {
				throw new \Exception("Cannot save state: no file was read.");
			}
			$hash = $this->state->fileData->getContentsHash();
		}
		$path = DATA_DIR . "/cache/kbart/$hash.serialized";
		return $this->state->save($path);
	}

	/**
	 * @param string $hash Either an absolute path, or a file hash.
	 */
	public function readFile(string $hash): bool
	{
		if ($hash[0] === '/') {
			$filepath = $hash;
		} else {
			$filepath = DATA_DIR . "/cache/kbart/{$hash}.txt";
		}
		$logger = $this->getLogger();
		$reader = new File($logger);
		try {
			$reader->load($filepath, $this->state->title);
			$this->state->fileData = $reader->readAll($this->state->getFilter());
			$this->state->hash = $this->state->fileData->getContentsHash();
		} catch (\Throwable $e) {
			$event = new LogEvent(LogFamily::fileRead(), $e->getMessage());
			$logger->addGlobal($event);
			if (YII_DEBUG) {
				throw new \Exception($e->getMessage());
			}
			return false;
		}
		$this->state->addStep(State::STEP_FILE);
		return true;
	}

	public function checkConsistency(): void
	{
		if (!$this->state->hasStep(State::STEP_FILE) || $this->state->fileData === null) {
			$event = new LogEvent(LogFamily::fileCheck());
			$this->getLogger()->addGlobal($event);
			return;
		}
		$checker = new \processes\kbart\work\CheckConsistency($this->state->fileData);
		$checker->run($this->getLogger());
		if ($this->logger !== null) {
			$event = new LogEvent(LogFamily::info(), $checker->getStats());
			$this->logger->addGlobal($event);
		}
		$this->state->logs = $this->logger;
		$this->state->addStep(State::STEP_CONSISTENCY);
	}

	public function identifyTitles(): void
	{
		if (!$this->state->hasStep(State::STEP_FILE)) {
			throw new \Exception("Coding error.");
		}
		$identifier = new Batch($this->state->getRessource());
		$rows = $this->state->fileData->getRows();
		$journals = $identifier->identifyAll($rows);

		$crossedData = new CrossedData();
		foreach ($rows as $row) {
			if (isset($journals[$row->position])) {
				$crossedData->addKnown($row, $journals[$row->position], []);
			} else {
				$crossedData->addUnknown($row);
			}
		}
		$identifier->addServices($crossedData, $this->state->getCollectionIds());

		$this->state->crossedData = $crossedData;
		$this->state->addStep(State::STEP_IDENTIFY);
	}

	public function diff(Config $defaults): DiffCollection
	{
		if (!$this->state->hasStep(State::STEP_IDENTIFY)) {
			throw new \Exception("Coding error. Cannot compute diff without analysis data.");
		}
		$processor = new ImportIdentifiedRows($this->state->getRessource(), $this->state->collectionIds, $defaults);
		$processor->setLogger($this->getLogger());
		$result = new DiffCollection();
		foreach ($this->state->crossedData->known as $known) {
			$result->addDiff($known->title->id, $processor->computeDiff($known));
		}
		$this->state->addStep(State::STEP_DIFF);
		return $result;
	}

	/**
	 * @return \Intervention[] List of Intervention records to apply.
	 */
	public function apply(Config $defaults, data\WritingStats $stats): array
	{
		foreach ($this->state->crossedData->unknown as $row) {
			$stats->addUnknown($row);
		}
		$stats->numIdentifiedJournals = count($this->state->crossedData->known);

		$diffs = $this->diff($defaults);
		$applyer = new Apply($this->state->getRessource(), $this->state->collectionIds);
		$applyer->enableDelete($defaults->suppressions);
		$interventions = [];
		foreach ($diffs->getDiffs() as $id => $diff) {
			$titre = $this->state->crossedData->known[$id]->title;
			if ($diff->erreur) {
				$stats->addIgnored($titre, $diff->erreur);
				continue;
			}

			// Update `hdateImport` in every matching Service, in order to later check for obsolescence.
			$servicesId = array_map(
				fn ($x) => (int) $x->id,
				$this->state->crossedData->known[$id]->services
			);
			if ($servicesId) {
				\Yii::app()->db
					->createCommand("UPDATE Service SET hdateImport = {$_SERVER['REQUEST_TIME']} WHERE id IN (" . join(",", $servicesId) . ")")
					->execute();
			}

			$i = $applyer->createIntervention($titre, $diff);
			if ($i->contenuJson->isEmpty()) {
				$stats->addUnchanged($titre);
			} else {
				$localLogs = $this->getLogger()->getRowLog($i->titreId);
				$i->commentaire = logger\LogRenderer::renderText($localLogs);
				$interventions[] = $i;
			}
		}
		if ($defaults->absents) {
			$missingJournals = $this->state->crossedData->findMissingJournals($this->state->getRessource()->id, $this->state->getCollectionIds());
			foreach ($missingJournals as $titre) {
				$i = $applyer->createRemovalIntervention($titre);
				if ($i->contenuJson->isEmpty()) {
					$stats->addUnchanged($titre);
				} else {
					$interventions[] = $i;
				}
			}
		}
		return $interventions;
	}

	public function getHash(): string
	{
		return $this->state->hash;
	}

	public function getFileData(): ?\processes\kbart\read\FileData
	{
		return $this->state->fileData;
	}

	public function hasDone(int $step): bool
	{
		return $this->state->hasStep($step);
	}
}
