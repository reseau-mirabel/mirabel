<?php

namespace processes\kbart\logger;

class LogEvent
{
	public readonly string $family;

	public readonly int $level;

	public readonly string $message;

	public int $count = 1;

	public function __construct(LogFamily $family, string $message = '')
	{
		$this->family = $family->id;
		$this->level = $family->level->value;
		$this->message = $message ?: $family->message;
	}
}
