<?php

namespace processes\kbart\logger;

enum LogLevel: int
{
	case Debug = 1;
	case Info = 2;
	case Warning = 3;
	case Error = 4;

	public function text(): string
	{
		return match ($this) {
			self::Debug => 'debug',
			self::Info => 'info',
			self::Warning => 'warning',
			self::Error => 'error',
		};
	}
}
