<?php

namespace processes\kbart\logger;

class LogFamily
{
	/**
	 * @var string If not empty, used for computing the number of events of this family.
	 */
	public readonly string $id;

	public readonly LogLevel $level;

	/**
	 * @var string The default message, which the event can overwrite.
	 */
	public readonly string $message;

	protected function __construct(LogLevel $level, string $message = '', ?AggregateId $id = null)
	{
		$this->level = $level;
		$this->message = $message;
		$this->id = $id ? $id->value : '';
	}

	/**
	 * Generic event, with no count of instances.
	 */
	public static function debug(): self
	{
		return new self(LogLevel::Debug); // no ID, so no family summary for this.
	}

	/**
	 * Generic event, with no count of instances.
	 */
	public static function info(): self
	{
		return new self(LogLevel::Info);
	}

	/**
	 * Generic event, with no count of instances.
	 */
	public static function warning(): self
	{
		return new self(LogLevel::Warning);
	}

	/**
	 * Generic event, with no count of instances.
	 */
	public static function error(): self
	{
		return new self(LogLevel::Error);
	}

	public static function fileRead(): self
	{
		return new self(LogLevel::Error);
	}

	public static function fileCheck(): self
	{
		return new self(LogLevel::Error, "Vérifications de cohérence impossibles, car le fichier n'a pas été lu correctement.");
	}

	public static function unidentified(): self
	{
		return new self(LogLevel::Warning, "Ligne sans ISSN-(P|E) ni ID interne", AggregateId::NoId);
	}

	public static function headerMissing(): self
	{
		return new self(LogLevel::Warning);
	}

	public static function headerNotFound(): self
	{
		return new self(LogLevel::Warning);
	}

	public static function headerUnknown(): self
	{
		return new self(LogLevel::Warning);
	}

	public static function wrongIdentificationMethod(string $method): self
	{
		return new self(LogLevel::Warning, "La méthode d'identification de titre '$method' est inconnue.");
	}

	public static function notSerialPublication(string $type): self
	{
		return new self(LogLevel::Warning, "Ligne avec le type '$type' au lieu de 'serial'.", AggregateId::Type);
	}

	public static function localUtf8Encoding(): self
	{
		return new self(LogLevel::Warning, "L'encodage du texte n'est pas de l'UTF-8 valide.", AggregateId::Utf8);
	}

	public static function issnNotValid(string $type, string $value, array $errors): self
	{
		return new self(
			LogLevel::Warning,
			sprintf("ISSN ignoré, car %s = '%s' n'est pas valide : %s", $type, $value, join(' ; ', $errors)),
			AggregateId::Issn
		);
	}

	public static function dateOutofbounds(string $msg): self
	{
		return new self(LogLevel::Info, $msg);
	}

	public static function dateReplaced(string $type, string $was, string $is): self
	{
		return new self(
			LogLevel::Info,
			"Embargo : la date de $type d'accès déclarée '{$was}' est remplacée par la date calculée '{$is}'.",
			AggregateId::DateReplaced
		);
	}

	public static function accessDateError(string $msg): self
	{
		return new self(LogLevel::Error, $msg, AggregateId::AccesstypeDate);
	}

	public static function accesstypeNotValid(string $value): self
	{
		return new self(LogLevel::Error, "'access_type' non reconnu : '$value'.", AggregateId::Accesstype);
	}

	public static function coveragedepthNotValid(string $value): self
	{
		return new self(LogLevel::Error, "Le contenu 'coverage_depth' n'est pas reconnu : '$value'", AggregateId::Coveragedepth);
	}

	public static function accesstypeEmpty(): self
	{
		return new self(LogLevel::Info, "'access_type' vide'.", AggregateId::AccesstypeEmpty);
	}

	public static function coveragedepthEmpty(): self
	{
		return new self(LogLevel::Info, "Le contenu (coverage_depth='fulltext', etc) est absent.", AggregateId::CoveragedepthEmpty);
	}
}
