<?php

namespace processes\kbart\logger;

class LogRenderer
{
	public static function renderHtml(LogStore $store): string
	{
		$html = "";
		$events = $store->getEvents();
		foreach ($events as $e) {
			$level = LogLevel::from($e->level)->text();
			$count = ($e->count > 1 ? sprintf(' <strong class="event-count">×%d</strong>', $e->count) : "");
			$message = \CHtml::encode($e->message);
			$html .= <<<HTML
				<div class="$level">
					<span class="label label-$level">{$level[0]}</span>
					$message $count
				</div>
				HTML;
		}
		return $html;
	}

	public static function renderText(LogStore $store): string
	{
		$events = $store->getEvents();
		$txt = [];
		foreach ($events as $e) {
			$txt[] = sprintf("* [%s] %s", LogLevel::from($e->level)->text(), $e->message);
		}
		return join("\n", $txt);
	}
}
