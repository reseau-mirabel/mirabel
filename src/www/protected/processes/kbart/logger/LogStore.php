<?php

namespace processes\kbart\logger;

class LogStore
{
	public bool $mergeByFamily = false;

	/**
	 * @var LogEvent[]
	 */
	private array $events = [];

	private int $maxLevel = 0;

	public function addEvent(LogEvent $event): void
	{
		// Update the max level
		if ($event->level > $this->maxLevel) {
			$this->maxLevel = $event->level;
		}

		// Check for family (unique ID) => if present, increase its count
		if ($event->family && $this->mergeByFamily) {
			if (isset($this->events[$event->family])) {
				$this->events[$event->family]->count += $event->count;
			} else {
				$this->events[$event->family] = $event;
			}
		} else {
			$this->events[] = $event;
		}
	}

	/**
	 * @return LogEvent[]
	 */
	public function getEvents(): array
	{
		return $this->events;
	}

	public function getMaxLevel(): string
	{
		return LogLevel::tryFrom($this->maxLevel)?->text() ?? '';
	}

	public function getMaxLevelInt(): int
	{
		return $this->maxLevel;
	}

	public function hasEvents(): bool
	{
		return count($this->events) > 0;
	}

	public function hasFamily(AggregateId $id): bool
	{
		return isset($this->events[$id->value]);
	}
}
