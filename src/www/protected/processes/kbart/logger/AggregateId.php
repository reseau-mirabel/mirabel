<?php

namespace processes\kbart\logger;

enum AggregateId: string
{
	case Accesstype = 'accesstype';
	case AccesstypeDate = 'accesstypedate';
	case AccesstypeEmpty = 'noaccesstype';
	case Coveragedepth = 'coveragedepth';
	case CoveragedepthEmpty = 'nocoveragedepth';
	case DateReplaced = 'datereplaced';
	case Issn = 'issn';
	case NoId = 'noid';
	case Type = 'type';
	case Utf8 = 'utf8';

	public function text(): string
	{
		return match ($this) {
			self::Accesstype => "Valeur incorrecte pour access_type",
			self::AccesstypeDate => "Date absente ou incohérente pour l'accès",
			self::AccesstypeEmpty => "access_type vide",
			self::Coveragedepth => "Valeur incorrecte pour coverage_depth",
			self::CoveragedepthEmpty => "coverage_depth vide",
			self::DateReplaced => "Date de début ou fin remplacée par une date calculée",
			self::Issn => "ISSN non valide",
			self::NoId => "Ligne sans ISSN-(P|E) ni ID interne",
			self::Type => "Le type KBART n'est pas 'serial', donc ce n'est pas un périodique.",
			self::Utf8 => "La ligne contient des caractères illicites.",
		};
	}
}
