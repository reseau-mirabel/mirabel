<?php

namespace processes\kbart;

trait LoggerTrait
{
	private ?Logger $logger = null;

	public function setLogger(Logger $logger): void
	{
		$this->logger = $logger;
	}

	public function getLogger(): Logger
	{
		if ($this->logger === null) {
			/**
			 * Use the global import logger, which is initialized at first use.
			 */
			$logger = \Yii::app()->getComponent('importLogger');
			assert($logger instanceof Logger);
			$this->setLogger($logger);
		}
		assert($this->logger instanceof Logger);
		return $this->logger;
	}
}
