<?php

namespace processes\kbart\identify;

use processes\kbart\logger\LogEvent;
use processes\kbart\logger\LogFamily;
use processes\kbart\Logger;
use processes\kbart\read\RowData;
use Titre;

/**
 * Identify a single journal, from RowData to Titre.
 */
class Single
{
	private \Ressource $ressource;

	private Logger $logger;

	public function __construct(Logger $logger)
	{
		$this->logger = $logger;
	}

	public function setRessource(\Ressource $ressource): self
	{
		$this->ressource = $ressource;
		return $this;
	}

	/**
	 * @param Titre $title
	 * @return \Service[]
	 */
	public function findExistingServices(Titre $title): array
	{
		return \Service::model()->findAllByAttributes([
			'titreId' => $title->id,
			'ressourceId' => $this->ressource->id,
		]);
	}

	/**
	 * Identify a Titre by its attributes.
	 *
	 * By default (overwritten by $this->ressource->importIdentifications) :
	 * - ISSN-P (et ISSNL)
	 * - ISSN-E
	 * - ressource + idInterne
	 * - URL
	 * - titre + editeur
	 *
	 * @return ?Titre Existing Titre, or null if not found.
	 */
	public function identify(RowData $attributes): ?Titre
	{
		foreach ($this->getMethods() as $method) {
			if (method_exists($this, "identifyBy" . $method)) {
				$title = call_user_func([$this, "identifyBy$method"], $attributes);
				if ($title) {
					return $title;
				}
			} else {
				$event = new LogEvent(LogFamily::notSerialPublication($method));
				$this->logger->addLocal($event);
			}
		}
		$this->logger->addLocal(new LogEvent(LogFamily::debug(), "Titre non identifié"));
		return null;
	}

	/**
	 * List of strings that describe identification methods
	 *
	 * @return string[]
	 */
	protected function getMethods(): array
	{
		if (empty($this->ressource->importIdentifications)) {
			return ['issn', 'issne', 'idInterne'];
		}
		return array_filter(preg_split('/\s*,\s*/', trim($this->ressource->importIdentifications, " ,")));
	}

	protected function identifyByIssn(RowData $attributes): ?Titre
	{
		if (!empty($attributes->issn)) {
			$title = Titre::model()->findBySql(
				"SELECT t.* FROM Titre t JOIN Issn i ON t.id = i.titreId WHERE i.issn = :issn ORDER BY (t.obsoletePar IS NULL) DESC, dateFin DESC",
				[':issn' => $attributes->issn]
			);
			if ($title) {
				$this->logger->addLocal(new LogEvent(LogFamily::debug(), "Titre identifié par issn {$attributes->issn}"));
				return $title;
			}
			$issnlTitle = Titre::model()->findBySql(
				"SELECT t.* FROM Titre t JOIN Issn i ON t.id = i.titreId WHERE i.issnl = :issn",
				[':issn' => $attributes->issn]
			);
			if ($issnlTitle) {
				$this->logger->addLocal(new LogEvent(LogFamily::debug(), "Titre identifié par issn (issnl) {$attributes->issn}"));
				return $issnlTitle;
			}
		}
		return null;
	}

	protected function identifyByIssne(RowData $attributes): ?Titre
	{
		if (empty($attributes->issne)) {
			return null;
		}
		$title = Titre::model()->findBySql(
			"SELECT t.* FROM Titre t JOIN Issn i ON t.id = i.titreId WHERE i.issn = :issn ORDER BY (t.obsoletePar IS NULL) DESC, dateFin DESC",
			[':issn' => $attributes->issne]
		);
		if ($title) {
			$this->logger->addLocal(new LogEvent(LogFamily::debug(), "Titre identifié par issne {$attributes->issne}"));
			return $title;
		}
		$issnlTitle = Titre::model()->findBySql(
			"SELECT t.* FROM Titre t JOIN Issn i ON t.id = i.titreId WHERE i.issnl = :issn",
			[':issn' => $attributes->issne]
		);
		if ($issnlTitle) {
			$this->logger->addLocal(new LogEvent(LogFamily::debug(), "Titre identifié par issne (issnl) {$attributes->issne}"));
			return $issnlTitle;
		}
		return null;
	}

	protected function identifyByIdInterne(RowData $attributes): ?Titre
	{
		$title = null;
		if (!empty($attributes->id)) {
			$title = Titre::model()->findBySql(<<<EOSQL
				SELECT t.*
				FROM Titre t JOIN Identification i ON t.id = i.titreId
				WHERE i.ressourceId = :rid AND idInterne = :idint
				EOSQL,
				[':rid' => $this->ressource->id, ':idint' => $attributes->id]
			);
			if ($title) {
				$this->logger->addLocal(new LogEvent(LogFamily::debug(), "Titre identifié par ID interne '{$attributes->id}'"));
			}
		}
		return $title;
	}

	protected function identifyByUrl(RowData $attributes): ?Titre
	{
		$title = null;
		if (!empty($attributes->url)) {
			$title = Titre::model()->findByAttributes(
				['titre' => $attributes->title, 'url' => $attributes->url]
			);
			if ($title) {
				$this->logger->addLocal(new LogEvent(LogFamily::debug(), 'Titre identifié par son URL'));
			}
		}
		return $title;
	}

	protected function identifyByEditeur(RowData $attributes): ?Titre
	{
		$title = null;
		if (!empty($attributes->publisherName)) {
			$sql = <<<EOSQL
				SELECT t.*
				FROM Titre t
					JOIN Titre_Editeur te ON te.titreId = t.id
					JOIN Editeur e ON e.id = te.editeurId
				WHERE t.titre = :titre AND e.nom = :editeur
				EOSQL;
			$titles = Titre::model()->findAllBySql(
				$sql,
				['titre' => $attributes->title, 'editeur' => $attributes->publisherName]
			);
			if (count($titles) === 1) {
				$title = $titles[0];
				$this->logger->addLocal(new LogEvent(LogFamily::debug(), 'Titre identifié par nom+éditeur'));
			}
		}
		return $title;
	}
}
