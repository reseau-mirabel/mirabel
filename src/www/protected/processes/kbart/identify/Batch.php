<?php

namespace processes\kbart\identify;

use components\db\InsertBuffer;
use processes\kbart\data\CrossedData;

class Batch
{
	private \Ressource $ressource;

	private array $columns = [];

	public function __construct(\Ressource $ressource)
	{
		$this->ressource = $ressource;
	}

	public function addServices(CrossedData $data, array $collectionIds): void
	{
		$ids = [];
		foreach ($data->known as $rowid => $analyzed) {
			$ids[(int) $analyzed->title->id] = $rowid;
		}
		if (!$ids) {
			return;
		}
		$idsStr = join(",", array_keys($ids));

		if (!$collectionIds) {
			$sql = "SELECT * FROM Service WHERE ressourceId = {$this->ressource->id} AND titreId IN ($idsStr)";
		} else {
			sort($collectionIds, SORT_NUMERIC);
			$cids = join(',', $collectionIds);
			// Search the Service records whose collections
			// are a non-empty subset of the target collections.
			/*
			$sql = <<<SQL
				WITH InCollections AS (
					SELECT serviceId, GROUP_CONCAT(collectionId ORDER BY collectionId SEPARATOR ',') AS collections
					FROM Service_Collection
					GROUP BY serviceId
				)
				SELECT s.*
				FROM Service s
					JOIN InCollections c ON s.id = c.serviceId
				WHERE LOCATE(c.collections, '$cids') > 0
					AND ressourceId = {$this->ressource->id} AND titreId IN ($idsStr)
				SQL;
			 */
			// Search the Service records whose collections
			// intersect the target collections.
			$sql = <<<SQL
				SELECT DISTINCT s.*
				FROM Service s
					JOIN Service_Collection sc ON s.id = sc.serviceId
				WHERE ressourceId = {$this->ressource->id} AND titreId IN ($idsStr)
					AND sc.collectionId IN ($cids)
				SQL;
		}
		$cmd = \Yii::app()->db->createCommand($sql);
		foreach ($cmd->query() as $row) {
			$rowid = $ids[$row['titreId']];
			$service = \Service::model()->populateRecord($row);
			if ($service instanceof \Service) {
				$data->known[$rowid]->services[] = $service;
			}
		}
	}

	/**
	 * Identify a list of RowData into a list of Titre.
	 *
	 * By default (overwritten by $this->ressource->importIdentifications) :
	 * - ISSN-P (et ISSNL)
	 * - ISSN-E
	 * - ressource + idInterne
	 * - URL
	 * - titre + editeur
	 *
	 * @param \processes\kbart\read\RowData[] $rows
	 * @return array<null|\Titre> Existing Titre, or null if not found.
	 */
	public function identifyAll(array $rows): array
	{
		$methods = self::getMethods($this->ressource);
		$this->columns = $this->createTempTable($methods);
		$this->insert($rows);

		foreach ($methods as $m) {
			$methodName = "identifyBy" . ucfirst($m);
			if (method_exists($this, $methodName)) {
				call_user_func([$this, $methodName]);
			}
		}

		$results = self::extractResults();
		\Yii::app()->db->createCommand("DROP TEMPORARY TABLE IdentifyTitre")->execute();
		return $results;
	}

	protected function identifyByIssn(): void
	{
		$support = \Issn::SUPPORT_PAPIER;
		\Yii::app()->db
			->createCommand(<<<SQL
				UPDATE IdentifyTitre t
					JOIN Issn i ON t.issn = i.issn AND i.support = '$support'
				SET t.titreId = i.titreId
				WHERE t.titreId IS NULL AND t.issn <> ''
				SQL
			)
			->execute();
	}

	protected function identifyByIssne(): void
	{
		$support = \Issn::SUPPORT_ELECTRONIQUE;
		\Yii::app()->db
			->createCommand(<<<SQL
				UPDATE IdentifyTitre t
					JOIN Issn i ON t.issne = i.issn AND i.support = '$support'
				SET t.titreId = i.titreId
				WHERE t.titreId IS NULL AND t.issne <> ''
				SQL
			)
			->execute();
	}

	protected function identifyByIdinterne(): void
	{
		\Yii::app()->db
			->createCommand(<<<SQL
				UPDATE IdentifyTitre t
					JOIN Identification i ON t.idinterne = i.idInterne AND i.ressourceId = {$this->ressource->id}
				SET t.titreId = i.titreId
				WHERE t.titreId IS NULL AND t.idinterne <> ''
				SQL
			)
			->execute();
	}

	protected function identifyByUrl(): void
	{
		\Yii::app()->db
			->createCommand(<<<SQL
				UPDATE IdentifyTitre t
					JOIN Titre i ON t.url = UNHEX(MD5(i.url))
				SET t.titreId = i.id
				WHERE t.titreId IS NULL AND t.url IS NOT NULL AND i.url <> ''
				SQL
			)
			->execute();
	}

	protected function identifyByEditeur(): void
	{
		\Yii::app()->db
			->createCommand(<<<SQL
				UPDATE IdentifyTitre t
					JOIN Editeur e ON t.editeur = UNHEX(MD5(e.nom))
				SET t.editeurId = e.id
				SQL
			)
			->execute();
		\Yii::app()->db
			->createCommand(<<<SQL
				UPDATE IdentifyTitre t
					JOIN Editeur e ON t.editeur = UNHEX(MD5(concat(e.prefixe, e.nom)))
				SET t.editeurId = e.id
				SQL
			)
			->execute();
		\Yii::app()->db
			->createCommand(<<<SQL
				UPDATE IdentifyTitre t
					JOIN Titre_Editeur te ON te.editeurId = t.editeurId
					JOIN Titre i ON i.id = te.titreId
				SET t.titreId = i.id
				WHERE t.titreId IS NULL AND t.titre = UNHEX(MD5(i.titre))
				SQL
			)
			->execute();
		\Yii::app()->db
			->createCommand(<<<SQL
				UPDATE IdentifyTitre t
					JOIN Titre_Editeur te ON te.editeurId = t.editeurId
					JOIN Titre i ON i.id = te.titreId
				SET t.titreId = i.id
				WHERE t.titreId IS NULL AND t.titre = UNHEX(MD5(concat(i.prefixe, i.titre)))
				SQL
			)
			->execute();
	}

	/**
	 * List of strings that describe identification methods
	 *
	 * @return string[]
	 */
	protected static function getMethods(\Ressource $ressource): array
	{
		if (empty($ressource->importIdentifications)) {
			return ['issn', 'issne', 'idInterne'];
		}
		return array_filter(
			preg_split('/\s*,\s*/', strtolower(trim($ressource->importIdentifications, " ,")))
		);
	}

	/**
	 * Create a temporary table where we store the KBART data to match it against M.
	 *
	 * @return string[] names of data columns in the temp table
	 */
	private function createTempTable(array $columns): array
	{
		$extra = [];
		foreach ($columns as $col) {
			$c = strtolower($col);
			$extra = array_merge(
				$extra,
				match ($c) {
					'issn' => ['issn' => "issn CHAR(9) NOT NULL COLLATE ascii_bin"],
					'issne' => ['issne' => "issne CHAR(9) NOT NULL COLLATE ascii_bin"],
					'idinterne' => ['idinterne' => "idinterne VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci"],
					'url' => ['url' => "url BINARY(16) NULL"],
					'editeur' => [
						'titre' => "titre BINARY(16) NULL",
						'editeur' => "editeur BINARY(16) NULL",
						'editeurId' => "editeurId INT UNSIGNED DEFAULT NULL",
					],
					default => throw new \Exception("Méthode d'identification inconnue: '$c'"),
				}
			);
		}
		$extraSql = join(', ', $extra);
		$sql = <<<SQL
			CREATE TEMPORARY TABLE IdentifyTitre (
				rowid INT UNSIGNED NOT NULL,
				titreId INT UNSIGNED DEFAULT NULL,
				$extraSql
			)
			SQL;
		\Yii::app()->db->createCommand($sql)->execute();
		return array_keys($extra);
	}

	/**
	 * Insert all of the KBART data into the temporary table.
	 *
	 * NB: For a kbart file with 5k rows,
	 * creating indexes on issn and issne after the inserts
	 * did not reduce significatively the execution time.
	 *
	 * @param \processes\kbart\read\RowData[] $rows
	 */
	private function insert(array $rows): void
	{
		$dataColumns = [];
		foreach ($this->columns as $c) {
			if ($c === 'editeurId') {
				continue;
			}
			$dataColumns[$c] = match ($c) {
				'url', 'titre', 'editeur' => "UNHEX(?)",
				default => "?",
			};
		}

		$columnsSql = "rowid, " . join(", ", array_keys($dataColumns));
		$buffer = new InsertBuffer("INSERT INTO IdentifyTitre ($columnsSql) VALUES ");
		$buffer->setBatchSize(200);
		$buffer->setRowPattern("(?, " . join(',', array_values($dataColumns)) . ")");

		foreach ($rows as $r) {
			$data = [$r->position];
			foreach (array_keys($dataColumns) as $c) {
				$data[] = match ($c) {
					'issn' => $r->issn,
					'issne' => $r->issne,
					'idinterne' => $r->id,
					'url' => md5($r->url),
					'titre' => md5($r->title),
					'editeur' => md5($r->publisherName),
					default => throw new \Exception("Champ '$c' inconnu"),
				};
			}
			$buffer->add($data);
		}
		$buffer->close();
	}

	private static function extractResults(): array
	{
		$cmd = \Yii::app()->db->createCommand(<<<SQL
			SELECT k.rowid, t.id, t.revueId, t.titre, t.prefixe, t.sigle, t.dateDebut, t.dateFin, t.url, t.langues
			FROM IdentifyTitre k
				JOIN Titre t ON k.titreId = t.id
			ORDER BY k.rowid
			SQL
		);
		$titres = [];
		foreach ($cmd->query() as $row) {
			$rowid = (int) $row['rowid'];
			unset($row['rowid']);
			$titres[$rowid] = \Titre::model()->populateRecord($row, false);
		}
		return $titres;
	}
}
