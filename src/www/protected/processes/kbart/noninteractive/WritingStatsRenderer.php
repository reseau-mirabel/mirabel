<?php

namespace processes\kbart\noninteractive;

use processes\kbart\data\WritingStats;

class WritingStatsRenderer
{
	/**
	 * @var bool Display a block about unknown journals (that exist in the KBART but not in M).
	 */
	public bool $titreInconnus = false;

	/**
	 * @var string If set, will display this URL under the header.
	 */
	public string $url = "";

	public function getSummary(WritingStats $stats): string
	{
		return sprintf(
			"%7d titres rencontrés, dont %d nouveautés %s, %d modifications, %d erreurs, et %d créations d'un premier accès.\n",
			$stats->numIdentifiedJournals + count($stats->unknown),
			count($stats->unknown), // titres inconnus
			$this->titreInconnus ? "à traiter" : "ignorées",
			$stats->interventionTypes['service-U'] ?? 0,
			count($stats->interventionFailures),
			$stats->interventionTypes['service-C'] ?? 0
		);
	}

	public function renderAsText(WritingStats $stats, Config $config): string
	{
		$output = "";

		if ($this->url) {
			$output .= "{$this->url}\n";
		}

		if ($stats->interventionFailures) {
			$output .= "\n## Erreurs\n";
			$n = 1;
			foreach ($stats->interventionFailures as $info) {
				$output .= sprintf("%4d. %s : %s\n", $n, $info['titre'], strip_tags($info['errors']));
				$n++;
			}
		}

		if ($stats->interventionSuccesses && $config->verbose >= 1) {
			$output .= "\n## Interventions réussies\n";
			$n = 1;
			foreach ($stats->interventionSuccesses as $info) {
				$output .= sprintf("%4d. %s : %s  \n    %s  %s\n", $n, $info['titre'], $info['revueUrl'], $info['description'], $info['url']);
				$n++;
			}
		}

		if ($stats->ignored && $config->verbose >= 1) {
			$output .= "\n## Titres ignorés (données incomplètes, etc)\n";
			$n = 1;
			foreach ($stats->ignored as $info) {
				if ($config->verbose > 1) {
					$output .= sprintf("%4d. %s : %s  \n%s\n", $n, $info['titre'], $info['revueUrl'], $info['errors']);
				} else {
					$output .= sprintf("%4d. %s : %s\n", $n, $info['titre'], $info['revueUrl']);
				}
				$n++;
			}
		}

		if ($stats->unchanged && $config->verbose >= 2) {
			$output .= "\n## Titres inchangés\n";
			$n = 1;
			foreach ($stats->unchanged as $info) {
				$output .= sprintf("%4d. %s : %s\n", $n, $info['titre'], $info['revueUrl']);
				$n++;
			}
		}

		if ($stats->unknown && $this->titreInconnus && $config->verbose >= 2) {
			$output .= "\n## Titres inconnus de Mir@bel\n";
			$n = 1;
			foreach ($stats->unknown as ['title' => $name, 'url' => $url]) {
				if ($url) {
					$urlPath = array_shift($url);
					$output .= sprintf("%4d. %s  \n\t%s\n", $n, $name, \Yii::app()->createAbsoluteUrl($urlPath, $url));
				} else {
					$output .= sprintf("%4d. %s  \n\tSans ISSN\n", $n, $name);
				}
				$n++;
			}
		}

		return $output;
	}
}
