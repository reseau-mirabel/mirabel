<?php

namespace processes\kbart\noninteractive;

use components\FileStore;
use Intervention;
use processes\kbart\data\WritingStats;
use processes\kbart\Import;
use processes\kbart\State;

/**
 * Fully process a KBART file: reading, parsing, comparing to M, writing into the DB.
 */
class ImportAll
{
	private Config $config;

	private string $url = "";

	public function __construct(Config $config)
	{
		$this->config = $config;
	}

	/**
	 * Put the file from the requested URI into a local storage (first, download it if its an URL).
	 *
	 * @param string $uri Local path or URL. For the latter, '{{TODAY}}' is replaced by the ISO date.
	 */
	public function store(string $uri): void
	{
		if (preg_match('#^(https?|ftps?|sftp)://#', $uri)) {
			$this->url = str_replace('{{TODAY}}', date('Y-m-d'), $uri);
			$dlder = new \components\curl\Downloader('kbart');
			$dlder->download($this->url);
			$file = $dlder->getLocalFilepath();
			$this->config->fileName = $dlder->getRemoteFilename();
			$copy = false;
		} else {
			$file = $uri;
			$this->url = basename($file);
			$this->config->fileName = $this->url;
			if (!is_readable($file)) {
				throw new \Exception("Fichier '$file' inaccessible en lecture");
			}
			$copy = true;
		}

		$convertedPath = (new \processes\kbart\convert\Convert($file))->apply();
		if ($convertedPath !== $file) {
			$copy = false;
		}
		$this->config->filePath = $this->saveKbartFile($convertedPath, $copy);
		if ($this->config->filePath === '') {
			throw new \Exception("Erreur de lecture/stockage du fichier KBART.");
		}
	}

	/**
	 * Create the importer class that parses the file and prepares the Interventionlist.
	 *
	 * This method does not write anything.
	 */
	public function createImporter(): Import
	{
		$state = new State($this->config->ressource, $this->config->collectionIds);
		$state->title = $this->config->fileName;

		$import = new Import($state);
		$import->readFile($this->config->filePath);
		$import->checkConsistency();
		$import->identifyTitles();
		return $import;
	}

	public function getUrl(): string
	{
		return $this->url;
	}

	/**
	 * Apply each Intervention of the import and writes into the DB.
	 */
	public function write(Import $importer): WritingStats
	{
		$ressourceId = (int) $this->config->ressource->id;
		$writingStats = new WritingStats($ressourceId);

		$interventions = $importer->apply($this->config, $writingStats);
		foreach ($interventions as $i) {
			/** @var Intervention $i */
			if ($i->accept(true)) {
				$writingStats->addSuccess($i);
			} else {
				$writingStats->addFailure($i);
			}
		}
		return $writingStats;
	}

	/**
	 * @return string Path to the file in the store
	 */
	private function saveKbartFile(string $filePath, bool $copy): string
	{
		$fileStore = new FileStore('kbart-auto');
		$fileHash = md5_file($filePath);
		if ($fileHash === false) {
			return '';
		}
		$exists = $fileStore->get($fileHash);
		if ($exists !== '') {
			return $exists;
		}
		if (!$fileStore->set($fileHash, $filePath, $copy)) {
			return '';
		}
		$fileStore->flushOlderThan(15 * 86400); // Remove files older than 15 days
		return $fileStore->get($fileHash);
	}
}
