<?php

namespace processes\kbart\noninteractive;

class Config
{
	public ?\Ressource $ressource = null;

	/**
	 * @var int[]
	 */
	public array $collectionIds = [];

	/**
	 * @var string Valeur parmi \Service::$enumType (""|"Texte intégral"|...)
	 */
	public string $contenu = "";

	/**
	 * @var ""|"Libre"|"Restreint" Diffusion AKA acces
	 */
	public string $diffusion = '';

	public bool $absents = false;

	public bool $lacunaire = false;

	public bool $overrideDatesWithEmbargo;

	public bool $selection = false;

	public bool $suppressions = false;

	public string $fileName = '';

	public string $filePath = '';

	public int $verbose = 1;

	public function setDefaultAccess(string $access): void
	{
		if ($access !== '' && !in_array($access, \Service::$enumAcces)) {
			throw new \Exception("Paramètre 'acces' non valide");
		}
		$this->diffusion = $access;
	}

	public function setDefaultCoveragedepth(string $contenu): void
	{
		if ($contenu !== '' && !isset(\Service::$enumType[$contenu])) {
			throw new \Exception("Paramètre 'contenu' non valide : '$contenu'");
		}
		$this->contenu = $contenu;
	}

	public function setRessourceAndCollections(int $ressourceId, array $collectionIds): void
	{
		$this->ressource = \Ressource::model()->findByPk($ressourceId);
		if ($this->ressource === null) {
			throw new \Exception("La ressource d'ID $ressourceId n'existe pas.");
		}

		if ($collectionIds) {
			$ids = array_map('intval', $collectionIds);
			$this->collectionIds = array_map(
				'intval',
				\Yii::app()->db
					->createCommand("SELECT id FROM Collection WHERE ressourceId = {$this->ressource->id} AND id IN (" . join(',', $ids) . ")")
					->queryColumn()
			);
			if (count($collectionIds) !== count($this->collectionIds)) {
				throw new \Exception("La liste des collections est incorrecte pour cette ressource.");
			}
		} else {
			$this->collectionIds = [];
			$hasCollection = (bool) \Yii::app()->db
				->createCommand("SELECT 1 FROM Collection WHERE ressourceId = {$this->ressource->id} LIMIT 1")
				->queryColumn();
			if ($hasCollection) {
				throw new \Exception("Impossible d'importer hors-collection dans une ressource à collections.");
			}
		}
	}
}
