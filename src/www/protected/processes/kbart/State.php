<?php

namespace processes\kbart;

use processes\kbart\data\CrossedData;
use processes\kbart\read\FileData;
use Ressource;

class State
{
	public const STEP_FILE = 1;

	public const STEP_CONSISTENCY = 2;

	public const STEP_IDENTIFY = 3;

	public const STEP_DIFF = 4;

	/**
	 * @var ?CrossedData Data from the KBART coupled with data from M.
	 */
	public ?CrossedData $crossedData = null;

	public ?FileData $fileData = null;

	public string $hash = '';

	public Logger $logs;

	/**
	 * @var string Any text that could describe this import. Usually the uploaded file name.
	 */
	public string $title = '';

	/**
	 * @var int[]
	 */
	public array $collectionIds;

	public array $filter = [];

	/**
	 * @var int[]
	 */
	private array $done = [];

	private Ressource $ressource;

	final public function __construct(Ressource $ressource, array $collectionIds = [], array $done = [])
	{
		$this->ressource = $ressource;
		$this->collectionIds = $collectionIds;
		$this->done = $done;
		$this->logs = new Logger();
	}

	public static function load(string $path): self
	{
		if (!file_exists($path)) {
			throw new \Exception("File '$path' was not found.");
		}
		$data = unserialize(file_get_contents($path));
		if ($data === false) {
			throw new \Exception("File '$path' could not be parsed.");
		}

		$ressource = Ressource::model()->findByPk($data['ressourceId']);
		if (!($ressource instanceof Ressource)) {
			throw new \Exception("Ressource with ID {$data['ressourceId']} was not found.");
		}
		$new = new static($ressource, $data['collectionIds'], $data['done']);
		$new->crossedData = $data['crossedData'];
		$new->fileData = $data['fileData'];
		$new->hash = $data['hash'];
		$new->logs = $data['logs'] ?? new Logger();
		$new->title = $data['title'];
		$new->filter = $data['filter'];
		return $new;
	}

	/**
	 * @return int[]
	 */
	public function getCollectionIds(): array
	{
		return $this->collectionIds;
	}

	/**
	 * @return string[]
	 */
	public function getCollectionNames(): array
	{
		if (!$this->collectionIds) {
			return [];
		}
		$ids = join(",", array_map('intval', $this->collectionIds));
		return \Yii::app()->db
			->createCommand("SELECT nom FROM Collection WHERE id IN ($ids) ORDER BY nom")
			->queryColumn();
	}

	public function getRessource(): Ressource
	{
		return $this->ressource;
	}

	public function getFilter(): data\Filter
	{
		return new \processes\kbart\data\Filter($this->filter);
	}

	public function save(string $path): bool
	{
		$data = [
			'ressourceId' => (int) $this->ressource->id,
			'collectionIds' => $this->collectionIds,
			'fileData' => $this->fileData,
			'hash' => $this->hash,
			'logs' => $this->logs,
			'crossedData' => $this->crossedData,
			'done' => $this->done,
			'title' => $this->title,
			'filter' => $this->filter,
		];
		return (file_put_contents($path, serialize($data)) > 0);
	}

	public function addStep(int $step): void
	{
		if ($step === self::STEP_FILE) {
			$this->done = [self::STEP_FILE];
		} else {
			$this->done[] = $step;
		}
	}

	public function hasStep(int $step): bool
	{
		return in_array($step, $this->done, true);
	}
}
