<?php

namespace processes\kbart\read;

class FileData
{
	/**
	 * @var int Number of data rows in the KBART, before any filtering
	 */
	public int $numRows = 0;

	/**
	 * @var string The KBART file source.
	 */
	private string $path;

	/**
	 * @var string A signature of the contents of the file.
	 */
	private string $contentHash;

	/**
	 * @var RowData[]
	 */
	private array $rows = [];

	public function __construct(string $path, string $contentHash)
	{
		$this->path = $path;
		$this->contentHash = $contentHash;
	}

	public function getFileName(): string
	{
		return basename($this->path);
	}

	public function getContentsHash(): string
	{
		return $this->contentHash;
	}

	public function addRow(RowData $row): void
	{
		$this->rows[$row->position] = $row;
	}

	/**
	 * @return RowData[]
	 */
	public function getRows(): array
	{
		return $this->rows;
	}
}
