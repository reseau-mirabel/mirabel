<?php

namespace processes\kbart\read;

use Exception;

/**
 * RowData contains the converted data.
 */
class RowData
{
	public int $position = 0;

	public string $title = '';

	public string $prefix = '';

	public string $issn = '';

	public string $issne = '';

	public string $id = ''; // ID local to the resource (i.e. to the KBART author)

	public string $url = '';

	/**
	 * @var ?Issue Containing (date, #vol, #issue) of the first issue
	 */
	public $accessBegin;

	/**
	 * @var ?Issue Containing (date, #vol, #issue) of the last issue
	 */
	public $accessEnd;

	// vanilla fields...

	public string $accessType = '';

	public string $embargoInfo = '';

	public string $coverageDepth = '';

	public string $notes = '';

	public string $publisherName = '';

	/**
	 * @todo Remove this method once PHP >= 8.2 (dynamic properties are obsolete).
	 */
	public function __set(string $name, $value): void
	{
		if (!property_exists(self::class, $name)) {
			throw new Exception("RowData: wrong KBART attribute name");
		}
		$this->{$name} = $value;
	}

	/**
	 * Mass assign.
	 *
	 * @param array $values K/V
	 * @throws Exception on unknown keys
	 */
	public function assign(array $values): void
	{
		foreach ($values as $k => $v) {
			if (!property_exists(self::class, $k)) {
				throw new Exception("RowData: wrong KBART attribute name '$k'");
			}
			$this->{$k} = $v;
		}
	}
}
