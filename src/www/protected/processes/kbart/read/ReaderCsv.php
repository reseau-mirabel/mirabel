<?php

namespace processes\kbart\read;

use processes\kbart\logger\LogEvent;
use processes\kbart\logger\LogFamily;
use processes\kbart\LoggerTrait;

/**
 * Reads a CSV file, row after row.
 */
class ReaderCsv
{
	use LoggerTrait;

	private string $separator;

	private string $encoding = 'UTF-8';

	/**
	 * @var resource
	 */
	private $fileHandle;

	private int $position = 0;

	public function __construct(string $path, string $separator, string $encoding = 'UTF-8')
	{
		if ($separator === '\t' || strncasecmp($separator, 'tab', 3) === 0) {
			$separator = "\t";
		}
		$this->separator = $separator;
		$this->encoding = strtoupper($encoding);
		if (!$this->setFile($path)) {
			throw new \Exception("Erreur d'accès au fichier CSV.");
		}
	}

	/**
	 * Returns the list of the values from the current row of the CSV.
	 *
	 * Does NOT trim or clean contents.
	 *
	 * @throws \Exception on fatal errors
	 * @return array|null Cells from the current row
	 */
	public function read(): ?array
	{
		do {
			$row = fgetcsv($this->fileHandle, 0, $this->separator);
			if ($row && strncmp((string) $row[0], "\xEF\xBB\xBF", 3) === 0) {
				$row[0] = substr($row[0], 3); // Remove this UTF-8 "ero width no-break space
			}
			$this->position++;
			if ($this->logger) {
				$this->logger->setRowContext($this->position);
			}
			if ($this->position === 1) {
				if ($row === false) {
					throw new \Exception("Le fichier-source est vide.");
				}
				if (count($row) > 0) {
					$bom = "\xef\xbb\xbf";
					if (strncmp($row[0], $bom, 4) === 0) {
						$row[0] = substr($row[0], 4);
					}
				} else {
					throw new \Exception("La première ligne du fichier ne contient pas de séparateur de colonnes.");
				}
			}
			$retry = false;
			if (is_array($row) && !empty($row)) {
				if (mb_detect_encoding(join(" ", $row), 'UTF-8') !== 'UTF-8') {
					if ($this->logger) {
						$event = new LogEvent(LogFamily::localUtf8Encoding());
						$this->logger->addGlobal($event);
					}
					$retry = true;
				}
			}
		} while ($retry);
		return $row ?: null;
	}

	/**
	 * Read while skipping empty lines
	 */
	public function readSkippingEmpty(): ?array
	{
		do {
			$row = $this->read();
		} while (is_array($row) && count(array_filter($row)) === 0);
		return $row ?: null;
	}

	/**
	 * @return int
	 */
	public function getCurrentRowNumber(): int
	{
		return $this->position;
	}

	/**
	 * Reads the whole input file.
	 */
	private function setFile(string $path): bool
	{
		$this->initUtf8Locale();
		$this->fileHandle = $this->openFile($path);
		if ($this->fileHandle === null) {
			return false;
		}
		return true;
	}

	/**
	 * @throws \Exception
	 * @return ?resource File handle
	 */
	private function openFile(string $path)
	{
		if ($this->encoding === 'UTF-8') {
			$r = fopen($path, "r");
			if ($r === false) {
				throw new \Exception("Erreur d'ouverture du fichier '" . basename($path) . "'");
			}
			return $r;
		}

		$contents = file_get_contents($path); // may throw an \Error Exception
		if (!$contents) {
			throw new \Exception("Erreur de lecture du fichier '" . basename($path) . "'");
		}
		if (!mb_check_encoding($contents, $this->encoding)) {
			throw new \Exception("Le fichier n'est pas encodé en '{$this->encoding}' comme attendu.");
		}
		$unicodeContents = mb_convert_encoding(
			str_replace("\x0D\x0A", "\n", $contents), // Windows EOL
			'UTF-8',
			$this->encoding
		);
		$handle = tmpfile();
		if (!$handle) {
			return null;
		}
		fwrite($handle, $unicodeContents);
		fseek($handle, 0);
		return $handle;
	}

	private function initUtf8Locale(): void
	{
		$currentLocale = setlocale(LC_CTYPE, "0");
		if (!preg_match('/utf-?8/', $currentLocale)) {
			setlocale(LC_CTYPE, ['C.UTF-8', 'C.utf8', 'en_US.UTF-8', 'fr_FR.UTF-8']);
		}
	}
}
