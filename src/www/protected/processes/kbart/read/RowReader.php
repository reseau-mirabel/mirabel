<?php

namespace processes\kbart\read;

use models\validators\IssnValidator;
use processes\kbart\logger\LogEvent;
use processes\kbart\logger\LogFamily;
use processes\kbart\Logger;
use processes\kbart\LoggerTrait;

/**
 * Converts a single row of a KBART file into RowData.
 */
class RowReader
{
	use LoggerTrait;

	public array $prefixes = [];

	private Header $header;

	//	"publication_type" => "publication_type",
	//	"access_type" => "access_type",

	public function __construct(Header $header, ?Logger $logger)
	{
		$this->header = $header;
		if ($logger !== null) {
			$this->setLogger($logger);
		}
		Issue::$logger = $this->getLogger();
	}

	public function setTitlePrefixes(array $titlePrefixes): void
	{
		$this->prefixes = $titlePrefixes;
	}

	/**
	 * @param array $input Raw data from the file
	 */
	public function parse(array $input, int $rowPosition): ?RowData
	{
		$this->logger->setRowContext($rowPosition);

		$header = $this->header;
		$header->setRowData($input);

		$type = strtolower($header->getColumn("publication_type"));
		if ($type && $type !== "serial") {
			// not a journal, ignoring
			$event = new LogEvent(LogFamily::notSerialPublication($type));
			$this->logger->addLocal($event);
			return null;
		}

		$output = new RowData();
		$output->position = $rowPosition;

		// title attributes
		[$output->prefix, $output->title] = $this->detectTitlePrefix($header->getColumn("publication_title"));
		$output->issn = self::cleanIssn($header->getColumn("print_identifier"));
		$output->issne = self::cleanIssn($header->getColumn("online_identifier"));
		$output->id = $header->getColumn("title_id");
		$output->url = $header->getColumn("title_url");

		$output->accessType = $header->getOptionalColumn("access_type") ?? '';
		$output->embargoInfo = $header->getColumn("embargo_info");
		$output->coverageDepth = $header->getColumn("coverage_depth");
		$output->notes = trim(($header->getOptionalColumn("notes") ?? '') . "\n" . $header->getOptionalColumn("coverage_notes"));
		$output->publisherName = $header->getColumn("publisher_name");

		try {
			$output->accessBegin = Issue::create(
				$header->getColumn("date_first_issue_online"),
				$header->getColumn("num_first_vol_online"),
				$header->getColumn("num_first_issue_online")
			);
			$output->accessBegin->validate($this->logger);
		} catch (\Exception $e) {
			$event = new LogEvent(LogFamily::warning(), "Erreur en lisant les données de début : {$e->getMessage()}");
			$this->logger->addLocal($event);
		}

		try {
			$output->accessEnd = Issue::create(
				$header->getColumn("date_last_issue_online"),
				$header->getColumn("num_last_vol_online"),
				$header->getColumn("num_last_issue_online")
			);
			$output->accessEnd->validate($this->logger);
		} catch (\Exception $e) {
			$event = new LogEvent(LogFamily::warning(), "Erreur en lisant les données de fin : {$e->getMessage()}");
			$this->logger->addLocal($event);
		}

		$header->unsetRowData();

		return $output;
	}

	public function validateOutput(RowData $data): bool
	{
		static $v = null;
		if ($v === null) {
			$v = new IssnValidator();
		}
		$hasErrors = false;
		foreach (['issn', 'issne'] as $type) {
			$issn = $data->{$type};
			if (!empty($issn) && !$v->validateString($issn)) {
				$hasErrors = true;
				$data->{$type} = ''; // remove the ISSN value
				$event = new LogEvent(LogFamily::issnNotValid($type, $issn, $v->getErrors()));
				$this->logger->addLocal($event);
			}
		}
		return $hasErrors;
	}

	/**
	 * @return list{string, string} [prefix, title]
	 */
	private function detectTitlePrefix(string $title): array
	{
		$prefixes = array_filter($this->prefixes);
		foreach ($prefixes as $p) {
			if (mb_stripos($title, $p) === 0) {
				return [
					substr($title, 0, strlen($p)),
					substr($title, strlen($p)),
				];
			}
		}
		return ['', $title];
	}

	/**
	 * @return string clean ISSN or empty string
	 */
	private static function cleanIssn(string $issn): string
	{
		if (empty($issn)) {
			return "";
		}
		if (preg_match('/\s*en cours\s*/i', $issn)) {
			return "";
		}
		if (strpos($issn, ' ') !== false) {
			return trim(strtok($issn, "  \t"));
		}
		return str_replace('x', 'X', trim($issn));
	}
}
