<?php

namespace processes\kbart\read;

use processes\kbart\logger\LogEvent;
use processes\kbart\logger\LogFamily;
use processes\kbart\Logger;
use processes\kbart\Normalize;

/**
 * Issue of a journal
 */
class Issue
{
	public static Logger $logger;

	/**
	 * @var ?string e.g. "2008-12-29"
	 */
	public ?string $date = null;

	/**
	 * @var ?string e.g. "vol. I no 13"
	 */
	public ?string $numbering = null;

	public ?int $vol = null;

	public ?int $issue = null;

	public function __toString()
	{
		if ($this->isEmpty()) {
			return "";
		}
		return sprintf('date="%s" / livraison="%s" (vol=%s num=%s)', $this->date, $this->numbering, $this->vol, $this->issue);
	}

	public static function create(?string $date, ?string $vol, ?string $issue): Issue
	{
		$new = new self;
		if (empty($date) && empty($vol) && empty($issue)) {
			return $new;
		}

		$new->date = $date;
		$new->vol = self::toNum($vol);
		$new->issue = self::toNum($issue);
		if ($vol && $issue) {
			$new->numbering = sprintf("Vol. %s, no %s", $vol, $issue);
		} elseif ($vol) {
			$new->numbering = sprintf("Vol. %s", $vol);
		} elseif ($issue) {
			$new->numbering = sprintf("no %s", $issue);
		}
		return $new;
	}

	public function isEmpty(): bool
	{
		return empty($this->date) && empty($this->vol) && empty($this->issue);
	}

	public function validate(Logger $logger): void
	{
		if ($this->date) {
			$date = Normalize::toIsoDate($this->date);
			if ($this->date !== $date) {
				$event = new LogEvent(LogFamily::warning(), "La date '{$this->date}' n'est pas au format ISO. '{$date}' sera utilisé.");
				$logger->addLocal($event);
				$this->date = $date;
			}
		}
	}

	private static function toNum(?string $text): ?int
	{
		if ($text === null || $text === '') {
			return null;
		}
		if (ctype_digit($text) || preg_match('/^\d/', $text)) {
			return (int) $text;
		}
		if (preg_match('/^[IVXLCDM]+$/', $text)) {
			return Normalize::arabizeRomanNumber($text);
		}
		$event = new LogEvent(LogFamily::error(), "Erreur de lecture d'une issue : la valeur '$text' n'est pas numérique.");
		self::$logger->addLocal($event);
		return null;
	}
}
