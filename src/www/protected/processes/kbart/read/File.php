<?php

namespace processes\kbart\read;

use processes\kbart\data\Filter;
use processes\kbart\Logger;
use processes\kbart\LoggerTrait;

/**
 * KBART File to import.
 */
class File
{
	use LoggerTrait;

	private string $path;

	private string $contentHash;

	private ReaderCsv $csvReader;

	private RowReader $rowReader;

	public function __construct(Logger $logger)
	{
		$this->setLogger($logger);
	}

	public function load(string $path, string $name = ''): void
	{
		$fileName = $name ?: basename($path);
		$this->logger->setFileContext($fileName);
		if (is_readable($path)) {
			$this->path = $path;
			$this->contentHash = sha1_file($path);
		} else {
			throw new \Exception("Le fichier source '$fileName' est soit introuvable, soit interdit à la lecture.");
		}

		$this->csvReader = new ReaderCsv($path, "\t");
		$this->csvReader->setLogger($this->logger);
		$firstRow = $this->csvReader->readSkippingEmpty();

		$header = new Header();
		$header->setLogger($this->logger);
		$header->read($firstRow);

		$this->rowReader = new RowReader($header, $this->getLogger());
		$prefixes = array_filter(explode('/', (string) \Config::read('import.prefixes')));
		if ($prefixes) {
			$this->rowReader->setTitlePrefixes($prefixes);
		}
	}

	public function setFileName(string $name): void
	{
		$this->logger->setFileContext($name);
	}

	/**
	 * Read all the rows of the KBART file, without any external work on the content.
	 */
	public function readAll(Filter $filter): FileData
	{
		$data = new FileData($this->path, $this->contentHash);
		while ($row = $this->csvReader->readSkippingEmpty()) {
			$data->numRows++;
			$rowData = $this->rowReader->parse($row, $this->csvReader->getCurrentRowNumber());
			if ($rowData) {
				if ($filter->accept($rowData)) {
					$data->addRow($rowData);
				}
			}
		}
		return $data;
	}
}
