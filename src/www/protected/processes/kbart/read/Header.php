<?php

namespace processes\kbart\read;

use Exception;
use processes\kbart\logger\LogEvent;
use processes\kbart\logger\LogFamily;
use processes\kbart\LoggerTrait;

/**
 * Header of a KBART file, used to read a row of data.
 *
 * Specs:
 * https://groups.niso.org/apps/group_public/download.php/16900/RP-9-2014_KBART.pdf
 * KBART Recommended Practice, NISO RP-9-2014
 * Section 6.6 KBART Data Fields
 */
class Header
{
	use LoggerTrait;

	/**
	 * @var array columnName => value
	 */
	private array $columnMap = [];

	/**
	 * @var array Raw data of the current row
	 */
	private array $rowData;

	/**
	 * @param string[] $cells
	 * @throws Exception
	 */
	public function read(array $cells): void
	{
		$status = self::parse($cells);
		if ($this->logger !== null) {
			if ($status->missing) {
				$event = new LogEvent(LogFamily::headerMissing(), "L'entête KBART est incomplet, il manque : " . join(", ", $status->missing));
				$this->logger->addGlobal($event);
				throw new Exception("L'entête du KBART n'est pas valide. Le processus ne peut pas continuer.");
			}
			if ($status->unknown) {
				$event = new LogEvent(LogFamily::headerUnknown(), "Des champs de l'entête KBART sont inconnus : " . join(", ", $status->unknown));
				$this->logger->addGlobal($event);
			}
		}
		$this->columnMap = $status->map;
	}

	public function setRowData(array $row): void
	{
		$this->rowData = $row;
	}

	public function unsetRowData(): void
	{
		$this->rowData = [];
	}

	public function getColumn(string $name): string
	{
		if (isset($this->columnMap[$name])) {
			$index = $this->columnMap[$name];
			return isset($this->rowData[$index]) ? trim($this->rowData[$index]) : "";
		}
		if ($this->logger !== null) {
			$event = new LogEvent(LogFamily::headerNotFound(), "Le champ '$name' de l'entête KBART n'est pas présent dans le fichier.");
			$this->logger->addGlobal($event);
		}
		throw new Exception("Une colonne manque dans le KBART: $name.");
	}

	public function getOptionalColumn(string $name): ?string
	{
		if (isset($this->columnMap[$name])) {
			$index = $this->columnMap[$name];
			if (isset($this->rowData[$index])) {
				return trim($this->rowData[$index]);
			}
		}
		return null;
	}

	/**
	 * @return string[]
	 */
	public function listColumns(): array
	{
		return array_keys($this->columnMap);
	}

	/**
	 * @param string[] $cells
	 * @return object{missing: array, unknown: array, map: array<string, int>}
	 */
	private static function parse(array $cells): object
	{
		$possible = [
			"publication_title",
			"print_identifier",
			"online_identifier",
			"date_first_issue_online",
			"num_first_vol_online",
			"num_first_issue_online",
			"date_last_issue_online",
			"num_last_vol_online",
			"num_last_issue_online",
			"title_url",
			"first_author",
			"title_id",
			"embargo_info",
			"coverage_depth",
			"notes",
			"publisher_name",
			"publication_type",
			"date_monograph_published_print",
			"date_monograph_published_online",
			"monograph_volume",
			"monograph_edition",
			"first_editor",
			"parent_publication_title_id",
			"preceding_publication_title_id",
			"access_type",
		];
		$required = [
			'publication_title' => false,
			'print_identifier' => false,
			'online_identifier' => false,
			'title_url' => false,
			'embargo_info' => false,
		];
		$result = (object) [
			'missing' => [],
			'unknown' => [],
			'map' => [],
		];
		foreach ($cells as $index => $cell) {
			$name = strtolower($cell);
			if (isset($required[$name])) {
				$required[$name] = true;
			}
			if (!in_array($name, $possible) && $cell !== '') {
				$result->unknown[] = $cell;
			}
			$result->map[$name] = $index;
		}
		foreach ($required as $k => $v) {
			if (!$v) {
				$result->missing[] = $k;
			}
		}
		return $result;
	}
}
