Processus KBART
===============

1. Lecture de KBART dans Mir@bel
--------------------------------

### API publique

1. `$logger = new ImportLog()`, cf section suivante.
2. `$kbart = new File($path, $logger)` ouvre le fichier KBART `$path`.
3. `$kbart->readAll()` renvoie une structure `FileData` contenant les données lues, sans traitement extérieur.

Une alternative à (3) est d'utiliser `$kbart->read()` pour lire ligne par ligne, au format `RowData`.

### Logs

`ImportLog` contient tous les logs :

- au niveau global, par exemple, l'absence d'une colonne dans le fichier KBART ;
- au niveau local, par exemple, un numéro ISSN erronné.

### Specs de KBART

[KBART Recommended Practice, NISO RP-9-2014](https://groups.niso.org/apps/group_public/download.php/16900/RP-9-2014_KBART.pdf)

En particulier: *section 6.6 KBART Data Fields*


Fonctionnement ancien de l'import KBART
---------------------------------------

Le processus traite chaque ligne indépendamment des autres.

### Processus commun

1. Extraction des données du titre
2. Identification du titre
3. Sous condition, ignore les titres inconnus de M. Arrêt.

### 3.a Si titre inconnu

Prépare une intervention avec :
- le titre
- les ISSN
- les éditeurs

### 3.b Si titre connu

- Mise à jour de l'ID interne du titre dans cette ressource (table `Identification`)
- Prépare une intervention avec :
    - Si nouveaux ISSNs, mise à jour de la table Issn avec ces ISSN complétés par le sudoc
    - Éventuellement, modification de la table Titre si les dates d'ISSN sont pertinentes
    - Si création directe, mise à jour des éditeurs (éventuelle création de Editeur + lien Titre-Editeur)
    - Mise à jour des accès, et éventuellement de leurs liens aux collections

Problèmes avec l'ancien fonctionnement
--------------------------------------

- Comportement inadapté si plusieurs lignes concernent un même titre
- Le code est très complexe et monolithique (classe `Import` de plus de 1000 lignes de PHP)
- Pas de phase d'analyse préalable de l'ensemble des lignes
    - Si une erreur survient sur une ligne, le KBART sera partiellement appliqué
    - Pas de détection des doublons, donc risque de double intervention
