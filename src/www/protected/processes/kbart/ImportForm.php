<?php

namespace processes\kbart;

use processes\kbart\noninteractive\Config;

class ImportForm extends \CModel
{
	public $absents = false;

	public $lacunaire = false;

	public $selection = false;

	public $diffusion = '';

	public $contenu = '';

	public $suppressions = '';

	public $overrideDatesWithEmbargo = false;

	/**
	 * @var bool If true, the "diffusion" setting is meaningful. If false, it is useless.
	 */
	public bool $withAccessType = false;

	/**
	 * @var bool If true, the "contenu" setting is meaningful. If false, it is useless.
	 */
	public bool $withCoverageDepth = false;

	public function attributeNames()
	{
		return ['absents', 'contenu', 'diffusion', 'lacunaire', 'overrideDatesWithEmbargo', 'selection', 'suppressions'];
	}

	public function rules()
	{
		return [
			['absents, lacunaire, overrideDatesWithEmbargo, selection, suppressions', 'boolean'],
			['diffusion', 'in', 'range' => array_values(\Service::$enumAcces)],
			['contenu', 'in', 'range' => array_values(\Service::$enumType)],
		];
	}

	public function attributeLabels(): array
	{
		return [
			'absents' => "Supprimer les accès des titres absents",
			'lacunaire' => "Lacunaire",
			'selection' => "Sélection d'articles",
			'contenu' => "Contenu par défaut",
			'diffusion' => "Accès par défaut",
			'overrideDatesWithEmbargo' => "Remplacer les dates déclarées par les dates calculées par l'embargo",
			'suppressions' => "Supprimer les accès en ligne retirés",
		];
	}

	public function exportConfig(): Config
	{
		$c = new Config();
		$c->absents = (bool) $this->absents;
		$c->lacunaire = (bool) $this->lacunaire;
		$c->overrideDatesWithEmbargo = (bool) $this->overrideDatesWithEmbargo;
		$c->selection = (bool) $this->selection;
		$c->suppressions = (bool) $this->suppressions;
		$c->setDefaultAccess($this->diffusion);
		$c->setDefaultCoveragedepth($this->contenu);
		return $c;
	}
}
