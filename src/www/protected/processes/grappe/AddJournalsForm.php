<?php

namespace processes\grappe;

use processes\grappe\TitreSearchUrl;
use SearchTitre;
use Yii;

class AddJournalsForm extends \CFormModel
{
	public $id = "";

	public $save = "";

	public $spreadsheet;

	public $titreId = [];

	public $url = "";

	private int $ignoredRows = 0;

	public function rules()
	{
		return [
			['id', 'numerical', 'integerOnly' => true],
			['save', 'boolean'],
			['spreadsheet', 'file', 'allowEmpty' => true],
			['titreId', 'ext.validators.ArrayOfIntValidator'],
			['url', 'length', 'max' => 512],
		];
	}

	public function load(\CHttpRequest $request): bool
	{
		$name = \CHtml::modelName($this);
		$data = $request->getPost($name);
		if (!$data) {
			return false;
		}
		$this->setAttributes($data);
		return $this->validate();
	}

	/**
	 * @return int[]
	 */
	public function extractIds(): array
	{
		return array_merge($this->titreId, $this->extractNewIds());
	}

	public function getNumIgnoredRows(): int
	{
		return $this->ignoredRows;
	}

	/**
	 * @return int[]
	 */
	private function extractNewIds(): array
	{
		// Expand the list by a single ID
		$tId = (int) $this->id;
		if ($tId > 0) {
			$this->id = "";
			return [$tId];
		}

		// Expand the list by a search URL
		if ($this->url) {
			$criteria = (new TitreSearchUrl($this->url))->extractSearchCriteria();
			if ($criteria) {
				$this->url = "";
				$search = new SearchTitre((int) Yii::app()->user->getState('partenaireId'));
				$search->setAttributes($criteria);
				$search->validate();
				return $search->searchTitreIds();
			}
		}

		// Expand the list by a spreadsheet of ISSN
		$file = \CUploadedFile::getInstance($this, 'spreadsheet');
		if ($file !== null && !$file->hasError) {
			$mimeType = \CFileHelper::getMimeTypeByExtension($file->name) ?: \CFileHelper::getMimeType($file->tempName);
			$loader = new \processes\analyse\Compare();
			$loader->loadSpreadsheet($file->tempName, $mimeType);
			$ids = \Yii::app()->db
				->createCommand(<<<EOSQL
					SELECT DISTINCT i.titreId FROM RevueCompare t JOIN Issn i ON t.issn = i.issn
					EOSQL
				)
				->queryColumn();
			$this->ignoredRows = \Yii::app()->db
				->createCommand(<<<EOSQL
					WITH MatchingRows AS (SELECT DISTINCT t.position FROM RevueCompare t JOIN Issn i ON t.issn = i.issn)
					SELECT count(DISTINCT t.position) FROM RevueCompare t LEFT JOIN MatchingRows m ON t.position = m.position WHERE m.position IS NULL
					EOSQL
				)
				->queryScalar();
			return array_map('intval', $ids);
		}

		return [];
	}
}
