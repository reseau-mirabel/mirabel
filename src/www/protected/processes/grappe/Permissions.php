<?php

namespace processes\grappe;

use Grappe;
use WebUser;

class Permissions
{
	private WebUser $user;

	public function __construct(WebUser $user)
	{
		$this->user = $user;
	}

	public function canAdmin(): bool
	{
		if (\Yii::app() instanceof \CConsoleApplication) {
			return true;
		}
		return $this->user->checkAccess('admin');
	}

	public function canCreate(): bool
	{
		if ($this->canAdmin()) {
			return true;
		}
		$permName = \Yii::app()->params->itemAt('grappe-permission') ?? 'avec-partenaire';
		return $this->user->checkAccess($permName);
	}

	public function canRead(Grappe $grappe): bool
	{
		if ($this->canAdmin()) {
			return true;
		}
		if ($grappe->diffusion === Grappe::DIFFUSION_PARTAGE || $grappe->diffusion === Grappe::DIFFUSION_PUBLIC) {
			return true;
		}
		if ($this->user->checkAccess('avec-partenaire')) {
			if ($grappe->diffusion === Grappe::DIFFUSION_AUTHENTIFIE) {
				return true;
			}
			if ((int) $this->user->partenaireId === $grappe->partenaireId) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Can update a given Grappe, or some Grappe if null is given.
	 */
	public function canWrite(?Grappe $grappe): bool
	{
		if ($this->canAdmin()) {
			return true;
		}
		if ($grappe) {
			return $this->user->checkAccess('avec-partenaire')
				&& (int) $this->user->partenaireId === $grappe->partenaireId;
		}
		// No Grappe specified, so search if there is one that the user can update.
		return Grappe::model()->exists("partenaireId = " . (int) $this->user->partenaireId);
	}
}
