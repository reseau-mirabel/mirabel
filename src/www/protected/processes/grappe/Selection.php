<?php

namespace processes\grappe;

use Grappe;
use Partenaire;
use Yii;

class Selection
{
	/**
	 * @var int For comparing with Grappe.diffusion
	 */
	private int $accessLevel;

	private int $partenaireId;

	public function __construct(Partenaire $owner, ?\WebUser $user)
	{
		$this->partenaireId = (int) $owner->id;
		$this->accessLevel = $user ? self::getUserAccessLevel($user, (int) $owner->id) : 99;
	}

	/**
	 * Find the Grappe records that the current user can see in the current list.
	 *
	 * @return Grappe[]
	 */
	public function getVisibleGrappes(int $page): array
	{
		return \Grappe::model()->findAllBySql(<<<SQL
			SELECT g.*
			FROM Partenaire_Grappe pg
				JOIN Grappe g ON pg.grappeId = g.id
			WHERE pg.partenaireId = {$this->partenaireId} AND (pg.pages & $page) AND g.diffusion <= {$this->accessLevel}
			ORDER BY pg.position, g.nom
			SQL
		);
	}

	public function hasGrappesInList(int $page): bool
	{
		return (bool) Yii::app()->db
			->createCommand(<<<SQL
				SELECT 1
				FROM Partenaire_Grappe pg
					JOIN Grappe g ON pg.grappeId = g.id
				WHERE pg.partenaireId = {$this->partenaireId} AND (pg.pages & $page) AND g.diffusion <= {$this->accessLevel}
				LIMIT 1
				SQL
			)
			->queryScalar();
	}

	/**
	 * Find the Grappe records in the global list.
	 *
	 * @return Grappe[]
	 */
	public static function findGrappesPubliques(): array
	{
		return Grappe::model()->findAllBySql(
			<<<SQL
				SELECT g.*
				FROM Grappe g
					LEFT JOIN Partenaire_Grappe pg ON g.id = pg.grappeId
				WHERE g.diffusion = :partage AND pg.partenaireId IS NULL
				ORDER BY pg.position, g.nom ASC
				SQL,
			[':partage' => Grappe::DIFFUSION_PARTAGE]
		);
	}

	/**
	 * Return a struct detailling the references to this Grappe.
	 *
	 * @return array{ids: list<int|null>, isInGlobalList: bool, inOtherLists: int, isInOwnLists: bool, otherLists: array}
	 */
	public static function getListsReferences(Grappe $grappe): array
	{
		$result = [
			'ids' => [],
			'inOtherLists' => 0,
			'isInGlobalList' => false,
			'isInOwnLists' => false,
			'otherLists' => [],
		];
		if ($grappe->id > 0) {
			$usage = Yii::app()->db
				->createCommand(<<<SQL
					SELECT pg.partenaireId, p.nom AS partenaire, pg.pages
					FROM Partenaire_Grappe pg
						LEFT JOIN Partenaire p ON pg.partenaireId = p.id
					WHERE pg.grappeId = {$grappe->id} AND pg.pages > 0
					SQL
				)
				->queryAll();

			$partenaireId = (int) $grappe->partenaireId;
			foreach ($usage as $u) {
				$id = (int) $u['partenaireId'];
				if ($id === 0) {
					$result['isInGlobalList'] = true;
					$result['ids'][] = null;
				} elseif ($id === $partenaireId) {
					$result['isInOwnLists'] = true;
					$result['ids'][] = $id;
				} else {
					$result['inOtherLists']++;
					$result['ids'][] = $id;
					$inPagePartenaire = ((int) $u['pages']) & \processes\partenaire\GrappeAffichage::PAGE_PARTENAIRE;
					$inPageSelection = ((int) $u['pages']) & \processes\partenaire\GrappeAffichage::PAGE_SELECTION;
					$result['otherLists'][] = [
						(int) $u['partenaireId'],
						$u['partenaire'],
						(bool) $inPagePartenaire,
						(bool) $inPageSelection,
					];
				}
			}
		}
		return $result;
	}

	private static function getUserAccessLevel(\WebUser $user, int $partenaireId): int
	{
		if ($user->isGuest) {
			return \Grappe::DIFFUSION_PUBLIC;
		}
		if ((int) $user->id === $partenaireId) {
			return \Grappe::DIFFUSION_PARTENAIRE;
		}
		// Not guest, not same Partenaire as the Selection
		return $accessLevel = \Grappe::DIFFUSION_AUTHENTIFIE;
	}
}
