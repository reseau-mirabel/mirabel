<?php

namespace processes\grappe;

use SearchTitre;
use Yii;

class TitreSearchUrl
{
	private array $parsed;

	private array $query = [];

	private array $facets = [];

	public function __construct(string $url)
	{
		$this->parse($url);
	}

	/**
	 * Return an array of \SearchTitre attribute extracted from a search URL.
	 *
	 * e.g. https://mirabel.localhost/revue/search?q[editeurId]=4&titre=  →  [editeurId => 4]
	 */
	public function extractSearchCriteria(): array
	{
		if (!$this->parsed || !$this->query) {
			return [];
		}

		// Load then export the search arguments.
		$s = new SearchTitre(Yii::app()->user->partenaireId);
		$s->setAttributes($this->query);
		return $s->exportAttributes()->toArray();
	}

	public function extractFacetsCriteria(): array
	{
		if (!$this->parsed || !$this->facets) {
			return [];
		}
		return $this->facets;
	}

	private function parse(string $url): void
	{
		$this->parsed = \parse_url($url);
		if ($this->parsed['path'] !== '/revue/search') {
			$this->parsed = [];
			return;
		}

		// Extract the search arguments in the URL.
		$arguments = [];
		parse_str($this->parsed['query'], $arguments);

		// Arguments for the base query
		$filtered = [];
		if (isset($arguments['SearchTitre'])) {
			$filtered = SearchTitre::removeEmptyCriteria((array) $arguments['SearchTitre']);
		}
		if (isset($arguments['q'])) {
			$filtered = $arguments['q'];
		}
		$this->query = $filtered;

		// Arguments for the facets
		$this->facets = $arguments['f'] ?? [];
	}
}
