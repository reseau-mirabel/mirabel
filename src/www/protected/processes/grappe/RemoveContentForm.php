<?php

namespace processes\grappe;

class RemoveContentForm extends \CFormModel
{
	public $searches = [];

	public $titreIds = [];

	public function rules()
	{
		return [
			['searches', 'safe'],
			['titreIds', 'ext.validators.ArrayOfIntValidator'],
		];
	}

	public function afterValidate()
	{
		if (!$this->searches || !is_array($this->searches)) {
			$this->searches = [];
		} else {
			$this->searches = array_filter($this->searches);
		}
		return parent::afterValidate();
	}

	public function load(\CHttpRequest $request): bool
	{
		$name = \CHtml::modelName($this);
		$data = $request->getPost($name);
		if (!$data) {
			return false;
		}
		$this->setAttributes($data);
		return $this->validate();
	}
}
