<?php

namespace processes\grappe;

class Stats
{
	/**
	 * @return array{manuelle: int, recherche: int, total: int}
	 */
	public static function countByRevue(int $grappeId): array
	{
		$db = \Yii::app()->db;
		$sql = "SELECT count(DISTINCT t.revueId) FROM Grappe_Titre gt JOIN Titre t ON t.id = gt.titreId WHERE gt.grappeId = {$grappeId}";
		return [
			'manuelle' => $db->createCommand("$sql AND source = -1")->queryScalar(),
			'recherche' => $db->createCommand("$sql AND source >= 0")->queryScalar(),
			'total' => $db->createCommand($sql)->queryScalar(),
		];
	}

	/**
	 * @return array{manuelle: int, recherche: int, total: int}
	 */
	public static function countByTitre(int $grappeId): array
	{
		$db = \Yii::app()->db;
		$sql = "SELECT count(DISTINCT titreId) FROM Grappe_Titre WHERE grappeId = {$grappeId}";
		return [
			'manuelle' => $db->createCommand("$sql AND source = -1")->queryScalar(),
			'recherche' => $db->createCommand("$sql AND source >= 0")->queryScalar(),
			'total' => $db->createCommand($sql)->queryScalar(),
		];
	}
}
