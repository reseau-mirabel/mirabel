<?php

namespace processes\grappe;

use Grappe;

class GrappeForm extends \CFormModel
{
	public string $nom = "";

	public string $description = "";

	public string $note = "";

	public $diffusion;

	public $niveau = Grappe::NIVEAU_REVUE;

	public $partenaireId = "-1";

	private ?Grappe $grappe = null;

	public function init()
	{
		if (!\Yii::app()->user->checkAccess('admin')) {
			$this->diffusion = Grappe::DIFFUSION_PARTENAIRE;
		}
	}

	public function rules()
	{
		$rules = [
			[['nom', 'diffusion'], 'required'],
			['nom', 'length', 'max' => 255],
			['description, note', 'length', 'max' => 65535],
			['diffusion', 'numerical', 'integerOnly' => true, 'min' => 1, 'max' => max(...array_values(Grappe::ENUM_DIFFUSION))],
			['niveau', 'in', 'range' => array_keys(Grappe::NIVEAU)],
		];
		if (\Yii::app()->user->checkAccess('admin')) {
			array_push($rules,
				['partenaireId', 'numerical', 'integerOnly' => true]
			);
		}
		return $rules;
	}

	public function attributeLabels()
	{
		return [
			'nom' => 'Nom',
			'description' => 'Description',
			'note' => 'Note interne',
			'diffusion' => "Diffusion",
			'partenaireId' => 'Partenaire propriétaire',
		];
	}

	public function afterValidate()
	{
		parent::afterValidate();
		$partenaireId = (int) $this->partenaireId;
		if ($partenaireId > 0) {
			if (\Partenaire::model()->findByPk($partenaireId) === null) {
				$this->addError('partenaireId', "Aucun partenaire ne correspond.");
			} elseif (!\Yii::app()->user->checkAccess('admin') && $partenaireId !== (int) \Yii::app()->user->partenaireId) {
				$this->addError('partenaireId', "Seul un admin peut affecter une grappe à un autre partenaire.");
			}
		} elseif (!\Yii::app()->user->checkAccess('admin') || $partenaireId < 0) {
			$this->addError('partenaireId', "Choix requis.");
		}
	}

	public function getGrappe(): Grappe
	{
		if ($this->grappe === null) {
			$g = new Grappe();
			$g->partenaireId = (int) $this->partenaireId;
			return $g;
		}
		return $this->grappe;
	}

	public function getGrappeId(): ?int
	{
		return $this->grappe->id;
	}

	public function setGrappe(Grappe $g): void
	{
		$this->grappe = $g;
		$this->nom = $g->nom;
		$this->description = $g->description;
		$this->note = $g->note;
		$this->diffusion = $g->diffusion;
		$this->niveau = $g->niveau;
		$this->partenaireId = $g->partenaireId ? (string) $g->partenaireId : "0";
	}

	public function save(): Grappe
	{
		$grappe = $this->grappe;
		$isNewRecord = false;
		if ($grappe === null) {
			$grappe = new Grappe();
			$isNewRecord = true;
		}

		if ($grappe->id && $grappe->partenaireId && (int) $grappe->partenaireId !== (int) $this->partenaireId) {
			// Changement du partenaire de rattachement => on retire la grappe des listes de l'ancien propriétaire.
			\Yii::app()->db
				->createCommand("DELETE FROM Partenaire_Grappe WHERE partenaireId = :pid AND grappeID = :gid")
				->execute([':pid' => $grappe->partenaireId, ':gid' => $grappe->id]);
		}

		$grappe->nom = trim($this->nom);
		$grappe->description = trim($this->description);
		$grappe->note = trim($this->note);
		$grappe->diffusion = (int) $this->diffusion;
		$grappe->niveau = (int) $this->niveau;
		$grappe->partenaireId = $this->partenaireId > 0 ? (int) $this->partenaireId : null;
		$grappe->save(false);

		if ($grappe->id && $grappe->partenaireId) {
			// Ajoute la nouvelle grappe à la sélection du partenaire, mais non affichée.
			$now = time();
			\Yii::app()->db
				->createCommand(<<<SQL
					INSERT IGNORE INTO Partenaire_Grappe (partenaireId, grappeId, pages, hdateCreation)
					VALUES ({$grappe->partenaireId}, {$grappe->id}, 0, $now)
					SQL
				)
				->execute();
		}
		if (!$isNewRecord) {
			if ($grappe->diffusion !== \Grappe::DIFFUSION_PARTAGE) {
				// Delete from external lists (global and other Partenaire).
				$locked = false;
				if (!\Yii::app()->user->checkAccess('admin')) {
					$locked = (bool) \Yii::app()->db
						->createCommand("SELECT 1 FROM Partenaire_Grappe WHERE pages > 0 AND grappeId = {$grappe->id} AND NOT (partenaireId <=> :p)")
						->queryScalar([':p' => $grappe->partenaireId]);
				}
				if ($locked) {
					$grappe->diffusion = \Grappe::DIFFUSION_PARTAGE;
					$grappe->save(false);
				} else {
					\Yii::app()->db->createCommand()
						->delete(
							"Partenaire_Grappe",
							"grappeId = {$grappe->id} AND (partenaireId IS NULL OR partenaireId <> :p)",
							[':p' => (int) $grappe->partenaireId]
						);
				}
			}
		}

		return $grappe;
	}
}
