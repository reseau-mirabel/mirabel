<?php

namespace processes\grappe;

use components\db\InsertBuffer;
use Grappe;
use processes\grappe\TitreSearchUrl;
use Yii;

class ContentManager
{
	private Grappe $grappe;

	public function __construct(Grappe $grappe)
	{
		$this->grappe = $grappe;
	}

	/**
	 * @param array $ids List of Titre.id
	 * @return int Number of effective inserts
	 */
	public function addJournals(array $ids): int
	{
		if (!$ids) {
			return 0;
		}
		$buffer = new InsertBuffer("INSERT IGNORE INTO Grappe_Titre (grappeId, source, titreId) VALUES ");
		foreach ($ids as $id) {
			$buffer->add([$this->grappe->id, Grappe::SOURCE_NOTSEARCH, (int) $id]);
		}
		$buffer->close();

		$this->grappe->hdateModif = $_SERVER['REQUEST_TIME'];
		$this->grappe->update(['hdateModif']);
		self::markChangedJournals($ids);
		return $buffer->countInserted();
	}

	/**
	 * @return array{status: "error"|"success", message: string}
	 */
	public function addSearch(string $url): array
	{
		$parsedUrl = new TitreSearchUrl($url);
		if ($parsedUrl->extractFacetsCriteria()) {
			return [
				'status' => 'error',
				'message' => "L'URL ne doit pas contenir de filtres de recherche (facettes). Utilisez <strong>uniquement le formulaire</strong> de la recherche avancée.",
			];
		}

		$criteria = $parsedUrl->extractSearchCriteria();
		if (!$criteria) {
			return [
				'status' => 'error',
				'message' => "L'URL soumise ne contenait pas de critère de recherche (de type /revue/search).",
			];
		}

		$searches = json_decode($this->grappe->recherche, true);
		if (in_array($criteria, $searches)) {
			return [
				'status' => 'error',
				'message' => "Cette recherche est déjà présente dans la grappe.",
			];
		}
		$searches[] = $criteria;
		$this->grappe->recherche = json_encode($searches, JSON_UNESCAPED_SLASHES);
		if (!$this->grappe->save(false)) {
			return [
				'status' => 'error',
				'message' => "Erreur lors de l'enregistrement en base de données.",
			];
		}
		$this->refreshSearches();
		return [
			'status' => 'success',
			'message' => "La recherche a été ajoutée à la grappe.",
		];
	}

	/**
	 * Update the table Grappe_Titre with the records induced by the searches of this Grappe.
	 *
	 * Adds a titreId for each Titre selected by the searches.
	 * Removes titreId for records no longer in search results.
	 */
	public function refreshSearches(): void
	{
		// Store all the actual Titre.id in a temporary table, so that we can detect what to add and remove.
		$db = Yii::app()->db;
		$db->createCommand("CREATE TEMPORARY TABLE Ids (id INT NOT NULL, rank TINYINT NOT NULL) ENGINE=MEMORY")->execute();

		$transaction = $db->beginTransaction();
		foreach ($this->grappe->getSearchCriteria() as $rank => $search) {
			// Find the Titre IDs that match the search
			$ids = $search->searchTitreIds();
			$cmd = $db->createCommand("INSERT IGNORE INTO Ids VALUES (:tid, {$rank})");
			foreach ($ids as $id) {
				$cmd->execute([':tid' => $id]);
			}
		}
		$transaction->commit();

		// Index the added|removed titles
		Yii::app()->db
			->createCommand(<<<SQL
				REPLACE INTO IndexingRequiredTitre (id, updated, deleted)
					WITH AllIds AS (
						SELECT id FROM Ids
						UNION ALL
						SELECT titreId FROM Grappe_Titre WHERE grappeId = {$this->grappe->id}
					),
					ChangedIds AS (
						SELECT id FROM AllIds GROUP BY id HAVING count(*) < 2
					)
					SELECT id, unix_timestamp(), 0 FROM ChangedIds
				SQL
			)
			->execute();

		// Add IDs, but will not change the source field which may be wrong (obsolete or incomplete).
		$db->createCommand("INSERT IGNORE INTO Grappe_Titre (grappeId, titreId, source) SELECT {$this->grappe->id}, id, rank FROM Ids")->execute();
		// Remove the obsolete IDs, but do not change the records that are not linked to search criteria.
		$db->createCommand("DELETE FROM Grappe_Titre WHERE grappeId = {$this->grappe->id} AND source > :notsearch AND (titreId, source) NOT IN (SELECT id, rank FROM Ids)")
			->execute([':notsearch' => Grappe::SOURCE_NOTSEARCH]);
		$db->createCommand("DROP TEMPORARY TABLE IF EXISTS Ids")->execute();
	}

	/**
	 * @param array $ids List of Titre.id
	 * @return int Number of removals
	 */
	public function removeJournals(array $ids): int
	{
		if (!$ids) {
			return 0;
		}
		$this->grappe->hdateModif = $_SERVER['REQUEST_TIME'];
		$this->grappe->update(['hdateModif']);
		self::markChangedJournals($ids);
		$idsStr = join(",", array_map('intval', $ids));
		return (int) Yii::app()->db
			->createCommand(
				"DELETE FROM Grappe_Titre WHERE grappeId = {$this->grappe->id} AND titreId IN ($idsStr)"
			)
			->execute();
	}

	/**
	 * @return bool True is content has changed.
	 */
	public function removeSearches(array $patterns): bool
	{
		if (!$patterns) {
			return false;
		}
		$hasChanged = false;
		$criteria = json_decode($this->grappe->recherche, true, 16, JSON_THROW_ON_ERROR);
		$remnants = [];
		foreach ($criteria as $c) {
			$json = json_encode($c, JSON_UNESCAPED_SLASHES);
			if (in_array($json, $patterns, true)) {
				$hasChanged = true;
			} else {
				$remnants[] = $c;
			}
		}
		if (!$hasChanged) {
			return false;
		}
		$this->grappe->recherche = json_encode($remnants, JSON_UNESCAPED_SLASHES);
		$this->grappe->hdateModif = $_SERVER['REQUEST_TIME'];
		$this->grappe->save(false);
		$this->refreshSearches();
		return true;
	}

	/**
	 * These Titre.id will become targets of the next Manticore indexing.
	 *
	 * @param array $ids List of Titre.id
	 */
	private static function markChangedJournals(array $ids): void
	{
		$buffer = new InsertBuffer("INSERT IGNORE INTO IndexingRequiredTitre (id, updated, deleted) VALUES");
		foreach ($ids as $id) {
			$buffer->add([(int) $id, 1, 0]);
		}
		$buffer->close();
	}
}
