<?php

namespace processes\grappe;

use Yii;

class FilteredList extends \CModel
{
	/**
	 * @var string Filter the list by Grappe.nom
	 */
	public string $nom = '';

	public int $pid = 0;

	private int $partenaireId = 0;

	private bool $excludePartenaire = false;

	public function attributeNames()
	{
		return ['nom', 'pid'];
	}

	public function load(array $input): void
	{
		if (isset($input['g-nom'])) {
			$this->nom = (string) $input['g-nom'];
		}
		if (isset($input['g-p'])) {
			$this->pid = (int) $input['g-p'];
		}
	}

	/**
	 * Build an array [Partenaire.id => Partenaire.nom] for those that have a Grappe.
	 */
	public function listPartenaires(): array
	{
		$p = Yii::app()->db
			->createCommand(<<<SQL
				SELECT g.partenaireId, IF(p.sigle, p.sigle, p.nom) AS pnom
				FROM Grappe g
					JOIN Partenaire p ON p.id = g.partenaireId
				GROUP BY g.partenaireId
				ORDER BY pnom
				SQL
			)
			->setFetchMode(\PDO::FETCH_KEY_PAIR)
			->queryAll();
		if ($this->excludePartenaire) {
			unset($p["{$this->partenaireId}"]);
		}
		return ['0' => '', '-1' => "Aucun"] + $p;
	}

	public function hasRequirePartenaire(): bool
	{
		return ($this->partenaireId > 0) && !$this->excludePartenaire;
	}

	public function requirePartenaire(int $pId): self
	{
		$this->partenaireId = $pId;
		$this->excludePartenaire = false;
		return $this;
	}

	public function excludePartenaire(int $pId): self
	{
		$this->partenaireId = $pId;
		$this->excludePartenaire = true;
		return $this;
	}

	public function getDataProvider(): \CSqlDataProvider
	{
		return new \CSqlDataProvider(
			$this->createCommand(),
			[
				'sort' => self::createSort(),
				'pagination' => false,
			]
		);
	}

	private function createCommand(): \CDbCommand
	{
		$cmd = Yii::app()->db->createCommand();
		$cmd->from("Grappe g")
			->leftJoin("Partenaire p", "g.partenaireId = p.id")
			->leftJoin(<<<EOSQL
				(SELECT
					gt.grappeId,
					count(DISTINCT t.id) AS titres,
					count(DISTINCT t.revueId) AS revues
				  FROM Grappe_Titre gt
				    JOIN Titre t ON gt.titreId = t.id
				  GROUP BY gt.grappeId
				) Nums
				EOSQL,
				"g.id = Nums.grappeId"
			)
			->leftJoin(<<<SQL
				(SELECT g.id AS grappeId, count(*) AS inlists
					FROM Grappe g
						JOIN Partenaire_Grappe pg ON pg.grappeId = g.id
					WHERE pg.pages > 0
						AND (g.partenaireId IS NULL OR pg.partenaireId IS NULL OR pg.partenaireId <> g.partenaireId)
					GROUP BY g.id
				) Refs
				SQL,
				"g.id = Refs.grappeId"
			)
			->select("g.*, p.id AS partenaireId, IF(p.sigle <> '', p.sigle, p.nom) AS partenaire, Nums.titres, Nums.revues, Refs.inlists");

		if ($this->partenaireId) {
			if ($this->excludePartenaire) {
				$cmd->andWhere("NOT (g.partenaireId <=> :exclude)", [':exclude' => $this->partenaireId]);
			} else {
				$cmd->andWhere("g.partenaireId = :require", [':require' => $this->partenaireId]);
			}
		}

		if ($this->nom) {
			$like = str_replace(['%', '_'], ['', ''], $this->nom);
			$cmd->andWhere("g.nom LIKE :nom", [':nom' => "%{$like}%"]);
		}
		if ($this->pid > 0) {
			$cmd->andWhere("g.partenaireId = :pid", [':pid' => $this->pid]);
		} elseif ($this->pid < 0) {
			$cmd->andWhere("g.partenaireId IS NULL");
		}

		return $cmd;
	}

	private static function createSort(): \CSort
	{
		$sort = new \CSort();
		$sort->attributes = [
			'id' => [
				'asc' => 'g.id ASC',
				'desc' => 'g.id DESC',
			],
			'nom' => [
				'asc' => 'g.nom ASC',
				'desc' => 'g.nom DESC',
			],
			'partenaire' => [
				'asc' => 'p.nom ASC, g.nom ASC',
				'desc' => 'p.nom DESC, g.nom ASC',
			],
			'date' => [
				'asc' => 'g.hdateModif ASC',
				'desc' => 'g.hdateModif DESC',
				'default' => 'desc',
			],
			'inlists' => [
				'asc' => '(g.diffusion=1) ASC, Refs.inlists ASC, g.nom ASC',
				'desc' => '(g.diffusion=1) DESC, Refs.inlists DESC, g.nom ASC',
			],
		];
		$sort->defaultOrder = ['nom' => \CSort::SORT_ASC];
		return $sort;
	}
}
