<?php

namespace processes\intervention;

use models\import\Normalize;
use Categorie;
use CLogger;
use TitreEditeur;
use TitreLiens;

class Formatter
{
	public bool $showRestrictedInfo = false;

	public function format(\Intervention $i): string
	{
		$action = $i->action ?: '';
		try {
			$html = $this->formatContents($i->contenuJson->toArray(), $action);
		} catch (\Throwable $e) {
			\Yii::log("Intervention {$i->id} impossible à formatter : " . $e->getMessage(), "warning", 'rss');
			$html = "";
		}
		return $html;
	}

	public function formatContents(array $content, string $action): string
	{
		$details = [];
		foreach ($content as $item) {
			try {
				$html = $this->formatContentItem($item, $action);
				array_push($details, ...$html);
			} catch (\Throwable $e) {
				\Yii::log("Intervention impossible à formatter, contenant : " . $e->getMessage(), "warning", 'rss');
			}
		}
		if ($details) {
			return self::arrayToHtmlUl(array_unique($details), 0);
		}
		return "";
	}

	public static function arrayToHtmlUl(array $a, int $level): string
	{
		$prefix = str_repeat('  ', $level);
		if (!$a) {
			return "{$prefix}…";
		}
		return "<ul>\n{$prefix}<li>" . join("</li>\n{$prefix}<li>", $a) . "</li>\n{$prefix}</ul>";
	}

	/**
	 * @return string[] HTML
	 */
	public static function compareLinkListsJson(mixed $a, mixed $b): array
	{
		if (!$a && !$b) {
			return [];
		}
		$aa = self::validateLinkList($a);
		$bb = self::validateLinkList($b);
		return self::compareLinkLists($aa, $bb);
	}

	/**
	 * @param array $item Item from the [content] array in Intervention.contenuJson
	 * @param string $action
	 * @return string[] list of HTML texts
	 */
	private function formatContentItem(array $item, string $action = ''): array
	{
		if ($action === 'revue-I') {
			if ($item['operation'] == 'create') {
				$categorie = Categorie::model()->findByPk($item['after']['categorieId']);
				if ($categorie && ($categorie->role === 'public' || $this->showRestrictedInfo)) {
					return [sprintf(
						"Ajout du thème <em>%s</em>%s.",
						htmlspecialchars($categorie->categorie, ENT_COMPAT | ENT_SUBSTITUTE | ENT_HTML5),
						($categorie->role === 'public' ? "" : " (non public)")
					)];
				}
				return ["Ajout d'un thème (supprimé ensuite)"];
			}
			if ($item['operation'] == 'delete') {
				$categorie = Categorie::model()->findByPk($item['before']['categorieId']);
				if ($categorie && ($categorie->role === 'public' || $this->showRestrictedInfo)) {
					return [sprintf(
						"Retrait du thème <em>%s</em>%s.",
						htmlspecialchars($categorie->categorie, ENT_COMPAT | ENT_SUBSTITUTE | ENT_HTML5),
						($categorie->role === 'public' ? "" : " (non public)")
					)];
				}
				return ["Retrait d'un thème"];
			}
			return [];
		}
		if ($item['operation'] === 'update') {
			$details = self::buildChangesList($item['model'], $item['before'], $item['after']);
			if ($item['model'] === 'Issn' && empty($details)) {
				return ["Données ISSN complétées"];
			}
			return $details;
		}
		if ($item['model'] === 'Issn' && $action !== 'revue-C') {
			if ($item['operation'] == 'delete') {
				$keys = array_fill_keys(array_keys($item['before']), "");
				$changes = self::buildChangesList($item['model'], $item['before'], $keys);
				return ["Suppression d'un ISSN" . self::arrayToHtmlUl($changes, 1)];
			}
			if ($item['operation'] == 'create') {
				$keys = array_fill_keys(array_keys($item['after']), "");
				$changes = self::buildChangesList($item['model'], $keys, $item['after']);
				return ["Ajout d'un ISSN" . self::arrayToHtmlUl($changes, 1)];
			}
			return self::buildChangesList($item['model'], $item['before'] ?? [], $item['after']);
		}
		if ($item['model'] === 'ServiceCollection') {
			return ["Attribution aux collections"];
		}
		if (!empty($item['msg'])) {
			return [htmlspecialchars($item['msg'], ENT_COMPAT | ENT_SUBSTITUTE | ENT_HTML5)];
		}
		return [];
	}

	/**
	 * @return string[] list of HTML texts
	 */
	private static function buildChangesList(string $model, array $beforeList, array $afterList): array
	{
		$details = [];
		$seen = [];
		foreach ($beforeList as $k => $v) {
			if (isset($seen[$k])) {
				continue;
			}
			$seen[$k] = true;
			$name = self::attrToText($model, $k);
			if (!$name) {
				continue;
			}
			if ($k === 'liensJson') {
				$items = self::compareLinkListsJson($v, $afterList['liensJson']);
				if (!$items) {
					continue; // skip
				}
				$details[$name] = sprintf("<b>%s</b> : %s", htmlspecialchars($name), self::arrayToHtmlUl($items, 2));
			} else {
				$before = self::formatValue($v, $k);
				$after = self::formatValue($afterList[$k] ?? "", $k);
				if (($before || $after) && ($before !== $after)) {
					$details[$name] = sprintf("<b>%s</b> : %s &#8594; %s.", htmlspecialchars($name), $before, $after);
				}
			}
		}
		// records in "after" that have no matching "before" record.
		foreach ($afterList as $k => $v) {
			if (isset($seen[$k]) || $v === "") {
				continue;
			}
			$seen[$k] = true;
			$name = self::attrToText($model, $k);
			if (!$name) {
				continue;
			}
			$before = self::formatValue("", $k);
			$after = self::formatValue($v, $k);
			$details[$name] = sprintf("<b>%s</b> : %s &#8594; %s.", htmlspecialchars($name), $before, $after);
		}
		ksort($details);
		return array_values($details);
	}

	private static function formatValue(mixed $v, string $attr): string
	{
		$bool = [
			'electronique' => 1, // compatibilité anciens ISSN
			'exportPossible' => 1,
			'indexation' => 1,
			'noteContenu' => 1,
			'autoImport' => 1,
			'lacunaire' => 1,
			'selection' => 1,
		];
		if (is_array($v)) {
			$v = json_encode($v);
		}
		if ($attr === 'liensJson') {
			return TitreLiens::htmlFormat($v);
		}
		if ($attr === 'role') {
			return $v ? (\Editeur::getPossibleRoles()[$v] ?? $v) : "non défini";
		}
		if ($attr === 'embargoInfo' && $v) {
			return Normalize::getReadableKbartEmbargo($v);
		}
		if (preg_match('/(?:^u|[a-z]U)rl$/', $attr)) {
			$url = htmlspecialchars($v);
			return sprintf('<a href="%s">%s</a>', $url, $url);
		}
		if (isset($bool[$attr])) {
			return ($v ? 'Oui' : 'Non');
		}
		return htmlspecialchars('"' . $v . '"');
	}

	/**
	 * Returns the full name of an attribute, if it has to be printed.
	 */
	private static function attrToText(string $objectName, string $attrName): string
	{
		static $map = [
			'Titre' => [
				'titre' => 'Titre',
				'prefixe' => 'Préfixe',
				'sigle' => 'Sigle',
				'dateDebut' => 'Date de début',
				'dateFin' => 'Date de fin',
				'sudoc' => 'Sudoc', // compatibilité anciens ISSN
				'worldcat' => 'WorldCat', // compatibilité anciens ISSN
				'issn' => 'ISSN', // compatibilité anciens ISSN
				'issnl' => 'ISSN-L', // compatibilité anciens ISSN
				'url' => 'Site web',
				'urlCouverture' => 'Vignette de couverture',
				'liensJson' => 'Autres liens',
				'periodicite' => 'Périodicité',
				'electronique' => 'Électronique', // compatibilité anciens ISSN
				'langues' => 'Langues',
			],
			'Issn' => [
				'bnfArk' => 'BnF ARK',
				'issn' => 'ISSN',
				'support' => "Support de l'ISSN",
				'issnl' => 'ISSN-L',
				'pays' => 'Pays',
				'sudocPpn' => 'Sudoc PPN',
				'worldcatOcn' => 'worldcat OCN',
			],
			'Ressource' => [
				'nom' => 'Nom',
				'prefixe' => 'Préfixe',
				'sigle' => 'Sigle',
				'description' => 'Description',
				'type' => 'Type',
				'acces' => 'Type d\'accès',
				'url' => 'Adresse web',
				'diffuseur' => 'Diffuseur',
				'partenaires' => 'Partenaires',
				'partenairesNb' => 'Nombre de partenaires',
				'disciplines' => 'Disciplines',
				'revuesNb' => 'Nombre de revues',
				'articlesNb' => 'Nombre d\'articles',
				'alerteRss' => 'Flux RSS',
				'alerteMail' => 'Alerte courriel',
				'exportPossible' => 'Export possible',
				'indexation' => 'Indexation',
				'noteContenu' => 'Note de contenu',
				'autoImport' => 'Import auto',
			],
			'Editeur' => [
				'nom' => 'Nom',
				'prefixe' => 'Préfixe',
				'sigle' => 'Sigle',
				'dateDebut' => "Début d'activité",
				'dateFin' => "Fin d'activité",
				'description' => 'Description',
				'url' => 'Adresse web',
				'geo' => 'Repère géo',
				'idref' => 'IdRef',
				'sherpa' => 'ID Sherpa',
				'liensJson' => 'Autres liens',
				'logoUrl' => "URL du logo",
				'role' => "Rôle/type",
			],
			'Service' => [
				'type' => 'Type',
				'acces' => 'Type d\'accès',
				'url' => 'Adresse web',
				'alerteRssUrl' => 'Flux RSS',
				'alerteMailUrl' => 'Alerte par courriel',
				'derNumUrl' => 'Dernier numéro',
				'lacunaire' => 'Couverture lacunaire',
				'selection' => 'Sélection d\'articles',
				'volDebut' => 'Premier volume (numérique)',
				'noDebut' => 'Premier numéro (numérique)',
				'numeroDebut' => 'Etat de collection (premier numéro)',
				'volFin' => 'Dernier volume (numérique)',
				'noFin' => 'Dernier numéro (numérique)',
				'numeroFin' => 'Etat de collection (dernier numéro)',
				'dateBarrDebut' => 'Date de début',
				'dateBarrFin' => 'Date de fin',
				'dateBarrInfo' => 'Date-barrière',
				'embargoInfo' => "Embargo",
				'notes' => "Notes KBART",
			],
		];
		if (empty($map['TitreEditeur'])) {
			$map['TitreEditeur'] = TitreEditeur::model()->attributeLabels();
		}
		return $map[$objectName][$attrName] ?? '';
	}

	/**
	 * Return an array where each element has a "url" field, or stops with an exception.
	 *
	 * @throws \Exception
	 */
	private static function validateLinkList($ll): array
	{
		if ($ll === null || $ll === '') {
			return [];
		}
		if (is_string($ll)) {
			$result = json_decode($ll, true);
		} else {
			$result = $ll;
		}
		if ($result === null || $result === '') { // after deserialization
			return [];
		}
		if (!is_array($result)) {
			throw new \Exception("Intervention contenant des liens mal formés : " . print_r($ll, true));
		}
		foreach ($result as $link) {
			if (!isset($link['url'])) {
				throw new \Exception("Intervention contenant des liens mal formés : " . print_r($ll, true));
			}
		}
		return $result;
	}

	/**
	 * Return a list of changes, suitable for display.
	 *
	 * Link order is ignored.
	 * Links are the same if both their "url" and their "src" are the same.
	 *
	 * @return string[] HTML
	 */
	private static function compareLinkLists(array $aa, array $bb): array
	{
		$cmp = function ($x, $y) {
			$diff = strcmp($x['url'], $y['url']);
			if ($diff !== 0) {
				return $diff;
			}
			return (isset($x['src']) !== isset($y['src']) ? 1 : strcmp($x['src'], $y['src']));
		};
		$keys = [];
		$removed = [];
		$added = [];
		foreach (array_udiff($aa, $bb, $cmp) as $link) {
			if (is_array($link) && isset($link['url'])) {
				$url = htmlspecialchars($link['url']);
				$k = $link['src'] ?? $link['url'];
				$keys[$k] = 1;
				$removed[$k] = $url;
			} else {
				\Yii::log("Format de lien non-valide : $link", CLogger::LEVEL_ERROR);
			}
		}
		foreach (array_udiff($bb, $aa, $cmp) as $link) {
			if (is_array($link) && isset($link['url'])) {
				$url = htmlspecialchars($link['url']);
				$k = $link['src'] ?? $link['url'];
				$keys[$k] = 1;
				$added[$k] = $url;
			} else {
				\Yii::log("Format de lien non-valide : $link", CLogger::LEVEL_ERROR);
			}
		}

		$result = [];
		foreach (array_keys($keys) as $k) {
			if (isset($removed[$k]) && isset($added[$k])) {
				$result[] = sprintf("%s : <a href=\"%s\">%s</a> → <a href=\"%s\">%s</a>", htmlspecialchars($k), $removed[$k], $removed[$k], $added[$k], $added[$k]);
			} elseif (isset($removed[$k])) {
				$result[] = sprintf("<b>-</b> %s : <a href=\"%s\">%s</a>", htmlspecialchars($k), $removed[$k], $removed[$k]);
			} else {
				$result[] = sprintf("<b>+</b> %s : <a href=\"%s\">%s</a>", htmlspecialchars($k), $added[$k], $added[$k]);
			}
		}
		return $result;
	}
}
