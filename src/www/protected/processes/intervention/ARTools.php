<?php

namespace processes\intervention;

use CActiveRecord;
use CDbTableSchema;
use Yii;

class ARTools
{
	public static function castFieldType(string $tableName, string $attribute, $value)
	{
		$tableSchema = Yii::app()->db->getSchema()->getTable($tableName);
		if ($tableSchema === null) {
			if (class_exists("\\$tableName")) {
				$tableSchema = CActiveRecord::model($tableName)->getTableSchema();
			} else {
				return $value;
			}
		}
		$fixed = self::fixTypes($tableSchema, [$attribute => $value]);
		return $fixed[$attribute];
	}

	public static function fixModelTypes(CActiveRecord $o): void
	{
		$attributes = $o->getAttributes();
		if (array_key_exists('id', $attributes)) {
			unset($attributes['id']);
		}
		$o->setAttributes(self::fixTypes($o->getTableSchema(), $attributes));
	}

	public static function fixFieldsTypes(string $tableName, array $attributes): array
	{
		$tableSchema = Yii::app()->db->getSchema()->getTable($tableName);
		if ($tableSchema === null) {
			if (class_exists("\\$tableName")) {
				$tableSchema = CActiveRecord::model($tableName)->getTableSchema();
			} else {
				Yii::log("Invalid model or table name '{$tableName}'");
				return $attributes;
			}
		}
		return self::fixTypes($tableSchema, $attributes);
	}

	private static function fixTypes(CDbTableSchema $tableSchema, array $attributes): array
	{
		$result = [];
		foreach ($attributes as $name => $v) {
			$colSchema = $tableSchema->getColumn($name);
			$result[$name] = ($colSchema === null ? $v : $colSchema->typecast($v));
		}
		return $result;
	}
}
