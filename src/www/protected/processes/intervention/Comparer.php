<?php

namespace processes\intervention;

class Comparer
{
	public static function isSameLinkJson(string $a, string $b): bool
	{
		if ($a) {
			$aa = json_decode($a, true, 32, JSON_THROW_ON_ERROR) ?? [];
			if (!is_array($aa)) {
				throw new \Exception("Invalid link structure.");
			}
		} else {
			$aa = [];
		}
		if ($b) {
			$bb = json_decode($b, true, 32, JSON_THROW_ON_ERROR) ?? [];
			if (!is_array($bb)) {
				throw new \Exception("Invalid link structure.");
			}
		} else {
			$bb = [];
		}
		return self::isSameLinkList($aa, $bb);
	}

	public static function isSameLinkList(array $a, array $b): bool
	{
		if (count($a) !== count($b)) {
			return false;
		}

		for ($i = 0; $i < count($a); $i++) {
			if (!self::isSameLink($a[$i], $b[$i])) {
				return false;
			}
		}
		return true;
	}

	private static function isSameLink(array $x, array $y): bool
	{
		if ($x['url'] !== $y['url']) {
			return false;
		}
		if (strcmp($x['src'] ?? '', $y['src'] ?? '') !== 0) {
			return false;
		}
		if ((int) ($x['sourceId'] ?? 0) !== (int) ($y['sourceId'] ?? 0)) {
			return false;
		}
		return true;
	}
}
