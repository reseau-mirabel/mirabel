<?php

namespace processes\intervention;

use CHtml;
use Intervention;
use Yii;

/**
 * This is a rendering helper for the Intervention model.
 */
class HtmlHelper
{
	/**
	 * @var Intervention
	 */
	private $model;

	public function __construct(Intervention $intervention)
	{
		$this->model = $intervention;
	}

	public function errors(): array
	{
		$errors = [];
		foreach ($this->model->getErrors() as $k => $e) {
			if ($k !== 'contenuJson') {
				$errors[$k] = join(" + ", $e);
			}
		}
		$hasdetailedError = false;
		foreach ($this->model->contenuJson->getErrors() as $k => $e) {
			if (is_int($k)) {
				if (!$hasdetailedError) {
					$errors[] = is_array($e) ? join(" + ", $e) : $e;
				}
			} else {
				$errors[$k] = is_array($e) ? join(" + ", $e) : $e;
				$hasdetailedError = true;
			}
		}
		return $errors;
	}

	public function summary(): string
	{
		if (empty($this->model->revueId) || empty($this->model->contenuJson)) {
			return "";
		}
		$actionDescription = [
			'service-D' => 'accès : suppression',
			'service-U' => 'accès : mise à jour',
			'editeur-C' => 'éditeur : création',
			'editeur-D' => 'éditeur : suppression',
			'editeur-U' => 'éditeur : mise à jour',
			'collection-C' => 'collection : création',
			'collection-D' => 'collection : suppression',
			'collection-U' => 'collection : mise à jour',
			'ressource-C' => 'ressource : création',
			'ressource-D' => 'ressource : suppression',
			'ressource-U' => 'ressource : mise à jour',
			'revue-C' => 'revue : création',
			'revue-D' => 'revue : suppression',
			'revue-U' => 'revue : mise à jour',
			'revue-I' => 'thématique : affectation',
		];
		if ($this->model->action && isset($actionDescription[$this->model->action])) {
			return $actionDescription[$this->model->action];
		}
		return $this->model->contenuJson->getSummary();
	}

	public function linkWithDetails(bool $avecPartenaire): string
	{
		if (empty($this->model->id)) {
			Yii::log("Intervention : affichage d'un lien vers une intervention sans ID", \CLogger::LEVEL_WARNING);
			return "<span>Intervention erronée</span>";
		}
		$details = $this->getFormatter()->format($this->model);
		return $this->link($avecPartenaire) . CHtml::tag('div', [], $details);
	}

	/**
	 * Returns a HTML link toward this object.
	 *
	 * @return string HTML link.
	 */
	public function link(bool $avecPartenaire): string
	{
		if (empty($this->model->id)) {
			Yii::log("Intervention : affichage d'un lien vers une intervention sans ID", \CLogger::LEVEL_WARNING);
			return "<span>Intervention erronée</span>";
		}
		[$text, $url] = $this->prepareLink($avecPartenaire);
		if ($url) {
			return CHtml::link(CHtml::encode($text), $url);
		}
		return CHtml::encode($text);
	}

	public static function hasPendingInterventions(int $partenaireId): bool
	{
		$floorTime = mktime(0, 0, 0) - 90 * 86400; // last 90 days
		$sql = <<<EOSQL
			SELECT 1
			FROM Intervention i
			JOIN Suivi s ON s.cible = '%s' AND i.%sId = s.cibleId
			WHERE s.partenaireId = $partenaireId AND hdateProp > $floorTime AND hdateVal IS NULL AND i.statut = 'attente'
			LIMIT 1
			EOSQL;
		if (\Yii::app()->db->createCommand(sprintf($sql, 'Revue', 'revue'))->queryScalar()) {
			return true;
		}
		if (\Yii::app()->db->createCommand(sprintf($sql, 'Ressource', 'ressource'))->queryScalar()) {
			return true;
		}
		if (\Yii::app()->db->createCommand(sprintf($sql, 'Editeur', 'editeur'))->queryScalar()) {
			return true;
		}
		return false;
	}

	/**
	 * Returns the Intervention records that match the Suivi rights, in the last 100 days.
	 *
	 * @param int|null $partenaireId If NULL, return intv for object that have no "Suivi" record.
	 * @param string|null $status
	 * @param int $since (opt) timestamp (seconds)
	 * @param int $limit (opt)
	 * @return Intervention[]
	 */
	public static function listInterventionsSuivi(?int $partenaireId, ?string $status, int $since = 0, int $limit = 0): array
	{
		$floorTime = $since ?: mktime(0, 0, 0) - 90 * 86400; // last 90 days
		$pId = $partenaireId ?: 'NULL';
		$left = $partenaireId ? "" : "LEFT";
		$limitSql = $limit > 0 ? sprintf("LIMIT %d", $limit) : "";
		if ($status === null) {
			$cond = "";
			$sort = "ORDER BY hdateProp DESC";
		} elseif ($status === 'attente') {
			$cond = " AND i.statut = 'attente'";
			$sort = "ORDER BY hdateProp DESC";
		} elseif (in_array($status, ['accepté', 'refusé'], true)) {
			$cond = " AND i.statut = '{$status}'";
			$sort = "ORDER BY hdateVal DESC";
		} else {
			throw new \Exception("Invalid parameter. Input was not sanitized.");
		}

		$sql = <<<EOSQL
			SELECT i.*
			FROM Intervention i
			$left JOIN Suivi s ON s.cible = 'Revue' AND i.revueId = s.cibleId
			WHERE s.partenaireId <=> $pId AND hdateProp > $floorTime $cond

			UNION

			SELECT i.*
			FROM Intervention i
			$left JOIN Suivi s ON s.cible = 'Ressource' AND i.ressourceId = s.cibleId
			WHERE s.partenaireId <=> $pId AND hdateProp > $floorTime $cond

			UNION

			SELECT i.*
			FROM Intervention i
			$left JOIN Suivi s ON s.cible = 'Editeur' AND i.editeurId = s.cibleId
			WHERE s.partenaireId <=> $pId AND hdateProp > $floorTime $cond

			$sort
			$limitSql
			EOSQL;
		return Intervention::model()->findAllBySql($sql);
	}

	/**
	 * Returns the Intervention records on the Editeur table.
	 *
	 * @param string $status
	 * @param int $limit (opt)
	 * @return Intervention[]
	 */
	public static function listInterventionsEditeurs(?string $status, int $limit = 10): array
	{
		$sql = "SELECT i.* "
			. "FROM Intervention i "
			. "WHERE editeurId IS NOT NULL";
		if ($status === null) {
			$sql .= " ORDER BY i.hdateProp DESC";
		} elseif ($status === 'attente') {
			$sql .= " AND i.statut = 'attente' ORDER BY i.hdateProp DESC";
		} elseif (in_array($status, ['accepté', 'refusé'], true)) {
			$sql .= " AND i.statut = '{$status}' ORDER BY i.hdateVal DESC";
		}
		$sql .= " LIMIT " . (int) $limit;
		return Intervention::model()->findAllBySql($sql);
	}

	/**
	 * Returns the Intervention records on the Editeur table.
	 *
	 * @param ?string $status
	 * @param int $limit (opt)
	 * @return Intervention[]
	 */
	public static function listInterventionsPartenairesEditeurs(?string $status, int $limit = 10): array
	{
		$sql = "SELECT i.* "
			. "FROM Intervention i JOIN Utilisateur u ON utilisateurIdProp = u.id JOIN Partenaire p ON p.id = u.partenaireId"
			. " WHERE p.type = 'editeur'";
		if ($status === null) {
			$sql .= " ORDER BY i.hdateProp DESC";
		} elseif ($status === 'attente') {
			$sql .= " AND i.statut = 'attente' ORDER BY i.hdateProp DESC";
		} elseif (in_array($status, ['accepté', 'refusé'], true)) {
			$sql .= " AND i.statut = '{$status}' ORDER BY i.hdateVal DESC";
		}
		$sql .= " LIMIT " . (int) $limit;
		return Intervention::model()->findAllBySql($sql);
	}

	private function prepareLink(bool $avecPartenaire): array
	{
		$title = $this->model->description;
		$url = '';
		if (!empty($this->model->titreId) && ($titre = \Titre::model()->findByPk($this->model->titreId))) {
			if (!$title) {
				$title = $titre->getFullTitle();
			}
			$url = Yii::app()->createUrl('/revue/view', ['id' => $this->model->revueId]);
		} elseif (isset($this->model->revueId) && ($revue = \Revue::model()->findByPk($this->model->revueId))) {
			if (!$title) {
				$title = $revue->getFullTitle();
			}
			$url = Yii::app()->createUrl('/revue/view', ['id' => $this->model->revueId]);
		} elseif (isset($this->model->ressourceId)) {
			$ressource = $this->model->ressource;
			if ($ressource) {
				if (!$title) {
					$title = $this->model->ressource->getFullName() . " (R)";
				}
				$url = Yii::app()->createUrl('/ressource/view', ['id' => $this->model->ressourceId]);
			} else {
				$title = "*ressource supprimée*";
			}
		} elseif (isset($this->model->editeurId)) {
			$editeur = $this->model->editeur;
			if ($editeur) {
				if (!$title) {
					$title = $this->model->editeur->getFullName() . " (E)";
				}
				$url = $this->model->editeur->getSelfUrl();
			} else {
				$title = "*éditeur supprimé*";
			}
		} else {
			Yii::log("Intervention : aucune table principale liée à {$this->model->id}", \CLogger::LEVEL_WARNING);
			if (!$title) {
				$title = "???";
			}
		}
		if (!$avecPartenaire) {
			if ($url) {
				return [$title, $url];
			}
			return [$title, []];
		}
		// Authentified with a Partenaire, so internal link to view the Intervention.
		return [$title, ['/intervention/view', 'id' => $this->model->id]];
	}

	private function getFormatter(): \processes\intervention\Formatter
	{
		static $formatter = null;
		if ($formatter === null) {
			$formatter = new \processes\intervention\Formatter();
			$formatter->showRestrictedInfo = Yii::app()->user->checkAccess("avec-partenaire");
		}
		return $formatter;
	}
}
