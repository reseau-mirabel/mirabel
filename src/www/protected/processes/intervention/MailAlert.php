<?php

namespace processes\intervention;

use CLogger;
use components\email\Mailer;
use Config;
use Intervention;
use Partenaire;
use Suivi;
use Utilisateur;
use Yii;

class MailAlert
{
	private int $since; // timestamp

	private int $before; // timestamp

	private bool $simulation;

	private string $ccToPartEd = "";

	private string $ccToPartVe = "";

	private string $cc = "";

	private string $logMaxLevel = '';

	private array $logMessages = [];

	public function __construct(string $since, string $before, bool $simulation)
	{
		$this->logMaxLevel = \CLogger::LEVEL_INFO;
		if ($since) {
			$sinceTs = strtotime($since);
			if ($sinceTs === false) {
				throw new \Exception("Since : wrong date");
			}
			$this->since = $sinceTs;
		} else {
			$this->since = time() - 86400; // = 24*60*60 = s/day
		}
		if ($before) {
			$beforeTs = strtotime($before);
			if ($beforeTs === false) {
				throw new \Exception("Before : wrong date");
			}
			$this->before = $beforeTs;
		} else {
			$this->before = 0;
		}
		$this->simulation = $simulation;
	}

	public function setRecipients(string $ccToPartEd, string $ccToPartVe, string $cc): void
	{
		$this->ccToPartEd = $ccToPartEd;
		$this->ccToPartVe = $ccToPartVe;
		$this->cc = $cc;
	}

	public function alertInterventionNonSuivi()
	{
		$interventions = Suivi::listInterventionsNotTracked("attente", (int) $this->since, (int) $this->before);
		if (!$interventions) {
			$this->log("## Alertes pour les objets non suivis.\nPas d'intervention en attente qui ne soit suivie.", CLogger::LEVEL_INFO);
			return 0;
		}

		$this->log("## Alertes pour les objets non suivis.", CLogger::LEVEL_INFO);
		$users = Utilisateur::model()->findAllByAttributes(['suiviNonSuivi' => 1, 'actif' => 1]);
		if (!$users) {
			$this->log("Erreur, pas de destinataire déclaré.", CLogger::LEVEL_WARNING);
			return 1;
		}
		$this->log("Utilisateurs concernés : " . count($users), CLogger::LEVEL_INFO);

		$this->sendEmail($users, $interventions, "non-suivi", $this->cc);
		return 0;
	}

	public function alertInterventionSuivi(): void
	{
		$byInstitute = Suivi::listInterventionsByInstitute((int) $this->since, (int) $this->before);
		if (!$byInstitute) {
			$this->log("## Alertes pour les objets suivis.\nAucun établissement concerné.", CLogger::LEVEL_INFO);
			return;
		}

		$this->log("## Alertes pour les objets suivis.\nÉtablissements concernés : " . count($byInstitute), CLogger::LEVEL_INFO);
		foreach ($byInstitute as $pid => $interventions) {
			$partenaire = Partenaire::model()->findByPk($pid);
			if (!($partenaire instanceof Partenaire)) {
				continue;
			}
			if ($this->ccToPartEd || $this->ccToPartVe) {
				if ($this->ccToPartEd && $partenaire->editeurId) {
					$cc = $this->ccToPartEd;
				} elseif ($this->ccToPartVe && !$partenaire->editeurId) {
					$cc = $this->ccToPartVe;
				} else {
					$cc = "";
				}
			} else {
				$cc = $this->cc;
			}
			$this->sendEmail([$partenaire], $interventions, "default", $cc);
		}
	}

	public function alertInterventionPartenaireEditeurs()
	{
		$this->log("## Alertes pour les propositions des partenaires-éditeurs.", CLogger::LEVEL_INFO);
		$interventions = suivi::listinterventionspartenairesediteurs((int) $this->since, (int) $this->before);
		if (!$interventions) {
			$this->log("Pas d'intervention en attente.", CLogger::LEVEL_INFO);
			return 0;
		}

		$users = utilisateur::model()->findallbyattributes(['suiviPartenairesEditeurs' => 1, 'actif' => 1]);
		if (!$users) {
			$this->log("Erreur, pas de destinataire déclaré.", CLogger::LEVEL_WARNING);
			return 1;
		}
		$this->log("Utilisateurs concernés : " . count($users), CLogger::LEVEL_INFO);

		$this->sendemail($users, $interventions, "partenaires-editeurs", $this->cc);
		return 0;
	}

	public function alertOnly(Partenaire $partenaire): int
	{
		$this->log("Alertes pour {$partenaire->nom}.", CLogger::LEVEL_INFO);
		$byInstitute = Suivi::listInterventionsByInstitute((int) $this->since, (int) $this->before);
		if (empty($byInstitute[$partenaire->id])) {
			$this->log("Rien de nouveau.", CLogger::LEVEL_INFO);
			return 0;
		}
		$this->log($byInstitute[$partenaire->id] . " objets concernés", CLogger::LEVEL_INFO);
		$this->sendEmail([$partenaire], $byInstitute[$partenaire->id]);
		return 0;
	}

	/**
	 * If there was an error/warning, the verbosity will be forced to trace/info.
	 *
	 * @param string $level Cf \CLogger::LEVEL_*
	 */
	public function getLogString(string $level): string
	{
		if ($this->logMaxLevel === \CLogger::LEVEL_ERROR) {
			$level = \CLogger::LEVEL_TRACE;
		} elseif ($this->logMaxLevel === \CLogger::LEVEL_WARNING && self::compareLogLevels($level, \CLogger::LEVEL_INFO) < 0) {
			$level = \CLogger::LEVEL_INFO;
		}
		$logs = [];
		foreach ($this->logMessages as [$mLevel, $message]) {
			if (self::compareLogLevels($mLevel, $level) >= 0) {
				$logs[] = $message;
			}
		}
		return join("\n", $logs);
	}

	/**
	 * Sends an email about these interventions.
	 *
	 * @param Partenaire[]|Utilisateur[] $dest Array of Partenaire|Utilisateur, required attributes: "email" and "nom".
	 * @param Intervention[] $interventions
	 * @param string $msgCategory
	 * @param string|array $cc
	 * @return bool
	 */
	private function sendEmail($dest, $interventions, $msgCategory = "default", $cc = ""): bool
	{
		if (!$dest || !$interventions) {
			return false;
		}
		$emails = [];
		foreach ($dest as $d) {
			assert(($d instanceof Partenaire) || ($d instanceof Utilisateur));
			if (empty($d->email)) {
				$this->log("Pas d'adresse e-mail pour {$d->nom}, alerte annulée pour ce destinataire.", CLogger::LEVEL_WARNING);
				continue;
			}
			$destEmails = strpos($d->email, "\n") ? array_filter(array_map('trim', explode("\n", $d->email))) : [$d->email];
			$destName = null;
			if (!empty($d->nomComplet)) {
				$destName = $d->nomComplet;
			} elseif (!empty($d->sigle)) {
				$destName = $d->sigle;
			} elseif (!empty($d->nom)) {
				$destName = $d->nom;
			}
			foreach ($destEmails as $e) {
				if ($destName) {
					$emails[$e] = $destName;
				} else {
					$emails[] = $e;
				}
				$this->log("- $e\n", CLogger::LEVEL_TRACE);
			}
		}
		if (empty($emails)) {
			$this->log("Aucun destinataire pour cette alerte, annulation.", CLogger::LEVEL_WARNING);
			return false;
		}

		$subject = Config::read("alerte.$msgCategory.subject");
		if (!$subject) {
			$subject = "Propositions à traiter dans Mir@bel : ";
		}
		$subject = "[" . Yii::app()->name . "] " . $subject
			. count($interventions)
			. " ("
			. ($this->before ? 'rappel' : date('Y-m-d', (int) $this->since))
			. ")";
		$body = Config::read("alerte.$msgCategory.body");
		if (!$body) {
			$body = "Bonjour,\n
une proposition de mise à jour a été faite sur une revue ou ressource que vous suivez dans Mir@bel.

%INTERVENTIONS%
Merci d'avance du suivi de cette proposition (validation ou rejet et réponse éventuelle).\n
Ceci est un message automatique généré par Mir@bel. Pour toute question : contact@reseau-mirabel.info
";
		}
		$content = '';
		foreach ($interventions as $i) {
			$content .= $i->renderAsText();
		}
		$body = str_replace('%INTERVENTIONS%', $content, $body);

		if ($this->simulation) {
			$to = $emails[0] ?? reset($emails);
			if (count($emails) > 1) {
				$to .= "… (" . count($emails) . " destinataires)";
			}
			$textCc = $cc ? (is_array($cc) ? join(", ", $cc) : $cc) : '';
			$this->log(
				"To: {$to}\n" . ($cc ? "Cc: $textCc\n" : "") . "$subject\n~~~~\n$body~~~~\n",
				CLogger::LEVEL_TRACE
			);
			return true;
		}
		$from = Config::read('email.from.alertes');
		if (!$from) {
			$from = Config::read('email.from');
		}
		$message = Mailer::newMail()
			->subject($subject)
			->from(new \Symfony\Component\Mime\Address($from, "Utilisateur Mir@bel"))
			->setTo($emails)
			->text($body);
		if ($cc) {
			$message->addCc($cc);
		}
		return Mailer::sendMail($message);
	}

	private function log(string $message, string $level): void
	{
		if (self::compareLogLevels($level, $this->logMaxLevel) > 0) {
			$this->logMaxLevel = $level;
		}
		$this->logMessages[] = [$level, $message];
	}

	private static function compareLogLevels(string $l1, string $l2): int
	{
		$toNum = [
			\CLogger::LEVEL_ERROR => 4,
			\CLogger::LEVEL_WARNING => 3,
			\CLogger::LEVEL_INFO => 2,
			\CLogger::LEVEL_TRACE => 1,
			\CLogger::LEVEL_PROFILE => 0,
		];
		return ($toNum[$l1] ?? 0) <=> ($toNum[$l2] ?? 0);
	}
}
