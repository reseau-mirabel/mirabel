<?php

namespace processes\analyse;

class CompareForm extends \CFormModel
{
	/**
	 * @var ?\CUploadedFile
	 */
	public $uploadedFile;

	public function rules()
	{
		return [
			['uploadedFile', 'required'],
			['uploadedFile', 'file', 'types' => "csv, tsv, txt, ods, xlsx"],
		];
	}

	public function attributeLabels()
	{
		return [
			'uploadedFile' => "Fichier de revues",
		];
	}

	/**
	 * @inheritDoc
	 */
	public function setAttributes($values, $safeOnly = true)
	{
		parent::setAttributes($values, $safeOnly);
		$this->uploadedFile = \CUploadedFile::getInstance($this, 'uploadedFile');
	}

	public function hasErrors($attribute = null)
	{
		return parent::hasErrors($attribute)
			|| !$this->uploadedFile
			|| $this->uploadedFile->hasError;
	}
}
