<?php

namespace processes\analyse;

use Exception;
use Yii;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use models\validators\IssnValidator;

class Compare
{
	private const MIMETYPE_ODS = 'application/vnd.oasis.opendocument.spreadsheet';

	private const MIMETYPE_XLSX = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';

	private int $numMatches = 0;

	private int $numRevues = 0;

	private int $numRows = 0;

	private int $numTitres = 0;

	public function __construct()
	{
		Yii::app()->db->createCommand(
			<<<EOSQL
			CREATE TEMPORARY TABLE IF NOT EXISTS RevueCompare (
				id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
				issn CHAR(9) COLLATE ascii_bin,
				position INT UNSIGNED NOT NULL,
				rawinput VARBINARY(2047)
			)
			EOSQL
		)->execute();
	}

	public function __destruct()
	{
		Yii::app()->db->createCommand("DROP TEMPORARY TABLE IF EXISTS RevueCompare")->execute();
	}

	/**
	 * Load a file after guessing its type from its extension.
	 */
	public function load(string $filepath): void
	{
		$extension = substr($filepath, strrpos($filepath, "."));
		switch ($extension) {
			case '.csv':
			case '.tsv':
			case '.txt':
				$this->loadCsv($filepath);
				break;
			case '.ods':
				$this->loadSpreadsheet($filepath, self::MIMETYPE_ODS);
				break;
			case '.xlsx':
				$this->loadSpreadsheet($filepath, self::MIMETYPE_XLSX);
				break;
			default:
				throw new \Exception("Extension non reconnue parmi csv, tsv, txt, ods, xlsx");
		}
	}

	public function loadCsv(string $filepath): void
	{
		$insert = Yii::app()->db->createCommand("INSERT INTO RevueCompare (issn, position, rawinput) VALUES (:issn, :position, :input)");
		$fh = fopen($filepath, "r");
		if (!$fh) {
			throw new Exception("Could not read file '$filepath'");
		}

		$validator = new IssnValidator();
		$position = 0;
		while (($line = fgets($fh)) !== false) {
			$position++;
			$matches = [];
			if (preg_match_all('/\b(\d{4}-\d{3}[\dX])\b/', $line, $matches, PREG_PATTERN_ORDER)) {
				$this->numRows++;
				foreach (array_unique($matches[1]) as $issn) {
					if ($validator->validateString($issn)) {
						$insert->execute([':issn' => $issn, ':position' => $position, ':input' => substr($line, 0, 2047)]);
					}
				}
			}
		}
		Yii::app()->db->createCommand("ALTER TABLE RevueCompare ADD INDEX IF NOT EXISTS issn_k (issn)");
		fclose($fh);
	}

	public function loadSpreadsheet(string $filepath, string $mimeType): void
	{
		$insert = Yii::app()->db->createCommand("INSERT INTO RevueCompare (issn, position, rawinput) VALUES (:issn, :position, :input)");

		if (str_starts_with($mimeType, self::MIMETYPE_ODS)) {
			$reader = ReaderEntityFactory::createODSReader();
		} elseif (str_starts_with($mimeType, self::MIMETYPE_XLSX)) {
			$reader = ReaderEntityFactory::createXLSXReader();
		} elseif (strncmp($mimeType, 'text/', 5) === 0) {
			$reader = ReaderEntityFactory::createCSVReader();
		} else {
			throw new \Exception("Format de tableur non reconnu");
		}
		$reader->setShouldFormatDates(true);
		$reader->open($filepath);

		$validator = new IssnValidator();
		foreach ($reader->getSheetIterator() as $worksheet) {
			/** @var \Box\Spout\Writer\Common\Entity\Sheet $row */
			$position = 0;
			foreach ($worksheet->getRowIterator() as $row) {
				/** @var \Box\Spout\Common\Entity\Row $row */
				$position++;
				$issnFound = [];
				$line = [];
				foreach ($row->getCells() as $cell) {
					/** @var \Box\Spout\Common\Entity\Cell $cell */
					$cellContent = (string) $cell->getValueEvenIfError();
					$line[] = $cellContent;
					$matches = [];
					if (preg_match_all('/\b(\d{4}-\d{3}[\dX])\b/', $cellContent, $matches, PREG_PATTERN_ORDER)) {
						foreach (array_unique($matches[1]) as $issn) {
							$issnFound[] = $issn;
						}
					}
				}
				foreach ($issnFound as $issn) {
					if ($validator->validateString($issn)) {
						$insert->execute([':issn' => $issn, ':position' => $position, ':input' => substr(join("\t", $line), 0, 2047)]);
					}
				}
				if ($issnFound) {
					$this->numRows++;
				}
			}
			break; // stop after the first worksheet
		}

		$reader->close();
	}

	public function getNumIssns(): int
	{
		return (int) \Yii::app()->db->createCommand("SELECT count(DISTINCT issn) FROM RevueCompare")->queryScalar();
	}

	public function getNumIssnMissing(): int
	{
		return (int) \Yii::app()->db->createCommand("SELECT count(DISTINCT r.issn) FROM RevueCompare r LEFT JOIN Issn i ON r.issn = i.issn WHERE i.issn IS NULL")->queryScalar();
	}

	public function getNumMatches(): int
	{
		return $this->numMatches;
	}

	public function getNumRevues(): int
	{
		return $this->numRevues;
	}

	public function getNumRows(): int
	{
		return $this->numRows;
	}

	public function getNumTitres(): int
	{
		return $this->numTitres;
	}

	/**
	 * @return CompareResult[]
	 */
	public function getMatches(int $partenaireId): array
	{
		$cmd = Yii::app()->db->createCommand(
			<<<EOSQL
			SELECT
				r.position,
				GROUP_CONCAT(r.issn ORDER BY r.issn SEPARATOR ' ') AS issn_input,
				GROUP_CONCAT(i.issn ORDER BY i.issn SEPARATOR ' ') AS issn_matching,
				MAX(t.titre) AS titre,
				GROUP_CONCAT(DISTINCT i.titreId SEPARATOR ' ') AS titreIds,
				GROUP_CONCAT(DISTINCT t.revueId SEPARATOR ' ') AS revueIds,
				MAX(s.partenaireId) AS suivi,
				GROUP_CONCAT(DISTINCT pt.identifiantLocal SEPARATOR ' ') AS idLocal,
				ISNULL(pt.identifiantLocal) AS possessionisnull,
				r.rawinput
			FROM RevueCompare r
				LEFT JOIN Issn i ON i.issn = r.issn AND i.statut = :st
				LEFT JOIN Titre t ON t.id = i.titreId
				LEFT JOIN Suivi s ON s.cible = 'Revue' AND s.cibleId = t.revueId
				LEFT JOIN Partenaire_Titre pt ON (pt.partenaireId = :pid AND pt.titreId = i.titreId)
			GROUP BY r.position
			EOSQL
		);
		$r = [];
		$revueIds = [];
		$titreIds = [];
		foreach ($cmd->query([':st' => \Issn::STATUT_VALIDE, ':pid' => $partenaireId]) as $row) {
			$r[] = new CompareResult($row);
			if ($row['titreIds']) {
				$this->numMatches++;
			}
			foreach (explode(" ", (string) $row['revueIds']) as $id) {
				if ($id) {
					$revueIds[] = (int) $id;
				}
			}
			foreach (explode(" ", (string) $row['titreIds']) as $id) {
				if ($id) {
					$titreIds[] = (int) $id;
				}
			}
		}
		$this->numRevues = count(array_unique($revueIds));
		$this->numTitres = count(array_unique($titreIds));
		return $r;
	}

	public function getIssnsMissingFromMirabel(): array
	{
		$cmd = Yii::app()->db->createCommand(
			<<<EOSQL
			WITH MatchingRows AS (
				SELECT r.position
				FROM RevueCompare r
					JOIN Issn i ON i.issn = r.issn AND i.statut = :st
				GROUP BY r.position
			)
			SELECT
				r.position,
				GROUP_CONCAT(r.issn ORDER BY r.issn SEPARATOR ' ') AS issn_missing
			FROM RevueCompare r
				JOIN MatchingRows USING(position)
				LEFT JOIN Issn i ON (i.issn = r.issn AND i.statut = :st) OR i.issnl = r.issn
			WHERE i.id IS NULL
			EOSQL
		);
		$r = [];
		foreach ($cmd->query([':st' => \Issn::STATUT_VALIDE]) as $row) {
			if ($row['issn_missing']) {
				$r[$row['position']] = explode(" ", $row['issn_missing']);
			}
		}
		return $r;
	}
}
