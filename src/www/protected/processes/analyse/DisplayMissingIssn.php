<?php

namespace processes\analyse;

class DisplayMissingIssn
{
	private array $missing;

	public function __construct(array $missing)
	{
		$this->missing = $missing;
	}

	public function formatList(int $position, string $issns): string
	{
		if (!isset($this->missing[$position])) {
			return $issns;
		}
		$missing = $this->missing[$position];
		$html = [];
		foreach (explode(" ", $issns) as $issn) {
			if (in_array($issn, $missing, true)) {
				$html[] = '<strong title="ISSN absent de Mirabel">∅' . $issn . '</strong>';
			} else {
				$html[] = $issn;
			}
		}
		return join(" ", $html);
	}

	public function getHtmlClass(int $position): string
	{
		return isset($this->missing[$position]) ? 'missing-issn error' : '';
	}
}
