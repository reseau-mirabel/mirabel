<?php

namespace processes\analyse;

class CompareResult
{
	public int $num;

	public string $issnInput;

	public string $issnMatching;

	public string $titre;

	public int $titreId;

	public int $revueId;

	public string $url;

	public bool $suivi;

	public int $numTitres;

	public int $numRevues;

	public string $possession; // identifiant local de possession ou "oui"

	public string $input;

	private static string $baseUrl = '';

	public function __construct(array $record)
	{
		if (self::$baseUrl === '') {
			self::$baseUrl = \Yii::app()->params->itemAt('baseUrl');
		}
		$this->num = (int) $record['position'];
		$this->issnInput = (string) $record['issn_input'];
		$this->issnMatching = (string) $record['issn_matching'];
		$this->titre = (string) $record['titre'];

		$titreIds = array_filter(explode(" ", (string) $record['titreIds']));
		$this->titreId = $titreIds ? (int) $titreIds[0] : 0;
		$this->numTitres = count($titreIds);

		$revueIds = array_filter(explode(" ", (string) $record['revueIds']));
		$this->revueId = $revueIds ? (int) $revueIds[0] : 0;
		$this->numRevues = count($revueIds);

		$this->url = $this->revueId ? self::$baseUrl . "/revue/{$this->revueId}" : "";
		$this->suivi = (bool) $record['suivi'];

		$this->possession = (string) $record['idLocal'] !== ''
			? (string) $record['idLocal']
			: ($record['possessionisnull'] ? '' : 'oui')
		;
		$this->input = $record['rawinput'];
	}
}
