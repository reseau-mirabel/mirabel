<?php

namespace processes\categorie;

use Categorie;
use CategorieAlias;
use CModel;
use Vocabulaire;
use Yii;

class CategorieAliasImport extends CModel
{
	public array $vocabsCreate = [];

	public array $vocabsUpdate = [];

	/**
	 * @var array name => col#
	 */
	private array $csvVocabs = [];

	/**
	 * @var array List of Vocabulaire names that exist in the CSV but not in the DB.
	 */
	private array $vocabsCreateCandidates = [];

	/**
	 * @var ?int col# for "ID"
	 */
	private ?int $idCol = null;

	/**
	 * @var ?int col# for "Thème"
	 */
	private ?int $nameCol = null;

	private $csvHandle;

	private string $csvSeparator = ';';

	private array $reportData = [];

	public function attributeNames()
	{
		return ['vocabsCreate', 'vocabsUpdate'];
	}

	public function attributeLabels()
	{
		return [
			'vocabsCreate' => "Vocabulaires nouveaux",
			'vocabsUpdate' => "Vocabulaires connus",
		];
	}

	public function rules()
	{
		return [
			['vocabsCreate, vocabsUpdate', 'isVocabList'],
			['vocabs', 'safe'],
		];
	}

	/**
	 * @param string $attribute the name of the attribute to be validated
	 * @param array $params options specified in the validation rule
	 * @return bool
	 */
	public function isVocabList($attribute, $params)
	{
		if (empty($this->{$attribute})) {
			return false;
		}
		if (!is_array($this->{$attribute})) {
			$this->addError($attribute, "Ce champ devrait être une liste de vocabulaires.");
			return false;
		}
		$this->{$attribute} = array_filter(
			array_map(function ($x) {
				return trim($x, " \n\t ");
			}, $this->{$attribute})
		);
		foreach ($this->{$attribute} as $v) {
			if (!isset($this->csvVocabs[$v])) {
				$this->addError($attribute, "Le vocabulaire $v ne figure pas dans le CSV.");
				return false;
			}
		}
		return true;
	}

	/**
	 * Return a list of Vocabulaire names that exist in the CSV but not in the DB.
	 *
	 * @return array
	 */
	public function getVocabsCreateCandidates()
	{
		return $this->vocabsCreateCandidates;
	}

	/**
	 * Return a list of Vocabulaire names that exist in the CSV and in the DB.
	 *
	 * @return array
	 */
	public function getVocabsUpdateCandidates()
	{
		return array_values(
			array_diff(array_keys($this->csvVocabs), $this->vocabsCreateCandidates)
		);
	}

	/**
	 * Load a CSV file and parse its header line.
	 */
	public function loadCsvFile(string $csvFile, string $separator): bool
	{
		if (!is_readable($csvFile)) {
			return false;
		}
		$handle = fopen($csvFile, "r");
		if ($handle === false) {
			return false;
		}
		$headers = fgetcsv($handle, 4096, $separator);
		if (!$headers || count($headers) < 2) {
			return false;
		}
		$this->csvHandle = $handle;
		$this->csvSeparator = $separator;

		$k = 0;
		foreach (array_filter(array_map('trim', $headers)) as $h) {
			if (strtoupper($h) === 'ID') {
				$this->idCol = $k;
			} elseif ($h === "Catégorie" || $h === "Thème") {
				$this->nameCol = $k;
			} else {
				$this->csvVocabs[trim($h, " \n\t ")] = $k;
			}
			$k++;
		}
		$this->vocabsCreateCandidates = $this->listUnknownVocabs($this->csvVocabs);
		return true;
	}

	/**
	 * Import into Vocabulaire and CategorieAlias from the CSV and selected vocabs.
	 *
	 * @return bool Success?
	 */
	public function import(): bool
	{
		$this->reportData = ['__global' => []];
		if (!$this->csvHandle) {
			$this->reportData['__global'][] = "Erreur d'accès au fichier CSV.";
			return false;
		}
		if (empty($this->vocabsCreate) && empty($this->vocabsUpdate)) {
			$this->reportData['__global'][] = "Aucun vocabulaire à importer.";
			return false;
		}

		$vocabs = $this->prepareVocabs();

		$rowNum = 0;
		while ($row = fgetcsv($this->csvHandle, 4096, $this->csvSeparator)) {
			$rowNum++;
			if (count(array_filter($row)) === 0) {
				continue;
			}
			$category = $this->findCategory($row, $rowNum);
			if ($category) {
				foreach ($vocabs as $v) {
					$this->importAlias($v, $category, $row[$this->csvVocabs[$v->titre]]);
				}
			}
		}
		return true;
	}

	public function getImportReport(): string
	{
		$report = '';
		foreach ($this->reportData as $group => $messages) {
			if ($messages) {
				$report .= '<div>'
					. '<h3>' . ($group === '__global' ? 'Erreurs' : $group) . '</h3>'
					. '<ul><li>'
					. join('</li><li>', $messages)
					. '</li></ul></div>';
			}
		}
		return $report;
	}

	/**
	 * Build a list of Vocabulaire instances from $this->vocabsCreate and $this->vocabsUpdate.
	 *
	 * @return array List of Vocabulaire instances
	 */
	private function prepareVocabs(): array
	{
		$vocabs = [];
		if ($this->vocabsCreate) {
			foreach ($this->vocabsCreate as $vName) {
				$vocab = new Vocabulaire();
				$vocab->titre = trim($vName);
				$vocab->commentaire = '';
				$vocab->save(false);
				$vocabs[] = $vocab;
			}
		}
		if ($this->vocabsUpdate) {
			foreach ($this->vocabsUpdate as $vName) {
				$vocabs[] = Vocabulaire::model()->findByAttributes(['titre' => $vName]);
			}
		}
		return $vocabs;
	}

	/**
	 * Find the Categorie instance matching a CSV line.
	 */
	private function findCategory(array $row, int $rowNum): ?Categorie
	{
		$messages = [];
		$categoryId = 0;
		$categoryName = '';
		if (isset($this->idCol)) {
			$categoryId = $row[$this->idCol];
		}
		if (isset($this->nameCol)) {
			$categoryName = $row[$this->nameCol];
		}
		$category = null;
		if ($categoryId) {
			$category = Categorie::model()->findByPk((int) $categoryId);
		} elseif ($categoryName) {
			$category = Categorie::model()->findByAttributes(['categorie' => $categoryName]);
		} else {
			$messages[] = "Erreur, le thème n'a ni ID ni nom";
		}
		if ($category) {
			return $category;
		}
		$messages[] = "Le thème n'a pas été trouvé dans Mir@bel";
		$this->reportData["Ligne $rowNum"] = $messages;
		return null;
	}

	private function importAlias(Vocabulaire $vocab, Categorie $category, string $aliasTitle): void
	{
		if (empty($aliasTitle)) {
			return;
		}
		if (empty($this->reportData[$vocab->titre])) {
			$this->reportData[$vocab->titre] = ["0 alias", "0 alias inchangés", "0 alias créés"];
		}
		$this->reportData[$vocab->titre][0] = (1 + (int) $this->reportData[$vocab->titre][0]) . " alias";
		$alias = CategorieAlias::model()
			->findByAttributes(['vocabulaireId' => $vocab->id, 'categorieId' => $category->id, "alias" => $aliasTitle]);
		if ($alias) {
			$this->reportData[$vocab->titre][1] = (1 + (int) $this->reportData[$vocab->titre][1]) . " alias inchangés";
		} else {
			$alias = new CategorieAlias();
			$alias->vocabulaireId = (int) $vocab->id ?: null;
			$alias->categorieId = $category->id;
			$alias->alias = trim($aliasTitle);
			$alias->modifPar = isset(Yii::app()->user) ? Yii::app()->user->id : null;
			$this->reportData[$vocab->titre][2] = (1 + (int) $this->reportData[$vocab->titre][2]) . " alias créés";
			if (!$alias->save()) {
				$this->reportData[$vocab->titre][] = "Erreur en enregistrant l'alias « {$alias->alias} »";
			}
		}
	}

	/**
	 * List the vocabs not in the DB among those received.
	 *
	 * @param array $vocabs assoc array: name => col#
	 * @return array
	 */
	private function listUnknownVocabs(array $vocabs): array
	{
		if (!$vocabs) {
			return [];
		}
		$unknown = [];
		foreach (array_keys($vocabs) as $v) {
			if (!Vocabulaire::model()->findByAttributes(['titre' => $v])) {
				$unknown[] = $v;
			}
		}
		return $unknown;
	}
}
