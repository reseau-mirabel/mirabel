<?php

namespace processes\attribut;

class ImportParsingForm extends \CFormModel
{
	public const NO_COLUMN = -1;

	/**
	 * @var ?int
	 */
	public $colonneAttribut;

	/**
	 * @var null|int|string
	 */
	public $colonneIssn1;

	/**
	 * @var ?int
	 */
	public $colonneIssn2;

	/**
	 * @var ?int
	 */
	public $colonneIssn3;

	/**
	 * @var null|int|string
	 */
	public $colonneMirabel;

	/**
	 * @var null|int|string
	 */
	public $colonnePpn1;

	/**
	 * @var ?int
	 */
	public $colonnePpn2;

	/**
	 * @var ?int
	 */
	public $sourceattributId;

	/**
	 * @var null|int|string
	 */
	public $colonneTitre = null;

	/**
	 * @var string
	 */
	public $filtre = "";

	public $deleteEmpty = true;

	public $deleteMissing = true;

	/**
	 * @var array [position => title] for each column header, used to fill the dropdown lists.
	 */
	public array $sourceColumns = [];

	public function rules()
	{
		return [
			['colonneAttribut, sourceattributId', 'required'],
			['colonneAttribut, colonneIssn1, colonneIssn2, colonneIssn3, colonneMirabel, colonnePpn1, colonnePpn2, colonneTitre', 'numerical', 'integerOnly' => true],
			['sourceattributId', 'numerical', 'integerOnly' => true],
			['filtre', 'length', 'max' => 255],
			['deleteEmpty, deleteMissing', 'boolean'],
		];
	}

	public function afterValidate()
	{
		if ($this->colonneIssn1 === '' && $this->colonneMirabel === '' && $this->colonnePpn1 === '') {
			$this->addError('colonneIssn1', "Au moins une colonne d'identifiant doit être choisie.");
		}
		return parent::afterValidate();
	}

	public function attributeLabels()
	{
		return [
			'colonneAttribut' => "Colonne de l'attribut",
			'colonneIssn1' => 'ISSN',
			'colonneIssn2' => 'Autre ISSN',
			'colonneIssn3' => 'Autre ISSN',
			'colonneMirabel' => "ID de titre Mir@bel",
			'colonnePpn1' => "PPN Sudoc",
			'colonnePpn2' => "Autre PPN",
			'colonneTitre' => "Colonne de titre",
			'sourceattributId' => 'Attribut',
			'filtre' => 'Filtre par valeur',
			'deleteEmpty' => "Supprimer les valeurs vides",
			'deleteMissing' => "Supprimer les valeurs pour les titres absents",
		];
	}

	/**
	 * Used for dropdown lists that select a column.
	 */
	public function getSourceattrColumns(): array
	{
		// We cannot use array_merge since it renumbers keys.
		// And we need a loop since NO_COLUMN should be the first element.
		$x = [
			self::NO_COLUMN => "sans colonne, l'attribut vaut 1 si le titre est présent",
		];
		foreach ($this->sourceColumns as $k => $v) {
			$x[$k] = $v;
		}
		return $x;
	}

	public function getSourceColumns(): array
	{
		return $this->sourceColumns;
	}

	public function extractConfig(): ImportConfig
	{
		return new ImportConfig([
			'filtre' => $this->filtre,
			'sourceattributId' => (int) $this->sourceattributId,
			'deleteEmpty' => (bool) $this->deleteEmpty,
			'deleteMissing' => (bool) $this->deleteMissing,
			'colAttribut' => (int) $this->colonneAttribut,
			'colTitre' => ($this->colonneTitre === null || $this->colonneTitre === "" ? -1 : (int) $this->colonneTitre),
			'colIdentifiers' => [
				'issn' => self::filterColumns([$this->colonneIssn1, $this->colonneIssn2, $this->colonneIssn3]),
				'mirabel' => self::filterColumns([$this->colonneMirabel]),
				'ppn' => self::filterColumns([$this->colonnePpn1, $this->colonnePpn2]),
			],
		]);
	}

	private static function filterColumns(array $cs): array
	{
		return array_map(
			'intval',
			array_filter(
				$cs,
				function ($x) {
					return $x !== null && $x !== '';
				}
			)
		);
	}
}
