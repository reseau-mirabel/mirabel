<?php

namespace processes\attribut;

/**
 * Store the import parameters.
 *
 * Once PHP 8.1+ is required, use `readonly` for every property.
 */
class ImportConfig implements \JsonSerializable
{
	/**
	 * @var int Position de la colonne contenant la valeur lue, en commençant à 0.
	 *    Avec -1 si aucune valeur (présence uniquement.
	 */
	public int $colAttribut = -1;

	/**
	 * @var array<string, int[]>
	 */
	public array $colIdentifiers = [
		'issn' => [],
		'ppn' => [],
		'mirabel' => [], // Titre.id
	];

	/**
	 * @var int Positif, ou -1 si pas de colonne pour l'intitulé.
	 */
	public int $colTitre = -1;

	/**
	 * @var string Si non-vide, toute autre valeur de l'attribut sera ignorée (considérée vide).
	 */
	public string $filtre = '';

	public int $sourceattributId;

	public bool $deleteEmpty = true;

	public bool $deleteMissing = true;

	public function __construct(array $config)
	{
		$reflection = new \ReflectionObject($this);
		foreach ($config as $k => $v) {
			if (!$reflection->hasProperty($k)) {
				throw new \Exception("Paramètre '$k' inconnu");
			}
			$this->{$k} = $v;
		}
		$this->validateConfig();
	}

	#[\ReturnTypeWillChange]
	public function jsonSerialize()
	{
		$data = [
			'sourceattributId' => $this->sourceattributId,
		];
		// Add each property which differs from its default value.
		if ($this->colAttribut !== -1) {
			$data['colAttribut'] = $this->colAttribut;
		}
		if ($this->colTitre !== -1) {
			$data['colTitre'] = $this->colTitre;
		}
		if ($this->filtre !== '') {
			$data['filtre'] = $this->filtre;
		}
		if (!$this->deleteEmpty) {
			$data['deleteEmpty'] = false;
		}
		if (!$this->deleteMissing) {
			$data['deleteMissing'] = false;
		}
		$ids = array_filter($this->colIdentifiers);
		if ($ids) {
			$data['colIdentifiers'] = $ids;
		}
		return $data;
	}

	private function validateConfig(): void
	{
		if ($this->sourceattributId < 0) {
			throw new \Exception("Valeur incorrecte pour la colonne de l'attribut");
		}
		if (empty($this->colIdentifiers['issn']) && empty($this->colIdentifiers['ppn']) && empty($this->colIdentifiers['mirabel'])) {
			throw new \Exception("Need at least one ISSN/PPN/Mirabel column");
		}
	}
}
