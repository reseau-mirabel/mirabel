<?php

namespace processes\attribut;

use AttributImportLog;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use CFileHelper;
use components\FileStore;
use Exception;
use Yii;

/**
 * Encapsule ImporteAttribute pour gérer le rapport, le log, etc, en plus de l'import lui-même.
 */
class FileImport
{
	public const FILESTORE_ID = 'attributs-import';

	/**
	 * @var string Full path to the source file (CSV, ODS...)
	 */
	private string $filePath = "";

	private string $lastError = "";

	private ImportConfig $config;

	private AttributImportLog $log;

	private ?Report $report = null;

	public function __construct(AttributImportLog $log, ImportConfig $config)
	{
		$this->log = $log;
		$this->filePath = $log->getFilePath();
		if (!$this->filePath) {
			throw new Exception("Fichier non trouvé. Trop ancien ?");
		}
		$this->config = $config;
		$log->sourceattributId = $config->sourceattributId;
	}

	public function getConfig(): ImportConfig
	{
		return $this->config;
	}

	public function getLastError(): string
	{
		return $this->lastError;
	}

	public function getLog(): \AttributImportLog
	{
		return $this->log;
	}

	public function getReport(): ?Report
	{
		return $this->report;
	}

	public static function readSourceHeader(string $file): array
	{
		if ($file === '') {
			throw new \Exception("Pas de fichier source.");
		}
		if (!is_readable($file)) {
			throw new \Exception("Pas de droit de lecture sur le fichier source.");
		}
		switch (\CFileHelper::getExtensionByMimeType($file)) {
			case 'ods':
				$reader = ReaderEntityFactory::createODSReader();
				break;
			case 'xlsx':
				$reader = ReaderEntityFactory::createXLSXReader();
				break;
			default:
				$csvDelimiter = \components\Csv::guessCsvFieldDelimiter($file);
				$fh = fopen($file, 'r');
				$firstLine = fgets($fh);
				fclose($fh);
				return str_getcsv($firstLine, $csvDelimiter, '"', '\\');
		}
		// For non-text files, parse with the Reader instance.
		$reader->setShouldFormatDates(true);
		$reader->open($file);
		foreach ($reader->getSheetIterator() as $worksheet) {
			assert($worksheet instanceof \Box\Spout\Reader\SheetInterface);
			foreach ($worksheet->getRowIterator() as $row) {
				assert($row instanceof \Box\Spout\Common\Entity\Row);
				$sourceColumns = $row->toArray();
				// stop after the first row of the first worksheet
				$reader->close();
				return $sourceColumns;
			}
		}
		return [];
	}

	public function save(): bool
	{
		$this->log->needNewLog();
		try {
			$this->report = self::import($this->filePath, $this->getConfig());
			$this->report->header = self::readSourceHeader($this->filePath);
			$this->report->sourceFile = $this->log->fileKey;
		} catch (\Throwable $e) {
			$this->lastError = $e->getMessage();
			Yii::log("Erreur au chargement du fichier : {$this->lastError}", 'warning');
			return false;
		}

		$this->log->reportKey = uniqid() . ".json";
		$store = new FileStore(self::FILESTORE_ID);
		if (!$store->setByContent($this->log->reportKey, json_encode($this->report, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE))) {
			$this->lastError = "Erreur à l'enregistrement du rapport dans '{$this->log->reportKey}'.";
			Yii::log($this->lastError, 'warning');
			return false;
		}
		$this->log->sourceattributId = (int) $this->log->sourceattributId;
		$this->log->logtime = time();
		return $this->log->save(false);
	}

	public static function storeFile(string $path, string $extension = ''): string
	{
		$hash = hash_file('md5', $path);
		if (!$extension) {
			$extension = CFileHelper::getExtension($path);
		}
		$key = "{$hash}.{$extension}";
		$store = new FileStore(self::FILESTORE_ID);
		$store->flushOlderThan(90*86400);
		$store->set($key, $path);
		return $key;
	}

	private static function import(string $filepath, ImportConfig $config): Report
	{
		$importer = new Importeattribut($config);
		$extension = CFileHelper::getExtension($filepath);
		$importer->load($filepath, $extension);
		return $importer->saveAttribute();
	}
}
