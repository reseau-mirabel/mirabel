<?php

namespace processes\attribut;

use Yii;

class Apc
{
	public static function countErrors(): int
	{
		$apc = Yii::app()->cache->get('apc-alert');
		if ($apc === false) {
			$apc = Yii::app()->db
				->createCommand(<<<SQL
					WITH Apc AS (
						SELECT
							at.titreId,
							json_arrayagg(json_object('id', sa.identifiant, 'nom', sa.nom, 'valeur', at.valeur)) AS attrValues,
							MAX(sa.identifiant LIKE 'apc-avec-%') AS avec,
							MAX(sa.identifiant LIKE 'apc-sans-%') AS sans
						FROM Sourceattribut sa
							JOIN AttributTitre at ON sa.id = at.sourceattributId
						WHERE sa.identifiant LIKE 'apc-%'
						GROUP BY at.titreId
					)
					SELECT count(*)
					FROM Apc
					WHERE avec > 0 AND sans > 0
					SQL
				)
				->queryScalar();
			Yii::app()->cache->set('apc-alert', $apc, time() + 3600);
		}
		return (int) $apc;
	}
}
