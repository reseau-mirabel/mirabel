<?php

namespace processes\attribut;

use CHtml;
use Titre;
use Yii;

class DisplayedAttributes
{
	/**
	 * sert uniquement à l'affichage des consignes utilisateur
	 */
	public const NAMES = ['apc-avec-*', 'apc-sans-*', 'doaj-sceau', 'doaj-france', 'latindex-sceau'];

	/**
	 * @return string[]
	 */
	public static function getTexts(Titre $titre): array
	{
		return self::valuesToTexts(self::getValues($titre));
	}

	/**
	 * @return array [$identifiant => $valeur]
	 */
	public static function getValues(Titre $titre): array
	{
		$attributs = Yii::app()->db
			->createCommand(<<<EOSQL
				SELECT sa.identifiant, at.valeur
				FROM Sourceattribut sa JOIN AttributTitre at ON sa.id = at.sourceattributId
				WHERE at.titreId = :tid
				EOSQL
			)
			->setFetchMode(\PDO::FETCH_KEY_PAIR)
			->queryAll(true, [':tid' => $titre->id]);
		$result = [];
		if (($attributs['latindex-sceau'] ?? '') === '1') {
			$result['latindex-sceau'] = true;
		}
		if (($attributs['doaj-sceau'] ?? '') === 'Yes') {
			$result['doaj-sceau'] = true;
		}
		// S'il y a des incohérences de données entre apc-avec et apc-sans, le résultat est effacé.
		foreach (array_keys($attributs) as $cle) {
			if (preg_match('/^apc-avec-/', $cle)) {
				if (isset($result['apc']) && $result['apc'] === false) {
					unset($result['apc']);
					break;
				}
				$result['apc'] = true;
			}
			if (preg_match('/^apc-sans-/', $cle)) {
				if (isset($result['apc']) && $result['apc'] === true) {
					unset($result['apc']);
					break;
				}
				$result['apc'] = false;
			}
		}
		if (!empty($attributs['doaj-france'])) {
			$result['doaj-france'] = true;
		}
		return $result;
	}

	/**
	 * @param array $values Result from ::getValues()
	 * @return string[]
	 */
	public static function valuesToTexts(array $values): array
	{
		$texts = [];
		if (isset($values['latindex-sceau'])) {
			$texts[] = CHtml::tag(
				'span',
				['title' => "Cette revue a reçu le sceau Latindex"],
				CHtml::image('/images/attributes/latindex-sceau.svg', "sceau Latindex", ['width' => '15', 'height' => '16'])
				. " Catálogo 2.0 Latindex"
			);
		}
		if (isset($values['doaj-sceau'])) {
			$texts[] = CHtml::tag(
				'span',
				['title' => "Cette revue a reçu le sceau DOAJ"],
				CHtml::image('/images/attributes/doaj-sceau-feuilles.svg', "DOAJ seal", ['width' => '16', 'height' => '16'])
				. " DOAJ seal"
			);
		}
		return $texts;
	}
}
