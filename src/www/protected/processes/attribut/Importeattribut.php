<?php

namespace processes\attribut;

use components\Csv;
use Exception;
use Yii;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;

/**
 * Classe de bas niveau pour effectuer l'import des valeurs d'un attribut.
 * Utilisée par FileImport qui se charge de préparer le rapport, de gérer l'historique, etC.
 */
class Importeattribut
{
	public ImportConfig $config;

	/**
	 * @var int nombre de lignes contenant un issn et un attribut
	 */
	private int $numRows = 0;

	private Report $report;

	public function __construct(ImportConfig $config)
	{
		$this->config = $config;
		$this->report = new Report($config);
	}

	/**
	 * Load a file into a temp SQL table.
	 */
	public function load(string $filepath, string $extension, string $delimiter = ""): void
	{
		$this->initTmpTable();
		switch ($extension) {
			case 'csv':
				$this->loadCsv($filepath, $delimiter ?: Csv::guessCsvFieldDelimiter($filepath));
				break;
			case 'tsv':
				$this->loadCsv($filepath, $delimiter ?: "\t");
				break;
			case 'txt':
				$this->loadCsv($filepath, $delimiter ?: Csv::guessCsvFieldDelimiter($filepath));
				break;
			case 'ods':
				$this->loadSpreadsheet($filepath, 'ods');
				break;
			case 'xlsx':
				$this->loadSpreadsheet($filepath, 'xlsx');
				break;
			default:
				throw new \Exception("Extension non reconnue parmi csv, tsv, txt, ods, xlsx");
		}
		$this->identifyInTmpTable();
	}

	/**
	 * Met à jour la table AttributTitre.
	 */
	public function saveAttribute(): Report
	{
		$this->report->numRows = $this->numRows;

		$startTime = time();
		$this->deleteValues();
		$this->findInconsistencies();
		$this->upsertValues();
		// Must be after deleteValues().
		$this->report->unknownTitles = $this->listUnknownTitles();

		$this->report->attributTitre = Yii::app()->db
			->createCommand(<<<EOSQL
				SELECT titreId, valeur
				FROM AttributTitre
				WHERE sourceattributId = {$this->config->sourceattributId} AND unix_timestamp(hdate) >= :ts
				EOSQL
			)->queryAll(false, [':ts' => $startTime]);
		$this->report->numChanges = Yii::app()->db
			->createCommand("SELECT count(*) FROM AttributTitre WHERE sourceattributId = {$this->config->sourceattributId} AND unix_timestamp(hdate) >= :ts")
			->queryScalar([':ts' => $startTime]);

		$this->afterSave();
		return $this->report;
	}

	public function getNumRows(): int
	{
		return $this->numRows;
	}

	protected function afterSave()
	{
		Yii::app()->db->createCommand("DROP TEMPORARY TABLE AttributImport")->execute();

		// Drop the cached value about APC inconsistencies.
		$attribut = \Sourceattribut::model()->findByPk($this->config->sourceattributId);
		if ($attribut && strncmp($attribut->identifiant, 'apc-', 4) === 0) {
			Yii::app()->cache->delete('apc-alert');
		}
	}

	/**
	 * Load a CSV into a temp table.
	 */
	private function loadCsv(string $filepath, string $separator): void
	{
		$fh = fopen($filepath, "r");
		if (!$fh) {
			throw new Exception("Could not read file '$filepath'");
		}
		$position = 1;
		while (($line = fgetcsv($fh, 0, $separator)) !== false) {
			if ($this->parseRow($line, $position)) {
				$this->numRows++;
			}
			$position++;
		}
		fclose($fh);
	}

	/**
	 * Load the first worksheet into a temp table.
	 */
	private function loadSpreadsheet(string $filepath, string $format): void
	{
		if ($format === 'ods') {
			$reader = ReaderEntityFactory::createODSReader();
		} elseif ($format === 'xlsx') {
			$reader = ReaderEntityFactory::createXLSXReader();
		} else {
			throw new \Exception("Format de tableur non reconnu");
		}
		$reader->setShouldFormatDates(true);
		$reader->open($filepath);

		foreach ($reader->getSheetIterator() as $worksheet) {
			/** @var \Box\Spout\Reader\SheetInterface $worksheet */
			$position = 1;
			foreach ($worksheet->getRowIterator() as $row) {
				/** @var \Box\Spout\Common\Entity\Row $row */
				if ($this->parseRow($row->getCells(), $position)) {
					$this->numRows++;
				}
				$position++;
			}
			break; // stop after the first worksheet
		}
		$reader->close();
	}

	/**
	 * Update $this->attributs with the ISSN and values found in the row.
	 * If $this->filtre is defined, updates only the rows with a matching value.
	 *
	 * @return bool True if at least one identifier was found and the value is authorized.
	 */
	private function parseRow(array $row, int $position): bool
	{
		if ($this->config->colAttribut > 0 && count($row) <= $this->config->colAttribut) {
			return false; // ignore empty or incomplete rows
		}
		$value = (string) ($this->config->colAttribut < 0 ? 1 : $row[$this->config->colAttribut]);

		if (!empty($this->config->filtre) && $value !== $this->config->filtre) {
			return false;
		}

		$data = [":issn0" => null, ":issn1" => null, ":issn2" => null, ":ppn" => null, ":titreId" => null, ':titre' => ''];
		$matches = [];
		if (!empty($this->config->colIdentifiers['issn'])) {
			foreach ($this->config->colIdentifiers['issn'] as $num => $colIssn) {
				if (!isset($row[$colIssn])) {
					return false;
				}
				$issncell = $row[$colIssn];
				if (preg_match('/\b(\d{4}-\d{3}[\dX])\b/', $issncell, $matches)) {
					$data[":issn$num"] = (string) $matches[1];
				}
			}
		}
		if (!empty($this->config->colIdentifiers['ppn'])) {
			$colPpn = $this->config->colIdentifiers['ppn'][0];
			if (!isset($row[$colPpn])) {
				return false;
			}
			$cell = $row[$colPpn];
			if (preg_match('/\b(\d{8}[\dX])\b/', $cell, $matches)) {
				$data[":ppn"] = (string) $matches[1];
			}
		}
		if (!empty($this->config->colIdentifiers['mirabel'])) {
			$colMir = $this->config->colIdentifiers['mirabel'][0];
			if (!isset($row[$colMir])) {
				return false;
			}
			$cell = $row[$colMir];
			if (preg_match('/^\s*(\d+)\s*$/', $cell, $matches)) {
				$data[":titreId"] = (int) $matches[1];
			}
		}
		if ($this->config->colTitre >= 0) {
			if (!empty($row[$this->config->colTitre])) {
				$data[":titre"] = $row[$this->config->colTitre];
			}
		}
		if (!array_filter($data)) {
			return false;
		}
		$data[':position'] = $position;
		$data[':valeur'] = $value;
		$this->insertIntoTmpTable($data);
		return true;
	}

	private function deleteValues(): void
	{
		if ($this->config->deleteMissing) {
			$deletions = Yii::app()->db
				->createCommand(<<<EOSQL
					SELECT at.titreId, at.valeur
					FROM AttributTitre at LEFT JOIN AttributImport ai ON at.titreId = ai.titreId
					WHERE at.sourceattributId = {$this->config->sourceattributId} AND ai.titreId IS NULL
					GROUP BY at.titreId
					EOSQL
				)->queryAll(false);
			foreach ($deletions as [$id, $v]) {
				$this->report->deletedAttributTitre[] = [(int) $id, $v];
			}
			$this->report->numDeletes += Yii::app()->db
				->createCommand(<<<EOSQL
					DELETE AttributTitre
					FROM AttributTitre LEFT JOIN AttributImport ai ON AttributTitre.titreId = ai.titreId
					WHERE AttributTitre.sourceattributId = {$this->config->sourceattributId} AND ai.titreId IS NULL
					EOSQL
				)->execute();
		}
		if ($this->config->deleteEmpty) {
			$deletions = Yii::app()->db
				->createCommand(<<<EOSQL
					SELECT at.titreId, at.valeur
					FROM AttributTitre at LEFT JOIN AttributImport ai ON at.titreId = ai.titreId
					WHERE at.sourceattributId = {$this->config->sourceattributId} AND ai.valeur = ''
					GROUP BY at.titreId
					EOSQL
				)->queryAll(false);
			foreach ($deletions as [$id, $v]) {
				$this->report->deletedAttributTitre[] = [(int) $id, $v];
			}
			$this->report->numDeletes += Yii::app()->db
				->createCommand(<<<EOSQL
					DELETE AttributTitre
					FROM AttributTitre JOIN AttributImport ai ON AttributTitre.titreId = ai.titreId
					WHERE AttributTitre.sourceattributId = {$this->config->sourceattributId} AND ai.valeur = ''
					EOSQL
				)->execute();
		}
	}

	private function initTmpTable(): void
	{
		$db = \Yii::app()->db;
		$db->createCommand(<<<EOSQL
				CREATE TEMPORARY TABLE AttributImport (
					position INT unsigned NOT NULL,
					issn0 CHAR(9) NULL COLLATE ascii_bin,
					issn1 CHAR(9) NULL COLLATE ascii_bin,
					issn2 CHAR(9) NULL COLLATE ascii_bin,
					ppn VARCHAR(9) NULL COLLATE ascii_bin,
					titreId INT unsigned NULL,
					titre TEXT NOT NULL,
					valeur TEXT NOT NULL
				)
				EOSQL)
			->execute();
		Yii::app()->db->createCommand("CREATE UNIQUE INDEX index_AttributImportIssn0 ON AttributImport (issn0)")->execute();
		Yii::app()->db->createCommand("CREATE UNIQUE INDEX index_AttributImportIssn1 ON AttributImport (issn1)")->execute();
		Yii::app()->db->createCommand("CREATE UNIQUE INDEX index_AttributImportIssn2 ON AttributImport (issn2)")->execute();
		Yii::app()->db->createCommand("CREATE UNIQUE INDEX index_AttributImportPpn ON AttributImport (ppn)")->execute();
	}

	private function insertIntoTmpTable(array $data): void
	{
		$insert = Yii::app()->db->createCommand("INSERT INTO AttributImport VALUES (:position, :issn0, :issn1, :issn2, :ppn, :titreId, :titre, :valeur)");
		try {
			$insert->execute($data);
		} catch (\Throwable $e) {
			$jsonData = json_encode(array_filter($data), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
			if (strpos($e->getMessage(), "1062 Duplicate entry")) {
				$this->report->messages[] = "Identifiants en doublons pour les données $jsonData.";
			} else {
				$this->report->messages[] = "ERREUR pour $jsonData, message :" . $e->getMessage();
			}
		}
	}

	private function identifyInTmpTable(): void
	{
		// Remove the wrong titreId values.
		Yii::app()->db
			->createCommand("UPDATE AttributImport a LEFT JOIN Titre t ON t.id = a.titreId SET a.titreId = NULL WHERE a.titreId > 0 AND t.id IS NULL")
			->execute();
		// Match by ISSN
		Yii::app()->db
			->createCommand("UPDATE AttributImport a JOIN Issn i ON i.issn = a.issn0 SET a.titreId = i.titreId WHERE a.titreId IS NULL")
			->execute();
		Yii::app()->db
			->createCommand("UPDATE AttributImport a JOIN Issn i ON i.issn = a.issn1 SET a.titreId = i.titreId WHERE a.titreId IS NULL")
			->execute();
		Yii::app()->db
			->createCommand("UPDATE AttributImport a JOIN Issn i ON i.issn = a.issn2 SET a.titreId = i.titreId WHERE a.titreId IS NULL")
			->execute();
		// Match by PPN
		Yii::app()->db
			->createCommand("UPDATE AttributImport a JOIN Issn i ON i.sudocPpn = a.ppn SET a.titreId = i.titreId WHERE a.titreId IS NULL")
			->execute();
	}

	private function upsertValues(): void
	{
		Yii::app()->db->createCommand("SET @startimport = now()")->execute(); // for the hook

		// Udate the existing attribute values.
		Yii::app()->db
			->createCommand(<<<EOSQL
				UPDATE AttributTitre at JOIN AttributImport ai USING(titreId)
				SET at.valeur = ai.valeur, at.hdate = NOW()
				WHERE at.sourceattributId = {$this->config->sourceattributId}
				EOSQL
			)->execute();

		// Insert the new attribute values.
		$condition = ($this->config->deleteEmpty ? "AND ai.valeur <> ''" : "");
		Yii::app()->db
			->createCommand(<<<EOSQL
				INSERT IGNORE INTO AttributTitre (titreId, sourceattributId, valeur)
					SELECT ai.titreId, {$this->config->sourceattributId}, ai.valeur
					FROM AttributImport ai
						LEFT JOIN AttributTitre at ON at.titreId = ai.titreId AND at.sourceattributId = {$this->config->sourceattributId}
					WHERE ai.titreId IS NOT NULL AND at.titreId IS NULL $condition
				EOSQL
			)->execute();

		// Hook to refresh the Manticore index of these Titre records.
		Yii::app()->db
			->createCommand(<<<SQL
				INSERT IGNORE INTO IndexingRequiredTitre (id, updated, deleted)
				SELECT titreId, hdate, 0 FROM AttributTitre WHERE hdate >= @startimport
				SQL
			)
			->execute();
	}

	private function listUnknownTitles(): array
	{
		if ($this->config->colTitre < 0) {
			return [];
		}
		$a = [];
		$condition = ($this->config->deleteEmpty ? "AND valeur <> ''" : "");
		$cmd = Yii::app()->db
			->createCommand(<<<EOSQL
				SELECT position, issn0, issn1, issn2, titre
				FROM AttributImport
				WHERE position > 1 AND titreId IS NULL AND titre <> '' $condition
				ORDER BY position
				EOSQL)
			->query();
		foreach ($cmd as $row) {
			$issn = trim(join(" ", [$row['issn0'], $row['issn1'], $row['issn2']]));
			$a[$row['position']] = [$issn, $row['titre']];
		}
		return $a;
	}

	/**
	 * Add report messages.
	 */
	private function findInconsistencies(): void
	{
		$condition = ($this->config->deleteEmpty ? "AND AttributImport.valeur <> ''" : "");
		$cmd = Yii::app()->db
			->createCommand(<<<EOSQL
				SELECT GROUP_CONCAT(position) AS positions, MIN(valeur) AS v1, MAX(valeur) AS v2, titreId
				FROM AttributImport
				WHERE titreId IS NOT NULL $condition
				GROUP BY titreId HAVING count(*) > 1
				EOSQL)
			->query();
		foreach ($cmd as $row) {
			$this->report->messages[] = "Les lignes {$row['positions']} correspondent au même titre d'ID {$row['titreId']} "
				. ($row['v1'] === $row['v2'] ? "(même valeur d'attribut)" : "<b>(valeurs différentes)</b>");
		}
		Yii::app()->db
			->createCommand(<<<EOSQL
				DELETE AttributImport
				FROM AttributImport
					JOIN AttributImport a2 ON AttributImport.titreId = a2.titreId AND AttributImport.position > a2.position
				WHERE AttributImport.titreId IS NOT NULL $condition
				EOSQL)
			->execute();
	}
}
