<?php

namespace processes\attribut;

class ImportSourceForm extends \CFormModel
{
	/**
	 * @var ?\CUploadedFile
	 */
	public $uploadedFile;

	/**
	 * @var string
	 */
	public $url = "";

	public function rules()
	{
		return [
			['uploadedFile', 'file', 'types' => "csv, tsv, txt, ods, xlsx", 'allowEmpty' => true],
			['url', 'url'],
		];
	}

	public function afterValidate()
	{
		parent::afterValidate();
		if (empty($this->uploadedFile)) {
			$this->uploadedFile = \CUploadedFile::getInstance($this, 'uploadedFile');
		}
		if (empty($this->uploadedFile) && !$this->url) {
			$this->addError('url', "Fournir soit un fichier, soit une URL.");
		}
		if (!empty($this->uploadedFile) && $this->url) {
			$this->addError('url', "Fournir soit un fichier, soit une URL, pas les deux.");
		}
	}

	public function attributeLabels()
	{
		return [
			'uploadedFile' => "Fichier de revues",
			'url' => "URL d'un fichier de revues",
		];
	}

	public function hasErrors($attribute = null)
	{
		return parent::hasErrors($attribute)
			|| ($this->uploadedFile && $this->uploadedFile->hasError);
	}
}
