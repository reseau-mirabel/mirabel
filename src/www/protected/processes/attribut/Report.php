<?php

namespace processes\attribut;

use AttributImportLog;
use components\FileStore;
use Exception;
use Sourceattribut;

class Report
{
	/**
	 * @var list{int, string}[] Chaque élément est [titreId, valeur-attribut].
	 */
	public array $attributTitre = [];

	/**
	 * @var list{int, string}[] Chaque élément est [titreId, valeur-attribut].
	 */
	public array $deletedAttributTitre = [];

	/**
	 * @var array<int, array> En clé, un numéro de lignes où le titres n'a pas été identifié.
	 *     En valeur, un tableau dont le premier élément est le texte des ISSN, le second le titre.
	 */
	public array $unknownTitles = [];

	/**
	 * @var string[] Liste numérotée des titres de colonnes (première ligne du fichier importé).
	 */
	public array $header = [];

	/**
	 * @var array<string, int[]> Rangs des colonnes où sont lus les idenifiants (clés 'issn', 'mirabel', 'ppn')
	 *    dans le fichier en entrée (rangs commençant à 0).
	 */
	public array $identifierColumns = [];

	/**
	 * @var string[] Message (erreurs, avertissements) produits en important les données.
	 */
	public array $messages = [];

	public int $numChanges = 0;

	public int $numDeletes = 0;

	public int $numRows = 0;

	public string $sourceFile = '';

	public int $version = 2;

	public ImportConfig $config;

	public function __construct(ImportConfig $config, array $attributes = [])
	{
		if ($attributes) {
			if (empty($attributes['version']) || $attributes['version'] !== $this->version) {
				throw new Exception("Impossible de charger un rapport ancien, car le format de stockage a changé.");
			}
			foreach ($attributes as $k => $v) {
				$this->{$k} = $v;
			}
		}
		$this->config = $config;
	}

	public function __set($name, $value)
	{
		throw new Exception("Coding error: cannot create property '{$name}' in the Report class.");
	}

	public static function loadByKey(string $key): self
	{
		$store = new FileStore(FileImport::FILESTORE_ID);
		$reportPath = $store->get($key);
		if (!$reportPath) {
			throw new Exception("Le fichier de rapport n'a pas été trouvé. Trop vieux ?");
		}
		$contents = file_get_contents($reportPath);
		if (!$contents) {
			throw new Exception("Le fichier de rapport n'a pas été trouvé ou est vide.");
		}
		$decoded = json_decode($contents, true, 512, JSON_THROW_ON_ERROR);
		$config = new ImportConfig($decoded['config']);
		unset($decoded['config']);
		return new self($config, $decoded);
	}

	public static function loadLast(mixed $identifiant): self
	{
		if (is_int($identifiant) || ctype_digit($identifiant)) {
			$attribut = Sourceattribut::model()->findByPk((int) $identifiant);
		} else {
			$attribut = Sourceattribut::model()->findByAttributes(['identifiant' => $identifiant]);
		}
		if (!($attribut instanceof Sourceattribut)) {
			throw new Exception("Aucun attribut n'a cet identifiant : $identifiant");
		}

		$log = AttributImportLog::model()
			->findBySql(
				"SELECT * FROM `AttributImportLog` WHERE `sourceattributId` = :id ORDER BY `logtime` DESC LIMIT 1",
				[':id' => $attribut->id]
			);
		if (!($log instanceof AttributImportLog)) {
			throw new Exception("Aucun import de cet attribut dans l'historique");
		}

		return self::loadByKey($log->reportKey);
	}
}
