<?php

namespace processes\upload;

use Yii;

class UploadDestination extends \CComponent
{
	public string $name = "";

	public string $message = "";

	public string $path = "";

	public string $url = "";

	public $forceDestName = null;

	public string $subpath = "";

	public string $types = ''; // extensions

	protected function __construct(array $config)
	{
		foreach (['name', 'path', 'url'] as $k) {
			if (!key_exists($k, $config)) {
				throw new \Exception("Erreur dans le code, la configuration est incomplète pour $k.");
			}
		}
		foreach ($config as $k => $v) {
			$this->{$k} = $v;
		}

		if (!is_dir($this->path)) {
			if (!@mkdir($this->path, 0773, true)) {
				throw new \Exception("Création du répertoire '{$this->path}' impossible. Problème de permissions de fichiers.");
			}
		} elseif (!is_writable($this->path)) {
			Yii::app()->user->setFlash(
				'error',
				"Un répertoire de destination n'est pas accessible en écriture : " . $this->path
			);
		}
	}

	public static function conventions(): self
	{
		$app = Yii::app();
		return new self([
			'name' => 'Conventions - fichiers privés',
			'path' => DATA_DIR . '/upload/conventions',
			'url' => $app->createAbsoluteUrl('/upload/view') . '/',
			'subpath' => 'conventions/',
			'types' => 'odp, odt, pdf, doc, docx, ppt, xls, md, txt',
		]);
	}

	public static function editeurs(): self
	{
		$app = Yii::app();
		return new self([
			'name' => 'Éditeurs - lettres',
			'path' => DATA_DIR . '/upload/editeurs',
			'url' => $app->createAbsoluteUrl('/upload/view') . '/',
			'subpath' => 'editeurs/',
			'types' => 'odp, odt, pdf, doc, docx, ppt, xls, md, txt',
		]);
	}

	public static function logoEditeur(): self
	{
		$app = Yii::app();
		return new self([
			'name' => 'Éditeurs - Logo',
			'path' => DATA_DIR . '/images/editeurs',
			'url' => $app->createAbsoluteUrl('/upload/view') . '/images/editeurs/',
		]);
	}

	public static function logoPartenaire(): self
	{
		$app = Yii::app();
		$user = $app->user;
		if ($user->checkAccess("avec-partenaire") && !$user->checkAccess('redaction/editorial')) {
			$paddedPartenaireId = sprintf('%03d', $user->getState('partenaireId'));
		} else {
			$paddedPartenaireId = null;
		}
		return new self([
			'name' => 'Logos des partenaires',
			'message' => "<li>Le logo doit avoir l'extension <em>jpg</em> ou <em>png</em>.</li>",
			'path' => DATA_DIR . '/images/partenaires/',
			'url' => $app->createAbsoluteUrl('/upload/view') . '/images/partenaires/',
			'forceDestName' => $paddedPartenaireId,
			'types' => 'png, jpg, jpeg, md, txt',
		]);
	}

	public static function logoRessource()
	{
		$app = Yii::app();
		return new self([
			'name' => 'Ressource - Logo',
			'path' => DATA_DIR . '/images/ressources/',
			'url' => $app->createAbsoluteUrl('/upload/view') . '/images/ressources/',
		]);
	}

	public static function private(): self
	{
		$app = Yii::app();
		return new self([
			'name' => 'Dépôt de fichiers privés',
			'path' => DATA_DIR . '/upload/prive',
			'url' => $app->createAbsoluteUrl('/upload/view') . '/prive/',
			'types' => 'odp, odt, pdf, doc, docx, ppt, xls, zip, png, jpg, jpeg, csv, mkv, mp4, avi, webm, md, txt',
		]);
	}

	public static function public(): self
	{
		$app = Yii::app();
		return new self([
			'name' => 'Dépôt de fichiers publics',
			'path' => DATA_DIR . '/upload/public',
			'url' => $app->getBaseUrl(true) . '/public/',
			//'forceDestName' => $paddedPartenaireId,
		]);
	}

	public static function videos(): self
	{
		$app = Yii::app();
		return new self([
			'name' => 'Vidéos',
			'message' => "<li>La vidéo doit avoir l'extension <em>mkv</em> <em>avi</em> ou <em>mp4</em>.</li>"
				. "<li>L'image doit avoir l'extension <em>jpg</em> ou <em>png</em>.</li>",
			'path' => DATA_DIR . '/upload/videos/',
			'url' => $app->createAbsoluteUrl('/upload/view') . '/videos/',
			'types' => 'png, jpg, jpeg, mkv, mp4, avi, webm, md, txt',
		]);
	}

	public function glob(string $path): array
	{
		$matches = [];
		$offset = strlen($this->path) + 1;
		$fullPath = $this->path . '/' . ltrim($path, '/');
		foreach (glob($fullPath) as $found) {
			$matches[] = substr($found, $offset); // remove the base path
		}
		return $matches;
	}
}
