<?php

namespace processes\editeur;

use components\sphinx\DataProvider;

class Export
{
	/**
	 * @var DataProvider
	 */
	private $provider;

	/**
	 * @param false|resource $out
	 */
	public function printCsv($out): void
	{
		if (!is_resource($out)) {
			throw new \Exception("bad code");
		}
		fputcsv(
			$out,
			[
				"id M",
				"préfixe",
				"nom",
				"sigle",
				"type",
				"id IdRef",
				"id Sherpa",
				"pays",
				"pays alpha2",
				"pays alpha3",
				"nb revues dans M",
				"nb titres vivants dans M",
				"date de vérification",
			],
			";",
			'"',
			'\\'
		);
		foreach ($this->provider->getData() as $data) {
			/** @var \models\sphinx\Editeurs $data */
			$record = $data->getRecord();
			if (!$record) {
				continue;
			}
			$pays = $record->pays;
			fputcsv(
				$out,
				[
					(int) $data->id,
					$record->prefixe,
					$record->nom,
					$record->sigle,
					$record->role ?? '',
					$record->idref,
					$record->sherpa,
					$pays ? $pays->nom : '',
					$pays ? $pays->code2 : '',
					$pays ? $pays->code : '',
					(int) $data->nbrevues,
					(int) $data->nbtitresvivants,
					$record->hdateVerif ? date("Y-m-d", $record->hdateVerif) : "",
				],
				';',
				'"',
				'\\'
			);
		}
	}

	public function setProvider(DataProvider $provider): void
	{
		$provider->criteria->limit = 10000;
		$provider->criteria->offset = 0;
		$this->provider = $provider;
	}
}
