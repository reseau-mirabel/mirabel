<?php

namespace processes\editeur;

use Partenaire;

class ListFilter
{
	public string $lettre;

	public int $letterRank;

	public ?int $suivi = null;

	public ?Partenaire $suiviPartenaire;

	public static function load(array $data): self
	{
		$filter = new self;
		$filter->lettre = empty($data['lettre']) ? "A" : strtoupper(substr($data['lettre'], 0, 1));
		$filter->suivi = empty($data['suivi']) ? 0 : (int) $data['suivi'];

		if ($filter->suivi > 0) {
			$filter->suiviPartenaire = Partenaire::model()->findByPk($filter->suivi);
			if ($filter->suiviPartenaire === null) {
				$filter->suivi = 0;
			}
		}
		if ($filter->suivi !== 0) {
			// no pagination;
			$filter->lettre = '';
		}

		return $filter;
	}
}
