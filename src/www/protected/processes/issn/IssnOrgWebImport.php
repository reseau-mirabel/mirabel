<?php

namespace processes\issn;

/**
 * Uses IssnOrgWeb to update the "pays" field of all the Issn records in the DB.
 *
 * Since the full process takes a long time,
 * DB access is done by batches, and DB connection is cut in-between.
 */
class IssnOrgWebImport
{
	/**
	 * Number of *Titre* records per batch, so the number of ISSN will be greater than this.
	 */
	private const BATCH_SIZE = 100;

	/**
	 * @var int Each error is on an Intervention with all the ISSN of a Titre.
	 */
	private int $countErrors = 0;

	/**
	 * @var int Each update is on an ISSN, part of an Intervention.
	 */
	private int $countUpdates = 0;

	private string $csv = '';

	private array $validCountries;

	public function __construct()
	{
		$this->validCountries = \components\SqlHelper::sqlToPairs("SELECT code2, 1 FROM Pays");
		$this->validCountries[''] = true;
	}

	/**
	 * Count the failed saves of Intervention records.
	 */
	public function countErrors(): int
	{
		return $this->countErrors;
	}

	/**
	 * Count the ISSN updated.
	 */
	public function countUpdates(): int
	{
		return $this->countUpdates;
	}

	public function getCsv(): string
	{
		return $this->csv;
	}

	public function run(bool $onlyEmpty): void
	{
		$this->countUpdates = 0;
		$this->csv = '';
		$sqlCondition = ($onlyEmpty ? " AND i.pays = ''" : '');

		$out = fopen('php://memory', 'w');
		$start = 0;
		$max = (int) \Yii::app()->db->createCommand("SELECT max(titreId) FROM Issn i WHERE i.issn IS NOT NULL $sqlCondition")->queryScalar();
		$updater = new IssnOrgPortal(['name', 'pays', 'support']); // 'name' fills 'titreReference'
		while ($start < $max) {
			// Fetch the ISSN data from the DB.
			$end = $start + self::BATCH_SIZE - 1;
			\Yii::app()->db->setActive(true);
			$issns = \Yii::app()->db
				->createCommand(<<<SQL
					SELECT i.id, i.issn, i.issnl, i.pays, i.support, i.titreReference, i.titreId, t.revueId
					FROM Issn i JOIN Titre t ON t.id = i.titreId
					WHERE i.titreId BETWEEN $start AND $end
						AND i.issn IS NOT NULL
						AND i.statut <> :err
						$sqlCondition
					SQL
				)
				->setFetchMode(\PDO::FETCH_OBJ)
				->queryAll(true, [':err' => \Issn::STATUT_ERRONE]);
			if ($issns) {
				\Yii::app()->db->setActive(false);

				// Fetch the ISSN portal info and list the updates for this batch.
				$updates = $updater->updateRecords($issns);
				$batchUpdates = []; // Grouped by Titre.id
				foreach ($updates as ['changes' => $changes, 'record' => $record]) {
					if (isset($changes['error'])) {
						fputcsv($out, [$record->titreId, $record->issn, join(' ', $changes)], ';', '"', '\\');
						continue;
					}
					if (!$this->validateCountry($record->pays)) {
						fputcsv($out, [$record->titreId, $record->issn, "pays:{$record->pays}-non-valide"], ';', '"', '\\');
						unset($changes['pays']);
					}
					if (!$changes) {
						continue;
					}

					// Add a assoc array of field changes, for later writes.
					$data = [
						'issnId' => (int) $record->id,
						'issn' => $record->issn,
						'changes' => $changes, // Human readable change list
						'fields' => [],
					];
					foreach (array_keys($changes) as $c) {
						$data['fields'][$c] = $record->{$c};
					}
					if (isset($batchUpdates[$record->titreId])) {
						$batchUpdates[$record->titreId][] = $data;
					} else {
						$batchUpdates[$record->titreId] = [(int) $record->revueId, $data];
					}
				}

				// Write this batch into the DB
				\Yii::app()->db->setActive(true);
				self::writeBatch($batchUpdates, $out);
				sleep(1);
			}

			$start += self::BATCH_SIZE;
		}
		$this->csv = (string) stream_get_contents($out, -1, 0);
	}

	private function validateCountry(string $alpha2): bool
	{
		return isset($this->validCountries[$alpha2]);
	}

	/**
	 * @param resource $out
	 */
	private function writeBatch(array $batchUpdates, $out): void
	{
		foreach ($batchUpdates as $titreId => $updates) {
			$int = new \Intervention();
			$int->statut = 'attente';
			$int->contenuJson = new \InterventionDetail();
			$int->setImport(\models\import\ImportType::getSourceId('issn.org-portal'));
			$int->action = "revue-U";
			$int->description = "Mise à jour des données ISSN via le portail issn.org";
			$int->titreId = (int) $titreId;
			$int->revueId = (int) array_shift($updates);

			foreach ($updates as $u) {
				$issnRecord = \Issn::model()->findByPk($u['issnId']);
				assert($issnRecord instanceof \Issn);
				$int->contenuJson->updateByAttributes($issnRecord, $u['fields']);
			}

			if ($int->accept()) {
				foreach ($updates as $u) {
					$this->countUpdates++;
					fputcsv($out, [$titreId, $u['issn'], join(' ', $u['changes'])], ';', '"', '\\');
				}
			} else {
				$this->countErrors++;
				$issnChanges = [];
				foreach ($updates as $u) {
					$issnChanges[$u['issn']] = sprintf("« {%s} %s »", $u['issn'], join(" ", $u['changes']));
				}
				$errors = $int->contenuJson->getErrors();
				$errorMessage = "ERREUR, L'enregistrement de "
					. join(" + ", $issnChanges)
					. " a échoué : "
					. array_shift($errors)[0];
				fputcsv($out, [$titreId, join(',', array_keys($issnChanges)), $errorMessage], ';', '"', '\\');
			}
		}
	}
}
