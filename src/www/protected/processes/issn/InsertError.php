<?php

namespace processes\issn;

class InsertError
{
	public int $position = 0;

	public string $issnp = '';

	public string $issne = '';

	public string $error = '';
}
