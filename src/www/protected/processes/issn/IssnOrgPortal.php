<?php

namespace processes\issn;

/**
 * Query the web site of issn.org by ISSN, then extract the JSON-LD data.
 *
 * This class can send concurrent requests.
 */
class IssnOrgPortal
{
	private const MAX_PARALLEL_REQUESTS = 4;

	private const URL_BASE = 'https://portal.issn.org/resource/ISSN/';

	private array $normalizeCountries;

	private array $updateFields = [
		'issnl' => false,
		'name' => false,
		'pays' => false,
		'support' => false,
	];

	/**
	 * @param string[] $fields List, subset of ['issnl', 'name', 'pays', 'support'].
	 */
	public function __construct(array $fields)
	{
		foreach ($fields as $k) {
			if (!isset($this->updateFields[$k])) {
				throw new \Exception("Invalid code, '$k' is not available for updates.");
			}
			$this->updateFields[$k] = true;
		}
		$this->normalizeCountries = \Config::read('pays.correspondance') ?? [];
		$this->normalizeCountries['OI'] = 'ZZ'; // convert "OI" (International) into "ZZ" (multiple)
	}

	/**
	 * Wrapper of updateRecords() for a single record.
	 *
	 * @param object $record
	 * @return array List of changes
	 */
	public function updateRecord(object $record): array
	{
		$issn = $record->issn;
		if (!$issn) {
			return [];
		}
		$result = $this->updateRecords([$record]);
		return $result[$issn]['changes'];
	}

	/**
	 * Updates objects (usually Issn records) with issn.org's website data.
	 *
	 * It will download some JSON data for each ISSN in the records received.
	 *
	 * @param array $records List where each item is an associative record to update: object{issn: ?string, issnl: string, pays: string}[]
	 * @return array<string, array{changes: string[], record: object}> For each ISSN, updated record and names of attributes changed.
	 */
	public function updateRecords(array $records): array
	{
		$issns = [];
		$result = [];
		foreach ($records as $r) {
			if ($r->issn) {
				$issns[] = $r->issn;
				$result[$r->issn] = ['changes' => [], 'record' => $r];
			}
		}

		$jsons = $this->getJsonld($issns);
		if (!$jsons) {
			return $result;
		}

		foreach ($jsons as $issn => $data) {
			if (!$data) {
				$result[$issn]['changes']['error'] = "Aucune donnée. Probablement une erreur de téléchargement.";
				continue;
			}

			if ($this->updateFields['issnl']) {
				$issnl = self::readIssnl($data);
				if ($issnl && $issnl !== $result[$issn]['record']->issnl) {
					$result[$issn]['changes']['issnl'] = "issnl:{$result[$issn]['record']->issnl}→{$issnl}";
					$result[$issn]['record']->issnl = $issnl;
				}
			}

			if ($this->updateFields['name']) {
				if (property_exists($result[$issn]['record'], 'titreReference') || isset($result[$issn]['record']->titreReference)) {
					$field = 'titreReference'; // \Issn
				} elseif (property_exists($result[$issn]['record'], 'titreIssn')) {
					$field = 'titreIssn'; // \models\sudoc\Notice
				} elseif (property_exists($result[$issn]['record'], 'titre')) {
					$field = 'titre';
				} else {
					throw new \Exception("Invalid record");
				}

				$name = self::readName($data);
				if ($name && $name !== $result[$issn]['record']->{$field}) {
					$result[$issn]['changes'][$field] = "$field:{$result[$issn]['record']->{$field}}→{$name}";
					if ($result[$issn]['record'] instanceof \models\sudoc\Notice && !$result[$issn]['record']->titre) {
						$result[$issn]['record']->titre = $name;
					}
					$result[$issn]['record']->{$field} = $name;
				}
			}

			if ($this->updateFields['pays']) {
				$country = $this->readCountryAlpha2($data);
				if ($country && $country !== $result[$issn]['record']->pays) {
					$result[$issn]['changes']['pays'] = "pays:{$result[$issn]['record']->pays}→{$country}";
					$result[$issn]['record']->pays = $country;
				}
			}

			if ($this->updateFields['support']) {
				$support = self::readSupport($data);
				if ($support && $support !== $result[$issn]['record']->support) {
					$result[$issn]['changes']['support'] = "support:{$result[$issn]['record']->support}→{$support}";
					$result[$issn]['record']->support = $support;
				}
			}
		}

		return $result;
	}

	/**
	 * @param string[] $issns
	 * @return array<string, string> ISSN => HTML
	 */
	protected function downloadHtml(array $issns): array
	{
		$curl = curl_multi_init();
		// HTTP2, so many requests are sent through a single TCP connection.
		curl_multi_setopt($curl, \CURLMOPT_MAX_CONCURRENT_STREAMS, self::MAX_PARALLEL_REQUESTS);
		curl_multi_setopt($curl, \CURLMOPT_MAX_HOST_CONNECTIONS, 1);
		curl_multi_setopt($curl, \CURLMOPT_PIPELINING, 2); // Magic constant, see https://www.php.net/manual/en/function.curl-multi-setopt.php

		// Add the requests to the multi curl.
		$requests = [];
		foreach ($issns as $issn) {
			$subcurl = curl_init();
			curl_setopt_array(
				$subcurl,
				[
					\CURLOPT_CONNECTTIMEOUT => 5, // seconds
					\CURLOPT_COOKIEFILE => "",          // store cookies?
					\CURLOPT_FOLLOWLOCATION => true,    // follow redirection (301, etc)
					\CURLOPT_HEADER => false,
					\CURLOPT_MAXREDIRS => 4,
					\CURLOPT_PIPEWAIT => true, // wait for pipelining/multiplexing if HTTP2
					\CURLOPT_RETURNTRANSFER => true,
					\CURLOPT_SSL_VERIFYHOST => false,
					\CURLOPT_SSL_VERIFYPEER => false,
					\CURLOPT_TIMEOUT => 200, // seconds. Timeout for the whole transfer, including waiting for a stream or connection!
					\CURLOPT_URL => self::URL_BASE . $issn,
					\CURLOPT_VERBOSE => false, // If enabled, debug on STDERR
				]
			);
			$requests[$issn] = $subcurl;
			curl_multi_add_handle($curl, $subcurl);
		}

		// Send the queries
		$active = null;
		do {
			if ($active) {
				curl_multi_select($curl, 1); // wait for action, up to 1 s.
			}
			$status = curl_multi_exec($curl, $active); // Modifies $active!!!
		} while ($active > 0 && $status === CURLM_OK);
		if ($status !== CURLM_OK) {
			throw new \Exception("Erreur fatale de téléchargement : " . curl_multi_strerror($status));
		}

		$htmls = [];
		foreach ($requests as $issn => $subcurl) {
			$htmls[$issn] = (string) curl_multi_getcontent($subcurl);
			curl_multi_remove_handle($curl, $subcurl);
		}
		curl_multi_close($curl);
		return $htmls;
	}

	/**
	 * Return an assoc array for each ISSN.
	 *
	 * @param string[] $issns
	 * @return array<string, array> ISSN => decoded-JSON-LD
	 */
	private function getJsonld(array $issns): array
	{
		$htmls = $this->downloadHtml($issns);
		$result = [];
		foreach ($htmls as $issn => $html) {
			if ($html === '') {
				$result[$issn] = [];
			} else {
				$json = preg_replace(['#^.+?<script type="application/ld\+json">#s', '#</script>.+$#s'], ['', ''], $html);
				$decoded = json_decode($json, true);
				$result[$issn] = $decoded ?? [];
			}
		}
		return $result;
	}

	private static function readIssnl(array $data): string
	{
		if (empty($data['identifier'])) {
			return "";
		}
		foreach ($data['identifier'] as $x) {
			if ($x["@type"] === "PropertyValue" && $x["name"] === "ISSN-L" && $x["description"] === "Valid") {
				return $x["value"] ?? "";
			}
		}
		return "";
	}

	private function readCountryAlpha2(array $data): string
	{
		if (empty($data['publication']) || empty($data['publication']['location'])) {
			return "";
		}
		$location = $data['publication']['location'];
		if ($location["@type"] !== "Country") {
			\Yii::log("Issn.org pour {$data['issn']} : 'location' n'est pas un pays mais '{$location['@type']}'", 'warning');
		}
		$m = [];
		if (preg_match('!https?://www\.iso\.org/obp/ui/#iso:code:3166:([A-Z]{2})$!', $location['@id'], $m)) {
			return $this->normalizeCountry($m[1]);
		}
		return "";
	}

	private static function readName(array $data): string
	{
		return \Norm::text(
			trim($data['name'] ?? '', " .")
		);
	}

	private static function readSupport(array $data): string
	{
		$material = $data['material'] ?? '';
		if (!$material) {
			return '';
		}
		$convert = [
			'Print' => \Issn::SUPPORT_PAPIER,
			'Online' => \Issn::SUPPORT_ELECTRONIQUE,
		];
		return $convert[$material] ?? '';
	}

	/**
	 * normalisation pour les codes de DOM-TOM cf. https://tickets.silecs.info/view.php?id=4819
	 * @param string $alpha2
	 * @return string
	 */
	private function normalizeCountry(string $alpha2): string
	{
		return $this->normalizeCountries[$alpha2] ?? $alpha2;
	}
}
