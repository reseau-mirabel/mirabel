<?php

namespace processes\issn;

use models\import\ImportType;
use Intervention;
use Issn;
use Titre;
use Yii;

class Importpe
{
	private const SQL_MATCHING_TABLE = <<<EOSQL
		WITH Matching AS (
			SELECT
				Issn.titreId,
				Titre.revueId,
				Titre.titre,
				IssnPE.issnp,
				IssnPE.issne,
				IssnPE.position,
				IFNULL(MAX(IssnPE.issne = i.issn), 0) AS hasThisIssne,
				GROUP_CONCAT(i.issn SEPARATOR ' ') AS mirabelIssns
			FROM
				IssnPE
				JOIN Issn ON IssnPE.issnp = Issn.issn AND Issn.support = 'papier'
				JOIN Titre ON Titre.id = Issn.titreId
				LEFT JOIN Issn i ON i.titreId = Issn.titreId AND i.support = 'electronique'
			GROUP BY titreId, Issn.issn
			ORDER BY position ASC
		)
		EOSQL;

	public bool $querySudocApi = true;

	private bool $isEmpty = true;

	/**
	 * @var InsertError[]
	 */
	private $errorsLog = [];

	private $insertLog = [];

	public function __construct()
	{
		Yii::app()->db
			->createCommand(<<<EOSQL
				CREATE TEMPORARY TABLE IF NOT EXISTS IssnPE (
					issnp CHAR(9) COLLATE ascii_bin,
					issne CHAR(9) COLLATE ascii_bin,
					position INT UNSIGNED NOT NULL
				)
				EOSQL
			)
			->execute();
	}

	public function __destruct()
	{
		Yii::app()->db->createCommand("DROP TEMPORARY TABLE IF EXISTS IssnPE")->execute();
	}

	public function isEmpty(): bool
	{
		return $this->isEmpty;
	}

	/**
	 * Lit un fichier CSV de deux colonnes, ISSN-P et ISSN-E
	 * remplit la table IssnPE  et  $this->matching
	 *
	 * See Mantis #5208.
	 */
	public function loadFile(string $fileIn): void
	{
		$handle = fopen($fileIn, "r");
		if (!$handle) {
			throw new \Exception("Could not read file '$fileIn'");
		}

		$insert = Yii::app()->db->createCommand("INSERT INTO IssnPE (issnp, issne, position) VALUES (:issnp, :issne, :position)");
		$position = 0;
		while ($row = fgetcsv($handle, 0, "\t")) {
			$position++;
			if (count($row) < 2) {
				continue;
			}
			if (preg_match('/^(\d{4}-\d{3}[\dX])$/', $row[0]) && preg_match('/^(\d{4}-\d{3}[\dX])$/', $row[1])) {
				$insert->execute([':issnp' => $row[0], ':issne' => $row[1], ':position' => $position]);
			}
		}
		fclose($handle);

		$this->isEmpty = false;
	}

	/**
	 * @return InsertError[]
	 */
	public function listErrors(): iterable
	{
		return $this->errorsLog;
	}

	/**
	 * Liste les couples d'ISSN chargés par CSV et déjà présents dans M.
	 *
	 * @return iterable<int, array> Champs : titreId, revueId, titre, issnp, issne, position
	 */
	public function listExisting(): iterable
	{
		$sql = self::SQL_MATCHING_TABLE . <<<EOSQL
			SELECT titreId, revueId, titre, issnp, issne, position
			FROM Matching
			WHERE hasThisIssne = 1
			EOSQL;
		return Yii::app()->db->createCommand($sql)->query();
	}

	public function listInserts(): iterable
	{
		return $this->insertLog;
	}

	/**
	 * Liste les couples d'ISSN chargés par CSV et déjà présents dans M.
	 *
	 * @return iterable<int, array>
	 */
	public function listToInsert(): iterable
	{
		$sql = self::SQL_MATCHING_TABLE . <<<EOSQL
			SELECT titreId, revueId, titre, issnp, issne, position, mirabelIssns
			FROM Matching
			WHERE hasThisIssne = 0
			EOSQL;
		return Yii::app()->db->createCommand($sql)->query();
	}

	public function listUnknown(): iterable
	{
		return Yii::app()->db
			->createCommand(<<<EOSQL
				SELECT
					IssnPE.issnp,
					IssnPE.issne,
					IssnPE.position
				FROM
					IssnPE
					LEFT JOIN Issn ON IssnPE.issnp = Issn.issn AND Issn.support = 'papier'
				WHERE Issn.id IS NULL
				ORDER BY position ASC
				EOSQL
			)->query();
	}

	/**
	 * Ajoute tous les ISSN électroniques manquant
	 */
	public function insertMissing(): int
	{
		foreach ($this->listToInsert() as $row) {
			$titre = new Titre();
			$titre->id = (int) $row['titreId'];
			$titre->revueId = (int) $row['revueId'];
			$titre->titre = $row['titre'];
			$interv = $this->createIssnIntervention($titre, $row['issne']);
			if ($interv->accept()) {
				$this->insertLog[] = $row;
			} else {
				$error = new InsertError();
				$error->position = (int) $row['position'];
				$error->issne = $row['issne'];
				$error->issnp = $row['issnp'];
				$helper = new \processes\intervention\HtmlHelper($interv);
				$error->error = json_encode($helper->errors(), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
				$this->errorsLog[] = $error;
			}
		}
		return count($this->insertLog);
	}

	private function createIssnIntervention(Titre $titre, string $issne): Intervention
	{
		$issnRecord = new Issn('import');
		$issnRecord->issn = $issne;
		if ($this->querySudocApi) {
			$issnRecord->fillBySudoc();
		}
		$issnRecord->titreId = (int) $titre->id;
		if (empty($issnRecord->support) || $issnRecord->support === Issn::SUPPORT_INCONNU) {
			$issnRecord->support = Issn::SUPPORT_ELECTRONIQUE;
		}

		$intervention = self::createIntervention($titre);
		$intervention->setImport(ImportType::getSourceId('issn.org'));
		$intervention->contenuJson->create($issnRecord);
		return $intervention;
	}

	private static function createIntervention(Titre $titre): Intervention
	{
		$i = new Intervention();
		$i->setAttributes(
			[
				'ressourceId' => null,
				'revueId' => $titre->revueId,
				'titreId' => $titre->id,
				'editeurId' => null,
				'statut' => 'attente',
				'import' => ImportType::getSourceId('issn.org'),
				'ip' => '',
				'contenuJson' => new \InterventionDetail(),
				'suivi' => null !== \Suivi::isTracked($titre),
				'description' => "Modification du titre « {$titre->titre} »",
			],
			false
		);
		return $i;
	}
}
