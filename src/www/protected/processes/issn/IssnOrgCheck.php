<?php

namespace processes\issn;

class IssnOrgCheck
{
	private const DELIMITER = "\t";

	public int $verbose = 0;

	/**
	 * @var resource
	 */
	private $output;

	/**
	 * @param null|resource $output
	 */
	public function __construct($output = null)
	{
		if ($output === null) {
			$this->output = fopen("php://memory", 'w');
		} else {
			if (!is_resource($output)) {
				throw new \Exception("PHP resource required");
			}
			$this->output = $output;
		}
	}

	public function addHeader(): void
	{
		fputcsv($this->output, ["titreId", "message", "issnl", "issn"], self::DELIMITER, '"', '\\');
	}

	public function getOutput(): string
	{
		return (string) stream_get_contents($this->output, -1, 0);
	}

	/**
	 * Recherche les titres de Mirabel dans la base ISSN en utilisant leur ISSN-L.
	 * Liste les données manquant dans Mirabel.
	 */
	public function check(): void
	{
		$issnlImport = new IssnlImport();
		$cmd = \Yii::app()->db
			->createCommand(
				"SELECT titreId, issn, issnl FROM Issn"
				. " WHERE issnl IS NOT NULL AND issnl <> '' AND statut = :s"
			)->setFetchMode(\PDO::FETCH_ASSOC);
		foreach ($cmd->query([':s' => \Issn::STATUT_VALIDE]) as $row) {
			$matchingIssnList = $issnlImport->searchByIssnl($row['issnl']);
			if ($matchingIssnList) {
				if (empty($row['issn'])) {
					$row['issn'] = join(', ', $matchingIssnList);
					$this->addRow($row, 'ISSN-L sans ISSN dans Mirabel');
				} elseif (in_array($row['issn'], $matchingIssnList)) {
					if ($this->verbose) {
						$this->addRow($row, 'OK');
					}
				} else {
					self::addRow($row, "ISSN-L trouvé dans issn.org, mais un ISSN associé dans Mirabel n'y est pas associé selon issn.org");
				}
			} elseif ($issnlImport->searchByIssn($row['issnl'])) {
				$this->addRow($row, 'Cet ISSN-L dans Mirabel est un ISSN non-L selon issn.org');
			} else {
				$this->addRow($row, 'ISSN-L présent dans Mirabel mais absent de la base issn.org (ni ISSN ni ISSN-L)');
			}
		}
	}

	private function addRow(array $row, string $msg): void
	{
		fputcsv($this->output, [(int) $row['titreId'], $msg, $row['issnl'], $row['issn']], self::DELIMITER, '"', '\\');
	}
}
