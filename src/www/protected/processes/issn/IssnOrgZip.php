<?php

namespace processes\issn;

class IssnOrgZip
{
	public int $verbose = 0;

	private string $lastError = '';

	public function getLastError(): string
	{
		return $this->lastError;
	}

	public function getFileTime(): int
	{
		$path = IssnlImport::getDirectory();
		$targetFileName = "$path/issntables.zip";
		if (!file_exists($targetFileName)) {
			return 0;
		}
		return stat($targetFileName)['mtime'];
	}

	public function downloadIssnorgZip(bool $backup): bool
	{
		$path = rtrim(IssnlImport::getDirectory(), '/');

		$targetFileName = "$path/issntables.zip";
		if ($backup && file_exists($targetFileName)) {
			if (file_exists("$path/issntables.OLD.zip")) {
				unlink("$path/issntables.OLD.zip");
			}
			rename($targetFileName, "$path/issntables.OLD.zip");
		}
		$fh = @fopen($targetFileName, "w");
		if (!$fh) {
			$this->lastError = "Impossible de créer ou modifier le fichier en écriture !";
			return false;
		}

		// download
		$curl = new \components\Curl();
		$curl->setopt(\CURLOPT_SSL_VERIFYHOST, false);
		$curl->setopt(\CURLOPT_SSL_VERIFYPEER, false);
		$curl->setopt(\CURLOPT_TIMEOUT, 120);
		$curl->getFile('https://www.issn.org/wp-content/uploads/2014/03/issnltables.zip', $fh);
		$code = $curl->getHttpCode();
		if ($code < 200 || $code >= 300) {
			$this->lastError = "Permission refusée ? Code HTTP $code";
			return false;
		}
		$size = filesize($targetFileName);
		if ($size < 1000000) {
			$this->lastError = "Le zip pèse moins de 1Mo.";
			return false;
		}
		if ($this->verbose > 0) {
			fprintf(STDERR, "INFO: issnltables.zip téléchargé\n");
		}

		// extract
		if (extension_loaded('zip')) {
			$zip = new \ZipArchive;
			if ($zip->open($targetFileName) !== true) {
				$this->lastError = "L'ouverture du zip a échoué.";
				return false;
			}
			chdir($path);
			if ($zip->extractTo(".") === false) {
				$this->lastError = "Zip error: " . $zip->getStatusString();
				return false;
			}
			$zip->close();
			unset($zip);
		} else {
			chdir($path);
			system("unzip issntables.zip");
		}

		// rename files
		$issnFile = glob("$path/*ISSN-to-ISSN-L.txt");
		$issnlFile = glob("$path/*ISSN-L-to-ISSN.txt");
		if (count($issnFile) !== 1 || count($issnlFile) !== 1) {
			$this->lastError = "Impossible de trouver les fichiers txt des ISSN.";
			return false;
		}
		rename($issnFile[0], IssnlImport::FILE_ISSN_ISSNL);
		rename($issnlFile[0], IssnlImport::FILE_ISSNL_ISSN);
		if (is_file("$path/issnl.sqlite")) {
			unlink("$path/issnl.sqlite");
		}
		if ($this->verbose > 0) {
			fprintf(
				STDERR,
				"INFO: Fichiers dans '%s/' :\n - ISSN vers ISSNL : '%s'\n - ISSNL vers ISSN : '%s'\n",
				$path, IssnlImport::FILE_ISSN_ISSNL, IssnlImport::FILE_ISSNL_ISSN
			);
		}
		return true;
	}

	public function restoreZip(): bool
	{
		$path = IssnlImport::getDirectory();
		if (file_exists("$path/issntables.OLD.zip")) {
			return rename("$path/issntables.OLD.zip", "$path/issntables.zip");
		}
		return false;
	}
}
