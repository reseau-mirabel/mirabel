<?php

namespace processes\issn;

use models\import\ImportType;

class IssnOrgFromFile
{
	private const DELIMITER = "\t";

	public bool $simulation = false;

	public int $verbose = 0;

	/**
	 * @var resource
	 */
	private $output;

	/**
	 * @param null|resource $output
	 */
	public function __construct($output = null)
	{
		if ($output === null) {
			$this->output = fopen("php://memory", 'w');
		} else {
			if (!is_resource($output)) {
				throw new \Exception("PHP resource required");
			}
			$this->output = $output;
		}
	}

	public function addHeader(): void
	{
		$this->addCsvRow("titreId", "message", "issnl", "issn");
	}

	public function getOutput(): string
	{
		return (string) stream_get_contents($this->output, -1, 0);
	}

	public function fillIssnl(): void
	{
		$issnlImport = new IssnlImport();
		$cmd = \Yii::app()->db
			->createCommand(
				"SELECT issn, titreId FROM Issn "
				. "WHERE issn IS NOT NULL AND issnl IS NULL AND statut = " . \Issn::STATUT_VALIDE
			)->setFetchMode(\PDO::FETCH_ASSOC);
		foreach ($cmd->query() as $row) {
			$issn = $row['issn'];
			$issnl = $issnlImport->searchByIssn($issn);
			if ($issnl) {
				$issnRecord = \Issn::model()->findBySql(
					"SELECT * FROM Issn WHERE issn = :issn",
					[':issn' => $issn]
				);
				$titre = \Titre::model()->findByPk($issnRecord->titreId);
				assert($titre instanceof \Titre);
				$intervention = $this->createIntervention($titre);
				$intervention->contenuJson->updateByAttributes($issnRecord, ['issnl' => $issnl]);
				if (!$intervention->accept()) {
					$errors = self::formatErrors($intervention->getErrors());
					$this->addCsvRow($row['titreId'], "ERREUR lors de l'ajout d'ISSN-L : $errors", $issnl, $issn);
				}
				$this->addCsvRow($row['titreId'], "ISSNL ajouté (MàJ)", $issnl, $issn);
			} else {
				$this->addCsvRow($row['titreId'], 'ISSN absent du fichier importé', "", $issn);
			}
		}
	}

	public function fillIssn(): void
	{
		$issnlImport = new IssnlImport();
		$cmd = \Yii::app()->db
			->createCommand(<<<SQL
				SELECT issnl, GROUP_CONCAT(issn SEPARATOR ',') AS issns, titreId
				FROM Issn
				WHERE issnl IS NOT NULL AND issnl <> ''
				GROUP BY issnl
				SQL
			)->setFetchMode(\PDO::FETCH_ASSOC);
		foreach ($cmd->query() as $row) {
			$issnl = $row['issnl'];
			$issns = array_filter(explode(",", $row['issns']));
			$matchingIssnList = $issnlImport->searchByIssnl($issnl);
			if (!$matchingIssnList) {
				if ($this->verbose > 1) {
					$this->addCsvRow('', 'ISSNL absent du fichier importé', $issnl, "");
				}
				continue;
			}
			foreach ($matchingIssnList as $importedIssn) {
				if (in_array($importedIssn, $issns, true)) {
					// nothing to update
					continue;
				}
				if (count($matchingIssnList) > 1) {
					$this->addCsvRow($row['titreId'], "Cet ISSN-L a plusieurs ISSN. Insertion d'ISSN impossible.", $issnl, $importedIssn);
					continue;
				}
				$titre = \Titre::model()->findByPk($row['titreId']);
				$intervention = $this->createIntervention($titre);
				$issnRecord = \Issn::model()->findBySql(
					"SELECT * FROM Issn WHERE issnl = :issnl AND (issn = '' OR issn IS NULL)",
					[':issnl' => $issnl]
				);
				if ($issnRecord) {
					$newRecord = clone ($issnRecord);
					$newRecord->issn = $importedIssn;
					$newRecord->support = \Issn::SUPPORT_INCONNU;
					$intervention->contenuJson->update($issnRecord, $newRecord);
					$this->addCsvRow($row['titreId'], "ISSN ajouté (MàJ id {$issnRecord->id})", $issnl, $importedIssn);
				} else {
					$newRecord = new \Issn();
					$newRecord->titreId = $titre->id;
					$newRecord->issn = $importedIssn;
					$newRecord->statut = \Issn::STATUT_VALIDE;
					$newRecord->support = \Issn::SUPPORT_INCONNU;
					$newRecord->issnl = $importedIssn;
					$intervention->contenuJson->create($newRecord);
					$this->addCsvRow($row['titreId'], "ISSN ajouté (nouvelle entrée)", $issnl, $importedIssn);
				}
				if ($this->simulation) {
					continue;
				}
				try {
					if (!$intervention->accept()) {
						$this->addCsvRow($row['titreId'], "ERREUR lors de l'ajout d'ISSN : " . self::formatErrors($intervention->getErrors()), $issnl, $importedIssn);
					}
				} catch (\Exception $e) {
					$this->addCsvRow($row['titreId'], "ERREUR fatale lors de l'ajout d'ISSN. {$e->getMessage()}", $issnl, $importedIssn);
				}
			}
		}
	}

	private function addCsvRow(...$data): void
	{
		if (count($data) !== 4) {
			throw new \Exception("Requires 4 arguments");
		}
		fputcsv($this->output, $data, self::DELIMITER, '"', '\\');
	}

	private function createIntervention(\Titre $titre): \Intervention
	{
		$i = new \Intervention();
		$i->setAttributes(
			[
				'ressourceId' => null,
				'revueId' => (int) $titre->revueId,
				'titreId' => (int) $titre->id,
				'editeurId' => null,
				'statut' => 'attente',
				'ip' => '',
				'contenuJson' => new \InterventionDetail(),
				'suivi' => null !== \Suivi::isTracked($titre),
				'description' => "Modification du titre « {$titre->titre} »",
			],
			false
		);
		$i->setImport(ImportType::getSourceId('issn.org'));
		return $i;
	}

	private static function formatErrors(array $errors): string
	{
		if (!$errors) {
			return "";
		}
		$formatted = [];
		foreach ($errors as $k => $v) {
			$attrErrors = strip_tags(preg_replace('/[\s\n]+/', " ", join(" + ", $v)));
			$formatted[] = "[$k] $attrErrors";
		}
		return join(" / ", $formatted);
	}
}
