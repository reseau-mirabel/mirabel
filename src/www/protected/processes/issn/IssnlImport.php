<?php

namespace processes\issn;

use PDO;

/**
 * Imports data from a issn.org zip file into a special sqlite DB.
 */
class IssnlImport
{
	public const FILE_ISSNL_ISSN = 'ISSN-L_to_ISSN.txt';

	public const FILE_ISSN_ISSNL = 'ISSN_to_ISSN-L.txt';

	private const CSV_SEPARATOR = "\t";

	private const CODES_EXCLUDED = ['ca', 'ck', 'cb', 'cm', 'cc', 'co', 'cd', 'ce', 'cf', 'ch', 'cj', 'ha', 'hg', 'hb', 'hh', 'hc', 'hj', 'hd', 'hu', 'he', 'hz', 'hf', 'h|', 'vd'];

	public $verbose;

	private PDO $pdo;

	private string $filepath;

	public function __construct()
	{
		$this->filepath = self::getDirectory();
		$this->pdo = new PDO('sqlite:' . $this->filepath . 'issnl.sqlite');
	}

	public static function rebuildIfMissing(): void
	{
		$path = self::getDirectory();
		$dbfile = "{$path}issnl.sqlite";
		if (is_file($dbfile) && filesize($dbfile) < 1024*1024) {
			unlink($dbfile);
		}
		if (!is_file($dbfile)) {
			$new = new self;
			$new->import();
		}
	}

	public static function getDirectory(): string
	{
		$path = \Yii::app()->runtimePath . '/issn.org/';
		if (!is_dir($path)) {
			mkdir($path, 0770);
		}
		if ((fileperms($path) & 070) !== 070) {
			@chmod($path, 0770);
		}
		return $path;
	}

	/**
	 * Import the CSV files into the sqlite DB (used as a K/V store).
	 */
	public function import(): void
	{
		$this->pdo->exec("PRAGMA synchronous = OFF");
		$this->pdo->exec("PRAGMA journal_mode = WAL");
		$this->pdo->exec("DROP TABLE IF EXISTS issnl");
		$this->pdo->exec("DROP TABLE IF EXISTS issn");
		$this->pdo->exec("CREATE TABLE issnl (k TEXT, v TEXT) STRICT");
		$this->pdo->exec("CREATE TABLE issn (k TEXT, v TEXT) STRICT");
		$this->insertCsvAsKeyValue($this->filepath . self::FILE_ISSNL_ISSN, 'issnl', true);
		$this->insertCsvAsKeyValue($this->filepath . self::FILE_ISSN_ISSNL, 'issn', false);
		$this->pdo->exec("CREATE INDEX issnlk ON issnl (k)");
		$this->pdo->exec("CREATE INDEX issnk ON issn (k)");
		$this->pdo->exec("PRAGMA wal_checkpoint");
	}

	/**
	 * Return the list of issn which are linked to this issnl.
	 *
	 * @param string $issnl
	 * @return array|null Array of ISSN (string), or null if the issnl was not found
	 */
	public function searchByIssnl(string $issnl): ?array
	{
		$search = $this->pdo->prepare("SELECT v FROM issnl WHERE k = :k");
		$search->execute([':k' => $issnl]);
		$value = $search->fetch(PDO::FETCH_ASSOC);
		if ($value) {
			$issns = explode("\t", (string) $value['v']);
			$validate = $this->pdo->prepare("SELECT k FROM issn WHERE k IN ('" . join("','", $issns) . "')");
			$validate->execute();
			return $validate->fetchAll(PDO::FETCH_COLUMN);
		}
		return null;
	}

	/**
	 * Return the issnl which is linked to this issn.
	 *
	 * @return string|null ISSN-L, or null if the issn was not found
	 */
	public function searchByIssn(string $issn): ?string
	{
		$search = $this->pdo->prepare("SELECT v FROM issn WHERE k = :k");
		$search->execute([':k' => $issn]);
		$issnl = $search->fetchColumn();
		return ($issnl ? (string) $issnl : null);
	}

	protected function insertCsvAsKeyValue(string $filename, string $table, bool $mergeColumns): void
	{
		if (!is_readable($filename)) {
			throw new \Exception("$filename n'est pas lisible.");
		}
		if ($this->verbose > 0) {
			fprintf(STDERR, "Import de $filename en cours...\n");
		}
		$handle = fopen($filename, 'r');
		if (!$handle) {
			throw new \Exception("$filename n'est pas accessible.");
		}

		$header = fgetcsv($handle, 1024, self::CSV_SEPARATOR);
		if (!$header) {
			throw new \Exception("En-tête CSV absent de $filename.");
		}

		$insert = $this->pdo->prepare("INSERT INTO $table VALUES (?, ?)");
		while (($data = fgetcsv($handle, 1024, self::CSV_SEPARATOR)) !== false) {
			$code = $data[2] ?? '';
			$key = array_shift($data);
			if (in_array($code, self::CODES_EXCLUDED)) {
				// Do not insert the ISSN with this support,
				continue;
			}
			if ($mergeColumns) {
				$value = join("\t", $data);
			} else {
				$value = $data[0];
			}
			if ($key) {
				$insert->execute([$key, $value]);
			}
		}
		fclose($handle);
	}
}
