<?php

/** @var StatsController $this */
/** @var processes\stats\LiensScienceOuverte $liens */
/** @var array $ratio */
/** @var \processes\stats\scienceouverte\ResultatParPays[] $paysDoaj */
/** @var \processes\stats\scienceouverte\ResultatParPays[] $paysDoajSceau */
/** @var \processes\stats\scienceouverte\ResultatParPays[] $paysDoajSansApc */
/** @var array $numsDoajFrance */

assert($this instanceof Controller);

$this->pageTitle = Yii::app()->name . ' - Indicateurs Science ouverte';
$this->breadcrumbs = [
	"Indicateurs" => ['/stats/index'],
	"Science ouverte au " . date('d/m/Y'),
];
?>

<h1>Indicateurs science ouverte</h1>

<div<?= $this->localhostConnection ? ' class="localhost-connection"' : '' ?>>

<h2>Globalement</h2>

<section>
	<h3>Noms des liens</h3>
	<table class="table table-striped table-bordered table-condensed exportable">
		<thead>
			<tr>
				<th>Source des liens</th>
				<th>Nombre de titres dans M</th>
				<th>Nombre de revues</th>
				<th>Nombre de titres importables</th>
				<th>Ratio titres</th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach ($liens->names as $x) {
				$name = $x['name'];
				$source = Sourcelien::model()->findByAttributes(['nom' => $name]);
				echo "<tr>";
				echo CHtml::tag('td', [], CHtml::encode($name));
				echo CHtml::tag('td', [], CHtml::link($x['num'], ['stats/liens', 'name' => $x['name']]));
				echo CHtml::tag('td', [], CHtml::link($x['numRevues'], ['/revue/search', 'q' => ['lien' => $source->id]]));
				echo CHtml::tag('td', [], $ratio[$name]['total']);
				echo CHtml::tag('td', [], $ratio[$name]['ratio']);
				echo "</tr>\n";
			}
			?>
		</tbody>
	</table>
</section>

<section>
	<h3>Domaines des URL</h3>
	<table class="table table-striped table-bordered table-condensed exportable">
		<thead>
			<tr>
				<th>Domaine</th>
				<th>Nombre de titres</th>
				<th>Nombre de revues</th>
				<th title="Nombre de titres pour lesquels ce domaine est déclaré avec une source connue">
					<abbr title="Nombre de titres pour lesquels ce domaine est déclaré avec une source connue">Identifiés</abbr>
				</th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach ($liens->domains as $x) {
				echo "<tr>";
				$source = Sourcelien::identifyUrl($x['domain']);
				// Domaine
				echo CHtml::tag('td', [], $x['domain'] . ($source === null ? "" : $source->getLogo()));
				// Nombre de titres
				echo CHtml::tag('td', [], CHtml::link($x['num'], ['stats/liens', 'domain' => $x['domain']]));
				// Nombre de revues
				echo CHtml::tag('td', [], $x['numRevues']);
				// Identifiés
				if ($source !== null && $x['numIdentif'] > 0) {
					echo CHtml::tag(
						'td',
						['class' => ($x['numIdentif'] === $x['num'] ? '' : 'danger')],
						$x['numIdentif']
					);
				} else {
					echo "<td></td>";
				}
				echo "</tr>\n";
			}
			?>
		</tbody>
	</table>
</section>

<section>
	<h2>Par pays</h2>
	<p>
		Pour les ISSN, la colonne <em>France</em> compte les titres dont au moins un ISSN est rattaché à la France.
		Pour les éditeurs, la colonne <em>France</em> compte les titres dont au moins un éditeur actuel est situé en France.
		Les autres pays, ainsi que les organisations internationales, les  pays multiples et indéterminés ("ZZ" en MARC), sont comptés dans <em>Autres</em>.
	</p>
	<?= $this->renderPartial('_so_table-par-pays', ['titreCellule' => "Titres de DOAJ", 'data' => $paysDoaj, 'france' => $numsDoajFrance]) ?>
	<?= $this->renderPartial('_so_table-par-pays', ['titreCellule' => "Titres DOAJ avec sceau", 'data' => $paysDoajSceau]) ?>
	<?= $this->renderPartial('_so_table-par-pays', ['titreCellule' => "Titres DOAJ sans APC", 'data' => $paysDoajSansApc]) ?>
</section>

</div>
