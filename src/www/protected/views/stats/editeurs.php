<?php

use processes\stats\Editeurs;

/** @var StatsController $this */

assert($this instanceof \Controller);
$countAll = Editeurs::count();

$this->pageTitle = sprintf("Indicateurs des %d éditeurs", $countAll);
$this->breadcrumbs = [
	"Indicateurs" => ['/stats'],
	$this->pageTitle,
];
?>

<div<?= $this->localhostConnection ? ' class="localhost-connection"' : '' ?>>

	<h1><?= CHtml::encode($this->pageTitle) ?> au <?= date('d/m/Y') ?></h1>

<section id="pays">
	<h2>Principaux pays</h2>

	<?= Editeurs::countByCountry()
		->addClass('exportable sortable')
		->setHtmlAttributes(['id' => 'editeurs-par-pays'])
		->toHtml(false) ?>
</section>

<section id="idref">
	<h2>Nombre d'IdRef</h2>

	<?php
	$countIdref = Editeurs::countHavingIdref();
	echo $countIdref
		->addClass('exportable sortable')
		->setHtmlAttributes(['id' => 'editeurs-idref'])
		->toHtml(false)
	?>
</section>

<section id="revues">
	<h2>Nombre de revues</h2>

	<?= Editeurs::countByRevues()->addClass('exportable')->toHtml(false) ?>

	<?= Editeurs::getEditeursMaxRevues()->addClass('exportable sortable')->toHtml(false) ?>
</section>

<section id="liens">
	<?= $this->renderPartial('_liens-editeurs', ['liens' => Editeurs::listSourcesFrequencies()])?>
</section>

</div>
