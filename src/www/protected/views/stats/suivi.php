<?php

/** @var StatsController $this */
/** @var \processes\stats\Suivi $suivi */

assert($this instanceof Controller);

$this->pageTitle = Yii::app()->name . " - État des suivis";
$this->breadcrumbs = [
	"Indicateurs" => ['/stats/index'],
	"État des suivis",
];
?>

<div class="<?= $this->localhostConnection ? 'localhost-connection' : '' ?>">

	<h1>État des suivis au <?= date('d/m/Y') ?></h1>

	<?php
	$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	[
		'id' => 'suivis-grid',
		'dataProvider' => $suivi->provider,
		'filter' => null,
		'columns' => [
			[
				'name' => 'nom',
				'header' => 'Partenaire',
				'type' => 'raw',
				'value' => function ($data) {
					return CHtml::link(
						CHtml::encode($data['nom']),
						['/partenaire/view', 'id' => $data['id']],
						['title' => 'statut ' . $data['statut']]
					);
				},
				'cssClassExpression' => function ($rank, array $data) {
					return "partenaire statut-{$data['statut']}";
				},
			],
			[
				'name' => 'type',
				'header' => 'Type',
				'type' => 'raw',
				'value' => function ($data) {
					$statut = ($data['statut'] === 'actif' ? '' : $data['statut']);
					if ($data['type'] === 'normal') {
						return $statut;
					}
					return trim($statut . " " . Partenaire::$enumType[$data['type']]);
				},
			],
			'derConnexion:date:Der Connexion',
			'nomComplet:text:Nom',
			'verifRecente:date:Vérif. + récente',
			'verifAncienne:date:Vérif. + ancienne',
			'verifNon:text:Revues non vérifiées',
			'revuesSuivies:text:Revues suivies',
			'ressourcesSuivies:text:Ressources suivies',
			[
				'header' => "#possessions",
				'type' => 'text',
				'value' => function (array $data) use ($suivi) {
					return isset($suivi->possessions[$data['id']]) ? (int) $suivi->possessions[$data['id']] : '';
				},
			],
			[
				'header' => "#abonnements",
				'type' => 'text',
				'value' => function (array $data) use ($suivi) {
					return isset($suivi->abonnements[$data['id']]) ? (int) $suivi->abonnements[$data['id']] : '';
				},
			],
			[
				'header' => "#ressources masquées",
				'type' => 'text',
				'value' => function (array $data) use ($suivi) {
					return isset($suivi->masques[$data['id']]) ? (int) $suivi->masques[$data['id']] : '';
				},
			],
		],
		'ajaxUpdate' => false,
		'itemsCssClass' => 'exportable',
		'rowHtmlOptionsExpression' => function(int $index, array $data) {
			return ['class' => "partenaire-{$data['id']}"];
		},
	]
);
	?>
</div>
