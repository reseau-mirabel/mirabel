<?php

/** @var Controller $this */
/** @var string $titrePage */
/** @var array $liensTitre */
/** @var array $liensEditeur */

assert($this instanceof Controller);

$this->pageTitle = Yii::app()->name . ' - Sélection de liens';

$this->breadcrumbs = !Yii::app()->user->checkAccess("avec-partenaire") ? ['Indicateurs' => '#'] : ['Indicateurs' => ['index']];
$this->breadcrumbs[] = 'Sélection de liens';
?>

<h1>Sélection de liens</h1>

<h2><?= $titrePage ?></h2>

<h3><?= count($liensTitre) ?> dans les revues</h3>
<table class="table table-striped table-bordered table-condensed exportable">
	<thead>
		<tr>
			<th>Revue</th>
			<th>Nom du lien</th>
			<th>URL</th>
			<th style="display: none">URL M</th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($liensTitre as $x) {
			// Each row is an array with keys revueId, titre, name, domain, url
			echo '<tr>'
				, '<td>', CHtml::link(CHtml::encode($x['titre']), ['revue/view', 'id' => $x['revueId']]), '</td>'
				, '<td>', CHtml::encode($x['name']), '</td>'
				, '<td>', CHtml::link(CHtml::encode($x['url']), $x['url']), '</td>'
				, '<td style="display: none">', CHtml::encode(Yii::app()->params->itemAt('baseUrl') . "/revue/{$x['revueId']}"), '</td>'
				, '</tr>';
		}
		?>
	</tbody>
</table>

<h3><?= count($liensEditeur) ?> dans les éditeurs</h3>
<table class="table table-striped table-bordered table-condensed exportable">
	<thead>
		<tr>
			<th>Éditeur</th>
			<th>Nom du lien</th>
			<th>URL</th>
			<th style="display: none">URL M</th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($liensEditeur as $x) {
			// Each row is an array with keys editeurId, nom, name, domain, url
			echo '<tr>'
				, '<td>', CHtml::link(CHtml::encode($x['nom']), ['editeur/view', 'id' => $x['editeurId']]), '</td>'
				, '<td>', CHtml::encode($x['name']), '</td>'
				, '<td>', CHtml::link(CHtml::encode($x['url']), $x['url']), '</td>'
				, '<td style="display: none">', CHtml::encode(Yii::app()->params->itemAt('baseUrl') . "/editeur/{$x['editeurId']}"), '</td>'
				, '</tr>';
		}
		?>
	</tbody>
</table>
