<?php

/** @var Controller $this */
/** @var models\forms\StatsForm $userData */
/** @var processes\stats\Activite $statsActivite */

$this->pageTitle = Yii::app()->name . " - Indicateurs d'activité";
$this->breadcrumbs = [
	"Indicateurs" => ['/stats/index'],
	$statsActivite->partenaire->getFullName() => ['/partenaire/view', 'id' => $statsActivite->partenaire->id],
	"admin" => ['view-admin', 'id' => $statsActivite->partenaire->id],
	"Indicateurs d'activité - " . $statsActivite->partenaire->getShortName(),
];
?>

<h1>Indicateurs d'activité — <?= CHtml::encode($statsActivite->partenaire->nom) ?></h1>

<h2>Suivi</h2>

<p>
	Consulter le <?= CHtml::link("tableau de bord", ['/partenaire/tableau-de-bord', 'id' => $statsActivite->partenaire->id]) ?> détaillé.
</p>

<?= $statsActivite->getSuivi()->addClass('exportable')->toHtml() ?>

<h2>Interventions<?= $statsActivite->getIntervalMessage() ?></h2>

<div>
	<?php
	$form = $this->beginWidget(
		'BootActiveForm',
		[
			'id' => 'interventions-count-form',
			'type' => BootActiveForm::TYPE_INLINE,
			'enableClientValidation' => true,
			'method' => 'GET',
		]
	);
	/** @var BootActiveForm $form */
	echo $form->errorSummary($userData);
	echo "Période du "
		. $form->textField($userData, 'startDate', ['class' => 'input-small'])
		. " au "
		. $form->textField($userData, 'endDate', ['class' => 'input-small']);
	?>
	<button type="submit" class="btn">OK</button>
	<?php $this->endWidget(); ?>
</div>

<section>
	<h3>Interventions validées</h3>
	<p>
		Décompte des interventions validées pendant cette période.
	</p>
	<?= $statsActivite->countInterventions()->addClass('exportable')->toHtml() ?>
</section>

<section>
	<h3>Détail des propositions par statut</h3>
	<p>
		La répartition se fait selon le statut <em>actuel</em> de ces interventions.
		La sélection d'une période ne détermine que le choix des interventions qui ont surgi dans cet intervalle.
		Les interventions par import ne figurent pas dans ce tableau.
	</p>
	<?= $statsActivite->getPropParStatut()->addClass('exportable')->toHtml() ?>
</section>

<section>
	<h3>Détail des validations par type d'action</h3>
	<p>
		La sélection d'une période restreint aux interventions qui ont été validées dans cet intervalle.
		Les interventions par import ne figurent pas dans ce tableau.
	</p>
	<?= $statsActivite->getValParType()->addClass('exportable')->toHtml() ?>
</section>

<section>
	<h3>Activité individuelle</h3>
	<p>
		Pour chaque utilisateur, on indique le nombre de propositions et de validations sur cette période,
		sans tenir compte des imports.
		Une modification directe compte pour une proposition et une validation.
	</p>
	<?= $statsActivite->countPropPerUser()->addClass('exportable')->toHtml() ?>
</section>
