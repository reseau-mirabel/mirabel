<?php

/** @var Controller $this */
/** @var array $pdfs */
assert($this instanceof Controller);

$this->breadcrumbs = [
	'Indicateurs' => ['/stats'],
	'Historique',
];
?>

<h1>Historique des indicateurs</h1>

<ol>
<?php
if (empty($pdfs)) {
	echo "<p>Aucun fichier d'historique n'est présent.</p>";
} else {
	foreach ($pdfs as $pdf) {
		echo "<li>"
			. CHtml::link(
				CHtml::encode(str_replace('_', ' ', $pdf)),
				['pdf', 'file' => $pdf]
			)
			. "</li>\n";
	}
}
?>
</ol>
