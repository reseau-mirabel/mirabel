<?php

/** @var Controller $this */
/** @var object $liens */

use components\HtmlTable;

assert($this instanceof Controller);
?>

<h2>Liens externes</h2>

<p>
	Cette section concerne les autres liens externes pour les éditeurs.
	Les données sont recalculées toutes les heures.
</p>

<h3>Noms des liens</h3>
<p>
	Tous les noms apparaissant au moins 2 fois parmi les <em>autres liens</em> d'éditeurs.
	La casse est ignorée.
</p>
<?php
echo HtmlTable::build(
	array_map(
		function ($x) {
			$x['num'] = CHtml::link($x['num'], ['stats/liens', 'name' => $x['name']]);
			return $x;
		},
		$liens->names
	),
	["Nom", "Nombre d'éditeurs"]
)->addClass('exportable liens-noms')->toHtml();
?>

<h3>Domaines des URL</h3>
<p>
	Tous les domaines apparaissant au moins 2 fois.
	Les sous-domaines <code>www.</code> sont ignorés.
</p>

<table class="table table-striped table-bordered table-condensed exportable liens-domaines">
	<thead>
		<tr>
			<th>Domaine</th>
			<th>Nombre d'éditeurs</th>
			<th>Identifiés</th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($liens->domains as $x) {
			echo CHtml::tag(
				'tr',
				($x['domain'] == 'interne à Mir@bel' ? ['class' => 'info'] : [])
			);
			$source = Sourcelien::identifyUrl($x['domain']);
			// Domaine
			echo CHtml::tag('td', [], $x['domain'] . ($source === null ? "" : $source->getLogo()));
			// Nombre d'éditeurs
			echo CHtml::tag('td', [], CHtml::link($x['num'], ['stats/liens', 'domain' => $x['domain']]));
			// Identifiés
			echo CHtml::tag('td', [], $source ? 'oui' : 'non');
			echo "</tr>\n";
		}
?>
	</tbody>
</table>
