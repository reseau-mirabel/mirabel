<?php

use components\HtmlHelper;
use components\HtmlTable;
use models\forms\StatsForm;
use processes\stats\Contenu;

/** @var StatsController $this */
/** @var StatsForm $userData */
/** @var Contenu $stats */
/** @var processes\stats\Liens $liens */

assert($this instanceof Controller);

$this->pageTitle = Yii::app()->name . ' - Indicateurs';
$this->breadcrumbs = [
	'Indicateurs',
];
?>

<?php if (Yii::app()->user->checkAccess("avec-partenaire")) { ?>
<div class="lateral-menu-small">
	<ul class="nav nav-tabs nav-stacked">
		<li class="active"><?= CHtml::link("Indicateurs") ?></li>
		<li><?= CHtml::link("… d'activité (Mir@bel)", ['/stats/interventions']) ?></li>
		<li><?= CHtml::link("… d'activité (mon partenaire)", ['activite', 'partenaireId' => Yii::app()->user->partenaireId]) ?></li>
		<li><?= CHtml::link("… d'éditeurs", ['/stats/editeurs']) ?></li>
		<li><?= CHtml::link("… de politiques", ['/stats/politiques']) ?></li>
		<?php if (Yii::app()->user->checkAccess('stats/suivi')) { ?>
		<li><?= CHtml::link("… de suivi", ['/stats/suivi']) ?></li>
		<li><?= CHtml::link("… de science ouverte", ['/stats/science-ouverte']) ?></li>
		<?php } ?>
	</ul>
</div>
<?php } ?>

<div<?= $this->localhostConnection ? ' class="localhost-connection"' : '' ?> >

<h1>Indicateurs de contenu</h1>

<h2 style="clear:right">Contenu actuel <em>(<?= date('Y-m-d'); ?>)</em></h2>

<div class="span12" style="margin-bottom:3ex">
<div class="row">
	<div class="span4"><?= $stats->getMainInfoRevues()->addClass('exportable')->toHtml() ?></div>
	<div class="span4"><?= $stats->getMainInfoRessources()->addClass('exportable')->toHtml() ?></div>
	<div class="span4"><?= $stats->getMainInfo()->addClass('exportable')->toHtml() ?></div>
</div>
</div>

<h3>Ressources par type</h3>
<?php
echo HtmlTable::build($stats->getRessourcesParType(), null)->addClass('exportable')->toHtml();
?>

<h3>Ressources par nombre de revues/titres/accès</h3>
<?php
echo HtmlHelper::table($stats->getRessourcesParAcces(20), 'exportable');
?>

<h3>Ressources avec import automatique</h3>
<?php
echo HtmlHelper::table($stats->getRessourcesImportees(), 'exportable');
?>

<h3>Revues par accès</h3>
<?php
echo HtmlTable::buildFromAssoc($stats->getRevuesParAcces())->addClass('exportable')->toHtml();
?>

<h3>Accès par type</h3>
<table class="table table-striped table-bordered table-condensed exportable">
	<thead>
		<tr>
			<th>Type d'accès</th>
			<th>Libre</th>
			<th>Restreint</th>
			<th>Rapport libre/restreint</th>
			<th>Total des accès</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$acces = $stats->getAccesParType();
		foreach ($acces as $a) {
			?>
		<tr>
			<td><?php echo CHtml::encode($a['type']); ?></td>
			<td><?php echo CHtml::encode($a['Libre']); ?></td>
			<td><?php echo CHtml::encode($a['Restreint']); ?></td>
			<td><?php echo CHtml::encode($a['Taux']); ?> %</td>
			<td><?php echo CHtml::encode($a['Total']); ?></td>
		</tr>
		<?php
		}
		?>
	</tbody>
</table>

<h3>Éditeurs des revues</h3>
<?php
echo $stats->getEditeursNb()->addClass('exportable')->toHtml();
?>

<h3>Langues des revues</h3>
<p>
	Mir@bel a actuellement <?= $stats->countLangGroups() ?> déclarations distinctes
	pour les langues de revues (par exemple "français" ou "français, anglais, allemand, slave", l'ordre étant significatif)
	et <strong><?= $stats->countLangGroupsUnordered() ?> ensembles de langues</strong> (en ignorant leur ordre).
	Plutôt que d'afficher une liste exhaustive, on se restreint à deux tableaux de synthèse.
	Voir les <?= CHtml::link("indicateurs d'éditeurs", ['/stats/editeurs']) ?> pour des décomptes de revues par pays.
</p>
<p>
	Ces tableaux analysent la langue du titre le plus récent d'une revue —
	contrairement aux pages de recherche dans Mir@bel qui cherchent les langues de
	tous les titres d'une revue.
</p>
<?= $stats->getLangNb()->addClass('exportable')->toHtml() ?>
<?= $stats->getLangInfo()->addClass('exportable')->toHtml() ?>

<?php
$this->renderPartial('_liens-titres', ['liens' => $liens]);
?>

<hr />
<?= CHtml::link('Historique des rapports statistiques', ['history']); ?>
<?php
if (Yii::app()->user->checkAccess("avec-partenaire")) {
	echo " — "
		. CHtml::link("Rapport d'activité de mon partenariat", ['activite', 'partenaireId' => Yii::app()->user->partenaireId]);
}

Yii::import('application.controllers.StatsaccesController');
if (file_exists(StatsaccesController::getStatsPath('', ''))) {
	echo '<p id="statsacces">'
		. CHtml::link("Statistiques de fréquentation du site Mir@bel", ['/statsacces/index'])
		. "</p>";
}
?>

</div>
