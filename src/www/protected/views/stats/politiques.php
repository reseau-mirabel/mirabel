<?php

use components\HtmlTable;

/** @var Controller $this */
/** @var \processes\stats\PolitiqueEditeurs $editeurs */
/** @var \processes\stats\PolitiqueUtilisateurs $utilisateurs */

assert($this instanceof StatsController);

$this->pageTitle = "Indicateurs des politiques au " . date("d/m/Y");
$this->breadcrumbs = [
	'Indicateurs' => ['/stats/index'],
	$this->pageTitle,
];
?>

<h1><?= $this->pageTitle ?></h1>

<section id="politiques-editeurs">
	<h2>Politiques</h2>

	<h3>Par statut</h3>
	<?=	HtmlTable::build($editeurs->byStatus())
		->setColumnHeaders(["Statut", "nb politiques"])
		->addClass('exportable') ?>

	<h3>Par éditeur</h3>
	<p>Les politiques de statut "supprimée" ou "à supprimer" ne sont pas décomptées.</p>
	<?=	HtmlTable::build($editeurs->byPublisher())
		->setColumnHeaders(["Éditeur", "nb politiques", "nb revues avec politiques", "nb revues sans politique"])
		->addClass('exportable')->toHtml(false) ?>

	<h3>Par nombre de titres associés</h3>
	<?php
	$byJournal = $editeurs->byJournal();
	?>
	<p>
		Les politiques de statut "supprimée" ou "à supprimer" ne sont pas décomptées.
		La première ligne du tableau indique que 
		<em><?= $byJournal[0][1] ?> politiques s'appliquent chacune à exactement <?= $byJournal[0][0] ?> titre<?= $byJournal[0][0] > 1 ? "s" : "" ?></em>.
	</p>
	<?=	HtmlTable::build($byJournal)
		->setColumnHeaders(["nb titres", "nb politiques"])
		->addClass('exportable')->toHtml(false) ?>
</section>

<section id="politiques-utilisateurs">
	<h2>Utilisateurs</h2>

	<h3>Total</h3>
	<?=	HtmlTable::buildFromAssoc($utilisateurs->counts(), false)->addClass('exportable')->toHtml(false) ?>

	<h3>Par type de partenaire</h3>
	<p>Nombre d'utilisateurs en charge de déclarer les politiques.</p>
	<?=	HtmlTable::build($utilisateurs->byInstitute())
		->setColumnHeaders(["Type du partenaire", "nb utilisateurs"])
		->addClass('exportable')->toHtml(true) ?>

	<h3>Par éditeur</h3>
	<?=	HtmlTable::build($utilisateurs->byPublisher())
		->setColumnHeaders(["Éditeur", "nb utilisateurs"])
		->addClass('exportable')->toHtml(false) ?>
</section>
