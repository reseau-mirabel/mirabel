<?php

/** @var StatsController $this */
/** @var string $titreCellule */
/** @var \processes\stats\scienceouverte\ResultatParPays[] $data */
/** @var ?array $france */

?>
<table class="table table-striped table-bordered table-condensed exportable">
	<caption><?= $titreCellule ?></caption>
	<thead>
		<tr>
			<th><?= $titreCellule ?></th>
			<th>France</th>
			<th>France<br />(titres vivants)</th>
			<th><abbr title="Titres sans élément lié (ISSN valide ou éditeur actuel) ou dont aucun de ces éléments n'a de pays déclaré">Inconnu</abbr></th>
			<th><abbr title="Titres dont aucun pays n'est la France, et au moins un pays est connu">Autres</abbr></th>
			<th>Total</th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($data as $row) {
			echo "<tr>";
			echo CHtml::tag('td', [], $row->source);
			echo CHtml::tag('td', [], $row->getFrTitresRevues());
			echo CHtml::tag('td', [], $row->getFrVivantTitresRevues());
			echo CHtml::tag('td', [], $row->inconnus);
			echo CHtml::tag('td', [], $row->autres);
			echo CHtml::tag('td', [], ((int) $row->frTitres) + $row->inconnus + $row->autres);
			echo "</tr>\n";
		}
		if (!empty($france)) {
			echo "<tr>";
			echo CHtml::tag('td', [], "Par pays DOAJ");
			echo CHtml::tag('td', [], "{$france['titres']} ⇔ {$france['revues']} revues");
			echo CHtml::tag('td', [], "");
			echo CHtml::tag('td', [], "");
			echo CHtml::tag('td', [], "");
			echo CHtml::tag('td', [], "");
			echo "</tr>\n";
		}
		?>
	</tbody>
</table>
