<?php

/** @var Controller $this */
/** @var object $liens */

use components\HtmlTable;

assert($this instanceof Controller);
?>

<h2>Liens externes</h2>

<p>
	Cette section concerne les autres liens externes pour les titres.
	Les données sont recalculées toutes les heures.
</p>

<h3>Noms des liens</h3>
<p>
	Tous les noms apparaissant au moins 2 fois parmi les <em>autres liens</em> de titres.
	La casse est ignorée.
</p>
<?php
echo HtmlTable::build(
	array_map(
		function ($x) {
			$x['num'] = CHtml::link($x['num'], ['stats/liens', 'name' => $x['name']]);
			return $x;
		},
		$liens->names
	),
	["Nom", "Nombre de titres", "Nombre de revues"]
)->addClass('exportable')->toHtml();
?>

<h3>Domaines des URL</h3>
<p>
	Tous les domaines apparaissant au moins 2 fois.
	Les sous-domaines <code>www.</code> sont ignorés.
	La colonne <em>Identifiés</em> vérifie que le domaine correspond bien à une source identifiée et appliquées à tous les liens concernés.
</p>
<table class="table table-striped table-bordered table-condensed exportable">
	<thead>
		<tr>
			<th>Domaine</th>
			<th>Nombre de titres</th>
			<th>Nombre de revues</th>
			<th>Identifiés</th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($liens->domains as $x) {
			echo CHtml::tag(
				'tr',
				($x['domain'] == 'interne à Mir@bel' ? ['class' => 'info'] : [])
			);
			$source = Sourcelien::identifyUrl($x['domain']);
			// Domaine
			echo CHtml::tag('td', [], $x['domain'] . ($source === null ? "" : $source->getLogo()));
			// Nombre de titres
			echo CHtml::tag('td', [], CHtml::link($x['num'], ['stats/liens', 'domain' => $x['domain']]));
			// Nombre de revues
			echo CHtml::tag('td', [], $x['numRevues']);
			// Identifiés
			if ($source !== null && $x['numIdentif'] > 0) {
				echo CHtml::tag(
					'td',
					['class' => ($x['numIdentif'] === $x['num'] ? '' : 'danger')],
					CHtml::link($x['numIdentif'], ['/revue/search', 'q' => ['lien' => $source->id]])
				);
			} else {
				echo "<td></td>";
			}
			echo "</tr>\n";
		}
		?>
	</tbody>
</table>
