<?php

/** @var StatsController $this */
/** @var \processes\stats\Activite $statsActivite */
/** @var \models\forms\StatsForm $userData */

$this->pageTitle = 'Activité';
$this->breadcrumbs = [
	"Indicateurs" => ['/stats'],
	$this->pageTitle,
];
?>

<div<?= $this->localhostConnection ? ' class="localhost-connection"' : '' ?> >

<h1>Indicateurs d'activité</h1>

<h2>Période</h2>
<div>
	<?php
	$form = $this->beginWidget(
		'BootActiveForm',
		[
			'id' => 'interventions-count-form',
			'type' => BootActiveForm::TYPE_INLINE,
			'enableClientValidation' => true,
			'action' => $this->createUrl('stats/interventions'),
			'method' => 'GET',
		]
	);
	/** @var BootActiveForm $form */
	echo $form->errorSummary($userData);
	echo "Période du "
		. $form->textField($userData, 'startDate', ['class' => 'input-small'])
		. " au "
		. $form->textField($userData, 'endDate', ['class' => 'input-small']);
	?>
	<button type="submit" class="btn">OK</button>
	<?php $this->endWidget(); ?>
</div>

<h2>Interventions</h2>
<section>
	<h3>Interventions validées<?= $statsActivite->getIntervalMessage() ?></h3>
	<p>
		Décompte des interventions validées pendant cette période, qu'elles l'aient été immédiatement
		(par exemple, lors de l'import) ou postérieurement à la proposition.
	</p>
	<?= $statsActivite->countInterventions()->addClass('exportable')->toHtml() ?>
</section>

<section>
	<h3>Nombre d'interventions créées <?= $statsActivite->getIntervalMessage() ?></h3>
	<p>
		Les interventions par import ne figurent pas dans ce tableau.
	</p>
	<?= $statsActivite->getProp()->addClass('exportable')->toHtml() ?>
</section>

<section>
	<h3>Détail par statut des interventions créées <?= $statsActivite->getIntervalMessage() ?></h3>
	<p>
		La répartition se fait selon le statut <em>actuel</em> de ces interventions.
		La sélection d'une période ne détermine que le choix des interventions qui ont surgi dans cet intervalle.
		Les interventions par import ne figurent pas dans ce tableau.
	</p>
	<?= $statsActivite->getPropParStatut()->addClass('exportable')->toHtml() ?>
</section>

<section>
	<h3>Détail des validations par type d'action <?= $statsActivite->getIntervalMessage() ?></h3>
	<p>
		La sélection d'une période restreint aux interventions qui ont été validées dans cet intervalle.
		Les interventions par import ne figurent pas dans ce tableau.
	</p>
	<?= $statsActivite->getValParType()->addClass('exportable')->toHtml() ?>
</section>

<hr />
<?= CHtml::link('Historique des rapports statistiques', ['history']); ?>
<?php
if (Yii::app()->user->checkAccess("avec-partenaire")) {
	echo " — "
		. CHtml::link("Rapport d'activité de mon partenariat", ['activite', 'partenaireId' => Yii::app()->user->partenaireId]);
}
?>

</div>
