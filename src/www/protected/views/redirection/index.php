<?php

/** @var Controller $this */
/** @var \processes\redirection\Display[] $redirEditeur */
/** @var \processes\redirection\Display[] $redirRevue */
/** @var \processes\redirection\Display[] $redirTitre */

assert($this instanceof Controller);

$this->pageTitle = "Redirections";
$this->breadcrumbs = [
	'Admin' => ['/admin/index'],
	'Redirections',
];
?>

<div style="float:right; max-width:30ex;">
	<ul class="well nav nav-list">
		<li class="nav-header">Navigation dans la page</li>
		<li><?= CHtml::link("Éditeurs (" . count($redirEditeur) . ")", "#editeurs") ?></li>
		<li><?= CHtml::link("Revues (" . count($redirRevue) . ")", "#revues") ?></li>
		<li><?= CHtml::link("Titres (" . count($redirTitre) . ")", "#titres") ?></li>
	</ul>
</div>

<h1>Redirections</h1>

<section id="editeurs">
	<h2>Éditeurs</h2>
	<table class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>Source</th><th>Cible</th><th>Nom cible</th><th>Date modification</th>
			</tr>
		</thead>
		<tbody>
		<?php
		foreach ($redirEditeur as $redir) {
			echo "<tr>";
			echo CHtml::tag('td', [], (string) $redir->sourceId);
			echo CHtml::tag('td', [], CHtml::link((string) $redir->cibleId, $redir->getUrl()));
			echo CHtml::tag('td', [], CHtml::link($redir->getName(), $redir->getUrl()));
			echo CHtml::tag('td', [], date('Y-m-d H:i:s', $redir->hdateModif));
			echo "</tr>";
		}
		?>
		</tbody>
	</table>
</section>

<section id="revues">
	<h2>Revues</h2>
	<table class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>Source</th><th>Cible</th><th>Nom cible</th><th>Date modification</th>
			</tr>
		</thead>
		<tbody>
		<?php
		foreach ($redirRevue as $redir) {
			echo "<tr>";
			echo CHtml::tag('td', [], (string) $redir->sourceId);
			echo CHtml::tag('td', [], CHtml::link((string) $redir->cibleId, $redir->getUrl()));
			echo CHtml::tag('td', [], CHtml::link($redir->getName(), $redir->getUrl()));
			echo CHtml::tag('td', [], date('Y-m-d H:i:s', $redir->hdateModif));
			echo "</tr>";
		}
		?>
		</tbody>
	</table>
</section>

<section id="titres">
	<h2>Titres</h2>
	<table class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>Source</th><th>Cible</th><th>Nom cible</th><th>Date modification</th>
			</tr>
		</thead>
		<tbody>
		<?php
		foreach ($redirTitre as $redir) {
			echo "<tr>";
			echo CHtml::tag('td', [], (string) $redir->sourceId);
			echo CHtml::tag('td', [], CHtml::link((string) $redir->cibleId, $redir->getUrl()));
			echo CHtml::tag('td', [], CHtml::link($redir->getName(), $redir->getUrl()));
			echo CHtml::tag('td', [], date('Y-m-d H:i:s', $redir->hdateModif));
			echo "</tr>";
		}
		?>
		</tbody>
	</table>
</section>
