<?php

/** @var Controller $this */
/** @var \Grappe $grappe */

assert($this instanceof Controller);

$this->pageTitle = "Grappe {$grappe->nom} : ajouter une recherche";
if ($grappe->partenaireId > 0) {
	$this->breadcrumbs = [
		"Partenaires" => ['/partenaire/index'],
		$grappe->partenaire->nom => ['/partenaire/view', 'id' => $grappe->partenaireId],
		"Administrer mes grappes" => ['/grappe/partenaire', 'id' => $grappe->partenaireId],
	];
} else {
	$this->breadcrumbs = [
		"Grappes" => ['/grappe/index'],
		"Administrer les grappes" => ['/grappe/admin'],
	];
}
$this->breadcrumbs["{$grappe->nom} (vue publique)"] = ['/grappe/view', 'id' => $grappe->id];
$this->breadcrumbs["{$grappe->nom} (vue propriétaire)"] = ['/grappe/admin-view', 'id' => $grappe->id];
$this->breadcrumbs[] = "Ajouter une recherche";
?>

<h1>Grappe <em><?= CHtml::encode($grappe->nom) ?></em></h1>
<h2>Ajout de recherche</h2>

<?= CHtml::form() ?>
	<label for="url">Copiez ici une URL de <?= CHtml::link("recherche avancée", ['/revue/search'], ['target' => '_blank']) ?> de revues (/revue/search)</label>
	<div>
		Les résultats de cette recherche seront intégrés de manière dynamique dans la grappe, au fil du temps.
	</div>
	<div>
		Attention : les filtres de recherche (<em>facettes</em> de gauche) ne seront pas pris en compte.
	</div>
	<?= CHtml::textField("url", "", ['class' => 'span12', 'pattern' => '.*/revue/search\?q.+']) ?>
	<div class="form-actions">
		<button type="submit" class="btn btn-primary">Ajouter</button>
	</div>
<?= CHtml::endForm() ?>
