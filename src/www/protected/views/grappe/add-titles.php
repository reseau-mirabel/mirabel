<?php

/** @var Controller $this */
/** @var processes\grappe\AddJournalsForm $formData */
/** @var \Grappe $grappe */
/** @var \Titre[] $titres */

assert($this instanceof Controller);

Yii::app()->getClientScript()->registerScript('add-titles', file_get_contents(__DIR__ . '/add-titles.js'));

$this->pageTitle = "Grappe {$grappe->nom} : ajouter des titres";
if ($grappe->partenaireId > 0) {
	$this->breadcrumbs = [
		"Partenaires" => ['/partenaire/index'],
		$grappe->partenaire->nom => ['/partenaire/view', 'id' => $grappe->partenaireId],
		"Administrer mes grappes" => ['/grappe/partenaire', 'id' => $grappe->partenaireId],
	];
} else {
	$this->breadcrumbs = [
		"Grappes" => ['/grappe/index'],
		"Administrer les grappes" => ['/grappe/admin'],
	];
}
$this->breadcrumbs["{$grappe->nom} (vue publique)"] = ['/grappe/view', 'id' => $grappe->id];
$this->breadcrumbs["{$grappe->nom} (vue propriétaire)"] = ['/grappe/admin-view', 'id' => $grappe->id];
$this->breadcrumbs[] = "Ajouter des titres";
?>

<h1>Grappe <em><?= CHtml::encode($grappe->nom) ?></em> : ajout de titres</h1>

<div class="alert alert-info">
	<ol>
		<li>
			Les trois méthodes ci-dessous permettent d'alimenter la liste de titres sur cette page.
			Elles peuvent être cumulées, mais une à la fois.
		<li>
			Après ces ajouts, vous pouvez vérifier votre sélection dans la table <strong>Titres à ajouter</strong>
			et en retirer des titres.
		<li>Ensuite vous pouvez ajouter cette liste de titres à la grappe.
	</ol>
</div>

<?php
/** @var BootActiveForm $form */
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'grappe-add-journals-form',
		'enableAjaxValidation' => false,
		'type' => BootActiveForm::TYPE_VERTICAL,
		'htmlOptions' => ['enctype' => 'multipart/form-data'], // file upload
	]
);

echo $form->errorSummary($formData);
?>
<div class="columns">
	<div id="sources">
		<div class="well" id="par-titre">
			<label>Sélectionnez un titre par son nom ou ISSN</label>
			<?php
			$attribute = 'id';
			$htmlOptions = ['id' => 'titre-id'];
			echo $form->hiddenField($formData, $attribute, $htmlOptions);
			$this->widget('zii.widgets.jui.CJuiAutoComplete', [
				'name' => "{$attribute}Complete",
				'source' => <<<EOJS
					js:function(request, response) {
						$.ajax({
							url: "/titre/ajax-complete",
							data: {"term": request.term},
							success: function(data) { response(data); }
						});
					}
					EOJS,
				'options' => [
					'minLength' => '3',
					'select' => "js:function(event, ui) { jQuery('#{$htmlOptions['id']}').val(ui.item.id); }",
				],
				'htmlOptions' => [
					'class' => 'span12',
					'id' => "idComplete",
				],
			]);
			Yii::app()->getClientScript()->registerScript(
				"jui-autocomplete-custom-id",
				<<<EOJS
					const instance = $("#idComplete").autocomplete("instance");
					instance.menu.options.items = '> li:not(.ui-menu-item-disabled)';
					instance._renderItem = function(ul, item) {
						if (item.hasOwnProperty('forbid') && item.forbid !== '') {
							return $('<li class="ui-menu-item-disabled">')
								.append("<div>" + item.label + '</div><div class="item-forbid">' + item.forbid + "</div>")
								.appendTo(ul);
						}
						return $('<li>')
							.append("<div>" + item.label + "</div>")
							.appendTo(ul);
					};
					EOJS
			);
			?>
			<div>
				<button type="submit" class="btn btn-primary">Lister ce titre</button>
			</div>
		</div>
		<div class="well" id="par-recherche">
			<label>Copiez ici une URL de recherche de revues (/revue/search)</label>
			<?= $form->textField($formData, "url", ['class' => 'span12', 'pattern' => '.*/revue/search\?q.+']) ?>
			<div>
				<button type="submit" class="btn btn-primary">Lister ces titres</button>
			</div>
		</div>
		<div class="well" id="par-fichier">
			<label>Déposez un fichier de tableur (CSV, ODS, XLSX) contenant des ISSN</label>
			<?= $form->fileField($formData, "spreadsheet", ['class' => 'span12']) ?>
			<div>
				<button type="submit" class="btn btn-primary">Lister ces titres</button>
			</div>
		</div>
	</div>
	<div id="journals">
		<h2>Titres à ajouter</h2>
		<?php
		if ($formData->getNumIgnoredRows() > 0) {
			echo CHtml::tag(
				'div',
				['class' => 'warning'],
				"Dans le fichier chargé, {$formData->getNumIgnoredRows()} lignes contenaient des ISSN sans aucune correspondance dans Mir@bel."
			);
		}
		?>
		<table class="table table-striped table-bordered table-condensed">
			<tbody>
				<?php
				if ($titres) {
					$rowNum = 1;
					foreach ($titres as $t) {
						echo "<tr>"
							, "<td>{$rowNum}", CHtml::hiddenField(CHtml::modelName($formData) . '[titreId][]', (string) $t->id, ['id' => false]), "</td>"
							, "<td>{$t->getSelfLink()}</td>"
							, '<td><a href="#" class="rm">Retirer</a></td>'
							, '</tr>';
						$rowNum++;
					}
				} else {
					echo "<tr><td><em>Aucun titre sélectionné</em></td></tr>";
				}
				?>
			</tbody>
		</table>
		<button type="submit" <?= $titres ? "" : "disabled" ?> class="btn btn-primary" name="<?= CHtml::modelName($formData) ?>[save]" value="1">
			Ajouter cette liste à la grappe
		</button>
	</div>
</div>
<?php $this->endWidget() ?>
