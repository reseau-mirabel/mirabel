<?php

/** @var Controller $this */
/** @var \Grappe $grappe */
/** @var \Titre[] $titres */
/** @var \processes\grappe\Permissions $checkAccess */

assert($this instanceof Controller);

$this->pageTitle = "Grappe {$grappe->nom}";
if ($grappe->partenaireId > 0) {
	$this->breadcrumbs = array_filter([
		"Partenaires" => ['/partenaire/index'],
		$grappe->partenaire->nom => ['/partenaire/view', 'id' => $grappe->partenaireId],
		"Administrer mes grappes" => ($checkAccess->canWrite($grappe) ? ['/grappe/partenaire', 'id' => $grappe->partenaireId] : null),
	]);
} else {
	$this->breadcrumbs = array_filter([
		"Grappes" => ['/grappe/index'],
		"Administrer les grappes" => ($checkAccess->canWrite($grappe) ? ['/grappe/admin'] : null),
	]);
}
if ($checkAccess->canWrite($grappe)) {
	$this->breadcrumbs["{$grappe->nom} (vue propriétaire)"] = ['/grappe/admin-view', 'id' => $grappe->id];
}
$this->breadcrumbs[] = $grappe->nom;
?>

<?= $this->renderPartial('_logo-grappe') ?>
<h1><span class="alerte-orange">Grappe</span> <em><?= CHtml::encode($grappe->nom) ?></em></h1>

<?php
if ($checkAccess->canWrite($grappe)) {
	echo '<div class="pull-right">',
		CHtml::link("Administrer cette grappe", ['/grappe/admin-view', 'id' => $grappe->id], ['class' => 'btn btn-default']),
		'</div>';
}
?>

<div>
	<?php
	if ($grappe->description) {
		echo '<div>' . $grappe->description . '</div>';
	}
	?>
</div>

<?php
$countRevues = \processes\grappe\Stats::countByRevue($grappe->id);
$params = ['grappe' => $grappe, 'titres' => $titres, 'countRevues' => $countRevues];
if ($grappe->niveau === Grappe::NIVEAU_TITRE) {
	$count = \processes\grappe\Stats::countByTitre($grappe->id);
	$params['countTitres'] = $count;
	$this->renderPartial('_liste-titres', $params);
} else {
	$count = $countRevues;
	$this->renderPartial('_liste-revues', $params);
}

if (count($titres) < $count['total']) {
	?>
	<p>
		Cet affichage présente les <?= count($titres) ?> premiers éléments sur un total de <?= $count['total'] ?>.
		<a href="#" id="load-full-list" class="btn btn-default btn-small">Charger la liste complète</a>
	</p>
	<?php
	Yii::app()->clientScript->registerScript('load-full-list', <<<JS
		const loadButton = document.getElementById('load-full-list');
		loadButton.addEventListener('click', function(e) {
			e.preventDefault();
			fetch(window.location.href.trim('#') + '?full=on')
				.then(r => r.text())
				.then(function(html) {
					document.getElementById('grappe-liste').innerHTML = html;
				});
			loadButton.parentNode.remove();
			return false;
		});
		JS
	);
}
