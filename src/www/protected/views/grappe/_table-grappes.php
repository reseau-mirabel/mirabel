<?php

/** @var Controller $this */
/** @var processes\grappe\FilteredList $grappes */
/** @var bool $search */

if ($search) {
	$form = new CActiveForm();
	$form->action = "/{$this->route}";
	$form->method = 'GET';
	$form->init();
}

$provider = $grappes->getDataProvider();
?>
<table class="table table-hover">
	<thead>
		<tr>
			<th><?= $provider->getSort()->link('id', "ID") ?></th>
			<th><?= $provider->getSort()->link('nom', "Nom") ?></th>
			<th>Titres</th>
			<th>Revues</th>
			<th>Critères de recherche</th>
			<?php if (!$grappes->hasRequirePartenaire()) { ?>
			<th><?= $provider->getSort()->link('partenaire', "Partenaire") ?></th>
			<?php } ?>
			<th>Diffusion</th>
			<th><abbr title="Nombre de partenaires qui placent cette grappe dans leurs listes"><?= $provider->getSort()->link('inlists', "Utilisée<br>si partage") ?></abbr></th>
			<th>
				<?= CHtml::tag(
					'abbr',
					['title' => "Date de dernière modification par un utilisateur\nLe contenu dynamique d'une grappe peut évoluer sans modification."],
					$provider->getSort()->link('date', "Modifée")
				) ?>
			</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php
		if ($search) {
			echo "<tr><td></td><td>"
				, $form->textField($grappes, 'nom', ['name' => "g-nom", 'style' => 'width: 100%; margin: 0;', 'aria-role' => 'search'])
				, "</td>"
				, '<td colspan="3"></td>'
				, '<td>', $form->dropDownList($grappes, 'pid', $grappes->listPartenaires(), ['name' => "g-p", 'aria-role' => 'search', 'onchange' => "this.closest('form').submit()"]), '<td>'
				, '<td colspan="3"></td>'
				,'</tr>';
		}

		if ($provider->getItemCount(true) === 0) {
			echo '<tr><td colspan="10">Aucune grappe ne correspond.</td></tr>';
		}

		$checkAccess = new \processes\grappe\Permissions(\Yii::app()->user);
		foreach ($provider->getData() as $row) {
			$grappe = Grappe::model()->populateRecord($row);
			if (!($grappe instanceof \Grappe) || !$checkAccess->canRead($grappe)) {
				continue;
			}
			?>
		<tr class="grappe">
			<td>
				<?= $grappe->id ?>
			</td>
			<td>
				<div>
					<?= $grappe->getSelfLink() ?>
				</div>
				<div class="ellipsis" style="font-size:small; font-style:italic;" 
					 title="<?= CHtml::encode(strip_tags($grappe->description)) ?>">
					<?= $grappe->description ? " " : "" ?> 
				</div>
			</td>
			<td>
				<?php
				if ($grappe->niveau === Grappe::NIVEAU_TITRE) {
					echo $row['titres'] ?: "0";
				}
				?>
			</td>
			<td>
				<?= $row['revues'] ?: "" ?>
			</td>
			<td>
				<?= $grappe->countCriteria() ?: "" ?>
			</td>
			<?php if (!$grappes->hasRequirePartenaire()) { ?>
			<td>
				<?= isset($grappe->partenaireId) ? ($grappe->partenaire->getSelfLink(true)) : '(aucun)' ?>
			</td>
			<?php } ?>
			<td>
				<?= Grappe::ENUM_DIFFUSION[$grappe->diffusion] ?>
			</td>
			<td>
				<?= $grappe->diffusion === Grappe::DIFFUSION_PARTAGE ? (int) $row['inlists'] : '' ?>
			</td>
			<td>
				<?= Yii::app()->dateFormatter->format('d/M/yyyy', $grappe->hdateModif) ?>
			</td>
			<td class="button-column">
				<?= CHtml::link(
					'<i class="icon-eye-open"></i>',
					$grappe->getSelfUrl(),
					['class' => 'view', 'title' => "Vue publique de cette grappe"]
				) ?>
				<?php
				if ($checkAccess->canWrite($grappe)) {
					echo CHtml::link(
						'<i class="icon-pencil"></i>',
						['/grappe/admin-view', 'id' => $grappe->id],
						['class' => 'update', 'title' => "Administrer cette grappe"]
					);
				}
				?>
			</td>
		</tr>
		<?php
		}
		?>
	</tbody>
</table>
<?php
if ($search) {
	$form->run();
}
