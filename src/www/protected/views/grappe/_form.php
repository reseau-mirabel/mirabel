<?php

/** @var Controller $this */
/** @var processes\grappe\GrappeForm $formData */

assert($this instanceof Controller);

$permissions = new processes\grappe\Permissions(\Yii::app()->user);

$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'grappe-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => false,
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'hints' => Hint::model()->getMessages('Grappe'),
		'htmlOptions' => ['class' => 'well'],
	]
);
/** @var BootActiveForm $form */
?>

<?php
echo $form->errorSummary($formData);

echo $form->textFieldRow($formData, 'nom', ['class' => 'span12']);

new \widgets\CkEditor();
echo $form->textAreaRow($formData, 'description', ['rows' => 6, 'cols' => 50, 'class' => 'span8 htmleditor']);

echo $form->textAreaRow($formData, 'note', ['class' => 'span12', 'rows' => 4, 'placeholder' => "Texte caché aux autres partenaires…"]);

$inLists = \processes\grappe\Selection::getListsReferences($formData->getGrappe());
$hasExternalUse = $inLists['inOtherLists'] || $inLists['isInGlobalList'];
if ($formData->diffusion !== Grappe::DIFFUSION_PARTAGE || !$hasExternalUse || $permissions->canAdmin()) {
	echo $form->dropDownListRow(
		$formData,
		'diffusion',
		\Grappe::ENUM_DIFFUSION,
		['prompt' => "[Qui peut voir cette grappe ?]", 'style' => "min-width: 37ex;", 'id' => 'grappe-diffusion']
	);
} else {
	?>
	<div class="control-group">
		<label class="control-label" for="Grappe_diffusion">Diffusion</label>
		<div class="controls"><span class="uneditable-input"><?= \Grappe::ENUM_DIFFUSION[$formData->diffusion] ?></span></div>
	</div>
	<?php
}
echo '<div id="grappe-diffusion-commentaire" class="controls" style="margin-top:-1em; padding-bottom:1em;"> </div>';
if ($inLists['ids']) {
	$memberships = [];
	if ($inLists['isInOwnLists']) {
		$memberships[] = "les listes de ce partenaire";
	}
	if ($inLists['inOtherLists'] > 0) {
		$memberships[] = "{$inLists['inOtherLists']} listes d'autres partenaires";
	}
	if ($inLists['isInGlobalList']) {
		$memberships[] = "la liste globale de Mir@bel";
	}
	echo '<p class="alert alert-warning controls" style="margin-top:-1em">'
		. 'Cette grappe est actuellement affichée dans ' . count($inLists['ids']) . ' contexte' . (count($inLists['ids']) > 1 ? 's' : '') . ' : '
		. join(", ", $memberships) . ". "
		. ($permissions->canAdmin() ? "Restreindre sa diffusion l'enlèvera de ces listes." : "Vous ne pouvez pas restreindre sa diffusion.")
		. "</<p>";
}
$comments = json_encode(Grappe::DIFFUSION_COMMENT);
\Yii::app()->clientScript->registerScript('diffusion', <<<JS
	function displayComment(diffusion) {
		const comments = $comments;
		let comment = comments[diffusion];
		document.getElementById('grappe-diffusion-commentaire').innerHTML = comment;
	}
	const diffusionElement = document.getElementById('grappe-diffusion');
	diffusionElement.addEventListener('change', function() {
		displayComment(parseInt(diffusionElement.value) || 0);
	});
	displayComment(diffusionElement.value);
	JS
);

echo $form->dropDownListRow($formData, 'niveau', \Grappe::NIVEAU, ['prompt' => "[Sous quelle forme afficher publiquement le contenu ?]", 'style' => "min-width: 37ex;"]);

if ($permissions->canAdmin()) {
	echo '<fieldset><legend>Champs réservés à un admin</legend>';

	echo $form->dropDownListRow(
		$formData,
		'partenaireId',
		[
			'-1' => "choix spécifique aux admins…",
			'0' => "** Sans propriétaire **",
		] + \components\SqlHelper::sqlToPairs("SELECT id, nom FROM Partenaire WHERE statut = 'actif' ORDER BY nom"),
		['class' => 'span12']
	);
	echo "</fieldset>";
}
?>

<div class="form-actions">
	<button type="submit" class="btn btn-primary">Enregistrer</button>
</div>

<?php
$this->endWidget();
