<?php

/** @var Controller $this */
/** @var \Grappe $grappe */
/** @var \Titre[] $titresDirects */
/** @var \Titre[] $titresInduits */

use components\HtmlHelper;

assert($this instanceof Controller);

$this->pageTitle = "Grappe {$grappe->nom}";
$perms = new \processes\grappe\Permissions(\Yii::app()->user);
if ($grappe->partenaireId > 0) {
	$this->breadcrumbs = array_filter([
		"Partenaires" => ['/partenaire/index'],
		$grappe->partenaire->nom => ['/partenaire/view', 'id' => $grappe->partenaireId],
		"Administrer mes grappes" => ($perms->canWrite($grappe) ? ['/grappe/partenaire', 'id' => $grappe->partenaireId] : null),
	]);
} else {
	$this->breadcrumbs = array_filter([
		"Grappes" => ['/grappe/index'],
		"Administrer les grappes" => ($perms->canWrite($grappe) ? ['/grappe/admin'] : null),
	]);
}
$this->breadcrumbs["{$grappe->nom} (vue publique)"] = ['/grappe/view', 'id' => $grappe->id];
$this->breadcrumbs[] = "{$grappe->nom} (vue propriétaire)";
?>

<h1><span class="alerte-orange">Grappe</span> <em><?= CHtml::encode($grappe->nom) ?></em></h1>

<section id="grappe-proprietes">
	<div class="pull-right" style="display:flex; gap:4px;">
		<?php
		if ($perms->canWrite($grappe)) {
			echo CHtml::link("Modifier", ['/grappe/update', 'id' => $grappe->id], ['class' => "btn btn-primary"]);
			echo \components\HtmlHelper::postButton(
				"Supprimer",
				['/grappe/delete', 'id' => $grappe->id],
				[],
				['class' => "btn btn-danger", 'onclick' => "return confirm('Êtes-vous certain de vouloir supprimer définitivement cette grappe ?')"]
			);
		}
		?>
	</div>
	<h2>Propriétés</h2>
	<?php
	if ($grappe->description) {
		echo '<div>' . $grappe->description . '</div>';
	}
	if ($grappe->note) {
		echo '<div class="well"><strong>[Note interne]</strong> ' . nl2br(CHtml::encode($grappe->note)) . '</div>';
	}
	?>
	<table class="table">
		<tbody>
			<tr class="diffusion">
				<td>Diffusion</td>
				<td>
					<strong><?= Grappe::ENUM_DIFFUSION[$grappe->diffusion] ?></strong>
					<div>
						<?= Grappe::DIFFUSION_COMMENT[$grappe->diffusion] ?? '' ?>
					</div>
					<div>
						Voir cette grappe
						<?= CHtml::link("en mode public (liste de " . Grappe::NIVEAU[$grappe->niveau] . ")", ['/grappe/view', 'id' => $grappe->id]) ?>
					</div>
				</td>
			</tr>
			<tr class="lists">
				<td>Listes</td>
				<td>
					<?php
					$inLists = \processes\grappe\Selection::getListsReferences($grappe);
					if ($inLists['isInOwnLists']) {
						echo CHtml::link("Utilisée dans mes listes", ['/partenaire/grappes-affichage', 'id' => $grappe->partenaireId]);
					} else if ($grappe->diffusion === Grappe::DIFFUSION_PUBLIC) {
						echo CHtml::link("Absente de mes listes", ['/partenaire/grappes-affichage', 'id' => $grappe->partenaireId]);
					}
					if ($inLists['inOtherLists'] > 0) {
						echo "<details>"
							. "<summary>Utilisée dans {$inLists['inOtherLists']} listes d'autres partenaires.</summary>"
							. "<ul>";
						foreach ($inLists['otherLists'] as $other) {
							[$pid, $nom, $pageP, $pageS] = $other;
							echo "<li>", CHtml::encode($nom);
							if ($pageP) {
								echo " ", CHtml::link("[page partenaire]", ['/partenaire/view', 'id' => $pid, '#' => 'grappes-selection']);
							}
							if ($pageS) {
								echo " ", CHtml::link("[page grappes]", ['/partenaire/grappes', 'id' => $pid]);
							}
							echo "</li>\n";
						}
						echo "</ul></details>";
					}
					if ($inLists['isInGlobalList']) {
						echo "<div>Utilisée dans la " . CHtml::link("liste globale de Mir@bel", ['/grappe/index']) . ".</div>";
					}
					if ($grappe->diffusion === Grappe::DIFFUSION_PARTAGE && $inLists['inOtherLists'] === 0 && !$inLists['isInGlobalList']) {
						echo "<div>Cette grappe n'est pas utilisée par d'autres partenaires, ni dans la liste globale de Mir@bel.</div>";
					}
					?>
				</td>
			<tr class="niveau">
				<td>Niveau</td>
				<th><?= Grappe::NIVEAU[$grappe->niveau] ?></th>
			</tr>
			<?php if ($grappe->partenaireId) { ?>
			<tr class="partenaire">
				<td>Partenaire</td>
				<th><?= $grappe->partenaire->getSelfLink() ?></th>
			</tr>
			<?php } ?>
		</tbody>
	</table>
</section>

<section id="grappe-stats">
	<h2>Stats de contenu</h2>
	<?php
	$countTitres = \processes\grappe\Stats::countByTitre($grappe->id);
	$countRevues = \processes\grappe\Stats::countByRevue($grappe->id);
	?>
	<table class="table table-condensed" style="max-width: 60ex">
		<thead>
			<tr>
				<th></th>
				<th>Titres</th>
				<th>Revues</th>
			</tr>
		</thead>			
		<tbody>
			<tr>
				<th>Total</th>
				<td>
					<?= $grappe->niveau === Grappe::NIVEAU_TITRE ?
						CHtml::link((string) $countTitres['total'], ['/grappe/view', 'id' => $grappe->id, 'public' => 1], ['title' => "Vue publique"])
						: $countTitres['total']
					?>
				</td>
				<td>
					<?= $grappe->niveau === Grappe::NIVEAU_REVUE ?
						CHtml::link((string) $countRevues['total'], ['/grappe/view', 'id' => $grappe->id, 'public' => 1], ['title' => "Vue publique"])
						: $countRevues['total']
					?>
					<?= CHtml::link(
						'<span class="glyphicon glyphicon-search"></span>',
						['/revue/search', 'q' => ['grappe' => $grappe->id]],
						['title' => "Rechercher les revues de cette grappe"]
					) ?>
				</td>
			</tr>
			<tr>
				<td>dont ajouts manuels</td>
				<td><a href="#titres-directs"><?= $countTitres['manuelle'] ?></a></td>
				<td><?= $countRevues['manuelle'] ?></td>
			</tr>
			<tr>
				<td>dont ajouts par recherches</td>
				<td><a href="#titres-induits"><?= $countTitres['recherche'] ?></a></td>
				<td><?= $countRevues['recherche'] ?></td>
			</tr>
		</tbody>
	</table>
	<p>
		<?= CHtml::link("Vérifier les liens morts dans ces revues", ['/verification/liens-revues', 'VerifUrlForm' => ['grappe' => $grappe->id]]) ?>
		ou
		<?= CHtml::link("dans leurs éditeurs", ['/verification/liens-editeurs', 'VerifUrlForm' => ['grappe' => $grappe->id]]) ?>.
	</p>
</section>

<section style="margin-top: 1em" id="titres-induits">
	<h2>Recherches ajoutant des titres (liste dynamique)</h2>
	<div style="text-align:right;">
		<?php if ($perms->canWrite($grappe)) { ?>
		<?= CHtml::link("Ajouter une recherche", ['/grappe/ajout-recherche', 'id' => $grappe->id], ['class' => "btn btn-primary"]) ?>
		<?= CHtml::link("Rafraîchir les titres", ['/grappe/refresh', 'id' => $grappe->id], ['class' => "btn btn-default", 'title' => "Met à jour la liste des titres induits"]) ?>
		<?= CHtml::link("Supprimer des recherches", ['/grappe/suppression-contenu', 'id' => $grappe->id], ['class' => "btn btn-danger"]) ?>
		<?php } ?>
	</div>
	<?php if ($grappe->recherche !== '[]') { ?>
		<ol>
			<?php
			foreach ($grappe->getSearchCriteria() as $pos => $search) {
				$count = (int) Yii::app()->db
					->createCommand("SELECT count(*) FROM Grappe_Titre WHERE grappeId = {$grappe->id} AND source = $pos")
					->queryScalar();
				echo "<li>"
					, $search->htmlSummary()
					, " ($count titres)"
					, " ", CHtml::link(
						'<span class="glyphicon glyphicon-search"></span>',
						['/revue/search', 'q' => $search->exportAttributes()->toArray()],
						['title' => "Rechercher les revues correspondant à ces critères"]
					)
					, "</li>\n";
			}
			?>
		</ol>
	<?php } else { ?>
		<p>Aucune</p>
	<?php }?>

	<details>
		<summary><h3 style="display:inline-block;">Titres induits par ces recherches</h3></summary>
		<?= $this->renderPartial('_table-titres',
			['titres' => $titresInduits, 'csvName' => HtmlHelper::exportableName("Grappe-{$grappe->id}_Titres-par-recherche")]
		) ?>
	</details>
</section>

<section style="margin-top: 1em" id="titres-directs">
	<h2>Titres ajoutés directement (liste statique)</h2>
	<div style="text-align:right;">
		<?php if ($perms->canWrite($grappe)) { ?>
		<?= CHtml::link("Ajouter des titres par recherche ou ISSN", ['/grappe/ajout-titres', 'id' => $grappe->id], ['class' => "btn btn-primary"]) ?>
		<?= CHtml::link("Supprimer des titres", ['/grappe/suppression-contenu', 'id' => $grappe->id], ['class' => "btn btn-danger"]) ?>
		<?php } ?>
	</div>
	<?= $this->renderPartial('_table-titres',
		['titres' => $titresDirects, 'csvName' => HtmlHelper::exportableName("Grappe-{$grappe->id}_Titres-statiques")]
	) ?>
</section>
