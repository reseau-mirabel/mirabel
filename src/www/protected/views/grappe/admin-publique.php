<?php

/** @var Controller $this */
/** @var processes\partenaire\GrappesSelection $formData */
/** @var processes\partenaire\GrappeAffichage[] $selected */

$this->pageTitle = "Gestion de la liste publique des grappes";

$this->breadcrumbs = [
	"Liste publique des grappes" => ['/grappe/index'],
	"Gestion",
];
?>
<h1><?= $this->pageTitle ?></h1>
<p class="alert alert-info">
	Cette page gère la <?= CHtml::link("liste publique des grappes", ['/grappe']) ?>.
</p>

<section id="liste-globale-candidats">
	<h2>Ajouter des grappes <em>partagées</em> à la liste</h2>
	<?php
	$this->renderPartial('/partenaire/_grappe-selection', ['formData' => $formData]);
	?>
</section>

<section id="liste-globale-selection">
	<h2>Liste publique <small>(<?= count($selected) ?> grappes)</small></h2>
	<?php
	Yii::app()->clientScript->registerCoreScript('jquery.ui'); // sortable items
	?>
	<p class="alert alert-info">Glissez-déposez une ligne pour modifier l'ordre des grappes.</p>

	<?php
	$form = $this->beginWidget(
		'bootstrap.widgets.BootActiveForm',
		[
			'enableAjaxValidation' => false,
			'type' => BootActiveForm::TYPE_HORIZONTAL,
			'method' => 'POST',
		]
	);
	/** @var BootActiveForm $form */
	?>

	<table class="table">
		<thead>
			<tr>
				<th>Grappes</th>
				<th style="text-align: right;"><span class="glyphicon glyphicon-pencil" title="Modifier une grappe"></span></th>
			</tr>
		<tbody id="sortable">
			<?php
			$buttonTemplate = <<<HTML
				<button type="button" class="btn btn-small btn-danger remove-by-id" data-id="{{ID}}">Retirer</button>
				HTML;

			foreach ($selected as $row) {
				echo $form->errorSummary($formData);
				echo "<tr>";

				echo "<td>"
					. '<span class="glyphicon glyphicon-menu-hamburger"></span> '
					. CHtml::encode($row->grappeName)
					. CHtml::hiddenField('GrappesSelection[add][]', (string) $row->grappeId)
					. "</td>";

				echo "<td>";
				echo '<div style="display:flex; justify-content:space-around;">';
				echo CHtml::link(
					'<span class="glyphicon glyphicon-eye-open" title="Afficher cette grappe"></span>',
					['/grappe/view', 'id' => $row->grappeId]
				);
				echo CHtml::link(
					'<span class="glyphicon glyphicon-pencil" title="Modifier cette grappe"></span>',
					['/grappe/admin-view', 'id' => $row->grappeId]
				);
				echo str_replace('{{ID}}', (string) $row->grappeId, $buttonTemplate);
				echo '</div>';
				echo "</td>";

				echo "</tr>";
			}
			?>
		</tbody>
	</table>

	<div class="form-actions">
		<button type="submit" class="btn btn-primary">Enregistrer les modifications</button>
	</div>

	<?php
	$this->endWidget();

	Yii::app()->clientScript->registerScript('sortable', <<<JS
		$("#sortable").sortable({
			items: "tr",
			axis: "y",
		});
		$('.remove-by-id').on('click', function(e) {
			if (!confirm('Retirer cette grappe de ma sélection ?')) {
				return;
			}
			const id = e.target.getAttribute('data-id');
			const form = document.querySelector('#remove-from-selection form');
			form.querySelector('input').value = id;
			form.submit();
		});
		JS
	);

	?>
	<div style="display:none" id="remove-from-selection">
		<?= \components\HtmlHelper::postButton(
			"Retirer",
			['/grappe/admin-publique-suppr', 'id' => 0],
			['grappeId' => '{{ID}}']
		) ?>
	</div>
</section>
