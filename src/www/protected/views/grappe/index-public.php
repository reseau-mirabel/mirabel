<?php

/** @var Controller $this */
/** @var Grappe[] $grappes */

assert($this instanceof Controller);

$this->pageTitle = "Grappes";
$this->breadcrumbs = [
	'Grappes',
];
?>

<?= $this->renderPartial('_logo-grappe') ?>
<h1>Les grappes de Mir@bel</h1>

<?= Cms::getBlock('grappe-intro')->toHtml() ?>

<?php
$perms = new \processes\grappe\Permissions(Yii::app()->user);
if ($perms->canCreate() || $perms->canWrite(null)) {
	echo '<div class="pull-right">',
		($perms->canAdmin() ? CHtml::link("Gestion de la liste publique", ['/grappe/admin-publique'], ['class' => 'btn btn-default']) : ""),
		CHtml::link("Administration des grappes", ['/grappe/admin'], ['class' => 'btn btn-default']),
		'</div>';
}

if ($grappes) {
	$this->renderPartial('_liste-publique', ['grappes' => $grappes]);
} else {
	echo "<p class=\"alert alert-info\">Aucune grappe n'est actuellement publique.</p>";
}
