<?php

/** @var Controller $this */
/** @var \Grappe $grappe */
/** @var \Titre[] $titres */
/** @var array{total: int} $countRevues */
/** @var array{total: int} $countTitres */

$loupe = CHtml::link(
	'<span class="glyphicon glyphicon-search"></span>',
	['/revue/search', 'q' => ['grappe' => $grappe->id]],
	['title' => "Rechercher les revues de cette grappe"]
);
?>
<section style="margin-top: 1em">
	<h2>
		<?= $countTitres['total'] ?> titres
		<small>
			soit <?= $countRevues['total'] ?> revues
			<?= $loupe ?>
		</small>
	</h2>
	<p>
		La loupe <?= $loupe ?> vous permet de rechercher parmi ce corpus via la recherche avancée de Mir@bel.
	</p>
	<?php
	if ($titres) {
		echo '<ol id="grappe-liste">';
		foreach ($titres as $t) {
			echo "<li>{$t->getSelfLink()}</li>\n";
		}
		echo "</ol>";
	} else {
		echo "<p>Aucun</p>";
	}
	?>
</section>
