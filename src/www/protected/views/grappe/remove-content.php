<?php

/** @var Controller $this */
/** @var processes\grappe\RemoveContentForm $formData */
/** @var \Grappe $grappe */
/** @var \Titre[] $titres */

assert($this instanceof Controller);

$this->pageTitle = "Grappe {$grappe->nom} : ajout de recherche";
$this->breadcrumbs = [
	'Grappes' => ['index'],
	"{$grappe->nom} (vue publique)" => ['view', 'id' => $grappe->id],
	"(vue admin)" => ['admin-view', 'id' => $grappe->id],
	"Supprimer…",
];
?>

<h1>Grappe <em><?= CHtml::encode($grappe->nom) ?></em></h1>
<h2 class="text-warning">Suppression de contenu</h2>

<p class="alert alert-warning">
	Cochez les éléments (recherches ou titres) que vous voulez retirer de cette grappe.
</p>

<?php
/** @var BootActiveForm $form */
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'grappe-remove-content-form',
		'enableAjaxValidation' => false,
		'type' => BootActiveForm::TYPE_VERTICAL,
		'htmlOptions' => ['enctype' => 'multipart/form-data'], // file upload
	]
);

echo $form->errorSummary($formData);
?>
<section id="recherches">
	<h3>Supprimer des recherches (liste dynamique)</h3>
	<?php
	$decoded = json_decode($grappe->recherche, true, 16, JSON_THROW_ON_ERROR);
	if ($decoded) {
		?>
		<table class="table">
			<tbody>
				<?php
				foreach ($decoded as $s) {
					$sStr = json_encode($s, JSON_UNESCAPED_SLASHES);
					$search = new SearchTitre();
					$search->setAttributes($s);
					$search->validate();
					echo "<tr>"
						, "<td>", CHtml::checkBox(CHtml::modelName($formData) . '[searches][]', false, ['value' => $sStr]), '</td>'
						, "<td>{$search->htmlSummary()}</td>"
						, "</tr>\n";
				}
				?>
			</tbody>
		</table>
		<?php
	} else {
		echo "<p>Aucune recherche.</p>";
	}
	?>
</section>
<section id="titres">
	<h3>Supprimer des titres (liste statique)</h3>
	<?php if ($titres) { ?>
	<table class="table">
		<thead>
			<tr>
				<th><input type="checkbox" class="selectall" id="titres-selectall" /></th>
				<th><label for="titres-selectall">Tout (dé)sélectionner</label></th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach ($titres as $t) {
				echo "<tr>"
					, "<td>", CHtml::checkBox(CHtml::modelName($formData) . '[titreIds][]', false, ['value' => $t->id]), '</td>'
					, "<td>{$t->getSelfLink()}</td>"
					, "</tr>\n";
			}
			?>
		</tbody>
	</table>
	<?php } else { ?>
	<p>Aucun titre.</p>
	<?php } ?>
</section>
<div class="form-actions">
	<button type="submit" class="btn btn-danger">
		Retirer les éléments sélectionnés
	</button>
</div>
<?php $this->endWidget() ?>

<?php
Yii::app()->clientScript->registerScript('selectall', <<<EOJS
	$('.selectall').on('click', function() {
		const checked = this.checked;
		for (let e of this.closest('table').querySelectorAll('tbody input[type="checkbox"]')) {
			e.checked = checked;
		}
	});
	EOJS
);
