<?php

/** @var Controller $this */
/** @var processes\grappe\GrappeForm $formData */

assert($this instanceof Controller);

$this->pageTitle = "Modifier la grappe {$formData->nom}";
$grappe = $formData->getGrappe();
if ($grappe->partenaireId > 0) {
	$this->breadcrumbs = [
		"Partenaires" => ['/partenaire/index'],
		$grappe->partenaire->nom => ['/partenaire/view', 'id' => $grappe->partenaireId],
		"Administrer mes grappes" => ['/grappe/partenaire', 'id' => $grappe->partenaireId],
	];
} else {
	$this->breadcrumbs = [
		"Grappes" => ['/grappe/index'],
		"Administrer les grappes" => ['/grappe/admin'],
	];
}
$this->breadcrumbs["{$grappe->nom} (vue publique)"] = ['/grappe/view', 'id' => $grappe->id];
$this->breadcrumbs["{$grappe->nom} (vue propriétaire)"] = ['/grappe/admin-view', 'id' => $grappe->id];
$this->breadcrumbs[] = "Modifier";
?>

<h1>Modifier la grappe <em><?= CHtml::encode($formData->nom) ?></em></h1>

<?= $this->renderPartial('_form', [
	'formData' => $formData,
]) ?>
