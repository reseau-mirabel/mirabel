<?php

/** @var Controller $this */
/** @var processes\grappe\FilteredList $grappesPart */
/** @var processes\grappe\FilteredList $grappesAutres */
/** @var Partenaire $partenaire Pour l'utilisateur courant */

assert($this instanceof Controller);

$this->pageTitle = "Grappes (admin)";
$this->breadcrumbs = [
	'Grappes' => ['/grappe/index'],
	'Administration',
];
?>

<?= $this->renderPartial('_logo-grappe') ?>
<h1>Les grappes de Mir@bel (vue réservée aux admins)</h1>

<p>
	Chaque grappe dans Mir@bel est un ensemble de revues, ou de titres de revues.
</p>

<div style="text-align: right; margin-bottom: 1ex">
	<?= CHtml::link('Gérer la liste publique de Mir@bel', ['/grappe/admin-publique'], ['class' => 'btn btn-default']) ?>
</div>
<div class="pull-right">
	<?= CHtml::link("Listes de {$partenaire->getShortName()}", ['/partenaire/grappes-affichage', 'id' => $partenaire->id], ['class' => 'btn btn-default']) ?>
	<?= CHtml::link('Créer une grappe', ['/grappe/create'], ['class' => 'btn btn-primary']) ?>
</div>

<section id="grappes-miennes">
	<h2>Grappes de mon établissement</h2>
	<?= $this->renderPartial('_table-grappes', ['grappes' => $grappesPart, 'search' => false]) ?>
</section>

<section id="grappes-autres">
	<h2>Autres grappes</h2>
	<?= $this->renderPartial('_table-grappes', ['grappes' => $grappesAutres, 'search' => true]) ?>
</section>
