$('#journals table').on('click', 'a.rm', function(e) {
	$(this).closest('tr').remove();
	e.preventDefault();
	return false;
});

$('#grappe-add-journals-form').on('submit', function(e) {
	const ids = [];
	for (let input of document.querySelectorAll('#grappe-add-journals-form #journals input')) {
		ids.push(parseInt(input.value));
		input.remove();
	}
	const serialized = document.createElement('input');
	serialized.type = 'hidden';
	serialized.name = 'AddJournalsForm[titreId]';
	serialized.value = ids.join('_');
	document.querySelector('#grappe-add-journals-form').append(serialized);
});


