<?php

/** @var Controller $this */
/** @var processes\grappe\FilteredList $grappesPart */
/** @var Partenaire $partenaire */

assert($this instanceof Controller);

$this->pageTitle = "Grappes (gestion)";
$this->breadcrumbs = [
	"Partenaires" => ['/partenaire/index'],
	$partenaire->nom => ['/partenaire/view', 'id' => $partenaire->id],
	"admin" => ['view-admin', 'id' => $partenaire->id],
	'Grappes',
];

$permissions = new \processes\grappe\Permissions(Yii::app()->user);
if ($permissions->canCreate()) {
	\Yii::app()->getComponent('sidebar')->menu = [
		[
			'label' => 'Créer une grappe',
			'url' => ['/grappe/create'],
		],
		[
			'label' => "Paramètres d'affichage",
			'url' => ['/partenaire/grappes-affichage', 'id' => $partenaire->id],
		],
		[
			'label' => 'Administration (pour admins)',
			'url' => ['/grappe/admin', 'id' => $partenaire->id],
			'visible' => $permissions->canAdmin(),
		],
	];
}
?>

<?= $this->renderPartial('_logo-grappe') ?>

<section id="grappes-miennes">
	<h1>Grappes de mon établissement – <?= CHtml::encode($partenaire->getShortName()) ?></h1>
	<p>
		Chaque grappe dans Mir@bel est un ensemble de revues, ou de titres de revues.
	</p>

	<?= $this->renderPartial('_table-grappes', ['grappes' => $grappesPart, 'search' => false]) ?>
</section>
