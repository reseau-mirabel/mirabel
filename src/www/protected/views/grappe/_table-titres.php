<?php

/** @var Controller $this */
/** @var \Titre[] $titres */
/** @var string $csvName */

if (!$titres) {
	echo "<p>Aucun</p>";
	return;
}
?>
<table class="table table-bordered table-condensed table-hover exportable" data-exportable-filename="<?= $csvName ?>">
	<thead>
		<tr>
			<th>ID</th>
			<th>ID revue</th>
			<th>Titre</th>
			<th>ISSN-P</th>
			<th>ISSN-E</th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($titres as $t) {
			$issns = \Yii::app()->db
				->createCommand("SELECT support, GROUP_CONCAT(issn SEPARATOR ' ') FROM Issn WHERE titreId = {$t->id} AND issn <> '' GROUP BY support")
				->queryAll(false);
			$issnp = "";
			$issne = "";
			foreach ($issns as $row) {
				if ($row[0] === Issn::SUPPORT_PAPIER) {
					$issnp = $row[1];
				} elseif ($row[0] === Issn::SUPPORT_ELECTRONIQUE) {
					$issne = $row[1];
				}
			}

			echo "<tr>",
				"<td>" . CHtml::link((string) $t->id, ['/titre/view', 'id' => $t->id]) . "</td>",
				"<td>" . CHtml::link((string) $t->revueId, ['/revue/view', 'id' => $t->revueId]) . "</td>",
				"<td>" . CHtml::encode($t->getFullTitle()) . "</td>",
				"<td>$issnp</td>",
				"<td>$issne</td>",
				"</tr>";
		}
		?>
	</tbody>
</table>
