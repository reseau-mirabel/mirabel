<?php

/** @var Controller $this */
/** @var Grappe[] $grappes */
/** @var ?int $level */

if (!isset($level)) {
	$level = 2;
}
?>

<table class="table grappe-index">
	<tbody>
		<?php
		$options = ($level === 2 ? ['style' => 'color:inherit;'] : []);
		foreach ($grappes as $grappe) {
			$countRevues = \processes\grappe\Stats::countByRevue($grappe->id);
			$lienRecherche = $countRevues['total'] . ' revues ';
			?>
			<tr>
				<td>
					<h<?= $level ?>>
						<div style="font-size: 16px; float: right">
						<?php
						echo CHtml::link(
							$lienRecherche . '<span class="glyphicon glyphicon-search"></span>',
							['/revue/search', 'q' => ['grappe' => $grappe->id]],
							['title' => "Rechercher les revues de cette grappe"]
						);
						?>
						</div>
						<?= CHtml::link(CHtml::encode($grappe->nom), $grappe->getSelfUrl(), $options) ?>
					</h<?= $level ?>>
					<div><?= $grappe->description ?></div>
				</td>
			</tr>
		<?php
		}
		?>
	</tbody>
</table>
