<?php

/** @var Controller $this */
/** @var processes\grappe\GrappeForm $formData */

assert($this instanceof Controller);

$this->pageTitle = "Nouvelle grappe";
$grappe = $formData->getGrappe();
if (Yii::app()->user->checkAccess('admin')) {
	$this->breadcrumbs = [
		"Grappes" => ['/grappe/index'],
		"Administrer les grappes" => ['/grappe/admin'],
	];
} else {
	$this->breadcrumbs = [
		"Partenaires" => ['/partenaire/index'],
		$grappe->partenaire->nom => ['/partenaire/view', 'id' => $grappe->partenaireId],
		"admin" => ['view-admin', 'id' => $grappe->partenaire->id],
		"Administrer mes grappes" => ['/grappe/partenaire', 'id' => $grappe->partenaireId],
	];
}
$this->breadcrumbs[] = "Créer";
?>

<h1>Nouvelle grappe</h1>

<p class="alert alert-info">
	Une grappe est essentiellement une liste de titres.
	Ce contenu peut être choisi par deux méthodes : par titre (choix statique) ou par recherche (choix dynamique).
	Le résultat sera une liste fusionnant ces deux sources.
	L'affichage public sera soit cette liste de titres, soit la liste des revues correspondantes.
</p>

<?= $this->renderPartial('_form', ['formData' => $formData]) ?>
