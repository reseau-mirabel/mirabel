<?php

/** @var ServiceController $this */
/** @var models\forms\ContactServiceForm $model */
/** @var Titre $titre */
/** @var string $bodyStart */

use models\forms\ContactForm;

assert($this instanceof ServiceController);

$this->pageTitle = Yii::app()->name . ' - Contact';
$this->breadcrumbs = [
	'Revues' => ['/revue/index'],
	$titre->titre => ['/revue/view', 'id' => $titre->revueId],
	'Signaler sur un accès importé',
];
?>

<h1>Signaler un problème sur les accès importés</h1>


<p>
	Ce formulaire vous permet de nous signaler un problème sur un accès en ligne qui est importé automatiquement.
	Si l'erreur est confirmée nous ferons remonter votre signalement en amont de Mir@bel afin de pouvoir la corriger.
</p>

<div class="form">
	<?php
	$form = $this->beginWidget(
		'BootActiveForm',
		[
			'id' => 'contact-form',
			'type' => 'horizontal',
			'enableClientValidation' => true,
			'clientOptions' => [
				'validateOnSubmit' => true,
			],
			'htmlOptions' => ["autocomplete" => "off"],
		]
	);
	/** @var BootActiveForm $form */
	?>

	<?php
	echo $form->errorSummary($model);

	echo $form->textFieldRow($model, 'name', ['class' => 'span4']);
	echo CHtml::tag('div', ['class' => "contact-outside"], $form->textFieldRow($model, 'email', ['tabindex' => 10, 'autocomplete' => 'off', 'type' => 'email'])); // fake
	echo $form->textFieldRow($model, 'emailr', ['class' => 'span4', 'type' => 'email']);
	echo CHtml::tag('div', ['class' => "contact-outside"], $form->textFieldRow($model, 'subject', ['tabindex' => 10, 'autocomplete' => 'off'])); // fake
	echo $form->textFieldRow($model, 'subjectr', ['class' => 'span6', 'readonly' => 'readonly']);
	echo '<div class="control-group">'
		. $form->label($model, 'body', ['class' => "control-label"])
		. '<div class="controls">'
		. '<div class="span6" title="Ce texte sera inséré au début du message pour préciser quel accès en ligne est concerné.">' . CHtml::encode($bodyStart) . '</div>'
		. $form->textArea(
			$model,
			'body',
			['rows' => 6, 'class' => 'span6', 'style' => 'clear: left;', 'placeholder' => "Merci de compléter le texte ci-dessus en précisant quel accès en ligne est concerné."]
		)
		. '</div>'
		. '</div>';
	if (CCaptcha::checkRequirements()) {
		echo '<div class="controls">';
		$form->widget('CCaptcha', ['captchaAction' => 'site/captcha']);
		echo "</div>";
		echo $form->textFieldRow($model, 'verifyCode');
		echo '<div class="controls">'
			. "Veuillez saisir le résultat du calcul ci-dessus."
			. "</div>";
	}

	Yii::app()->clientScript->registerScript(
		'fakeSubject',
		'document.querySelector("#ContactServiceForm_subject").setAttribute("value", ' . json_encode(ContactForm::FAKE_DATA) . ');
		$("#contact-form").on("submit", function() {
			$("#ContactServiceForm_email").val("");
			return true;
		});
		'
	);
	?>

	<div class="form-actions">
		<noscript>
			<p class="alert alert-danger">Ce formulaire utilise JavaScript pour lutter contre le spam.</p>
		</noscript>
		<button type="submit" class="btn btn-primary">Envoyer</button>
	</div>

	<?php $this->endWidget(); ?>
</div>
