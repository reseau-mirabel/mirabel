<?php

use models\import\Normalize;

/** @var Controller $this */
/** @var Service $model */
/** @var bool $direct */
/** @var Intervention $intervention */
/** @var Titre[] $titres Empty if $model->titreId is set. */
/** @var bool $forceProposition */
/** @var bool $forbidCollectionsUpdate */

use components\SqlHelper;

assert($this instanceof Controller);

$assets = new widgets\ServiceFormAssets();
if ($titres) {
	$assets->setJournals($titres);
} elseif ($model->titreId) {
	$assets->addJournal($model->titre);
}
$assets->run();

$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'service-form',
		'enableAjaxValidation' => false,
		'hints' => Hint::model()->getMessages('Service'),
		'htmlOptions' => ['class' => 'well'],
		'type' => BootActiveForm::TYPE_HORIZONTAL,
	]
);
/** @var BootActiveForm $form */
?>

<div class="alert">
	<?php
	if ($direct) {
		echo "Les modifications seront appliquées immédiatement.";
	} else {
		echo "Les modifications que vous proposez devront être validées "
			,"par un membre partenaire de Mi@bel avant d'être appliquées.";
	}
	?>
</div>

<p class="help-block">Les champs avec <span class="required">*</span> sont obligatoires.</p>

<?php
echo $form->errorSummary($model);
if (Yii::app()->user->checkAccess("avec-partenaire")) {
	echo UrlFetchableValidator::toggleUrlChecker($model);
}
if ($model->titreId) {
	$this->renderPartial(
		'/intervention/_errors',
		[
			'form' => $form,
			'intervention' => $intervention,
			'model' => $model->titre->revue,
		]
	);
}
?>
<fieldset>
	<legend>Accès en ligne</legend>
<?php
if ($model->isNewRecord) {
	if ($titres) {
		$titresById = array_map(fn (\Titre $t) => $t->getFullTitleWithPerio(), $titres);
		echo $form->dropDownListRow($model, 'titreId', $titresById, ["class" => "span6"]);
	} else {
		$this->renderPartial(
			'/global/_autocompleteField',
			[
				'model' => $model,
				'attribute' => 'titreId',
				'url' => '/titre/ajax-complete',
				'form' => $form,
				'value' => ($model->titre === null ? '' : $model->titre->titre),
			]
		);
	}
	$this->renderPartial(
		'/global/_autocompleteField',
		[
			'model' => $model,
			'attribute' => 'ressourceId',
			'url' => '/ressource/ajax-complete',
			'urlParams' => ['noimport' => 1],
			'form' => $form,
			'value' => ($model->ressource === null ? '' : $model->ressource->nom),
		]
	);
	if (!Yii::app()->user->checkAccess("avec-partenaire")) {
		echo "<div style=\"margin-bottom: 1.5ex;\">Si la ressource n'existe pas encore dans Mir@bel, "
			. CHtml::link("proposez sa création", ['ressource/create'])
			. " en indiquant dans le champ commentaire "
			. "quel accès en ligne et quelle revue vous souhaitiez signaler.</div>";
	} else {
		echo "<div style=\"margin-bottom: 1.2ex; margin-left: 180px;\">Si la ressource n'existe pas encore dans Mir@bel, "
			. CHtml::link("créer une nouvelle ressource", ['ressource/create'], ['target' => '_blank', 'title' => "nouvel onglet"])
			. ".</div>";
	}
} else {
	// modifying an existing Service record
	if ($titres) {
		$titresById = array_map(fn (\Titre $t) => $t->getFullTitleWithPerio(), $titres);
		echo $form->dropDownListRow($model, 'titreId', $titresById, ["class" => "span6"]);
	} else {
		?>
		<div class="control-group ">
			<label class="control-label"><?= $model->getAttributeLabel('titreId'); ?></label>
			<div class="controls">
				<span class="uneditable-input span8"><?= $model->titre->getFullTitle(); ?></span>
			</div>
		</div>
	<?php
	}
	if (!Yii::app()->user->checkAccess("avec-partenaire") || $model->ressource->hasCollections()) {
		echo $form->hiddenField($model, 'ressourceId'); ?>
	<div class="control-group ">
		<label class="control-label"><?= $model->getAttributeLabel('ressourceId'); ?></label>
		<div class="controls">
			<span class="uneditable-input span8"><?= CHtml::encode($model->ressource->getFullName()) ?></span>
		</div>
	</div>
		<?php
	} else {
		$this->renderPartial(
			'/global/_autocompleteField',
			[
				'model' => $model,
				'attribute' => 'ressourceId',
				'url' => '/ressource/ajax-complete',
				'urlParams' => ['noimport' => 1],
				'form' => $form,
				'value' => (empty($model->ressourceId) ? '' : $model->ressource->nom),
			]
		);
		echo "<div style=\"margin-bottom: 1.2ex; margin-left: 180px;\">Si la ressource n'existe pas encore dans Mir@bel, "
			. CHtml::link("créer une nouvelle ressource", ['ressource/create'], ['target' => '_blank', 'title' => "nouvel onglet"])
			. ".</div>";
	}
}
$isAdminCollections = Yii::app()->user->checkAccess('collection/admin');
if (!empty($model->ressourceId)) {
	$ressourceCollections = $model->ressource->getCollectionsDiffuseur(false, false);
	if ($forbidCollectionsUpdate) {
		// affichage fixe
		if ($model->collections) {
			?>
			<div class="control-group ">
				<label class="control-label">Collections du diffuseur</label>
				<div class="controls">
					<ul>
						<?php
						foreach ($model->collections as $c) {
							echo CHtml::tag(
								"li",
								[],
								CHtml::encode($c->nom)
									. " "
									. ($c->description ? CHtml::tag('i', ['class' => 'icon-info-sign', 'title' => $c->description], '') : '')
							);
						} ?>
					</ul>
				</div>
			</div>
			<?php
		}
	} elseif ($ressourceCollections) {
		// modification possible dans une liste fixée de collections
		$colls = [];
		$collectionAppliedIds = $model->id
			? SqlHelper::sqlToPairs("SELECT collectionId, 1 FROM Service_Collection WHERE serviceId = {$model->id}")
			: [];
		foreach (array_merge($ressourceCollections, $model->collections) as $c) {
			if (!isset($colls[$c->id])) {
				if (!$c->visible) {
					$c->nom .= " (non visible)";
				}
				if ($c->importee) {
					$c->nom .= " (importée)";
				}
				$colls[$c->id] = [
					'label' => $c->nom,
					'id' => $c->id,
					'checked' => !empty($collectionAppliedIds[$c->id]),
					'hint' => $c->description,
					'disabled' => (!$c->visible || $c->importee) && !$isAdminCollections,
				];
			}
		}
		usort($colls, function ($a, $b) {
			return strcmp($a['label'], $b['label']);
		});
		echo '<div id="service-collections-dynamic" data-collections="'
			. CHtml::encode(json_encode($colls))
			. '"></div>';
	}
} else {
	// modification possible dans une liste dynamique de collections
	echo '<div id="service-collections-dynamic" data-excludeimport="' . ($isAdminCollections ? 0 : 1) . '"></div>';
}
?>
</fieldset>

<fieldset>
	<legend>URLs</legend>
	<?php
	echo $form->textFieldRow($model, 'url', ['class' => 'span8', 'type' => 'url']);
	echo $form->textFieldRow($model, 'alerteRssUrl', ['class' => 'span8']);
	echo $form->textFieldRow($model, 'alerteMailUrl', ['class' => 'span8']);
	echo $form->textFieldRow($model, 'derNumUrl', ['class' => 'span8']);
	?>
</fieldset>

<fieldset>
	<legend>Détails</legend>
	<?php
	echo $form->dropDownListRow($model, 'type', Service::$enumType, ['empty' => '']);
	echo $form->dropDownListRow($model, 'acces', Service::$enumAcces, ['empty' => '']);
	if ($model->notes) {
		?>
		<div class="control-group ">
			<label class="control-label">Notes du fournisseur</label>
			<div class="controls" style="line-height: 28px;">
				<?= CHtml::encode($model->notes) ?>
			</div>
		</div>
		<?php
	}
	?>
</fieldset>

<fieldset>
	<legend>Couverture</legend>
	<?php
	echo $form->checkBoxRow($model, 'lacunaire', ['label-position' => 'left']);
	echo $form->checkBoxRow($model, 'selection', ['label-position' => 'left']);
	?>
</fieldset>

<fieldset>
	<legend>Début de l'accès en ligne</legend>
	<?php
	echo $form->textFieldRow($model, 'dateBarrDebut');
	?>
	<div class="volno input-prepend">
		<div class="control-label">Format numérique (info interne)</div>
		<div class="controls">
			<span class="add-on">Vol. </span>
			<?php
			echo $form->textFieldInline($model, 'volDebut', ['prepend' => 'Vol. ', 'class' => 'input-large volno-num vol']);
			?>
			,
			<span class="add-on">no </span>
			<?php
			echo $form->textFieldInline($model, 'noDebut', ['prepend' => 'no ', 'class' => 'input-large volno-num no']);
			?>
		</div>
		<div style="clear:left;"></div>
		<?php
		echo $form->textFieldRow(
				$model,
				'numeroDebut',
				['class' => 'input-xxlarge joined', 'labelOptions' => ['label' => 'Format textuel (affichage public)']]
			);
		?>
	</div>
</fieldset>

<fieldset>
	<legend>Fin de l'accès en ligne</legend>
	<?php
	echo $form->textFieldRow($model, 'dateBarrFin');
	?>
	<div class="volno input-prepend">
		<div class="control-label">Format numérique (info interne)</div>
		<div class="controls">
			<span class="add-on">Vol. </span>
			<?php
			echo $form->textFieldInline($model, 'volFin', ['prepend' => 'Vol. ', 'class' => 'input-large volno-num vol']);
			?>
			,
			<span class="add-on">no </span>
			<?php
			echo $form->textFieldInline($model, 'noFin', ['prepend' => 'no ', 'class' => 'input-large volno-num no']);
			?>
		</div>
		<div style="clear:left;"></div>
		<?php
		echo $form->textFieldRow(
				$model,
				'numeroFin',
				['class' => 'input-xxlarge joined', 'labelOptions' => ['label' => 'Format textuel (affichage public)']]
			);
		?>
	</div>
</fieldset>

<fieldset>
	<legend>Date barrière</legend>
	<?php
	echo $form->textFieldRow($model, 'dateBarrInfo', ['class' => 'input-xxlarge']);
	if ($model->embargoInfo) {
		?>
		<p>
			L'accès est importé automatiquement, et notamment son champ <em>embargo info</em>, non modifiable.
		</p>
		<div class="control-group ">
			<label class="control-label">embargo_info</label>
			<div class="controls" style="line-height: 28px;">
				<?= $model->embargoInfo ?>
				<em>(<?= Normalize::getReadableKbartEmbargo($model->embargoInfo) ?>)</em>
			</div>
		</div>
		<?php
	}
	if (Yii::app()->user->checkAccess('service/admin')) {
		echo $form->textFieldRow($model, 'embargoInfo', ['append' => ' réservé aux admins']);
	}
	?>
</fieldset>

<?php
$this->renderPartial(
		'/intervention/_contact',
		[
			'form' => $form,
			'intervention' => $intervention,
			'canForceValidating' => true,
			'forceProposition' => $forceProposition,
		]
	);
?>

<div class="form-actions">
	<?php
$this->widget(
	'bootstrap.widgets.BootButton',
	[
		'buttonType' => 'submit',
		'type' => 'primary',
		'label' => $model->isNewRecord ?
			($direct ? 'Créer' : 'Proposer cette création')
			: ($direct ? 'Modifier' : 'Proposer cette modification'),
	]
);
?>
</div>

<?php
$this->endWidget();

if (!$model->isNewRecord && $model->isLastServiceOfHisRessource()) {
	Yii::app()->clientScript->registerScript('serviceFormConfirmation', <<<EOJS
		const initialRessourceId = {$model->ressourceId};
		$('#service-form').on('submit', function (){
			if (parseInt($('#Service_ressourceId').val()) !==initialRessourceId){
				return confirm('Attention : Modifier la ressource de cet accès rendra l\'ancienne ressource orpheline')
			}
			return true;
		})
		EOJS
	);
}
?>
