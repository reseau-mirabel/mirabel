<?php

/** @var Controller $this */
/** @var Service $model */
/** @var bool $direct */
/** @var Intervention $intervention */
/** @var Titre[] $titres Empty if $model->titreId is set. */
/** @var bool $forceProposition */
/** @var bool $forbidCollectionsUpdate */

assert($this instanceof ServiceController);

$this->breadcrumbs = [];
if (!empty($model->titreId)) {
	$this->breadcrumbs = [
		'Revues' => ['/revue/index'],
		$model->titre->getFullTitle() => ['/revue/view', 'id' => $model->titre->revueId],
	];
} elseif (!empty($model->ressourceId)) {
	$this->breadcrumbs = [
		'Ressource' => ['/ressource/index'],
		$model->ressource->getFullName() => ['/ressource/view', 'id' => $model->ressourceId],
	];
}
$this->breadcrumbs[] = 'Modifier un accès en ligne';

if (Yii::app()->user->checkAccess("avec-partenaire")) {
	$orphan = '';
	if ($model->isLastServiceOfHisRessource()) {
		$orphan = "\nSupprimer cet accès rendra la ressource orpheline";
	}
	\Yii::app()->getComponent('sidebar')->menu = [
		['label' => 'Supprimer cet accès', 'url' => '#',
			'linkOptions' => [
				'submit' => ['delete', 'id' => $model->id],
				'confirm' => "Êtes vous certain de vouloir supprimer ceci ?\n" . $orphan,
			],
		],
	];
}
?>

<h1>Modifier un accès en ligne</h1>

<?php
echo $this->renderPartial(
	'_form',
	[
		'model' => $model,
		'direct' => $direct,
		'intervention' => $intervention,
		'titres' => $titres,
		'forceProposition' => $forceProposition,
		'forbidCollectionsUpdate' => $forbidCollectionsUpdate,
	]
);
?>
