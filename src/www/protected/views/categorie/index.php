<?php

/** @var Controller $this */
/** @var CDataProvider $dataProvider */
assert($this instanceof Controller);

$this->pageTitle = "Thématique des revues de Mir@bel";

$jstreeConfig = new \widgets\jstree\JsTreeCore();
$jstreeConfig->data = ['url' => $this->createUrl('/categorie/ajaxNode')];
$jstreeConfig->translateToFr();
$jstree = new widgets\jstree\JsTree($jstreeConfig, ["search", "sort", "state", "types", "revues"]);
$jstree->types = [
	'root' => [
		'icon' => '/images/icons/glyphicons-318-tree-deciduous.png',
	],
	'candidat' => [
		'icon' => '/images/icons/glyphicons-195-circle-question-mark.png',
	],
	'refus' => [
		'icon' => '/images/icons/glyphicons-200-ban-circle.png',
	],
];
$jstree->init(".jstree");

$jsUrl = Yii::app()->assetManager->publish(__DIR__ . '/js/index-tree.js');
Yii::app()->clientScript->registerScriptFile($jsUrl);
?>

<h1><?= $this->pageTitle ?></h1>

<?php
if (!Yii::app()->params->itemAt('displayThemesOnHomePage')) { // Cf fichier src/www/protected/config/local.php
	echo '<section class="thematique"><h2>Domaines principaux</h2>';
	$this->renderPartial('_liste');
	echo '</section><br />';
}
?>

<section>
	<h2>Arbre thématique</h2>
	<form id="jstree-search" class="form-search">
		<div class="input-append">
			<input type="search" id="jstree-search-q" placeholder="Recherche dans les titres et descriptions des thèmes" class="input-xxlarge" />
			<button type="submit" class="btn">Chercher</button>
		</div>
	</form>

	<?php if (!Yii::app()->user->checkAccess("avec-partenaire")) { ?>
		<p class="alert alert-warning">
			Les revues de Mir@bel sont classifiées par thème.
			<br />
			Cet arbre dépliable permet de visualiser l'organisation des thèmes
			et de rebondir sur les revues auxquelles un thème a été attribué.
		</p>
	<?php } else { ?>
		<p class="alert alert-warning">
			Cet arbre dépliable permet de visualiser l'organisation des thèmes.
			Les thèmes candidats, en attente de confirmation, sont préfixés d'une icône en point d'interrogation.
		</p>
	<?php } ?>
	<div id="categorie-tree" class="jstree avec-revues" data-revues-direct="<?= Yii::app()->user->checkAccess("avec-partenaire") ? "1" : "0" ?>"></div>
</section>
