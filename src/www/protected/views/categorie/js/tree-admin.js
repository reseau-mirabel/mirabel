
jQuery(function($) {

var localeCompare = new Intl.Collator("fr").compare;
$.jstree.defaults.sort = function(a, b) {
	localeCompare(this.get_text(a), this.get_text(b));
};

$(".jstree-edit, .jstree").on("click", ".jstree-edit-create", function() {
	var ref = $("#categorie-tree").jstree(true);
	var sel = ref.get_selected();
	if (!sel.length) {
		return false;
	}
	var url = ref.urls.create;
	if (/\?/.test(url)) {
		url += '&parentId=' + encodeURIComponent(sel[0]);
	} else {
		url += '?parentId=' + encodeURIComponent(sel[0]);
	}
	document.location.href = url;
});
$(".jstree-edit, .jstree").on("click", ".jstree-edit-rename", function() {
	var ref = $("#categorie-tree").jstree(true);
	var sel = ref.get_selected();
	if (!sel.length) {
		return false;
	}
	ref.edit(sel);
});
$(".jstree-edit, .jstree").on("click", ".jstree-edit-update", function() {
	var ref = $("#categorie-tree").jstree(true);
	var sel = ref.get_selected();
	if (!sel.length) {
		return false;
	}
	var url = ref.urls.update;
	if (/\?/.test(url)) {
		url += '&id=' + encodeURIComponent(sel[0]);
	} else {
		url += '?id=' + encodeURIComponent(sel[0]);
	}
	document.location.href = url;
});
$(".jstree-edit, .jstree").on("click", ".jstree-edit-delete", function() {
	var ref = $("#categorie-tree").jstree(true);
	var sel = ref.get_selected();
	if (!sel.length) {
		return false;
	}
	var node = ref.get_node(sel[0]);
	if (typeof node.children === "object" && node.children.length > 0) {
		showTreeMessage("error", "Impossible de supprimer un thème ayant des fils.");
		return false;
	}
	if (confirm("Êtes-vous sûr de vouloir supprimer ce thème « " + node.text + " » ?")) {
		ref.delete_node(node);
	}
});

$("#categorie-tree").on("create_node.jstree", function(e, data) {
	// data: node, parent, position
	var ref = data.instance;
	$.ajax({
		data: {
			ajax: 1,
			Categorie: {
				categorie: data.node.text,
				parentId: data.parent,
				role: "public"
			}
		},
		error: function(xh, textStatus, errorThrown) {
			console.log("Erreur réseau ? Le nouveau thème n\'a pas été enregistré.");
			console.log(textStatus + " / " + errorThrown);
			alert("Erreur réseau ? Le nouveau thème n\'a pas été enregistré.");
		},
		success: function(result) {
			console.log("Nouveau thème enregistré par AJAX");
			console.log(result);
			showTreeMessage(result.success, result.message);
			if (result.success === "success") {
				ref.set_id(data.node, result.node.id);
			} else {
				ref.delete_node(data.node);
			}
		},
		type: "POST",
		url: ref.urls.create
	});
});
$("#categorie-tree").on("rename_node.jstree", function(e, data) {
	// data: node, text, old
	var ref = data.instance;
	if (data.text === "Nouveau nœud") {
		showTreeMessage("error", "Ce nom de thème n\'est pas valide.");
		$("#categorie-tree").jstree(true).edit(data.node);
		return false;
	}
	$.ajax({
		data: {
			ajax: 1,
			Categorie: {
				id: data.node.id,
				categorie: data.text,
				parentId: data.parent,
				role: "public"
			}
		},
		error: function(xh, textStatus, errorThrown) {
			console.log("Erreur réseau ? Le thème n\'a pas été enregistré.");
			console.log(textStatus + " / " + errorThrown);
			alert("Erreur réseau ? Le thème n\'a pas été enregistré.");
		},
		success: function(result) {
			console.log("thème enregistré par AJAX");
			console.log(result);
			showTreeMessage(result.success, result.message);
			if (result.success !== "success") {
				ref.refresh_node(data.node);
			}
		},
		type: "POST",
		url: ref.urls.rename
	});
});
$("#categorie-tree").on("delete_node.jstree", function(e, data) {
	// data: node, parent
	var ref = data.instance;
	$.ajax({
		data: {
			ajax: 1,
			id: data.node.id
		},
		error: function(xh, textStatus, errorThrown) {
			console.log("Erreur réseau ? Le thème n\'a pas été supprimé.");
			console.log(textStatus + " / " + errorThrown);
			alert("Erreur réseau ? Le thème n\'a pas été supprimé. Recharger la page.");
		},
		success: function(result) {
			console.log("Thème supprimé par AJAX");
			console.log(result);
			if (result.success !== "success") {
				ref.refresh_node(data.parent);
			}
			showTreeMessage(result.success, result.message);
		},
		type: "POST",
		url: ref.urls.delete
	});
});

function showTreeMessage(success, message) {
	$("#jstree-edit-message")
		.prop("class", "alert alert-" + success)
		.html(message)
		.show();
	setTimeout(function() { $("#jstree-edit-message").prop("class", "").html(" "); }, 4000);
}

var inlineActions = $('<span class="jstree-edit-inline"></span>')
		.append('<button type="button" class="jstree-edit-create" title="Ajouter un nouveau thème sous le thème sélectionné">Aj.</button> ')
		.append('<button type="button" class="jstree-edit-rename" title="Renommer ce thème (modification à la volée)">Ren.</button> ')
		.append('<button type="button" class="jstree-edit-update" title="Modifier ce thème (formulaire complet)">Mod.</button> ')
		.append('<button type="button" class="jstree-edit-delete" title="Supprimer">Suppr.</button>');
$("#categorie-tree").on("select_node.jstree", function(e, data) {
	//data: node, select, event
	var ref = data.instance;
	var jqNode = ref.get_node(data.node.id, true);
	if (data.node.data.updatable) {
		jqNode.prepend(inlineActions.clone());
		if (data.node.parents.length === 3) {
			jqNode.find(".jstree-edit-create").prop("disabled", "true").prop("title", "La profondeur est déjà maximale.");
		}
		if (data.node.children.length > 0) {
			jqNode.find(".jstree-edit-delete").prop("disabled", "true").prop("title", "Suppression impossible à cause des thèmes-files.");
		}
	}
	return true;
});
$("#categorie-tree").on("deselect_all.jstree", function(e, data) {
	//data: node, select, event
	data.instance.get_container().find(".jstree-edit-inline").remove();
	return true;
});

$("#categorie-tree").on("search.jstree", function(e, data) {
	if (data.nodes.length === 1) {
		data.instance.deselect_all();
		data.instance.select_node(data.nodes[0]);
	}
});

});
