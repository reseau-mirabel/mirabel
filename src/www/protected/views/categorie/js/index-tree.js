
var localeCompare = new Intl.Collator("fr").compare;
$.jstree.defaults.sort = function (a, b) {
	localeCompare(this.get_text(a), this.get_text(b));
};

$.jstree.plugins.revues = function (options, parent) {
	var revuesDirect = parseInt($("#categorie-tree").attr('data-revues-direct'));
	this.redraw_node = function (obj, deep, callback, force_draw) {
		obj = parent.redraw_node.call(this, obj, deep, callback, force_draw);
		if (obj) {
			var node = this.get_node($(obj).attr("id"));
			if (node && node.data && node.data.depth > 1 && "numRevues" in node.data && (node.data.numRevues > 0 || node.data.numRevuesRec > 0)) {
				var title = revuesDirect ? getInfoRevuesMulti(node) : getInfoRevuesSimple(node);
				$(obj).prepend(' <span class="label label-revues">' + title + "</span>");
			}
		}
		return obj;
	};
};

function getInfoRevuesSimple(node) {
	var revuesTxt = ' revue' + (node.data.numRevuesRec > 1 ? "s" : "");
	return '<a href="' + htmlEscape(node.data.url) + '"' +
			' title="' + node.data.numRevuesRec + revuesTxt + ' avec ce thème ou un sous-thème">' +
			padLeft(5, node.data.numRevuesRec) + revuesTxt +
			"</a>";
}

function getInfoRevuesMulti(node) {
	var title = padLeft(5, node.data.numRevues) + " revue" + (node.data.numRevues > 1 ? "s" : " ");
	if ("search" in node.data) {
		title = ' <a href="' + htmlEscape(node.data.search) + '"' +
				' title="' + node.data.numRevues + ' revue' + (node.data.numRevues > 1 ? "s" : "") + ' avec ce thème">' +
				title +
				"</a>";
	}
	if (node.data.hasChildren) {
		if ("url" in node.data) {
			title = '<i>(<a href="' + htmlEscape(node.data.url) + '" title="' + node.data.numRevuesRec + ' revues en intégrant les sous-thèmes">' +
					node.data.numRevuesRec +
					"</a>)</i>" +
					title;
		} else {
			title = "<i>(" + node.data.numRevuesRec + ")</i>" + title;
		}
	}
	return title;
}
