<?php

/** @var Controller $this */

assert($this instanceof Controller);

$discipline = Categorie::model()->findByAttributes(['categorie' => 'Discipline', 'profondeur' => 1]);
$themes = Categorie::model()->with(['categorieStats', 'children'])->findAllByAttributes(['parentId' => $discipline->id]);
?>
<ul id="categorie-liste">
<?php foreach ($themes as $theme) { ?>
	<li>
		<?= CHtml::link(
			CHtml::encode($theme->categorie),
			$theme->getSelfUrl(),
			[
				'title' => ($theme->categorieStats ? $theme->categorieStats->numRevuesIndRec : 0) . " revues.\n"
					. "Sous-thématiques :\n- "
					. join("\n- ", array_map(function ($x) {
						return $x->categorie;
					}, $theme->children)),
			]
		) ?>
	</li>
<?php } ?>
</ul>
