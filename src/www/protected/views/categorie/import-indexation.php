<?php

/** @var Controller $this */
/** @var \models\forms\IndexationImportForm $formData */
/** @var \processes\categorie\IndexationImport $import */

use components\HtmlTable;
use models\forms\IndexationImportForm;
use processes\categorie\IndexationImport;

assert($this instanceof Controller);

$this->pageTitle = "Import d'indexation";
$this->breadcrumbs[] = "Import d'indexation";

$separators = [
	";" => ";",
	"," => ",",
	'\t' => "tabulation",
	"|" => "|",
];
?>

<h1>Importer une indexation</h1>

<div class="alert alert-notice">
	<ul>
		<li>Deux possibilités :
			<ol>
				<li>un CSV à 2 colonnes : identifiant de titre (ISSN ou ID Mir@bel), et mot-clé ;</li>
				<li>un fichier KBART (tabulation comme séparateur) avec une colonne <em>discipline</em> où les mots-clés sont séparés par <code>;</code>.</li>
			</ol>
		</li>
		<li>Chaque mot-clé doit figurer dans le vocabulaire choisi, sinon il sera ignoré.</li>
		<li>
			<em>Si la case "Remplace les indexations de ce même vocabulaire" est cochée,</em>
			alors toutes les indexations précédentes sous ce même vocabulaire seront effacées pour être remplacées par le résultat de l'import.
			Dans le cas contraire, l'import ne fonctionnera qu'en ajout.
		</li>
	</ul>
</div>

<div class="form">
	<?php
	/** @var BootActiveForm $form */
	$form = $this->beginWidget(
		'BootActiveForm',
		[
			'id' => 'import-indexation-form',
			'type' => 'horizontal',
			'enableClientValidation' => false,
			'htmlOptions' => ['enctype' => 'multipart/form-data'],
			'hints' => Hint::model()->getMessages('IndexationImportForm'),
		]
	);
	?>

	<?php
	echo $form->errorSummary($formData);

	echo $form->dropDownListRow($formData, 'vocabulaireId', CHtml::listData(Vocabulaire::model()->findAll(), 'id', 'titre'), ['prompt' => '-']);
	echo $form->fileFieldRow($formData, 'csvfile', ['class' => 'span10']);
	echo $form->dropDownListRow($formData, 'separator', $separators);
	echo $form->checkBoxRow($formData, 'clean');
	?>
	<div class="form-actions">
		<button type="submit" class="btn btn-primary">Importer ce fichier</button>
	</div>
	<?php $this->endWidget(); ?>
</div>

<?php
$log = $import->getLog();
if ($log["lignes lues"] > 0 || $import->getErrors()) {
	?>
<div class="logs">
	<h2>Logs de l'import</h2>

	<?php if ($import->getErrors()) { ?>
	<div class="alert alert-error">
		<?= join('<br />', $import->getErrors()) ?>
	</div>
	<?php } else { ?>
	<p class="alert alert-success">
		L'opération s'est déroulée sans accroc notable.
	</p>
	<?php } ?>

	<table class="table">
		<tbody>
			<tr>
				<td>Lignes lues</td>
				<td><?= $log["lignes lues"] ?></td>
			</tr>
			<tr>
				<td>Lignes correctes</td>
				<td><?= $log["lignes ok"] ?></td>
			</tr>
			<tr>
				<td>Lignes avec erreur</td>
				<td><?= $log["lignes lues"] - $log["lignes ok"] ?></td>
			</tr>
			<tr>
				<td>Indexations pré-existantes</td>
				<td><?= $log["indexations pré-existantes"] ?></td>
			</tr>
			<tr>
				<td>Indexations effacées</td>
				<td><?= $log["indexations effacées"] ?></td>
			</tr>
			<tr>
				<td>Indexations enregistrées</td>
				<td><?= $log["indexations enregistrées"] ?></td>
			</tr>
		</tbody>
	</table>

	<?php
	if ($log["Indexation ajoutée"]) {
		?>
		<h3>Indexations ajoutées <?= ($import->removeOld ? "" : " ou inchangées")?></h3>
		<?php
		echo HtmlTable::build(
			array_map(function ($x) {
				return explode(" | ", $x);
			}, $log["Indexation ajoutée"]),
			["ID titre", "Titre", "ID revue", "Thème Mir@bel", "Données du CSV"]
		)->addClass('exportable')->toHtml();
	} ?>

	<?php
	if ($log["Titre non trouvé"]) {
		?>
		<h3>Titres non trouvés dans Mir@bel</h3>
		<?php
		echo HtmlTable::build($log["Titre non trouvé"], ["Identifiant"])->addClass('exportable')->toHtml();
	} ?>

	<?php
	if ($log["Alias non déclaré"]) {
		?>
		<h3>Mots-clés absents de ce vocabulaire</h3>
		<?php
		$mots = array_filter($log["Alias non déclaré"]);
		sort($mots);
		echo HtmlTable::build(array_unique($mots), ["Mot-clé"])->addClass('exportable')->toHtml();
	} ?>

	<?php
	$parsedCsv = $import->getParsedCsv();
	if ($parsedCsv) {
		?>
		<h3>Détails des avertissements et erreurs</h3>
		<pre><?= $parsedCsv ?></pre>
		<?php
	} ?>
</div>
<?php
}
