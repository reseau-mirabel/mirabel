<?php

/** @var Controller $this  */
/** @var Intervention $model */
/** @var string[] $emails */
/** @var string $body */
/** @var string $subject */
/** @var int $id*/

assert($this instanceof Controller);
$this->breadcrumbs = [
	'Interventions' => ['admin'],
	$id => ['intervention/view', 'id' => $id],
];


/** @var BootActiveForm */
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'action' => Yii::app()->createUrl($this->route, ['id' => $id]),
		'enableAjaxValidation' => false,
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'htmlOptions' => ['class' => 'well'],
	]
);

echo CHtml::openTag('div', ['class' => 'control-group']);
echo CHtml::label("Destinataires", 'emails', ["class" => "control-label"]);
echo CHtml::openTag('ul', ['id' => 'emails', 'class' => 'list-unstyled controls']);
foreach ($emails as $clef => $valeur) {
	if (is_numeric($clef)) {
		// Cas: ["email", "email", ...]
		echo CHtml::tag('li', [], CHtml::encode($valeur));
	} else {
		// Cas: ["email" => "nom", ...]
		$email = CHtml::encode($clef);
		$nom = CHtml::encode($valeur);
		echo CHtml::tag('li', [], "{$nom} - {$email}");
	}
}
echo CHtml::closeTag('ul');
echo CHtml::closeTag('div');

echo CHtml::openTag('div', ['class' => 'control-group']);
echo CHtml::label('Sujet du message', 'subject', ["class"=>"control-label"]);
echo CHtml::openTag('div', ['class' => 'controls']);
echo CHtml::textField('subject', $subject, ['class' => 'span6 ']);
echo CHtml::closeTag('div');
echo CHtml::closeTag('div');

echo CHtml::openTag('div', ['class' => 'control-group']);
echo CHtml::label('Corps du message', 'body', ["class"=>"control-label"]);
echo CHtml::openTag('div', ['class' => 'controls']);
echo CHtml::textArea('body', $body, ['class' => 'span12', 'rows' => '20']);
echo CHtml::closeTag('div');
echo CHtml::closeTag('div');
echo CHtml::submitButton('Envoyer', ['class' => 'btn btn-primary']);

$this->endWidget();
