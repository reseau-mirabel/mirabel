<?php

/** @var Controller $this */
/** @var \models\searches\InterventionSearch $model */

use components\SqlHelper;
use models\import\ImportType;

assert($this instanceof Controller);

$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'action' => Yii::app()->createUrl($this->route),
		'method' => 'get',
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'hints' => Hint::model()->getMessages('models\searches\InterventionSearch'),
	]
);
/** @var BootActiveForm $form */


echo '<button type="button" class="btn btn-default" id="toggle-extra-search">Recherche avancée…</button>';

echo '<div id="extra-search">';

$users = SqlHelper::sqlToPairs(<<<EOSQL
	SELECT u.id, CONCAT(p.nom, ' — ', u.nomComplet) AS name
	FROM Utilisateur u
		JOIN Partenaire p ON u.partenaireId = p.id
	ORDER BY p.nom, u.login
	EOSQL
);
$partenaires = SqlHelper::sqlToPairs(<<<EOSQL
	SELECT p.id, p.nom
	FROM Partenaire p
	ORDER BY p.nom
	EOSQL
);

echo $form->dropDownListRow($model, 'statut', Intervention::STATUTS, ['prompt' => '']);

echo $form->textFieldRow($model, 'ressource_nom');
echo $form->textFieldRow($model, 'titre_titre');
echo $form->textFieldRow($model, 'editeur_nom');
echo $form->textFieldRow($model, 'description');

echo $form->dropDownListRow($model, 'import', ImportType::getImportList(), ['prompt' => '']);

echo $form->textFieldRow($model, 'hdateProp', ['class' => 'span5']);
echo $form->textFieldRow($model, 'hdateVal', ['class' => 'span5']);
echo $form->textFieldRow($model, 'delai', ['class' => 'span3', 'placeholder' => "secondes"]);

if (Yii::app()->user->checkAccess('admin')) {
	echo $form->dropDownListRow($model, 'partenaireId', $partenaires, ['prompt' => '']);
	echo $form->dropDownListRow($model, 'utilisateurIdProp', ['0' => 'ANONYME'] + $users, ['prompt' => '', 'class' => 'input-block-level']);
	echo $form->dropDownListRow($model, 'utilisateurIdVal', $users, ['prompt' => '', 'class' => 'input-block-level']);
	echo $form->dropDownListRow($model, 'suiviPartenaireId', $partenaires, ['prompt' => '', 'class' => 'input-block-level']);
}
echo "</div>\n";

echo $form->checkBoxRow($model, 'suivi');
echo $form->checkBoxRow($model, 'auteur');
?>

<div class="form-actions">
	<button type="submit" class="btn btn-primary">Filtrer</button>
	<?= CHtml::link("Réinitialiser", ['/intervention/admin'], ['class' => 'btn btn-default']) ?>
	<?= CHtml::link("Exporter en CSV", ['/intervention/export', 'q' => array_filter($model->getAttributes())], ['class' => 'btn btn-default pull-right']) ?>
</div>

<?php $this->endWidget(); ?>
