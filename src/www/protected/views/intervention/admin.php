<?php

/** @var Controller $this */
/** @var \models\searches\InterventionSearch $model */

assert($this instanceof InterventionController);

$this->breadcrumbs = [
	'Interventions' => ['admin'],
	'Administration',
];

\Yii::app()->getComponent('sidebar')->menu = [];
?>

<h1>Interventions</h1>

<p>
	Vous pouvez saisir un opérateur de comparaison
	(<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
	ou <b>=</b>) au début de chaque zone de recherche afin de désigner le mode de comparaison.
</p>

<?php
$summary = $model->getSummary();
if ($summary) {
	?>
	<p class="alert alert-info">
		Filtres actifs : <?= CHtml::encode(join(" ", $summary)) ?>
	</p>
	<?php
}
?>

<?php
$this->renderPartial('_search', ['model' => $model]);

$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	[
		'id' => 'intervention-grid',
		'dataProvider' => $model->search(),
		'filter' => $model, // null to disable
		'columns' => $model->getColumnsHtml(),
		'ajaxUpdate' => false,
	]
);
?>

<?php
$this->loadViewJs(__FILE__);
