<?php

/** @var Controller $this */
/** @var InterventionDetail $detail */
/** @var array $change */

assert($this instanceof Controller);

if (isset($change['id'])) {
	['titreId' => $titreId, 'editeurId' => $editeurId] = $change['id'];
} else {
	$titreId = $change['after']['titreId'];
	$editeurId = $change['after']['editeurId'];
}
$titre = Titre::model()->findByPk((int) $titreId);
$editeur = Editeur::model()->findByPk((int) $editeurId);
?>
<h3>Relation titre-éditeur : <?= strtolower($detail->getLabel($change['operation'])) ?></h3>

	<?php
if (isset($change['msg'])) {
	echo "<p>" . CHtml::encode($change['msg']) . '</p>';
}
?>

<div>
	<div>
		<?php
		if ($editeur) {
			echo "Éditeur : ID {$editeurId} = {$editeur->getSelfLink()}";
		} else {
			echo "Éditeur : ID {$editeurId} (éditeur supprimé depuis)";
		}
		?>
	</div>
	<div>
		<?php
		if ($titre) {
			echo "Titre : ID {$titreId} = {$titre->getSelfLink()}";
		} else {
			echo "Titre : ID {$titreId} (titre supprimé depuis)";
		}
		?>
	</div>
	<div class="row-fluid">
		<?php
		$attributes = [
			['name' => 'ancien'],
			['name' => 'intellectuel'],
			['name' => 'commercial'],
			[
				'name' => 'role',
				'value' => function(TitreEditeur $te) {
					$rolesTE = TitreEditeur::getPossibleRoles();
					return $rolesTE[$te->role] ?? $te->role;
				}
			],
		];
		if (!empty($change['before'])) {
			$attributes = array_filter(
				$attributes,
				function ($a) use ($change) {
					return array_key_exists($a['name'], $change['before']);
				}
			);
		}
		?>
		<div class="span6">
			<?php
			if (!empty($change['before'])) {
				$before = new TitreEditeur();
				$before->unsetAttributes();
				$before->setAttributes($change['before'], false);
				if (!empty($change['after'])) {
					echo "<h4>Avant</h4>";
				}
				$this->widget(
					'bootstrap.widgets.BootDetailView',
					[
						'data' => $before,
						'attributes' => $attributes,
						'htmlOptions' => [
							'class' => "table table-striped table-bordered table-condensed",
						],
					]
				);
			}
			?>
		</div>
		<div class="span6">
			<?php
			if (!empty($change['after'])) {
				$after = new TitreEditeur();
				$after->unsetAttributes();
				$after->setAttributes($change['after'], false);
				if (!empty($change['before'])) {
					echo "<h4>Après</h4>";
				}
				$this->widget(
					'bootstrap.widgets.BootDetailView',
					[
						'data' => $after,
						'attributes' => array_filter(
							$attributes,
							function ($a) use ($change) {
								return array_key_exists($a['name'], $change['after']);
							}
						),
						'htmlOptions' => [
							'class' => "table table-striped table-bordered table-condensed",
						],
					]
				);
			}
			?>
		</div>
	</div>
</div>
