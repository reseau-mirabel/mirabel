<?php

/** @var Controller $this */
/** @var CModel $model */
/** @var ?Intervention $intervention */
/** @var CActiveForm $form */
assert($this instanceof Controller);

if (isset($intervention)) {
	if (method_exists($model, 'getSelfLink') && $intervention->getErrors('commentaire') && $intervention->contenuJson->isEmpty()) {
		$footer = "Pour annuler la saisie, retourner à " . $model->getSelfLink() . ".";
	} else {
		$footer = null;
	}
	echo $form->errorSummary($intervention, null, $footer);
}
