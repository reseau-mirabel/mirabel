<?php

use models\import\ImportType;

/** @var Controller $this */
/** @var Intervention $model */
/** @var bool $canValidate */
/** @var string $statut ""|"warning" */

assert($this instanceof Controller);

$this->pageTitle = "Intervention #{$model->id}";
$this->breadcrumbs = [
	'Interventions' => ['admin'],
	$model->id,
];

/** @var \components\WebSidebar */
$sidebar = \Yii::app()->getComponent('sidebar');
$sidebar->menu = [
	['label' => 'Liste des interventions', 'url' => ['admin']],
];
if ($canValidate) {
	if ($model->statut != 'accepté') {
		$sidebar->menu[] = [
			'encodeLabel' => false,
			'label' => $this->widget(
				'bootstrap.widgets.BootButton',
				[
					'label' => 'Accepter' . ($statut === 'warning' ? " de force" : ""),
					'url' => ['accept', 'id' => $model->id, 'force' => ($statut === 'warning' ? "1" : "")],
					'type' => 'primary',
					'size' => 'small',
				],
				true
			),
		];
	}
	if ($model->statut == 'attente') {
		$sidebar->menu[] = [
			'encodeLabel' => false,
			'label' => $this->widget(
				'bootstrap.widgets.BootButton',
				[
					'label' => 'Rejeter',
					'url' => ['reject', 'id' => $model->id],
					'type' => 'primary',
					'size' => 'small',
				],
				true
			),
		];
		if (Suivi::isTracked($model)) {
			$sidebar->menu[] = [
				'encodeLabel' => false,
				'label' => $this->widget(
					'bootstrap.widgets.BootButton',
					[
						'label' => 'Envoyer un rappel',
						'url' => ['send-reminder', 'id' => $model->id],
						'type' => 'primary',
						'size' => 'small',
					],
					true
				),
			];
		}
	} elseif ($model->statut === 'accepté' && !$model->getContent()->isEmpty()) {
		$sidebar->menu[] = [
			'encodeLabel' => false,
			'label' => $this->widget(
				'bootstrap.widgets.BootButton',
				[
					'label' => 'Annuler cette intervention',
					'url' => ['revert', 'id' => $model->id],
					'type' => 'primary',
					'size' => 'small',
				],
				true
			),
		];
	}
}
?>

<h1><em>Intervention #<?= $model->id ?></em></h1>

<h2><?= CHtml::encode($model->description) ?></h2>

<?php
$ressource = '';
if (isset($model->ressourceId)) {
	if ($model->ressource) {
		$ressource = $model->ressource->getSelfLink();
		$p = $model->ressource->getPartenairesSuivant();
		if ($p) {
			$ressource .= "<div>Suivie par";
			foreach ($p as $part) {
				$ressource .= "<li>" . $part->getSelfLink() . "</li>";
			}
			$ressource .= "</div>";
		}
	} else {
		$ressource = "[ressource supprimée]";
	}
}
$titre = '';
if (isset($model->titreId)) {
	if ($model->titre) {
		$titre = CHtml::link($model->titre->getFullTitle(), ['/titre/view', 'id' => $model->titreId]);
		$editeurs = $model->titre->editeurs;
		if (empty($editeurs)) {
			$titre .= " <div>Sans éditeur</div>";
		} else {
			$titre .= " <div>Éditeurs : <ul>";
			foreach ($editeurs as $e) {
				$titre .= "<li>" . CHtml::encode($e->nom) . "</li>";
			}
			$titre .= " </ul></div>";
		}
	} else {
		$titre = "[titre supprimé]";
	}
}
$revue = '';
if (isset($model->revueId)) {
	if ($model->revue) {
		$revue = $model->revue->getSelfLink();
		$p = $model->revue->getPartenairesSuivant();
		if ($p) {
			$revue.= "<div>Suivie par";
			foreach ($p as $part) {
				$revue.= "<li>" . $part->getSelfLink() . "</li>";
			}
			$revue.= "</div>";
		}
	} else {
		$revue = "[revue supprimée]";
	}
}
$editeur = '';
if (isset($model->editeurId)) {
	if ($model->editeur) {
		$editeur = $model->editeur->getSelfLink();
	} else {
		$editeur = "[éditeur supprimé]";
	}
}
$attributes = [
	'description',
	[
		'name' => 'ressourceId',
		'type' => 'raw',
		'value' =>  $ressource,
	],
	[
		'name' => 'revueId',
		'type' => 'raw',
		'value' => $revue,
	],
	[
		'name' => 'titreId',
		'type' => 'raw',
		'value' => $titre,
	],
	[
		'name' => 'editeurId',
		'type' => 'raw',
		'value' => $editeur,
	],
	[
		'label' => $model->getAttributeLabel('statut'),
		'value' => Intervention::STATUTS[$model->statut],
		'cssClass' => 'alert',
	],
];
if ($model->hdateProp === $model->hdateVal) {
	$attributes[] = ['label' => $model->getAttributeLabel('hdateProp'), 'value' => ''];
	$attributes[] = ['label' => $model->getAttributeLabel('utilisateurIdProp'), 'value' => ''];
} else {
	$attributes[] = 'hdateProp:datetime';
	$attributes[] = [
		'name' => 'utilisateurIdProp',
		'type' => 'raw',
		'value' => UtilisateurDecorator::fromId($model->utilisateurIdProp)
			->getFullHtmlLink(($model->import ? '<em>import</em>' : '<em>anonyme</em>'), true),
	];
}
array_push(
	$attributes,
	'ip',
	[
		'name' => 'email',
		'type' => 'email',
		'value' => $model->email ?: ($model->utilisateurIdProp && isset($model->utilisateurProp) ? $model->utilisateurProp->email : ''),
	],
	[
		'name' => 'commentaire',
		'type' => 'raw',
		'value' => strpos($model->commentaire, '<') === false ? nl2br(wordwrap($model->commentaire, 100)) : $model->commentaire,
		'cssClass' => 'alert alert-info intervention-commentaire',
	],
	'hdateVal:datetime',
	[
		'name' => 'utilisateurIdVal',
		'type' => 'raw',
		'value' => ($model->import && $model->hdateVal === $model->hdateProp) ? '-'
			: UtilisateurDecorator::fromId($model->utilisateurIdVal)->getFullHtmlLink("-", true),
	],
	[
		'name' => 'import',
		'value' => ImportType::getSourceName($model->import),
	]
);
if ($model->action) {
	$attributes[] = [
		'name' => 'action',
		'value' => Intervention::ACTIONS[$model->action],
	];
}
$this->widget(
	'bootstrap.widgets.BootDetailView',
	[
		'data' => $model,
		'attributes' => $attributes,
		'nullDisplay' => '<em>non validé</em>',
		'hideEmptyLines' => true,
	]
);
?>

<h2>Détail des modifications</h2>
<?php
$this->renderPartial('_detail', ['detail' => $model->contenuJson]);
?>
