<?php

/** @var Controller $this */
/** @var Intervention[] $interventions */
/** @var array $descriptions [title, comment] */
/** @var ?int $truncate */

assert($this instanceof Controller);

Yii::app()->getClientScript()->registerCssFile('/css/glyphicons.css');

if (empty($truncate)) {
	$truncate = 0;
}

if ($descriptions) {
	echo "<div>",
		"<strong>" . $descriptions[0] . ".</strong>",
		(count($descriptions) > 1 ? " {$descriptions[1]}." : ""),
		"</div>";
}
if (count($interventions) === 0) {
	echo '<p>Aucune intervention.</p>';
	return;
}

echo '<ul>';
$avecPartenaire = Yii::app()->user->checkAccess("avec-partenaire");
$position = 0;
foreach ($interventions as $intervention) {
	$position++;
	if ($truncate && $position > $truncate) {
		echo "<li>Plus de $truncate interventions…</li>";
		break;
	}
	echo '<li>' . $intervention->render()->linkWithDetails($avecPartenaire) . '</li>';
}
echo '</ul>';
