<?php

/** @var Controller $this */
/** @var models\searches\UtilisateurPolitique $model */

assert($this instanceof UtilisateurController);

$this->pageTitle = "Administration des utilisateurs-politiques";
$this->breadcrumbs = [
	'Politiques' => ['/admin/politique'],
	'Politiques - utilisateurs',
];
?>

<h1>Administration des utilisateurs-politiques</h1>

<?php if (Yii::app()->user->checkAccess('admin')) { ?>
<p>
	<?= CHtml::link("Administration de tous les utilisateurs", ['/utilisateur/admin', 'UtilisateurSearch' => ['special' => 'politiques']]) ?>
</p>
<?php } ?>
<p>
	Les utilisateurs membres d'un partenaire ne sont pas affichés.
	Si le rôle d'un utilisateur sur un éditeur est en attente de validation,
	le nom de l'éditeur est en italique.
</p>

<?php
$columns = [
	[
		'header' => "Nom complet",
		'name' => 'nom',
		'type' => 'raw',
		'value' => function (array $data) {
			return CHtml::link(
				CHtml::encode($data['nomComplet'] ?: "[effacé]"),
				['/utilisateur/view', "id" => (int) $data['id']],
				['title' => "Consulter le profil complet de l'utilisateur"]
			);
		},
		'htmlOptions' => ['class' => 'nom'],
	],
	[
		'header' => "email",
		'name' => 'email',
		'type' => 'email',
		'value' => function (array $data) {
			if ($data['nomComplet'] === '') {
				return '';
			}
			return $data['email'];
		},
		'htmlOptions' => ['class' => 'email'],
	],
	[
		'name' => 'actif',
		'filter' => ['Non', 'Oui'],
		'value' => function (array $data) {
			return (int) $data['actif'] ? 'oui' : 'non';
		},
	],
	[
		'header' => 'Éditeurs',
		'type' => 'raw',
		'value' => function (array $data) {
			$editeurs = array_map(
				function ($e) {
					if (strncmp($e, '#pending# ', 10) === 0) {
						return '<em class="role-pending">' . CHtml::encode(substr($e, 10)) . "</em>";
					}
					return CHtml::encode($e);
				},
				explode(' + ', $data['editeurs'])
			);
			return join(" + ", $editeurs) . ' '
				. CHtml::link(
					'<i class="icon-pencil"></i>',
					['/utilisateur/politiques-editeurs', 'id' => $data['id']],
					['class' => 'btn btn-small', 'title' => "Modifier la liste des éditeurs"]
				);
		},
	],
	[
		'header' => 'Der. connexion',
		'name' => 'derConnexion',
		'value' => function (array $data) {
			return $data['derConnexion'] ? date('Y-m-d H:i', $data['derConnexion']) : "";
		},
		'filter' => false,
	],
	[
		'header' => 'Modification',
		'name' => 'hdateModif',
		'value' => function (array $data) {
			return date('Y-m-d H:i', $data['hdateModif']);
		},
		'filter' => false,
	],
];

$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	[
		'id' => 'utilisateur-grid',
		'dataProvider' => $model->search(),
		'filter' => $model, // null to disable
		'columns' => $columns,
		'ajaxUpdate' => false,
	]
);
?>
