<?php

/** @var Controller $this */

use components\HtmlHelper;

assert($this instanceof Controller);

$this->pageTitle = "Administration";
$this->breadcrumbs = [
	'Administration de Mir@bel',
];
?>

<h1>
	Administration de Mir@bel
	<small>(permission <em>admin</em> requise)</small>
</h1>

<div class="flexstack">
	<div class="well">
		<h2>Paramètres</h2>
		<ul>
			<li><?= CHtml::link("Messages de suivi", ['/config', 'Config' => ['category' => 'alerte']]) ?></li>
			<li><?= CHtml::link("Serveur e-mail", ['/config', 'Config' => ['category' => 'email']]) ?> : SMTP et réglages techniques</li>
			<li><?= CHtml::link("Sherpa", ['/config', 'Config' => ['category' => 'sherpa']]) ?> : aide et traductions</li>
			<li><?= CHtml::link("Autres réglages", ['/config', 'Config' => ['category' => 'autres']]) ?> : contact, envois de mots de passe, RSS…</li>
			<li><?= CHtml::link("Paramètres fixés par le fichier <code>config/local.php</code>", ['/config/index']) ?></li>
		</ul>
	</div>

	<div class="well">
		<h2>Contenus</h2>
		<ul>
			<li><?= CHtml::link("Attributs de titres", ['/attribut/declare']) ?></li>
			<li><?= CHtml::link("Grappes", ['/grappe/admin']) ?></li>
			<li><?= CHtml::link("Sources de liens", ['/sourcelien']) ?></li>
			<li><?= CHtml::link("Thématique", ['/categorie/admin']) ?> : modifier les thèmes</li>
			<li><?= CHtml::link("Baromètre", ['/barometre/index']) ?></li>
		</ul>
	</div>

	<div class="well">
		<h2>Listes de contrôle</h2>
		<ul>
			<li><?= CHtml::link("Redirections", ['/redirection/index']) ?></li>
			<li><?= CHtml::link("APC incohérents", ['/verification/attribut-apc']) ?></li>
		</ul>
	</div>

	<div class="well">
		<h2>Gestion des partenaires</h2>
		<ul>
			<li><?= CHtml::link("Partenaires", ['/partenaire/admin']) ?></li>
			<li><?= CHtml::link("Utilisateurs", ['/utilisateur/admin']) ?></li>
			<?=
				Yii::app()->params->itemAt('utilisateurs-par-lot') ?
				'<li>' . CHtml::link("Création comptes tests", ['/utilisateur/create-multi']) . '</li>' : ''
			?>
			<li><?= CHtml::link("Indicateurs de suivi", ['/stats/suivi']) ?></li>
		</ul>
	</div>

	<div class="well">
		<h2>Maintenance</h2>
		<ul>
			<li>
				<?= CHtml::link(
					"Bandeau d'alerte",
					['/config', 'Config[category]' => 'bandeau']
				) ?>
			</li>
			<li>
				<?= CHtml::link(
					"Mode de maintenance",
					['/config', 'Config[category]' => 'maintenance']
				) ?>
				<?php
				if (\Config::read('maintenance.authentification.inactive')) {
					echo "<p>Les connexions sont actuellement restreintes aux administrateurs.</p>";
					echo HtmlHelper::postButton(
						"Autoriser les connexions",
						['/admin/fin-maintenance'],
						[],
						['class' => 'btn btn-success', 'confirm' => 'Voulez-vous ré-autoriser les connexions ?']
					);
				} else {
					echo "<p>Déconnecte les autres utilisateurs
						et bloque toute authentification non-admin jusqu'à nouvel ordre.</p>";
					echo HtmlHelper::postButton(
						"Déconnexion générale",
						['/admin/deconnexion'],
						[],
						['class' => 'btn btn-danger', 'confirm' => 'Voulez-vous déconnecter les autres utilisateurs ?']
					);
				}
				?>
			</li>
		</ul>
	</div>

	<div class="well">
		<h2>Planification (cron)</h2>
		<ul>
			<li><?= CHtml::link("Liste des tâches", ['/cron/index']) ?></li>
		</ul>
	</div>
</div>
