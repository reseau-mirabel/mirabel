<?php

/** @var Controller $this */
/** @var \processes\politique\AlertesSuivi $alertes */

assert($this instanceof Controller);

$this->pageTitle = "Administration des politiques";
$this->breadcrumbs = [
	'Administration des politiques',
];
?>

<h1>
	Administration des politiques
</h1>

<ul>
	<li><?= CHtml::link("Utilisateurs-politiques", ['/admin/politique-utilisateurs']) ?> : utilisateurs-politiques sans partenaire</li>
	<li>
		<?= CHtml::link("Validations des demandes", ['/admin/politique-validation']) ?> : utilisateurs-politiques demandant le rattachement à des éditeurs
		<?php
		if ($alertes->demandesUtilisateurs) {
			echo '→ <span class="badge badge-important">' . $alertes->demandesUtilisateurs . '</span> en attente';
		}
		?>
	</li>
	<li>
		<?= CHtml::link("Modération des politiques de publication", ['/politique/moderation']) ?> : toutes les politiques, en particulier celles en attente de publication
		<?php
		if ($alertes->demandesPolitiques) {
			echo '→ <span class="badge badge-important">' . $alertes->demandesPolitiques . '</span> en attente';
		}
		?>
	</li>
</ul>

<ul>
	<li><?= CHtml::link("Indicateurs chiffrés", ['/stats/politiques']) ?> : tableaux et statistiques simples sur les politiques déclarées</li>
</ul>

<?php if (Yii::app()->user->checkAccess('admin')) { ?>
<h2>Actions réservées aux administrateurs de Mir@bel</h2>
<ul>
	<li><?= CHtml::link("Export CSV des politiques par titres", ['/admin/politique-export']) ?></li>
	<li><?= CHtml::link("Export CSV des utilisateurs et de leurs politiques", ['/politique/export-utilisateurs']) ?></li>
	<li><?= CHtml::link("Bulles d'aide", ['/hint/admin', 'Hint' => ['model' => 'Politique']]) ?></li>
	<li><?= CHtml::link("Textes d'aide", ['/cms', 'Cms' => ['name' => 'politique']]) ?> : textes modifiables sur certaines pages</li>
	<li><?= CHtml::link("Configuration Sherpa", ['/config', 'Config' => ['category' => 'politique']]) ?> : listes de choix en modification de politique</li>
	<li><?= CHtml::link("Jetons d'accès à l'API", ['/politique/jetons']) ?></li>
</ul>
<?php } ?>
