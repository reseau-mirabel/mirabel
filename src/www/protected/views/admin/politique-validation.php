<?php

use processes\admin\PolitiqueValidation;

/** @var Controller $this */
/** @var PolitiqueValidation $model */

assert($this instanceof Controller);

$this->pageTitle = "Validation des comptes responsables de politiques";
$this->breadcrumbs = [
	'Politiques' => ['/admin/politique'],
	'Politiques - validation',
];
?>

<h1><?= CHtml::encode($this->pageTitle); ?></h1>

<section id="users-to-delete">
	<h2>Utilisateurs sans connexion</h2>
	<div class="alert alert-info">
		Ces utilisateurs sont soit à supprimer, soit à contacter.
		Deux cas sont possibles :
		<ul>
			<li>
				Leur compte a été créé <strong>au moins <?= PolitiqueValidation::GHOST_DAYS ?> jours auparavant</strong>,
				sans partenaire, et sans aucune connexion depuis.
			</li>
			<li>
				Leur compte n'a ni partenaire ni éditeur.
				Il est probable que l'éditeur auquel il était lié (ou demandait à être lié) a été supprimé.
			</li>
		</ul>
	</div>
	<?php
	if ($model->ghostUsers) {
		$this->widget(
			'ext.bootstrap.widgets.BootGridView',
			[
				'dataProvider' => new CArrayDataProvider($model->ghostUsers, ['pagination' => false]),
				'columns' => [
					'nomComplet:text:Nom complet',
					'email:email',
					'hdateCreation:datetime:Création du compte',
					[
						'header' => "Éditeurs",
						'value' => function (array $u) {
							return $u['editeurs'];
						},
					],
					[
						'header' => "",
						'type' => 'raw',
						'value' => function (array $u) {
							return components\HtmlHelper::postButton(
								'<i class="icon icon-remove"></i>',
								['/utilisateur/delete-totally', 'id' => $u['id']],
								['returnUrl' => '/' . $this->getRoute()],
								['onclick' => "return confirm('Supprimer ce compte ?')"]
							);
						},
					],
				],
				'filter' => null,
			]
		);
	} else {
		echo "<p>Aucun compte de ce type.</p>";
	}
	?>
</section>

<section style="margin-top: 5ex;">
	<h2>Utilisateurs demandant le rattachement à un éditeur</h2>

	<div class="alert alert-info">
		<p>
			Un utilisateur peut être soit <b>responsable officiel</b> soit <b>délégué</b> de l'éditeur.
		<p>
			Accepter de lier à utilisateur à un éditeur lui permettra
			<strong>d'affecter une politique à des titres</strong> de cet éditeur.
		<p>
			Si tous les rattachements d'un compte sont refusés, alors le compte est automatiquement supprimé.
	</div>
	<table class="table">
		<thead>
			<tr>
				<th>Utilisateur</th>
				<th>Adresse électronique</th>
				<th>Éditeur</th>
				<th>Responsables existants</th>
				<th>Politique créée ?</th>
				<th>Date de la demande</th>
				<th>Rôle demandé</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody id="politique-validation-roles">
			<?= (new widgets\PolitiqueValidation($model->pendingUsers, $model->pendingRoles))->run('politique-validation-roles') ?>
			<tr>
				<td colspan="8"><em>Aucune demande en attente</em></td>
			</tr>
		</tbody>
	</table>
</section>
