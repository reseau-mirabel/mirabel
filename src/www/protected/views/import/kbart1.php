<?php

/** @var \Controller $this */
/** @var \processes\kbart\Import $import */
/** @var ?\processes\kbart\read\FileData $filedata */
/** @var \processes\kbart\Logger $logger */
/** @var \processes\kbart\ImportForm $model */

$this->pageTitle = Yii::app()->name . ' - Import KBART 2/4';
$this->breadcrumbs = [
	'Import KBART' => ['kbart0'],
	"Vérifications de cohérence",
];
?>
<h1>Import KBART 2/4</h1>

<h2>Résumé</h2>
<div>
	<?php
	if ($filedata === null) {
		echo "Erreur fatale lors de la lecture";
	} else {
		echo $this->renderPartial('_kbart-global-info', ['import' => $import, 'filedata' => $filedata]);
	}
	?>
</div>

<h2>Logs de lecture</h2>
<?= $this->renderPartial('_logs', ['logger' => $logger]) ?>

<h2>Vérifications sur le contenu</h2>
<label>
	Filtrer les lignes
	<select id="loglevel-filter">
		<option value="0">Tout afficher, même sans problème</option>
		<option value="1">Infos et +</option>
		<option value="2">Avertissements et +</option>
		<option value="3">Erreurs</option>
	</select>
</label>
<?php
Yii::app()->clientScript->registerScript('toggleSuccess', '
$("#loglevel-filter").on("click", function(event) {
	const level = $(this).val();
	$("#import-read tbody").removeClass()
		.addClass(`display-loglevel-${level}`);
});
');
?>
<table id="import-read" class="table table-bordered table-condensed exportable">
	<thead>
		<tr>
			<th>Ligne</th>
			<th>Titre</th>
			<th>ISSN-P</th>
			<th>ISSN-E</th>
			<th>Remarques</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$rows = ($filedata === null ? [] : $filedata->getRows());
		foreach ($rows as $row) {
			assert($row instanceof \processes\kbart\read\RowData);
			$localLog = $logger->getRowLog($row->position);
			if ($localLog->hasEvents()) {
				$loglevel = $localLog->getMaxlevelInt();
				$class = $localLog->getMaxlevel();
				$details = \processes\kbart\logger\LogRenderer::renderHtml($localLog);
			} else {
				$loglevel = 0;
				$class = "success";
				$details = "";
			}
			echo "<tr class=\"$class\" data-loglevel=\"$loglevel\">"
				. '<td>' . $row->position . '</td>'
				. '<td>' . CHtml::encode($row->title) . '</td>'
				. '<td>' . $row->issn . '</td>'
				. '<td>' . $row->issne . '</td>'
				. '<td>' . $details . '</td>'
				. "</tr>\n";
		}
		?>
	</tbody>
</table>

<h2>Passer à l'étape suivante</h2>
<div class="form">
	<p>
		Les champs qui déclarent des valeurs par défaut
		pour <strong>accès</strong> (<em>access_type</em>) et <strong>contenu</strong> (<em>coverage_depth</em>)
		sont activés seulement si des valeurs sont absentes de lignes du KBART.
	</p>
	<?php
	$form = $this->beginWidget(
		'BootActiveForm',
		array(
			'action' => ['kbart2', 'hash' => $import->getHash()],
			'id' => 'import-kbart-form',
			'type' => 'horizontal',
			'enableClientValidation' => false,
		)
	);
	/** @var BootActiveForm $form */

	?>
	<fieldset>
		<legend>Interpréter</legend>
		<?php
		echo $form->checkBoxRow($model, 'lacunaire', ['hint' => "Tous les accès seront dans l'état <em>lacunaire</em>.<br> À noter qu'indépendamment de ce réglage, les accès de type <code>Selected articles</code> seront importés comme du <em>texte intégral lacunaire</em>."]);
		echo $form->checkBoxRow($model, 'selection');
		echo $form->checkBoxRow($model, 'overrideDatesWithEmbargo', ['hint' => "En cas d'écrasement d'une date déclarée, les champs de volume et de numéro sont effacés pour éviter des incohérences.<br> Indépendamment de ce réglage, les dates vides sont remplacées par les dates calculées par embargo."]);
		echo $form->dropDownListRow($model, 'diffusion', \Service::$enumAcces, ['empty' => '-', 'disabled' => !$model->withAccessType]);
		echo $form->dropDownListRow($model, 'contenu', \Service::$enumType, ['empty' => '-', 'disabled' => !$model->withCoverageDepth]);
		?>
	</fieldset>
	<div class="form-actions">
		<button type="submit" class="btn btn-primary">→ 3/4 Analyser en regard de Mir@bel</button>
	</div>

	<?php
	$this->endWidget(); // form
	?>

</div>
