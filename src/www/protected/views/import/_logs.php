<?php

/** @var \Controller $this */
/** @var \processes\kbart\Logger $logger */

$global = $logger->getGlobalLog();

if ($global->getMaxLevel() === '' && $logger->getLocalCount() === 0) {
	echo "<p>RAS</p>";
	return;
}

if ($global->getMaxLevel() !== '') {
	?>
	<h3>Au niveau global</h3>
	<dl class="dl-horizontal">
		<?php
		foreach ($global->getEvents() as $e) {
			$level = processes\kbart\logger\LogLevel::from($e->level)->text();
			$message = $e->message;
			echo "<dt>$level</dt><dd>"
			. nl2br(\CHtml::encode($message))
			. ($e->count > 1 ? sprintf(' <strong class="event-count">×%d</strong>', $e->count) : '')
			. '</dd>';
		}
	?>
	</dl>
	<?php
	if ($logger->getLocalCount() > 0) {
	?>
	<h3>Au niveau local (ligne à ligne)</h3>
	<dl class="dl-horizontal">
		<?php
		foreach ($logger->getLocalSummary()->getEvents() as $e) {
			$agg = \processes\kbart\logger\AggregateId::from($e->family);
			$level = processes\kbart\logger\LogLevel::from($e->level)->text();
			echo "<dt>$level</dt><dd>"
			. nl2br(\CHtml::encode($agg->text()))
			. ($e->count > 1 ? sprintf(' <strong class="event-count">×%d</strong>', $e->count) : '')
			. '</dd>';
		}
	?>
	</dl>
	<?php
	}
}
