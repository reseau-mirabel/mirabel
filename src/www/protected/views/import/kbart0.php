<?php

/** @var \Controller $this */
/** @var \models\forms\ImportKbart0 $model */
/** @var array $collections */

$this->pageTitle = Yii::app()->name . ' - Import KBART 1/4';
$this->breadcrumbs = [
	'Import KBART',
];
?>
<h1>Import KBART 1/4</h1>

<div class="form">
	<?php
	$form = $this->beginWidget(
		'BootActiveForm',
		[
			'id' => 'import-kbart-form',
			'type' => 'horizontal',
			'enableClientValidation' => false,
			'htmlOptions' => ['enctype' => 'multipart/form-data'],
			'hints' => Hint::model()->getMessages('ImportKbart0'),
		]
	);
	/** @var BootActiveForm $form */
	?>

	<?= $form->errorSummary($model) ?>

	<fieldset>
		<legend>URL ou fichier</legend>
		<?= $form->fileFieldRow($model, 'kbartfile', ['class' => 'span10']) ?>
		<?= $form->textFieldRow($model, 'kbarturl', ['class' => 'span10']) ?>
	</fieldset>

	<fieldset>
		<legend>Destination dans Mir@bel</legend>
		<?php
		$this->renderPartial(
			'/global/_autocompleteField',
			array(
				'model' => $model,
				'attribute' => 'ressourceId',
				'url' => '/ressource/ajax-complete', 'urlParams' => ['noimport' => 0],
				'form' => $form,
				'value' => (empty($model->ressourceId) ? '' : $model->getRessource()->nom),
			)
		);

		if ($model->ressourceId) {
			echo $form->checkBoxListRow($model, 'collectionIds', $collections);
		} else {
			echo $form->dropDownListRow($model, 'collectionIds', [], ['multiple' => true]);
		}
		Yii::app()->clientScript->registerScript('collectionsParRessource', '
	$("#ImportKbart0_ressourceIdComplete").on("autocompleteselect", function(event, ui) {
		$.ajax({
			url: ' . CJavaScript::encode($this->createUrl('collection/ajax-list')) . ',
			data: { ressourceId: ui.item.id },
			method: "get",
			success: function (data, more) {
				$("#ImportKbart0_collectionIds").empty().append($("<option></option>"));
				$.each(data, function(index, item) {
					var option = $("<option></option>").attr("value", item.id).text(item.label);
					$("#ImportKbart0_collectionIds").append(option);
				});
				$("#ImportKbart0_collectionIds").attr("style", "width: auto; height: " + (30 + data.length * 18) + "px");
			}
		});
	});
		');
		?>
	</fieldset>

	<fieldset>
		<legend>Filtrer les lignes</legend>
		<?= $form->dropDownListRow($model, 'filterAccessType', \models\forms\ImportKbart0::ENUM_FILTER_ACCESSTYPE) ?>
	</fieldset>

	<div class="form-actions">
        <button class="btn btn-primary" type="submit">
            → 2/4 Lire et valider les données
        </button>
		<div>Détecte les doublons (même titre) et les incohérences de données.</div>
		<?php
		Yii::app()->clientScript->registerScript('require-ressourceId', <<<JS
			$('.form-actions button').on('click', function(e) {
				const completer = document.getElementById('ImportKbart0_ressourceIdComplete');
				completer.setCustomValidity("");
				const id = document.getElementById('ImportKbart0_ressourceId').value;
				if (!id) {
					e.preventDefault();
					completer.setCustomValidity("Utilisez la complétion pour sélectionner une ressource.");
					return false;
				}
			});
			JS
		);
		?>
	</div>

	<?php
	$this->endWidget(); // form
	?>

</div>
