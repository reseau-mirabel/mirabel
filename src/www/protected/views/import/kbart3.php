<?php

/** @var \Controller $this */
/** @var \processes\kbart\Logger $logger */
/** @var \processes\kbart\State $state */
/** @var array{selection: bool, lacunaire: bool, contenu: string, diffusion: string} $importParams */
/** @var processes\kbart\data\WritingStats $writingStats */

$this->pageTitle = Yii::app()->name . ' - Import KBART 4/4';
$this->breadcrumbs = [
	'Import KBART' => ['kbart0'],
	"2/4 Analyse" => ['kbart1', 'hash' => $state->hash],
	"3/4 Comparaison",
	"Bilan des interventions",
];
?>
<h1>Import KBART 4/4 : bilan des interventions</h1>

<section>
	<h2>crontab</h2>
	<p>
		Pour réaliser cet import en ligne de commande :<br>
		<code>
			./yii kbart import --file="chemin-ou-url"
			--ressourceId=<?= $state->getRessource()->id ?>
			<?= $state->collectionIds ? "--collectionIds=" . join(', ', $state->collectionIds) : '' ?>
			<?= empty($importParams['diffusion']) ? "" : "--acces={$importParams['diffusion']}" ?>
			<?= empty($importParams['contenu']) ? "" : "--contenu={$importParams['contenu']}" ?>
			<?= $importParams['lacunaire'] ? "--lacunaire=1" : "" ?>
			<?= $importParams['selection'] ? "--selection=1" : "" ?>
		</code>
	</p>
</section>

<section id="interventions">
	<h2>Interventions</h2>
	<p>
		<?= CHtml::link(
			"Tableau des interventions enregistrées par cet import",
			$writingStats->getInterventionsUrl(),
			['target' => '_blank']
		) ?>
	<dl class="dl-horizontal">
		<?php
		foreach ($writingStats->interventionTypes as $action => $count) {
			$name = CHtml::encode(Intervention::ACTIONS[$action]);
			echo "<dt>$name</dt><dd>$count interventions</dd>\n";
		}
		?>
		<dt>Total des réussites</dt><dd><?= count($writingStats->interventionSuccesses) ?></dd>
		<dt>Total des interventions</dt><dd><?= $writingStats->interventionTotal ?></dd>
	</dl>

	<?php
	$filename = sprintf(
		'%s_import-kbart_%s_%s.csv',
		\Norm::urlParam(\Yii::app()->name),
		Norm::urlParam($state->getRessource()->nom),
		date('Y-m-d')
	);
	?>
	<table class="table table-bordered table-condensed exportable" data-exportable-filename="<?= CHtml::encode($filename) ?>">
		<thead>
			<tr>
				<th>Titre</th>
				<th>Intervention</th>
			</tr>
		</thead>
		<tbody>
			<?php
			if ($writingStats->interventionTotal === 0) {
				echo '<tr><td colspan="2" style="text-align: center"><strong>Aucun changement</strong></td></tr>';
			} else {
				foreach ($writingStats->interventionFailures as $info) {
					echo '<tr class="error">'
						. "<td>{$info['titreLink']}</td>" // titre
						. "<td>"
						. CHtml::link(CHtml::encode($info['description']), $info['url']) // intervention
						. $info['errors']
						. "</td>"
						. '</td>'
						. "</tr>\n";
				}
				foreach ($writingStats->interventionSuccesses as $info) {
					echo '<tr class="success">'
						. "<td>{$info['titreLink']}</td>" // titre
						. "<td>"
						. CHtml::link(CHtml::encode($info['description']), $info['url']) // intervention
						. "</td>"
						. '</td>'
						. "</tr>\n";
				}
			}
			?>
		</tbody>
	</table>
</section>

<?php if ($writingStats->ignored) { ?>
<section>
	<h2>Titres ignorés (données non-valides)</h2>
	<table class="table table-bordered table-condensed exportable" data-exportable-filename="<?= CHtml::encode($filename) ?>">
		<thead>
			<tr>
				<th></th>
				<th>Titre</th>
				<th>URL revue</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$n = 1;
			foreach ($writingStats->ignored as $info) {
				printf('<tr><td>%4d</td><td>%s</td><td>%s</td></tr>', $n, $info['titre'], $info['revueUrl']);
				$n++;
			}
			?>
		</tbody>
	</table>
</section>
<?php } ?>