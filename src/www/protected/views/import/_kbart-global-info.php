<?php

/** @var \processes\kbart\Import $import */
/** @var ?\processes\kbart\read\FileData $filedata */

if (!isset($filedata)) {
	$filedata = null;
}
?>
<dl class="dl-horizontal">
	<?php
	$ressource = $import->state->getRessource();
	?>
	<dt>Ressource</dt>
	<dd><?= $ressource->getSelfLink() ?></dd>

	<dt>Collections</dt>
	<dd>
		<?php
		$cnames = $import->state->getCollectionNames();
		if ($cnames) {
			echo CHtml::encode(join(", ", $cnames));
		} else {
			echo "–";
		}
		?>
	</dd>

	<dt>Identification de titres</dt>
	<dd>
		<?php
		if ($ressource->importIdentifications) {
			echo CHtml::encode($ressource->importIdentifications) . " <em>(configuration spécifique à cette ressource)</em>";
		} else {
			echo "issn, issne, idInterne <em>(configuration par défaut)</em>";
		}
		?>
		<a href="#" data-title="Méthodes d'identification des titres" data-content="Pour chaque titre du KBART, les méthodes sont appliquées dans l'ordre jusqu'à ce que l'une identifie le titre correspondant dans M." data-trigger="hover" rel="popover">?</a>
	</dd>

	<dt>Fichier</dt>
	<dd><?= CHtml::encode($import->state->title) ?></dd>

	<?php if ($filedata !== null) { ?>
	<dt>Lignes présentes</dt>
	<dd><?= $filedata->numRows ?></dd>
	<dt>Lignes chargées</dt>
	<dd>
		<?= count($filedata->getRows()) ?>
		<?php
		if ($import->state->filter) {
			echo " (filtre : <strong>{$import->state->getFilter()->summary()}</strong>)";
		}
		?>
	</dd>
	<?php } ?>
</dl>
