<?php

/** @var \Controller $this */
/** @var \processes\kbart\data\Analyzed $record */
/** @var \processes\kbart\data\DiffCollection $diffs */
/** @var array $diffStatusTo */

$positions = array_map(
	function ($r) { return $r->position; },
	$record->rows
);
$row = $record->rows[0];

$diff = $diffs->getDiff($record->title->id);
if ($diff === null) {
	return;
}
if ($diff->services) {
	$summary = $diff->services->getTextSummary(true);
} else {
	$summary = '';
}
$class = $diffStatusTo[$diff->status]['class'];

$otherChanges = [];
if ($diff->erreur) {
	$class = 'error';
	$otherChanges[] = CHtml::tag("abbr", ['title' => $diff->erreur], "err");
}
if (isset($diff->issn[$row->issn])) {
	$revueId = $diff->issn[$row->issn];
	$title = "Dans M, ce titre n'a pas l'ISSN-P {$row->issn}.";
	if ($revueId === (int) $record->title->revueId) {
		$title .= " Cet ISSN est attribué à un autre titre de la même revue.";
	} elseif ($revueId > 0) {
		$title .= " Cet ISSN est attribué à un titre d'une autre revue (ID $revueId).";
	} else {
		$title .= " Cet ISSN n'est pas dans Mir@bel.";
	}
	$otherChanges[] = CHtml::tag('abbr', ['title' => $title], "ISSN-P");
}
if (isset($diff->issn[$row->issne])) {
	$revueId = $diff->issn[$row->issne];
	$title = "Dans M, ce titre n'a pas l'ISSN-E {$row->issne}.";
	if ($revueId === (int) $record->title->revueId) {
		$title .= " Cet ISSN est attribué à un autre titre de la même revue.";
	} elseif ($revueId > 0) {
		$title .= " Cet ISSN est attribué à un titre d'une autre revue (ID $revueId).";
	} else {
		$title .= " Cet ISSN n'est pas dans Mir@bel.";
	}
	$otherChanges[] = CHtml::tag('abbr', ['title' => $title], "ISSN-E");
}
if ($diff->publishers) {
	$otherChanges[] = CHtml::tag('abbr', ['title' => "Au moins un lien titre-éditeur du KBART n'est pas dans M."], "Éditeur");
}
if ($diff->identification) {
	$otherChanges[] =  CHtml::tag('abbr', ['title' => "Nouvel identifiant local à cette ressource : {$diff->identification[1]}"], "id");
}
?>
<tr class="<?= $class ?>">
	<td style="max-width:70em;"><?= $record->title->getSelfLink() ?></td>
	<td><?= count($record->services) ?></td>
	<td><?= $row->issn ?></td>
	<td><?= $row->issne ?></td>
	<td><span class="show-details" data-id="<?= $record->title->id ?>" title="Détails (nouveau cadre dans cette page)"><?= join(", ", $positions) ?></span></td>
	<td><?= $summary ?></td>
	<td><?= join(" ", $otherChanges) ?></td>
</tr>
