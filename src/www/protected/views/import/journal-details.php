<?php

/** @var \Controller $this */
/** @var \processes\kbart\data\Analyzed $analyzed */
/** @var \processes\kbart\data\Diff $diff */
assert($analyzed instanceof \processes\kbart\data\Analyzed);
assert($diff instanceof \processes\kbart\data\Diff);

const IGNORED_COLUMNS = [
	"hdateModif" => true,
	"hdateCreation" => true,
	"hdateImport" => true,
	"import" => true,
	"statut" => true,
];
?>

<section id="kbart-data">
	<h3>Données KBART <small>telles qu'interprétées par M</small></h3>
	<table class="table table-bordered table-condensed table-compact">
		<thead>
			<tr>
			<?php
			$columns = [
				'position' => "position",
				'title' => "titre",
				'prefix' => "préfixe",
				'issn' => "ISSN-P",
				'issne' => "ISSN-E",
				'id' => "ID interne",
				'url' => "URL",
				'acessBegin' => "accès (début)",
				'acessEnd' => "accès (fin)",
				'accessType' => "accès (type)",
				'embargoInfo' => "embargo",
				'coverage_depth' => "contenu",
				'notes' => "notes",
				'publisherName' => "éditeur",
			];
			foreach ($columns as $k => $name) {
				echo "<th>$name</th>";
			}
			?>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach ($analyzed->rows as $row) {
				echo "<tr>";
				foreach ((array) $row as $k => $v) {
					switch ($k) {
						case 'notes':
							echo CHtml::tag('td', ['class' => 'notes', 'title' => $v], CHtml::encode($v));
							break;
						case 'accessBegin':
						case 'accessEnd':
							assert($v instanceof \processes\kbart\read\Issue);
							if ($v->isEmpty()) {
								echo "<td>∅</td>";
							} else {
								echo CHtml::tag('td', [], "<div>{$v->date}</div><div>{$v->numbering}");
							}
							break;
						default:
							echo CHtml::tag('td', [], CHtml::encode($v));
					}
				}
				echo "</tr>";
			}
			?>
		</tbody>
	</table>
</section>

<?php if ($diff->titre) { ?>
<section>
	<h3>Titre <small>nouvelles données lues sur ce titre</small></h3>
	<?= components\HtmlTable::buildFromAssoc($diff->titre)->toHtml() ?>
	<?php
	if ($diff->publishers) {
		echo "<h4>Éditeurs</h4>";
		echo "<ul>";
		foreach ($diff->publishers as $publisher) {
			if (is_int($publisher)) {
				$e = Editeur::model()->findByPk($publisher);
				echo "<li>{$e->getSelfLink()}</li>";
			} else {
				echo "<li>" . components\HtmlTable::build($publisher, array_keys($publisher))->toHtml() . "</li>";
			}
		}
		echo "</ul>";
	}
	?>
</section>
<?php } ?>

<?php if ($diff->identification || $diff->issn) { ?>
<section>
	<h3>Identifiants</h3>
	<?php
	if ($diff->identification) {
		echo "<p>Nouvel identifiant pour cette ressource : "
			. CHtml::tag('strong', [], CHtml::encode("{$diff->identification[0]} → {$diff->identification[1]}"))
			. "</p>";
	}
	if ($diff->issn) {
		echo "<h4>ISSN du KBART divergeant de M</h4>";
		echo '<dl class="dl-horizontal">';
		foreach ($diff->issn as $issn => $revueId) {
			$title = "Dans M, ce titre n'a pas l'ISSN {$issn}.";
			if ($revueId === (int) $analyzed->title->revueId) {
				$title .= " Cet ISSN est attribué à un autre titre de la même revue.";
			} elseif ($revueId > 0) {
				$title .= " Cet ISSN est attribué à un titre d'une autre revue (ID $revueId).";
			} else {
				$title .= " Cet ISSN n'est pas dans Mir@bel.";
			}
			echo "<dt>$issn</dt><dd>$title</dd>";
		}
		echo "</dl>";
	}
	?>
</section>
<?php } ?>

<div>
	<h3>Accès en ligne</h3>

	<section id="access-existing">
		<h4>Accès présents dans M</h4>
		<?php
		$existingServices = $analyzed->services;
		if ($existingServices) {
			$rows = [];
			foreach ($existingServices as $s) {
				$rows[] = array_diff_key($s->getAttributes(), IGNORED_COLUMNS);
			}
			echo components\HtmlTable::build($rows)->toHtml(true);
		} else {
			echo '<p>Aucun</p>';
		}
		?>
	</section>

	<section id="access-creation">
		<h4>Création d'accès</h4>
		<?php
		$creates = $diff->services ? $diff->services->getCreate() : null;
		if ($creates) {
			$changes = array_map(
				function($x) {
					return array_diff_key($x->getAttributes(), IGNORED_COLUMNS);
				},
				$creates
			);
			echo components\HtmlTable::build($changes)->toHtml(true);
		} else {
			echo '<p>Aucune</p>';
		}
		?>
	</section>

	<section id="access-update">
		<h4>Modification d'accès</h4>
		<?php
		$updates = $diff->services ? $diff->services->getUpdate() : null;
		if ($updates) {
			foreach ($updates as $update) {
				$before = $update['before']->getAttributes();
				$after = [];
				$rawAfter = $update['after']->getAttributes();
				foreach ($before as $k => $v) {
					if (isset(IGNORED_COLUMNS[$k])) {
						unset($before[$k]);
						unset($after[$k]);
						continue;
					}
					if ((string) $v === '' && (string) $rawAfter[$k] === '') {
						unset($before[$k]);
					} else {
						$after[$k] = ($rawAfter[$k] == $v ? $rawAfter[$k] : "<b>$rawAfter[$k]</b>");
					}
				}
				echo components\HtmlTable::build([$before, $after])->addClass('kbart-data')->toHtml();
			}
			echo "<div>Les champs vides avant et après modification ne sont pas affichés.</div>";
		} else {
			echo '<p>Aucune</p>';
		}
		?>
	</section>

	<?php
	$removals = $diff->services ? $diff->services->getDelete() : null;
	if ($removals) {
		?>
		<section id="access-remove">
			<h4>Suppression d'accès</h4>
			<?php
			$changes = array_map(
				function($x) {
					return array_diff_key($x->getAttributes(), IGNORED_COLUMNS);
				},
				$removals
			);
			echo components\HtmlTable::build($changes)->toHtml(true);
			?>
		</section>
		<?php
	}
	?>
</div>
