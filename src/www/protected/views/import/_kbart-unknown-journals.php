<?php

/** @var \Controller $this */
/** @var \processes\kbart\read\RowData[] $journals */

?>
<details>
	<summary><?= count($journals) ?> lignes où les titres sont inconnus de Mir@bel.</summary>
	<div>
		<p class="alert alert-info">
			Les liens dans le tableau ci-dessous permettent de créer ces titres dans de nouveaux onglets,
			à partir de leurs ISSN.
			Recharger ensuite cette page pour importer leurs accès en ligne.
		</p>
		<table class="table table-bordered table-condensed exportable">
			<thead>
				<tr>
					<th>Titre</th>
					<th>ISSN ISSN-E</th>
					<th>Ligne</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach ($journals as $row) {
					assert($row instanceof \processes\kbart\read\RowData);
					$issns = trim("{$row->issn} {$row->issne}");
					$extra = [
						'issnSupport' => [],
						'publishers' => [],
					];
					if ($row->issn) {
						$extra['issnSupport'][$row->issn] = Issn::SUPPORT_PAPIER;
					}
					if ($row->issne) {
						$extra['issnSupport'][$row->issne] = Issn::SUPPORT_ELECTRONIQUE;
					}
					if ($row->publisherName) {
						$names = preg_split('/\s*;\s*/', $row->publisherName);
						foreach ($names as $n) {
							$publishers = \Yii::app()->db->createCommand("SELECT id FROM Editeur WHERE nom = :n")
								->queryColumn([':n' => $n]);
							if ($publishers && count($publishers) === 1) {
								$extra['publishers'][$publishers[0]] = $n;
							}
						}
					}
					if ($issns) {
						$url = ['/titre/create-by-issn', 'issn' => $issns];
						$extra = array_filter($extra);
						if ($extra) {
							$url['extra'] = json_encode($extra);
						}
						$link = CHtml::link($issns, $url, ['target' => '_blank', 'title' => "Création par ISSN, nouvel onglet"]);
					} else {
						$link = "sans ISSN";
					}
					echo "<tr>"
						. '<td>' . CHtml::encode($row->title) . '</td>'
						. '<td>' . $link . '</td>'
						. '<td>' . $row->position . '</td>'
						. "</tr>\n";
				}
				?>
			</tbody>
		</table>
	</div>
</details>
