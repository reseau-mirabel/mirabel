<?php

use processes\kbart\data\Diff;

/** @var \Controller $this */
/** @var ?\processes\kbart\Import $import */
/** @var \processes\kbart\data\CrossedData $crossedData */
/** @var \processes\kbart\data\DiffCollection $diffs */
/** @var \processes\kbart\Logger $logger */
/** @var \processes\kbart\ImportForm $model */

$diffStatusTo = [
	Diff::STATUS_EMPTY => ['name' => "RAS", 'class' => 'success'],
	Diff::STATUS_ERROR => ['name' => "Avertissement,  erreur", 'class' => 'error'],
	Diff::STATUS_JOURNAL_UPDATE => ['name' => "Divergences de titre/ISSN", 'class' => 'warning'],
	Diff::STATUS_UPDATE => ['name' => "Modification des accès", 'class' => 'info'],
	Diff::STATUS_CREATE => ['name' => "Création d'accès", 'class' => 'info'],
	Diff::STATUS_DELETE => ['name' => "Suppression des accès", 'class' => 'warning'],
];

$this->containerClass = "container full-width";
$this->pageTitle = Yii::app()->name . ' - Import KBART 3/4';
$this->breadcrumbs = [
	'Import KBART' => ['kbart0'],
	"2/4 Analyse" => ['kbart1', 'hash' => $import->getHash()],
	"3/4 Comparaison",
];
?>
<h1>Import KBART 3/4</h1>

<?= $this->renderPartial('_kbart-global-info', ['import' => $import]) ?>

<?php
if ($logger->getGlobalLog()->hasEvents()) {
	?>
	<h2>Logs de comparaison</h2>
	<?= $this->renderPartial('_logs', ['logger' => $logger]) ?>
<?php
}
?>

<section id="kbart-known-journals">
	<h2>Titres connus</h2>

	<div style="display: flex; flex-direction: row; justify-content: space-between;">
		<div>
		<div>
			<h3>Résumé des interventions prévues</h3>
			<dl class="dl-horizontal">
			<?php
			foreach ($diffs->getStatsByStatus() as $status => $num) {
				if ($num > 0) {
					echo "<dt>{$diffStatusTo[$status]['name']}</dt><dd>$num</dt>\n";
				}
			}
			?>
			</dl>
		</div>

		<div>
			<h3>Filtrer les lignes</h3>
			<select id="loglevel-filter" style="width: 40ex">
				<option value="0">Tout afficher, même sans changement</option>
				<option value="1">Modification d'accès et +</option>
				<option value="2">Suppressions d'accès et +</option>
				<option value="3">Avertissements et +</option>
			</select>
		</div>
		<?php
		Yii::app()->clientScript->registerScript('toggleSuccess', '
		$("#loglevel-filter").on("click", function(event) {
			const level = $(this).val();
			$("#import-contents tbody").removeClass()
				.addClass(`display-loglevel-${level}`);
		});
		');
		?>
		</div>

		<div class="well" style="max-width: 30em">
			<h3>Légende</h3>
			<code>1C 2M 1S 2coll</code>
			= 1 création d'accès + 2 modifications d'accès + 1 suppression + 2 accès ont des changement de collections
			<br>
			<code>ISSN-P ISSN-E Éditeur id</code>
			= signalements ISSN + signalement éditeur + ajout/modification d'identifiant local à la ressource
			<br>
			<code>++</code> pour voir le détail des changements concernant ce titre.
		</div>
	</div>

	<table id="import-contents" class="table table-bordered table-condensed exportable">
		<thead>
			<tr>
				<th rowspan="2">Titre M</th>
				<th rowspan="2">Accès M</th>
				<th rowspan="2">ISSN-P</th>
				<th rowspan="2">ISSN-E</th>
				<th rowspan="2">Ligne</th>
				<th colspan="2">Comparaison M</th>
			</tr>
			<tr>
				<th>accès</th>
				<th>autres</th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach ($crossedData->known as $record) {
				$this->renderPartial('_kbart2-row', ['record' => $record, 'diffs' => $diffs, 'diffStatusTo' => $diffStatusTo]);
			}
			?>
		</tbody>
	</table>
</section>
<?php
$encodedDefaults = json_encode($model->getAttributes());
Yii::app()->clientScript->registerScript('showKbartDetails', <<<EOJS
$("#kbart-known-journals").on("click", ".show-details", function() {
	const id = this.getAttribute('data-id');
	$.ajax({
		method: 'GET',
		url: '/import/journal-details',
		data: {
			hash: '{$import->getHash()}',
			id: id,
			defaults: '{$encodedDefaults}',
		},
	}).then(function(response) {
		const modal = document.getElementById("journal-details");
		modal.querySelector('.content').innerHTML = response;
		modal.showModal();
	});
});
EOJS);
?>
<dialog id="journal-details">
  <form method="dialog">
    <div>
		<button type="button" class="close" data-dismiss="modal" onclick="document.getElementById('journal-details').close()">&times;</button>
    </div>
    <div class="content">
    </div>
    <div>
      <button id="cancel" type="reset" onclick="document.getElementById('journal-details').close()">Fermer</button>
    </div>
  </form>
</dialog>


<section id="kbart-unknown-journals">
	<h2>Titres inconnus</h2>
	<?php
	if ($crossedData->unknown) {
		echo $this->renderPartial('_kbart-unknown-journals', ['journals' => $crossedData->unknown]);
	} else {
		echo "<p>Tous les titres du fichier KBART sont connus de Mir@bel.</p>";
	}
	?>
</section>

<section>
	<h2>Titres retirés</h2>
	<?php
	$missingJournals = $crossedData->findMissingJournals($import->state->getRessource()->id, $import->state->getCollectionIds());
	if ($missingJournals) { ?>
	<details>
		<summary><?= count($missingJournals) ?> titres déjà importés dans Mir@bel sont absents de cet import.</summary>
		<div>
			Ces titres ont des accès importés par KBART dans la ressource (et les collections) choisis pour cet import,
			mais ils sont absents de ce fichier KBART.
			<table class="table table-bordered table-condensed exportable">
				<thead>
					<tr>
						<th>Titre</th>
					</tr>
				</thead>
				<tbody>
					<?php
					foreach ($missingJournals as $t) {
						echo "<tr>"
							. '<td>' . $t->getSelfLink() . '</td>'
							. "</tr>\n";
					}
					?>
				</tbody>
			</table>
		</div>
	</details>
	<?php } else { ?>
	<p>Les titres importés par KBART dans cette ressource (et ces collections) de Mir@bel sont tous présents dans ce fichier KBART.</p>
	<?php } ?>
</section>

<section style="margin-top:2em">
	<h2>Importer ces données dans Mir@bel</h2>
	<div class="form">
		<?php
		$form = $this->beginWidget(
			'BootActiveForm',
			array(
				'action' => ['kbart3', 'hash' => $import->getHash()],
				'id' => 'import-kbart-form',
				'type' => 'horizontal',
				'enableClientValidation' => false,
			)
		);
		/** @var BootActiveForm $form */

		echo $form->hiddenField($model, 'lacunaire');
		echo $form->hiddenField($model, 'selection');
		echo $form->hiddenField($model, 'diffusion');
		echo $form->hiddenField($model, 'contenu');
		echo $form->hiddenField($model, 'suppressions');
		?>
		<fieldset>
			<legend>Rappel de la configuration de cet import</legend>
			<div class="controls">
			<ul>
				<li><?= $model->lacunaire ? "lacunaire" : "<del>lacunaire</del>" ?></li>
				<li><?= $model->selection ? "sélection d'articles" : "<del>sélection d'articles</del>" ?></li>
				<li><?= $model->overrideDatesWithEmbargo ? "préfère les dates déclarées aux dates calculées par l'embargo" : "préfère les dates calculées par l'embargo aux dates déclarées" ?></li>
				<li>
					Accès par défaut :
					<?= $model->withAccessType ? (\Service::$enumAcces[$model->diffusion] ?? '<strong>ne pas importer si absent</strong>') : "<em>inutile pour ce fichier</em>" ?>
				</li>
				<li>
					Contenu par défaut :
					<?= $model->withCoverageDepth ? (\Service::$enumType[$model->contenu] ?? '<strong>ne pas importer si absent</strong>') : "<em>inutile pour ce fichier</em>" ?>
				</li>
				<li><?= $model->suppressions ? "<strong>supprimer les accès en ligne retirés</strong>" : "<del>pas de suppression d'accès en ligne</del>" ?></li>
			</ul>
			</div>
		</fieldset>

		<fieldset>
			<legend>Configuration des suppressions</legend>
			<?= $form->checkBoxRow(
				$model,
				'suppressions',
				[
					'hint' => "<p>Ne concerne que les titres <b>présents dans ce KBART</b> mais avec moins d'accès en ligne que lors d'un précédent import."
						. "<p>Cette action n'a aucun effet sur les titres absents de ce KBART."
						. "<p>Si l'accès est dans une collection, la suppression de l'accès n'est effective que s'il n'a plus aucune collection.",
				]
			) ?>

			<?php
			if ($missingJournals) {
				$count = count($missingJournals);
				echo $form->checkBoxRow($model, 'absents', ['hint' => "Retire tous les accès importés de cette ressource qui sont liés aux $count titres absents de ce KBART"]);
			}
			?>
		</fieldset>

		<div class="form-actions">
			<button type="submit" class="btn btn-primary" name="step" value="3">→ 4/4 Importer dans Mir@bel</button>
			<div>Enregistrer ces accès en ligne</div>
		</div>

		<?php
		$this->endWidget(); // form
		?>

	</div>
</section>