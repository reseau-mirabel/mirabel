<?php

/** @var Controller $this */
/** @var \models\cms\BreveSearchForm $search */
/** @var CActiveDataProvider $provider */

assert($this instanceof Controller);

Yii::app()->getClientScript()->registerScript('redirection', <<<EOJS
	const hash = window.location.hash;
	if (hash !== '' && document.querySelector(hash) === null) {
		const m = hash.match(/^#actu(\d+)$/);
		if (m) {
			window.location.replace(window.location.origin + window.location.pathname + '/' + m[1] + window.location.search);
		}
	}
	EOJS
);

$blocks = $provider->getData();
$pagination = $provider->getPagination();

$filter = $search->exportAttributes();
$hasFilter = !empty($filter);
$headerLogo = '';

if ($search->partenaireId) {
	$partenaire = Partenaire::model()->findByPk($search->partenaireId);
	assert($partenaire instanceof Partenaire);
	$this->breadcrumbs = [
		"Actualité" => ['/site/actualite'],
		"Partenaire {$partenaire->nom}",
	];
	$suffix = $partenaire->nom;
	$headerLogo = $partenaire->getLogoImg(false);
	Yii::app()->clientScript->registerMetaTag($partenaire->getLogoUrl(), null, null, ['property' => 'og:image'], 'og:image');
	$header = "<h2>Actualité du partenaire " . $partenaire->getSelfLink() . "</h2>";
} elseif ($search->ressourceId) {
	$ressource = Ressource::model()->findByPk($search->ressourceId);
	assert($ressource instanceof Ressource);
	$this->breadcrumbs = [
		"Actualité" => ['/site/actualite'],
		"Ressource {$ressource->nom}",
	];
	$suffix = $ressource->nom;
	$headerLogo = $ressource->getLogoImg(false);
	Yii::app()->clientScript->registerMetaTag($ressource->getLogoUrl(), null, null, ['property' => 'og:image'], 'og:image');
	$header = "<h2>Actualité de la ressource " . $ressource->getSelfLink() . "</h2>";
} elseif ($search->sourcelienId) {
	$source = Sourcelien::model()->findByPk($search->sourcelienId);
	assert($source instanceof Sourcelien);
	$this->breadcrumbs = [
		"Actualité" => ['/site/actualite'],
		$source->nom,
	];
	$suffix = $source->nom;
	$header = "<h2>Actualité de <em>" . CHtml::encode($source->nom) . "</em></h2>";
} elseif ($search->startingId) {
	$date = Yii::app()->dateFormatter->format('d MMMM yyyy', $blocks[0]->hdateCreat);
	$this->breadcrumbs = [
		"Actualité" => ['/site/actualite'],
		$date,
	];
	$suffix = "";
	$header = "<h2>Actualité antérieure au <em>$date</em></h2>";
} elseif ($search->categorie) {
	$this->breadcrumbs = [
		"Actualité" => ['/site/actualite'],
		$search->categorie,
	];
	$suffix = $search->categorie;
	$header = "<h2>Actualité pour <em>" . CHtml::encode($search->categorie) . "</em></h2>";
} else {
	// Pas de filtre
	$this->breadcrumbs = [
		"Actualité",
	];
	$suffix = "";
	$header = Cms::getBlock('actualite-sans-filtre')->toHtml();
}

$this->pageTitle = "Actualité" . ($suffix ? " / $suffix" : "");
?>

<?= Cms::getBlock('actualite-menu')->toHtml() ?>

<h1 class="site-actualite-header">
	<?= $headerLogo ?>
	L'actualité du réseau Mir@bel <?= ($suffix ? " / <em>" . CHtml::encode($suffix) . "</em>" : "") ?>
	<?php
	if (!$hasFilter) {
		echo '<a href="/rss/breves"><img alt="RSS" src="/images/flux.png" width="14" height="14"></a>';
	}
	?>
</h1>

<?php
if ($hasFilter) {
	?>
	<p>
		Consulter <a href="/site/actualite">toute l'actualité de Mir@bel</a>.
	</p>
	<?php
}
?>

<?= Cms::getBlock('actualite-intro')->toHtml() ?>

<div class="site-actualite-header"><?= $header ?></div>

<?php
if ($pagination->getCurrentPage() > 0) {
	echo '<div style="text-align: right">'
		. CHtml::link("plus récentes…", ['/site/actualite', $pagination->pageVar => $provider->pagination->getCurrentPage(), 'q' => $filter])
		. '</div>';
}
?>

<ul class="breves">
	<?php
	/** @var Cms[] $blocks */
	foreach ($blocks as $block) {
		$options = ['id' => "actu{$block->id}"];
		if ((int) $block->id === $search->startingId) {
			$options['class'] = 'alert alert-info';
		}
		echo CHtml::tag('li', $options, $block->toHtml());
	}
	?>
</ul>

<?php
if ($pagination->getCurrentPage(false) < $pagination->getPageCount() - 1) {
	echo '<div style="text-align: right">'
		. CHtml::link("plus anciennes…", ['/site/actualite', $pagination->pageVar => $provider->pagination->getCurrentPage() + 2, 'q' => $filter])
		. '</div>';
}
