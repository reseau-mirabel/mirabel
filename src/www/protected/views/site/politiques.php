<?php

/** @var Controller $this */
/** @var Cms[] $blocs */

use processes\politique\Render;

assert($this instanceof Controller);

$this->pageTitle = "Politiques de publication et autoarchivage";
$this->breadcrumbs = [
	"Politiques",
];
?>

<h1>
	<?= CHtml::image("/images/logo_politiques_redux.png", "Mir@bel politiques", ['width' => 50, 'height' => 76]) ?>
	<?= CHtml::encode($this->pageTitle) ?>
</h1>

<p class="lead">Service de déclaration pour les revues et éditeurs scientifiques français</p>

<?php
if (Yii::app()->user->checkAccess('permRedaction')) {
	echo '<p class="alert alert-warning">Vous pouvez '
		. CHtml::link(
			"modifier le contenu des blocs",
			['/cms/admin', 'Cms' => ['name' => 'page-politiques-'], 'filter' => 'blocs']
		)
		. ' de cette page.</p>';
}
?>

<div id="politiques-blocs">
	<?= Render::toHtmlBlock($blocs[0], 1) ?>
	<?= Render::toHtmlBlock($blocs[1], 2) ?>
	<?= Render::toHtmlBlock($blocs[2], 3) ?>
	<?= Render::toHtmlBlock($blocs[3], 4) ?>
</div>
