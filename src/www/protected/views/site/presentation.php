<?php
// file included by PHP views
?>
<div id="page_presentation">
	<div id="presentation-video">
		<video controls preload="none" poster="/images/videos/presentation-HD.png">
			<source src="/images/videos/presentation-HD.mp4" type="video/mp4">
			<source src="/images/videos/presentation-HD.webm" type="video/webm">
			Votre navigateur est probablement trop ancien pour lire les vidéos HTML5.
		</video>
	</div>
</div>
