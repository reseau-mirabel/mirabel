<?php

/** @var Controller $this */

assert($this instanceof Controller);
?>
<section class="thematique">
	<h2>Thématiques des revues de Mir@bel</h2>
	<?= $this->renderPartial('/categorie/_liste') ?>
</section>