<?php

/** @var Controller $this */
/** @var array $intvByCategory */
/** @var ?\processes\politique\AlertesSuivi $alertesPolitiques */

assert($this instanceof Controller);

?>

<div id="page_accueil">
	<div id="accueil-bandeau">
		<h1><img id="logo_grand" alt="Mir@bel : Le site web qui facilite l'accès aux revues - (Re)cueillir les savoirs" src="/images/logotype-tagline.png" /></h1>
	</div>
	<div id="accueil-colonnes">
		<div id="accueil-gauche">
			<div>
			<?= Cms::getBlockAsHtml('accueil-intro') ?>
			<?php
			if (Yii::app()->params->itemAt('displayThemesOnHomePage')) { // Cf fichier src/www/protected/config/local.php
				echo '<section class="thematique"><h2>Thématiques des revues</h2>';
				$this->renderPartial('_categories');
				echo "</section>";
			}
			?>
			<?= Cms::getBlockAsHtml('accueil-principal') ?>
			</div>
		</div>
		<div id="accueil-droite">
			<?= Cms::getBlockAsHtml('accueil-actu') ?>
			<div id="presentation-video">
				<?= Cms::getBlockAsHtml('accueil-video') ?>
			</div>
		</div>
	</div>
</div>

<div style="clear: both"></div>
<?php
if (Yii::app()->user->checkAccess("avec-partenaire")) {
	echo Cms::getBlockAsHtml('accueil-connecté');
	if ($intvByCategory) {
		$this->renderPartial(
			'/utilisateur/_blocSuivi',
			['intvByCategory' => $intvByCategory, 'partenaireId' => (int) Yii::app()->user->partenaireId]
		);
	}
	if ($alertesPolitiques !== null && !$alertesPolitiques->isEmpty() && Yii::app()->user->access()->toPolitique()->validate()) {
		?>
		<h3>Suivi des politiques</h3>
		<ul>
			<li>
				<?= CHtml::link("Validations des demandes", ['/admin/politique-validation']) ?> : utilisateurs-politiques demandant le rattachement à des éditeurs
				<?php
				if ($alertesPolitiques->demandesUtilisateurs) {
					echo '→ '
						. CHtml::tag('span', ['title' => "Utilisateurs-politiques demandant le rattachement à des éditeurs", 'class' => "badge badge-important"], $alertesPolitiques->demandesUtilisateurs)
						. ' en attente';
				}
				?>
			</li>
			<li>
				<?= CHtml::link("Modération des politiques de publication", ['/politique/moderation']) ?> : toutes les politiques
				<?php
				if ($alertesPolitiques->demandesPolitiques) {
					echo '→ dont '
						. CHtml::tag('span', ['title' => "Politiques à publier en attente de modération", 'class' => "badge badge-important"], $alertesPolitiques->demandesPolitiques)
						. ' demandes de publication en attente';
				}
				?>
			</li>
		</ul>
		<?php
	}
}
