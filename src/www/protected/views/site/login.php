<?php

/** @var Controller $this */
/** @var string $mode */
/** @var models\forms\Login $model */

assert($this instanceof Controller);

$this->pageTitle = Yii::app()->name . ' - Connexion';
$this->breadcrumbs = [
	'Connexion',
];
?>

<h1>Connexion</h1>

<?php if (\Config::read('maintenance.authentification.inactive')) { ?>
	<div class="alert alert-error" role="alert">
		Site en maintenance : la connexion est restreinte aux administrateurs.
	</div>
<?php } ?>

<?php if ($mode === 'politique') { ?>
<section>
	<div class="alert alert-warning" role="alert">
		Pour <strong>déclarer la politique de diffusion d'un éditeur</strong>
		il faut s'identifier dans Mir@bel.
	</div>
</section>
<?php } ?>

<?= \Cms::getBlockAsHtml("connexion-intro") ?>

<div class="form">
	<?php
	$url = $this->createUrl('/site/login', $mode ? ['mode' => $mode] : []);
	/** @var BootActiveForm */
	$form = $this->beginWidget(
		'BootActiveForm',
		[
			'action' => $url,
			'id' => 'login-form',
			'type' => 'horizontal',
			'enableClientValidation' => false,
		]
	);
	?>

	<?= $form->errorSummary($model) ?>

	<p>
		Identifiez-vous en utilisant votre compte personnel :
	</p>

	<?php
	echo $form->textFieldRow($model, 'username');
	echo $form->passwordFieldRow(
		$model,
		'password',
		['hint' => ""]
	);
	?>

	<div class="form-actions">
		<button type="submit" class="btn">
			<i class="icon-ok"></i> Se connecter
		</button>

		<div id="link-login-recover">
			Identifiants perdus ?
			<?= CHtml::link('Demander un nouveau mot de passe', ['/utilisateur/reset-password']) ?>.
		</div>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->

<?php if ($mode === 'politique') { ?>
<section>
	<h2>Pas encore enregistré ?</h2>
	<p>
		<?= CHtml::link("<strong>Créez un compte</strong>", ['/politique/utilisateur'], ['rel' => 'nofollow']) ?> si vous n'en avez pas encore.
		Pour en savoir plus, consultez <?= CHtml::link("les pages sur Mir@bel et les politiques de diffusion d'articles en accès ouvert", ['/site/politiques']) ?>.
	</p>
</section>
<?php } ?>
