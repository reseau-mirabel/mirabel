<?php

/** @var Controller $this */
/** @var Collection $model */
assert($this instanceof Controller);

$this->pageTitle = "Nouvelle collection";

$this->breadcrumbs = [];
$this->breadcrumbs[$model->ressource->nom] = ['/ressource/view', 'id' => $model->ressourceId];
$this->breadcrumbs[] = 'Nouvelle collection';
?>

<h1>Nouvelle collection / <em><?= CHtml::encode($model->ressource->nom) ?></em></h1>

<?= $this->renderPartial('_form', ['model' => $model]) ?>
