<?php

/** @var Controller $this */
/** @var Collection $model */
/** @var array $types */
assert($this instanceof Controller);

if (empty($types)) {
	$types = [
		Collection::TYPE_COURANT => "Courant",
		Collection::TYPE_ARCHIVE => "Archive",
		Collection::TYPE_LICNATFRANCE => "Licence nationale France",
	];
	if ($model->type === Collection::TYPE_TEMPORAIRE) {
		$types[Collection::TYPE_TEMPORAIRE] = 'Temporaire';
	}
}

/** @var BootActiveForm $form */
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'collection-form',
		'enableAjaxValidation' => false,
		'htmlOptions' => ['class' => 'well'],
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'hints' => Hint::model()->getMessages('Collection'),
	]
);


echo $form->errorSummary($model);
if (Yii::app()->user->checkAccess("avec-partenaire")) {
	echo UrlFetchableValidator::toggleUrlChecker($model);
}
?>

<fieldset>
	<?php
	echo $form->hiddenField($model, 'ressourceId');
	echo $form->textFieldRow($model, 'nom', ['class' => 'span5']);
	echo $form->dropDownListRow($model, 'type', $types);
	echo $form->textAreaRow($model, 'description', ['rows' => 6, 'cols' => 50, 'class' => 'span8']);
	?>
</fieldset>
<fieldset>
	<?php
	echo '<legend>Ressource ' . CHtml::encode($model->ressource->nom) . '</legend>';
	echo $form->textFieldRow($model, 'identifiant', ['class' => 'input-xxlarge']);
	echo $form->textFieldRow($model, 'url', ['class' => 'span8', 'type' => 'url']);
	echo '<legend>Import spécifique à cette collection</legend>';
	echo $form->checkBoxRow($model, 'importee');
	if (Yii::app()->user->checkAccess('collection/admin')) {
		echo $form->dropDownListRow($model, 'exhaustif', Collection::$enumExhaustif);
	}
	?>
</fieldset>
<div class="form-actions">
	<?php
	$this->widget(
		'bootstrap.widgets.BootButton',
		[
			'buttonType' => 'submit',
			'type' => 'primary',
			'label' => $model->isNewRecord ? 'Créer' : 'Enregistrer',
		]
	);
	?>
</div>

<?php $this->endWidget(); ?>
