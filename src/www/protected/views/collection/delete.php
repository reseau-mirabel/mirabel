<?php

/** @var Controller $this */
/** @var \processes\collection\Delete $process */

assert($this instanceof Controller);

$this->pageTitle = "Collection {$process->collection->nom} - supprimer";

$this->breadcrumbs = [];
$this->breadcrumbs[$process->collection->ressource->nom] = ['/ressource/view', 'id' => $process->collection->ressourceId];
$this->breadcrumbs[] = $process->collection->nom;
?>

<h1>Supprimer la collection <em><?= CHtml::encode($process->collection->getFullName()) ?></em></h1>

<p>Cette collection contient <em><?= $process->servicesCount ?> accès</em> sur des titres.</p>

<?php if ($process->servicesToDelete) { ?>
<p class="alert alert-danger">
	Les <strong><?= count($process->servicesToDelete) ?></strong> accès liés à cette seule collection seront supprimés.
</p>
<?php } ?>

<?php
if ($process->abosCount === 0) {
	?>
	<form name="colection-delete" action="" method="post">
		<p>
			<input type="hidden" name="confirm" value="1" />
			<input type="hidden" name="id" value="<?= (int) $process->collection->id ?>" />
			<button type="submit" class="btn btn-danger">Confirmer cette suppression</button>
		</p>
	</form>
	<?php
} else {
	?>
	<p class="alert alert-danger">
		<strong>Il y a <?= $process->abosCount ?> abonnements.</strong>
		La suppression de collections ayant des abonnements est interdite.
		Il faut que les partenaires se désabonnent au préalable.
	</p>
	<?php
}
