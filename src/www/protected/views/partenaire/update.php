<?php

/** @var Controller $this */
/** @var Partenaire $model */
assert($this instanceof Controller);

$this->breadcrumbs = [
	'Partenaires' => ['index'],
	$model->getFullName() => ['view', 'id' => $model->id],
	"admin" => ['view-admin', 'id' => $model->id],
	'Modifier',
];

\Yii::app()->getComponent('sidebar')->menu = [
	['label' => 'Liste', 'url' => ['index']],
	['label' => 'Administration', 'url' => ['admin']],
];
?>

<h1>Modifier le partenaire <em><?= CHtml::encode($model->getFullName()); ?></em></h1>

<?php
echo $this->renderPartial('_form', ['model' => $model]);
?>
