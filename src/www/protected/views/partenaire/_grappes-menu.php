<?php

/** @var Controller $this */
/** @var Partenaire $partenaire */
/** @var bool $hasGrappesList */
/** @var bool $hasGrappesPage */

/** @psalm-suppress UndefinedGlobalVariable */
\Yii::app()->getComponent('sidebar')->menu = [
	['label' => "Grappes (admin)", 'url' => ['/grappe/admin'], 'visible' => \Yii::app()->user->checkAccess('admin')],
	['label' => "Administrer mes grappes", 'url' => ['/grappe/partenaire', 'id' => $partenaire->id]],
	[
		'label' => 'Créer une grappe',
		'url' => ['/grappe/create'],
	],
	['label' => "Ma sélection de grappes", 'itemOptions' => ['class' => 'nav-header']],
	['label' => "Choisir d'autres grappes", 'url' => ['grappes-selection', 'id' => $partenaire->id]],
	['label' => "Paramètres d'affichage", 'url' => ['grappes-affichage', 'id' => $partenaire->id]],
	['label' => "Voir ma sélection", 'itemOptions' => ['class' => 'nav-header']],
	[
		'label' => "Sur ma page de partenaire",
		'url' => ['view', 'id' => $partenaire->id, '#' => 'grappes-selection'],
		'visible' => $hasGrappesList,
	],
	[
		'label' => "Sur ma page de ma sélection de grappes",
		'url' => ['grappes', 'id' => $partenaire->id],
		'visible' => $hasGrappesPage,
	],
];
