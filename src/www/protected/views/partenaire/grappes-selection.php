<?php

/** @var Controller $this */
/** @var processes\partenaire\GrappesSelection $formData */
/** @var Partenaire $partenaire */

$this->pageTitle = "Ajouter des grappes à ma sélection";

$this->breadcrumbs = [
	"Partenaires" => ['index'],
	$partenaire->nom => ['view', 'id' => $partenaire->id],
	"admin" => ['view-admin', 'id' => $partenaire->id],
	"Affichage des grappes sélectionnées" => ['grappes-affichage', 'id' => $partenaire->id],
	$this->pageTitle,
];
require __DIR__ . '/_grappes-menu.php';

?>
<h1><?= $this->pageTitle ?></h1>

<p>
	Cherchez des grappes partagées par d'autres partenaires et ajoutez les à votre sélection de grappes.
</p>

<?php
$this->renderPartial('_grappe-selection', ['formData' => $formData]);
