<?php

/** @var Controller $this */
/** @var \models\forms\CustomizePartenaireForm $formData */
/** @var \Partenaire $partenaire */

$this->pageTitle = "Administrer les personnalisations";

$this->breadcrumbs = [
	"Partenaires" => ['index'],
	$partenaire->nom => ['view', 'id' => $partenaire->id],
	"admin" => ['view-admin', 'id' => $partenaire->id],
	"Personnalisations",
];

?>
<h1><?= $this->pageTitle ?></h1>

<?php
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'customize-links-form',
		'enableAjaxValidation' => false,
		'hints' => Hint::model()->getMessages('Partenaire'),
		'type' => BootActiveForm::TYPE_HORIZONTAL,
	]
);
/** @var BootActiveForm $form */

echo $form->errorSummary($formData);
?>

<fieldset>
	<legend>Réglages génériques</legend>
	<?php
	echo $form->textFieldRow($formData, 'phrasePerso', ['class' => 'span8']);
	if (empty($formData->editeurId)) {
		echo $form->textFieldRow($formData, 'ipAutorisees', ['class' => 'span8', 'placeholder' => '127.0.* 88.22.66.0/24 99.88.77.66']);
		echo $form->textFieldRow($formData, 'possessionUrl', ['class' => 'span11', 'placeolder' => 'https://exemple.com/notice/IDLOCAL']);
		echo $form->textFieldRow($formData, 'proxyUrl', ['class' => 'span11', 'placeholder' => 'https://proxy.exemple.com/?dest=URL']);
		echo $form->checkBoxRow($formData, 'fusionAcces');
	}
	?>
</fieldset>

<?php if (empty($partenaire->editeurId)) { ?>
<fieldset>
	<legend>États de collections</legend>
	<?php
	echo $form->textFieldRow($formData, 'rcr', ['class' => 'span5']);
	echo $form->checkBoxRow($formData, 'rcrPublic');
	echo $form->checkBoxRow($formData, 'rcrHorsPossession');
	?>
</fieldset>
<?php } ?>

<?php if (empty($partenaire->editeurId)) { ?>
<fieldset>
	<legend>Personnaliser la recherche</legend>
	<p class="alert alert-info">
		Ce réglage permet de changer la sélection de liens affichée dans les résultats d'une recherche détaillée,
		pour les utilisateurs qui affichent le site avec la personnalisation de l'établissement
		« <?= CHtml::encode($partenaire->nom) ?> ».
		L'utilisateur garde la possibilité de déclarer sa propre personnalisation.
	</p>

	<div class="alert alert-info">
		<strong>
			<?php if ($partenaire->persoRecherche === null) { ?>
			Vous n'avez pas défini de
			<?php } else { ?>
			Vous avez déjà défini une
			<?php } ?>
			personnalisation des liens pour votre établissement.
		</strong>
		<br>
		Pour information, la sélection par défaut (hors personnalisation par l'utilisateur ou l'établissement) est :
		<ul>
			<?php
			$encoded = join(
				', ',
				array_map(fn($x) => \Yii::app()->db->quoteValue($x), Sourcelien::DEFAULT_SEARCH_DISPLAY)
			);
			$default = \Yii::app()->db
				->createCommand("SELECT nom FROM Sourcelien WHERE nomcourt IN ($encoded) ORDER BY nom")
				->queryColumn();
			foreach ($default as $name) {
				echo "<li>" . CHtml::encode($name) . "</li>";
			}
			?>
		</ul>
	</div>

	<?php
	echo $form->checkBoxRow(
		$formData,
		'customLinks',
		[
			'hint' => ["Si décochée, la sélection par défaut de Mir@bel sera affichée.<br>Si cochée, la sélection ci-dessous s'appliquera.", "Activer la personnalisation des liens."],
			'id' => 'customize-links-check',
		]
	);
	$customizeLinksState = json_encode($formData->customLinks);
	$defaultJson = json_encode(Sourcelien::DEFAULT_SEARCH_DISPLAY);
	Yii::app()->clientScript->registerScript('onfly-disable', <<<JS
		const customizeLinksDefault = $defaultJson;
		let customizeLinksState = $customizeLinksState;
		$("#customize-links-check").on("change", function() {
			const customizeLinksState = this.checked;
			for (const c of document.querySelectorAll('.link-selection input')) {
				c.disabled = !customizeLinksState;
				if (!customizeLinksState) {
					c.checked = customizeLinksDefault.includes(c.value);
				}
			}
			for (const c of document.querySelectorAll('#customlinks-checkall button')) {
				c.disabled = !customizeLinksState;
			}
		});
		JS
	);
	?>
	<?= $form->checkBoxListRow(
		$formData,
		'selection',
		$formData->listCandidates(),
		['container-class' => 'direction-vertical link-selection', 'disabled' => !$formData->customLinks]
	) ?>
	<div class="controls" style="margin-top: -2ex" id="customlinks-checkall">
		<button type="button" class="btn btn-small" onclick="for (let i of this.closest('fieldset').querySelectorAll('.link-selection input:checked')) { i.checked = false; }">
			Aucun
		</button>
		<button type="button" class="btn btn-small" onclick="for (let i of this.closest('fieldset').querySelectorAll('.link-selection input[type=checkbox]')) { i.checked = true; }">
			Tous
		</button>
	</div>
</fieldset>
<?php } ?>

<div class="form-actions">
	<button type="submit" class="btn btn-primary">Enregistrer ces personnalisations</button>
</div>
<?php
$this->endWidget();
