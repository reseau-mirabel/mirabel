<?php

/** @var Controller $this */
/** @var Partenaire $model */
/** @var string $logoImg */
/** @var Cms[] $actus */
/** @var Grappe[] $grappes */
/** @var bool $grappesPage */

$this->pageTitle = "Partenaire " . $model->nom;
$this->pageDescription = "Accès en ligne aux contenus des revues pour le partenaire {$model->nom}";

$this->breadcrumbs = [
	"Partenaires" => ['index'],
	$model->nom,
];

$encodedName = CHtml::encode($model->nom);
$encodedDescription = CHtml::encode($this->pageDescription);
$this->appendToHtmlHead(<<<EOL
	<meta name="description" content="$encodedDescription" lang="fr" />
	<meta name="keywords" content="revue, accès en ligne, texte intégral, sommaire, périodique, $encodedName" lang="fr" />
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/" />
	<meta name="dcterms.title" content="Mirabel : partenaire $encodedName" />
	<meta name="dcterms.subject" content="revue, accès en ligne, texte intégral, sommaire, périodique, $encodedName" />
	<meta name="dcterms.language" content="fr" />
	<meta name="dcterms.creator" content="Mirabel" />
	<meta name="dcterms.publisher" content="Sciences Po Lyon" />
	EOL
);
$logoUrl = $model->getLogoUrl(false);
if ($logoUrl) {
	Yii::app()->clientScript->registerMetaTag($logoUrl, 'og:image', null, ['property' => 'og:image'], 'og:image');
}
?>

<div class="columns">
	<div style="flex:2">
		<?php
		if (!Yii::app()->user->isGuest && Yii::app()->user->checkAccess('avec-partenaire')) {
			echo CHtml::link("Vue admin", ['view-admin', 'id' => $model->id], ['class' => 'btn btn-default pull-right']);
		}
		?>
		<h1>
			<?php
			if ($model->editeurId > 0) {
				echo CHtml::image(
					"/images/mirabel_partenaires.png",
					"partenaire du réseau Mirabel",
					['width' => "32", 'height' => "32", 'style' => "vertical-align: baseline;", 'title' => "les partenaires du réseau Mir@bel"]
				);
			}
			?>
			<?= CHtml::encode($model->nom) ?>
		</h1>
		<?php
		// Détails
		$this->renderPartial('_view_section-presentation', ['partenaire' => $model, 'permModif' => false, 'permView' => false]);

		// Suivi
		$this->renderPartial('_view_section-suivi', ['partenaire' => $model]);

		// Grappes
		if ($grappes || $grappesPage) {
			echo '<section id="grappes-selection">';
			printf('<h2 style="margin-bottom:0;">Sélection de grappes – %s</h2>', $model->getShortName());
			if ($grappes) {
				$this->renderPartial('/grappe/_liste-publique', ['grappes' => $grappes, 'level' => 3]);
			}
			if ($grappesPage) {
				echo '<p>Consulter la page de la '
					. CHtml::link("sélection détaillée de grappes", ['grappes', 'id' => $model->id])
					. '.</p>';
			}
			echo '</section>';
		}
		?>
	</div>
	<div style="flex:1; max-width: 33%;">
		<div style="text-align: center">
			<?= $model->getLogoImg(true) ?>
		</div>
		<?php
		if ($model->editeurId > 0) {
			echo '<p style="text-align:center; margin: 1em 0 2em;">Ce partenaire est associé à la ',
				CHtml::link(
					"<strong>page éditeur</strong> " . CHtml::encode($model->editeur->nom),
					$model->editeur->getSelfUrl(),
					['class' => 'btn btn-light']
				),
				"</p>";
		}
		if ($actus) {
			$displayMore = false;
			if (count($actus) > \controllers\partenaire\ViewAction::MAX_ACTUS) {
				$displayMore = true;
				array_pop($actus);
			}
			echo processes\cms\ExpandVariables::formatNewsTitles($actus);
			if ($displayMore) {
				echo '<div style="margin-top: 1em">'
					. CHtml::link(
						"→ Toute l'actualité de <em>" . CHtml::encode($model->nom) . "</em> dans Mir@bel",
						['/site/actualite', 'q' => ['partenaireId' => $model->id]]
					)
					. "</div>\n";
			}
		}
		?>
	</div>
</div>
