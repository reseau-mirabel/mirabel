<?php

/** @var Controller $this */
/** @var Partenaire $partenaire */

assert($this instanceof Controller);
$pencilIcon = '<span class="glyphicon glyphicon-pencil"></span>';
$searchIcon = '<small><span class="glyphicon glyphicon-search"></span></small>';
?>

<section>
<h2>Les données suivies par ce partenaire dans Mir@bel</h2>
<?php if (Yii::app()->user->checkAccess("avec-partenaire")) { ?>
	<?php
	if ($partenaire->editeurId > 0) {
		echo "<p>Ce compte de partenaire est associé à ",
			CHtml::link(
				"l'éditeur <strong>" . CHtml::encode($partenaire->editeur->nom) . "</strong>",
				$partenaire->editeur->getSelfUrl()
			),
			" dans Mir@bel.</p>";
	}
	?>
	<?php if (Yii::app()->user->access()->toPartenaire()->suivi($partenaire)) { ?>
	<p>
		Consulter le <?= CHtml::link("tableau de bord", ['/partenaire/tableau-de-bord', 'id' => $partenaire->id]) ?> détaillé,
		<?php if (Yii::app()->user->access()->toPartenaire()->suivi($partenaire)) { ?>
		ou gérer le suivi des <?= CHtml::link("revues $pencilIcon", ['partenaire/suivi', 'id' => $partenaire->id, 'target' => 'Revue']) ?>
		et/ou <?= CHtml::link("ressources $pencilIcon", ['partenaire/suivi', 'id' => $partenaire->id, 'target' => 'Ressource']) ?>.
		<?php } ?>
	</p>
	<?php } ?>
<?php } ?>
<?php
$tabSuivi = $partenaire->getRightsSuivi(includeEditeurs: true);
if (count($tabSuivi)) {
	echo '<ul class="unstyled">';
	if (!empty($tabSuivi['Revue'])) {
		$nbRevues = count($tabSuivi['Revue']);
		echo "<li>$searchIcon "
			. CHtml::link(
				$nbRevues . ' revue' . ($nbRevues > 1 ? 's' : ''),
				['/revue/search', 'SearchTitre[suivi][]' => $partenaire->id]
			)
			. '</li>';
	}
	if (!empty($tabSuivi['Ressource'])) {
		$nbRessources = count($tabSuivi['Ressource']);
		echo "<li>$searchIcon "
			. CHtml::link(
				$nbRessources . ' ressource' . ($nbRessources == 1 ? '' : 's'),
				['/ressource/index', 'suivi' => $partenaire->id]
			)
			. '</li>';
	}
	if (!empty($tabSuivi['Editeur'])) {
		$nbEditeurs = count($tabSuivi['Editeur']);
		if ($nbEditeurs === 1) {
			$editeur = Editeur::model()->findByPk($tabSuivi['Editeur'][0]);
			echo "<li>$searchIcon "
				. CHtml::link("1 éditeur, " . CHtml::encode($editeur->getFullName()), ['/editeur/view', 'id' => $tabSuivi['Editeur'][0]])
				. '</li>';
		} else {
			echo "<li>$nbEditeurs éditeurs</li>";
		}
	}
	echo '</ul>';
} else {
	echo '<p>Aucun objet actuellement suivi.</p>';
}
?>
</section>
