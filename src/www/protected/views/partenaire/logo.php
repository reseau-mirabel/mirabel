<?php

/** @var Controller $this */
/** @var Partenaire $model */
/** @var Upload $upload */

use components\HtmlHelper;

assert($this instanceof Controller);

$this->pageTitle = sprintf("Partenaire %s - Logo", $model->getShortName());

$this->breadcrumbs=[
	'Partenaires' => ['index'],
	$model->getFullName() => ['view', 'id' => $model->id],
	"admin" => ['view-admin', 'id' => $model->id],
	'Logo',
];
?>

<h1>Logo du partenaire <em><?= CHtml::encode($model->getFullName()); ?></em></h1>

<p class="alert alert-info">
	Le logo sera redimensionné automatiquement après le téléchargement.
</p>

<div>
	<?php
	$img = $model->getLogoImg();
	if ($img) {
		echo "<div style=\"display: inline-block;\">Logo actuel : $img</div>";
		if (!$model->logoUrl) {
			echo ' <div style="display: inline-block;">'
				. HtmlHelper::postButton("Supprimer", ['logo', 'id' => $model->id], ['delete' => 1])
				. '</div>';
		}
	} else {
		echo "<strong>Pas encore de logo.</strong>";
	}
	?>
</div>

<section style="margin-top: 2ex">
	<h2>Utiliser un logo déjà en ligne</h2>
	<?php
	$form = $this->beginWidget(
		'bootstrap.widgets.BootActiveForm',
		[
			'id' => 'logo-url-form',
			'action' => ['logo', 'id' => $model->id],
			'enableAjaxValidation' => false,
			'type' => BootActiveForm::TYPE_HORIZONTAL,
			'hints' => Hint::model()->getMessages('Partenaire'),
		]
	);
	/** @var BootActiveForm $form */
	echo $form->errorSummary($model);
	echo $form->textFieldRow($model, 'logoUrl', ['class' => 'span11']);
	?>
	<div class="form-actions">
		<button class="btn btn-primary" type="submit">Télécharger ce logo</button>
		<?php
		if ($model->logoUrl) {
			echo "   <span>Une URL vide effacera le logo.</span>";
		}
		?>
	</div>
	<?php $this->endWidget() ?>
</section>

<section>
	<h2>Envoyer un fichier image</h2>
	<?php
	$formUp = $this->beginWidget(
		'bootstrap.widgets.BootActiveForm',
		[
			'id' => 'logo-upload-form',
			'action' => ['logo', 'id' => $model->id],
			'enableAjaxValidation' => false,
			'type' => BootActiveForm::TYPE_HORIZONTAL,
			'hints' => Hint::model()->getMessages('Partenaire'),
			'htmlOptions' => ['enctype' => 'multipart/form-data'],
		]
	);
	/** @var BootActiveForm $formUp */
	echo $formUp->errorSummary($upload);
	echo $formUp->fileFieldRow($upload, 'file', ['class' => 'span11', 'accept' => '.jpg,.jpeg,.png']);
	?>
	<div class="form-actions">
		<button class="btn btn-primary" type="submit">Envoyer ce fichier</button>
	</div>
	<?php $this->endWidget() ?>
</section>
