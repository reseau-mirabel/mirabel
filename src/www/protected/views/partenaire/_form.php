<?php

/** @var Controller $this */
/** @var Partenaire $model */

assert($this instanceof Controller);

/** @var BootActiveForm */
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'partenaire-form',
		'enableAjaxValidation' => false,
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'hints' => Hint::model()->getMessages('Partenaire'),
		'htmlOptions' => ['class' => 'well'],
	]
);
?>

<?= $form->errorSummary($model) ?>

<fieldset>
	<legend>Présentation publique</legend>
	<?php
	echo $form->textFieldRow($model, 'nom', ['class' => 'span11']);
	echo $form->textFieldRow($model, 'prefixe', ['class' => 'span1']);
	echo $form->textFieldRow($model, 'sigle', ['class' => 'span5']);

	$types = Partenaire::$enumType;
	if ($model->editeurId) {
		$types = ['editeur' => 'Éditeur'];
	} else {
		unset($types['editeur']);
	}

	echo $form->textAreaRow($model, 'description', ['rows' => 1, 'cols' => 50, 'class' => 'span8']);

	new \widgets\CkEditor();
	echo $form->textAreaRow($model, 'presentation', ['rows' => 10, 'cols' => 50, 'class' => 'span8 htmleditor']);

	if (empty($model->editeurId)) {
		echo $form->textFieldRow($model, 'url', ['class' => 'span11', 'type' => 'url']);
	}
	?>
</fieldset>

<fieldset>
	<legend>
		Détails du partenaire
		<abbr title="informations non publiques"><small class="glyphicon glyphicon-lock"></small></abbr>
	</legend>
	<?php
	echo $form->textAreaRow($model, 'email', ['class' => 'span8', 'rows' => 3]);
	if (empty($model->editeurId)) {
		echo $form->textAreaRow($model, 'importEmail', ['class' => 'span8', 'rows' => 3, 'placeholder' => 'une adresse électronique par ligne']);
	}
	echo $form->textAreaRow($model, 'implication', ['rows' => 10, 'cols' => 50, 'class' => 'span8 htmleditor']);
	if (empty($model->editeurId)) {
		echo $form->textFieldRow($model, 'sigb', ['class' => 'span8']);
	}
	if (Yii::app()->user->access()->hasPermission('admin')) {
		echo $form->textAreaRow($model, 'notes', ['class' => 'span11', 'placeholder' => "Notes privées, réservées aux admins."]);
		echo $form->dropDownListRow($model, 'statut', Partenaire::$enumStatut);
		echo $form->dropDownListRow($model, 'type', $types);
	}
	?>
</fieldset>

<fieldset>
	<legend>
		Utilisation des données de Mir@bel
		<abbr title="informations non publiques"><small class="glyphicon glyphicon-lock"></small></abbr>
	</legend>
	<div class="control-group">
		<?= CHtml::label("Extension activée", "extension", array('class' => 'control-label')); ?>
		<div class="controls">
			<?= CHtml::checkBoxList("Partenaire[usageLocal][]", json_decode($model->usageLocal), Partenaire::EXTENSION, array(
				'separator' => '',
				'template' => '{beginLabel} {input}{labelTitle} {endLabel}',
				'labelOptions' => array(
					'class' => 'checkbox inline'
				)
			)); ?>
		</div>
	</div>

	<div class="control-group">
		<?= CHtml::label("Outils de découverte", "decouverte", array('class' => 'control-label')); ?>
		<div class="controls">
			<?= CHtml::checkBoxList("Partenaire[usageLocal][]", json_decode($model->usageLocal), Partenaire::DECOUVERTE,  array(
				'separator' => '',
				'template' => '{beginLabel} {input}{labelTitle} {endLabel}',
				'labelOptions' => array(
					'class' => 'checkbox inline'
				)
			)); ?>
		</div>
	</div>

	<div class="control-group">
		<?= CHtml::label("Autres usages", "usageLocalAutre", array('class' => 'control-label')) ?>
		<div class="controls">
			<?= CHtml::textField(
				"Partenaire[usageLocal][]",
				implode(" ; ", $model->getAutreUsage()),
				['class' => 'span11', 'hint' => $form->hints['usageLocal'][0] ?? '']
			) ?>
		</div>
	</div>
</fieldset>
<fieldset>
	<legend>Géo-localisation</legend>
	<?php
	echo $form->dropDownListRow($model, 'paysId', CHtml::listData(Pays::model()->sorted()->findAll(), 'id', 'nom'), ['empty' => '—']);
	echo $form->textFieldRow($model, 'geo');
	?>
	<button type="button" id="geolocate" style="display: none; float: right">Utiliser ma position actuelle</button>
	<?php
	echo $form->textFieldRow($model, 'latitude');
	echo $form->textFieldRow($model, 'longitude');

	Yii::app()->clientScript->registerScript("geolocate", '
		if ("geolocation" in navigator && navigator.geolocation) {
			$("#geolocate").show()
				.on("click", function(){
					navigator.geolocation.getCurrentPosition(
						function(position) {
							$("#Partenaire_latitude").val(position.coords.latitude);
							$("#Partenaire_longitude").val(position.coords.longitude);
							$("#geolocate-warning").show();
						},
						function() { alert("Erreur en lisant la position."); },
						{ enableHighAccuracy: true }
					);
				});
		}
		');
	?>
	<div id="geolocate-warning" style="display: none">
		<strong>Les données de géolocalisation ne sont pas toujours fiables.</strong>
		Après validation, vous pourrez le vérifier sur la <?= CHtml::link("carte des partenaires", ['/partenaire/carte'], ['target' => '_blank']) ?>.
		Si cela ne fonctionne pas, indiquez manuellement votre latitude &amp; longitude.
	</div>
</fieldset>

<div class="form-actions">
	<?php
	$this->widget(
		'bootstrap.widgets.BootButton',
		[
			'buttonType' => 'submit',
			'type' => 'primary',
			'label' => $model->isNewRecord ? 'Créer' : 'Enregistrer',
		]
	);
	?>
</div>

<?php
$this->endWidget();
?>
