<?php

use processes\partenaire\GrappeResult;

/** @var Controller $this */
/** @var processes\partenaire\GrappesSelection $formData */
/** @var bool $hasGrappesList */
/** @var bool $hasGrappesPage */

/** @var BootActiveForm $form */
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'enableAjaxValidation' => false,
		'method' => 'GET',
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'hints' => Hint::model()->getMessages(processes\partenaire\GrappesSelection::class),
	]
);

echo $form->errorSummary($formData);
echo $form->textFieldRow($formData, 'nom', ['id' => 'grappe-search']);
$cs = Yii::app()->getClientScript();
$cs->registerCoreScript('jquery.ui');
$cs->registerCssFile($cs->getCoreScriptUrl() . '/jui/css/base/jquery-ui.css');
$cs->registerScript('complete', <<<EOJS
	$("#grappe-search").autocomplete({
		focus: () => false,
		minlength: 2,
		select: function(event, ui) {
			const terms = this.value.split(/[,;\s]+/);
			terms.pop();
			terms.push(ui.item.value);
			this.value = terms.join(" ");
			return false;
		},
		source: function(request, response) {
			$.ajax({
				url: "/grappe/complete-mot",
				data: {"term": request.term},
				success: function(data) { response(data); }
			});
		}
	})
	EOJS
);
?>
<div class="form-actions">
	<button type="submit" class="btn btn-primary">Chercher des grappes</button>
</div>
<?php
$this->endWidget();
?>

<?php
if ($formData->nom !== null) {
	/** @var BootActiveForm $form */
	$form = $this->beginWidget(
		'bootstrap.widgets.BootActiveForm',
		[
			'enableAjaxValidation' => false,
			'method' => 'POST',
			'type' => BootActiveForm::TYPE_HORIZONTAL,
			'hints' => Hint::model()->getMessages(processes\partenaire\GrappesSelection::class),
		]
	);
	echo $form->hiddenField($formData, 'nom');
	/** @var BootActiveForm $form */
	$this->widget(
		'ext.bootstrap.widgets.BootGridView',
		[
			'dataProvider' => $formData->search(),
			'filter' => null,
			'columns' => [
				'id',
				[
					'name' => 'nom',
					'type' => 'raw',
					'value' => fn (GrappeResult $r) => $r->getSelfLink(),
				],
				[
					'name' => 'size',
					'value' => fn (GrappeResult $r) => $r->size . " " . ((int) $r->niveau === Grappe::NIVEAU_TITRE ? "titres" : "revues"),
				],
				[
					'name' => 'partenaireNom',
					'type' => 'raw',
					'value' => fn (GrappeResult $r) => ($r->partenaireNom ? CHtml::encode($r->partenaireNom) : '<em>Mir@bel</em>'),
				],
				[
					'name' => 'selected',
					'type' => 'boolean',
					'sortable' => false,
				],
				[
					'header' => '<input type="checkbox" id="toggle-all-checkboxes">',
					'sortable' => false,
					'type' => 'raw',
					'value' => function (GrappeResult $r) use ($form, $formData) {
						if ($r->selected) {
							return '';
						}
						return $form->checkBox($formData, "add[]", ['class' => 'togglable', 'uncheckValue' => null, 'value' => "{$r->id}"]);
					},
				],
			],
			'ajaxUpdate' => false,
		]
	);
	Yii::app()->clientScript->registerScript('toggle-all', <<<JS
		document.getElementById('toggle-all-checkboxes').addEventListener('change', function(e) {
			const checked = e.target.checked;
			for (let x of document.querySelectorAll('input.togglable')) {
				x.checked = checked;
			}
		});
		JS
	);
	echo <<<HTML
		<div class="form-actions">
			<button type="submit" class="btn btn-primary">Ajouter les grappes cochées à ma sélection</button>
		</div>
		HTML;
	$this->endWidget();
}
