<?php

/** @var Controller $this */
/** @var Partenaire $partenaire */
/** @var bool $permModif */

$lockIcon = '<abbr title="informations non publiques"><small class="glyphicon glyphicon-lock"></small></abbr>';
?>
<section>
	<h2>Détails du partenaire <?= $lockIcon ?></h2>
	<?php
	$detailedAttributes = [
		[
			'name' => 'notes',
			'type' => 'ntext',
			'visible' => Yii::app()->user->access()->hasPermission('admin'),
		],
		'email:emails',
		'importEmail:emails',
		[
			'name' => 'implication',
			'type' => 'raw',
			'visible' => Yii::app()->user->checkAccess("avec-partenaire"),
		],
		[
			'name' => 'sigb',
			'visible' => Yii::app()->user->checkAccess("avec-partenaire") && (!$partenaire->editeurId),
		],
		['name' => 'statut', 'value' => Partenaire::$enumStatut[$partenaire->statut]],
		[
			'name' => 'type',
			'type' => 'raw',
			'value' => Partenaire::$enumType[$partenaire->type]
				. ($partenaire->editeurId ?
					" " . CHtml::link(CHtml::encode($partenaire->editeur->nom), $partenaire->editeur->getSelfUrl(), ['class' => 'btn btn-light'])
					: ""
				),
			'visible' => $permModif,
		],
		'hdateCreation:datetime',
		'hdateModif:datetime',
	];
	$this->widget(
		'bootstrap.widgets.BootDetailView',
		[
			'data' => $partenaire,
			'attributes' => $detailedAttributes,
			'hideEmptyLines' => true,
		]
	);
	?>
</section>