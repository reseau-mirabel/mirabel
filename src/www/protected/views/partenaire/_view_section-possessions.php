<?php

/** @var Controller $this */
/** @var Partenaire $partenaire */

$lockIcon = '<abbr title="informations non publiques"><small class="glyphicon glyphicon-lock"></small></abbr>';
$pencilIcon = '<span class="glyphicon glyphicon-pencil"></span>';
$searchIcon = '<small><span class="glyphicon glyphicon-search"></span></small>';
$numTitres = (int) Yii::app()->db
	->createCommand("SELECT count(*) FROM Partenaire_Titre WHERE partenaireId = " . $partenaire->id)
	->queryScalar();
?>
<section>
	<h2>Possessions de titres de revues déclarées par ce partenaire dans Mir@bel <?= $lockIcon ?></h2>
	<p>
		<?php
		if ($numTitres == 0) {
			echo "Aucun titre.";
		} else {
			echo "$searchIcon "
				. CHtml::link("$numTitres titres de revues.", ['/revue/search', 'q' => ['detenu' => $partenaire->id]]);
		}
		?>
	</p>
	<?php if (Yii::app()->user->checkAccess('possession/update', ['Partenaire' => $partenaire])) { ?>
	<p>
		<?= CHtml::link("Modifier manuellement les possessions $pencilIcon", ['possession', 'id' => $partenaire->id]) ?>
		ou les
		<?= CHtml::link("modifier via un import CSV $pencilIcon", ['/possession/import', 'partenaireId' => $partenaire->id]) ?>
	</p>
	<?php } ?>
</section>
