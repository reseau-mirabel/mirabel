<?php

/** @var Controller $this */
/** @var Partenaire $partenaire */
/** @var bool $permAdminGlobal */
/** @var bool $permAdminLocal */

$lockIcon = '<abbr title="informations non publiques"><small class="glyphicon glyphicon-lock"></small></abbr>';
$pencilIcon = '<span class="glyphicon glyphicon-pencil"></span>';
?>
<section id="partenaire-utilisateurs">
	<?php
	if ($permAdminGlobal) {
		echo '<div class="pull-right-icon">'
			. CHtml::link(
				$pencilIcon,
				['/utilisateur/admin', 'UtilisateurSearch' => ['partenaireId' => $partenaire->id]],
				['title' => "Gérer les utilisateurs (admin)"]
			)
			. '</div>';
	}

	echo "<h2>Utilisateurs associés $lockIcon</h2>\n";
	$users = $partenaire->utilisateurs;
	if ($users) {
		$output = [];
		foreach ($users as $user) {
			if ($user->actif) {
				$name = $user->nomComplet ?: $user->login;
				$output[] = '<span class="utilisateur">'
					. CHtml::link(
						CHtml::encode($name),
						['/utilisateur/view', 'id' => $user->id],
						['title' => $user->nomComplet]
					) . "</span>";
			}
		}
		echo "<p>" . join(" / ", $output) . "</p>";
	} else {
		echo '<p>Aucun</p>'
			. ($permAdminLocal ? '<p class="alert alert-warning">Utilisez le menu latéral pour créer un compte utilisateur.</p>' : '');
	}
	?>
</section>
