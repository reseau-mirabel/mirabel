<?php

use models\sphinx\Revues;

/** @var Controller $this */
/** @var Partenaire $partenaire */
/** @var RevueSearch $suiviRevues */
/** @var array $revueIdWithOrderProblem */
/** @var RessourceSearch $suiviRessources */
/** @var array $intvByCategory */
/** @var array $shares */

assert($this instanceof Controller);

$this->pageTitle = 'Tableau de bord - ' . $partenaire->nom;

$this->breadcrumbs = [
	'Partenaires' => ['index'],
	$partenaire->nom => ['view', 'id' => $partenaire->id],
	"admin" => ['view-admin', 'id' => $partenaire->id],
	"Tableau de bord",
];

$alertBefore = (int) strtotime("-6 month");
?>

<h1>
	Tableau de bord
	<small class="pull-right">
		<?= $partenaire->getLogoImg(); ?>
		<?= CHtml::encode($partenaire->nom); ?>
	</small>
</h1>

<?php
if ($partenaire->editeurId) {
	echo "<p>Ce compte de partenaire est associé à l'éditeur " . $partenaire->editeur->getSelfLink() . ".</p>";
}
?>

<?php
if ($intvByCategory) {
	$this->renderPartial('/utilisateur/_blocSuivi', ['intvByCategory' => $intvByCategory]);
}
?>

<section id="partenaire-suivi">
	<h2>Les données suivies dans Mir@bel</h2>

	<h3>Revues suivies</h3>
	<?php
	$revColumns = [
		[
			'name' => 'titrecomplet',
			'header' => 'Revue',
			'type' => 'html',
			'value' => function (Revues $t) use ($revueIdWithOrderProblem) {
				return CHtml::link(CHtml::encode($t->titrecomplet), ['/revue/view', 'id' => $t->revueid])
					. ($t->vivant ? "" : ' <span class="label label-info" title="Le dernier titre de cette revue a une date de fin.">†</span>')
					. ($t->revuecategorie ? "" : ' <small class="label label-warning">sans thème</small>')
					. (in_array($t->revueid, $revueIdWithOrderProblem) ?
						CHtml::link(' <span title="Tri nécessaire" class="glyphicon glyphicon-warning-sign" style="color:#eb6907"></span>', ['revue/trier-titre', 'id' => $t->revueid])
						: "")
				;
			},
		],
		[
			'visible' => in_array('Revue', $shares),
			'header' => '<abbr title="Lorsque le suivi est partagé par plusieurs partenaires">Autre suivi</abbr>',
			'type' => 'html',
			'value' => function (Revues $t) use ($partenaire) {
				$suivi = $t->getSuiviIds();
				if (count($suivi) <= 1) {
					return "";
				}
				$names = [];
				foreach ($suivi as $pid) {
					if ((int) $pid !== (int) $partenaire->id) {
						$names[] = CHtml::link(
							CHtml::encode(Partenaire::model()->findByPk($pid)->nom),
							['/partenaire/view', 'id' => $pid]
						);
					}
				}
				return join(" ; ", $names);
			},
		],
		[
			'name' => 'hdatemodif',
			'type' => 'date',
			'header' => 'Dernière modif.',
			'filter' => false,
		],
		[
			'name' => 'hdateverif',
			'type' => 'html',
			'header' => 'Dernière vérif.',
			'value' => function (Revues $t) use ($alertBefore) {
				$text = Yii::app()->format->formatDate($t->hdateverif);
				if (!$t->hdateverif || $t->hdateverif < $alertBefore) {
					$text .= ' <i class="icon icon-warning-sign" title="Attention, votre dernière vérification remonte à plus de 6 mois"></i>';
				}
				return $text;
			},
			'filter' => false,
		],
	];
	$this->widget(
		'ext.bootstrap.widgets.BootGridView',
		[
			'id' => 'partenaire-suivi-revues-grid',
			'dataProvider' => $suiviRevues->search(),
			'filter' => $suiviRevues,
			'columns' => $revColumns,
			'ajaxUpdate' => false,
			'summaryText' => CHtml::link('{count} revues suivies', ['/revue/search', 'SearchTitre[suivi][]' => $partenaire->id]),
			'rowHtmlOptionsExpression' => function (int $index, Revues $t) {
				return ['class' => "revue-{$t->revueid}"];
			},
		]
	);
	?>

	<h4>Liens morts dans ces revues</h4>
	<p>
		Consulter les listes
		des <?= CHtml::link("liens morts pour ces revues", ['/verification/liensRevues', 'VerifUrlForm[suiviPar]' => $partenaire->id]) ?>
		et des <?= CHtml::link("liens morts pour leurs éditeurs", ['/verification/liensEditeurs', 'VerifUrlForm[suiviPar]' => $partenaire->id]) ?>.
	</p>

	<?= $this->renderPartial(
		'_suivi-ressources',
		['partenaire' => $partenaire, 'suiviRessources' => $suiviRessources, 'shares' => $shares]
	) ?>
</section>
