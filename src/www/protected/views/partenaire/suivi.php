<?php

/** @var Controller $this */
/** @var \processes\partenaire\Follow $process */
/** @var array $duplicates */

assert($this instanceof Controller);

$existing = $process->findExisting();

$this->breadcrumbs = [
	'Partenaires' => ['index'],
	$process->partenaire->nom => ['view', 'id' => $process->partenaire->id],
	"admin" => ['view-admin', 'id' => $process->partenaire->id],
	"Suivi ({$process->suivi->cible})",
];

if (Yii::app()->user->checkAccess("avec-partenaire")) {
	\Yii::app()->getComponent('sidebar')->menu = [
		['label' => 'Liste des partenaires', 'url' => ['index']],
		['label' => ''],
		['label' => 'Suivi', 'itemOptions' => ['class' => 'nav-header']],
		['label' => "Tableau de bord", 'url' => ['tableau-de-bord', 'id' => $process->partenaire->id]],
		['label' => 'Revues', 'url' => ['suivi', 'id' => $process->partenaire->id, 'target' => 'Revue'], 'visible' => ($process->suivi->cible !== 'Revue')],
		['label' => 'Ressources', 'url' => ['suivi', 'id' => $process->partenaire->id, 'target' => 'Ressource'], 'visible' => ($process->suivi->cible !== 'Ressource')],
		[
			'label' => 'Éditeurs',
			'url' => ['suivi', 'id' => $process->partenaire->id, 'target' => 'Editeur'],
			'visible' => ($process->partenaire->type === 'editeur' && $process->suivi->cible !== 'Editeur'),
		],
	];
}
?>

<h1>Suivi pour le partenaire <em><?= CHtml::encode($process->partenaire->nom) ?></em></h1>

<h2>Nouveau suivi</h2>
<p>
	Utilisez la complétion pour ajouter de nouveaux éléments.
</p>
<?php
/** @var BootActiveForm $form */
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'suivi-add-form',
		'enableAjaxValidation' => false,
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'hints' => Hint::model()->getMessages('Suivi'),
		'htmlOptions' => ['class' => 'well'],
	]
);
echo $form->hiddenField($process->suivi, 'partenaireId');
echo $form->hiddenField($process->suivi, 'cible');
$this->widget(
	'ext.MultiAutoComplete.MultiAutoComplete',
	[
		'model' => $process->suivi,
		'attribute' => 'cibleId',
		'foreignModelName' => $process->suivi->cible,
		'foreignAttribute' => 'fullName',
		'ajaxUrl' => $this->createUrl('/' . strtolower($process->suivi->cible) . '/ajax-complete'),
		'options' => [
			// min letters typed before we try to complete
			'minLength' => '3',
		],
		'htmlOptions' => [
			'hint' => empty($form->hints['cible']) ? null : $form->hints['cible'],
		],
	]
);
echo CHtml::submitButton("Ajouter ces suivis", ['class' => 'btn']);
$this->endWidget();
?>

<h2><?= count($existing) ?> suivis déjà déclarés</h2>
<?php
if (empty($existing)) {
	echo "<p>Aucun objet de type '{$process->suivi->cible}' n'est actuellement suivi.";
} else {
	echo CHtml::form('', 'post', ['class' => 'form-horizontal', 'id' => 'suivi-remove-form'])
		. CHtml::hiddenField('id', (string) $process->partenaire->id)
		. CHtml::hiddenField('target', $process->suivi->cible);
	echo CHtml::submitButton("Enregistrer les modifications", ['class' => 'btn']);

	$columns = [
		[
			'header' => '',
			'value' => function ($object) {
				return CHtml::checkBox("existing[{$object->id}]", true, ['uncheckValue' => 0]);
			},
			'type' => 'raw',
		],
		[
			'header' => $process->suivi->cible,
			'value' => function ($object) {
				return CHtml::tag("label", ['for' => "existing[{$object->id}]"], $object->getSelfLink());
			},
			'type' => 'raw',
		],
		[
			'header' => "Suivis en doublon",
			'value' => function ($object) use ($duplicates) {
				if (!isset($duplicates[$object->id])) {
					return '';
				}
				$html = [];
				foreach ($duplicates[$object->id] as $pData) {
					$p = new Partenaire();
					$p->setAttributes($pData, false);
					$html[] = $p->getSelfLink(true);
				}
				return join(", ", $html);
			},
			'type' => 'raw',
		],
	];
	$this->widget(
		'ext.bootstrap.widgets.BootGridView',
		[
			'id' => 'partenaire-suivi-grid',
			'htmlOptions' => ['class' => 'table table-striped table-condensed'],
			'dataProvider' => new CArrayDataProvider($existing, ['pagination' => false]),
			'filter' => false,
			'enablePagination' => false,
			'columns' => $columns,
			'ajaxUpdate' => false,
			'summaryText' => false,
		]
	);

	echo CHtml::submitButton("Enregistrer les modifications", ['class' => 'btn']);
	echo CHtml::endForm();
}
?>
