<?php

/** @var Controller $this */
/** @var int $editeurId */

$lockIcon = '<abbr title="informations non publiques"><small class="glyphicon glyphicon-lock"></small></abbr>';
$uploadDest = \processes\upload\UploadDestination::editeurs();
$uploadedFiles = $uploadDest->glob("{$editeurId}_*");
?>
<section>
	<h2>Lettre de partenariat <?= $lockIcon ?></h2>
	<?php
	if (!$uploadedFiles) {
		echo "<p class=\"alert alert-warning\">Aucune lettre n'a encore été déposée sur le site.</p>";
	} else {
		echo "<ul>";
		foreach($uploadedFiles as $uploaded) {
			echo CHtml::tag(
				"li",
				[],
				CHtml::link($uploaded, $this->createUrl('/upload/view') . "/editeurs/$uploaded")
			);
		}
		echo "</ul>";
	}
	?>
</section>
