<?php

/** @var Controller $this */
/** @var AbonnementInfo $abonnementInfo */
/** @var Partenaire $partenaire */

$lockIcon = '<abbr title="informations non publiques"><small class="glyphicon glyphicon-lock"></small></abbr>';
$pencilIcon = '<span class="glyphicon glyphicon-pencil"></span>';
$searchIcon = '<small><span class="glyphicon glyphicon-search"></span></small>';
?>
<section>
	<?php
	if (Yii::app()->user->checkAccess('abonnement/update', $partenaire)) {
		echo '<div class="pull-right-icon">'
			. CHtml::link($pencilIcon, ['abonnements', 'id' => $partenaire->id], ['title' => "Modifier les abonnements"])
			. '</div>';
	}
	?>
	<h2>Abonnements électroniques déclarés par ce partenaire dans Mir@bel <?= $lockIcon ?></h2>
	<?php if ($abonnementInfo->count > 0) { ?>
	<div>
		<div>
			<?= $searchIcon ?>
			<?= CHtml::link("{$abonnementInfo->count} abonnements électroniques", ['/partenaire/abonnements', 'id' => $partenaire->id]) ?>
			(<?= $abonnementInfo->countRessources ?> ressources et <?= $abonnementInfo->countCollections ?> collections)
		</div>
		<div>
			<?= $searchIcon ?>
			donnant accès à <?= CHtml::link("{$abonnementInfo->countRevues} revues", ['/revue/search', 'q' => ['abonnement' => 1, 'pid' => $partenaire->id]]) ?>.
		</div>
	</div>
	<?php } else { ?>
	<p>
		Aucun abonnement déclaré pour l'instant.
	</p>
	<?php } ?>
</section>
