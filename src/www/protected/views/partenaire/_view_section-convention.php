<?php

/** @var Controller $this */
/** @var array $uploadedFiles */

$lockIcon = '<abbr title="informations non publiques"><small class="glyphicon glyphicon-lock"></small></abbr>';
?>
<section>
	<h2>Convention <?= $lockIcon ?></h2>
	<?php
	if (!$uploadedFiles) {
		echo "<p class=\"alert alert-warning\">Aucune convention n'a encore été déposée sur le site.</p>";
	} else {
		echo "<ul>";
		foreach($uploadedFiles as $uploaded) {
			echo CHtml::tag(
				"li",
				[],
				CHtml::link($uploaded, $this->createUrl('/upload/view') . "/$uploaded")
			);
		}
		echo "</ul>";
	}
	?>
</section>
