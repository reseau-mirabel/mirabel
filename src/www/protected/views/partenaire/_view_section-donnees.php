<?php

/** @var Controller $this */
/** @var Partenaire $partenaire */
/** @var bool $permModif */

$lockIcon = '<abbr title="informations non publiques"><small class="glyphicon glyphicon-lock"></small></abbr>';
?>
<section>
	<h2>Utilisation des données dans Mir@bel <?= $lockIcon ?></h2>
	<?php
	$internalAttributes = [
		[
			'name' => 'usageLocal',
			'value' => function($partenaire) {
				return implode(', ',json_decode($partenaire->usageLocal,true));
			},
		],
		[
			'name' => 'hash',
			'label' => 'Clé <a href="/api">API</a>',
			'type' => 'raw',
			'value' =>
				CHtml::tag(
					'span',
					['id' => 'partenaire-hash', 'title' => "Clé d'accès à l'API de Mir@bel"],
					$partenaire->hash ? CHtml::encode($partenaire->hash) : ""
				)
				. " "
				. ($permModif ?
					CHtml::tag('button', ['id' => "partenaire-hash-refresh", 'title' => "Générer une nouvelle clé d'accès à l'API"], '<i class="icon-refresh"></i>')
					: ""
				),
			'visible' => $permModif,
		],
	];
	$this->widget(
		'bootstrap.widgets.BootDetailView',
		[
			'data' => $partenaire,
			'attributes' => $internalAttributes,
			'hideEmptyLines' => true,
		]
	);
	?>
</section>