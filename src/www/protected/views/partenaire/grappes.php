<?php

/** @var Controller $this */
/** @var Grappe[] $grappes */
/** @var Partenaire $partenaire */

$this->pageTitle = "{$partenaire->sigle} Sélection de grappes";

$this->breadcrumbs = [
	"Partenaires" => ['index'],
	$partenaire->nom => ['view', 'id' => $partenaire->id],
	"admin" => ['view-admin', 'id' => $partenaire->id],
	"Sélection de grappes",
];
?>

<h1>
	<?= $partenaire->getLogoImg() ?>
	Sélection de grappes pour
	<em><?= CHtml::encode($partenaire->nom) ?></em>
</h1>

<?php
if ($grappes) {
	$this->renderPartial('/grappe/_liste-publique', ['grappes' => $grappes]);
} else {
	echo "<p>Cette sélection est actuellement vide.</p>";
}
