<?php

/** @var Controller $this */
/** @var \processes\partenaire\Owning $process */
/** @var array $bouquets */

use components\HtmlHelper;

assert($this instanceof Controller);

$this->breadcrumbs = [
	'Partenaires' => ['index'],
	$process->partenaire->nom => ['view', 'id' => $process->partenaire->id],
	"admin" => ['view-admin', 'id' => $process->partenaire->id],
	"Possessions",
];

if (Yii::app()->user->checkAccess("avec-partenaire")) {
	$hasRecords = !empty($process->existing);
	\Yii::app()->getComponent('sidebar')->menu = [
		['label' => 'Retour à ce partenaire', 'url' => ['view-admin', 'id' => $process->partenaire->id]],
		['label' => ''],
		['label' => 'Liste des partenaires', 'url' => ['index']],
		['label' => ''],
		['label' => 'Possessions', 'itemOptions' => ['class' => 'nav-header']],
		['label' => 'Import CSV', 'url' => ['/possession/import', 'partenaireId' => $process->partenaire->id]],
		['label' => 'Export CSV', 'url' => ['/possession/export', 'partenaireId' => $process->partenaire->id], 'visible' => $hasRecords],
		['label' => "Export CSV détaillé", 'url' => ['/possession/export-more', 'partenaireId' => $process->partenaire->id], 'visible' => $hasRecords],
		['label' => "Vérifier les liens", 'url' => ['/verification/liens-revues', 'VerifUrlForm' => ['possessions' => $process->partenaire->id]], 'visible' => $hasRecords],
	];
}

$searchNavs = $process->getSearchNav();
?>

<h1>Possessions du partenaire <em><?= CHtml::encode($process->partenaire->nom) ?></em></h1>


<h2>Nouvelle possession de titre</h2>
<p class="alert alert-info">
	<strong>Attention !</strong>
	Pour enregistrer la possession, il faut valider ce formulaire,
	après avoir complété ses deux champs optionnels.
</p>

<?php
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'possession-form',
		'action' => ['partenaire/possession', 'id' => $process->partenaire->id],
		'enableAjaxValidation' => false,
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'hints' => Hint::model()->getMessages('Possession'),
		'htmlOptions' => ['class' => 'well'],
	]
);
/** @var BootActiveForm $form */

$this->renderPartial(
	'/global/_autocompleteField',
	[
		'model' => $process->possession,
		'attribute' => 'titreId',
		'url' => '/titre/ajax-complete', 'form' => $form,
		'value' => (empty($process->possession->titreId) ? '' : $process->possession->titre->titre),
	]
);

echo $form->textFieldRow($process->possession, 'identifiantLocal');
?>
<div class="control-group">
	<label class="control-label required"><?= $process->possession->getAttributeLabel('bouquet') ?></label>
	<div class="controls">
		<?php
		$params = [
			'name' => "Possession[bouquet]",
			'source' => $bouquets,
			'options' => [
				// min letters typed before we try to complete
				'minLength' => '2',
			],
		];
		$this->widget('zii.widgets.jui.CJuiAutoComplete', $params);
		echo $form->error($process->possession, 'bouquet');
		echo HtmlHelper::getPopoverHint($form->hints['bouquet'] ?? '', $process->partenaire->getAttributeLabel('bouquet'));
		?>
	</div>
</div>
<?php
echo '<div id="overwrite" class="hide">' . $form->checkBoxRow($process->possession, 'overwrite') . '</div>';
echo CHtml::submitButton("Ajouter ce titre", ['class' => 'btn']);
$this->endWidget();
?>

<h2>Possessions déjà déclarées <?= empty($process->existing) ? "" : " (" . count($process->existing) . ")" ?></h2>
<p>
	En cochant des cases, puis en enregistrant les modifications en bas de page,
	les titres sélectionnés seront supprimés de vos possessions.
</p>
<?php
if (empty($process->existing)) {
	echo "<p>Aucun titre.</p>";
} else {
	echo CHtml::form('', 'post', ['class' => 'form-horizontal', 'id' => 'possessions-table'])
		. CHtml::hiddenField('id', (string) $process->partenaire->id); ?>
	<table class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>
					<input type="checkbox" name="checkall" id="possessions-checkall" />
				</th>
				<th>
					<?php echo CHtml::link("Bouquet", ['possession', 'id' => $process->partenaire->id, 'tri' => 'bouquet']); ?>
				</th>
				<th>
					<?php
					echo CHtml::link("Identifiant", ['possession', 'id' => $process->partenaire->id, 'tri' => 'identifiant'])
					. " " . CHtml::link("(num)", ['possession', 'id' => $process->partenaire->id, 'tri' => 'idnum']);
					?>
				</th>
				<th>
					<?php echo CHtml::link("Titre", ['possession', 'id' => $process->partenaire->id, 'tri' => '']); ?>
				</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$pos = 0;
			foreach ($process->existing as $p) {
				/** @var Possession $p */
				echo "<tr id=\"titre{$p->titreId}\">"
					. "<td>" . CHtml::checkBox("existing[t{$p->titreId}]", false, ['uncheckValue' => null]) . "</td>"
					. '<td class="bouquet">' . CHtml::encode($p->bouquet) . "</td>"
					. '<td class="id">'
					. CHtml::link(CHtml::encode($p->identifiantLocal), $process->partenaire->buildPossessionUrl($p->titre, $p->identifiantLocal))
					. "</td>";
				echo '<td>'
					. CHtml::link("revue", ['revue/view', 'id' => $p->titre->revueId, 's' => $searchNavs['revues']->getHash($pos)], ['class' => 'label pull-right'])
					. CHtml::link("titre", ['titre/view', 'id' => $p->titreId, 's' => $searchNavs['titres']->getHash()], ['class' => 'label pull-right', 'style' => 'margin-right: 1ex;'])
					. '<label for="existing_' . $p->titreId . '">'
					. CHtml::encode($p->titre->getFullTitle())
					. "</label>"
					. "</td>";
				echo "</tr>";
				$pos++;
			}
			?>
		</tbody>
	</table>
	<button type="submit" class="btn">Enlever les titres sélectionnés de mes possessions</button>
	<?php
	echo CHtml::endForm();
}

$this->loadViewJs(__FILE__);
?>
