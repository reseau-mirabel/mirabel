<?php

/** @var Controller $this */
/** @var Partenaire $partenaire */
/** @var bool $permModif */

$lockIcon = '<abbr title="informations non publiques"><small class="glyphicon glyphicon-lock"></small></abbr>';
$pencilIcon = '<span class="glyphicon glyphicon-pencil"></span>';
?>
<section id="partenaire-customized">
	<?php
	if ($permModif) {
		echo '<div class="pull-right-icon">' . CHtml::link($pencilIcon, ['personnaliser', 'id' => $partenaire->id], ['title' => "Personnaliser ce partenaire"]) . '</div>';
	}
	?>

	<h2>Personnalisations <?= $lockIcon ?></h2>

	<?php
	$toFullNamesHtml = function (string $persoRecherche) {
		$names = json_decode($persoRecherche, true, 2);
		if (!$names) {
			return "<strong>Aucun</strong>";
		}
		$escapedNames = array_map(
			fn($x) => \Yii::app()->db->quoteValue($x),
			$names
		);
		$escapedStr = join(",", $escapedNames);
		$fullNames = \Yii::app()->db
			->createCommand("SELECT nom FROM Sourcelien WHERE nomcourt IN ($escapedStr) ORDER BY nom")
			->queryColumn();
		return CHtml::encode(join(" ", $fullNames));
	};
	$attributes = [
		'phrasePerso',
		'ipAutorisees',
		'possessionUrl',
		'proxyUrl',
		[
			'label' => "Fusion des accès",
			'value' => ($partenaire->fusionAcces ? "Activée" : "Désactivée"),
		],
		[
			'label' => "Affichage des états de collection du Sudoc",
			'type' => 'raw',
			'value' => $partenaire->getRcrList()
				? "<ul>"
					. "<li><strong>Activé</strong></li>"
					. "<li>RCR : " . join(" ", $partenaire->getRcrList()) . "</li>"
					. "<li>" . $partenaire->getAttributeLabel('rcrPublic') . " : <strong>" . ($partenaire->rcrPublic ? "oui" : "non") . "</strong></li>"
					. "<li>" . $partenaire->getAttributeLabel('rcrHorsPossession') . " : <strong>" . ($partenaire->rcrHorsPossession ? "oui" : "non") . "</strong></li>"
					."</ul>"
				: "Non activé",
			'visible' => $partenaire->editeurId === null,
		],
		[
			'label' => "Personnalisation des liens",
			'type' => 'raw',
			'value' => $partenaire->persoRecherche
				? "<ul>"
					. "<li><strong>Activée</strong></li>"
					. "<li>Liens : " . $toFullNamesHtml($partenaire->persoRecherche) . "</li>"
					."</ul>"
				: "Non activée",
			'visible' => $partenaire->editeurId === null,
		],
	];
	$this->widget(
		'bootstrap.widgets.BootDetailView',
		[
			'data' => $partenaire,
			'attributes' => $attributes,
			'hideEmptyLines' => true,
		]
	);
	?>
	

	<?php
	if (Yii::app()->user->access()->toPartenaire()->customize($partenaire)) {
		$urlPerso1 = Yii::app()->createAbsoluteUrl('/', ['ppp' => $partenaire->id]);
		$urlPerso2 = Yii::app()->createAbsoluteUrl('/revue/view', ['id' => 2513, 'nom' => 'Mondes_emergents', 'ppp' => $partenaire->id]);
		$urlPerso3 = Yii::app()->createAbsoluteUrl('/site/search', ['global' => '"droits de l\'homme"', 'ppp' => $partenaire->id]); ?>
		<h2>Accès personnalisé <?= $lockIcon ?></h2>
		<p>
			Pour un accès personnalisé, avec votre logo et vos abonnements,
			vous pouvez utiliser le sélecteur en pied de page,
			ou le paramètre <code>?ppp=<?= $partenaire->id ?></code> dans une URL.
			Le choix est conservé pour toute la session.
			Par exemple, un lien vers la page d'accueil <a href="<?= CHtml::encode($urlPerso1) ?>"><code><?= CHtml::encode($urlPerso1) ?></code></a>,
			<a href="<?= CHtml::encode($urlPerso2) ?>">une revue</a>
			ou <a href="<?= CHtml::encode($urlPerso3) ?>">une recherche</a>.
		</p>
		<?php
	}
	?>
</section>
