<?php

/** @var Controller $this */
/** @var array $partenaires */
/** @var array $ressources */

assert($this instanceof Controller);

$this->pageTitle = "Partenaires";
$this->breadcrumbs = [
	'Partenaires',
];

if (Yii::app()->user->checkAccess('admin')) {
	/** @var \components\WebSidebar */
	$sidebar = \Yii::app()->getComponent('sidebar');
	$sidebar->menu = [
		['label' => 'Créer', 'url' => ['create']],
		['label' => 'Administration', 'url' => ['admin']],
	];
}

$cs = Yii::app()->clientScript;
$cs->registerScript('BootTabbable',
	<<<EOJS
	if (window.location.hash) {
		$("#partenaires-tabs a[href=\"" + window.location.hash + "\"]").tab("show");
	} else {
		$("#partenaires-tabs a").first().tab("show");
	}
	window.onpopstate = function(event) {
		$("#partenaires-tabs a[href=\"" + document.location.hash + "\"]").tab("show");
	};
	$("#partenaires-tabs a").on("click", function (e) {
		e.preventDefault();
		$(this).tab("show");
		history.pushState({}, '', this.href);
	});
	EOJS
);
?>

<h1>Les partenaires de Mir@bel</h1>

<ul class="nav nav-tabs" id="partenaires-tabs">
	<li class="active"><a href="#partenaires-institutions">Membres</a></li>
	<?php if (!empty($partenaires['editeur'])) { ?>
	<li><a href="#partenaires-editeurs">Éditeurs</a></li>
	<?php } ?>
	<?php if (!empty($ressources)) { ?>
	<li><a href="#partenaires-ressources">Ressources</a></li>
	<?php } ?>
</ul>

<div class="tab-content">
	<div class="tab-pane active" id="partenaires-institutions">
		<h2>Les <?= count($partenaires['normal']) ?> membres du réseau Mir@bel</h2>
		<p><?= CHtml::link("Carte des institutions partenaires", ['/partenaire/carte']); ?></p>
		<?= $this->renderPartial('_list', ['partenaires' => $partenaires['normal']]) ?>
	</div>

	<?php
	if (!empty($partenaires['editeur'])) {
		?>
		<div class="tab-pane" id="partenaires-editeurs">
			<h2>Les <?= count($partenaires['editeur']) ?> éditeurs partenaires de Mir@bel</h2>
			<p><?= CHtml::link("Carte des institutions partenaires", ['/partenaire/carte']); ?></p>
			<?= $this->renderPartial('_list', ['partenaires' => $partenaires['editeur']]); ?>
		</div>
		<?php
	}
	?>

	<?php
	if (!empty($ressources)) {
		?>
		<div class="tab-pane" id="partenaires-ressources">
			<h2>Les <?= count($ressources) ?> ressources partenaires qui sont suivies dans Mir@bel</h2>
			<?= $this->renderPartial('/ressource/_list', ['ressources' => $ressources, 'withDescription' => false]); ?>
		</div>
		<?php
	}
	?>
</div>
