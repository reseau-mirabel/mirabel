<?php

/** @var Controller $this */
/** @var Partenaire[] $partenaires */
assert($this instanceof Controller);

?>
<table class="items table">
	<thead>
		<tr>
			<th>Accès site web</th>
			<th>Nom</th>
			<th>Revues actuellement suivies</th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($partenaires as $partenaire) {
			$img = $partenaire->getLogoImg();
			$description = empty($partenaire->description) ?
				""
				: "<div><em> " . $partenaire->description . "</em></div>";
			?>
		<tr>
			<td><?= (empty($partenaire->url) ? $img : CHtml::link($img, $partenaire->url)) ?></td>
			<td>
				<div><?= $partenaire->getSelfLink() ?>
				</div>
				<div><?= $description ?></div>
			</td>
			<td>
				<?php
				$count = $partenaire->countRevuesSuivies();
				if ($count) {
					echo CHtml::link(
						(string) $count,
						['revue/search', 'SearchTitre[suivi][]' => $partenaire->id]
					);
				} else {
					echo "Aucune";
				}
				?>
			</td>
		</tr>
		<?php
		}
		?>
	</tbody>
</table>
