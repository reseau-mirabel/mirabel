<?php

use processes\partenaire\GrappeAffichage;

/** @var Controller $this */
/** @var GrappeAffichage[] $formData */
/** @var Partenaire $partenaire */
/** @var bool $hasGrappesList */
/** @var bool $hasGrappesPage */

$this->pageTitle = "Affichage des grappes sélectionnées";

$this->breadcrumbs = [
	"Partenaires" => ['index'],
	$partenaire->nom => ['view', 'id' => $partenaire->id],
	"admin" => ['view-admin', 'id' => $partenaire->id],
	$this->pageTitle,
];

$this->renderPartial(
	'_grappes-menu',
	['partenaire' => $partenaire, 'hasGrappesList' => $hasGrappesList, 'hasGrappesPage' => $hasGrappesPage]
);

Yii::app()->clientScript->registerCoreScript('jquery.ui'); // sortable items
?>
<h1><?= $this->pageTitle ?></h1>
<p class="alert alert-info">Glissez-déposez une ligne pour modifier l'ordre des grappes.</p>

<?php
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'enableAjaxValidation' => false,
		'type' => BootActiveForm::TYPE_HORIZONTAL,
	]
);
/** @var BootActiveForm $form */
?>

<table class="table" id="grappes-affichage">
	<thead>
		<tr>
			<th style="display:flex; flex-direction:row; justify-content:space-between;">
				<span>Grappes</span>
				<span>
					<?= CHtml::link(
						'<span class="glyphicon glyphicon-grain" style="font-size:18px; vertical-align:middle; margin-right:4px"></span>',
						['grappes-selection', 'id' => $partenaire->id],
						['title' => "Choisir d'autres grappes"]
					) ?>
					<?= CHtml::link(
						'<img src="/images/plus.png" alt="Choisir d’autres grappes">',
						['/grappe/create'],
						['title' => "Créer une grappe"]
					) ?>
				</span>
			</th>
			<th style="vertical-align:top;">Visible par…</th>
			<th>Sur ma page de partenaire</th>
			<th>Sur ma page de grappes</th>
			<th></th>
		</tr>
	<tbody id="sortable">
		<?php
		foreach ($formData as $row) {
			echo $form->errorSummary($formData);
			echo "<tr>";

			echo "<td>"
				. '<span class="glyphicon glyphicon-menu-hamburger"></span> '
				. CHtml::encode($row->grappeName)
				. "</td>";

			echo '<td class="grappe-diffusion">';
			if ($row->grappePartenaireId === (int) $partenaire->id) {
				// Propriétaire de la grappe
				if ($row->diffusion === Grappe::DIFFUSION_PARTAGE) {
					echo "Partagée";
				} else {
					$enumDiffusion = Grappe::ENUM_DIFFUSION;
					unset($enumDiffusion[Grappe::DIFFUSION_PARTAGE]);
					echo $form->dropDownList($row, "[{$row->grappeId}]diffusion", $enumDiffusion);
				}
			} else {
				echo "Partagée par<br>" . $row->getPartenaireCourt();
				if (!$row->pp && !$row->ps) {
					echo <<<HTML
					 <span class="glyphicon glyphicon-warning-sign"
						title="Cette grappe n'est pas visible tant que vous ne sélectionnez pas au moins un affichage (sur votre page de partenaire ou de grappes). L'auteur de la grappe peut retirer le statut “partagé” d'une grappe inutilisée.">
					</span>
					HTML;
				}
			}
			echo "</td>";

			$public = in_array($row->diffusion, [Grappe::DIFFUSION_PARTAGE, Grappe::DIFFUSION_PUBLIC], true);
			echo '<td style="text-align:center">'
				. $form->checkBox($row, "[{$row->grappeId}]pp", ['disabled' => !$public])
				. "</td>";
			echo '<td style="text-align:center">'
				. $form->checkBox($row, "[{$row->grappeId}]ps", ['disabled' => !$public])
				. "</td>";

			// Actions par icônes
			echo "<td>";
			if ($row->grappePartenaireId === (int) $partenaire->id) {
				echo '<div style="display:flex; justify-content:space-around;">';
				echo CHtml::link(
					'<span class="glyphicon glyphicon-eye-open" title="Afficher cette grappe"></span>',
					['/grappe/view', 'id' => $row->grappeId]
				);
				echo CHtml::link(
					'<span class="glyphicon glyphicon-pencil" title="Modifier cette grappe"></span>',
					['/grappe/admin-view', 'id' => $row->grappeId]
				);
				echo '</div>';
			} else {
				echo '<div style="white-space:pre;">';
				echo CHtml::link(
					'<span class="glyphicon glyphicon-eye-open" title="Afficher cette grappe"></span>',
					['/grappe/view', 'id' => $row->grappeId]
				);
				echo " ";
				echo CHtml::link(
					'<span class="glyphicon glyphicon-pushpin" style="color:red"></span>',
					'#',
					[
						'confirm' => "Retirer cette grappe de ma sélection ?",
						'title' => "Retirer cette grappe de ma sélection",
						'submit' => ['grappes-remove', 'id' => $partenaire->id], // Will use JS to switch from GET to POST
						'params' => ['grappeId' => $row->grappeId],
					]
				);
				echo '</div>';
			}
			echo "</td>";

			echo "</tr>";
		}
		?>
	</tbody>
</table>

<div class="form-actions">
	<button type="submit" class="btn btn-primary">Enregistrer les modifications</button>
</div>

<?php
$this->endWidget();

Yii::app()->clientScript->registerScript('sortable', <<<JS
	$("#sortable").sortable({
		items: "tr",
		axis: "y",
	});
	JS
);
