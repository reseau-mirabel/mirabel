<?php

/** @var Controller $this */
/** @var Partenaire $partenaire */
/** @var bool $permView */
/** @var bool $permModif */

?>
<section>
	<?php
	if ($permModif) {
		$pencilIcon = '<span class="glyphicon glyphicon-pencil"></span>';
		echo '<div class="pull-right-icon">' . CHtml::link($pencilIcon, ['update', 'id' => $partenaire->id]) . '</div>';
	}
	if ($permView) {
		echo "<h2>Présentation publique</h2>";
	}
	?>

	<div class="presentation">
		<?= $partenaire->presentation ?>
	</div>

	<?php
	$publicAttributes = [
		'nom',
		[
			'name' => 'sigle',
			'visible' => Yii::app()->user->checkAccess("avec-partenaire"),
		],
		'description',
		'url:url',
	];
	if (Yii::app()->user->checkAccess("avec-partenaire")) {
		array_push(
			$publicAttributes,
			[
				'label' => "Géolocalisation",
				'value' => trim(
					$partenaire->geo
						. ($partenaire->latitude && $partenaire->longitude ? " ({$partenaire->latitude}, {$partenaire->longitude})" : "")
						. ($partenaire->paysId ? ", " . $partenaire->pays->nom : ""),
					", "
				),
			],
		);
		Yii::app()->clientScript->registerScript(
			'partenaire-hash-refresh',
			<<<EOJS
				$("#partenaire-hash-refresh").on('click', function() {
					var hash = document.getElementById('partenaire-hash').innerText;
					if (hash === '' || confirm("Attention, l'ancienne clé ne sera plus valable. Tous les accès à l'API devront utiliser la nouvelle clé. Confirmez-vous ?")) {
						$('#partenaire-hash').load('/partenaire/ajax-refresh-hash?id={$partenaire->id}');
					}
				});
				EOJS
		);
	} else {
		array_push(
			$publicAttributes,
			[
				'label' => "Localisation",
				'value' => trim(
					$partenaire->geo . ($partenaire->paysId ? ", " . $partenaire->pays->nom : ""),
					", "
				),
			]
		);
	}
	$this->widget(
		'bootstrap.widgets.BootDetailView',
		[
			'data' => $partenaire,
			'attributes' => $publicAttributes,
			'hideEmptyLines' => true,
		]
	);
	?>
</section>