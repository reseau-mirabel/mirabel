<?php

/** @var Controller $this */
/** @var Partenaire $model */

assert($this instanceof Controller);

$this->breadcrumbs = [
	"Admin" => '/admin',
	'Partenaires',
];

\Yii::app()->getComponent('sidebar')->menu = [
	['label' => 'Liste', 'url' => ['index']],
	['label' => 'Créer', 'url' => ['create']],
];
?>

<h1>Administration des partenaires</h1>

<p>
	Vous pouvez saisir un opérateur de comparaison
	(<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
	ou <b>=</b>) au début de chaque zone de recherche afin de désigner le mode de comparaison.
</p>

<form action="">
	<button type="submit">Filtrer</button>
	<?php
	$columns = [
		'nom',
		[
			'name' => 'type',
			'filter' => Partenaire::$enumType,
		],
		'description',
		[
			'name' => 'statut',
			'filter' => Partenaire::$enumStatut,
		],
		'hdateModif:datetime',
		[
			'class' => 'BootButtonColumn',
			'header' => '',
		],
	];
	$this->widget(
		'ext.bootstrap.widgets.BootGridView',
		[
			'id' => 'partenaire-grid',
			'dataProvider' => $model->search(),
			'filter' => $model, // null to disable
			'columns' => $columns,
			'ajaxUpdate' => false,
		]
	);
	?>
</form>
