<?php

/** @var Controller $this */
/** @var Partenaire $partenaire */
/** @var Cms[] $actus */

?>
<section>
	<h2>Actualités dans Mir@bel</h2>
	<ul class="breves">
		<?php
		$limit = \controllers\partenaire\ViewAction::MAX_ACTUS;
		foreach ($actus as $block) {
			$limit--;
			if ($limit < 0) {
				echo '<li id="actu' . $block->id . '">'
					. CHtml::link(
						"→ Toute l'actualité de <em>" . CHtml::encode($partenaire->nom) . "</em> dans Mir@bel",
						['/site/actualite', 'q' => ['partenaireId' => $partenaire->id]]
					)
					. "</li>\n";
				break;
			}
			echo '<li id="actu' . $block->id . '">' . $block->toHtml() . "</li>\n";
		}
		?>
	</ul>
</section>
