<?php

/** @var Controller $this */
/** @var Partenaire $model */
/** @var string $logoImg */
/** @var AbonnementInfo $abonnementInfo */
/** @var array $uploadedFiles */
/** @var Cms[] $actus */
/** @var int $numGrappes */
/** @var int $numGrappesPagePart */
/** @var int $numGrappesPageSel */

$this->pageTitle = "Partenaire " . $model->nom;
$this->pageDescription = "Accès en ligne aux contenus des revues pour le partenaire {$model->nom}";

$this->breadcrumbs = [
	'Partenaires' => ['index'],
	$model->nom => ['view', 'id' => $model->id],
	"admin"
];

$lockIcon = '<abbr title="informations non publiques"><small class="glyphicon glyphicon-lock"></small></abbr>';
$searchIcon = '<small><span class="glyphicon glyphicon-search"></span></small>';

$permAdminGlobal = Yii::app()->user->checkAccess('admin');
$permAdminLocal = Yii::app()->user->access()->toPartenaire()->admin();
$permModif = Yii::app()->user->access()->toPartenaire()->update($model);
$permView = Yii::app()->user->access()->toPartenaire()->view($model);

if ($permModif) {
	if (!$model->email) {
		?>
		<div class="alert alert-error">
			<strong>Attention : adresse de contact manquante. </strong>
			L'envoi des alertes de propositions de modifications ne pourra
			pas se faire pour ce partenaire tant que son adresse de contact ne sera pas remplie.
		</div>
		<?php
	}
	if (!$logoImg) {
		?>
		<p class="alert alert-warning">
			Ce partenaire n'a pas de logo. Vous pouvez déposer une image en passant par le menu latéral.
		</p>
		<?php
	}
}
?>

<div class="pull-right">
	<?= CHtml::link("Vue publique", ['view', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
</div>

<h1>
	<?= $model->getLogoImg() ?>
	<em><?= CHtml::encode($model->nom) ?></em>
</h1>

<?php
if ($model->editeurId > 0) {
	echo "<p>Ce compte de partenaire est associé dans Mir@bel à ",
		CHtml::link(
			"l'éditeur <strong>" . CHtml::encode($model->editeur->nom) . "</strong>",
			$model->editeur->getSelfUrl(),
			['class' => 'btn btn-light']
		),
		"</p>";
}

// Détails
$this->renderPartial('_view_section-presentation', ['partenaire' => $model, 'permView' => $permView, 'permModif' => $permModif]);

if ($permView) {
	// Détails
	$this->renderPartial('_view_section-details', ['partenaire' => $model, 'permModif' => $permModif]);

	// Données
	$this->renderPartial('_view_section-donnees', ['partenaire' => $model, 'permModif' => $permModif]);
}

// Utilisateurs
if (Yii::app()->user->checkAccess("avec-partenaire")) {
	$this->renderPartial(
		'_view_section-utilisateurs',
		['partenaire' => $model, 'permAdminGlobal' => $permAdminGlobal, 'permAdminLocal' => $permAdminLocal]
	);
}

// Suivi
$this->renderPartial('_view_section-suivi', ['partenaire' => $model]);

// Grappes
$canCreateGrappe = (new \processes\grappe\Permissions(Yii::app()->user))->canCreate();
if ($canCreateGrappe) {
	?>
	<section id="grappes-selection">
	<h2 style="margin-bottom:0;">Grappes – <?= CHtml::encode($model->getShortName()) ?></h2>
	<ul class="unstyled" style="margin: 1ex 0 1em">
		<li>
			<img src="/images/plus.png" alt="Nouvelle grappe" style="vertical-align: text-bottom; margin-right:4px">
			<a href="/grappe/create">Créer une nouvelle grappe</a>
		</li>
		<?php if ($numGrappes) { ?>
		<li>
			Administrer <?= CHtml::link("mes $numGrappes grappes", ['/grappe/partenaire', 'id' => $model->id]) ?>
		</li>
		<?php } ?>
		<li>
			Gérer les <?= CHtml::link("paramètres d'affichage des grappes", ['/partenaire/grappes-affichage', 'id' => $model->id]) ?>
		</li>
		<li>
			<span class="glyphicon glyphicon-grain" style="font-size:18px; vertical-align:text-bottom; margin-right:4px"></span>
			<?= CHtml::link("Sélectionner des grappes d'autres partenaires", ['/partenaire/grappes-selection', 'id' => $model->id]) ?>
			à afficher sur les pages <em><?= CHtml::encode($model->nom) ?></em>
		</li>
	</ul>
	Actuellement :
	<ul class="unstyled" style="margin: 1ex 0 1em">
		<li>
			<small><span class="glyphicon glyphicon-search" style="margin-right:4px"></span></small>
			<?php
			if ($numGrappesPagePart > 0) {
				echo "$numGrappesPagePart grappes visibles sur "
					. CHtml::link("ma page publique partenaire", ['/partenaire/view', 'id' => $model->id])
					. ".";
			} else {
				echo "Aucun affichage de grappe sur ma page partenaire.";
			}
			?>
		</li>
		<li>
			<small><span class="glyphicon glyphicon-search" style="margin-right:4px"></span></small>
			<?php
			if ($numGrappesPageSel > 0) {
				echo "$numGrappesPageSel grappes visibles sur "
					. CHtml::link("ma page de grappes", ['/partenaire/grappes', 'id' => $model->id])
					. ".";
			} else {
				echo "Pas d'activation de page publique listant des grappes pour mon partenaire.";
			}
			?>
		</li>
	</ul>
	</section>
	<?php
}

if ($model->editeurId) {
	// Convention de partenariat
	if ($permModif) {
		$this->renderPartial('_view_section-lettre-editeur', ['editeurId' => $model->editeurId]);
	}
} else {
	// Possessions
	if (Yii::app()->user->checkAccess("avec-partenaire") && $model->canOwnTitles()) {
		$this->renderPartial('_view_section-possessions', ['partenaire' => $model]);
	}

	// Abonnements
	if (Yii::app()->user->checkAccess("avec-partenaire")) {
		$this->renderPartial('_view_section-abonnements', ['abonnementInfo' => $abonnementInfo, 'partenaire' => $model]);
	}

	// Personnalisation des recherches
	if ($permModif) {
		$this->renderPartial('_view_section-personnalisations', ['partenaire' => $model, 'permModif' => $permModif]);
	}

	// Convention de partenariat
	if ($permModif) {
		$this->renderPartial('_view_section-convention', ['uploadedFiles' => $uploadedFiles]);
	}
}

// Actualités
if ($permAdminGlobal || (int) Yii::app()->user->partenaireId === (int) $model->id) {
	?>
<section>
	<h2>Actualités dans Mir@bel</h2>
	<p>
		<i class="icon icon-search"></i>
		Vos <?= CHtml::link("actualités dans Mir@bel", ['/site/actualite', 'q' => ['partenaireId' => $model->id]]) ?>
	</p>
	<p>
		<span class="glyphicon glyphicon-envelope"></span>
		<a href="mailto:<?= CHtml::encode(Config::read('contact.visible.email')) ?>">Écrivez nous</a> pour signaler de nouvelles actualités à mettre en valeur
	</p>
</section>
	<?php
}
