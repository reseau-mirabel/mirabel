<?php

/** @var PartenaireController $this */
/** @var array $partenaires */
/** @var array $mapConfig */
/** @var string $stamen */

assert($this instanceof Controller);

$this->pageTitle = "Carte des membres de Mir@bel";

$this->breadcrumbs = [
	'Partenaires' => ['index'],
	"Carte",
];
?>
<h1>Localisation des membres et partenaires éditeurs du réseau Mir@bel</h1>

<?= (new \widgets\WorldMap($mapConfig, $stamen))->run() ?>

<div id="map-legend">
	<?php
	$miUrl = Yii::app()->getAssetManager()->publish(VENDOR_PATH . '/npm-asset/leaflet/dist') . '/images/marker-icon.png';
	?>
	<ul>
		<li><?= CHtml::image('/images/marker-mirabel.png', "goutte orange", ['height' => 41, 'width' => 25]) ?>
			<?= CHtml::link(count($partenaires['normal']) . ' institutions membres', ['/partenaire/index', '#' => 'partenaires-institutions']) ?>
		</li>
		<li><?= CHtml::image($miUrl, "goutte bleue", ['height' => 41, 'width' => 25]) ?>
			<?= CHtml::link(count($partenaires['editeur']) . ' éditeurs partenaires', ['/partenaire/index', '#' => 'partenaires-editeurs']) ?>
		</li>
	</ul>
	<p>
		Le zoom permet de visualiser précisément la localisation de chaque partenaire.
		Cliquer sur un partenaire permet d'afficher son nom et de rebonbir sur sa page dans Mir@bel.
	</p>
</div>
