<?php

/** @var Controller $this */
/** @var ?Partenaire $institute Personnalisation */

$user = Yii::app()->user;

Yii::app()->getClientScript()->registerCssFile('/css/glyphicons.css');

$menu = [];

if ($institute) {
	$img = $institute->getLogoImg(true, ['height' => 40]);
	$itemOptions = ['title' => "Vous consultez Mir@bel avec la personnalisation {$institute->nom}"];
	if ($img) {
		$itemOptions['class'] = 'etablissement';
	}
	$menu[] = [
		'label' => ($img ?: "[" . ($institute->sigle ?: $institute->nom) . "]"),
		'encodeLabel' => !$img,
		'url' => ['/partenaire/view', 'id' => $institute->id],
		'itemOptions' => $itemOptions,
	];
	unset($img);
	unset($itemOptions);
}

$lockImg = '<i class="glyphicon glyphicon-lock"></i>';
array_push(
	$menu,
	[
		'label' => 'Le réseau',
		'items' => [
			['label' => "Actualités", 'url' => ['/site/actualite']],
			['label' => 'Partenaires', 'url' => ['/partenaire']],
			['label' => 'Présentation', 'url' => ['/site/page', 'p' => 'presentation']],
		],
	],
	[
		'label' => 'Soutien',
		'url' => ['/site/page', 'p' => 'soutenir'],
	],
	[
		'label' => "{$lockImg}Infos",
		'encodeLabel' => false,
		'visible' => $user->checkAccess("avec-partenaire"),
		'items' => [
			['label' => 'Aide professionnelle et documentation', 'url' => ['/site/page', 'p' => 'aide']],
			['label' => "Indicateurs de contenu", 'url' => ['/stats']],
			['label' => "Indicateurs de suivi", 'url' => ['/stats/suivi'], 'visible' => $user->checkAccess('stats/suivi')],
			['label' => 'Liste des personnes membres', 'url' => ['/site/page', 'p' => 'partenaires']],
			['label' => 'Listes de diffusion et adresses', 'url' => ['/site/page', 'p' => 'diffusion']],
			['label' => 'Outils de communication', 'url' => ['/site/page', 'p' => 'communication']],
			['label' => 'Pilotage du réseau', 'url' => ['/site/page', 'p' => 'pilotage']],
			['label' => "Statistiques de fréquentation", 'url' => ['/statsacces/index']],
		],
	],
	[
		'label' => "{$lockImg}Admin",
		'encodeLabel' => false,
		'visible' => $user->checkAccess('menu-veilleur'),
		'items' => [
			['label' => "Administration", 'url' => ['/admin/index'], 'visible' => $user->checkAccess('admin')],
			['label' => "Analyse", 'url' => ['/analyse']],
			['label' => "Bulles d'aide", 'url' => ['/hint/admin'], 'visible' => $user->checkAccess('redaction/aide')],
			['label' => "Dépôt de fichiers", 'url' => ['/upload'], 'visible' => $user->checkAccess('redaction/editorial') || $user->access()->toPartenaire()->upload()],
			['label' => "Documentation technique", 'url' => ['/doc']],
			['label' => "Fonctions éditoriales", 'url' => ['/cms'], 'visible' => $user->checkAccess('redaction')],
			[
				'label' => 'Imports', 'url' => ['/import/kbart'],
				'visible' => $user->checkAccess('import'),
			],
			['label' => 'Interventions', 'url' => ['/intervention/admin']],
			['label' => "Politiques", 'url' => ['/admin/politique'], 'visible' => $user->checkAccess('politiques')],
			['label' => "Vérification des données", 'url' => ['/verification/index']],
		],
	],
	[
		'label' => 'Listes',
		'items' => [
			['label' => 'Revues', 'url' => ['/revue/index']],
			['label' => 'Grappes de revues', 'url' => ['/grappe']],
			['label' => 'Ressources', 'url' => ['/ressource/index']],
			['label' => 'Éditeurs', 'url' => ['/editeur/index']],
			['label' => "Thématique", 'url' => ['/categorie']],
		],
	],
);

$rightMenu = [];
if ($user->access()->toPolitique()->validate()) {
	$alertesPolitiques = new \processes\politique\AlertesSuivi();
	if ($alertesPolitiques->demandesPolitiques || $alertesPolitiques->demandesUtilisateurs) {
		$rightMenu[] = [
			'label' => '<span>'
				. CHtml::link(
					(string) $alertesPolitiques->demandesUtilisateurs,
					['/admin/politique-validation'],
					['title' => "Utilisateurs-politiques demandant le rattachement à des éditeurs", 'class' => "badge badge-" . ($alertesPolitiques->demandesUtilisateurs ? "important" : "success")]
				)
				. " / "
				. CHtml::link(
					(string) $alertesPolitiques->demandesPolitiques,
					['/politique/moderation'],
					['title' => "Politiques à publier en attente de modération", 'class' => "badge badge-" . ($alertesPolitiques->demandesPolitiques ? "important" : "success")]
					)
				. '</span>',
			'encodeLabel' => false,
			'header' => true,
			'itemOptions' => ['class' => 'navbar-text'],
		];
	}
}
if ($user->checkAccess('avec-partenaire')) {
	$partenaireId = $user->getState('partenaireId', 0);
	// Tableau de bord et ses alertes
	array_push(
		$rightMenu,
		[
			'label' => '<span class="glyphicon glyphicon-list"></span>'
				. (\processes\intervention\HtmlHelper::hasPendingInterventions($partenaireId) ? '<span class="glyphicon glyphicon-alert overlay"></span>' : ''),
			'encodeLabel' => false,
			'url' => ['/partenaire/tableau-de-bord', 'id' => $user->partenaireId],
			'linkOptions' => ['title' => "Tableau de bord de suivi pour la veille"],
		],
	);

	// Incohérences APC
	if ($user->checkAccess('admin') && processes\attribut\Apc::countErrors() > 0) {
		array_push(
			$rightMenu,
			[
				'label' => 'APC<span class="glyphicon glyphicon-alert overlay"></span>',
				'encodeLabel' => false,
				'url' => ['/verification/attribut-apc'],
				'itemOptions' => ['title' => "Incohérences dans les déclarations d'APC"],
			],
		);
	}
}
if ($user->isGuest) {
	array_push(
		$rightMenu,
		[
			'label' => '<span class="glyphicon glyphicon-user"></span>',
			'encodeLabel' => false,
			'linkOptions' => ['title' => "Connexion à mon compte personnel"],
			'url' => '/site/login',
		]
	);
} else {
	$userHasPolicies = (bool) Yii::app()->db
		->createCommand("SELECT 1 FROM Utilisateur_Editeur ue WHERE ue.utilisateurId = :id")
		->queryScalar([':id' => $user->id]);
	array_push(
		$rightMenu,
		[
			'label' => '<span class="glyphicon glyphicon-tags"></span>',
			'encodeLabel' => false,
			'url' => ['/politique'],
			'linkOptions' => ['title' => "Mes politiques : déclarer des politiques d'éditeur sur des revues"],
			'visible' => !$user->getState('partenaireId') || $userHasPolicies,
		],
		[
			'label' => '<span class="glyphicon glyphicon-user"></span>  ' . CHtml::encode($user->getState('nomComplet')),
			'encodeLabel' => false,
			'items' => [
				['label' => "Mon profil", 'url' => ['/utilisateur/view', 'id' => $user->id], 'itemOptions' => ['class' => 'profile']],
				empty($user->partenaireId) ? null : [
					'label' => $this->partenaire->nom,
					'url' => ['/partenaire/view-admin', 'id' => $user->partenaireId],
				],
				['label' => "Déconnexion", 'url' => '/site/logout'],
			],
		]
	);
}

$appName = Yii::app()->name . ' <em>' . Yii::app()->params->itemAt('version') . '</em>';
$navItems = [
	[
		'class' => 'bootstrap.widgets.BootMenu',
		'items' => $menu,
	],
];
if ($this->route !== 'site/index') {
	$navItems[] = $this->getSearchForm();
	$navItems[] = [
		'class' => 'bootstrap.widgets.BootMenu',
		'items' => [
			[
				'label' => 'Recherche<br />avancée',
				'url' => ['/revue/search'],
				'encodeLabel' => false,
				'itemOptions' => ['class' => 'double-line'],
			],
		],
	];
}
$navItems[] = '<div class="nav languages">'
	. CHtml::link("EN", ['/site/page', 'p' => 'english'], ['hreflang' => 'en', 'title' => "presentation in english"])
	. CHtml::link("ES", ['/site/page', 'p' => 'espanol'], ['hreflang' => 'es', 'title' => "presentación en español"])
	. '</div>';
$navItems[] = [
	'class' => 'bootstrap.widgets.BootMenu',
	'items' => [
		[
			'label' => '?', 'url' => ['/site/page', 'p' => 'aide'],
			'itemOptions' => ['title' => 'Aide', 'id' => 'navlink-help'],
		],
	],
];
$navItems[] = [
	'class' => 'bootstrap.widgets.BootMenu',
	'htmlOptions' => ['class' => 'pull-right'],
	'items' => $rightMenu,
];
$this->widget(
	'bootstrap.widgets.BootNavbar',
	[
		'fixed' => false,
		'fluid' => true,
		'collapse' => true, // requires bootstrap-responsive.css
		'brand' => (file_exists(Yii::app()->basePath . '/../images/logo-mirabel.png') ?
			CHtml::image(Yii::app()->baseUrl . '/images/logo-mirabel.png', strip_tags($appName), ['id' => 'logo', 'title' => "Le site qui facilite l'accès aux revues"])
			: $appName),
		'items' => $navItems,
	]
);
unset($menu, $rightMenu);
