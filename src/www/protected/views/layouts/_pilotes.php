<?php

/** @var Controller $this */
/** @var string $pilotes */

if (!$pilotes) {
	return;
}
?>
<div>
	Mir@bel est piloté par
	<?php
	$pilotesList = array_filter(array_map('trim', explode("\n", $pilotes)));
	foreach ($pilotesList as $rank => $line) {
		$parts = explode(" => ", $line);
		if (count($parts) > 2) {
			echo CHtml::link(CHtml::encode($parts[1]), ['/partenaire/view', 'id' => (int) $parts[0]], ['title' => $parts[2]]);
		} elseif (count($parts) === 2) {
			echo CHtml::link(CHtml::encode($parts[1]), ['/partenaire/view', 'id' => (int) $parts[0]]);
		} else {
			echo CHtml::tag('span', [], CHtml::encode($line));
		}
		if ($rank < count($pilotesList) - 2) {
			echo ", ";
		} elseif ($rank == count($pilotesList) - 2) {
			echo " et ";
		} else {
			echo ".";
		}
	}
	?>
</div>
