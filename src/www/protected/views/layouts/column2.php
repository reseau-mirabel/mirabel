<?php

/** @var Controller $this */
/** @var string $content */

assert($this instanceof Controller);

/** @var \components\WebSidebar */
$sidebar = \Yii::app()->getComponent('sidebar');
$hasSidebar = !$sidebar->isEmpty();

$this->beginContent('//layouts/main');
?>
<div class="row-fluid">
	<div class="span<?= $hasSidebar ? 10 : 12 ?>">
		<div id="content">
			<?= $content ?>
		</div>
	</div>
	<?php if ($hasSidebar) { ?>
	<div class="span2" id="sidebar"><?php $sidebar->render() ?></div>
	<?php } ?>
</div>
<?php $this->endContent(); ?>
