<?php

/** @var Controller $this */
/** @var string $content */

use components\SqlHelper;

$user = Yii::app()->getUser();
if (strncmp($this->pageTitle, Yii::app()->name, 5)) {
	$this->pageTitle = Yii::app()->name . ' - ' . $this->pageTitle;
}

$institute = Yii::app()->user->getInstitute();

new \widgets\DefaultAssets();
?><!doctype html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<title><?= CHtml::encode($this->pageTitle) ?></title>
	<?php
	if ($this->getCanonicalUrl()) {
		echo CHtml::tag('link', ['rel' => 'canonical', 'href' => $this->getCanonicalUrl()]) . "\n";
	}
	?>
	<link rel="shortcut icon" href="<?= Yii::app()->getBaseUrl() ?>/images/favicon.ico" type="image/x-icon" />
	<link rel="alternate" type="application/atom+xml" href="<?= CHtml::encode($this->createAbsoluteUrl('/rss/breves')) ?>" title="Actualités du réseau Mir@bel" />
	<?= Controller::linkEmbeddedFeed('rss2') ?>
	<?= Controller::linkEmbeddedFeed('atom') ?>
	<?= $this->getHtmlHead() ?>
</head>
<body data-instance="<?= CHtml::encode(Norm::urlParam(Yii::app()->name)) ?>">
	<header id="header">
		<div id="navbar-top-separator"></div>
		<?php
		$isMediaPrint = isset($_GET['media']) && $_GET['media'] === 'print';
		if (!$isMediaPrint) {
			$this->renderPartial('//layouts/_mainMenu', ['institute' => $institute]);
		}
		?>
		<div class="navbar-bottom-separator"></div>
	</header>

<?php
if ($this->backgroundL) {
	echo '<div class="feuilles-g">';
}
if ($this->backgroundR) {
	echo '<div class="feuilles-d">';
}

if ($institute && !empty($institute->phrasePerso)) {
	echo CHtml::tag(
		'div',
		['id' => "institute-warning"],
		CHtml::encode(str_replace('%NOM%', $institute->nom, $institute->phrasePerso))
	);
}
?>
<div class="<?= $this->containerClass ?>" id="page">
	<?php
	// Breadcrumbs
	if (!empty($this->breadcrumbs)) {
		$this->widget(
			'bootstrap.widgets.BootBreadcrumbs',
			[
				'links' => $this->breadcrumbs,
			]
		);
	}

	// Flash messages: 'success', 'info', 'warning', 'error', 'danger'
	$this->widget('bootstrap.widgets.BootAlert');

	// The page content
	echo $content;
	?>
</div>
<?php
if ($this->backgroundR) {
	echo '</div>';
}
if ($this->backgroundL) {
	echo '</div>';
}
?>

<footer>
	<div class="slogan">
		Mir@bel, le site qui facilite l'accès aux revues
		<?php
		if ($this->id === 'site' && $this->action->id === 'index') {
			// page d'accueil
			echo "— ISSN 2678-0038";
		}
		?>
	</div>
	<div id="footer-message">
		<div class="align-left">
			<div class="links">
				<a href="<?= $this->createUrl('/site/contact') ?>" title="Pour nous contacter">Contact</a>
				<a href="<?= $this->createUrl('/site/page/lettre') ?>" title="Lettre d'information sur Mir@bel : archives et abonnement">Lettre d’information</a>
				<a href="<?= $this->createUrl('/site/page/mentions_legales') ?>" title="Mentions légales et données personnelles">Mentions légales</a>
				<a href="<?= $this->createUrl('/site/page/conditions-utilisation') ?>" title="Conditions d'utilisation de ce site web">Conditions d'utilisation</a>
			</div>
		</div>
		<div class="footer-main">
			<div class="block">
				<?= $this->renderPartial('//layouts/_pilotes', ['pilotes' => Config::read('partenaires.pilotes')]); ?>
			</div>
			<div class="footer-logo">
				<?php
				if ($institute) {
					$logo = $institute->getLogoImg();
					echo CHtml::link(
						($logo ?: CHtml::encode($institute->nom)),
						['/partenaire/view', 'id' => $institute->id],
						['title' => "Vous êtes un utilisateur de " . $institute->nom]
					);
				} else {
					echo CHtml::link(
						"Personnalisation",
						['/site/page', 'p' => 'personnalisation'],
						['title' => "Vous consultez la vue par défaut de Mir@bel ; sélectionnez un établissement pour bénéficier d'informations personnalisées"],
					) . ": ";
				}
				?>
				<form id="form-footer-institute" action="#" style="display:none;">
					<div>
					<?php
					echo CHtml::dropDownList(
						'sel-institute',
						empty($institute) ? '' : (string) $institute->id,
						['0' => "[partenaire]"] + SqlHelper::sqlToPairs(
							"SELECT id, IF(sigle <> '', sigle, nom) AS name FROM Partenaire WHERE type='normal' AND statut='actif' ORDER BY name"
						),
						[
							'title' => "Sélectionnez votre établissement pour personnaliser votre navigation sur le site Mir@bel",
						]
					);
					?>
					</div>
				</form>
			</div>
		</div>
		<div class="align-right">
		</div>
	</div>
	<div id="footer-sep"></div>
	<?php
	if ((YII_DEBUG || $user->checkAccess("avec-partenaire")) && Yii::app()->params->itemAt('vcsVersion')) {
		echo '<div class="debug-info">'
			. "<em>version" . (YII_DEBUG ? " de développement" : "") . "</em> "
			. Yii::app()->params->itemAt('vcsVersion')
			. "</div>";
		if (Yii::app()->params->itemAt('dataVersion')) {
			echo '<div class="debug-info">Données synchronisées le ' . Yii::app()->params->itemAt('dataVersion') . '</div>';
		}
	}
	?>
	<?= $this->renderPartial('//layouts/_matomo', ['matomo' => Yii::app()->getComponent('matomo')]) ?>
</footer>

</body>
</html>
