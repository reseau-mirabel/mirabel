<?php

/** @var Controller $this */
/** @var components\Matomo $matomo */

assert($this instanceof Controller);

if (!$matomo->isActive()) {
	return;
}

$user = Yii::app()->getUser();
?>
<script>
	var _paq = window._paq || [];
	<?php
	// tracker methods like "setCustomDimension" should be called before "trackPageView"

	if (Yii::app()->user->getState('instituteByIp')) {
		printf(
			"_paq.push(['setCustomVariable', 1, 'Établissement /IP', %s, 'visit']);",
			json_encode(Yii::app()->user->getState('instituteShortname'))
		);
	}
	if (Yii::app()->user->getState('instituteId') > 0) {
		printf(
			"_paq.push(['setCustomVariable', 2, 'Établissement', %s, 'page']);",
			json_encode(Yii::app()->user->getState('instituteShortname'))
		);
	}
	if (!$user->isGuest) {
		printf("_paq.push(['setUserId', %s]);", json_encode($user->id));
		$userPartenaire = Partenaire::model()
			->findBySql("SELECT p.* FROM Partenaire p JOIN Utilisateur u ON p.id = u.partenaireId WHERE u.id = " . (int) $user->id);
		if ($userPartenaire) {
			printf(
				"_paq.push(['setCustomVariable', 3, 'Partenaire authentifié', %s, 'page']);",
				json_encode($userPartenaire->getShortName())
			);
		}
	} elseif ($this->action->id === 'logout') {
		echo "_paq.push(['resetUserId']);";
	}
	?>
	_paq.push(['trackPageView']);
	_paq.push(['enableLinkTracking']);
	<?= $matomo->getJsCommand() ?>
</script>
<?= $matomo->getNoScript() ?>
