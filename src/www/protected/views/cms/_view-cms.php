<?php

/** @var Controller $this */
/** @var Cms $model */

$this->widget(
	'bootstrap.widgets.BootDetailView',
	[
		'data' => $model,
		'attributes' => [
			'id',
			'name',
			'singlePage:boolean',
			'private:boolean',
			[
				'label' => 'URL publique',
				'type' => 'url',
				'value' => $model->singlePage ? $this->createAbsoluteUrl('/site/page', ['p' => $model->name]) : null,
			],
			'pageTitle',
			'type',
			[
				'name' => 'content',
				'type' => 'raw',
				'value' => $model->toHtml(false),
			],
			[
				'name' => 'dcdescription',
				'visible' => $model->isPage(),
			],
			'hdateCreat:datetime',
			'hdateModif:datetime',
		],
	]
);
