<?php

/** @var Controller $this */
/** @var \models\cms\BreveForm $model */

$this->breadcrumbs = [
	'Blocs éditoriaux' => ['admin'],
	'Brèves' => ['breves'],
	'Nouvelle brève',
];

\Yii::app()->getComponent('sidebar')->menu = [
	['label' => 'Liste des blocs', 'url' => ['admin']],
];
?>

<h1>Nouvelle brève</h1>

<?= $this->renderPartial('_form-breve', ['model' => $model]) ?>
