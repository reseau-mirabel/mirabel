<?php

/** @var Controller $this */
/** @var \models\cms\BreveForm $model */

$partenaires = \Yii::app()->db
	->createCommand("SELECT id, CONCAT(nom, ' ', sigle) AS label, nom AS value FROM Partenaire ORDER BY nom")
	->queryAll();

$sources = \components\SqlHelper::sqlToPairs("SELECT id, nom FROM Sourcelien ORDER BY nom");

$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'cms-breve-form',
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'enableAjaxValidation' => false,
		'hints' => Hint::model()->getMessages('Cms'),
		'htmlOptions' => ['class' => 'well'],
	]
);
/** @var BootActiveForm $form */
?>

<?= $form->errorSummary($model) ?>

<?= $form->textAreaRow($model, 'content', ['id' => 'cms-content', 'rows' => 30, 'cols' => 80, 'class' => "span10"]) ?>

<?= $form->textFieldRow($model, 'date', ['size' => 20, 'placeholder' => 'AAAA-MM-DD']) ?>
<div class="control-group">
	<label class="control-label"></label>
	<div class="controls">
		Par défaut, brève créée le <strong><?= $model->getDateFr() ?></strong>
		(<?= $model->getDateIso() ?>)
	</div>
</div>

<?= $form->textFieldRow($model, 'categories', ['class' => 'span12', 'placeholder' => 'catégories libres séparées par ;', 'id' => "breve-categories", 'autocomplete' => "off"]) ?>

<?= $form->textFieldRow($model, 'partenaireIds', ['class' => 'span6', 'placeholder' => 'ID séparés par ,', 'id' => 'breve-partenaires', 'autocomplete' => "off"]) ?>
<div class="control-group">
	<label class="control-label"></label>
	<div class="controls">
		<input class="span12" placeholder="nom du partenaire à ajouter" id="breve-partenaire" autocomplete="off" name="p" type="text">
	</div>
</div>

<?= $form->textFieldRow($model, 'ressourceIds', ['class' => 'span6', 'placeholder' => 'ID séparés par ,', 'id' => 'breve-ressources', 'autocomplete' => "off"]) ?>
<div class="control-group">
	<label class="control-label"></label>
	<div class="controls">
		<input class="span12" placeholder="nom de la ressource à ajouter" id="breve-ressource" autocomplete="off" name="r" type="text">
	</div>
</div>

<?= $form->textFieldRow($model, 'sourcelienIds', ['class' => 'span6', 'placeholder' => 'ID séparés par ,', 'id' => 'breve-sources', 'autocomplete' => "off"]) ?>
<div class="control-group">
	<label class="control-label"></label>
	<div class="controls">
		<input class="span12" placeholder="nom de la source de lien" id="breve-source" autocomplete="off" name="s" type="text">
	</div>
</div>

<?php
// Complétions
// 0. générique
$cs = Yii::app()->clientScript;
$cs->registerCoreScript('jquery.ui');
$cs->registerCssFile($cs->getCoreScriptUrl() . '/jui/css/base/jquery-ui.css');
// 1. categories
$sourcesCat = json_encode(array_values(\models\cms\BreveSearchForm::listCategories()));
$cs->registerScript('c', <<<EOJS
	const sources = $sourcesCat;
	function extractLast(term) {
		return term.toString().split(/\s*;\s*/).pop();
	}
	$("#breve-categories").autocomplete({
		minlength: 2,
		source: function(request, response) {
			const completion = $.ui.autocomplete.filter(
				sources,
				extractLast(request.term)
			);
			response(completion);
		},
		focus: function() {
		  return false;
		},
		select: function(event, ui) {
			var terms = this.value.split(/\s*;\s*/);
			terms.pop(); // remove the text that will be completed
			terms.push(ui.item.value);
			terms.push("");
			this.value = terms.join( " ; " );
			return false;
		}
	})
	EOJS
);
// 2. partenaires
$sourcesPart = json_encode($partenaires);
$cs->registerScript('p', <<<EOJS
	const sourcesPart = $sourcesPart;
	$("#breve-partenaire").autocomplete({
		minlength: 2,
		source: function(request, response) {
			const completion = $.ui.autocomplete.filter(
				sourcesPart,
				extractLast(request.term)
			);
			response(completion);
		},
		focus: function() {
		  return false;
		},
		select: function(event, ui) {
			let ids = $("#breve-partenaires").val();
			if (ids === '') {
				ids = ui.item.id.toString();
			} else {
				ids += ", " + ui.item.id;
			}
			$("#breve-partenaires").val(ids);
			$("#breve-partenaire").val("");
			return false;
		}
	})
	EOJS
);
// 3. ressources
$cs->registerScript('r', <<<EOJS
	$("#breve-ressource").autocomplete({
		minlength: 2,
		source: function(request, response) {
			$.getJSON(
				"/ressource/ajax-complete",
				{ term: extractLast(request.term) },
				response
			);
		},
		focus: function() {
		  return false;
		},
		select: function(event, ui) {
			let ids = $("#breve-ressources").val();
			if (ids === '') {
				ids = ui.item.id.toString();
			} else {
				ids += ", " + ui.item.id;
			}
			$("#breve-ressources").val(ids);
			$("#breve-ressource").val("");
			return false;
		}
	})
	EOJS
);
$sourcesLiens = json_encode(\Yii::app()->db->createCommand("SELECT id, nom AS label FROM Sourcelien ORDER BY nom")->queryAll());
// 4. sources de liens
$cs->registerScript('s', <<<EOJS
	$("#breve-source").autocomplete({
		minlength: 2,
		source: $sourcesLiens,
		focus: function() {
		  return false;
		},
		select: function(event, ui) {
			let ids = $("#breve-sources").val();
			if (ids === '') {
				ids = ui.item.id.toString();
			} else {
				ids += ", " + ui.item.id;
			}
			$("#breve-sources").val(ids);
			$("#breve-source").val("");
			return false;
		}
	})
	EOJS
);
?>

<div class="form-actions">
	<button type="button" id="preview" class="btn btn-info">Prévisualiser</button>
	<?php
	$ajaxUrl = json_encode($this->createUrl('preview'));
	$cs->registerScript('preview', <<<EOJS
		$('button#preview').on('click', function() {
			$.ajax({
				url: $ajaxUrl,
				method: 'POST',
				data: {
					"Cms[name]": "brève",
					"Cms[content]": $("#cms-content").val(),
				},
			}).then(function(data) {
				$("#preview .modal-body").html(data).parent().modal("show");
			});
		});
		EOJS
	);
	?>
	<button type="submit" class="btn btn-primary">Enregistrer cette brève</button>
</div>

<?php
$this->endWidget();
?>

<?php
$this->beginWidget(
	'bootstrap.widgets.BootModal',
	['id' => 'preview', 'htmlOptions' => ['class' => 'large']]
);
?>
	<div class="modal-header">
		<a href="#" class="close" data-dismiss="modal">&times;</a>
		<h3>Prévisualisation (non-sauvegardée)</h3>
	</div>
	<div class="modal-body">
	</div>
	<div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal">Fermer</a>
	</div>
<?php
$this->endWidget();

$this->renderPartial('_syntax', []);
