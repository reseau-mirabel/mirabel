<?php

/** @var Controller $this */
/** @var models\cms\BreveIndicateurs $indicateurs */

$this->breadcrumbs = [
	'Blocs éditoriaux' => ['admin'],
	"Brèves" => ['breves'],
	"Indicateurs",
];
?>

<div class="pull-right">
	<ul class="well nav nav-list">
		<li class="nav-header">Rubriques</li>
		<li><a href="#categories">Catégories</a></li>
		<li><a href="#partenaires">Partenaires</a></li>
		<li><a href="#ressources">Ressources</a></li>
		<li><a href="#sourcesliens">Liens</a></li>
	</ul>
</div>

<h1>Brèves : indicateurs</h1>

<section>
	<h2>Aucune catégorisation</h2>
	<p><?= CHtml::link($indicateurs->countNoMetadata() . " brèves", ['/cms/breves', 'q' => ['rien' => 1]]) ?></p>
</section>

<section id="categories">
	<h2>Catégories textuelles</h2>
	<table class="table table-striped table-condensed sortable">
		<thead>
			<tr>
				<th class="sorted ascend">Nom</th>
				<th>Brèves</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach ($indicateurs->listeCategorie() as $name => $count) {
				echo "<tr>",
					"<td>", CHtml::encode($name), "</td>",
					"<td>", CHtml::link("$count", ['/cms/breves', 'q' => ['categorie' => $name]]), "</td>",
					"<td>", CHtml::link('<span class="glyphicon glyphicon-globe"></span>', ['/site/actualite', 'q' => ['categorie' => $name]]), "</td>",
					"</tr>";
			}
			?>
		</tbody>
	</table>
</section>

<section id="partenaires">
	<h2>Partenaires</h2>
	<table class="table table-striped table-condensed sortable">
		<thead>
			<tr>
				<th class="sorted ascend">Nom</th>
				<th>Statut</th>
				<th>Brèves</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach ($indicateurs->listePartenaire() as ['id' => $id, 'partenaire' => $p, 'count' => $count]) {
				echo "<tr>",
					"<td>", ($p === null ? "SUPPRIMÉ ($id)" : $p->getSelfLink()), "</td>",
					"<td>", ($p !== null && $p->statut !== Partenaire::STATUT_ACTIF ? " <strong>{$p->statut}</strong>" : ""), "</td>",
					"<td>", CHtml::link("$count", ['/cms/breves', 'q' => ['partenaireId' => $id]]), "</td>",
					"<td>", CHtml::link('<span class="glyphicon glyphicon-globe"></span>', ['/site/actualite', 'q' => ['partenaireId' => $id]]), "</td>",
					"</tr>";
			}
			?>
		</tbody>
	</table>
</section>

<section id="ressources">
	<h2>Ressources</h2>
	<table class="table table-striped table-condensed sortable">
		<thead>
			<tr>
				<th class="sorted ascend">Nom</th>
				<th>Brèves</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach ($indicateurs->listeRessource() as ['id' => $id, 'ressource' => $r, 'count' => $count]) {
				echo "<tr>",
					"<td>", ($r === null ? "SUPPRIMÉE ($id)" : $r->getSelfLink()), "</td>",
					"<td>", CHtml::link("$count", ['/cms/breves', 'q' => ['ressourceId' => $id]]), "</td>",
					"<td>", CHtml::link('<span class="glyphicon glyphicon-globe"></span>', ['/site/actualite', 'q' => ['ressourceId' => $id]]), "</td>",
					"</tr>";
			}
			?>
		</tbody>
	</table>
</section>

<section id="sourcesliens">
	<h2>Autres liens</h2>
	<table class="table table-striped table-condensed sortable">
		<thead>
			<tr>
				<th class="sorted ascend">Nom</th>
				<th>Brèves</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach ($indicateurs->listeSourcelien() as ['id' => $id, 'sourcelien' => $s, 'count' => $count]) {
				echo "<tr>",
					"<td>", ($s === null ? "SOURCE SUPPRIMÉE ($id)" : CHtml::encode($s->nom)), "</td>",
					"<td>", CHtml::link("$count", ['/cms/breves', 'q' => ['sourcelienId' => $id]]), "</td>",
					"<td>", CHtml::link('<span class="glyphicon glyphicon-globe"></span>', ['/site/actualite', 'q' => ['sourcelienId' => $id]]), "</td>",
					"</tr>";
			}
			?>
		</tbody>
	</table>
</section>
