<?php

/** @var Controller $this */
/** @var models\cms\BreveSearchForm $model */
/** @var string[] $categories */
/** @var string[] $partenaires */
/** @var string[] $ressources */
/** @var string[] $sources */

$this->breadcrumbs = [
	'Blocs éditoriaux' => ['admin'],
	"Brèves",
];
?>

<h1>Brèves</h1>

<?php
if (Yii::app()->user->checkAccess('redaction/editorial')) {
	echo '<div style="text-align: right">'
		. CHtml::link("Indicateurs", ['breves-indicateurs'], ['class' => 'btn btn-default']) . " "
		. CHtml::link("Nouvelle brève", ['create-breve'], ['class' => 'btn btn-success'])
		. '</div>';
}
?>

<?php
$searchAttributes = $model->exportAttributes();
if ($searchAttributes) {
	?>
	<div class="pull-right">
		<code>%DER_ACTU(5, <?= CHtml::encode(json_encode($searchAttributes, JSON_UNESCAPED_UNICODE)) ?>)%</code><br />
		<?= CHtml::link("liste publique", ['site/actualite', 'q' => $searchAttributes]) ?>
	</div>
	<?php
}
?>

<?php
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'cms-breve-search-form',
		'method' => 'GET',
		'action' => '/cms/breves',
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'enableAjaxValidation' => false,
		'htmlOptions' => ['class' => 'well'],
	]
);
/** @var BootActiveForm $form */
?>

<?= $form->errorSummary($model) ?>

<?= $form->textFieldRow($model, 'texte') ?>
<?= $form->dropDownListRow($model, 'categorie', $categories, ['prompt' => "-"]) ?>
<?= $form->dropDownListRow($model, 'partenaireId', $partenaires, ['prompt' => "-"]) ?>
<?= $form->dropDownListRow($model, 'ressourceId', $ressources, ['prompt' => "-"]) ?>
<?= $form->dropDownListRow($model, 'sourcelienId', $sources, ['prompt' => "-"]) ?>
<?= $form->checkBoxRow($model, 'rien', $categories) ?>

<div class="form-actions" style="margin-bottom: 0; padding-top: 12px; padding-bottom: 0px;">
	<button type="submit" class="btn btn-primary">Filtrer les brèves</button>
</div>

<?php $this->endWidget(); ?>

<?php
$columns = [
	[
		'name' => 'content',
		'type' => 'raw',
		'filter' => false,
		'htmlOptions' => ['class' => 'contenu'],
		'value' => function ($data) {
			if (strlen($data->content) < 200) {
				return $data->content;
			}
			$text = "<b>" . CHtml::encode(strtok($data->content, "\n")) . "</b>";
			$text .= '<div style="padding-left: 4ex; font-size: 90%;">'
				. $this->renderPartial('_categorisation', ['json' => $data->categorisation, 'verbose' => 0], true)
				. '</div>';
			return $text;
		},
	],
	[
		'name' => 'hdateCreat',
		'type' => 'date',
		'filter' => false,
	],
	[
		'name' => 'hdateModif',
		'type' => 'date',
		'filter' => false,
	],
	[
		'class' => 'BootButtonColumn',
		'header' => '',
		'template' => '{view}{update}',
	],
];
$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	[
		'id' => 'cms-grid',
		'dataProvider' => $model->search(),
		'filter' => null, // null to disable
		'columns' => $columns,
		'ajaxUpdate' => false,
	]
);
