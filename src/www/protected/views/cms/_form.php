<?php

/** @var Controller $this */
/** @var Cms $model */
assert($this instanceof Controller);

$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'cms-form',
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'enableAjaxValidation' => false,
		'hints' => Hint::model()->getMessages('Cms'),
		'htmlOptions' => ['class' => 'well'],
	]
);
/** @var BootActiveForm $form */
?>

<?php
echo $form->errorSummary($model);

echo $form->hiddenField($model, 'singlePage');
if (!$model->id && !$model->isBreve()) {
	echo $form->textFieldRow($model, 'name', ['class' => 'span5', 'id' => 'page-name']);
	?>
	<div class="control-group">
		<div class="controls" id="name-in-url">
		</div>
	</div>
	<?php
	Yii::app()->clientScript->registerScript('name-refresh', <<<JS
		$('#page-name').on('input', function() {
			const value = this.value.trim();
			if (value === '') {
				$('#name-in-url').text("");
			} else {
				$('#name-in-url').text("Nom dans l'URL : " + encodeURIComponent(value));
			}
		});
		JS
	);
}
if ($model->singlePage) {
	echo $form->textFieldRow($model, 'pageTitle', ['class' => 'span10']);
	echo $form->checkBoxRow($model, 'private');
	echo $form->textAreaRow($model, 'dcdescription', ['rows' => 3, 'cols' => 80, 'class' => 'span10']);
}

$class = "span10";
if ($model->type === 'html') {
	new \widgets\CkEditor();
	$class .= " htmleditor";
}
echo $form->textAreaRow($model, 'content', ['rows' => 30, 'cols' => 80, 'class' => $class]);

if ($model->type !== 'markdown') {
	?>
	<div class="control-group">
		<div class="controls">
			Format : <?= CHtml::encode($model->type) ?>
		</div>
	</div>
	<?php
}
?>

<div class="form-actions">
	<?php
	if ($model->type !== 'html') {
		$this->widget(
			'bootstrap.widgets.BootButton',
			[
				'type' => 'info',
				'buttonType' => BootButton::BUTTON_AJAXSUBMIT,
				'label' => 'Prévisualiser',
				'url' => $this->createUrl('preview'),
				'ajaxOptions' => ['success' => 'js:function(data) {
						$("#preview .modal-body").html(data).parent().modal("show");
					}'],
			]
		);
	}
	?>
	<?php
	$this->widget(
		'bootstrap.widgets.BootButton',
		[
			'buttonType' => 'submit',
			'type' => 'primary',
			'label' => $model->isNewRecord ? 'Créer' : 'Enregistrer les modifications',
		]
	);
	?>
</div>

<?php
$this->endWidget();
?>

<?php
$this->beginWidget(
	'bootstrap.widgets.BootModal',
	['id' => 'preview', 'htmlOptions' => ['class' => 'large']]
);
?>
	<div class="modal-header">
		<a href="#" class="close" data-dismiss="modal">&times;</a>
		<h3>Prévisualisation (non-sauvegardée)</h3>
	</div>
	<div class="modal-body">
	</div>
	<div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal">Fermer</a>
	</div>
<?php
$this->endWidget();


if ($model->id > 0) {
	$this->renderPartial('_references', ['model' => $model]);
}

if ($model->type === 'markdown') {
	$this->renderPartial('_syntax', []);
} elseif ($model->name === 'actualite-menu') {
	?>
	Exemples de contenus (à insérer dans le source HTML) :
	<ul>
		<li>Pour créer un bloc flottant à droite, entourer le contenu par <?= CHtml::encode('<div class="pull-right"></div>') ?>.</li>
		<li>URL catégorie <em>M22</em> : <?= CHtml::encode($this->createUrl('/site/actualite', ['q' => ['categorie' => 'M22']])) ?></li>
		<li>URL partenaire ScPoLyon : <?= CHtml::encode($this->createUrl('/site/actualite', ['q' => ['partenaireId' => '1']])) ?></li>
		<li>URL ressource Cairn : <?= CHtml::encode($this->createUrl('/site/actualite', ['q' => ['ressourceId' => '3']])) ?></li>
		<li>Une liste démarrée avec <?= CHtml::encode('<ul class="nav nav-tabs nav-stacked">') ?> sera présentée sous forme de pile</li>
	</ul>
	<?php
}
?>
