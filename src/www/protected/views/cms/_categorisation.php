<?php

/** @var Controller $this */
/** @var string $json */
/** @var int $verbose */

if (!$json || $json === '{}') {
	return;
}

$categorisation = json_decode($json, false);
if (!$categorisation) {
	return;
}

// { categories: ["a"], partenaires: [1], ressources: [2], sourcesliens: [] }
?>
<ul class="unstyled">
	<?php if (!empty($categorisation->categories)) { ?>
	<li>
		<?php if ($verbose > 0) { ?>
		<strong>Catégories : </strong>
		<?php } ?>
		<?php
		$l = [];
		foreach ($categorisation->categories as $id) {
			$search = $verbose ? CHtml::link('<i class="icon icon-search"></i>', ['/cms/breves', 'q' => ['categorie' => $id]]) : "";
			$l[] = "$id $search";
		}
		?>
		<?= join(" ; ", $l) ?>
	</li>
	<?php } ?>

	<?php if (!empty($categorisation->partenaires)) { ?>
	<li>
		<?php if ($verbose > 0) { ?>
		<strong>Partenaires : </strong>
		<?php } ?>
		<?php
		$l = [];
		foreach ($categorisation->partenaires as $id) {
			$search = $verbose ? CHtml::link('<i class="icon icon-search"></i>', ['/cms/breves', 'q' => ['partenaireId' => $id]]) : "";
			$o = Partenaire::model()->findByPk($id);
			if ($o instanceof Partenaire) {
				$l[] = "$id {$o->getSelfLink()} $search";
			} else {
				$l[] = "$id (aucun partenaire avec cet ID) $search";
			}
		}
		?>
		<?= join(" ; ", $l) ?>
	</li>
	<?php } ?>

	<?php if (!empty($categorisation->ressources)) { ?>
	<li>
		<?php if ($verbose > 0) { ?>
		<strong>Ressources : </strong>
		<?php } ?>
		<?php
		$l = [];
		foreach ($categorisation->ressources as $id) {
			$search = $verbose ? CHtml::link('<i class="icon icon-search"></i>', ['/cms/breves', 'q' => ['ressourceId' => $id]]) : "";
			$o = Ressource::model()->findByPk($id);
			if ($o instanceof Ressource) {
				$l[] = "$id {$o->getSelfLink()} $search";
			} else {
				$l[] = "$id (aucune ressource avec cet ID) $search";
			}
		}
		?>
		<?= join(" ; ", $l) ?>
	</li>
	<?php } ?>

	<?php if (!empty($categorisation->sourcesliens)) { ?>
	<li>
		<?php if ($verbose > 0) { ?>
		<strong>Liens : </strong>
		<?php } ?>
		<?php
		$l = [];
		foreach ($categorisation->sourcesliens as $id) {
			$search = $verbose ? CHtml::link('<i class="icon icon-search"></i>', ['/cms/breves', 'q' => ['sourcelienId' => $id]]) : "";
			$o = Sourcelien::model()->findByPk($id);
			if ($o instanceof Sourcelien) {
				$l[] = "$id " . CHtml::encode($o->nom) . " $search";
			} else {
				$l[] = "$id (aucune source de lien avec cet ID) $search";
			}
		}
		?>
		<?= join(" ; ", $l) ?>
	</li>
	<?php } ?>
</ul>
