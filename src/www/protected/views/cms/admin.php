<?php

/** @var Controller $this */
/** @var Cms $model */
/** @var string $filter */
assert($this instanceof Controller);

$canEditAll = Yii::app()->user->checkAccess('redaction/editorial');

$this->breadcrumbs = [
	'Blocs éditoriaux',
];

\Yii::app()->getComponent('sidebar')->menu = [
	['label' => 'Nouvelle brève', 'url' => ['create-breve'], 'visible' => $canEditAll],
	['label' => 'Nouvelle page', 'url' => ['create-page']],
	['label' => 'Listes', 'itemOptions' => ['class' => 'nav-header']],
	['label' => 'Tout', 'url' => ['admin']],
	['label' => 'Blocs', 'url' => ['admin', 'filter' => 'blocs']],
	['label' => 'Brèves', 'url' => ['/cms/breves'], 'visible' => $canEditAll],
	['label' => 'Brèves (indicateurs)', 'url' => ['/cms/breves-indicateurs'], 'visible' => $canEditAll],
	['label' => 'Pages', 'url' => ['admin', 'filter' => 'pages']],
];
?>

<h1>Liste des blocs éditoriaux<?= CHtml::encode($filter ? " ($filter)" : ''); ?></h1>

<?php
$columns = [
	[
		'name' => 'name',
		'visible' => ($filter !== 'breves'),
	],
	[
		'name' => 'content',
		'htmlOptions' => ['class' => 'contenu'],
		'value' => function (Cms $data) {
			if (strlen($data->content) < 200) {
				return $data->content;
			}
			return strtok($data->content, "\n");
		},
	],
	[
		'name' => 'hdateCreat',
		'type' => 'date',
		'filter' => false,
	],
	[
		'name' => 'hdateModif',
		'type' => 'date',
		'filter' => false,
	],
	[
		'header' => '',
		'type' => 'raw',
		'htmlOptions' => ['style'=>'text-align: right', 'class' => 'button-column'],
		'value' => function (Cms $data) {
			$label = CHtml::link('<i class="icon-eye-open"></i>', ['view', 'id' => $data->id], ['title' => "Voir"])
				. CHtml::link('<i class="icon-pencil"></i>', ['update', 'id' => $data->id], ['title' => "Mettre à jour"]);
			if ($data->singlePage && $data->private) {
				$label = '<i class="glyphicon glyphicon-lock cms-private" title="page privée"></i>' . $label;
			}
			return $label;
		},
	],
];
$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	[
		'id' => 'cms-grid',
		'dataProvider' => $model->search(),
		'filter' => $model, // null to disable
		'columns' => $columns,
		'ajaxUpdate' => false,
	]
);
?>
