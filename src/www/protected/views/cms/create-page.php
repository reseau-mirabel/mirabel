<?php

/** @var Controller $this */
/** @var Cms $model */
assert($this instanceof Controller);

$this->pageTitle = "Nouvelle page éditoriale";

$this->breadcrumbs = [
	'Blocs éditoriaux' => ['admin'],
	'Nouvelle page',
];

\Yii::app()->getComponent('sidebar')->menu = [
	['label' => 'Liste des blocs', 'url' => ['admin']],
];
?>

<h1><?= $this->pageTitle ?></h1>

<?= $this->renderPartial('_form', ['model' => $model]) ?>
