<?php

/** @var Controller $this */
/** @var \models\cms\BreveForm $model */
/** @var int $id */

$this->breadcrumbs = [
	'Blocs éditoriaux' => ['admin'],
	'Brèves' => ['breves'],
	"Brève $id" => ['view', 'id' => $id],
	'Modification',
];

\Yii::app()->getComponent('sidebar')->menu = [
	['label' => 'Liste des blocs', 'url' => ['admin']],
	['label' => 'Voir cette brève', 'url' => ['view', 'id' => $id]],
	['label' => 'Créer une brève', 'url' => ['create-breve'], 'visible' => Yii::app()->user->checkAccess('redaction/editorial')],
	[
		'label' => 'Supprimer cette brève',
		'url' => '#',
		'visible' => Yii::app()->user->checkAccess('redaction/editorial'),
		'linkOptions' => ['submit' => ['delete', 'id' => $id], 'confirm' => 'Êtes-vous certain ?'],
	],
];
?>

<h1>Modifier une brève</h1>

<?= $this->renderPartial('_form-breve', ['model' => $model])?>
