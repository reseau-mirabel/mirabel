<?php

/** @var Controller $this */
/** @var Cms $model */
assert($this instanceof Controller);

$canEditAll = Yii::app()->user->checkAccess('redaction/editorial');

$this->breadcrumbs = [
	'Blocs éditoriaux' => ['admin'],
	"Bloc \"{$model->name}\"" => ['view', 'id' => $model->id],
	'Modification',
];

\Yii::app()->getComponent('sidebar')->menu = [
	['label' => 'Liste des blocs', 'url' => ['admin']],
	['label' => 'Voir ce bloc', 'url' => ['view', 'id' => $model->id]],
	['label' => 'Créer une brève', 'url' => ['create-breve'], 'visible' => $canEditAll],
	['label' => 'Créer une page', 'url' => ['create-page']],
	[
		'label' => 'Supprimer cette brève',
		'url' => '#',
		'visible' => $model->isBreve() && $canEditAll,
		'linkOptions' => ['submit' => ['delete', 'id' => $model->id], 'confirm' => 'Êtes-vous certain ?'],
	],
];
?>

<h1>Modifier le bloc <em><?= $model->name ?></em></h1>

<?= $this->renderPartial('_form', ['model' => $model])?>
