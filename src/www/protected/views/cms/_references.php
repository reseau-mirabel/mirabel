<?php

/** @var Controller $this */
/** @var Cms $model */
assert($this instanceof Controller);

if (Yii::app()->user->checkAccess('admin') && $model->isPage()) {
	$references = Yii::app()->db
		->createCommand("SELECT id FROM Cms WHERE content RLIKE :url1 OR content RLIKE :url2 OR content RLIKE :url3")
		->queryColumn([
			':url1' => "/site/page/" . preg_quote(rawurlencode($model->name)) . '\b',
			':url2' => "/site/page/" . preg_quote(str_replace(' ', '+', $model->name)) . '\b',
			':url3' => "/site/page/" . preg_quote(str_replace(' ', '_', $model->name)) . '\b',
		]);
	if ($references) {
		echo "<p>Cette page est référencée dans les éléments éditoriaux d'ID " . join(", ", $references) . ".</p>";
	} else {
		echo "<p>Aucun élément éditorial n'a de lien vers cette page.</p>";
		echo \components\HtmlHelper::postButton("Supprimer cette page", ['delete', 'id' => $model->id], [], ['class' => 'btn btn-danger', 'onclick' => 'return confirm("Suppression définitive ?");']);
	}
}
