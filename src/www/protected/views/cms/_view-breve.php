<?php

/** @var Controller $this */
/** @var Cms $model */

$this->widget(
	'bootstrap.widgets.BootDetailView',
	[
		'data' => $model,
		'attributes' => [
			'id',
			[
				'label' => 'URL publique',
				'type' => 'url',
				'value' => $this->createAbsoluteUrl('/site/actualite', ['id' => $model->id]),
			],
			'type',
			[
				'name' => 'content',
				'type' => 'raw',
				'value' => $model->toHtml(false),
			],
			[
				'name' => 'categorisation',
				'type' => 'raw',
				'value' => $this->renderPartial('_categorisation', ['json' => $model->categorisation, 'verbose' => 1], true),
				'visible' => $model->isBreve(),
			],
			'hdateCreat:datetime',
			'hdateModif:datetime',
		],
	]
);
