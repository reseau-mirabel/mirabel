<?php

/** @var Controller $this */
/** @var Cms $model */
assert($this instanceof Controller);

$this->pageTitle = 'CMS - ' . $model->name;

$this->breadcrumbs = [
	'Fonctions éditoriales' => ['admin'],
];
if ($model->isBreve()) {
	$this->breadcrumbs["Brèves"] = ['breves'];
	$title = "Brève";
} elseif ($model->isBloc()) {
	$this->breadcrumbs["Bloc"] = ['admin', 'filter' => 'blocs'];
	$title = "Bloc éditorial " . CHtml::tag('em', [], CHtml::encode($model->name));
} else {
	$this->breadcrumbs["Pages"] = ['admin', 'filter' => 'pages'];
	$title = "Page éditoriale " . CHtml::tag('em', [], CHtml::encode($model->name));
}
$this->breadcrumbs[] = $model->name;

\Yii::app()->getComponent('sidebar')->menu = [
	['label' => 'Liste des blocs', 'url' => ['admin', 'filter' => 'blocs']],
	['label' => 'Liste des brèves', 'url' => ['breves']],
	['label' => 'Liste des pages', 'url' => ['admin', 'filter' => 'pages']],
	['label' => 'Modifier', 'url' => ['update-' . ($model->isBreve() ? 'breve' : 'page'), 'id' => $model->id]],
	['label' => 'Créer une brève', 'url' => ['create-breve'], 'visible' => $model->isBreve() && Yii::app()->user->checkAccess('redaction/editorial')],
	['label' => 'Créer une page', 'url' => ['create-page'], 'visible' => $model->isPage() && Yii::app()->user->checkAccess('redaction/editorial')],
	[
		'label' => 'Supprimer cette brève',
		'url' => '#',
		'visible' => $model->isBreve() && Yii::app()->user->checkAccess('redaction/editorial'),
		'linkOptions' => ['submit' => ['delete', 'id' => $model->id], 'confirm' => 'Êtes-vous certain ?'],
	],
];
?>

<h1><?= $title ?></h1>

<?php
if ($model->isBreve()) {
	$this->renderPartial('_view-breve', ['model' => $model]);
} else {
	$this->renderPartial('_view-cms', ['model' => $model]);
}
?>

<?php
$this->renderPartial('_references', ['model' => $model]);
