<?php

/** @var string $graphContent */
/** @var string $graphName */
/** @var Controller $this */

$this->breadcrumbs = [
	'Administration' => ['admin/index'],
	'Baromètre Index' => ['barometre/index'],
	$graphName,
];
?>
<?= $graphContent; ?>
