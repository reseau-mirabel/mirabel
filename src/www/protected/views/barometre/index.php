<?php
/** @var array $graphNames */
/** @var string $modificationTime */
/** @var Controller $this */

assert($this instanceof Controller);

$this->breadcrumbs = [
	'Administration' => ['admin/index'],
	'Baromètre Index',
];
if ($modificationTime) {
	echo "<h1>Baromètre au $modificationTime</h1>";
} else {
	echo "<h1>Baromètre</h1>";
}
?>
<?php if (!empty($graphNames)): ?>
	<ul>
		<?php foreach ($graphNames as $graphName): ?>
			<li>
				<?= CHtml::link(
					CHtml::encode($graphName),
					['barometre/view-graph', 'graphName' => $graphName]
				) ?>
			</li>
		<?php endforeach; ?>
	</ul>
<?php else: ?>
	<p>Aucun graphique disponible</p>
<?php endif; ?>

