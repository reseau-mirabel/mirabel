<?php

/** @var Controller $this */
/** @var Upload $model */
/** @var array $uploaded */
/** @var string $lisezmoi HTML */

assert($this instanceof Controller);

$this->pageTitle = $model->getDestination()->name;
$this->breadcrumbs = [
	'Dépôt' => ['/upload'],
	$model->getDestination()->name,
];
\Yii::app()->getComponent('sidebar')->menu = [
	['label' => "Dépôts publics", 'itemOptions' => ['class' => 'nav-header']],
	['label' => 'Dépôt général', 'url' => ['/upload']],
	['label' => 'Logos de partenaires', 'url' => ['/upload', 'dest' => 'logos-p']],
	['label' => 'Vidéos', 'url' => ['/upload', 'dest' => 'videos']],
	['label' => "Dépôts privés", 'itemOptions' => ['class' => 'nav-header']],
	['label' => 'Dépôt privé', 'url' => ['/upload', 'dest' => 'private']],
	['label' => 'Conventions', 'url' => ['/upload', 'dest' => 'conventions']],
	['label' => 'Éditeurs - lettres', 'url' => ['/upload', 'dest' => 'editeurs']],
];
?>

<h1><?= CHtml::encode($model->getDestination()->name); ?></h1>

<ul>
	<?= $model->getDestination()->message; ?>
	<li>Le nom de fichier doit être composé de lettres non accentuées et de <code>()_.-</code>.</li>
</ul>

<?php
if ($lisezmoi) {
	echo "<div>$lisezmoi</div>\n";
}
?>

<?php
/** @var BootActiveForm */
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'upload-form',
		'enableAjaxValidation' => false,
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'htmlOptions' => ['class' => 'well', 'enctype' => 'multipart/form-data'],
	]
);

echo $form->errorSummary($model);

echo $form->hiddenField($model, 'destType');
if ($model->destType === 'logos-p') {
	if (Yii::app()->user->access()->toPartenaire()->admin()) {
		$data = ['' => " - "];
		foreach (Partenaire::model()->ordered()->findAll() as $p) {
			/** @var Partenaire $p */
			$pid = sprintf('%03d', $p->id);
			$data[$pid] = $pid . " " . $p->nom;
		}
		echo $form->dropDownListRow($model, 'destName', $data);
	} else {
		echo $form->hiddenField($model, 'destName');
		$p = Partenaire::model()->findByPk(Yii::app()->user->getState('partenaireId'));
		if ($p === null) {
			throw new CHttpException(500, "L'enregistrement de votre institution n'a pas été trouvé en base de données. Contactez un administrateur.");
		}
		echo $form->textFieldRow($model, 'destName', ['value' => "{$model->destName} - {$p->nom}", "disabled" => "disabled"]);
	}
}
echo $form->fileFieldRow($model, 'file', ['class' => 'span8']);
echo $form->checkBoxRow($model, 'overwrite');
if ($model->canOverrideFileName() && $model->destType !== 'logos-p') {
	echo $form->textFieldRow($model, 'destName', ['class' => 'span8', 'hint' => "Renommer le fichier déposé sur le serveur. L'extension sera ajoutée automatiquement."]);
}
/*
$this->widget(
	'CMultiFileUpload',
	array(
		'name' => 'files',
		'accept' => 'pdf|png|jpg|csv',
		'denied' => 'Ce type de fichier est interdit',
		'duplicate' => 'Doublons !',
		'remove' => 'Enlever de la liste'
	)
);
 */
echo '<div class="form-actions">';
$this->widget(
	'bootstrap.widgets.BootButton',
	[
		'buttonType' => 'submit',
		'type' => 'primary',
		'label' => 'Envoyer',
	]
);
echo '</div>';
$this->endWidget();
?>

<h2>Fichiers présents</h2>
<div>
	Filtres :
	<select name="filter-by-type" id="filter-by-type">
		<option value="">Tous types</option>
		<option value="\.(csv|odp|odt|docx|xls)$">Bureautique</option>
		<option value="\.(jpe?g|png)$">Image</option>
		<option value="\.(pdf)$">pdf</option>
		<option value="\.(avi|mkv|mp4|webm)$">Vidéo</option>
	</select>
	<select name="filter-by-date" id="filter-by-date">
		<option value="">Toutes dates</option>
		<option value="<?= date('Y-m-d', time() - 86400) ?>">depuis hier (24h)</option>
		<option value="<?= date('Y-m-d', time() - 30*86400) ?>">depuis un mois (30j)</option>
	</select>
</div>
<table id="files" class="table table-hover table-striped">
	<thead>
		<tr>
			<th>Nom</th>
			<th>Taille</th>
			<th>Dernière modif.</th>
			<?php
			if ($model->destType === 'logos-p') {
				echo '<th></th>';
			}
			?>
			<th>Suppr.</th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($uploaded as $rank => $file) {
			/** @var array $file */
			if ($model->destType === 'logos-p' && !Yii::app()->user->access()->toPartenaire()->admin() && (int) $file['name'] != Yii::app()->user->partenaireId) {
				continue;
			}
			echo '<tr><td class="name">'
				. CHtml::link(CHtml::encode($file['name']), $model->getViewUrl($file['name']))
				. '</td><td class="size">'
				. $file['size']
				. '</td><td class="date">'
				. $file['date']
				. "</td>";
			if ($model->destType === 'logos-p') {
				if (preg_match('/\.(jpe?g|png)$/', $file['name'])) {
					$path = preg_replace('/\.(?:png|jpe?g)$/', '.png', basename($file['name']));
					$url = "/public/images/partenaires/$path";
					echo '<td title="version affichée par Mir@bel, redimensionnée par rapport à la source">' . CHtml::link(CHtml::image($url), $url) . "</td>";
				} else {
					echo "<td></td>";
				}
			}
			echo '<td class="remove">'
				. CHtml::link('X', ['/upload/delete'], ['data-path' => str_replace(DATA_DIR, '', $file['path'])])
				. "</td>";
			echo "</tr>\n";
		}
		?>
	</tbody>
</table>
<?php
if (!$uploaded) {
	echo "<p>Aucun fichier n'est présent.</p>";
}
Yii::app()->getClientScript()->registerScript(
	'filter-files',
	<<<EOJS
	$("td.remove a").on('click', function(e) {
		e.preventDefault();
		if (!confirm(`Supprimer ce fichier ?`)) {
			return false;
		}
		let cell = this.closest('td.remove');
		let path = this.getAttribute('data-path');
		let url = this.getAttribute('href');
		$.ajax({
			url: url,
			method: 'POST',
			data: {file: path},
		}).then(function(htmlr) {
			$(cell).html(htmlr);
		}, function (xhr) {
			alert(xhr.responseText);
		});
	});

	(function(){
		var filterExt = false;
		var filterDate = '';

		$('#filter-by-type').on('change', function() {
			var val = $('#filter-by-type').val();
			if (val) {
				filterExt = new RegExp(val);
			} else {
				filterExt = false;
			}
			filterFiles();
		});
		$('#filter-by-date').on('change', function() {
			filterDate = $('#filter-by-date').val();
			filterFiles();
		});
		function filterFiles() {
			$('#files tr').each(function(){
				var trname = $(".name", this).text();
				var trdate = $(".date", this).text();
				if (filterExt !== false && !filterExt.test(trname)) {
					$(this).attr('class', 'hidden');
				} else if (filterDate !== '' && trdate < filterDate) {
					$(this).attr('class', 'hidden');
				} else {
					$(this).attr('class', '');
				}
			});
		}
	})();
	EOJS
);
