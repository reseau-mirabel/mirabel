<?php

/** @var Controller $this */
/** @var processes\attribut\ImportParsingForm $formData */
/** @var AttributImportLog $log */
/** @var string $maxUpload */

assert($this instanceof Controller);

$this->pageTitle = "Importer un attribut de titre";
$this->breadcrumbs = [
	'Admin' => ['/admin'],
	'Attributs' => ['/attribut/declare'],
	'Importer' => ['/attribut/import'],
	"Étape 2/3",
];
?>

<h1><?= $this->pageTitle ?></h1>

<div>
	Processus :
	<span><span class="badge">1</span> <a href="/attribut/import">Dépôt de fichier source</a></span>
	→ <span><span class="badge badge-info">2</span> Correspondance entre colonnes et attribut M</span>
	→ <span><span class="badge">3</span> Rapport sur l'import</span>
</div>

<h2>Configurer l'import du fichier</h2>
<div>
	Seules les colonnes choisies seront analysées dans le fichier <code><?= CHtml::encode($log->fileName) ?></code> :
	<ul>
		<li>
			Pour identifier le titre, choisir au moins une colonne qui contient des <strong>ISSN</strong>.
			Les lignes sans ISSN trouvé sont ignorées.
		</li>
		<li>
			La <strong>valeur de l'attribut</strong> pour chaque titre est lu dans l'autre colonne choisie.
		</li>
	</ul>
</div>

<section id="upload">
	<?php
	$form = $this->beginWidget(
		'bootstrap.widgets.BootActiveForm',
		[
			'enableAjaxValidation' => false,
			'enableClientValidation' => true,
			'type' => BootActiveForm::TYPE_HORIZONTAL,
			'htmlOptions' => ['enctype' => 'multipart/form-data'],
		]
	);
	/** @var BootActiveForm $form */
	?>

	<fieldset>
		<legend>Colonnes contenant les identifiants</legend>
		<?php
		$sourceColumns = $formData->getSourceColumns();
		echo $form->dropDownListRow($formData, "colonneIssn1", $sourceColumns, ['empty' => '—']);
		echo $form->dropDownListRow($formData, "colonneIssn2", $sourceColumns, ['empty' => '—']);
		echo $form->dropDownListRow($formData, "colonneIssn3", $sourceColumns, ['empty' => '—']);
		echo $form->dropDownListRow($formData, "colonnePpn1", $sourceColumns, ['empty' => '—']);
		echo $form->dropDownListRow($formData, "colonnePpn2", $sourceColumns, ['empty' => '—']);
		echo $form->dropDownListRow($formData, "colonneMirabel", $sourceColumns, ['empty' => '—']);
		?>
	</fieldset>

	<fieldset>
		<legend>Attribut</legend>
		<?php
		echo $form->dropDownListRow(
			$formData,
			'sourceattributId',
			\components\SqlHelper::sqlToPairs("SELECT id, concat(identifiant, ' – ', nom) FROM Sourceattribut ORDER BY identifiant ASC"),
			['empty' => '—']
		);

		echo $form->dropDownListRow($formData, "colonneAttribut", $formData->getSourceattrColumns(), ['empty' => '—']);
		Yii::app()->getClientScript()->registerScript(
			'filter-candidates',
			<<<EOJS
			$('select#q_colonneAttribut').on('change', function() {
				let column = $('select#q_colonneAttribut').val();
				if (column === '') {
					$('input#q_filtre').parent().find('.grid-view-loading').remove();
					return;
				}
				$('input#q_filtre').parent().append('<span class="grid-view-loading">     </span>');
				fetch('/attribut/ajax-list-values/{$log->id}?colNum=' + column)
					.then((response) => response.json())
					.then(function(data) {
						let html = '';
						for (let d of data) {
							html += '<option value="' + d.replace(/"/g, '&quot;') + '"></option>';
						}
						$("#existing-values").html(html);
						$('input#q_filtre').parent().find('.grid-view-loading').remove();
					});
			});
			EOJS
		);
		?>
	</fieldset>

	<fieldset>
		<legend>Mode de chargement</legend>
		<?php
		echo $form->textFieldRow(
			$formData,
			"filtre",
			[
				'list' => 'existing-values',
				'hint' => "Si cette case n'est pas vide, son contenu sera la seule valeur acceptée dans cet attribut. La liste déroulante contient les valeurs distinctes des 1000 premières lignes, pour la colonne d'attribut choisie."
			]
		);
		echo '<datalist id="existing-values"></datalist>';

		echo $form->checkBoxRow(
			$formData,
			'deleteEmpty',
			['hint' => "Si la case est cochée, alors les cellules vides seront ignorées, donc la valeur '' est équivalente à l'absence du titre dans fichier. "
				. "Si la case est décochée, la valeur '' pourra être enregistrée dans l'attribut d'un titre."]
		);
		echo $form->checkBoxRow(
			$formData,
			'deleteMissing',
			['hint' => "Si la case est cochée et qu'un titre ne figure pas dans cet import, alors il ne doit pas avoir d'attribut. On efface donc sa valeur venant d'un précédent import. "
				. "Si la case est décochée, les valeurs importées précédemment ne seront pas effacées (mais éventuellement écrasées par de nouvelles valeurs)."]
		);
		?>
	</fieldset>

	<fieldset>
		<legend>Pour la création de revues…</legend>
		<p>
			Ce champ n'est utile que pour proposer la création de revues à partir des ISSN inconnus de Mir@bel.
		</p>
		<?php
		echo $form->dropDownListRow($formData, "colonneTitre", $sourceColumns, ['empty' => '-']);
		?>
	</fieldset>

	<div class="form-actions">
		<button type="submit" class="btn btn-primary">Importer dans cet attribut</button>
	</div>
	<?php
	$this->endWidget();
	?>
</section>
