<?php

/** @var Controller $this */
/** @var processes\attribut\ImportSourceForm $formData */
/** @var string $maxUpload */

assert($this instanceof Controller);

$this->pageTitle = "Importer un attribut de titre";
$this->breadcrumbs = [
	'Admin' => ['/admin'],
	'Attributs' => ['/attribut/declare'],
	'Importer' => ['/attribut/import'],
	"Étape 1/3",
];
?>

<h1><?= $this->pageTitle ?></h1>

<div>
	Processus :
	<span><span class="badge badge-info">1</span> Dépôt de fichier source</span>
	→ <span><span class="badge">2</span> Correspondance entre colonnes et attribut M</span>
	→ <span><span class="badge">3</span> Rapport sur l'import</span>
</div>

<h2>Dépôt de fichier source</h2>
<p>
	Fournir une source, soit par dépôt d'un fichier local, soit par déclaration de l'URL du fichier.
</p>
<section id="upload">
	<?php
	$form = $this->beginWidget(
		'bootstrap.widgets.BootActiveForm',
		[
			'enableAjaxValidation' => false,
			'enableClientValidation' => true,
			'type' => BootActiveForm::TYPE_HORIZONTAL,
			'htmlOptions' => ['enctype' => 'multipart/form-data'],
		]
	);
	/** @var BootActiveForm $form */

	echo $form->fileFieldRow($formData, 'uploadedFile');
	?>
	<p style="margin-left: 5ex; margin-top: -2ex;">
		Le serveur est configuré pour une taille maximale de <?= CHtml::encode($maxUpload) ?>.
		Le fichier doit être soit textuel (extension <code>csv, tsv, txt</code>)
		soit tableur (extension <code>ods, xlsx</code>).
		Pour un fichier tableur, seule la première feuille est analysée.
	</p>

	<?php
	echo $form->textFieldRow($formData, "url", ['class' => 'span12', 'type' => 'url']);
	?>

	<div class="form-actions">
		<button type="submit" class="btn btn-primary">Envoyer</button>
	</div>
	<?php
	$this->endWidget();
	?>
</section>
