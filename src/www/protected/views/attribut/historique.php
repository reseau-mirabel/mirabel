<?php

/** @var Controller $this */
/** @var ?Sourceattribut $attribut */
/** @var CActiveDataProvider $logsProvider */

assert($this instanceof Controller);

$this->pageTitle = ($attribut
	? "Historique des imports de l'attribut {$attribut->nom}"
	: "Historique des imports d'attributs"
);

$this->breadcrumbs = [
	'Admin' => ['/admin'],
	'Attributs' => ['/attribut/declare'],
	"Historique",
];
?>
<h1>
	<?= CHtml::encode($this->pageTitle) ?>
</h1>

<?php if ($attribut) { ?>
<p>
	Lister les
	<?= CHtml::link("valeurs actuelles", ['valeurs', 'id' => $attribut->id]) ?>
	pour cet attribut.
</p>
<?php } ?>

<?php
$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	[
		'id' => 'attribut-valeurs-grid',
		'dataProvider' => $logsProvider,
		'filter' => null, // null to disable
		'ajaxUpdate' => false,
		'columns' => [
			[
				'header' => "Attribut",
				'value' => function (\AttributImportLog $data) {
					static $names = null;
					if ($names === null) {
						$names = \components\SqlHelper::sqlToPairs("SELECT id, nom FROM Sourceattribut");
					}
					return $names[$data->sourceattributId] ?? '?';
				},
				'visible' => $attribut === null,
			],
			[
				'name' => 'fileName',
				'sortable' => false,
			],
			[
				'name' => 'url',
				'type' => 'url',
				'sortable' => false,
			],
			[
				'name' => 'logtime',
				'type' => 'datetime',
				'sortable' => false,
			],
			[
				'header' => 'Rapport',
				'type' => 'raw',
				'value' => function (\AttributImportLog $data) {
					return CHtml::link("…", ['report', 'id' => $data->id]);
				},
			],
		],
	]
);
