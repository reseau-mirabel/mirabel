<?php

/** @var Controller $this */
/** @var processes\attribut\Report $report */

assert($this instanceof Controller);
$attribut = Sourceattribut::model()->findByPk($report->config->sourceattributId);
assert($attribut instanceof Sourceattribut);

$this->pageTitle = "Revues inconnues de Mir@bel";
$this->breadcrumbs = [
	'Admin' => ['/admin'],
	'Attributs' => ['/attribut/declare'],
	"Revues à créer",
];
?>

<h1>
	<?= $this->pageTitle ?>
	<small>lors du dernier import de l'attribut <em><?= CHtml::encode($attribut->nom) ?></em></small>
</h1>

<p>
	La première ligne du fichier est supposée être un entête, et ignorée pour cette liste.
	Cette liste est aussi visible depuis la ligne de commande avec l'instruction
	<code>./yii attribut unknown --identifiant="<?= $attribut->identifiant ?>"</code>
</p>

<table class="table table-striped table-bordered table-condensed exportable">
	<thead>
		<tr>
			<th>n° de ligne</th>
			<th>Titre à la source</th>
			<th>Lien de création dans M</th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($report->unknownTitles as $position => [$issns, $titre]) {
			$link = "";
			if ($issns) {
				$url = Yii::app()->getParams()->itemAt('baseUrl') . "/titre/create-by-issn?issn=" . str_replace(" ", "+", $issns);
				$link = CHtml::link($issns, $url);
			}
			echo "<tr><td>$position</td><td>" . CHtml::encode($titre) . "</td><td>{$link}</td></tr>";
		}
		?>
	</tbody>
</table>
