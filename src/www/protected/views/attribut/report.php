<?php

/** @var Controller $this */
/** @var Sourceattribut $attribut */
/** @var AttributImportLog $log */
/** @var \processes\attribut\Report $report */

assert($this instanceof Controller);

$this->pageTitle = "Import dans l'attribut '{$attribut->nom}'";
$this->breadcrumbs = [
	'Admin' => ['/admin'],
	'Attributs' => ['/attribut/declare'],
	'Importer' => ['/attribut/import'],
	"Étape 3/3",
];
?>

<h1><?= $this->pageTitle ?></h1>

<div id="import-steps">
	Processus :
	<span><span class="badge">1</span> <a href="/attribut/import">Dépôt de fichier source</a></span>
	→ <span><span class="badge">2</span> <?= CHtml::link("Correspondance entre colonnes et attribut M", ['/attribut/import', 'etape' => 1, 'id' => $log->id]) ?></span>
	→ <span><span class="badge badge-info">3</span> Rapport sur l'import</span>
</div>


<section id="synthese" style="clear: right">
	<h2>Rapport sur l'import</h2>
	<ul>
		<li>
			Fichier <code><?= CHtml::encode($log->fileName) ?></code>
			<?php
			if ($log->url) {
				echo " téléchargé à l'URL " . CHtml::link(CHtml::encode($log->url), $log->url);
			}
			?>
		</li>
		<li>
			Colonne où est lue la valeur de l'attribut :
			<?php
			if ($report->config->colAttribut < 0) {
				echo "<strong>Aucune (l'attribut vaut 1 si le titre est présent)</strong>";
			} else {
				echo "<strong>", CHtml::encode($report->header[$report->config->colAttribut]), "</strong>",
					" (",
					CHtml::tag('abbr', ['title' => "Les colonnes sont numérotées de 0 à " . (count($report->header) - 1)], "n° {$report->config->colAttribut}"),
					")";
			}
			?>
		</li>
		<li>Colonnes d'où sont extraits les identifiants :
			<ul>
				<?php
				foreach ($report->config->colIdentifiers as $k => $v) {
					foreach ($v as $col) {
						echo "<li>$k <strong>" . CHtml::encode($report->header[$col]) . "</strong>"
							, " ("
							, CHtml::tag('abbr', ['title' => "Les colonnes sont numérotées de 0 à " . (count($report->header) - 1)], "n° $col")
							, ")"
							, "</li>";
					}
				}
				?>
			</ul>
		</li>
		<?php
		if ($report->config->filtre) {
			echo "<li>Filtre par valeur [<strong>" . CHtml::encode($report->config->filtre) . "</strong>]</li>";
		}
		?>

		<?php if (!empty($report->config)) { ?>
		<li>
			<div style="overflow-x: scroll;">
				Commande pour import non-web :
				<code>
					./yii attribut import
					--uri=<?= escapeshellarg($log->url ?: $log->fileName) ?>
					--config=<?= escapeshellarg(json_encode($report->config)) ?>
				</code>
			</div>
		</li>
		<?php } ?>
	</ul>

	<h3>Résultats</h3>
	<ul>
		<li>Import effectué le <b><?= date('Y-m-d, H:i:s', $log->logtime) ?></b>.</li>
		<li><?= $report->numRows ?> lignes lues avec ISSN et attribut.</li>
		<li><?= $report->numChanges ?> créations ou modifications de valeurs d'attributs dans Mir@bel.</li>
		<li><?= $report->numDeletes ?> suppressions de valeurs d'attributs dans Mir@bel.</li>
	</ul>
</section>

<?php if ($report->unknownTitles) { ?>
<section id="suppressions">
	<h3>Revues non identifiées dans Mir@bel</h3>
	<?= CHtml::link("Liste des " . count($report->unknownTitles) . " titres", ['/attribut/journal-creation', 'id' => $attribut->id]) ?>
	avec leurs liens de créations.
</section>
<?php } ?>

<section id="valeurs">
	<h3>
		Les 30 premières valeurs enregistrées
	</h3>
	<ul>
		<li>Télécharger le <?= CHtml::link("CSV de toutes les valeurs", ['import-csv', 'id' => $log->id]) ?> enregistrées par cet import</li>
		<li>
			Afficher <?= CHtml::link("toutes les valeurs actuelles", ['valeurs', 'id' => $log->sourceattributId]) ?>
			pour l'attribut <em><?= CHtml::encode($attribut->nom) ?></em>
			(y compris de précédents imports).
		</li>
	</ul>
	<table class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>TitreId</th>
				<th>Valeur</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$position = 0;
			foreach ($report->attributTitre as $row) {
				$position++;
				if ($position > 30) {
					break;
				}
				echo "<tr>"
				, "<td><a href=\"/titre/{$row[0]}\">{$row[0]}</a></td>"
				, "<td>" . CHtml::encode($row[1]) . "</td>"
				, "</tr>\n";
			}
			?>
		</tbody>
	</table>
</section>

<?php if ($report->numDeletes > 0) { ?>
<section id="suppressions">
	<h3>Suppressions</h3>
	<table class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>TitreId</th>
				<th>Valeur</th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach ($report->deletedAttributTitre as $row) {
				echo "<tr>"
				, "<td><a href=\"/titre/{$row[0]}\">{$row[0]}</a></td>"
				, "<td>" . CHtml::encode($row[1]) . "</td>"
				, "</tr>\n";
			}
			?>
		</tbody>
	</table>
</section>
<?php } ?>

<section id="remarques">
	<h3>Remarques sur l'import</h3>
	<ul>
		<?php
		foreach ($report->messages as $m) {
			echo "<li>$m</li>\n"; // raw HTML
		}
		?>
	</ul>
</section>
