<?php

/** @var Controller $this */
/** @var Sourceattribut[] $sattributs*/
/** @var array $numValeurs */

assert($this instanceof Controller);

$this->pageTitle = "Gestion des attributs de titres";

$this->breadcrumbs = [
	'Admin' => ['/admin'],
	"Gestion des attributs",
];
?>
<h1>
	<?= CHtml::encode($this->pageTitle) ?>
</h1>

<p>
	<?= CHtml::link("Importer un fichier-tableur…", ['import']) ?>
	<br />
	<?= CHtml::link("Historique des imports", ['historique']) ?>
</p>

<p>
	Le <em>nom</em> est purement indicatif, seul le champ <em>identifiant</em> permet d'importer des informations.
</p>

<p>
	Les identifiants suivants sont utilisés pour l'affichage :
	<code><?= join(", ", processes\attribut\DisplayedAttributes::NAMES) ?></code>
</p>

<div class="row-fluid">
	<div class="span3"><strong>Nom</strong></div>
	<div class="span2"><strong>Identifiant</strong></div>
	<div class="span3"><strong>Description</strong></div>
	<div class="span1"><strong>Sourcelien (optionnel)</strong></div>
	<div class="span1"><abbr title="visible par les utilisateurs authentifiés"><strong>Visible</strong></abbr></div>
</div>

<?php
foreach ($sattributs as $sattribut) {
	$protected = $sattribut->isProtected();
	?>
	<div class="controls-row" style="margin: 0 0 20px">
		<?php
		/** @var BootActiveForm $form */
		$form = $this->beginWidget(
			'bootstrap.widgets.BootActiveForm',
			[
				'enableAjaxValidation' => false,
				'enableClientValidation' => false,
				'type' => BootActiveForm::TYPE_INLINE,
				'htmlOptions' => [
					'id' => ($sattribut->id ? "attribut-update-{$sattribut->id}" : 'attribut-create'),
					'style' => 'margin:0',
				],
			]
		);
		?>
		<div class="span3">
			<?= $form->errorSummary($sattribut) ?>
			<?= $form->textField($sattribut, "nom", ['class' => 'span12', 'required' => true]) ?>
		</div>

		<?= $form->textField($sattribut, "identifiant", ['class' => 'span2', 'required' => true, 'readonly' => $protected]) ?>

		<?= $form->textArea($sattribut, "description", ['class' => 'span3']) ?>

		<?= $form->dropDownList(
			$sattribut,
			'sourcelienId',
			CHtml::listData(Sourcelien::model()->sorted()->findAll(), 'id', 'nom'),
			['class' => 'span1', 'empty' => '—']
		) ?>

		<?= $form->checkBox($sattribut, "visibilite", ['class' => 'span1']) ?>

		<div class="span1">
			<button type="submit" name="Sourceattribut[id]" value="<?= (string) $sattribut->id ?>" class="btn btn-primary">
				<?= $sattribut->id ? "Modifier" : "Créer" ?>
			</button>
			<div>
				<?php
				if ($sattribut->id && !empty($numValeurs[$sattribut->id])) {
					echo CHtml::link("{$numValeurs[$sattribut->id]} valeurs", ['valeurs', 'id' => $sattribut->id], ['style' => "white-space: pre;"]);
					echo "<br />";
				}
				if ($sattribut->id) {
					echo CHtml::link("Historique", ['historique', 'id' => $sattribut->id]);
				}
				?>
			</div>
		</div>
	<?php
	$this->endWidget(); // form
	?>
	<div class="span1">
		<?php
		if ($sattribut->id && !$protected) {
			echo \components\HtmlHelper::postButton(
				'<i class="icon-trash"></i>',
				['/attribut/delete', 'id' => $sattribut->id],
				[],
				[
					'class' => "delete btn btn-link",
					'title' => "Supprimer cet attribut",
					'onclick' => "return confirm('Voulez-vous supprimer définitivement cet attribut et les valeurs associées ?')",
				]
			);
		}
		?>
	</div>
</div>
<?php
}
