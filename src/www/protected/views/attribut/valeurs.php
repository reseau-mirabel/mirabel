<?php

/** @var Controller $this */
/** @var Sourceattribut $attribut */
/** @var array $valeurs */

assert($this instanceof Controller);

$this->pageTitle = "Valeurs de l'attribut {$attribut->nom}";
if (Yii::app()->user->checkAccess('admin')) {
	$this->breadcrumbs = [
		'Admin' => ['/admin'],
		'Attributs' => ['/attribut/declare'],
		"Valeurs",
	];
} else {
	$this->breadcrumbs = ["Valeurs d'attributs"];
}
?>

<h1><?= $this->pageTitle ?></h1>

<p>
	<?= count($valeurs) ?> enregistrements.
</p>

<table class="table table-striped table-bordered table-condensed exportable">
	<thead>
		<tr>
			<th>ID de titre</th>
			<th>Revue</th>
			<th>Valeur</th>
			<th>Der modif</th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($valeurs as $v) {
			$t = Titre::model()->populateRecord($v, false);
			echo "<tr>"
				, "<td><a href=\"/titre/{$t->id}\">{$t->id}</a></td>"
				, "<td>{$t->getSelfLink()}</td>"
				, "<td>", CHtml::encode($v['attrValeur']), "</td>"
				, "<td>", date("Y-m-d H:i", $v['attrHdate']), "</td>"
				, "</tr>";
		}
		?>
	</tbody>
</table>
