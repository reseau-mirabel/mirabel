<?php

/** @var Controller $this */
/** @var \processes\verification\Urls $process */
/** @var array $verifs */

assert($this instanceof Controller);

$this->pageTitle = "Vérification des liens d'éditeurs";
$this->breadcrumbs = [
	'Vérification' => ['/verification/index'],
	"Liens morts des textes rédigés",
];
?>
<?php $this->renderPartial('_nav-links') ?>

<h1>Vérification des liens dans les textes rédigés</h1>

<?= $this->renderPartial("_date-verif", $process->dates) ?>

<?php
if (!$verifs) {
	echo "<p>Aucune erreur.</p>";
	return;
}
?>

<ol>
	<?php
	$lastCms = null;
	foreach ($verifs as $row) {
		if ($lastCms === null || (int) $lastCms->id !== (int) $row['sourceId']) {
			if ($lastCms !== null) {
				echo "</ul>\n</li>";
			}
			$lastCms = \Cms::model()->findByPk((int) $row['sourceId']);
			echo "<li>"
				, CHtml::tag('strong', [], CHtml::link(CHtml::encode($lastCms->name), ['/cms/update', 'id' => $lastCms->id]))
				, '<ul>';
		}
		echo "<li>", CHtml::link(CHtml::encode($row['url']), $row['url'])
			, " ", CHtml::encode($row['msg'])
			, '</li>';
	}
	echo "</ul>\n</li>";
	?>
</ol>
