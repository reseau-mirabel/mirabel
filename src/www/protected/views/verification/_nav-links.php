<?php

/** @var Controller $this */
assert($this instanceof Controller);

$user = Yii::app()->user;
?>
<div class="pull-right">
	<ul class="well nav nav-list">
		<li class="nav-header">Pages de vérification</li>
		<?php
		$links = [
			["Accès et ressources", ['/verification/data']],
			["Autres liens", ['/verification/liens']],
			["Couvertures", ['/verification/couvertures']],
			["Éditeurs", ['/verification/editeurs']],
			["ISSN", ['/verification/issn']],
			["Liens morts / Revues", ['/verification/liensRevues']],
			["Liens morts / Éditeurs", ['/verification/liensEditeurs']],
			["Liens morts / Ressources", ['/verification/liensRessources']],
			$user->checkAccess('redaction') ? ["Liens morts / Rédactionnel", ['/verification/liensCms']] : null,
			["PPN et Sudoc", ['/verification/ppn']],
			["Revues", ['/verification/revues']],
			["Thématiques", ['/verification/index']],
			["Wikidata", ['/verification/wikidata']],
		];
		if ($user->checkAccess('wikidata/admin')) {
			$links[] = ["Wikidata / Incohérences", ['/verification/wikidatabugs']];
		}

		foreach ($links as $link) {
			if ($link === null) {
				continue;
			}
			$options = [];
			if ($link[1][0] === "/{$this->id}/{$this->action->id}") {
				$options['class'] = 'active';
			}
			echo CHtml::tag('li', $options, CHtml::link($link[0], $link[1]));
		}
		?>
	</ul>
</div>
