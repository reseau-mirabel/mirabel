<?php

use processes\verification\Editeurs;

/** @var Controller $this */
/** @var \processes\verification\Editeurs $verif */

$this->breadcrumbs = [
	'Vérification' => ['/verification/index'],
	'Éditeurs',
];
?>
<?php $this->renderPartial('_nav-links') ?>
<div style="float:right; max-width:30ex;">
	<ul class="well nav nav-list">
		<li class="nav-header">Navigation dans la page</li>
		<li><?= CHtml::link("Éditeurs sans pays", "#sans-pays") ?></li>
		<li><?= CHtml::link("Éditeurs sans IdRef", "#idref") ?></li>
		<li><?= CHtml::link("Éditeurs sans Sherpa", "#sherpa") ?></li>
		<li><?= CHtml::link("Rôle absent", "#relations") ?></li>
		<li><?= CHtml::link("Noms avec date", "#date-dans-nom") ?></li>
		<li><?= CHtml::link("Morts mais actuels", "#mort-mais-actuel") ?></li>
		<li><?= CHtml::link("Pays incohérents ISSN/éditeurs", "#pays-incoherents") ?></li>
	</ul>
</div>

<h1>Éditeurs</h1>

<section id="sans-pays">
	<h2>Éditeurs sans pays</h2>
	<?php
	if ($verif->sansPays) {
		echo "<ol>";
		foreach ($verif->sansPays as $e) {
			echo '<li>' . $e->getSelfLink() . '</li>';
		}
		echo "</ol>";
	} else {
		echo '<p>Tous les éditeurs de Mir@bel ont un pays assigné.</p>';
	}
	?>
</section>

<section id="idref">
	<h2>Éditeurs français sans IdRef</h2>
	<p>
		<?php
		if ($verif->sansIdref) {
			echo CHtml::link("{$verif->sansIdref} éditeurs français", ['/editeur/search', 'q' => ['idref' => '!', 'pays' => 'FRA']])
				. " n'ont pas d'identifiant IdRef.";
		} else {
			echo 'Tous les éditeurs français ont un identifiant IdRef.';
		}
		?>
	</p>
</section>

<section id="sherpa">
	<h2>Éditeurs français sans Open policy finder (Sherpa)</h2>
	<p>
		<?php
		if ($verif->sansIdref) {
			echo CHtml::link("{$verif->sansSherpa} éditeurs français", ['/editeur/search', 'q' => ['sherpa' => '!', 'pays' => 'FRA']])
				. " n'ont pas d'identifiant Open policy finder.";
		} else {
			echo 'Tous les éditeurs français ont un identifiant “Open policy finder”.';
		}
		?>
	</p>
</section>

<section id="relations">
	<h2>Éditeurs français sans rôle sur leurs titres</h2>
	<p>
		Ces éditeurs n'ont aucun rôle (ni direction et rédaction, ni publication et diffusion) pour au moins un titre.
	</p>
	<?php
	if ($verif->relationIncomplete) {
		?>
		<p>
			<strong><?= $verif->relationIncomplete ?></strong> éditeurs français concernés.
		</p>
		<h3>Liste de titres concernés (tronquée à 500)</h3>
		<ol>
			<?php
			foreach ($verif->titresRelationIncomplete as $t) {
				echo CHtml::tag('li', ['class' => $t->getSuiviType()], $t->getSelfLink());
			}
			?>
		</ol>
		<?php
	} else {
		echo '<p>Toutes les relations entre titres et éditeurs sont parfaitement déterminées.</p>';
	}
	?>
</section>

<section id="date-dans-nom">
	<h2>Éditeurs ayant possiblement une période d'activité dans leur nom</h2>
	<p>
		Date détectée sous la forme "1234)" en fin de nom.
	</p>
	<?php
	if ($verif->avecDateDansNom) {
		?>
		<ol>
			<?php
			foreach ($verif->avecDateDansNom as $e) {
				echo CHtml::tag('li', [], $e->getSelfLink());
			}
			?>
		</ol>
		<?php
	} else {
		echo "<p>Aucun éditeur n'a de date d'activité dans son nom.</p>";
	}
	?>
</section>

<section id="mort-mais-actuel">
	<h2>Éditeurs morts mais actuels</h2>
	<p>
		Éditeur mort (avec une date de fin)
		mais éditeur actuel (relation pas marquée "ancien") de titres vivants (sans date de fin).
	</p>
	<?= Editeurs::displayExporTable("verification_editeur_mort-mais-actuel", $verif->mortsMaisActuels) ?>
</section>

<section id="pays-incoherents">
	<h2>Pays incohérents entre ISSN et éditeurs</h2>
	<p>
		Le pays de l'ISSN n'est dans aucun des éditeurs (y compris ZZ).
		Les codes de pays de publication sont triés par papier puis électronique, et fusionnés si identiques.
		"?" désigne un ISSN sans pays.
	</p>
	<?= Editeurs::displayExporTable("verification_editeur_pays_incoherents", $verif->paysIncoherents) ?>
</section>
