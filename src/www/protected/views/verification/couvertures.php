<?php

/** @var Controller $this */
/** @var processes\verification\Couvertures $verif */

assert($this instanceof Controller);

$this->breadcrumbs = [
	'Vérification' => ['/verification/index'],
	'Couvertures de titres',
];
?>
<?php $this->renderPartial('_nav-links') ?>
<div style="float:right; max-width:30ex;">
	<ul class="well nav nav-list">
		<li class="nav-header">Navigation dans la page</li>
		<li><?= CHtml::link("Couvertures absentes malgré leur URL présente", "#couvertures-absentes") ?></li>
		<li><?= CHtml::link("Revues dont le dernier titre n'a pas d'image de couverture", "#titres-sans-couvertures") ?></li>
	</ul>
</div>

<h1>Couvertures de titres</h1>

<div style="clear: both"></div>

<section id="couvertures-absentes">
	<h2>Couvertures absentes malgré leur URL présente</h2>
	<p>La cause est probablement une erreur au téléchargement (mauvaise URL, réseau indisponible, mécanisme anti-téléchargement…).</p>
	<table class="table table-striped table-bordered table-condensed exportable">
		<thead>
			<tr>
				<th>ID</th>
				<th>Titre</th>
				<th>URL</th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach ($verif->titresSansCouvertureAvecUrl as $t) {
				assert(count($t) === 4);
				echo "<tr class=\"{$t[0]}\">"
					, "<td style=\"vertical-align: middle\">{$t[1]}</td>"
					, "<td style=\"max-width: 20ex; overflow-x: ellipsis;\">{$t[2]}</td>"
					, "<td>{$t[3]}</td>"
					, "</tr>\n";
			}
			?>
		</tbody>
	</table>
</section>

<section id="titres-sans-couvertures">
	<h2>Revues dont le dernier titre n'a pas d'image de couverture</h2>
	<?php
	if (empty($verif->titresSansCouverture)) {
		echo "<p>Aucune revue.</p>";
	} else {
		?>
	<ol>
		<?php
		foreach ($verif->titresSansCouverture as $titre) {
			echo sprintf('<li class="%s">', $titre->getSuiviType())
				. $titre->getSelfLink()
				. ($titre->dateFin ? ' <span class="label label-info" title="Le dernier titre de cette revue a une date de fin.">†</span>' : '')
				. "</li>";
		} ?>
	</ol>
	<?php
	}
	?>
</section>
