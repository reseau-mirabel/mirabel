<?php
use models\wikidata\Compare;

/** @var Controller $this */
/** @var Wikidata $model */
assert($this instanceof Controller);

$this->pageTitle = "Vérification Incohérences wikidata";
$this->breadcrumbs = [
	'Vérification' => ['/verification/index'],
	'Wikidatabugs',
];
$this->renderPartial('_nav-links');
?>

<h1>
	Incohérences Wikidata / Mir@bel
</h1>

<table class="table table-striped table-bordered table-condensed plaincolors">
	<thead>
		<tr>
			<th>Incohérences ?</th>
			<th>Enregistrements</th>
			<th>Revues</th>
			<th>Titres</th>
			<th>QIDs</th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach (\Wikidata::getComparisonStatistics() as $row) {
			echo '<tr class="' . $row[5] . '">';
			echo "<td>" . join('</td> <td>', array_slice($row, 0, 5)) . "</td>\n";
			echo "</tr>\n";
		}
		?>
	</tbody>
</table>

<?php
echo CHtml::link(
	'Exporter',
	['/verification/wikidatabugs', 'export' => 1, 'Wikidata' => array_filter($model->getAttributes())],
	['class' => 'btn btn-primary btn-small']
);

$this->widget('ext.bootstrap.widgets.BootGridView', [
	'id' => 'wikidatabugs-grid',
	'itemsCssClass' => 'table items exportable',
	'ajaxUpdate' => false,
	'dataProvider' => $model->searchInconsistencies(),
	'filter' => $model,
	'rowCssClassExpression' => function (int $line, \Wikidata $data) {
		return $data->getComparisonLevel();
	},
	'columns' => [
		[
			'header' => 'Revue',
			'name' => 'revueLink',
			'type' => 'raw',
			'filter' => false,
		],
		[
			'header' => 'Page Wikidata',
			'name' => 'qidLink',
			'type' => 'raw',
			'filter' => false,
		],
		[
			'header' => 'ISSN',
			'name' => 'value',
		],
		[
			'name' => 'comparison',
			'value' => function (\Wikidata $data) {
				return $data->getComparisonText();
			},
			'type' => 'raw',
			'filter' => \Wikidata::getComparisonDDL(true),
		],
		'compDetails',
		[
			'class' => \BootButtonColumn::class,
			'template' => '{update}',
			'buttons'=> [
				'update' => [
					'label' => 'Modifier le titre',
					'options' => ['target' => '_blank'],
					'url' => function (\Wikidata $data) {
						return Yii::app()->createUrl('titre/update', ['id' => $data->titreId, '#' => 'titre-liens']);
					},
					'visible' => fn (int $rank, \Wikidata $data) => $data->isEditable(),
				],
			],
		],
	],
]);
