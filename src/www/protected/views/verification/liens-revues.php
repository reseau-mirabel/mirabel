<?php

/** @var Controller $this */
/** @var \processes\verification\Urls $process */

assert($this instanceof Controller);

$this->pageTitle = "Vérification des liens de revues";
$this->breadcrumbs = [
	'Vérification' => ['/verification/index'],
	'Liens morts de revues',
];
?>
<?php $this->renderPartial('_nav-links') ?>

<h1>Vérification des liens de revues</h1>

<?= $this->renderPartial("_date-verif", $process->dates) ?>

<?php
$skipped = array_map('trim', explode("\n", (string) Config::read('validate.skip.domains')));
$skiplinks = [];
foreach ($skipped as $skip) {
	$skiplinks[] = CHtml::link("<code>$skip</code>", ['/stats/liens', 'domain' => $skip]);
}
?>
<p class="alert alert-warning">
	Les liens vers <?= implode(', ', $skiplinks) ?>
	et <code>worldcat.org</code> (lien induit par le numéro OCN Worldcat)
	sont exclus de cette vérification,
	parce que ces sites web refusent les requêtes automatiques.
</p>

<?php
$form = $this->beginWidget(
	'BootActiveForm',
	[
		'action' => '/verification/liensRevues',
		'id' => 'filtre-verifurl-form',
		'type' => 'horizontal',
		'enableClientValidation' => false,
	]
);
/** @var BootActiveForm $form */

?>
<fieldset>
	<legend>Filtres</legend>
	<?= $form->errorSummary($process->input); ?>

	<?php
	if ($process->input->grappe > 0) {
		$grappe = Grappe::model()->findByPk($process->input->grappe);
			echo $form->checkBoxRow(
				$process->input,
				'grappe',
				['value' => $process->input->grappe, 'label' => "Grappe <em>" . CHtml::encode($grappe->nom) . "</em>"]
			);
	}

	// Possessions :
	//   si le champ contient un ID de partenaire différent de celui de la session,
	//   l'affichage est différent.
	/** @var ?Partenaire $partenaire */
	$partenaire = null;
	if ($process->input->possessions > 0) {
		$partenaire = Partenaire::model()->findByPk($process->input->possessions);
	}
	$isMyOrg = ($partenaire === null || (int) $partenaire->id === (int) Yii::app()->user->partenaireId);
	$optionsPossessions = [];
	if ($isMyOrg) {
		$hasPossessions = (bool) \Yii::app()->db
			->createCommand("SELECT 1 FROM Partenaire_Titre WHERE partenaireId = " . Yii::app()->user->partenaireId)
			->queryScalar();
		$optionsPossessions = [
			'disabled' => !$hasPossessions,
			'label' => "Revues que mon partenaire possède",
			'value' => Yii::app()->user->partenaireId,
		];
	} else {
		assert($partenaire instanceof Partenaire);
		$optionsPossessions = [
			'label' => "Revues dans les possessions de <em>" . CHtml::encode($partenaire->getShortName()) . "</em>",
			'value' => $process->input->possessions,
		];
	}
	if (empty($optionsPossessions['disabled'])) {
		echo $form->checkBoxRow($process->input, 'possessions', $optionsPossessions);
	}

	$suiviPartenaireId = $process->input->suiviPar ?: Yii::app()->user->partenaireId;
	echo $form->checkBoxRow(
		$process->input,
		'suiviPar',
		[
			'value' => $suiviPartenaireId,
			'label' => ($suiviPartenaireId == Yii::app()->user->partenaireId ?
				"Revues que mon partenaire suit"
				: "Revues que <em>" . CHtml::encode(Partenaire::model()->findByPk($suiviPartenaireId)->nom) . "</em> suit"),
		]
	);

	echo $form->checkBoxRow($process->input, 'nonSuivi');
	echo $form->dropDownListRow(
		$process->input,
		'accesIntegralLibre',
		[
			"1" => "requis",
			"0" => "indifférents",
			"-1" => "exclus",
		],
		[
			'labelOptions' => ['label' => "Accès libres en texte intégral"],
		]
	);
	echo $form->textFieldRow($process->input, 'msgContient', ['class' => "input-block-level"]);
	echo $form->textFieldRow($process->input, 'msgExclut', ['class' => "input-block-level"]);
	echo $form->textFieldRow($process->input, 'urlContient', ['class' => "input-block-level"]);
	echo $form->textFieldRow($process->input, 'urlExclut', ['class' => "input-block-level"]);
	?>
	<div class="control-group">
		<?php echo CHtml::submitButton('Filtrer'); ?>
	</div>
</fieldset>
<?php $this->endWidget(); ?>

<?php
$columns = [
	[
		'header' => '',
		'value' => function () {
			static $k = 1;
			return $k++;
		},
		'htmlOptions' => ['style' => "vertical-align: middle; text-align: right;"],
	],
	[
		'name' => 'id',
		'headerHtmlOptions' => ['class' => 'sorted ascend'],
	],
	[
		'name' => 'idRevue',
		'type' => 'raw',
		'value' => function ($x) {
			return CHtml::link(
				$x['sourceId'],
				['/revue/view', 'id' => $x['sourceId']]
			);
		},
	],
	[
		'name' => 'titre',
		'type' => 'raw',
		'value' => function ($x) {
			return CHtml::link(
				\Norm::shortenText($x['titre'], 40),
				['/revue/view', 'id' => $x['sourceId']]
			);
		},
	],
	[
		'name' => 'url',
		'type' => 'raw',
		'value' => function ($x) {
			return CHtml::link(\Norm::shortenText($x['url'], 40), $x['url']);
		},
	],
	[
		'name' => 'msg',
		'header' => 'Message',
	],
	[
		'header' => 'Suivi',
		'type' => 'text',
		'value' => function ($x) use ($process) {
			return $process->getSuivi((int) $x['sourceId']);
		},
	],
	[
		'header' => 'Der. vérif.',
		'type' => 'text',
		'value' => function ($x) {
			return $x['hdateVerif'] ? date('Y-m-d', $x['hdateVerif']) : '';
		},
	],
];

$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	[
		'id' => 'verification-liens-grid',
		'dataProvider' => $process->provider,
		'filter' => null,
		'columns' => $columns,
		'ajaxUpdate' => false,
		'rowCssClassExpression' => function ($line, $data) {
			$t = new \Titre();
			$t->setAttributes($data, false);
			$class = $t->getSuiviType();
			return ($class === 'suivi-none' ? '' : $class);
		},
		'itemsCssClass' => 'table-condensed sortable exportable',
	]
);
?>
