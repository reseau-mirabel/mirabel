<?php

use components\HtmlHelper;
use components\HtmlTable;

/** @var Controller $this */
/** @var processes\verification\TitreAccesDeborde $data */

$this->pageTitle = "Titres dont des accès débordent";
$this->breadcrumbs = [
	'Vérification des données' => ['/verification/index'],
	$this->pageTitle,
];
?>

<h1><?= $this->pageTitle ?></h1>

<div style="clear: right;"></div>

<?php foreach ($data->byResource as $id => $byResource) { ?>
<section id="ressource-<?= $id ?>">
	<details>
		<summary class="contains-block">
			<h2>
				<?= CHtml::encode($byResource['ressource']) ?>
				<small><?= count($byResource['table']) ?> accès</small>
			</h2>
		</summary>
		<div>
			<table class="table table-condensed table-hover exportable" data-exportable-filename="<?= HtmlHelper::exportableName("Ressource-{$id}_titres-acces-deborde") ?>">
				<thead>
					<tr>
						<th>ID accès</th>
						<th>écart<br>(années)</th>
						<th>ID revue</th>
						<th>Titre</th>
						<th>Collections</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$previous = "";
					foreach ($byResource['table'] as $row) {
						if ($row['titreId'] === $previous) {
							$titre = "";
						} else {
							$titre = $row['titre'];
						}
						$previous = $row['titreId'];
						echo '<tr>',
							'<td>', CHtml::link($row['serviceId'], ['/service/update', 'id' => $row['serviceId']]), '</td>',
							'<td>', $row['ecart'], '</td>',
							'<td>', $row['revueId'], '</td>',
							'<td>', $titre, '</td>',
							'<td>', $row['collections'], '</td>',
							'</tr>';
					}
					?>
				</tbody>
			</table>
		</div>
	</details>
</section>
<?php } ?>

<section>
	<details>
		<summary class="contains-block">
			<h2>
				Ressources avec un seul cas
				<small><?= count($data->singleErrors) ?> ressources</small>
			</h2>
		</summary>
		<div>
			<?php
			$table = HtmlTable::build(
				$data->singleErrors,
				["Ressource", "ID titre", "ID revue", "Titre", "Accès débordant", "Collections"]
			);
			$table->addClass('exportable');
			$table->htmlAttributes['data-exportable-filename'] = HtmlHelper::exportableName("Ressources_1-titre-acces-deborde");
			echo $table->toHtml();
			?>
		</div>
	</details>
</section>

