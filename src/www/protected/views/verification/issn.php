<?php

/** @var Controller $this */
/** @var \processes\verification\Issn $verif */

use processes\verification\Issn;

assert($this instanceof Controller);

$this->pageTitle = "Vérification des ISSN";
$this->breadcrumbs = [
	'Vérification' => ['/verification/index'],
	"Vérifications d'ISSN",
];

$this->renderPartial('_nav-links')
?>
<div class="controleur-verification">
<div style="float:right; max-width:30ex;">
	<ul class="well nav nav-list">
		<li class="nav-header">Navigation dans la page</li>
		<li><?= CHtml::link("ISSN-E sans <em>texte intégral</em>", "#issne-sans-integral") ?></li>
		<li><?= CHtml::link("Sans ISSN-E malgré <em>texte intégral</em>", "#integral-sans-issne") ?></li>
		<li><?= CHtml::link("ISSN sans ISSN-L", "#issn-sans-issnl") ?></li>
		<li><?= CHtml::link("ISSN-L sans ISSN", "#issnl-sans-issn") ?></li>
		<li><?= CHtml::link("ISSN-L incohérent", "#issnl-autre-revue") ?></li>
		<li><?= CHtml::link("ISSN-L non ISSN-(P|E)", "#issnl-non-issn") ?></li>
		<li><?= CHtml::link("Doublons d'ISSN-E", "#issne-doublon") ?></li>
		<li><?= CHtml::link("Support d'ISSN inconnu", "#issn-support-inconnu") ?></li>
		<li><?= CHtml::link("ISSN-L conflictuels", "#issnl-conflictuels") ?></li>
		<li><?= CHtml::link("ISSN-L partagés", "#issnl-partages") ?></li>
		<li><?= CHtml::link("Titres à multiples ISSN-P", "#issnp-multiples") ?></li>
		<li><?= CHtml::link("Titres à multiples ISSN-E", "#issne-multiples") ?></li>
		<li><?= CHtml::link("Dates discordantes", "#dates-discordantes") ?></li>
		<li><?= CHtml::link("Titres sans ISSN", "#sans-issn") ?></li>
		<li><?= CHtml::link("Titres avec un ISSN en cours", "#issn-encours") ?></li>
	</ul>
</div>

<h1>Vérifications d'ISSN</h1>

<p>
	Ces alertes ne signalent pas forcément une erreur.
	Elles permettent de faciliter les vérifications et corrections.
</p>

<section id="issne-sans-integral" style="clear: both">
	<h2>Revues avec un ISSN-E mais sans accès <em>texte intégral</em></h2>
	<p>
		Un des titres de la revue a un ISSN-E,
		mais aucun titre n'a d'accès en <em>texte intégral</em>.
	</p>
	<?= Issn::displayExporTable("verifications_titres-issne-sans-integral", $verif->titresIssneSansAcces) ?>
</section>

<section id="integral-sans-issne">
	<h2>Titre sans ISSN-E mais avec un accès au <em>texte intégral</em></h2>
	<p>
		Le titre possède au moins un accès en <em>texte intégral</em> complet (non restreint à une sélection d'article).
		Pourtant aucun ISSN-E n'est déclaré sur ce titre.
	</p>
	<?= Issn::displayExporTable("verifications_titres-integral-sans-issne", $verif->titresIntegralSansIssne) ?>
</section>

<section id="issn-sans-issnl">
	<h2>ISSN sans ISSN-L</h2>
	<p>
		Un titre de revue a un ISSN qui n'est pas associé à un ISSN-L.
	</p>
	<?= Issn::displayExporTable("verifications_titres-issn-sans-issnl", $verif->issnSansIssnl) ?>
</section>

<section id="issnl-sans-issn">
	<h2>ISSN-L sans ISSN</h2>
	<p>
		Un titre de revue a un ISSN-L qui n'est pas associé à un ISSN.
	</p>
	<?= Issn::displayExporTable("verifications_titres-issnl-sans-issn", $verif->issnlSansIssn) ?>
</section>

<section id="issnl-autre-revue">
	<h2>ISSN-L incohérent</h2>
	<p>
		ISSN-L appartient à une autre revue en tant qu'ISSN-(P|E).
	</p>
	<?= Issn::displayExporTable("verifications_titres-issnl-autre-revue", $verif->issnlAutreRevue) ?>
</section>

<section id="issnl-non-issn">
	<h2>ISSN-L absent en tant qu'ISSN-(P|E)</h2>
	<p>
		Un des titres de la revue a un ISSN-L qui n'est pas présent sous forme d'ISSN-P ou d'ISSN-E,
		dans aucun des titres (ni de la même revue ni des autres revues).
	</p>
	<?= Issn::displayExporTable("verifications_titres-issnl-non-issn", $verif->issnlAbsents) ?>
</section>


<section id="issne-doublon">
	<h2>Titres électroniques à doublons d'ISSN-E</h2>
	<p>
		Le dernier titre de ces revues est électronique,
		et l'ISSN-E associé se retrouve aussi dans de précédents titres.
	</p>
	<?= Issn::displayExporTable("verifications_titres-issne-doublon", $verif->titresElectroMultiIssne) ?>
</section>

<section id="issn-support-inconnu">
	<h2>Support d'ISSN inconnu</h2>
	<?= Issn::displayExporTable("verifications_titres-issn-support-inconnu", $verif->supportInconnu) ?>
</section>

<section id="issnl-conflictuels">
	<h2>ISSN-L conflictuels</h2>
	<p>
		Un même titre a plusieurs ISSN-L distincts.
	</p>
	<?= Issn::displayExporTable("verifications_issnl-conflictuels", $verif->conflictingIssnl) ?>
</section>

<section id="issnl-partages">
	<h2>ISSN-L partagés</h2>
	<p>
		Au moins deux titres de ces revues ont le même ISSN-L.
	</p>
	<?= Issn::displayExporTable("verifications_issnl-partages", $verif->sharedIssnl) ?>
</section>

<section id="issnp-multiples">
	<h2>Titres à multiples ISSN-P</h2>
	<p>
		Chacun de ces titres a plusieurs ISSN-Papier distincts et valides.
		Le premier de ces ISSN est cité à côté du lien vers la revue.
	</p>
	<?= Issn::displayExporTable("verifications_issnp-multiples", $verif->multipleIssnp) ?>
</section>

<section id="issne-multiples">
	<h2>Titres à multiples ISSN-E</h2>
	<p>
		Chacun de ces titres a plusieurs ISSN-électroniques distincts et valides.
		Le premier de ces ISSN est cité à côté du lien vers la revue.
	</p>
	<?= Issn::displayExporTable("verifications_issne-multiples", $verif->multipleIssne) ?>
</section>

<section id="dates-discordantes">
	<h2>Dates discordantes entre titre et ISSN</h2>
	<p>
		Les dates approximatives ("19XX", "201.", etc) ou non-remplies dans les ISSN sont ignorées.
	</p>
	<?= Issn::displayExporTable("verifications_dates-discordantes", $verif->discordingDates) ?>
</section>

<section id="sans-issn">
	<h2>Titres sans ISSN</h2>
	<?= Issn::displayExporTable("verifications_titres-sans-issn", $verif->sansIssn) ?>
</section>

<section id="issn-encours">
	<h2>Titres avec un ISSN en cours</h2>
	<?= Issn::displayExporTable("verifications_titres-issn-encours", $verif->issnEncours) ?>
</section>

</div>
