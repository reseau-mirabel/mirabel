<?php

/** @var Controller $this */
/** @var \processes\verification\Liens $model */
assert($this instanceof Controller);

$this->pageTitle = "Vérification de liens";
$this->breadcrumbs = [
	'Vérification' => ['/verification/index'],
	'Autres liens',
];
?>
<?php $this->renderPartial('_nav-links') ?>
<div style="float:right; max-width:30ex;">
	<ul class="well nav nav-list">
		<li class="nav-header">Navigation locale</li>
		<li><?= CHtml::link("Nom différent de la source", '#noms-faux') ?></li>
		<li><?= CHtml::link("URL hors cadre", '#urls-invalides') ?></li>
	</ul>
</div>

<h1>Vérifications des autres liens</h1>

<section id="noms-faux" style="clear: right">
	<h2>Liens dont le nom diffère de la source</h2>

	<h3>Revues</h3>
	<?php
	if (empty($model->nomsFauxTitres)) {
		echo "<p>Aucune revue.</p>";
	} else {
		?>
	<table class="table table-condensed">
		<thead>
			<tr>
				<th>Titre</th>
				<th>Nom de l'URL</th>
				<th>Nom de la source</th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach ($model->nomsFauxTitres as $row) {
				$titre = Titre::model()->populateRecord($row);
				echo "<tr>"
					. "<td>{$titre->getSelfLink()}</td>"
					. "<td>{$row['linkName']}</td>"
					. "<td>{$row['sourceName']}</td>"
					. "</tr>";
			} ?>
		</tbody>
	</table>
	<?php
	}
	?>

	<h3>Éditeur</h3>
	<?php
	if (empty($model->nomsFauxEditeurs)) {
		echo "<p>Aucun éditeur.</p>";
	} else {
		?>
	<table class="table table-condensed">
		<thead>
			<tr>
				<th>Éditeur</th>
				<th>Nom de l'URL</th>
				<th>Nom de la source</th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach ($model->nomsFauxEditeurs as $row) {
				$editeur = Editeur::model()->populateRecord($row);
				echo "<tr>"
					. "<td>{$editeur->getSelfLink()}</td>"
					. "<td>{$row['linkName']}</td>"
					. "<td>{$row['sourceName']}</td>"
					. "</tr>";
			} ?>
		</tbody>
	</table>
	<?php
	}
	?>
</section>

<section id="urls-invalides" style="margin-top: 2em">
	<h2>Liens où l'URL n'est pas du format attendu</h2>
	<p>
		Certaines sources sont liées à une vérification de l'URL par expression régulière (par exemple <code>^https://.+\.pinterest\.(com|fr)/</code>).
		Pour les autres liens ci-dessous, l'URL ne correspond pas à l'expression présente dans la source.
		Pour les sources qui n'ont pas d'expression régulière pour valider l'URL,
		on vérifie simplement que l'URL contient bien le domaine déclaré par la source.
	</p>
	<p>
		Dans les URL, le domaine correct est en gras.
		Au survol de la colonne "Source", l'expression validatrice (domaine ou expression régulière) s'affiche.
	</p>

	<h3>Titre</h3>
	<?php
	if (empty($model->urlsInvalidesTitres)) {
		echo "<p>Aucune revue.</p>";
	} else {
		?>
	<table class="table table-condensed">
		<thead>
			<tr>
				<th>Titre</th>
				<th>Source</th>
				<th>Domaine de l'URL</th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach ($model->urlsInvalidesTitres as $row) {
				$titre = Titre::model()->populateRecord($row);
				$url = strlen($row['linkUrl']) > 60
					? CHtml::tag('span', ['title' => $row['linkUrl']], CHtml::encode(substr($row['linkUrl'], 0, 60)) . '&hellip;')
					: CHtml::encode($row['linkUrl']);
				$domain = $model->domains[$row['sourceName']] ?? '';
				if ($domain) {
					$url = str_replace("$domain/", "<b>$domain</b>/", $url);
				}
				echo "<tr>"
					. "<td>{$titre->getSelfLink()}</td>"
					. "<td title=\"" . CHtml::encode($model->regexp[$row['sourceName']] ?? $domain) . "\">{$row['sourceName']}</td>"
					. "<td>{$url}</td>"
					. "</tr>";
			} ?>
		</tbody>
	</table>
	<?php
	}
	?>

	<h3>Éditeur</h3>
	<?php
	if (empty($model->urlsInvalidesEditeurs)) {
		echo "<p>Aucun éditeur.</p>";
	} else {
		?>
	<table class="table table-condensed">
		<thead>
			<tr>
				<th>Éditeur</th>
				<th>Source</th>
				<th>URL</th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach ($model->urlsInvalidesEditeurs as $row) {
				$editeur = Editeur::model()->populateRecord($row);
				$url = strlen($row['linkUrl']) > 60
					? CHtml::tag('span', ['title' => $row['linkUrl']], CHtml::encode(substr($row['linkUrl'], 0, 60)) . '&hellip;')
					: CHtml::encode($row['linkUrl']);
				$domain = $model->domains[$row['sourceName']] ?? '';
				if ($domain) {
					$url = str_replace("$domain/", "<b>$domain</b>/", $url);
				}
				echo "<tr>"
					. "<td>{$editeur->getSelfLink(Editeur::NAME_LONG)}</td>"
					. "<td title=\"" . CHtml::encode($model->regexp[$row['sourceName']] ?? $domain) . "\">{$row['sourceName']}</td>"
					. "<td>{$url}</td>"
					. "</tr>";
			} ?>
		</tbody>
	</table>
	<?php
	}
	?>
</section>
