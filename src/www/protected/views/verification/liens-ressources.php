<?php

/** @var Controller $this */
/** @var \processes\verification\Urls $process */

assert($this instanceof Controller);

$this->pageTitle = "Vérification des liens de ressources";
$this->breadcrumbs = [
	'Vérification' => ['/verification/index'],
	'Liens morts de ressources',
];
?>
<?php $this->renderPartial('_nav-links') ?>

<h1>Vérification des liens de ressources</h1>

<?= $this->renderPartial("_date-verif", $process->dates) ?>

<?php
$skipped = array_map('trim', explode("\n", (string) Config::read('validate.skip.domains')));
$skiplinks = [];
foreach ($skipped as $skip) {
	$skiplinks[] = CHtml::link("<code>$skip</code>", ['/stats/liens', 'domain' => $skip]);
}
?>
<p class="alert alert-warning">
	Les liens vers <?= implode(', ', $skiplinks) ?>
	sont exclus de cette vérification,
	parce que ces sites web refusent les requêtes automatiques.
</p>

<?php
$form = $this->beginWidget(
	'BootActiveForm',
	[
		'action' => '/verification/liensRessources',
		'id' => 'filtre-verifurl-form',
		'type' => 'horizontal',
		'enableClientValidation' => false,
	]
);
/** @var BootActiveForm $form */
?>
<fieldset>
	<legend>Limiter par type d'erreur</legend>
	<?= $form->errorSummary($process->input); ?>

	<?php
	$suiviPartenaireId = (int) ($process->input->suiviPar ?: Yii::app()->user->partenaireId);

	echo $form->checkBoxRow(
		$process->input,
		'suiviPar',
		[
			'value' => $suiviPartenaireId,
			'label' => ($suiviPartenaireId == Yii::app()->user->partenaireId ?
				"Ressources que mon partenaire suit"
				: "Ressources que <em>" . CHtml::encode(Partenaire::model()->findByPk($suiviPartenaireId)->nom) . "</em> suit"),
		]
	);
	echo $form->textFieldRow($process->input, 'msgContient', ['class' => "input-block-level"]);
	echo $form->textFieldRow($process->input, 'msgExclut', ['class' => "input-block-level"]);
	echo $form->textFieldRow($process->input, 'urlContient', ['class' => "input-block-level"]);
	echo $form->textFieldRow($process->input, 'urlExclut', ['class' => "input-block-level"]);
	?>
	<div class="control-group">
		<?= CHtml::submitButton('Filtrer') ?>
	</div>
</fieldset>
<?php $this->endWidget(); ?>

<?php
$columns = [
	[
		'header' => '',
		'value' => function () {
			static $k = 1;
			return $k++;
		},
	],
	[
		'name' => 'id',
		'headerHtmlOptions' => ['class' => 'sorted ascend'],
	],
	[
		'name' => 'idRessource',
		'type' => 'raw',
		'value' => function (array $x): string {
			return CHtml::link(
				$x['sourceId'],
				['/ressource/view', 'id' => $x['sourceId']]
			);
		},
	],
	[
		'name' => 'ressource',
		'type' => 'raw',
		'value' => function (array $x): string {
			return CHtml::link(
				\Norm::shortenText($x['ressource'], 40),
				['/ressource/view', 'id' => $x['sourceId']]
			);
		},
	],
	[
		'name' => 'url',
		'type' => 'raw',
		'value' => function (array $x): string {
			return CHtml::link(\Norm::shortenText($x['url'], 40), $x['url']);
		},
	],
	[
		'name' => 'msg',
		'header' => 'Message',
	],
	[
		'header' => 'Suivi',
		'type' => 'text',
		'value' => function (array $x) use ($process) {
			return $process->getSuivi((int) $x['sourceId']);
		},
	],
	[
		'header' => 'Der. vérif.',
		'type' => 'text',
		'value' => function ($x) {
			return $x['hdateVerif'] ? date('Y-m-d', $x['hdateVerif']) : '';
		},
	],
];

$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	[
		'id' => 'verification-liens-grid',
		'dataProvider' => $process->provider,
		'filter' => null,
		'columns' => $columns,
		'ajaxUpdate' => false,
		'rowCssClassExpression' => function (int $rowNum, array $data): string {
			if (empty($data['sourceId'])) {
				return '';
			}
			return (Yii::app()->user->isMonitoring(['model' => 'Ressource', 'id' => $data['sourceId']]) ? 'suivi-self' : '');
		},
		'itemsCssClass' => 'table-condensed sortable exportable',
	]
);
?>
