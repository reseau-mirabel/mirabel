<?php

/** @var Controller $this */
/** @var processes\verification\General $verif */

use components\HtmlTable;

assert($this instanceof Controller);

$this->breadcrumbs = [
	'Vérification' => ['/verification/index'],
	'Vérifications générales',
];
?>
<?php $this->renderPartial('_nav-links') ?>
<div style="float:right; max-width:30ex;">
	<ul class="well nav nav-list">
		<li class="nav-header">Navigation dans la page</li>
		<li><?= CHtml::link("Ressources sans accès", "#ressource-errors") ?></li>
		<li><?= CHtml::link("Collections sans accès", "#collections-sans-acces") ?></li>
		<li><?= CHtml::link("Collections temporaires", "#collections-temporaires") ?></li>
		<li><?= CHtml::link("Doublons (titre, ressource, ID)", "#doublons") ?></li>
		<li><?= CHtml::link("Accès libre+intégral absent", "#acces-libre-integral") ?></li>
	</ul>
</div>

<h1>Vérifications générales</h1>

<section id="ressource-errors">
	<h2>Ressources sans accès</h2>
	<?php
	if ($verif->ressources) {
		echo "<ol>";
		foreach ($verif->ressources as $r) {
			/** @var Ressource $r */
			echo sprintf('<li class="%s">%s</li>', $r->getSuiviType(), $r->getSelfLink());
		}
		echo "</ol>";
	} else {
		echo '<p>Toutes les ressources ont au moins un accès en ligne.</p>';
	}
	?>
</section>

<section id="collections-sans-acces">
	<h2>Collections sans accès</h2>
	<?php
	if ($verif->collectionsSansAcces) {
		echo "<ol>";
		foreach ($verif->collectionsSansAcces as $c) {
			echo '<li>'
				. CHtml::link(CHtml::encode($c['nom']), ['/ressource/view', 'id' => $c['id']])
				. " "
				. CHtml::encode($c['collection'])
				. '</li>';
		}
		echo "</ol>";
	} else {
		echo '<p>Toutes les collections ont au moins un accès en ligne.</p>';
	}
	?>
</section>

<section id="collections-temporaires">
	<h2>Collections temporaires</h2>
	<?php
	if ($verif->collectionsTemporaires) {
		echo "<ol>";
		foreach ($verif->collectionsTemporaires as $c) {
			/** @var Collection $c */
			echo sprintf('<li class="%s">', $c->ressource->getSuiviType())
				. $c->getSelfLink(false)
				. " dans la ressource "
				. $c->ressource->getSelfLink()
				. '</li>';
		}
		echo "</ol>";
	} else {
		echo "<p>Il n'y a pas de collection temporaire.</p>";
	}
	?>
</section>

<section id="doublons">
	<h2>Doublons sur les triplets (titre, ressource, ID interne)</h2>
	<?php
	$table2 = new HtmlTable(
		array_map(
			function ($t) {
				$links = array_map(
					function ($id) {
						return CHtml::link($id, ['/titre/identification', 'id' => $id]);
					},
					explode(',', $t["titreIds"])
				);
				return [
					join(' / ', $links),
					$t["titres"],
					CHtml::link($t["ressource"], ['/ressource/view', 'id' => $t["ressourceId"]]),
					$t["idInterne"],
				];
			},
			$verif->doublonsIdentification
		),
		["ID titres", "Titres", "Ressource", "ID interne"]
	);
	echo $table2->toHtml();
	?>
</section>

<section id="acces-libre-integral">
	<h2>Revues qui devraient avoir un accès libre en texte intégral</h2>
	<?php
	foreach ($verif->revuesSansAccesLibre as $src => $revues) {
		echo "<h3>" . CHtml::encode(strtoupper($src)) . "</h3>";
		$table = new HtmlTable(
			array_map(
				function ($r) {
					return [
						$r['revueId'],
						CHtml::link(CHtml::encode($r['titre']), ['/revue/view', 'id' => $r['revueId']]),
					];
				},
				$revues
			),
			["ID revue", "Titre"]
		);
		echo $table->addClass('exportable')->toHtml();
	}
	?>
	<h3>
		Revues du DOAJ sans accès libre en texte intégral sur ces 3 dernières années
	</h3>
	<?php
	echo (new HtmlTable(
		array_map(
			function ($r) {
				return [
					$r['revueId'],
					CHtml::link(CHtml::encode($r['titre']), ['/revue/view', 'id' => $r['revueId']]),
					$r['lastYear'],
				];
			},
			$verif->doajInactif
		),
		["ID revue", "Titre", "Année du dernier accès"]
	))
		->addClass('exportable')->toHtml();
	?>
</section>