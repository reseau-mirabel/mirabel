<?php

/** @var Controller $this */
/** @var processes\verification\TitresEditeurs $process */

use components\HtmlTable;

assert($this instanceof Controller);

$this->pageTitle = "Vérification des titres sans éditeurs";
$this->breadcrumbs = [
	'Vérification des données' => ['/verification/index'],
	'Titres sans éditeurs',
];
?>
<?php $this->renderPartial('_nav-links') ?>

<h1>Titres sans éditeur</h1>

<section id="titres-vivants">
	<h2 style="clear: right;">Titres vivants sans éditeur</h2>
	<?php
	echo HtmlTable::build(
		array_map(
			function (Titre $t) {
				return [$t->id, $t->revueId, $t->getSelfLink(), $t->electronique ? 'Électronique' : ''];
			},
			$process->getTitresVivantsSansEditeurs()
		),
		["ID titre", "ID revue", "Titre", "Électronique"]
	)
		->setFootnote("%d titres vivants sont concernés.")
		->addClass('exportable')
		->toHtml();
	?>
</section>

<section id="titres-morts">
	<h2 style="margin-top: 1em;">Titres morts sans éditeur</h2>
	<?php
	echo HtmlTable::build(
		array_map(
			function (Titre $t) {
				return [$t->id, $t->revueId, $t->getSelfLink(), ($t->dateFin ?: "Obsolète"), $t->electronique ? 'Électronique' : ''];
			},
			$process->getTitresMortsSansEditeurs()
		),
		["ID titre", "ID revue", "Titre", "Arrêt", "Électronique"]
	)
		->setFootnote("%d titres morts sont concernés.")
		->addClass('exportable')
		->toHtml();
	?>
</section>
