<?php
use models\wikidata\Compare;
use components\HtmlHelper;

/** @var Controller $this */
/** @var Wikidata $model */
assert($this instanceof Controller);

Yii::app()->getClientScript()->registerCssFile('/css/glyphicons.css');
$this->pageTitle = "Vérification wikidata";
$this->breadcrumbs = [
	'Vérification' => ['/verification/index'],
	'Wikidata',
];
$this->renderPartial('_nav-links');
?>

<h1>
	Cache Wikidata
	<?= empty($model->explicitProperty) ? "" : CHtml::encode("- $model->explicitProperty") ?>
</h1>

<?php
if (empty($model->explicitProperty)) {
	echo "<p>Veuillez sélectionner une propriété dans la ligne de filtre ci-dessous.</p>";
}
?>

<?php
if (!empty($model->property)) {
	?>
	<table class="items table">
		<tbody>
			<tr>
				<?php
				foreach (Wikidata::getPropertyComparisonStats($model->property) as $status => $count) {
					echo "<td>$status <b>$count</b></td> ";
				} ?>
			</tr>
		</tbody>
		<caption>Comparaison de données WikiData-Mir@bel</caption>
	</table>
	<?php
}
?>

<?php
echo HtmlHelper::postButton("Relancer les comparaisons", ['/verification/wikidata-compare'], [], ['class' => 'btn']);

$this->widget('ext.bootstrap.widgets.BootGridView', [
	'id' => 'wikidata-grid',
	'ajaxUpdate' => false,
	'dataProvider' => $model->search(),
	'filter' => $model,
	'rowCssClassExpression' => function (int $line, \Wikidata $data) {
		return $data->getComparisonLevel();
	},
	'columns' => [
		[
			'header' => 'Revue',
			'name' => 'revueLink',
			'type' => 'raw',
			'filter' => false,
		],
		[
			'header' => 'Page Wikidata',
			'name' => 'qidLink',
			'type' => 'raw',
			'filter' => false,
		],
		[
			'name' => 'property',
			'filter' => \Wikidata::getWdPropertyDDL(),
		],
		'value',
		[
			'name' => 'urlLink',
			'type' => 'raw',
			'filter' => false,
		],
		'object',
		'objectLabel',
		[
			'name' => 'compUrl',
			'type' => 'raw',
			'visible' => !empty($model->property),
			'filter' => false,
		],
		[
			'header' => 'Suivi',
			'value' => function (\Wikidata $data) {
				return $data->getSuiviLevel();
			},
			'type' => 'raw',
		],
		[
			'name' => 'comparison',
			'value' => function (\Wikidata $data) {
				return $data->getComparisonText();
			},
			'type' => 'raw',
			'visible' => !empty($model->property),
			'filter' => \Wikidata::getComparisonDDL(false),
		],
		[
			'header' => 'Éditable',
			'value' => function (\Wikidata $data): string {
				return $data->isEditable() ? '<span class="glyphicon glyphicon-ok"></span>' : '';
			},
			'type' => 'raw',
		],
	],
]);
