<?php

use processes\verification\Revues;

/** @var Controller $this */
/** @var Revues $verif */

assert($this instanceof Controller);

$this->breadcrumbs = [
	'Vérification' => ['/verification/index'],
	'Vérifications revues',
];

$this->renderPartial('_nav-links')
?>
<div class="controleur-verification">
<div style="float:right; max-width:30ex;">
	<ul class="well nav nav-list">
		<li class="nav-header">Navigation dans la page</li>
		<li><?= CHtml::link("Titres obsolètes sans fin", "#titres-sans-date") ?></li>
		<li><?= CHtml::link("Accès débordant au début", "#revues-debordement-debut") ?></li>
		<li><?= CHtml::link("Accès débordant à la fin", "#revues-debordement-fin") ?></li>
		<li><?= CHtml::link("Revue avec doublons d'obsolètePar", "#revues-obsoletepar-duplicate") ?></li>
		<li><?= CHtml::link("Revue avec des problèmes de dates", "#revues-date-problem") ?></li>
	</ul>
</div>

<h1>Vérifications des revues</h1>

<section id="titres-obsoletes-sans-fin" style="clear: both">
	<h2>Titres obsolètes sans date de fin</h2>
	<p>Il y a <b><?= count($verif->titresMorts) ?></b> titres dans ce cas :</p>
	<?= Revues::displayExporTable("titres-obsoletes-sans-fin", $verif->titresMorts) ?>
</section>

<section id="revues-debordement-debut">
	<h2>Revues dont les accès débordent par le début</h2>
	<p>
		Il y a <b><?= count($verif->revuesDebordementDebut) ?></b> accès commençant avant que leur revue n'apparaisse.
		Une revue peut être débordée plusieurs fois, mais elle ne sera listée qu'une fois, avec son titre actif.
	</p>
	<?= Revues::displayExporTable("revues-debordement-debut", $verif->revuesDebordementDebut) ?>
</section>

<section id="revues-debordement-fin">
	<h2>Revues dont les accès débordent par la fin</h2>
	<p>
		Il y a <b><?= count($verif->revuesDebordementFin) ?></b> accès finissant après l'arrêt de leur revue.
		Une revue peut être débordée plusieurs fois, mais elle ne sera listée qu'une fois, avec son titre actif.
	</p>
	<?= Revues::displayExporTable("revues-debordement-fin", $verif->revuesDebordementFin) ?>
</section>

<section id="revues-obsoletepar-duplicate">
	<h2>Revues avec plusieurs titres rendus obsolètes par le même titre</h2>
	<p>
		Il y a <b><?= count($verif->revuesWithObsoleteDuplicate) ?></b> revues avec des doublons d'obsoletePar.
	</p>
	<?= Revues::displayExporTable("revues-obsoletepar-duplicate", $verif->revuesWithObsoleteDuplicate) ?>
</section>

<section id="revues-date-problem">
	<h2>Revues avec des incohérences entre les dates de publication des différents titres</h2>
	<p>
		Il y a <b><?= count($verif->revuesWithDateProblem) ?></b> revues avec des incohérences de date.
	</p>
	<?= Revues::displayExporTable("revues-date-problem", $verif->revuesWithDateProblem) ?>
</section>
</div>
