<?php

/** @var Controller $this */
/** @var \processes\verification\Urls $process */

assert($this instanceof Controller);

$this->pageTitle = "Vérification des liens d'éditeurs";
$this->breadcrumbs = [
	'Vérification' => ['/verification/index'],
	"Liens morts d'éditeurs",
];
?>
<?php $this->renderPartial('_nav-links') ?>

<h1>Vérification des liens d'éditeurs</h1>

<?= $this->renderPartial("_date-verif", $process->dates) ?>

<?php
$skipped = array_map('trim', explode("\n", Config::read('validate.skip.domains')));
$skiplinks = [];
foreach ($skipped as $skip) {
	$skiplinks[] = CHtml::link("<code>$skip</code>", ['/stats/liens', 'domain' => $skip]);
}
?>
<p class="alert alert-warning">
	Les liens vers <?= implode(', ', $skiplinks) ?>
	sont exclus de cette vérification,
	parce que ces sites web refusent les requêtes automatiques.
</p>

<?php
$form = $this->beginWidget(
	'BootActiveForm',
	[
		'id' => 'filtre-verifurl-form',
		'type' => 'horizontal',
		'enableClientValidation' => false,
	]
);
/** @var BootActiveForm $form */

if ($process->input->grappe > 0) {
	$grappe = Grappe::model()->findByPk($process->input->grappe);
	?>
	<fieldset>
		<legend>Filtre par grappe</legend>
		<?= $form->checkBoxRow($process->input, 'grappe', ['value' => $process->input->grappe, 'label' => $grappe->nom]) ?>
	</fieldset>
	<?php
}
?>
<fieldset>
	<legend>Limiter par type d'erreur</legend>
	<?= $form->errorSummary($process->input); ?>

	<?php
	// Possessions :
	//   si le champ contient un ID de partenaire différent de celui de la session,
	//   l'affichage est différent.
	/** @var ?Partenaire $partenaire */
	$partenaire = null;
	if ($process->input->possessions > 0) {
		$partenaire = Partenaire::model()->findByPk($process->input->possessions);
	}
	$isMyOrg = ($partenaire === null || (int) $partenaire->id === (int) Yii::app()->user->partenaireId);
	$optionsPossessions = [];
	if ($isMyOrg) {
		$hasPossessions = (bool) \Yii::app()->db
			->createCommand("SELECT 1 FROM Partenaire_Titre WHERE partenaireId = " . Yii::app()->user->partenaireId)
			->queryScalar();
		$optionsPossessions = [
			'disabled' => !$hasPossessions,
			'label' => "Éditeurs des revues que mon partenaire possède",
			'value' => Yii::app()->user->partenaireId,
		];
	} else {
		assert($partenaire instanceof Partenaire);
		$optionsPossessions = [
			'label' => "Éditeurs des revues dans les possessions de <em>" . CHtml::encode($partenaire->getShortName()) . "</em>",
			'value' => $process->input->possessions,
		];
	}
	if (empty($optionsPossessions['disabled'])) {
		echo $form->checkBoxRow($process->input, 'possessions', $optionsPossessions);
	}

	$suiviPartenaireId = $process->input->suiviPar ?: Yii::app()->user->partenaireId;
	$suiviPartenaire = Partenaire::model()->findByPk($suiviPartenaireId);
	if (($suiviPartenaire instanceof Partenaire)) {
		if ($suiviPartenaireId == Yii::app()->user->partenaireId) {
			$suiviLabel = "mon partenaire";
		} else {
			$suiviLabel = "<em>" . CHtml::encode($suiviPartenaire->nom) . "</em>";
		}
		echo $form->checkBoxRow(
			$process->input,
			'suiviPar',
			[
				'value' => $suiviPartenaireId,
				'label' => "Éditeurs des revues que $suiviLabel suit",
			]
		);
	}

	echo $form->textFieldRow($process->input, 'msgContient', ['class' => "input-block-level"]);
	echo $form->textFieldRow($process->input, 'msgExclut', ['class' => "input-block-level"]);
	echo $form->textFieldRow($process->input, 'urlContient', ['class' => "input-block-level"]);
	echo $form->textFieldRow($process->input, 'urlExclut', ['class' => "input-block-level"]);
	?>
	<div class="control-group">
		<?php echo CHtml::submitButton('Filtrer'); ?>
	</div>
</fieldset>
<?php $this->endWidget(); ?>

<?php
$columns = [
	[
		'header' => '',
		'value' => function () {
			static $k = 1;
			return $k++;
		},
	],
	[
		'name' => 'id',
		'headerHtmlOptions' => ['class' => 'sorted ascend'],
	],
	[
		'name' => 'idEditeur',
		'type' => 'raw',
		'value' => function (array $x) {
			return CHtml::link(
				$x['sourceId'],
				['/editeur/view', 'id' => $x['sourceId']]
			);
		},
	],
	[
		'name' => 'nom',
		'type' => 'raw',
		'value' => function (array $x) {
			return CHtml::link(
				\Norm::shortenText($x['nom'], 40),
				['/editeur/view', 'id' => $x['sourceId']]
			);
		},
	],
	[
		'name' => 'url',
		'type' => 'raw',
		'value' => function (array $x) {
			return CHtml::link(\Norm::shortenText($x['url'], 40), $x['url']);
		},
	],
	[
		'name' => 'msg',
		'header' => 'Message',
	],
	[
		'header' => 'Der. vérif.',
		'type' => 'text',
		'value' => function ($x) {
			return $x['hdateVerif'] ? date('Y-m-d', $x['hdateVerif']) : '';
		},
	],
];

$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	[
		'id' => 'verification-liens-grid',
		'dataProvider' => $process->provider,
		'filter' => null,
		'columns' => $columns,
		'ajaxUpdate' => false,
		'itemsCssClass' => 'table-condensed sortable exportable',
	]
);
?>
