<?php

/** @var Controller $this */
/** @var array $incoherences */

$this->pageTitle = "Vérification des attributs APC";
$this->breadcrumbs = [
	'Vérification' => ['/verification/index'],
	"Vérifications d'attributs APC",
];
?>
<h1><?= $this->pageTitle ?></h1>

<?php
if ($incoherences) {
	?>
	<table class="table table-bordered table-hover table-striped">
		<thead>
			<tr>
				<th>Titre</th>
				<th>Conflit</th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach ($incoherences as $i) {
				$conflict = "";
				foreach (json_decode($i['attrValues']) as $v) {
					if (is_string($v)) {
						$v = json_decode($v); // Required with an old MariaDB.
					}
					$conflict .= "<div><strong>" . CHtml::encode($v->nom) . "</strong> : " . CHtml::encode($v->valeur) . "</div>";
				}
				unset($i['attrValues']);
				$titre = Titre::model()->populateRecord($i);
				assert ($titre instanceof Titre);
				echo "<tr>",
					"<td>{$titre->getSelfLink()}</td>",
					"<td>$conflict</td>",
					"</tr>";
			}
			?>
		</tbody>
	</table>
	<?php
} else {
	echo "<p>Aucune incohérence entre les déclarations d'APC.</p>";
}
