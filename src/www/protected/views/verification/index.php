<?php

/** @var Controller $this */
/** @var array $editeurs */
/** @var array $ressources */
/** @var array $collections */
/** @var array $titresIssneSansAcces */
/** @var array $titresMorts */
/** @var array $couvertures */
assert($this instanceof Controller);

$user = Yii::app()->user;
$this->pageTitle = "Vérifications";
$this->breadcrumbs = [
	'Vérification des données',
];

$vDataUrl = $this->createUrl('/verification/data');
$vIssnUrl = $this->createUrl('/verification/issn');
$vPpnUrl = $this->createUrl('/verification/ppn');
$vRevuesUrl = $this->createUrl('/verification/revues');
?>

<h1>Vérifications sur le contenu de Mir@bel</h1>

<div>
	<?= Cms::getBlockAsHtml('verification-intro') ?>
</div>
<hr />

<div class="flexstack">

<div class="well">
	<h2><?= CHtml::link("Accès et ressources", $vDataUrl) ?></h2>
	<ul>
		<li><?= CHtml::link("Ressources sans accès", "$vDataUrl#ressource-errors") ?></li>
		<li><?= CHtml::link("Collections sans accès", "$vDataUrl#collections-sans-acces") ?></li>
		<li><?= CHtml::link("Collections temporaires", "$vDataUrl#collections-temporaires") ?></li>
		<li><?= CHtml::link("Doublons sur les triplets (titre, ressource, ID interne)", "$vDataUrl#doublons") ?></li>
		<li><?= CHtml::link("Revues qui devraient avoir un accès libre en texte intégral", "$vDataUrl#acces-libre-integral") ?></li>
	</ul>
</div>
<div class="well">
	<h2><?= CHtml::link("Autres liens", ['/verification/liens']) ?></h2>
	<ul>
		<li><?= CHtml::link("Nom différent de la source", ['/verification/liens', '#' => 'noms-faux']) ?></li>
		<li><?= CHtml::link("URL hors cadre", ['/verification/liens', '#' => 'urls-invalides']) ?></li>
	</ul>
</div>
<div class="well">
	<?php $vCouvUrl = $this->createUrl('/verification/couvertures'); ?>
	<h2><?= CHtml::link("Couvertures de titres", $vCouvUrl) ?></h2>
	<ul>
		<li><?= CHtml::link("Couvertures absentes malgré leur URL présente", "$vCouvUrl#couvertures-absentes") ?></li>
		<li><?= CHtml::link("Revues dont le dernier titre n'a pas d'image de couverture", "$vCouvUrl#titres-sans-couvertures") ?></li>
	</ul>
</div>
<div class="well">
	<h2><?= CHtml::link("Éditeurs", ['/verification/editeurs']) ?></h2>
	<ul>
		<li><?= CHtml::link("Éditeurs sans pays", ['/verification/editeurs', '#' => "sans-pays"]) ?></li>
		<li><?= CHtml::link("Éditeurs français sans IdRef", ['/verification/editeurs', '#' => 'idref']) ?></li>
		<li><?= CHtml::link("Éditeurs français sans ID Sherpa", ['/verification/editeurs', '#' => 'sherpa']) ?></li>
		<li><?= CHtml::link("Éditeurs français sans rôle sur leurs titres", ['/verification/editeurs', '#' => 'relations']) ?></li>
		<li><?= CHtml::link("Noms avec date", ['/verification/editeurs', '#' => 'date-dans-nom']) ?></li>
		<li><?= CHtml::link("Morts mais actuels", ['/verification/editeurs', '#' => 'mort-mais-actuel']) ?></li>
		<li><?= CHtml::link("Pays incohérents ISSN/éditeurs", ['/verification/editeurs', '#' => 'pays-incoherents']) ?></li>
	</ul>
</div>
<div class="well">
	<h2><?= CHtml::link("ISSN", ['/verification/issn']) ?></h2>
	<ul>
		<li><?= CHtml::link("Revues avec un ISSN-E mais sans accès <em>texte intégral</em>", "$vIssnUrl#issne-sans-integral") ?></li>
		<li><?= CHtml::link("Sans ISSN-E malgré un accès <em>texte intégral</em>", "$vIssnUrl#integral-sans-issne") ?></li>
		<li><?= CHtml::link("ISSN sans ISSN-L", "$vIssnUrl#issn-sans-issnl") ?></li>
		<li><?= CHtml::link("ISSN-L sans ISSN", "$vIssnUrl#issnl-sans-issn") ?></li>
		<li><?= CHtml::link("ISSN-L incohérents", "$vIssnUrl#issnl-autre-revue") ?></li>
		<li><?= CHtml::link("ISSN-L absents en tant qu'ISSN-(P|E)", "$vIssnUrl#issnl-non-issn") ?></li>
		<li><?= CHtml::link("Titres électroniques à doublons d'ISSN-E", "$vIssnUrl#issne-doublon") ?></li>
		<li><?= CHtml::link("Support d'ISSN inconnu", "$vIssnUrl#issn-support-inconnu") ?></li>
		<li><?= CHtml::link("ISSN-L conflictuels", "$vIssnUrl#issnl-conflictuels") ?></li>
		<li><?= CHtml::link("ISSN-L partagés", "$vIssnUrl#issnl-partages") ?></li>
		<li><?= CHtml::link("Titres à multiples ISSN-P", "$vIssnUrl#issnp-multiples") ?></li>
		<li><?= CHtml::link("Titres à multiples ISSN-E", "$vIssnUrl#issne-multiples") ?></li>
		<li><?= CHtml::link("Dates discordantes entre titre et ISSN", "$vIssnUrl#dates-discordantes") ?></li>
		<li><?= CHtml::link("Titres sans ISSN", "$vIssnUrl#sans-issn") ?></li>
		<li><?= CHtml::link("Titres avec un ISSN en cours", "$vIssnUrl#issn-encours") ?></li>
	</ul>
</div>
<div class="well">
	<h2>Liens morts</h2>
	<ul>
		<li><?= CHtml::link("Liens morts / revues", ['/verification/liensRevues']) ?></li>
		<li><?= CHtml::link("Liens morts / éditeurs", ['/verification/liensEditeurs']) ?></li>
		<li><?= CHtml::link("Liens morts / ressources", ['/verification/liensRessources']) ?></li>
		<?= Yii::app()->user->checkAccess('redaction') ? "<li>" . CHtml::link("Liens morts / rédactionnel", ['/verification/liensCms']) . "</li>" : "" ?>
	</ul>
</div>
<div class="well">
	<h2><?= CHtml::link("PPN et Sudoc", $vPpnUrl) ?></h2>
	<ul>
		<li><?= CHtml::link("PPN sans ISSN", "$vPpnUrl#ppn-sans-issn") ?></li>
		<li><?= CHtml::link("ISSN sans PPN", "$vPpnUrl#issn-sans-ppn") ?></li>
		<li><?= CHtml::link("PPN en no-holding", "$vPpnUrl#ppn-no-holding") ?></li>
	</ul>
</div>
<div class="well">
	<h2><?= CHtml::link("Revues", ['revues']) ?></h2>
	<ul>
		<li><?= CHtml::link("Revues dont le dernier titre est sans langue", ['revue/search', 'q' => ['langues' => 'aucune']]) ?></li>
		<li><?= CHtml::link("Revues sans aucun accès", ['revue/search', 'q' => ['sansAcces' => 1]]) ?></li>
		<li><?= CHtml::link("Accès débordant au début", ['/verification/revues', '#' => 'revues-debordement-debut']) ?></li>
		<li><?= CHtml::link("Accès débordant à la fin", ['/verification/revues', '#' => 'revues-debordement-fin']) ?></li>
		<li><?= CHtml::link("Revue avec doublons d'obsolètePar", ['/verification/revues', '#' => 'revues-obsoletepar-duplicate']) ?></li>
		<li><?= CHtml::link("Revue avec incohérence de date", ['/verification/revues', '#' => 'revues-date-problem']) ?></li>
		<li><?= CHtml::link("Titres obsolètes sans date de fin", ['/verification/revues', '#' => 'titres-sans-date']) ?></li>
		<li><?= CHtml::link("Titres sans éditeur", ['/verification/titresEditeurs']) ?></li>
		<li><?= CHtml::link("Titres groupés par ressource dont un accès déborde", ['/verification/titres-acces-deborde']) ?></li>
	</ul>
</div>
<div class="well">
	<h2>Thématiques</h2>
	<ul>
		<li><?= CHtml::link("Revues indexées par seuls imports", ['/verification/thematique']) ?></li>
		<li><?= CHtml::link("Revues sans thématiques", ['revue/search', 'q' => ['sanscategorie' => 1]]) ?></li>
	</ul>
</div>
<div class="well">
	<h2>Wikidata</h2>
	<ul>
		<li><?= CHtml::link("Wikidata", ['/verification/wikidata']) ?></li>
		<li><?= CHtml::link("Wikidata / Incohérences", ['/verification/wikidatabugs']) ?></li>
	</ul>
</div>

</div>
