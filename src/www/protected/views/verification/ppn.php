<?php

/** @var Controller $this */
/** @var \processes\verification\Ppn $ppn */

assert($this instanceof VerificationController);

$this->pageTitle = "Vérification des PPN";
$this->breadcrumbs = [
	'Vérifications' => ['/verification/index'],
	"Vérifications de PPN",
];
?>
<?php $this->renderPartial('_nav-links') ?>
<div style="float:right; max-width:30ex;">
	<ul class="well nav nav-list">
		<li class="nav-header">Navigation dans la page</li>
		<li><?= CHtml::link("PPN sans ISSN", "#ppn-sans-issn") ?></li>
		<li><?= CHtml::link("ISSN sans PPN", "#issn-sans-ppn") ?></li>
		<li><?= CHtml::link("PPN en no-holding", "#ppn-no-holding") ?></li>
	</ul>
</div>

<h1>Vérifications de PPN et Sudoc</h1>

<section id="ppn-sans-issn">
	<h2>PPN sans ISSN</h2>
	<p>
		Un titre de revue a un PPN du Sudoc qui n'est pas associé à un ISSN.
	</p>
	<?php
	$ppnSansIssn = $ppn->getPpnSansIssn();
	if ($ppnSansIssn) {
		echo "<ol>";
		foreach ($ppnSansIssn as $t) {
			echo sprintf('<li class="%s">', $t->getSuiviType()),
				"<code>{$t->extra}</code> {$t->getSelfLink()}</li>\n";
		}
		echo "</ol>";
	} else {
		echo '<p>Aucun.</p>';
	}
	?>
</section>

<section id="issn-sans-ppn">
	<h2>ISSN sans PPN</h2>
	<p>
		Un titre de revue n'a pas de PPN du Sudoc associé à son ISSN.
	</p>
	<?php
	$issnSansPpn = $ppn->getIssnSansPpn();
	if ($issnSansPpn) {
		echo "<ol>";
		foreach ($issnSansPpn as $t) {
			echo sprintf('<li class="%s">', $t->getSuiviType()),
				"<code>{$t->extra}</code> {$t->getSelfLink()}</li>\n";
		}
		echo "</ol>";
	} else {
		echo '<p>Aucun.</p>';
	}
	?>
</section>

<section id="ppn-no-holding">
	<h2>PPN en <em>no holding</em></h2>
	<p>
		Un titre de revue a au moins un PPN sans localisation dans le Sudoc (<em>no holding</em>).
	</p>
	<?php
	$ppnNoHolding = $ppn->getPpnNoHolding();
	if ($issnSansPpn) {
		echo "<ol>";
		foreach ($ppnNoHolding as $t) {
			echo sprintf('<li class="%s">', $t->getSuiviType()),
				"<code>{$t->extra}</code> {$t->getSelfLink()}</li>\n";
		}
		echo "</ol>";
	} else {
		echo '<p>Aucun.</p>';
	}
	?>
</section>
