<?php

/** @var Controller $this */
/** @var Editeur $editeur */
/** @var array $constraints HTML blocks */
/** @var bool $isRedirectionRequired */

$this->pageTitle = "Supprimer l'éditeur {$editeur->nom}";
$this->breadcrumbs = [
	'Éditeurs' => ['/editeur/index'],
	$editeur->getFullName() => $editeur->getSelfUrl(),
	"Supprimer",
];
?>

<h1>Supprimer l'éditeur <em><?= CHtml::encode($editeur->nom) ?></em></h1>

<div class="alert alert-warning">
	<p>
		Vous vous apprêtez à supprimer un éditeur dans Mir@bel.
		Cette opération est <strong>possible</strong> mais reste <strong>rare et exceptionnelle</strong>
		en raison des alignements de données avec d'autres systèmes d'information.
	</p>
</div>

<?php if ($constraints) { ?>
<section id="constraints">
	<h2>Contraintes bloquant la suppression</h2>
	<ol>
		<?php
		foreach ($constraints as $html) {
			echo CHtml::tag('li', ['class' => 'alert alert-danger'], $html);
		}
		?>
	</ol>
	<p>
		Vous pouvez éliminer ces points de blocage et recharger cette page
		ou
		<?= CHtml::link("Annuler et revenir à la page de l'éditeur", ['/editeur/view', 'id' => $editeur->id], ['class' => 'btn']) ?>
	</p>
</section>
<?php } else { ?>
<p class="alert alert-info">
	Aucune contrainte n'empêche cette suppression : pas d'IdRef, de titre lié, etc.
</p>
<section>
	<h2>Confirmer la suppression</h2>

	<?= CHtml::beginForm('', 'post', ['id' => 'confirm-delete']) ?>
	<div>
		<?= CHtml::hiddenField('confirm', '1') ?>
		<label class="annoying">Écrivez « Je confirme » dans le champ ci-dessous</label>
		<?= CHtml::textField('annoying', '', ['placeholder' => "Je confirme", 'size' => '20', 'class' => 'annoying', 'required' => true]) ?>
	</div>
	<div style="margin-top: 1ex">
		<label>
			<?= $isRedirectionRequired ? "Choisissez" : "Éventuellement, choisissez" ?>
			quel éditeur remplacera celui-ci (redirection d'URL)
		</label>
		<?php
		$this->widget(
			'zii.widgets.jui.CJuiAutoComplete',
			[
				'name' => "redirection-autocomplete",
				'source' => <<<EOJS
					js:function(request, response) {
						$.ajax({
							url: "{$this->createUrl('/editeur/ajax-complete')}",
							data: { term: request.term, excludeIds: [{$editeur->id}] },
							success: function(data) { response(data); }
						});
					}
					EOJS,
				// See <http://jqueryui.com/demos/autocomplete/#options>
				'options' => [
					// min letters typed before we try to complete
					'minLength' => '1',
					'select' => <<<EOJS
						js:function(event, ui) {
							$('#redirection').val(ui.item.id);
							$('#redirection-name').text(ui.item.value);
							$('#redirection-autocomplete').val('');
							$('#redirection-rm.hidden').toggleClass('hidden');
							$("#redirection-comment").css('font-weight', 'normal');
							return false;
						}
						EOJS,
				],
				'htmlOptions' => [
					'style' => 'width: 100%',
					'placeholder' => "éditeur par ID ou complétion du nom",
				],
			]
		);
		echo CHtml::hiddenField('redirection', '');
		?>
		<button type="button" id="redirection-rm" class="pull-right hidden" onclick="$(this).toggleClass('hidden');$('#redirection').val('');$('#redirection-name').text(' ');">
			<span class="glyphicon glyphicon-remove"></span>
		</button>
		<div id="redirection-name" style="font-weight: bold"> </div>
		<div id="redirection-comment"><?= $isRedirectionRequired ? "Une nouvelle redirection est nécessaire pour cette suppression car cet objet est déjà enregistré dans la table des redirections." : "" ?></div>
	</div>
	<div class="form-actions">
		<button class="btn btn-danger" type="submit">
			Supprimer définitivement cet éditeur
		</button>
		<?= CHtml::link("Annuler et revenir à la page de l'éditeur", ['/editeur/view', 'id' => $editeur->id], ['class' => 'btn']) ?>
	</div>
	<?= CHtml::endForm() ?>

	<?php
	Yii::app()->getClientScript()->registerScript('confirm-delete',
		"const isRedirectionRequired = " . ($isRedirectionRequired ? "true" : "false") . ";\n"
		. <<<EOJS
		$('#confirm-delete').on('submit', function(e) {
			if ($('input.annoying').val() !== "Je confirme") {
				e.preventDefault();
				$("label.annoying").css('font-weight', 'bold');
				return false;
			}
			const redir = document.querySelector('input[name="redirection"]').value;
			if (isRedirectionRequired > 0) {
				if (redir === '') {
					e.preventDefault();
					$("#redirection-comment").css('font-weight', 'bold');
					return false;
				}
			}
			if (!redir && document.querySelector('#redirection-autocomplete').value !== '') {
				e.preventDefault();
				alert("Le champ de choix d'un éditeur n'est pas vide alors qu'aucun éditeur n'est choisi.");
				return false;
			}
			return true;
		});
		EOJS
	);
	?>
</section>
<?php } ?>
