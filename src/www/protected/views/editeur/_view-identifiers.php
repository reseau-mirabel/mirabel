<?php

/** @var Controller $this */
/** @var Editeur $editeur */

assert($this instanceof Controller);

echo '<ul class="unstyled">';
if ($editeur->idref) {
	echo '<li>',
		CHtml::link(
			CHtml::image('/images/logo-idref.png', "IdRef {$editeur->idref}", ['height' => 30, 'width' => 90, 'style' => 'width:inherit; height:16px;'])
				. " IdRef",
			"https://www.idref.fr/{$editeur->idref}",
			['title' => "IdRef {$editeur->idref}", 'data-idref' => $editeur->idref]
		),
		'</li>';
}
if ($editeur->sherpa) {
	if (Yii::app()->user->isGuest) {
		$sherpaName = "";
	} else {
		$sherpaName = (string) \Yii::app()->db
			->createCommand("SELECT JSON_EXTRACT(content, '$.name[0].name') FROM `SherpaPublisher` WHERE id = {$editeur->sherpa}")
			->queryScalar();
		if ($sherpaName) {
			$sherpaName = " → " . CHtml::encode(json_decode($sherpaName));
		} else {
			$lastFetch = (int) \Yii::app()->db
				->createCommand("SELECT MAX(lastFetch) FROM `SherpaPublisher`")
				->queryScalar();
			$sherpaName = sprintf(' <abbr title="Absent des données de Sherpa lues le %s">en erreur</abbr>', date('d/m/Y', $lastFetch));
		}
	}
	echo '<li>',
		CHtml::link(
			CHtml::image('/images/logo-jisc.png', "Open policy finder, ID éditeur {$editeur->sherpa}", ['height' => 16, 'width' => 16]) . " Open policy finder",
			"https://openpolicyfinder.jisc.ac.uk/id/publisher/{$editeur->sherpa}",
			['title' => "Open policy finder, ID éditeur {$editeur->sherpa}"]
		),
		$sherpaName,
		'</li>';
}
if ($editeur->ror) {
	echo '<li>',
	CHtml::link(
		CHtml::image(
			'/images/logo-ror.png',
			"ROR {$editeur->ror}",
			['height' => 30, 'width' => 90, 'style' => 'width:inherit; height:16px;']
		) . " ROR",
		"https://ror.org/{$editeur->ror}",
		['title' => "Research Organization Registry {$editeur->ror}", "data-ror" => $editeur->ror]
	),
	'</li>';
}
echo "</ul>\n";

