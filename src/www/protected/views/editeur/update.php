<?php

/** @var Controller $this */
/** @var Editeur $model */
/** @var bool $direct */
/** @var Intervention $intervention */
assert($this instanceof Controller);

$this->pageTitle = "Modifier l'éditeur " . $model->nom;

$this->breadcrumbs=[
	'Éditeurs' => ['index'],
	$model->getFullName() => ['view', 'id' => $model->id],
	'Modifier',
];
?>

<h1>Modifier l'éditeur <em><?= CHtml::encode($model->getFullName()); ?></em></h1>

<?= $this->renderPartial(
	'_form',
	['model' => $model, 'direct' => $direct, 'intervention' => $intervention]
) ?>

<?php
Yii::app()->getClientScript()->registerScript('editeur-update', <<<EOJS
	const helpBlock = document.createElement("div");
	helpBlock.classList.add("help-block");
	helpBlock.innerHTML = `Vous vous apprêtez à modifier un champ important pour un éditeur aligné à d'autres référentiels.
		Des évolutions trop importantes chez un éditeur imposent la création d’un nouvel éditeur plutôt que la mise à jour de l’existant.
		En cas de doute consultez la <a href="/public/Procedure_Editeurs.pdf">notice explicative</a>.
		Vous pouvez également adresser votre demande à l'adresse <a href="mailto:mirabel_editeurs@listes.sciencespo-lyon.fr">mirabel_editeurs@listes.sciencespo-lyon.fr</a>.`;

	let hasImportantValue = false;
	for (let f of document.querySelectorAll(".important-field")) {
		if (f.value !== '') {
			hasImportantValue = true;
			watchChanges(f);
		}
	}
	if (hasImportantValue) {
		const nameField = document.querySelector("#Editeur_nom");
		watchChanges(nameField);
	}
	function watchChanges(field) {
		const group = field.closest('.control-group');
		const initialValue = field.value;
		field.addEventListener('input', function() {
			if (field.value === initialValue) {
				group.classList.remove('warning');
				group.querySelector(".help-block").remove();
			} else {
				group.classList.add('warning');
				if (group.querySelector(".help-block") === null) {
					field.insertAdjacentElement('afterend', helpBlock);
				}
			}
		});
	}
	EOJS
);
