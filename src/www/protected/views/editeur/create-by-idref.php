<?php

/** @var Controller $this */

assert($this instanceof Controller);

$this->breadcrumbs = [
	'Éditeurs' => ['index'],
	'Créer via IdRef',
];
$this->pageTitle = "Nouvel éditeur";
?>

<h1><?= $this->pageTitle ?></h1>

<p>
	Avec la recherche ci-dessous, vous pouvez chercher un éditeur sur <a href="https://www.idref.fr">idref.fr</a>
	pour pré-remplir le fomulaire de création de l'éditeur dans Mir@bel.
</p>
<p>
	Si la recherche ne donne pas de bon résultat,
	il reste possible de <a href="/editeur/create-final">créer l'éditeur</a> sans données externes.
</p>

<?= (new widgets\IdrefQuery)->run() ?>
