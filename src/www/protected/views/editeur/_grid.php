<?php

use components\HtmlHelper;
use components\SqlHelper;
use models\sphinx\Editeurs;

/** @var Controller $this */
/** @var \models\searches\EditeurSearch $model */
/** @var int $pagination */
/** @var bool $displaySearchSummary */

if (!$model->validate() || $model->isEmpty()) {
	return;
}

try {
	$dataProvider = $model->search($pagination);
} catch (\Throwable $ex) {
	echo <<<EOP
		<p class="alert alert-danger">Cette recherche n'est pas valable. Le terme recherché doit contenir au moins un mot.</p>
		EOP;
	return;
}

if ($displaySearchSummary) {
	?>
	<p id="search-summary">
		<strong>Critères :</strong>
		<?= CHtml::encode($model->getSummary()) ?>
	</p>
	<?php
}
?>

<div style="clear: both;"></div>
<?php
$pays = SqlHelper::sqlToPairs("SELECT id, nom FROM Pays ORDER BY nom");
$columns = [
	[
		'name' => 'nom',
		'header' => 'Éditeur',
		'type' => 'raw',
		'value' => function (Editeurs $e) {
			return $e->getRecord()->getSelfLink();
		},
	],
	[
		'name' => 'nbrevues',
		'header' => 'Revues',
		'type' => 'raw',
		'value' => function (Editeurs $e): string {
			return CHtml::link((string) $e->nbrevues, ['/revue/search', 'q[editeurId][]' => $e->id]);
		},
	],
	[
		'name' => 'nbtitresvivants',
		'header' => 'Titres en cours',
		'value' => function (Editeurs $e) {
			return $e->nbtitresvivants;
		},
	],
	[
		'header' => 'Pays',
		'type' => 'raw',
		'value' => function (Editeurs $e) use ($pays) {
			if ($e->paysid) {
				return CHtml::link(
					CHtml::encode($pays[$e->paysid]),
					['/editeur/pays', 'paysId' => $e->paysid, 'nom' =>  Norm::urlParam($pays[$e->paysid])]
				);
			}
			return '';
		},
		'visible' => !$model->pays,
	],
	[
		'header' => 'Repère géo',
		'value' => function (Editeurs $e) {
			return $e->getRecord()->geo;
		},
	],
];
$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	[
		'id' => 'editeur-grid',
		'dataProvider' => $dataProvider,
		'filter' => null,
		'columns' => $columns,
		'ajaxUpdate' => false,
		'rowCssClassExpression' => function ($index, Editeurs $e) {
			return HtmlHelper::linkItemClass('editeur', $e->getSuiviIds());
		},
		'summaryText' => $pagination > 0
			? 'Résultats de {start} à {end} sur {count} éditeurs'
			: '{count} éditeurs',
	]
);

$maxPageRank = $dataProvider->getPagination()->getPageCount();
(new \widgets\SearchNavigationInit('editeur', $dataProvider->getData(), $maxPageRank))->run();
