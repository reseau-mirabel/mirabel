<?php

/** @var Controller $this */
/** @var Editeur $model */
/** @var ?bool $embedded */
/** @var bool $direct */
/** @var Intervention $intervention */

use components\HtmlHelper;

assert($this instanceof Controller);

if (empty($embedded) && Yii::app()->user->checkAccess("avec-partenaire")) {
	$jsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application') . '/widgets/assets/query-idref.js');
	Yii::app()->getClientScript()->registerScriptFile($jsUrl);
}

echo '<p style="margin-top: 1ex;">Veuillez consulter la '
	. CHtml::link(
		"documentation détaillée au format PDF",
		Yii::app()->baseUrl . '/public/Procedure_Editeurs.pdf',
		['target' => '_blank', 'title' => "PDF dans une nouvelle page"]
	);

if ($model->isNewRecord) {
	$url = $this->createUrl('/editeur/create-final');
} else {
	$url = $this->createUrl('/editeur/update', ['id' => $model->id]);
}

$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'editeur-form',
		'action' => $url,
		'enableAjaxValidation' => false,
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'hints' => Hint::model()->getMessages('Editeur'),
		'htmlOptions' => ['class' => 'well'],
	]
);
/** @var BootActiveForm $form */

new \widgets\CkEditor();
?>

<?php
if (empty($embedded)) {
	?>
<div class="alert">
	<?php
	if ($direct) {
		echo "Les modifications seront appliquées immédiatement.";
	} else {
		echo "Les modifications que vous proposez devront être validées "
			, "par un membre partenaire de Mi@bel avant d'être appliquées.";
	}
	?>
</div>
<?php
}
?>

<?php
echo HtmlHelper::errorSummary($model);
if (Yii::app()->user->checkAccess("avec-partenaire")) {
	$hasUrlErrors = $model->getError('url') || $model->getLiens()->hasUrlError();
	echo UrlFetchableValidator::toggleUrlChecker($model, $hasUrlErrors);
}
$this->renderPartial(
	'/intervention/_errors',
	[
		'form' => $form,
		'intervention' => $intervention,
		'model' => $model,
	]
);

echo $form->textFieldRow($model, 'nom', ['class' => 'span11']);
if (isset($model->confirm) && $model->confirm === false) {
	echo $form->checkBoxRow($model, 'confirm');
}
echo $form->textFieldRow($model, 'prefixe', ['class' => 'span2']);
echo $form->textFieldRow($model, 'sigle', ['class' => 'span4']);
echo $form->textFieldRow($model, 'dateDebut', ['class' => 'span4']);
echo $form->textFieldRow($model, 'dateFin', ['class' => 'span4']);
echo $form->textAreaRow($model, 'description', ['rows' => 6, 'cols' => 50, 'class' => 'span11']);
echo $form->textFieldRow($model, 'url', ['class' => 'span11', 'type' => 'url']);
echo $form->dropDownListRow($model, 'role', Editeur::getPossibleRoles(), ['empty' => '—']);
echo $form->dropDownListRow($model, 'paysId', CHtml::listData(Pays::model()->sorted()->findAll(), 'id', 'nom'), ['empty' => '—']);
echo $this->renderPartial(
	'/global/_autocompleteDistinct',
	['model' => $model, 'attribute' => 'geo', 'form' => $form]
);
?>
<fieldset>
	<legend>Identifiants</legend>
	<?php
	echo $form->textFieldRow($model, 'idref', ['size' => '10', 'class' => 'idref important-field']);
	echo $form->textFieldRow($model, 'sherpa', ['size' => '10', 'class' => 'sherpa important-field']);
	echo $form->textFieldRow($model, 'ror', ['size' => '10', 'class' => 'sherpa important-field']);
	?>
</fieldset>
<fieldset class="other-links">
	<legend>Liens</legend>
	<?php
	$this->renderPartial(
		'/global/_form_links',
		[
			'form' => $form,
			'model' => $model,
			'embedded' => !empty($embedded),
		]
	);
	?>
</fieldset>

<input type="hidden" name="editeur-duplicate-id" id="editeur-duplicate-id" value="<?= uniqid() ?>" />

<?php
if (empty($embedded)) {
	$this->renderPartial('/intervention/_contact', ['form' => $form, 'intervention' => $intervention, 'canForceValidating' => !$model->isNewRecord]);
	?>
	<div class="form-actions">
		<?php
		$this->widget(
			'bootstrap.widgets.BootButton',
			[
				'buttonType' => 'submit',
				'type' => 'primary',
				'label' => $model->isNewRecord ?
					($direct ? 'Créer cet éditeur' : "Proposer de créer cet éditeur")
					: ($direct ? 'Enregistrer' : 'Proposer cette modification'),
			]
		); ?>
	</div>
	<?php
}

$this->endWidget('editeur-form');

if ($model->isNewRecord) {
	Yii::app()->getClientScript()->registerScript('editeur-form', <<<EOJS
		$('#editeur-form').on('submit', function(e) {
			if ($('select#Editeur_paysId').val() === '') {
				return confirm("Attention, vous n'avez pas choisi le pays. Il est important pour la carte des éditeurs proposée par Mir@bel. Vous confirmez cet éditeur sans pays ?");
			}
			return true;
		});
		EOJS
	);
}
?>
