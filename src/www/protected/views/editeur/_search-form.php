<?php

/** @var Controller $this */
/** @var \models\searches\EditeurSearch $model */

use components\HtmlHelper;
use components\SqlHelper;

assert($this instanceof EditeurController);
?>
<section id="editeur-search-form">
	<h2>Nouvelle recherche</h2>
	<?php
	$form = $this->beginWidget(
		'bootstrap.widgets.BootActiveForm',
		[
			'id' => 'editeur-search-form',
			'action' => $this->createAbsoluteUrl('/editeur/search'),
			'method' => 'get',
			'enableAjaxValidation' => false,
			'type' => BootActiveForm::TYPE_HORIZONTAL,
			'htmlOptions' => ['class' => 'well'],
			'hints' => Hint::model()->getMessages(get_class($model)),
		]
	);
	/** @var BootActiveForm $form */
	?>

	<?= HtmlHelper::errorSummary($model) ?>

	<?php
	echo $form->textFieldRow($model, 'global', ['class' => 'span11']);
	echo $form->textFieldRow($model, 'nom', ['class' => 'span11']);
	// pour rendre les champs 'global' et 'nom' mutuellement exclusifs
	Yii::app()->clientScript->registerScript('match-exclude', <<<'EOJS'
		var sel_g = document.getElementById('q_global');
		var sel_n = document.getElementById('q_nom');
		function onInputGlobal(e) {
			if (sel_g.value === '') {
				q_nom.disabled = false;
				q_nom.placeholder = '';
			} else {
				q_nom.disabled = true;
				q_nom.placeholder = 'Incompatible avec le champ Tous';
			}
		}
		function onInputNom(e) {
			if (sel_n.value === '') {
				q_global.disabled = false;
				q_global.placeholder = '';
			} else {
				q_global.disabled = true;
				q_global.placeholder = 'Incompatible avec le champ Nom';
			}
		}

		if (sel_g) {
			onInputGlobal();
			sel_g.addEventListener('input', onInputGlobal);
		}
		if (sel_n) {
			onInputNom();
			sel_n.addEventListener('input', onInputNom);
		}
		EOJS
	);

	$countries = SqlHelper::sqlToPairs("SELECT p.id, p.nom FROM Pays p JOIN Editeur e ON e.paysId = p.id GROUP BY p.id ORDER BY p.code2 = 'ZZ' DESC, p.nom");
	echo $form->dropDownListRow($model, 'pays', $countries, ['empty' => '—']);
	?>
	<div class="control-group">
		<label class="control-label">Identifiants</label>
		<div class="controls">
			<div>
			<div class="input-prepend">
				<span class="add-on" style="min-width: 16ex; text-align: left;">IdRef</span>
				<?= CHtml::activeTextField($model, 'idref', ['placeholder' => '* OU ! OU IdRef']) ?>
			</div>
			</div>
			<div>
			<div class="input-prepend">
				<span class="add-on" style="min-width: 16ex; text-align: left;">Open policy finder</span>
				<?= CHtml::activeTextField($model, 'sherpa', ['placeholder' => '* OU ! OU ID Open policy finder']) ?>
			</div>
			</div>
			<div>
				<div class="input-prepend">
					<span class="add-on" style="min-width: 16ex; text-align: left;">ROR</span>
					<?= CHtml::activeTextField($model, 'ror', ['placeholder' => '* OU ! OU ID ROR']) ?>
				</div>
			</div>
		</div>
	</div>
	<?php
	echo $form->checkBoxRow($model, 'vivants', ['label' => "Restreindre aux éditeurs actuels"]);
	?>

	<fieldset>
		<legend>Dans Mir@bel</legend>
		<?php
		echo $form->textFieldRow($model, 'hdateModif', ['placeholder' => '> 2021-02 OU 2020 OU 2015 2019']);
		echo $form->textFieldRow($model, 'hdateVerif', ['placeholder' => '> 2021-02 OU 2020 OU 2015 2019']);
		?>
	</fieldset>

	<div class="form-actions">
		<button type="submit" class="btn btn-primary">Rechercher</button>
	</div>

	<?php
	$this->endWidget('editeur-form');
	?>
</section>
