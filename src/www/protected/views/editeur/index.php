<?php

/** @var Controller $this */
/** @var \components\sphinx\DataProvider $dataProvider */
/** @var ?\processes\editeur\ListFilter $filter */
/** @var \components\paginationalpha\ListViewAlpha $listView */
/** @var string $searchHash */
/** @var int $totalEditeurs */

use components\HtmlHelper;

assert($this instanceof Controller);

$this->breadcrumbs = [
	'Éditeurs',
];
$this->pageTitle = "Éditeurs";
?>

<div class="legende">
	<h3>Légende</h3>
	<ul>
		<?php
		if (!Yii::app()->user->checkAccess("avec-partenaire")) {
			?>
			<li class="editeur suivi-other">Suivi par un partenaire</li>
			<?php
		} else {
			?>
			<li class="editeur suivi-self">Suivi par son propre partenaire</li>
			<li class="editeur suivi-other">Suivi par un autre partenaire</li>
			<?php
		}
		?>
	</ul>
</div>
<h1>Éditeurs<?php
	if ($filter->suivi > 0) {
		echo " suivis par le partenaire <em>" . CHtml::encode($filter->suiviPartenaire->nom) . "</em>";
	} elseif ($filter->suivi < 0) {
		echo " suivis par un partenaire";
	} elseif (!empty($filter->lettre)) {
		echo " — " . CHtml::encode($filter->lettre);
	}
?></h1>

<figure class="link-to-map">
	<?= CHtml::link(
		CHtml::image('/public/carte_editeurs_vignette.png', "Carte des $totalEditeurs éditeurs dans Mir@bel"),
		['/editeur/carte']
	) ?>
	<figcaption>Carte des <?= $totalEditeurs ?> éditeurs</figcaption>
</figure>
<div>
	<?= CHtml::link("Recherche avancée d'éditeurs", ['/editeur/search']) ?>
</div>

<?php
if ($listView !== null) {
	$listView->renderLetterSelector();
	$listView->renderPager();
}
?>

<?php
if (!$filter->suivi) {
	echo CHtml::link(
		CHtml::image(Yii::app()->baseUrl . "/images/plus.png", "Ajout d'éditeur"),
		['create'],
		[
			'style' => 'float: right; margin-right: 40px; clear: right;',
			'title' => (Yii::app()->user->access()->toEditeur()->createDirect() ? "Créer" : "Proposer") . " un nouvel éditeur",
		]
	);
}
?>

<table class="items table list-results">
	<thead>
		<tr>
			<th>Nom</th>
			<th>Revues</th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($dataProvider->getData() as $result) {
			echo '<tr><td>'
				. HtmlHelper::editeurLinkItem($result)
				. '</td><td>'
				. CHtml::link(
					$result->nbrevues . ' revue' . ($result->nbrevues > 1 ? 's' : ''),
					['/revue/search', 'SearchTitre[editeurId][]' => $result->id]
				) . '</td></tr>';
		}
		?>
	</tbody>
</table>

<?php
if ($listView !== null) {
	$listView->renderLetterSelector();
	$listView->renderPager();
}
$maxPageRank = $dataProvider->getPagination()->getPageCount();
(new \widgets\SearchNavigationInit('editeur', $dataProvider->getData(), $maxPageRank))->run();
