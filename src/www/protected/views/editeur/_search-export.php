<?php

/** @var Controller $this */
/** @var models\searches\EditeurSearch $search */

assert($this instanceof Controller);

$criteria = array_filter($search->attributes);
?>
<div class="well">
	<h4 title="Export au format tableur CSV de cette liste des éditeurs">Exporter les éditeurs</h4>
	<?= CHtml::link("Fichier CSV", ['/editeur/export', 'criteria' => json_encode($criteria)], ['class' => 'btn']) ?>
</div>
