<?php

/** @var Controller $this */
/** @var Pays $pays */
/** @var \models\searches\EditeurSearch $model */

assert($this instanceof Controller);

$this->breadcrumbs = [
	'Éditeurs' => ['editeur/index'],
	'Carte' => ['editeur/carte'],
	$pays->nom,
];
$this->pageTitle = "Éditeurs / " . $pays->nom;
\Yii::app()->getComponent('sidebar')->menu[] = [
	'label' => 'Nouvelle recherche ↓',
	'url' => '#editeur-search-form',
];
?>

<?= $this->renderPartial('_suivi-legend') ?>

<h1><?= CHtml::encode($this->pageTitle) ?></h1>

<?= $this->renderPartial(
	'_grid',
	[
		'model' => $model,
		'pagination' => false,
		'displaySearchSummary' => false,
	]
) ?>

<?= $this->renderPartial('_search-form', ['model' => $model]) ?>
