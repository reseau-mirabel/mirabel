<div class="legende">
	<h3>Légende</h3>
	<ul>
		<?php
		if (!Yii::app()->user->checkAccess("avec-partenaire")) {
			?>
			<li class="editeur suivi-other">Suivi par un partenaire</li>
			<?php
		} else {
			?>
			<li class="editeur suivi-self">Suivi par son propre partenaire</li>
			<li class="editeur suivi-other">Suivi par un autre partenaire</li>
			<li class="editeur suivi-none">Non suivi</li>
			<?php
		}
		?>
	</ul>
</div>
