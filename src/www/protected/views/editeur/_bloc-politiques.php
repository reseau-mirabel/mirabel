<?php

/** @var Controller $this */
/** @var Editeur $editeur */

assert($this instanceof Controller);

?>
<section>
	<h2>Politiques de publication et auto-archivage</h2>

	<p>
		<?= CHtml::link("Gestion des politiques", ['/politique', 'editeurId' => $editeur->id]) ?> :
		liste des politiques déjà définies, et modification si autorisé.
	</p>

	<h3>Utilisateurs habilités</h3>
	<?php
	$data = Yii::app()->db
		->createCommand(
			<<<EOSQL
			SELECT u.*, ue.`role`, ue.confirmed
			FROM Utilisateur_Editeur ue JOIN Utilisateur u ON ue.utilisateurId = u.id
			WHERE ue.editeurId = :eid AND u.actif = 1
			ORDER BY u.nom, u.prenom
			EOSQL
		)
		->queryAll(true, [':eid' => $editeur->id]);
	if (!$data) {
		echo "<p>Aucun utlisateur de Mir@bel n'est responsable de déclarer la politique.</p>";
	} else {
		echo "<ul>";
		$canSeeUserLink = Yii::app()->user->access()->hasPermission('politiques');
		$roles = [
			Politique::ROLE_GUEST => "délégué",
			Politique::ROLE_OWNER => "responsable",
		];
		foreach ($data as $row) {
			echo "<li>";
			if ($canSeeUserLink) {
				echo CHtml::link(CHtml::encode($row['nomComplet']), ['/utilisateur/view', 'id' => $row['id']]);
			} else {
				echo CHtml::tag('span', [], CHtml::encode($row['nomComplet']));
			}
			if ($row['confirmed'] === '0') {
				echo ' <span class="label label-warning">en attente de validation</label>';
			}
			echo ' <span class="label">' . $roles[$row['role']] . '</span>';
			echo " ; ", CHtml::link(CHtml::encode($row['email']), "mailto:{$row['email']}");
			echo "</li>";
		}
		echo "</ul>";
	}
	?>
</section>
