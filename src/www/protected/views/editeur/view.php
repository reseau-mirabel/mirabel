<?php

/** @var Controller $this */
/** @var Editeur $model */
/** @var bool $forceRefresh */
/** @var ?SearchNavigation $searchNavigation */
/** @var Cms[] $actus */

$this->pageTitle = "Éditeur " . $model->getFullName();
$this->pageDescription = "Mirabel liste les accès en ligne aux revues de l'éditeur {$model->getFullName()}";

$this->breadcrumbs = [
	'Éditeurs' => ['index'],
	$model->getFullName(),
];

$imgUrl = Yii::app()->getAssetManager()
	->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview';

if (Yii::app()->user->checkAccess("avec-partenaire")) {
	$jsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application') . '/widgets/assets/query-idref.js');
	Yii::app()->getClientScript()->registerScriptFile($jsUrl);
}

$partenaire = $model->partenaire;

$encodedTitle = CHtml::encode($this->pageTitle);
$encodedDescription = CHtml::encode($this->pageDescription);
$encodedName = CHtml::encode($model->getFullName());
$this->appendToHtmlHead(<<<EOL
	<meta name="description" content="$encodedDescription" lang="fr" />
	<meta name="keywords" content="éditeur, $encodedName, revue, accès en ligne, texte intégral, sommaire, périodique" lang="fr" />
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/" />
	<meta name="dcterms.title" content="Mirabel : $encodedTitle" />
	<meta name="dcterms.subject" content="revue, accès en ligne, texte intégral, sommaire, périodique, éditeur, $encodedName" />
	<meta name="dcterms.language" content="fr" />
	<meta name="dcterms.creator" content="Mirabel" />
	<meta name="dcterms.publisher" content="Sciences Po Lyon" />
	EOL
);
if ($partenaire) {
	$logoUrl = $partenaire->getLogoUrl(reduced: false);
	if ($logoUrl) {
		Yii::app()->clientScript->registerMetaTag($logoUrl, 'og:image', null, ['property' => 'og:image'], 'og:image');
	}
}

$this->renderPartial('/global/_interventionLocal', ['model' => $model]);
?>
<?= (new \widgets\SearchNavigationDisplay())->run(); ?>

<h1>
	Éditeur <em><?= CHtml::encode($model->getFullName()) ?></em>
</h1>

<div class="editeur<?= ($model->partenaire ? "-partenaire" : "") ?>">
	<?php if ($model->partenaire ) { ?>
	<div class="partenaire">
		<?php
		$logoUrl = $partenaire->getLogoUrl(reduced: true);
		if ($logoUrl) {
			echo '<div>' . CHtml::image($logoUrl, $partenaire->getFullName(), ['class' => 'editeur-logo']) . '</div>';
		}
		?>
		<div style="display:flex; flex-direction:row; justify-content:space-evenly; text-align:left;">
			<img src="/images/mirabel_partenaires.png" width="300" height="300" alt="partenaire du réseau Mirabel" style="max-height:64px; max-width:64px;">
			<h3 style="color:var(--mirabel-orange); font-weight:normal;">
				Éditeur partenaire<br>de Mir@bel
			</h3>
		</div>
		<div>
		<?= CHtml::link(
			"<strong>Page partenaire</strong> " . CHtml::tag('span', [], CHtml::encode($model->partenaire->nom)),
			['/partenaire/view', 'id' => $model->partenaire->id],
			['class' => 'btn btn-light']
		) ?>
		</div>
	</div>
	<?php } ?>
	<div class="editeur">
		<div class="guest-operations">
			<?= CHtml::link(
				CHtml::image($imgUrl . '/update.png', "Proposer une modification"),
				['update', 'id' => $model->id],
				['title' => "Proposer une modification"]
			) ?>
		</div>
		<?php
		$attributes = [
			[
				'name' => 'nom',
				'value' => $model->getLongName(),
			],
			'sigle',
			[
				'label' => "Période d'activité",
				'visible' => $model->dateDebut || $model->dateFin,
				'value' => $model->getPeriode(),
			],
			[
				'name' => 'description',
				'type' => 'raw',
				'value' => nl2br(CHtml::encode($model->description)),
			],
			[
				'label' => 'Notes privées',
				'type' => 'ntext',
				'value' => $partenaire === null ? '' : $partenaire->notes,
				'visible' => Yii::app()->user->access()->hasPermission('admin') && ($partenaire !== null),
			],
			[
				'name' => 'url',
				'type' => 'url',
				'visible' => $model->url !== '',
			],
			[
				'name' => 'role',
				'value' => $model->role ? (Editeur::getPossibleRoles()[$model->role] ?? $model->role) : "non défini",
				'visible' => !empty($model->role),
			],
			'liensExternes:raw:Autres liens',
			'liensInternes:raw:Liens Mir@bel',
			[
				'name' => 'paysId',
				'value' => ($model->paysId ? $model->pays->nom : ''),
			],
			'geo',
			[
				'label' => 'Identifiants',
				'type' => 'raw',
				'value' => $this->renderPartial('_view-identifiers', ['editeur' => $model], true),
				'visible' => $model->idref || $model->sherpa || $model->ror,
			],
		];
		$this->widget(
			'bootstrap.widgets.BootDetailView',
			[
				'data' => $model,
				'attributes' => $attributes,
				'hideEmptyLines' => true,
			]
		);
		?>
	</div>
</div>

<h2>Revues de cet éditeur dans Mir@bel</h2>
<p>
	<?php
	$numActive = $model->countRevues(true);
	$num = $model->countRevues(false);
	if (!$num) {
		echo "Cet éditeur n'a aucun titre dans Mir@bel.";
	} else {
		if ($numActive) {
			echo CHtml::link(
				$numActive . ' revue' . ($numActive > 1 ? 's' : '') . ' en cours de parution',
				['/revue/search', 'q' => ['editeurId' => $model->id, 'vivant' => 1]]
			);
			if ($num === $numActive) {
				echo " de cet éditeur";
			}
		} else {
			echo "Aucune revue en cours de parution";
		}
		if ($numActive > 1) {
			echo " sont présentes";
		} elseif ($numActive === 1) {
			echo " est présente";
		} else {
			echo " n'est présente";
		}
		if ($num === $numActive) {
			echo " dans Mir@bel.";
		} else {
			echo ", pour un total de "
				. CHtml::link(
					$num . ' revue' . ($num > 1 ? 's' : ''),
					['/revue/search', 'SearchTitre[aediteurId][]' => $model->id]
				)
				. " de cet éditeur dans Mir@bel.";
		}
	}
	?>
</p>

<?php
if (Yii::app()->user->access()->toEditeur()->viewPolitiques($model)) {
	$this->renderPartial('_bloc-politiques', ['editeur' => $model]);
}
?>

<?php
if ($actus) {
	?>
	<section>
		<h2>Actualités dans Mir@bel</h2>
		<ul class="breves">
			<?php
			$limit = \controllers\partenaire\ViewAdminAction::MAX_ACTUS;
			foreach ($actus as $block) {
				$limit--;
				if ($limit < 0) {
					echo '<li id="actu' . $block->id . '">'
						. CHtml::link(
							"→ Toute l'actualité de <em>" . CHtml::encode($partenaire->nom) . "</em> dans Mir@bel",
							['/site/actualite', 'q' => ['partenaireId' => $partenaire->id]]
						)
						. "</li>\n";
					break;
				}
				echo '<li id="actu' . $block->id . '">' . $block->toHtml() . "</li>\n";
			}
			?>
		</ul>
	</section>
	<?php
}
?>

<?php
$partenaireSuivant = Suivi::isTracked($model);
if ($partenaireSuivant) {
	echo "<h2>Suivi</h2>";
	echo '<ul>';
	echo '<li>Le '
		. CHtml::link("partenaire " . CHtml::encode($partenaireSuivant->nom), ['/partenaire/view', 'id' => $partenaireSuivant->id])
		. ' suit cet éditeur dans Mir@bel.</li>';
	echo "</ul>\n";
}
?>

<?= $this->renderPartial('/global/_modif-verif', ['target' => $model]) ?>
