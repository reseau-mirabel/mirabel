<?php

/** @var Controller $this */
/** @var array $mapConfig */
assert($this instanceof Controller);

$this->pageTitle = "Carte des éditeurs";

$this->breadcrumbs = [
	'Éditeurs' => ['index'],
	'Carte',
];
?>

<h1>Localisation des éditeurs recensés dans Mir@bel</h1>

<?= (new \widgets\WorldMap($mapConfig))->run() ?>

<p>
	Cliquer sur un pays vous permet d'afficher les détails
	puis de rebondir sur les listes correspondantes d'éditeurs et de revues.
</p>
