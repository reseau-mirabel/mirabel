<?php

/** @var Controller $this */
/** @var Titre $model */
/** @var processes\titres\Delete $process */
/** @var bool $isRedirectionRequired */

assert($this instanceof Controller);

$this->pageTitle = 'Supprimer le titre ' . $model->titre;
$revueTitles = $model->revue->getTitresOrderedByObsolete();
$this->breadcrumbs = [
	'Revues' => ['/revue/index'],
	reset($revueTitles)->getFullTitle() => ['/revue/view', 'id' => $model->revueId],
	"[titre] " . $model->getFullTitle() => ['/titre/view', 'id' => $model->id],
	"Supprimer",
];
$domain = "reseau-mirabel.info";
?>

<h1>Supprimer le titre <em><?= CHtml::encode($model->titre) ?></em></h1>

<div class="alert alert-warning">
	<p>
		Vous vous apprêtez à supprimer un titre dans Mir@bel.
		Cette opération est <strong>possible</strong> mais reste <strong>rare et exceptionnelle</strong>
		en raison des alignements de données avec d'autres systèmes d'information.
	</p>

	<p>
		<strong>Si le titre n'est pas placé dans la bonne revue</strong>,
		ne le supprimez pas,
		mais contactez (<a href="mailto:suivi-veille@<?= $domain ?>">suivi-veille@<?= $domain ?></a>)
		un administrateur de Mir@bel pour qu'il déplace le titre.
	</p>
</div>

<?php
$constraints = $process->getConstraints();
if ($constraints) {
	?>
<section id="constraints">
	<h2>Contraintes bloquant la suppression</h2>
	<ol>
		<?php
		foreach ($constraints as $c) {
			echo CHtml::tag('li', ['class' => 'alert alert-danger'], $c);
		}
		?>
	</ol>
	<p>
		Vous pouvez éliminer ces points de blocage et recharger cette page
		ou
		<?= CHtml::link("Annuler et revenir à la page du titre", ['/titre/view', 'id' => $model->id], ['class' => 'btn']) ?>
	</p>
</section>
	<?php
}
?>

<section>
	<h2>Confirmer la suppression</h2>

	<?= CHtml::beginForm('', 'post', ['id' => 'confirm-delete']) ?>

	<?php
	if (!$constraints) {
		?>
		<p class="alert alert-info">
			Aucune contrainte n'empêche cette suppression : pas d'intervention en attente, ni de politique de publication, etc.
		</p>
		<?php
		if ($process->isRevueDeleted()) {
			echo "<p class=\"alert alert-info\">La revue associée à ce titre sera aussi supprimée car elle ne contient pas d'autre titre.</p>";
		}
		?>
		<div style="margin: 1ex 0 1em">
			<label>
				<?= $isRedirectionRequired ? "Choisissez" : "Éventuellement, choisissez" ?>
				quel titre remplacera celui-ci (redirection d'URL)
			</label>
			<?php
			$this->widget(
				'zii.widgets.jui.CJuiAutoComplete',
				[
					'name' => "redirection-autocomplete",
					'source' => <<<EOJS
						js:function(request, response) {
							$.ajax({
								url: "{$this->createUrl('/titre/ajax-complete')}",
								data: { term: request.term, excludeIds: [{$model->id}] },
								success: function(data) { response(data); }
							});
						}
						EOJS,
					'options' => [
						// min letters typed before we try to complete
						'minLength' => '1',
						'select' => <<<EOJS
							js:function(event, ui) {
								const escaped = $('<div/>').text(ui.item.value).html();
								$('#redirection').val(ui.item.id);
								$('#redirection-name').html(`[Titre ID = \${ui.item.id}] <a href="/titre/\${ui.item.id}" target="_blank">\${escaped}</a>`);
								$('#redirection-autocomplete').val('');
								$('#redirection-rm.hidden').toggleClass('hidden');
								$("#redirection-comment").css('font-weight', 'normal');
								return false;
							}
							EOJS,
					],
					'htmlOptions' => [
						'style' => 'width: 100%',
						'placeholder' => "titre, par ID ou par complétion du nom",
					],
				]
			);
			echo CHtml::hiddenField('redirection', '');
			?>
			<p>
				Attention, il faut choisir un <strong>titre</strong> et non une revue.
			</p>
			<button type="button" id="redirection-rm" class="pull-right hidden" onclick="$(this).toggleClass('hidden');$('#redirection').val('');$('#redirection-name').text(' ');">
				<span class="glyphicon glyphicon-remove"></span>
			</button>
			<div id="redirection-name" style="font-weight: bold"> </div>
			<div id="redirection-comment"><?= $isRedirectionRequired ? "Une nouvelle redirection est nécessaire pour cette suppression car cet objet est déjà enregistré dans la table des redirections." : "" ?></div>
		</div>
	<?php
	}
	?>

	<div style="margin-top:1ex">
		<label class="annoying">Écrivez « Je confirme » dans le champ ci-dessous</label>
		<?= CHtml::hiddenField('confirm', '1') ?>
		<?= CHtml::textField('annoying', '', ['placeholder' => "Je confirme", 'size' => '20', 'class' => 'annoying', 'autocomplete' => 'off', 'required' => true]) ?>
	</div>
	<div class="form-actions">
		<button class="btn btn-danger" type="submit" <?= ($constraints ? "disabled" : "") ?> >
			Supprimer définitivement ce titre
		</button>
		<?= CHtml::link("Annuler et revenir à la page du titre", ['/titre/view', 'id' => $model->id], ['class' => 'btn']) ?>
	</div>
	<?= CHtml::endForm() ?>

	<?php
	Yii::app()->getClientScript()->registerScript('confirm-delete', <<<EOJS
		$('#confirm-delete').on('submit', function(e) {
			if ($('input.annoying').val() !== "Je confirme") {
				e.preventDefault();
				$("label.annoying").css('font-weight', 'bold');
				return false;
			}
			return true;
		});
		EOJS
	);
	?>
</section>
