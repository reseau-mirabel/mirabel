<?php

/** @var Controller $this */
/** @var processes\titres\Editeurs $model */

assert($this instanceof TitreController);

$this->pageTitle = "Rôles des éditeurs pour " . $model->titre->titre;
$this->breadcrumbs = [
	'Revues' => ['/revue/index'],
	$model->titre->revue->getFullTitle() => ['/revue/view', 'id' => $model->titre->revueId],
	"Rôles des éditeurs",
];

Yii::app()->getClientScript()->registerCssFile('/css/glyphicons.css');
?>
<h1>
	Rôles des éditeurs pour <em><?= CHtml::encode($model->titre->titre) ?></em>
</h1>

<?php
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'titre-editeurs-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => false,
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'hints' => Hint::model()->getMessages('TitreEditeur'),
		'htmlOptions' => ['class' => 'well'],
	]
);
/** @var BootActiveForm $form */
?>

<?php
$publisherPossibleRoles = Editeur::getPossibleRoles();
$roleValues = ['' => "indéterminée"] + TitreEditeur::getPossibleRoles();
foreach ($model->editeurs as $editeur) {
	$te = $model->roles[$editeur->id];
	$roleLabel = $te->getAttributeLabel('role') ;
	if (!$editeur->role && !$te->role) {
		$roleLabel .= ' <span class="glyphicon glyphicon-exclamation-sign" title="Cette fonction est vide dans les données de Mir@bel."></span>';
	}
	?>
	<fieldset class="<?= $te->isComplete() ? "complete" : "incomplete" ?> editeur-<?= $editeur->id ?>">
		<legend style="display: flex">
			<div style="flex: 1">
				<?= $editeur->getSelfLink() ?>
				<span class="label">
					<?= $editeur->role ? ($publisherPossibleRoles[$editeur->role] ?? $editeur->role) : "type non défini" ?>
				</span>
			</div>
		</legend>
		<div class="fields">
			<div>
				<?php
				if (!$te->role && $editeur->role) {
					$roleEditeur = $publisherPossibleRoles[$editeur->role] ?? $editeur->role;
					echo CHtml::tag('div', ['style' => "float:right"], "Rôle par défaut de l'éditeur : <em>$roleEditeur</em>");
				}
				?>
				<?= $form->dropDownListRow($te, "[{$editeur->id}]role", $roleValues, ['labelOptions' => ['label' => $roleLabel]]) ?>
			</div>

			<div class="control-group">
				<label class="control-label"></label>
				<div class="controls">
					<div class="relation-titre-editeur">
						<label>
							<?= $form->checkBox($te, "[{$editeur->id}]ancien", ['uncheckValue' => 0, 'value' => 1]) ?>
							éditeur précédent
						</label>
					</div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label"></label>
				<div class="controls">
					<div class="relation-titre-editeur">
						<label>
							<?= $form->checkBox($te, "[{$editeur->id}]intellectuel", ['uncheckValue' => 0, 'value' => 1]) ?>
							<?= $te->getAttributeLabel('intellectuel') ?>
						</label>
					</div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label"></label>
				<div class="controls">
					<div class="relation-titre-editeur">
						<label>
							<?= $form->checkBox($te, "[{$editeur->id}]commercial", ['uncheckValue' => 0, 'value' => 1]) ?>
							<?= $te->getAttributeLabel('commercial') ?>
						</label>
					</div>
				</div>
			</div>
		</div>
	</fieldset>
	<?php
}
?>

<div class="form-actions">
	<button type="submit" class="btn btn-primary">Enregistrer les changements</button>
</div>

<?php
$this->endWidget();
