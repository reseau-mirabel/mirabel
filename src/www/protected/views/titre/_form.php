<?php

/** @var Controller $this */
/** @var Titre $model */
/** @var array $editeurNew */
/** @var bool $direct */
/** @var Intervention $intervention */
/** @var bool $forceProposition */
/** @var array $issns */
/** @var bool $titreTriable */

use components\HtmlHelper;
use components\Tools;

assert($this instanceof Controller);

$hints = [
	'Issn' => Hint::model()->getMessages('Issn'),
	'Titre' => Hint::model()->getMessages('Titre'),
];

$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'titre-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => false,
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'hints' => array_merge($hints['Issn'], $hints['Titre'], $hints),
	]
);
/** @var BootActiveForm $form */

$showIssnBlock = $model->isNewRecord || Tools::hasErrors($issns);
?>

<div class="alert">
	<?php
	if ($direct) {
		echo "Les modifications seront appliquées immédiatement.";
	} else {
		echo "Les modifications que vous proposez devront être validées "
			,"par un membre partenaire de Mi@bel avant d'être appliquées.";
	}
	?>
</div>

<fieldset>
	<legend>Titre</legend>
	<?php
	echo HtmlHelper::errorSummary(array_merge(['titre' => $model], $issns));
	if (Yii::app()->user->checkAccess("avec-partenaire")) {
		echo UrlFetchableValidator::toggleUrlChecker($model, $model->hasUrlError);
	}
	if ($model->revueId) {
		$this->renderPartial(
			'/intervention/_errors',
			[
				'form' => $form,
				'intervention' => $intervention,
				'model' => $model->revue,
			]
		);
	}

	echo $form->textFieldRow($model, 'titre', ['class' => 'span8']);
	if (isset($model->confirm) && $model->confirm === false) {
		?>
		<div class="control-group error">
			<div class="controls">
				<label>
					<input type="checkbox" name="Titre[confirm]" value="1" />
					<?= $model->getAttributeLabel('confirm') ?>
				</label>
			</div>
		</div>
		<?php
	}
	echo $form->textFieldRow($model, 'prefixe');
	echo $form->textFieldRow($model, 'sigle');
	$others = $model->getOtherTitles(true);
	if ($others) {
		$obsoleteByTitleName = $model->obsoletePar ? ($others[$model->obsoletePar] ?? "???") : '(dernier titre)';
		echo '<div class="control-group">';
		echo $form->textFieldRow($model, 'obsoletePar', [
			'value' => $obsoleteByTitleName,
			'readonly' => 'readonly',
			'disabled' => 'disabled',
			'class' =>'span8'
		]);
		echo "<div class=\"offset1 help-block\">Le tri des titres se fera dans un second temps</div>";
		echo "</div>";
	}


	if (!empty($issns)) {
		?>
		<div class="control-group ">
			<label class="control-label">Catalogues</label>
			<div class="controls">
				<?php
				if ($model->isNewRecord) {
					?>
					<p class="alert alert-info">
						C'est ici que vous devez ajouter un bloc supplémentaire de support/identifiants en plus de l'ISSN
						<em><?= isset($issns["new0"]->support) ? Issn::$enumSupport[$issns["new0"]->support] : "" ?></em>
						si cette revue est publiée en supports papier et électronique.
					</p>
					<?php
				} else {
					?>
					<p class="alert alert-info hidden" id="issn-changed-by-sudoc">
						Le web-service du Sudoc et le portail issn.org ont été interrogés pour ces ISSN et PPN,
						et <strong>des données ont été pré-remplies ou pré-corrigées automatiquement</strong> dans ce formulaire.
						Les champs modifiés sont signalés par la couleur verte dans le bloc des ISSN.
					</p>
					<?php
				} ?>
				<?php $this->renderPartial('_issn-table', ['issns' => $issns]); ?>
				<?php
				if (!$showIssnBlock) {
					?>
					<button type="button" class="btn btn-default" id="change-issns">Modifier le bloc catalogues</button>
					<?php
				} ?>
			</div>
		</div>
		<?php
	} elseif (!$showIssnBlock) {
		?>
		<button type="button" class="btn btn-default" id="change-issns">Modifier le bloc catalogues</button>
		<?php
	}
	?>
</fieldset>
<fieldset id="titre-issns"<?= $showIssnBlock ? "" : ' class="hidden"' ?> data-titreId="<?= intval($model->id) ?>">
	<legend>Catalogues : supports et identifiants associés</legend>
	<?php
	if (empty($issns)) {
		$newIssn = new Issn();
		$newIssn->titreId = (int) $model->id;
		$newIssn->support = Issn::SUPPORT_INCONNU;
		$issns = ["new0" => $newIssn];
	}
	foreach ($issns as $k => $issn) {
		$this->renderPartial(
			'_issn',
			[
				'form' => $form,
				'position' => $k,
				'issn' => $issn,
			]
		);
	}
	?>
	<div class="form-actions">
		<button type="button" class="btn btn-info" id="more-issn">
			Ajouter un bloc supplémentaire de support/identifiants
		</button>
	</div>
</fieldset>
<fieldset>
	<legend>Détails du titre de revue</legend>
	<?php
	echo $form->textFieldRow($model, 'url', ['class' => 'span8', 'type' => 'url']);
	echo $form->textFieldRow($model, 'urlCouverture', ['class' => 'span8', 'type' => 'url']);
	echo $form->checkBoxRow(
		$model,
		"urlCouvertureDld",
		[
			'title' => "Si cette case n'est pas cochée, rien ne sera changé pour la vignette.",
			'id' => 'update-image',
		]
	);

	echo $form->textFieldRow($model, 'dateDebut');
	echo $form->textFieldRow($model, 'dateFin');
	echo $form->autoCompleteRow(
		$model,
		'periodicite',
		[
			'source' => Titre::PERIODICITY_NORM,
			'minLength' => '0',
			'delay' => 0,
		]
	);

	echo $this->renderPartial(
		'/global/_autocompleteLang',
		[
			'model' => $model,
			'attribute' => 'langues',
			'form' => $form,
		]
	);
	?>
</fieldset>

<fieldset id="titre-liens">
	<legend>Liens extérieurs</legend>
	<?php
	$this->renderPartial(
		'/global/_form_links',
		[
			'form' => $form,
			'model' => $model,
			'embedded' => false,
		]
	);
	?>
	<?php
	$attributeTexts = \processes\attribut\DisplayedAttributes::getTexts($model);
	if (count($attributeTexts) > 0) {
		echo '<div class="control-group">'
			. '<label class="control-label"><abbr title="non modifiable">Labellisation</abbr></label>'
			. '<div class="controls">'
			. CHtml::tag(
				'ul',
				['class' => 'labellisation'],
				join("", array_map(
					function(string $x) {
						return "<li>$x</li>";
					},
					$attributeTexts
				))
			)
			. '</div>'
			. '</div>';
	}
	?>
</fieldset>

<fieldset id="editeurs">
	<legend>Éditeurs et responsabilités</legend>
	<?php
	$titreEditeurs = $model->titreEditeurs;
	$editeurs = $model->editeurs; // sorted by TE.ancien,E.nom
	$firstEditeur = true;
	if ($editeurs) {
		echo '<div class="control-group">'
			. '<label class="control-label">Éditeurs déjà associés</label>'
			. '<div class="controls">';
		foreach ($editeurs as $rid => $editeur) {
			$titreEditeur= $titreEditeurs[$rid];
			echo '<div class="checkbox">';
			echo CHtml::checkBox("TitreEditeur[{$rid}][editeurId]", true, ['uncheckValue' => 0, 'value' => $rid])
				. CHtml::label($editeur->getFullName(\Editeur::NAME_DATES), "TitreEditeur_{$rid}", ['class' => 'editeur-nom']);
			if (Yii::app()->params->itemAt('editeurs-roles')) {
				?>
				<div class="relation-titre-editeur">
					<div class="runin">
						<?php
						if ($firstEditeur) {
							$firstEditeur = false;
							echo HtmlHelper::getPopoverHint($hints['Titre']['editeurs'] ?? '', "Éditeurs du titre");
						}
						?>
					</div>
					<div>
						<label class="editeur-ancien">
							<?= CHtml::checkBox("TitreEditeur[{$rid}][ancien]", (bool) $titreEditeur->ancien, ['uncheckValue' => 0, 'value' => 1]) ?>
							 éditeur précédent
						</label>
					</div>
					<div>
						Rôle(s) :
						<label>
							<?= CHtml::checkBox("TitreEditeur[{$rid}][intellectuel]", (bool) $titreEditeur->intellectuel, ['uncheckValue' => 0, 'value' => 1]) ?>
							 direction &amp; rédaction
						</label>
						<label>
							<?= CHtml::checkBox("TitreEditeur[{$rid}][commercial]", (bool) $titreEditeur->commercial, ['uncheckValue' => 0, 'value' => 1]) ?>
							 publication &amp; diffusion
						</label>
					</div>
				</div>
				<?php
			}
			echo '</div>';
		}
		echo '</div>'
			. '</div>';
	}
	?>
	<div class="control-group">
		<label class="control-label">Éditeurs associés</label>
		<div class="controls">
		<?php
		if ($editeurNew) {
			foreach ($editeurNew as $editeur) {
				echo '<div class="checkbox">'
					. CHtml::checkBox("TitreEditeurNew[{$editeur->id}][editeurId]", true, ['uncheckValue' => 0, 'value' => $editeur->id])
					. CHtml::label($editeur->getFullName(), '', ['class' => 'editeur-nom'])
					. '</div>';
			}
		}

		// Add a new relation to "Editeur"
		echo '<div class="checkbox hidden" id="TitreEditeurNew">'
			. '<label class="editeur-nom">'
			. '<input type="checkbox" name="TitreEditeurNew[0][editeurId]" value="" /> '
			. '</label>';
		if (Yii::app()->params->itemAt('editeurs-roles')) {
			?>
			<div class="relation-titre-editeur">
				<div class="runin">
					<?php
					if ($firstEditeur) {
						$firstEditeur = false;
						echo HtmlHelper::getPopoverHint($hints['Titre']['editeurs'] ?? '', "Éditeurs du titre");
					}
					?>
				</div>
				<div>
					<label class="editeur-ancien">
						<?= CHtml::checkBox("TitreEditeurNew[0][ancien]", false, ['uncheckValue' => 0, 'value' => 1]) ?>
						 éditeur précédent
					</label>
				</div>
				<div>
					Rôle(s) :
					<label>
						<?= CHtml::checkBox("TitreEditeurNew[0][intellectuel]", false, ['uncheckValue' => 0, 'value' => 1]) ?>
						 direction &amp; rédaction
					</label>
					<label>
						<?= CHtml::checkBox("TitreEditeurNew[0][commercial]", false, ['uncheckValue' => 0, 'value' => 1]) ?>
						 publication &amp; diffusion
					</label>
				</div>
			</div>
			<?php
		}
		echo '</div>';
		$num = 0;
		$this->widget(
			'zii.widgets.jui.CJuiAutoComplete',
			[
				'name' => "TitreEditeurComplete",
				'source' => 'js:function(request, response) {
	$.ajax({
		url: "' . $this->createUrl('/editeur/ajax-complete') . '",
		data: { term: request.term, exclude: ' . CJavaScript::encode(array_keys($editeurs)) . ' },
		success: function(data) { response(data); }
	});
}',
				// additional javascript options for the autocomplete plugin
				// See <http://jqueryui.com/demos/autocomplete/#options>
				'options' => [
					// min letters typed before we try to complete
					'minLength' => '2',
					'select' => "js:function(event, ui) {
	addNewEditeur(ui.item.id, ui.item.label);
	jQuery('#TitreEditeurComplete').val('');
	return false;
}",
				],
				'htmlOptions' => [
					'class' => 'span6',
				],
			]
		);
		if (!empty($form->hints["TitreEditeurComplete"])) {
			echo HtmlHelper::getPopoverHint($form->hints['TitreEditeurComplete'], "Éditeurs associés");
		}
		?>
		</div>
	</div>
	<div class="control-group">
	<?php
	if (!Yii::app()->user->access()->toEditeur()->createDirect()) {
		?>
		<p>
			Si l'éditeur n'est pas déjà présent dans Mir@bel
			il n'apparaîtra pas dans les propositions au remplissage du champ "éditeurs associés".
			Dans ce cas merci de l'indiquer plus bas, dans le champ commentaire.
		</p>
	<?php
	} else {
		?>
		<label class="control-label">Associer à un éditeur nouvellement créé</label>
		<div class="controls">
			<?php
			echo CHtml::link(
				'Créer un nouvel éditeur',
				'#modal-editeur-create',
				[
					'class' => 'btn',
					'id' => 'modal-editeur-create-link',
				]
			);
			Yii::app()->clientScript->registerScript('open-modal', <<<'EOJS'
				document.querySelector("#modal-editeur-create-link").addEventListener("click", () => {
					document.querySelector("#modal-editeur-create").showModal();
				});
				EOJS
			);
			?>
		</div>
		<?php
	}
	?>
	</div>
</fieldset>

<?php
if (Yii::app()->user->checkAccess("avec-partenaire") && Yii::app()->user->checkAccess('possession/update')) {
	echo "<fieldset><legend>Suivi et possession</legend>";
	// "suivi" field
	if ($model->isNewRecord && $model->revueId == 0) {
		$htmlOptions = [];
		if ($model->suivre) {
			$htmlOptions = ['onclick' => 'return false', 'onkeydown' => 'return false;'];
		}
		echo $form->checkBoxRow($model, 'suivre', $htmlOptions);
	}

	// monitors list
	if ($model->revueId > 0) {
		$psuivant = $model->getPartenairesSuivant();
		if (!$psuivant) {
			echo "<p class=\"offset2\">Cette revue n'est actuellement pas suivie.</p>";
		} else {
			echo "<div class=\"offset2\">Cette revue est actuellement suivie par :<ul>";
			foreach ($psuivant as $partenaire) {
				echo "<li>" . $partenaire->getSelfLink() . "</li>";
			}
			echo "</ul></div>";
		}

		// owners list
		$owners = $model->getOwners();
		if (empty($owners)) {
			echo "<p class=\"offset2\">Aucun partenaire ne possède ce titre.</p>";
		} else {
			echo "<div class=\"offset2\">Les partenaires suivants possèdent ce titre :<ul>";
			foreach ($owners as $owner) {
				echo "<li>" . CHtml::encode($owner->getFullName()) . "</li>";
			}
			echo "</ul></div>";
		}
	}
	echo "</fieldset>";
}
?>

<?php
$this->renderPartial(
	'/intervention/_contact',
	[
		'form' => $form,
		'intervention' => $intervention,
		'canForceValidating' => true,
		'forceProposition' => $forceProposition,
	]
);
?>

<div class="form-actions">
	<button class="btn btn-primary" type="submit">
		<?= $direct ? 'Enregistrer' : 'Proposer cette modification' ?>
	</button>
</div>

<?php
$this->endWidget('titre-form');
?>

<dialog id="modal-editeur-create" class="modal">
<div>
	<div class="modal-header">
		<a class="close close-modal">&times;</a>
		<div id="editors-created"></div>
		<h3>Créer un nouvel éditeur</h3>
	</div>

	<div class="modal-body">
		<div id="embedded-editeur-idref"></div>
		<?= (new widgets\IdrefQuery)->run("embedded-editeur-idref") ?>
		<div id="embedded-editeur-form"></div>
	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-primary" id="submit-editeur">Enregistrer</button>
		<button type="button" class="btn close-modal">Annuler la création d'éditeur</button>
	</div>
</div>
</dialog>

<?php
$cs = Yii::app()->getClientScript();
if (Yii::app()->user->checkAccess("avec-partenaire")) {
	$jsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application') . '/widgets/assets/query-idref.js');
	$cs->registerScriptFile($jsUrl);
}
$cs->registerScript('_form-editeur', file_get_contents(__DIR__ . '/js/_form-editeur.js'));
$cs->registerScript('_form-issn', file_get_contents(__DIR__ . '/js/_form-issn.js'));
$cs->registerScript('_form-misc', file_get_contents(__DIR__ . '/js/_form-misc.js'));
