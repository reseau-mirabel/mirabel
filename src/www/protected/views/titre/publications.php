<?php

/** @var Controller $this */
/** @var string $doajUrl */
/** @var array $publications */
/** @var Titre $titre */

assert($this instanceof TitreController);

$this->pageTitle = 'Politiques de publication pour ' . $titre->titre;
$this->breadcrumbs = [
	'Revues' => ['/revue/index'],
	$titre->revue->getFullTitle() => ['/revue/view', 'id' => $titre->revueId],
	"Politique de publication" => ['/titre/publication', 'id' => $titre->id],
	'Détails',
];
$this->containerClass = "container full-width";

Yii::app()->getClientScript()->registerCssFile('/css/glyphicons.css');

$mirabelRevueUrl = $this->createUrl('/revue/view', ['id' => $titre->revueId, 'nom' => Norm::urlParam($titre->getFullTitle())]);
?>
<h1>
	<?= CHtml::encode($titre->getFullTitle()) ?>
	<a href="<?= CHtml::encode($mirabelRevueUrl) ?>" title="Voir la page principale de cette revue dans Mir@bel">
		<?= CHtml::image(Yii::app()->getBaseUrl() . '/images/logo-mirabel-carre.png', "Mir@bel", ['width' => 24, 'height' => 24]) ?>
	</a>
	<br>
	<small>Comparaison des politiques Sherpa - Mir@bel</small>
</h1>

<?php if ($doajUrl) { ?>
<p>
	Revue en accès ouvert,
	<?php
	$titleAttr = \processes\attribut\DisplayedAttributes::getValues($titre);
	if (isset($titleAttr['apc'])) {
		echo "<strong> " . ($titleAttr['apc'] ? 'avec': 'sans') . " frais pour l’auteur</strong>, ";
	}
	?>
	voir <a href="<?= CHtml::encode($doajUrl) ?>" title="Directory of Open Access Journals">DOAJ</a>.
</p>
<?php } ?>

<div class="columns">
<?php
foreach ($publications as $data) {
	echo "<article>";
	if ($data['source'] instanceof Politique) {
		echo '<h1 class="page-header">Politique <em>' . CHtml::encode($data['source']->name) . "</em></h1>";
	} else {
		echo '<h1 class="page-header">' . CHtml::encode($data['source']) . "</h1>";
	}
	$this->renderPartial('_publication', ['publication' => $data['publication'], 'titre' => $titre]);
	echo "</article>";
}
?>
</div>

<div style="margin-top: 1ex" class="liens">
	<p>
		<a href="<?= CHtml::encode($mirabelRevueUrl) ?>" title="Voir cette revue dans Mir@bel">
			<?= CHtml::image(Yii::app()->getBaseUrl() . '/images/logo-mirabel-carre.png', "Mir@bel", ['width' => 16, 'height' => 16]) ?>
		</a>
		Voir la <a href="<?= CHtml::encode($mirabelRevueUrl) ?>" title="Revue <?= CHtml::encode($titre->getFullTitle()) ?>">page principale</a> pour cette revue.
	</p>
</div>
