<?php

/** @var Controller $this */
/** @var string $doajUrl */
/** @var \models\sherpa\Publication $publication */
/** @var Titre $titre */
/** @var string $intro HTML */
/** @var ?\models\sherpa\Item $old */

assert($this instanceof TitreController);

$this->pageTitle = 'Politique de publication pour ' . $titre->titre;
$this->breadcrumbs = [
	'Revues' => ['/revue/index'],
	$titre->revue->getFullTitle() => ['/revue/view', 'id' => $titre->revueId],
	"Politique de publication",
];

Yii::app()->getClientScript()->registerCssFile('/css/glyphicons.css');

$mirabelRevueUrl = $this->createUrl('/revue/view', ['id' => $titre->revueId, 'nom' => Norm::urlParam($titre->getFullTitle())]);
?>
<h1>
	<?= CHtml::encode($titre->getFullTitle()) ?>
	<a href="<?= CHtml::encode($mirabelRevueUrl) ?>" title="Voir la page principale de cette revue dans Mir@bel">
		<?= CHtml::image(Yii::app()->getBaseUrl() . '/images/logo-mirabel-carre.png', "Mir@bel", ['width' => 24, 'height' => 24]) ?>
	</a>
	<br>
	<small>Politique en matière de droits, de dépôt et d'auto-archivage</small>
</h1>

<?php if ($doajUrl) { ?>
<p>
	Revue en accès ouvert,
	<?php
	$titleAttr = \processes\attribut\DisplayedAttributes::getValues($titre);
	if (isset($titleAttr['apc'])) {
		echo "<strong> " . ($titleAttr['apc'] ? 'avec': 'sans') . " frais pour l’auteur</strong>, ";
	}
	?>
	voir <a href="<?= CHtml::encode($doajUrl) ?>" title="Directory of Open Access Journals">DOAJ</a>.
</p>
<?php } ?>

<?php if ($publication->isHybrid()) { ?>
<p>
	<abbr title="Certaines modalités de publication sont soumises au paiement par l'auteur de frais de publication">Revue hybride</abbr>.
</p>
<?php } ?>

<div class="alert alert-info">
	<?= $intro ?>
</div>

<?= $this->renderPartial('_publication', ['publication' => $publication, 'titre' => $titre]) ?>

<div style="margin-top: 1ex" class="liens">
	<?php if ($publication->romeoUrl) { ?>
		<div>
			Voir la page <?= CHtml::link("Open policy finder", $publication->romeoUrl) ?> pour cette revue.
		</div>
	<?php } elseif ($old !== null && $old->getRomeoUrl()) { ?>
		<div>
			Voir la politique enregistrée par <?= CHtml::link("Open policy finder", $old->getRomeoUrl()) ?> pour cette revue.
		</div>
	<?php } ?>
	<p>
		<a href="<?= CHtml::encode($mirabelRevueUrl) ?>" title="Voir cette revue dans Mir@bel">
			<?= CHtml::image(Yii::app()->getBaseUrl() . '/images/logo-mirabel-carre.png', "Mir@bel", ['width' => 16, 'height' => 16]) ?>
		</a>
		Voir la <a href="<?= CHtml::encode($mirabelRevueUrl) ?>" title="Revue <?= CHtml::encode($titre->getFullTitle()) ?>">page principale</a> pour cette revue.
	</p>
</div>
