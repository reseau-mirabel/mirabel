<?php

/** @var Controller $this */
/** @var processes\titres\Identify $process */

assert($this instanceof Controller);

$revueTitles = $process->titre->revue->getTitresOrderedByObsolete();
$directAccess = Yii::app()->user->checkAccess("avec-partenaire") && Yii::app()->user->checkAccess('suivi', $process->titre);

$this->pageTitle = "Identification / " . $process->titre->titre;
$this->breadcrumbs = [
	'Revues' => ['/revue/index'],
	reset($revueTitles)->getFullTitle() => ['/revue/view', 'id' => $process->titre->revueId],
	"[titre] " . $process->titre->getFullTitle() => ['view', 'id' => $process->titre->id],
	"id internes",
];

\Yii::app()->getComponent('sidebar')->menu = [
	['label' => 'Voir ce titre', 'url' => ['view', 'id' => $process->titre->id]],
];
?>

<h1>ID internes pour <em><?= CHtml::encode($process->titre->getFullTitle()) ?></em></h1>

<p>
	Sur cette page, vous pouvez modifier les identifiants locaux à une ressource
	pour le titre <em><?= CHtml::encode($process->titre->getFullTitle()) ?></em>.
<p>
<dl>
	<dt>
		Si la ressource est liée par un accès en ligne,
	</dt>
	<dd>
		elle apparaît en début de liste, avec un formulaire par identification existante
		(donc aucun formulaire s'il y a un accès sans identification).
	</dd>
	<dt>
		Si la ressource déclare déjà une identification,
	</dt>
	<dd>
		mais sans accès en ligne, elle apparaît plus loin dans la liste,
		avec un formulaire par identification existante.
	</dd>
	<dt>
		Si la ressource n'est pas liée à ce titre,
	</dt>
	<dd>
		le formulaire en pied de page permet de la sélectionner (par complétion sur son nom)
		et d'ajouter une identification.
	</dd>
</dl>

<?php
/** @var BootActiveForm $form */
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'identification-form',
		'enableAjaxValidation' => false,
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'hints' => Hint::model()->getMessages('Identification'),
		'htmlOptions' => ['class' => 'well'],
	]
);
$options = ['class' => 'span5'];
foreach ($process->identifications as $rid => $idents) {
	?>
	<fieldset class="ressource-<?= $rid ?>">
		<legend>
			Ressource <em><?= CHtml::encode($process->ressources[$rid]->getFullName()) ?></em>
		</legend>
		<?php
		foreach ($idents as $num => $identification) {
			if ($num > 0) {
				// separator
				echo "<hr />";
			}
			echo $form->errorSummary($identification);

			echo $form->hiddenField($identification, "[$rid][$num]ressourceId");
			echo $form->textFieldRow($identification, "[$rid][$num]idInterne", $options);
		}
		?>
	</fieldset>
<?php
}
?>

<fieldset class="ressource-0">
	<legend>
		Nouvelle association
	</legend>
	<?php
	echo $form->errorSummary($process->creation);

	$this->renderPartial(
		'/global/_autocompleteField',
		[
			'model' => $process->creation, 'attribute' => '[0]ressourceId',
			'url' => '/ressource/ajax-complete',
			'form' => $form, 'value' => '',
		]
	);
	echo $form->textFieldRow($process->creation, "[0]idInterne", $options);
	?>
</fieldset>

<div class="form-actions">
	<?php
	$this->widget(
		'bootstrap.widgets.BootButton',
		[
			'buttonType' => 'submit',
			'type' => 'primary',
			'label' => 'Enregistrer',
		]
	);
	?>
</div>

<?php
$this->endWidget();
?>
