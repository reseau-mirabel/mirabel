<?php

/** @var Controller $this */
/** @var TitreController $this */
/** @var Issn[] $issns */
/** @var ?Issn $highlightIssn */
assert($this instanceof Controller);

if (empty($issns)) {
	return "";
}
?>
<table class="issns table table-condensed">
	<thead>
		<tr>
			<th>ISSN</th>
			<th>ISSN-E</th>
			<th>Début</th>
			<th>Fin</th>
			<th>ISSN-L</th>
			<th>PPN</th>
			<th>BNF</th>
			<th>Worldcat</th>
			<th>Pays</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($issns as $issn) { ?>
		<tr class="issn<?= !empty($highlightIssn) && $issn->id == $highlightIssn->id ? ' highlight' : '' ?>">
			<td class="issnp"><?php
				if ($issn->support === Issn::SUPPORT_PAPIER || $issn->support === Issn::SUPPORT_INCONNU) {
					if ($issn->support === Issn::SUPPORT_INCONNU) {
						echo CHtml::tag('i', ['class' => 'icon-warning-sign', 'title' => "Support indéterminé : modifier l'ISSN pour préciser le support papier ou électronique"], '') . " ";
					}
					echo $issn->getHtml(false, true);
				}
			?></td>
			<td class="issne"><?= ($issn->support === Issn::SUPPORT_ELECTRONIQUE ? $issn->getHtml(false, true) : '') ?></td>
			<td><?= CHtml::encode($issn->dateDebut) ?></td>
			<td><?= CHtml::encode($issn->dateFin) ?></td>
			<td class="issnl"><?= CHtml::encode($issn->issnl) ?></td>
			<td class="sudoc-ppn"><?= $issn->getSudocLink(false) ?></td>
			<td class="bnf-ark"><?= $issn->getBnfLink() ?></td>
			<td class="worldcat-ocn"><?= $issn->getWorldcatLink() ?></td>
			<td class="pays"><?= CHtml::encode($issn->getCountry()) ?></td>
		</tr>
		<?php } ?>
	</tbody>
</table>
