<?php

/** @var Controller $this */
/** @var Titre $titre */

$hasDoaj = $titre->liens->containsSource(1);
$attributs = \processes\attribut\DisplayedAttributes::getValues($titre);

if (!$hasDoaj && empty($attributs)) {
	return;
}
?>
<div class="logo-open-access">
	<?php
	if ($hasDoaj) {
		echo CHtml::image('/images/logo_libre_acces.png', "Libre accès / Open Access", ['width' => '64', 'height' => '32', 'title' => "Cette revue est en libre accès, au sens du DOAJ."]);
	}
	?>
</div>
