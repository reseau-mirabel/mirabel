<?php

/** @var Controller $this */
/** @var Titre $model */

assert($this instanceof Controller);

$strTitle = $model->getFullTitle();

$this->pageTitle = "Changer la revue du titre " . $model->titre;
$this->breadcrumbs = [
	'Revues' => ['/revue/index'],
	$strTitle => ['/revue/view', 'id' => $model->revueId],
	"[titre] $strTitle" => ['view', 'id' => $model->id],
	"Changer la revue",
];

\Yii::app()->getComponent('sidebar')->menu = [
	['label' => 'Voir ce titre', 'url' => ['view', 'id' => $model->id]],
];
?>

<h1>Changer la revue de <em><?= CHtml::encode($strTitle) ?></em></h1>

<p>
	Cette opération n'est accessible qu'aux administrateurs.
<p>

<?php
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'changeJournal-form',
		'enableAjaxValidation' => false,
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'htmlOptions' => ['class' => 'well'],
	]
);
/** @var BootActiveForm $form */
?>
<div class="control-group">
	<label class="control-label required" for="revueId">
		Identifiant de la revue
	</label>
	<div class="controls">
		<input type="text" name="revueId" placeholder="0 pour créer une nouvelle revue" />
	</div>
</div>

<div class="form-actions">
	<?php
	$this->widget(
		'bootstrap.widgets.BootButton',
		[
			'buttonType' => 'submit',
			'type' => 'primary btn-danger',
			'label' => 'Enregistrer',
		]
	);
	?>
</div>

<?php
$this->endWidget();
?>
