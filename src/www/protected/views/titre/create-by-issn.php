<?php

/** @var TitreController $this */
/** @var IssnForm $model */
/** @var ?Revue $revue */
/** @var ?array $extra */

assert($this instanceof Controller);

$this->pageTitle = "Nouveau titre";
$this->breadcrumbs = ['Revues' => ['/revue/index']];
if ($revue) {
	$revueTitle = $revue->getFullTitle();
	$this->pageTitle .= " pour " . $revueTitle;
	$this->breadcrumbs[$revueTitle] = ['/revue/view', 'id' => $revue->id];
}
$this->breadcrumbs[] = "Créer un titre par ISSN";
?>

<h1><?= CHtml::encode($this->pageTitle) ?></h1>

<?php
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'issn-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => false,
		'type' => BootActiveForm::TYPE_HORIZONTAL,
	]
);
/** @var BootActiveForm $form */
?>

<div class="alert alert-info">
	Saisissez un ou plusieurs ISSN pour préremplir les données du titre grâce au service Sudoc français,
	ou passez à l'étape suivante si ce titre n'a pas d'ISSN.
	<?php if (Yii::app()->user->checkAccess("avec-partenaire")) { ?>
	Pour mémoire vous pouvez consulter une
	<?= CHtml::link("page récapitulative sur la création de revue", ['/site/page', 'id' => 'Création de revue']) ?>
	dans Mir@bel.
	<?php } ?>
</div>

<?php if ($extra) { ?>
<div class="alert alert-info">
	En complément de ce que Mir@bel peut recevoir du Sudoc, les données suivantes seront appliquées pour cette création :
	<dl class="dl-horizontal">
		<?php
		if (isset($extra['issnSupport'])) {
			foreach ($extra['issnSupport'] as $k => $v) {
				?>
				<dt>ISSN <?= CHtml::encode($k) ?></dt>
				<dd>support <?= CHtml::encode($v) ?></dd>
				<?php
			}
		}
		?>
		<?php
		if (isset($extra['publishers'])) {
			foreach ($extra['publishers'] as $v) {
				?>
				<dt>Éditeur</dt>
				<dd><?= CHtml::encode($v) ?></dd>
				<?php
			}
		}
		?>
	</dl>
</div>
<?php } ?>

<?= preg_replace_callback(
	'/\b(\d{4}-\d{3}[\dX])\b/',
	function ($matches) {
		return CHtml::Link($matches[0], ['/revue/viewBy', 'by' => 'issn', 'id' => $matches[1]]);
	},
	$form->errorSummary($model)
) ?>

<div id="titre-issns" class="issn-block">
	<?= $form->hiddenField($model, 'revueId') ?>
	<?= $form->textFieldRow($model, 'issn', ['class' => 'issn-number input-xlarge']) ?>
	<div class="alert alert-error hidden"></div>
</div>

<div class="form-actions">
	<button type="submit" class="btn btn-success" id="create-with-issn" name="ok" value="1">
		Créer un titre avec ces ISSN
	</button>
	<span class="icon-loading masked" style="margin-right: 1ex">
		<?= CHtml::image(Yii::app()->getBaseUrl() . '/images/hourglass.png', 'loading...') ?>
	</span>
	<?= CHtml::link(
		"Créer un titre <strong>sans pré-remplissage ISSN</strong>",
		['/titre/create', 'revueId' => $revue ? $revue->id : null],
		['class' => 'btn btn-primary', 'id' => 'create-without-issn']
	) ?>
</div>

<div id="sudoc-info">
</div>
<?php
$this->endWidget();

$cs = Yii::app()->getClientScript();
/** @var CClientScript $cs */
$cs->registerScript('create-by-issn', file_get_contents(__DIR__ . '/js/_create-by-issn.js'));
