<?php

/** @var Controller $this */
/** @var Titre $model */
/** @var array $editeurNew */
/** @var bool $direct */
/** @var Intervention $intervention */
/** @var bool $forceProposition */
/** @var array $issns */
assert($this instanceof Controller);

$this->pageTitle = 'Nouveau titre';
$this->breadcrumbs = [
	'Revues' => ['/revue/index'],
];
if (empty($model->revueId)) {
	$this->breadcrumbs[] = 'Nouvelle revue';
} else {
	$this->breadcrumbs[$model->revue->getFullTitle()] = ['/revue/view', 'id' => $model->revueId];
	$this->breadcrumbs[] = 'Nouveau titre';
}
?>

<h1>Créer <?= (empty($model->revueId) ? 'une nouvelle revue' : 'un nouveau titre') ?></h1>

<?php
echo $this->renderPartial(
	'_form',
	[
		'model' => $model,
		'editeurNew' => $editeurNew,
		'revue' => false,
		'direct' => $direct,
		'intervention' => $intervention,
		'forceProposition' => $forceProposition,
		'issns' => $issns,
	]
);

if (!$model->hasErrors()) {
	// automatic querying of Sudoc
	Yii::app()->getClientScript()->registerScript(
		'auto-query-sudoc',
		'$("#titre-issns input.issn-number").trigger("input");'
	);
}
