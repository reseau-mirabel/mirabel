<?php

use models\sherpa\Publication;

/** @var Controller $this */
/** @var \models\sherpa\Publication $publication */
/** @var Titre $titre */

?>
<div class="sherpa-oa">
	<?php
	Yii::app()->getClientScript()->registerScript(
		'popover', // https://getbootstrap.com/2.3.2/javascript.html#popovers
		<<<EOJS
		$('.help-popover').popover({
			container: 'body',
			trigger: 'click',
			html: true,
		}).on('click', function(event) {
			var target = $(this);
			$("body").one("click", function(e) {
				target.popover('hide');
			});
			event.preventDefault();
			return false;
		});
		EOJS
	);
	$popoverOptions = ['class' => 'help-popover'];

	$helpEmbargo = \Config::read('sherpa.aide.embargo');
	if ($helpEmbargo) {
		$helpEmbargoLink = ' '
			. CHtml::link("(aide)", "#", array_merge($popoverOptions, ['data-title' => "Explication des embargos", 'data-content' => $helpEmbargo]));
	} else {
		$helpEmbargoLink = "";
	}
	unset($helpEmbargo);

	$helpFee = \Config::read('sherpa.aide.frais');
	if ($helpFee) {
		$helpFeeLink = CHtml::link(
			"(aide)",
			"#",
			array_merge($popoverOptions, ['data-title' => "Explication des frais additionnels", 'data-content' => $helpFee])
		);
	} else {
		$helpFeeLink = "";
	}
	unset($helpFee);

	$helpLicense = \Config::read('sherpa.aide.licence');
	if ($helpLicense) {
		$helpLicenseLink = ' '
			. CHtml::link("(aide)", "#", array_merge($popoverOptions, ['data-title' => "Pour en savoir plus sur les licences…", 'data-content' => $helpLicense]));
	} else {
		$helpLicenseLink = "";
	}
	unset($helpLicense);

	$versionsTitles = [
		'published' => "Version publiée",
		'accepted' => "Version acceptée",
		'submitted' => "Version soumise",
	];
	if (!$publication->policy) {
		echo "<p class=\"alert alert-error\">Cette revue n'a pas de politique connue pour son mode de publication.</p>";
	}
	foreach ($versionsTitles as $version => $versionFr) {
		$oas = $publication->getOaByVersion($version);
		echo '<section>';
		$help = \Config::read("sherpa.aide.version.$version");
		if ($help) {
			$helpLink = CHtml::link("(aide)", "#", array_merge($popoverOptions, ['data-title' => "$versionFr", 'data-content' => $help]));
		} else {
			$helpLink = "";
		}
		$h2Options = ['class' => 'collapse-toggle toggled', 'data-collapse-target' => ".version-$version"];
		if (count($oas) === 0) {
			$summary = '<span class="oa-summary"><span class="glyphicon glyphicon-remove" title="Non autorisée"></span></span>';
			echo CHtml::tag('h2', $h2Options, "$versionFr $summary $helpLink");
			echo '<div class="collapse-target in version-' . $version . '">';
		} elseif (count($oas) === 1) {
			echo CHtml::tag('h2', $h2Options, "$versionFr {$oas[0]->getSummaryIcons()} $helpLink");
			echo '<div class="collapse-target in version-' . $version . '">';
		} else {
			echo CHtml::tag('h2', $h2Options, "$versionFr $helpLink");
			echo '<div class="oa-options version-' . $version . '">';
		}
		unset($help);

		if (!$oas) {
			?>
			<table class="table-condensed table-hover <?= (count($oas) > 1 ? "collapse-target in version-$version" : '') ?>">
				<tbody>
					<tr>
						<td>
							<span class="glyphicon glyphicon-remove" title="Non autorisée"></span>
							<strong>Diffusion non autorisée</strong>
						</td>
					</tr>
				</tbody>
			</table>

			<?php
		}
		foreach ($oas as $num => $oa) {
			/** @var \models\sherpa\Oa $oa */
			if (count($oas) > 1) {
				echo "<div>";
				echo CHtml::tag(
					'h3',
					['class' => 'collapse-toggle-show', 'data-collapse-target' => ".version-$version"],
					"Option " . ((string) ($num + 1)) . $oa->getSummaryIcons()
				);
			} ?>
			<table class="table-condensed table-hover <?= (count($oas) > 1 ? "collapse-target in version-$version" : '') ?>">
				<tbody>
					<?php if ($oa->prerequisites) { ?>
					<tr>
						<td>
							<span class="glyphicon glyphicon-check"></span>
							Il existe des <strong>prérequis</strong> pour cette option
							<?php
							if (!$publication->romeoUrl) {
								echo " :";
								$trPre = \Config::read('politique.liste.prerequisites');
								$translator = [];
								foreach ($trPre as $t) {
									$translator[$t[0]] = $t[1];
								}
								echo '<ul>';
								foreach ($oa->getUpstream()->prerequisites->prerequisites as $pre) {
									echo "<li>" . ($translator[$pre] ?? $pre) . "</li>";
								}
								echo "</ul>";
							} else {
								echo ", voir ", CHtml::link("<em>Prerequisites</em> sur Open policy finder", $publication->romeoUrl), ".";
							}
							?>

						</td>
					</tr>
					<?php } ?>
					<?php if (in_array('this_journal', $oa->locations) || in_array('any_website', $oa->locations)) { ?>
					<tr>
						<td>
							<span class="glyphicon glyphicon-eye-open"></span>
							<?php
							if (in_array('this_journal', $oa->locations) && in_array('any_website', $oa->locations)) {
								echo "Publication en accès ouvert sur le site de l'éditeur et autorisée sur tout site web";
							} elseif (in_array('this_journal', $oa->locations)) {
								echo "Publication en accès ouvert sur le site de l'éditeur";
							} else {
								echo "Publication autorisée sur tout site web";
							}
							?>
							<a href="<?= CHtml::encode($titre->url) ?>"><span class="glyphicon glyphicon-link"></span></a>
						</td>
					</tr>
					<?php } ?>
					<?php if ($oa->additionalOaFee) { ?>
					<tr>
						<td>
							<span class="glyphicon glyphicon-euro"></span>
							<strong>Option au choix de l’auteur soumise à des frais de publication en accès ouvert</strong>
							<?= $helpFeeLink ?>
						</td>
					</tr>
					<?php } ?>
					<tr>
						<td>
							<span class="glyphicon glyphicon-hourglass<?= (empty($oa->embargo) ? " glyphicon-slashed" : "") ?>"></span>
							<strong><?= (empty($oa->embargo) ? "Pas d'embargo" : "Embargo de {$oa->getPrintableEmbargo()}") ?></strong>
							<?= $helpEmbargoLink ?>
						</td>
					</tr>
					<?php if ($oa->licenses) { ?>
					<tr>
						<td>
							<span class="cc-icon"><img src="/images/icons/cc.svg" width=14 height=14 /></span>
							Licence Creative Commons <strong><?= join("</strong> ou <strong>", $oa->licenses) ?></strong>
							<?= $helpLicenseLink ?>
						</td>
					</tr>
					<?php } ?>
					<?php if ($oa->copyrightOwner) { ?>
					<tr>
						<td>
							<span class="glyphicon glyphicon-copyright-mark"></span>
							Détenteur des droits :
							<strong><?= $oa->getPrintableCopyrightOwner() ?></strong>
						</td>
					</tr>
					<?php } ?>
					<?php
					$locations = $oa->getPrintableLocations('');
					if ($locations) { ?>
					<tr>
						<td>
							<span class="glyphicon glyphicon-education"></span>
							Emplacements autorisés :
							<div class="block-value"><?= join("<br>", $locations) ?></div>
						</td>
					</tr>
					<?php } ?>
					<?php if ($oa->conditions) { ?>
					<tr>
						<td>
							<span class="glyphicon glyphicon-list"></span>
							Conditions :
							<div class="block-value"><?= join("<br>", $oa->getPrintableConditions()) ?></div>
						</td>
					</tr>
					<?php } ?>
					<?php if ($oa->publicNotes) { ?>
					<tr>
						<td>
							<span class="glyphicon glyphicon-edit"></span>
							Il existe des <strong>remarques</strong> pour cette option
							<?php
							if ($publication->romeoUrl) {
								echo ", voir ", CHtml::link("<em>Notes</em> sur Open policy finder", $publication->romeoUrl), ".";
							} else {
								echo " :";
								$trNotes = \Config::read('politique.liste.public_notes');
								$translator = [];
								foreach ($trNotes as $t) {
									$translator[$t[0]] = $t[1];
								}
								echo '<ul>';
								foreach ($oa->getUpstream()->public_notes as $note) {
									echo "<li>" . ($translator[$note] ?? $note) . "</li>";
								}
								echo "</ul>";
							}
							?>
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
			<?php
			if (count($oas) > 1) {
				echo "</div>"; // option
			}
		}
		echo '</div>'; // .oa-options
		echo '</section>'; // version
	}
	?>
</div>

<?php
$openAccess = $publication->getOpenAccess();
if (in_array($openAccess, [Publication::ACCESS_CLOSED, Publication::ACCESS_MIXED], true) || $publication->getResourceUrls()) {
	?>
<div class="sherpa-general">
	<h2>Informations sur la politique de publication</h2>
	<?php
	if ($openAccess === Publication::ACCESS_CLOSED) {
		echo "<div>L'éditeur refuse l'accès ouvert.</div>";
	} elseif ($openAccess === Publication::ACCESS_MIXED) {
		echo "<div>L'éditeur peut refuser l'accès ouvert.</div>";
	}
	?>
	<div>
		<ul>
			<?php
			foreach ($publication->getResourceUrls() as $name => $url) {
				echo CHtml::tag('li', [], CHtml::link($name, $url));
			}
			?>
		</ul>
	</div>
</div>
<?php
}
?>

<?php
if (
	(count($publication->preferredTitle) > 1)
	|| (count($publication->preferredTitle) > 0 && $publication->preferredTitle !== $titre->titre)
) {
	?>
<div class="sherpa-titles" style="margin-top: 2ex">
	<h2>Titre et éditeur dans Open policy finder (Sherpa Romeo)</h2>
	<ul>
	<?php
	foreach ($publication->preferredTitle as $title) {
		echo '<li>' . CHtml::encode($title) . '</li>';
	}
	?>
	</ul>
	<?php
	if ($publication->publishers) {
		echo "<p>Publié par :";
		echo '<ul class="editeurs">';
		foreach ($publication->publishers as $publisher) {
			echo '<li>';
			echo CHtml::encode($publisher->name[0]);
			if ($publisher->relationship) {
				echo ", ", $publisher->getPrintableRelationship();
			}
			echo '</li>';
		}
		echo '</ul>';
	}
	?>
</div>
<?php
}
?>
<div>
	<?php
	if ($publication->dateModified) {
		echo "Dernière modification : {$publication->dateModified}";
	}
	?>
</div>
