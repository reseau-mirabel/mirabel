function closeModal() {
	$('#modal-editeur-create #embedded-editeur-form').html('');
	IdrefQuery.reset();
	document.querySelector('#embedded-editeur-idref').removeAttribute('style');
	document.querySelector('#modal-editeur-create #submit-editeur').setAttribute('style', 'display:none;');
	document.querySelector('#modal-editeur-create').close();
}
$('#modal-editeur-create .close-modal').on('click', closeModal);

$('#modal-editeur-create #submit-editeur').on('click', function() {
	if (this.disabled) {
		return;
	}
	$('#editeur-form').submit();
});

let lastEditeurHash = "";
$('#modal-editeur-create').on('submit', '#editeur-form', function(ev) {
	ev.preventDefault();
	if ($("#editeur-duplicate-id").val() === lastEditeurHash) {
		console.log("Demande déjà envoyée, ignore la soumission");
		return false;
	}
	lastEditeurHash = $("#editeur-duplicate-id").val();
	$.ajax({
		type: 'POST',
		url: $(this).attr("action"),
		data: $(this).serialize(),
		success: function (data) {
			if (data.result === 'success') {
				addNewEditeur(data.id, data.label);
				$("#modal-editeur-create-link").closest('.controls').append(
					'<div class="alert alert-success">' + data.message + '</div>'
				);
				closeModal();
			} else {
				lastEditeurHash = "";
				$('#modal-editeur-create #embedded-editeur-form').html(data.html);
			}
		},
		dataType: 'json'
	});
	return false;
});
$('#modal-editeur-create').on('click', 'button.link-publisher', function() {
	const btn = $(this);
	const id = btn.attr('data-id');
	const name = btn.attr('data-name');
	closeModal();
	addNewEditeur(id, name);
})

function addNewEditeur(id, label) {
	$('#TitreEditeurNew').clone()
		.find('label.editeur-nom').append(label).end()
		.find('label.editeur-nom input').val(id).attr('checked', 'checked').end()
		.find('a[rel="popover"]').addClass("titre-editeurs-hint").end()
		.html(function(index, oldhtml) {
			return oldhtml.replaceAll('TitreEditeurNew[0]', 'TitreEditeurNew[' + id + ']');
		})
		.insertBefore('#TitreEditeurComplete').removeClass('hidden');
	$('a.titre-editeurs-hint').filter(function(index) { return index > 0; }).remove();
	$('a.titre-editeurs-hint').popover();
	return true;
}
