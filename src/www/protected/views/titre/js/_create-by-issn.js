const sudocInfo = document.querySelector('#sudoc-info');
const sudocAlert = document.querySelector('#titre-issns > .alert');
const buttonWith = document.querySelector('#create-with-issn');
const buttonWithout = document.querySelector('#create-without-issn');
const icon = document.querySelector('.icon-loading');
let lastIssn = "";

$(".issn-number").on("input", onIssnInput);
onIssnInput(); // Once DOM is loaded.

function onIssnInput() {
	const issnInput = $(".issn-number").val().trim();

	// valid issns?
	const validIssns = extractIssnList(issnInput);
	if (validIssns.length === 0) {
		buttonWith.setAttribute('disabled', 'disabled');
		buttonWithout.removeAttribute('disabled');
		return;
	}
	buttonWith.removeAttribute('disabled');
	buttonWithout.setAttribute('disabled', 'disabled');
	const issns = validIssns.join(" ");

	// new input?
	if (issns === lastIssn) {
		return;
	}
	lastIssn = issns;

	// remove the server-static alerts
	$(".alert-block.alert-error, .control-group.error .help-inline").remove();
	$("#titre-issns .error").removeClass('error');

	// send these ISSN numbers
	icon.classList.remove("masked");
	$.ajax({
		type: 'GET',
		url: '/titre/ajax-sudoc',
		data: { issns: issns, withHtml: 1 }
	}).done(function (data) {
		sudocInfo.innerHTML = data.html;

		if (countDistinctIssnl(data.notices) > 1) {
			sudocAlert.innerHTML = "Ces ISSN ont des <strong>ISSN-L différents</strong>. Veuillez vérifier qu'ils pointent bien sur la même revue.";
			sudocAlert.classList.remove('hidden');
		} else {
			sudocAlert.innerHTML = "";
			sudocAlert.classList.add('hidden');
		}

		if (data.duplicates.length > 0) {
			buttonWith.setAttribute('disabled', 'disabled');
			buttonWithout.removeAttribute('disabled');
			let links = [];
			for (let row of data.duplicates) {
				links.push(`<a href="/revue/${row.revueId}" target="_blank">${row.issn}</a>`);
			}
			sudocAlert.innerHTML = (links.length > 1 ? "Ces ISSN sont déjà présents" : "Cet ISSN est déjà présent")
				+ " dans Mir@bel : " + links.join(", ");
			sudocAlert.classList.remove('hidden');
		}
	}).fail(function (jqXHR, textStatus, errorThrown) {
		var errorMsg;
		try {
			errorMsg = jqXHR.responseJSON.html;
		} catch (e) {
			errorMsg = "<div>Le web-service proxy de Mirabel sur le Sudoc a rencontré une erreur : <strong>"
				+ htmlEscape(jqXHR.responseText)
				+ "</strong></div>"
				+ "<p>Nous vous conseillons d'attendre le rétablissement du service du Sudoc pour créer votre revue. Si vous voulez néanmoins la créer maintenant, utilisez le bouton « Créer un titre sans ISSN » (vous pourrez remplir tous les champs, y compris ISSN, sans pré-remplissage par le service Sudoc).</p>";
		}
		sudocAlert.innerHTML = errorMsg;
		sudocAlert.classList.remove('hidden');
		// set buttons
		buttonWith.setAttribute('disabled', 'disabled');
		buttonWithout.removeAttribute('disabled');
	}).always(function() {
		icon.classList.add("masked");
	});
}

function extractIssnList(issntext) {
	const validIssns = [];
	for (let match of issntext.matchAll(/\d{4}-?\d{3}[\dX]/g)) {
		validIssns.push(match[0]);
	}
	return validIssns;
}

function countDistinctIssnl(notices) {
	const issnls = new Set();
	for (let notice of notices) {
		if (notice.issnl) {
			issnls.add(notice.issnl);
		}
	}
	return issnls.size;
}
