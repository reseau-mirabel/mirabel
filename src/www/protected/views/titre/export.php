<?php

/** @var Controller $this */
assert($this instanceof Controller);

?>
<h1>Exporter les titres</h1>

<ul>
	<li>
		<?= CHtml::link("Titres avec leurs identifiants", ['/titre/export', 'type' => 'identifiants']) ?>
		Cf #3223 et cf 5529 pour la version non connectée (allégée).
	</li>
	<li>
		<?= CHtml::link("Titres avec plein d'infos", ['/titre/export', 'type' => 'maxi']) ?>
		Cf #3510
	</li>
</ul>
