<?php

/** @var Controller $this */
/** @var Titre $model */
/** @var ?Intervention $lastUpdate */
/** @var array $autresTitres */
/** @var list{string, string}[] $variantes [variante, langue][] */

assert($this instanceof Controller);

$revueTitles = $model->revue->getTitresOrderedByObsolete();
$directAccess = Yii::app()->user->checkAccess("avec-partenaire") && Yii::app()->user->checkAccess('suivi', $model);
$collections = $model->collections;

$this->pageTitle = 'Détails du titre ' . $model->titre;
$this->breadcrumbs = [
	'Revues' => ['/revue/index'],
	reset($revueTitles)->getFullTitle() => ['/revue/view', 'id' => $model->revueId],
	"[titre] " . $model->getFullTitle(),
];

if (Yii::app()->user->checkAccess("avec-partenaire")) {
	$orphan = '';
	foreach ($model->editeurs as $e) {
		$count = TitreEditeur::model()->countByAttributes(['editeurId' => $e->id]);
		if ($count < 2) {
			$orphan .= "\nSupprimer ce titre rendra orphelin l'éditeur : " . $e->nom;
		}
	}
	\Yii::app()->getComponent('sidebar')->menu = [
		['label' => 'Modifier ce titre', 'url' => ['update', 'id' => $model->id]],
		[
			'label' => 'Changer sa revue',
			'url' => ['change-journal', 'id' => $model->id],
			'visible' => Yii::app()->user->access()->toTitre()->admin(),
		],
		[
			'label' => "Supprimer…",
			'url' => ['/titre/delete', 'id' => $model->id],
			'visible' => Yii::app()->user->access()->toTitre()->delete($model),
		],
		['label' => ' ', 'itemOptions' => ['class' => 'nav-header']],
		['label' => 'Créer un titre (même revue)', 'url' => ['create-by-issn', 'revueId' => $model->revueId]],
		['label' => 'Titre — Ressource', 'itemOptions' => ['class' => 'nav-header']],
		[
			'label' => 'Ajouter un accès en ligne',
			'url' => ['/service/create', 'titreId' => $model->id, 'revueId' => $model->revueId],
		],
		[
			'label' => 'Éditer les id internes',
			'url' => ['/titre/identification', 'id' => $model->id],
		],
		[
			'label' => 'Gérer les collections liées',
			'url' => ['/titre/collection', 'id' => $model->id],
			'visible' => $model->hasPotentialCollections(),
		],
	];
}

?>

<?= $this->renderPartial('/global/_search-navigation', ['searchNavigation' => $this->loadSearchNavigation(), 'currentId' => (int) $model->id, 'attr' => 'titre']); ?>

<h1>
	Détail du Titre <em><?= CHtml::encode($model->getFullTitle()); ?></em>
	<?= $this->renderPartial('/titre/_logos-attributs', ['titre' => $model]) ?>
</h1>

<?php
$editeurs = [];
$rolesE = Editeur::getPossibleRoles();
$rolesTE = TitreEditeur::getPossibleRoles();
foreach ($model->titreEditeurs as $te) {
	$editeur = $te->editeur;
	if ($te->role) {
		$role = $rolesTE[$te->role] ?? "{$te->role} (pas de traduction)";
	} elseif ($editeur->role) {
		$role = $rolesE[$editeur->role] ?? "{$editeur->role} (pas de traduction)";
	} else {
		$role = "fonction éditoriale indéterminée";
	}
	$editeurs[] = '<div>' . $te->editeur->getSelfLink(Editeur::NAME_DATES)
		. ($te->ancien === null ? ' <span class="label label-important">éditeur précédent: indéterminé</span>' : ($te->ancien ? ' <span class="label label-info">éditeur précédent</span>' : ""))
		. ($te->commercial ? ' <span class="label">publication &amp; diffusion</span>' : "")
		. ($te->intellectuel ? ' <span class="label">direction &amp; rédaction</span>' : "")
		. " <span class=\"label\">{$role}</span>"
		. '</div>';
}
if (Yii::app()->user->access()->toTitre()->relationEditeurs($model)) {
	$imgUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets'))
		. '/gridview';
	$editeurs[] = CHtml::link(
		CHtml::image($imgUrl . '/update.png', "Modifier les relation du titre à ses éditeurs"),
		['/titre/editeurs', 'id' => $model->id]
	);
}
$attributeValues = \processes\attribut\DisplayedAttributes::getValues($model);
$attributeTexts = \processes\attribut\DisplayedAttributes::valuesToTexts($attributeValues);
$this->widget(
	'bootstrap.widgets.BootDetailView',
	[
		'data' => $model,
		'attributes' => [
			'id',
			'revueId',
			'titre',
			'prefixe',
			'sigle',
			[
				'name' => 'obsoletePar',
				'type' => 'raw',
				'value' => $model->obsoletePar ? $model->obsoleteParTitre->getSelfLink() : '',
			],
			'dateDebut',
			'dateFin',
			[
				'label' => "Catalogues",
				'type' => 'raw',
				'value' => $this->renderPartial(
					'_issn-table',
					['issns' => $model->issns, 'highlightIssn' => $model->getPreferedIssn()],
					true
				),
			],
			'url:url',
			'urlCouverture:url',
			[
				'name' => "Labellisation",
				'type' => 'raw',
				'value' => CHtml::tag('ul', ['class' => 'labellisation'], join("", array_map(
					function (string $x) { return "<li>$x</li>"; },
					$attributeTexts
				))),
				'visible' => count($attributeTexts) > 0,
			],
			[
				'label' => "Frais de publication",
				'type' => 'raw',
				'value' => isset($attributeValues['apc']) && $attributeValues['apc'] ?
					'Avec frais de publication (<abbr title="Article Processing Charge">APC</abbr>)'
					: "Sans frais de publication (modèle diamant)",
				'visible' => isset($attributeValues['apc']),
			],
			[
				'name' => 'liensJson',
				'type' => 'raw',
				'value' => $model->getDetailedLinksOther(),
			],
			'liensInternes:raw',
			[
				'label' => "Variantes de nom",
				'type' => 'raw',
				'value' => components\HtmlTable::build($variantes, ["Titre", "Langue"]),
				'visible' => count($variantes) > 0,
			],
			[
				'label' => "Politique de publication",
				'type' => 'raw',
				'value' => $model->getLinksEditorial(),
			],
			'periodicite',
			'electronique:boolean',
			[
				'name' => 'langues',
				'value' => \models\lang\Convert::codesToFullNames(json_decode($model->langues)),
			],
			[
				'name' => 'Éditeurs',
				'type' => 'raw',
				'value' => join("\n", $editeurs),
			],
			[
				'label' => 'Dernière modification',
				'type' => 'raw',
				'value' => $lastUpdate ?
					(!Yii::app()->user->checkAccess("avec-partenaire") ? Yii::app()->format->formatDatetime($lastUpdate->hdateVal)
						:CHtml::link(Yii::app()->format->formatDatetime($lastUpdate->hdateVal), ['/intervention/view', 'id' => $lastUpdate->id]))
					: '-',
			],
		],
	]
);
?>

<?php if (count($autresTitres) > 1) { ?>
<section id="titre-historique">
	<h2>Historique des titres</h2>
	<ol>
	<?php
	foreach ($autresTitres as $autre) {
		/** @var Titre $autre */
		echo "<li>";
		if ($autre->id == $model->id) {
			echo CHtml::encode($model->getFullTitleWithPerio());
		} else {
			echo CHtml::link(CHtml::encode($autre->getFullTitleWithPerio()), ['titre/view', 'id' => $autre->id]);
		}
		echo "</li>";
	}
	?>
	</ol>
</section>
<?php } ?>

<section id="attributs">
	<h2>Attributs</h2>
	<?php
	$isAdmin = Yii::app()->user->checkAccess('admin');
	$attributs = Sourceattribut::getAttributesForTitre($model, $isAdmin);
	if ($attributs) {
		?>
		<div>
			<table class="table table-striped table-bordered table-condensed">
				<thead>
					<th>Identifiant</th>
					<th>Nom</th>
					<th>Valeur</th>
					<?= $isAdmin ? '<th>Visible</th>' : '' ?>
				</thead>
				<tbody>
				<?php
				foreach ($attributs as $identifiant => ['nom' => $nom, 'valeur' => $valeur, 'visibilite' => $vis]) {
					echo '<tr>'
						. '<td>' . CHtml::encode($identifiant) . '</td>'
						. '<td>' . CHtml::encode($nom) . '</td>'
						. '<td>' . CHtml::encode($valeur) . '</td>'
						. ($isAdmin ? "<td>" . ($vis ? 'X' : '') . "</td>" : '')
						. "</tr>\n";
				}
				?>
				</tbody>
			</table>
		</div>
	<?php
	} else {
		echo "<p>Aucun attribut spécifique n'est associé à ce titre.</p>";
	}
	?>
</section>

<h2>Accès en ligne</h2>
<?php
$display = new DisplayServices($model->services, Yii::app()->user->getState('instituteId'));
$display->displayAll = true;
$this->renderPartial(
	'/service/_summaryTable',
	[
		'display' => $display,
		'directAccess' => $directAccess,
		'partenaireId' => Yii::app()->user->getState('instituteId'),
		'publicView' => true,
		'publicViewUrl' => '',
	]
);

if ($collections) {
	echo '<h3 class="collapse-toggle">Collections</h3>'
		. '<div class="collapse-target">'
		. '<table class="table table-striped table-bordered table-condensed">'
		. '<thead><th>Ressource</th><th>Collection</th></thead>'
		. '<tbody>';
	foreach ($collections as $c) {
		echo '<tr>'
			. '<td>' . CHtml::encode($c->ressource->nom) . '</td>'
			. '<td>' . CHtml::encode($c->nom) . '</td>'
			. "</tr>\n";
	}
	echo '</tbody></table>';
	echo "</div>\n";
}
?>

<h2>Suivi</h2>
<?php
$partenaires = $model->getPartenairesSuivant();
if ($partenaires) {
	echo '<ul>';
	foreach ($partenaires as $partenaire) {
		echo '<li>' . $partenaire->getSelfLink() . ' suit la revue de ce titre, dans Mir@bel.</li>';
	}
	echo "</ul>\n";
} else {
	echo "<p>Ce titre n'est pas suivi dans Mir@bel.</p>";
}
?>

<h2>Possession</h2>
<?php
$owners = $model->getOwners();
if (empty($owners)) {
	echo "<p>Aucun institut ne possède ce titre.</p>";
} else {
	echo "<ul>";
	/** @var Partenaire $owner */
	foreach ($owners as $owner) {
		$url = $owner->buildPossessionUrl($model);
		echo "<li>"
			. ($url
				? CHtml::encode($owner->getFullName()) . " " . CHtml::link("[Notice]", $url)
				: CHtml::encode($owner->getFullName()))
			. "</li>";
	}
	echo "</ul>";
}
?>

<?php
$this->renderPartial('/global/_modif-verif', ['target' => $model, 'rssTarget' => $model->revue]);
