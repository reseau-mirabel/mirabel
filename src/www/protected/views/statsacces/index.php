<?php

/** @var Controller $this */
/** @var array $reports */

use components\HtmlTable;

assert($this instanceof Controller);

$this->pageTitle = Yii::app()->name . " - Statistiques d'accès";
$this->breadcrumbs = [
	"Statistiques de fréquentation",
];
?>

<h1>Statistiques de fréquentation</h1>

<p>
	Chaque rapport porte sur un mois de fréquentation du site.
</p>
<p>
	Chaque lien s'ouvre dans un nouvel onglet.
</p>

<?= HtmlTable::build($reports, ["Année", "janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"])
	->toHtml(); ?>
