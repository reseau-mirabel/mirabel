<?php

/** @var Controller $this */
/** @var Hint $model */
assert($this instanceof Controller);

$this->breadcrumbs = [
	'Formulaires' => ['admin'],
	'Administration',
];

\Yii::app()->getComponent('sidebar')->menu = [
	['label' => 'Créer', 'url' => ['create']],
];
?>

<h1>Documentation des formulaires</h1>

<?php if (Yii::app()->user->checkAccess('redaction/aide')) { ?>
<p>
	<?= CHtml::link("Créer une nouvelle bulle d'aide", ['create'], ['class' => 'btn btn-default']) ?>
</p>
<?php } ?>

<p>
	Vous pouvez saisir un opérateur de comparaison
	(<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
	ou <b>=</b>) au début de chaque zone de recherche afin de désigner le mode de comparaison.
</p>

<?php
$columns = [
	[
		'name' => 'model',
		'filter' => Hint::ENUM_MODEL,
		'value' => function (Hint $data) {
			return Hint::ENUM_MODEL[$data->model] ?? $data->model;
		},
	],
	'attribute',
	'name',
	'description:html',
	'hdateModif:datetime',
	[
		'class' => 'BootButtonColumn', // CButtonColumn
		'template' => '{update}',
	],
];
$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	[
		'id' => 'hints-grid',
		'dataProvider' => $model->search(),
		'filter' => $model, // null to disable
		'columns' => $columns,
		'ajaxUpdate' => false,
	]
);
?>
