<?php

/** @var Controller $this */
/** @var Hint $model */
assert($this instanceof Controller);

$this->breadcrumbs = [
	'Admin' => ['/admin'],
	"Bulles d'aide" => ['/hint/admin'],
	'Créer',
];
?>

<h1>Nouvelle bulle d'aide</h1>

<?php
echo $this->renderPartial('_form', ['model' => $model]);
?>
