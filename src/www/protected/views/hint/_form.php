<?php

/** @var Controller $this */
/** @var Hint $model */
assert($this instanceof Controller);

/** @var BootActiveForm $form */
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'hint-form',
		'enableAjaxValidation' => false,
		'hints' => Hint::model()->getMessages('Hint'),
		'type' => BootActiveForm::TYPE_HORIZONTAL,
	]
);
?>

<?php

echo $form->errorSummary($model);

if ($model->isNewRecord) {
	$labels = $model->attributeLabels();
	echo $form->dropDownListRow(
		$model,
		'model',
		Hint::ENUM_MODEL,
		[
			'prompt' => "",
			'ajax' => [
				'type'=>'GET',
				'url' => $this->createUrl('/hint/dynamic-attributes'),
				'success' =>
					<<<EOJS
					function(data) {
						$("#Hint_attribute").html('<option value="">-</option>');
						for (let k in data) {
							let option = document.createElement("option");
							option.value = k;
							option.innerText = data[k];
							document.getElementById("Hint_attribute").appendChild(option);
						}
					}
					EOJS,
			],
		]
	);

	echo $form->dropDownListRow($model, 'attribute', []);
	Yii::app()->clientScript->registerScript('attribute',
		<<<'EOJS'
		$("#Hint_attribute").on("change", function() {
			let val = $(this).val()
			if (val) {
				$("#Hint_name").val($("#Hint_attribute").find("option:selected").text());
			}
		});
		EOJS
	);

	echo $form->textFieldRow($model, 'name', ['class' => 'span5']);
} else {
	echo "<p>Les premiers champs ne sont pas modifiables, ils décrivent l'entrée de formulaire à documenter.</p>";
	echo $form->uneditableRow($model, 'model');
	echo $form->uneditableRow($model, 'attribute');
	echo $form->textFieldRow($model, 'name', ['class' => 'span5']);
}

new \widgets\CkEditor();
echo $form->textAreaRow($model, 'description', ['rows' => 8, 'cols' => 50, 'class' => 'span8 htmleditor']);

?>
<div class="form-actions">
	<?php
$this->widget(
	'bootstrap.widgets.BootButton',
	[
		'buttonType' => 'submit',
		'type' => 'primary',
		'label' => $model->isNewRecord ? 'Créer' : 'Enregistrer',
	]
);
?>
</div>

<?php
$this->endWidget();
?>
