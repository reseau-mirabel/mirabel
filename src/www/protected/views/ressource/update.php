<?php

/** @var Controller $this */
/** @var Ressource $model */
/** @var bool $direct */
/** @var Intervention $intervention */
assert($this instanceof Controller);

$this->pageTitle = 'Ressource - Modifier';

$this->breadcrumbs=[
	'Ressources' => ['index'],
	$model->getFullName() => ['view', 'id' => $model->id],
	'Modifier',
];

\Yii::app()->getComponent('sidebar')->menu = [
	['label' => 'Liste', 'url' => ['index']],
];
?>

<h1>Modifier la ressource <em><?= CHtml::encode($model->getLongName()); ?></em></h1>

<?php
echo $this->renderPartial(
	'_form',
	['model' => $model, 'direct' => $direct, 'intervention' => $intervention]
);
?>
