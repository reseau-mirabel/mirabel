<?php

/** @var Controller $this */
/** @var Ressource $ressource*/
/** @var array $checks */

use components\HtmlHelper;

assert($this instanceof Controller);

$this->pageTitle = $ressource->nom . " - Vérification des liens";

$this->breadcrumbs = [
	'Ressources' => ['index'],
	$ressource->nom => ['view', 'id' => $ressource->id],
	'Vérification des liens',
];

\Yii::app()->getComponent('sidebar')->menu = [
	['label' => 'Liste des ressources', 'url' => ['index']],
	['label' => 'Cette ressource', 'url' => ['view', 'id' => $ressource->id]],
];

$checks['urls'][''] = true;
$buildTr = HtmlHelper::builderCheckLinks($checks['urls']);
?>

<h1>
	Vérification des liens de la ressource <?= CHtml::encode($ressource->nom) ?>
</h1>

<h2>URLs vérifiées</h2>
<table class="table table-bordered table-condensed">
	<thead>
		<tr>
			<th>URL</th>
			<th>Résultat</th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach (array_keys($checks['urls']) as $url) {
			echo $buildTr($url);
		}
		?>
	</tbody>
</table>

<?php
