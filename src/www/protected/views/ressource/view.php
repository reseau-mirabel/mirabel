<?php

/** @var Controller $this */
/** @var Ressource $model */
/** @var ?AbonnementSearch $abonnements Si null, l'utilisateur n'a pas sélectionné d'établissement */
/** @var ?Partenaire $partenaireCourant */
/** @var ?SearchNavigation $searchNavigation */
/** @var bool $forceRefresh */
/** @var Cms[] $actus */

use components\HtmlHelper;

$this->pageTitle = $model->getLongName();
$this->pageDescription = 'Description des accès en ligne aux contenus des revues de ' . $model->getLongName();

$this->breadcrumbs = [
	'Ressources' => ['index'],
	$model->getFullName(),
];

$instituteId = Yii::app()->user->getState('instituteId');
$institute = Yii::app()->user->getInstitute();

$isGuest = !Yii::app()->user->checkAccess("avec-partenaire");

if (!$isGuest) {
	/** @var \components\WebSidebar */
	$sidebar = \Yii::app()->getComponent('sidebar');
	$sidebar->menu = [
		['label' => 'Liste des ressources', 'url' => ['index']],
		['label' => 'Créer une ressource', 'url' => ['create']],

		['label' => 'Sur cette ressource', 'itemOptions' => ['class' => 'nav-header']],
		['label' => 'Vérifier les liens', 'url' => ['checkLinks', 'id' => $model->id]],
		['label' => 'Modifier', 'url' => ['update', 'id' => $model->id]],
		[
			'encodeLabel' => false,
			'label' => HtmlHelper::postButton(
				'Supprimer',
				['/ressource/delete', 'id' => $model->id],
				[],
				['class' => 'btn btn-link alert-danger']
			),
		],
		[
			'label' => 'Logo',
			'url' => ['logo', 'id' => $model->id],
			'visible' => Yii::app()->user->checkAccess('ressource/logo'),
		],

		['label' => 'Collections', 'itemOptions' => ['class' => 'nav-header']],
		[
			'label' => 'Créer une collection',
			'url' => ['collection/create', 'ressourceId' => $model->id],
			'visible' => Yii::app()->user->checkAccess('collection/create', $model),
		],
	];
	if (Yii::app()->user->checkAccess('verify', $model)) {
		$abonnementRessource = isset($abonnements) ? $abonnements->readStatus($model) : null;
		$nbAbonnements = $model->countAbonnes();
		$sidebar->menu = array_merge(
			$sidebar->menu,
			[
				['label' => 'Vérification', 'itemOptions' => ['class' => 'nav-header']],
				[
					'encodeLabel' => false,
					'label' => $this->widget(
						'bootstrap.widgets.BootButton',
						[
							'label' => 'Indiquer que la ressource a été vérifiée',
							'url' => ['verify', 'id' => $model->id],
							'type' => 'primary', 'size' => 'small',
						],
						true
					),
				],
				['label' => 'Abonnements', 'itemOptions' => ['class' => 'nav-header']],
				[
					'label' => ($nbAbonnements ? $nbAbonnements . " abonné" . ($nbAbonnements > 1 ? 's' : '') : "Aucun abonné"),
					'url' => ['abonnements', 'id' => $model->id],
				],
				[
					'label' => 'Mes abonnements',
					'url' => ['/partenaire/abonnements', 'id' => Yii::app()->user->partenaireId],
				],
				['label' => isset($abonnementRessource) ? Abonnement::$enumMask[$abonnementRessource] : "pas abonné"],
			]
		);
		// Si la personnalisation par établissement est pour l'établissement de l'utilisateur courant, il peut s'abonner.
		if (!$model->getCollectionsDiffuseur() && $abonnements !== null && (int) $abonnements->partenaireId === (int) Yii::app()->user->partenaireId && $abonnementRessource !== null) {
			$subscribe = (new AbonnementDecorator($abonnementRessource))
				->getHtmlButton(['partenaireId' => Yii::app()->user->partenaireId, 'ressourceId' => $model->id]);
			if ($subscribe) {
				array_push(
					$sidebar->menu,
					['label' => $subscribe, 'encodeLabel' => false, 'title' => "Abonnement à la ressource (collection maîtresse) et à tous ses accès"]
				);
			}
		}
	}
}

$encodedName = CHtml::encode($model->nom);
$encodedTitle = CHtml::encode($this->pageTitle);
$encodedDescription = CHtml::encode($this->pageDescription);
$this->appendToHtmlHead(
	<<<EOL
		<meta name="description" content="$encodedDescription" lang="fr" />
		<meta name="keywords" content="revue, accès en ligne, texte intégral, sommaire, périodique, $encodedName" lang="fr" />
		<link rel="schema.dcterms" href="http://purl.org/dc/terms/" />
		<meta name="dcterms.title" content="Mirabel : ressource $encodedName" />
		<meta name="dcterms.subject" content="revue, accès en ligne, texte intégral, sommaire, périodique, $encodedName" />
		<meta name="dcterms.language" content="fr" />
		<meta name="dcterms.creator" content="Mirabel" />
		<meta name="dcterms.publisher" content="Sciences Po Lyon" />
		EOL
);
$logoUrl = $model->getLogoUrl(false);
if ($logoUrl) {
	Yii::app()->clientScript->registerMetaTag($logoUrl, 'og:image', null, ['property' => 'og:image'], 'og:image');
}

$imgUrl = Yii::app()->getAssetManager()
	->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview';

$this->renderPartial('/global/_interventionLocal', ['model' => $model]);
?>

<?= (new \widgets\SearchNavigationDisplay())->run(); ?>

<div class="ressource-logo"><?= $model->getLogoImg(true, $forceRefresh) ?></div>

<h1>Ressource <em><?= CHtml::encode($model->getLongName()); ?></em></h1>

<div class="guest-operations">
	<?= CHtml::link(
		CHtml::image($imgUrl . '/update.png', "Proposer une modification"),
		['update', 'id' => $model->id],
		['title' => "Proposer une modification"]
	) ?>
</div>

<?php
$attributes = [
	[
		'name' => 'nom',
		'value' => $model->prefixe . $model->nom,
	],
	'sigle',
	[
		'name' => 'url',
		'type' => 'raw',
		'value' => CHtml::link(CHtml::encode($model->url), $model->proxifyUrl($model->url, $institute, $abonnements)),
	],
	'disciplines',
	'diffuseur',
	'partenaires',
	'partenairesNb',
	[
		'name' => 'description',
		'type' => 'raw',
		'value' => nl2br(CHtml::encode($model->description)),
	],
	[
		'name' => 'type',
		'value' => Ressource::$enumType[$model->type],
	],
	'revuesNb',
	'articlesNb',
	'indexation:boolean',
	[
		'name' => 'noteContenu',
		'type' => 'raw',
		'value' => nl2br(CHtml::encode($model->noteContenu)),
	],
	[
		'name' => 'acces',
		'value' => Ressource::$enumAcces[$model->acces],
	],
	'alerteRss:boolean',
	'alerteMail:boolean',
	'exportPossible:boolean',
	[
		'name' => 'exhaustif',
		'value' => (int) $model->exhaustif === Ressource::EXHAUSTIF_OUI ?
			"Oui, toutes les revues de cette ressource sont signalées dans Mir@bel"
			: "Non, une partie seulement des revues de cette ressource sont signalées dans Mir@bel",
		'visible' => (int) $model->exhaustif !== Ressource::EXHAUSTIF_INDET,
	],
];
if (!$isGuest) {
	$attributes = array_merge(
		$attributes,
		[
			'autoImport:boolean',
			'importIdentifications',
			'partenaire:boolean',
		]
	);
}

$this->widget(
	'bootstrap.widgets.BootDetailView',
	[
		'data' => $model,
		'attributes' => $attributes,
		'hideEmptyLines' => true,
	]
);
?>

<h2>Revues et abonnements</h2>
<?php
$numR = $model->countRevues();
if ($numR) {
	echo '<p>'
		. CHtml::link(
			$numR . ' revue' . ($numR > 1 ? 's présentes' : ' présente') . ' dans Mir@bel',
			['/revue/search', 'SearchTitre[ressourceId][]' => $model->id]
		)
		. ($isGuest ? "" : " (" . $model->countTitres() . " titres)")
		. ' signale' . ($numR > 1 ? 'nt' : '')
		. ' des accès en ligne sur cette ressource.</p>';
} else {
	echo "<div class=\"alert alert-error\">Cette ressource n'a pas de revue dans Mir@bel.</div>";
}
?>

<?php
$canUpdateCollections = !$isGuest
	&& Yii::app()->user->checkAccess('collection/update', $model);
$canAct = !$isGuest
	&& $partenaireCourant
	&& $instituteId
	&& $instituteId == (int) $partenaireCourant->id
	&& $partenaireCourant->type === 'normal';
$collections = $model->getCollectionsWithNumRevues();
if ($collections) {
	if (!$isGuest) {
		echo CHtml::link(
			CHtml::image(Yii::app()->baseUrl . "/images/plus.png", "Nouvelle collection") . " Nouvelle collection",
			['collection/create', 'ressourceId' => $model->id],
			[
				'style' => 'float: right; margin-right: 40px;',
				'title' => 'Nouvelle collection',
			]
		);
	}
	$hasExhaust = false;
	foreach ($collections as $c) {
		if ((int) $c->exhaustif !== Collection::EXHAUSTIF_INDET) {
			$hasExhaust = true;
		}
	} ?>
	<table class="table table-condensed" id="ressource-collections">
		<thead>
			<th>Collection</th>
			<?php if ($hasExhaust || !$isGuest) { ?>
			<th><?= $isGuest ? "Couverture dans Mir@bel" : "Type" ?></th>
			<?php } ?>
			<?= ($instituteId > 0 ? '<th></th>' : '') ?>
			<th>Revues</th>
			<?= $isGuest ? '' : '<th>Titres</th>' ?>
			<?= ($canUpdateCollections ? '<th>Actions</th>' : '') ?>
			<?= ($canAct ? '<th>Actions/partenaire</th>' : '') ?>
		</thead>
		<tbody>
		<?php
		$exhaustif = [
			Collection::EXHAUSTIF_INDET => '',
			Collection::EXHAUSTIF_NON => 'non-exhaustive',
			Collection::EXHAUSTIF_OUI => 'exhaustive',
		];
	foreach ($collections as $c) {
		/** @var CollectionWithNums $c */
		if (!$c->visible && $isGuest) {
			continue;
		}
		echo '<tr>';

		// nom et description
		echo '<td>' . CHtml::encode($c->nom);
		if ($c->url) {
			echo " " . CHtml::link(
				CHtml::image(Yii::app()->baseUrl . '/images/www.png'),
				$c->url,
				['title' => "Site web officiel"]
			);
		}
		if ($c->description) {
			echo CHtml::tag(
				"div",
				['class' => "collection-description ellipsis", 'title' => $c->description],
				CHtml::encode($c->description)
			);
		}
		echo '</td>';

		// type + auto-import? + exhaustif?
		if ($hasExhaust || !$isGuest) {
			echo "<td>";
			if ($isGuest) {
				echo $exhaustif[(int) $c->exhaustif];
			} else {
				echo CHtml::encode($c->type)
						. ($c->importee ? " (importée)" : "")
						. ($exhaustif[(int) $c->exhaustif] ? " (" . $exhaustif[(int) $c->exhaustif] . " dans Mir@bel)" : "");
			}
			echo "</td>";
		}

		// institut
		if ($instituteId > 0) {
			echo "<td>"
					. (new AbonnementDecorator($abonnements->readStatus($c), $c->type))
						->getHtmlStatus(Yii::app()->user->getState('instituteShortname'))
					. "</td>\n";
		}

		// #revues
		$revuesTxt = $c->numRevues . " revue" . ($c->numRevues > 1 ? 's' : '');
		if ($c->numRevues) {
			$revues = CHtml::link($revuesTxt, ['revue/search', 'SearchTitre[collectionId]' => $c->id]);
		} else {
			$revues = $revuesTxt;
		}
		echo '<td>' . $revues . '</td>';

		// #titres
		if (!$isGuest) {
			echo "<td>" . $c->numTitres . "</td>";
		}

		// actions sur les collections
		if ($canUpdateCollections) {
			echo "<td>";
			echo CHtml::link(
				CHtml::image($imgUrl . '/update.png', 'Modifier'),
				['/collection/update', 'id' => $c->id],
				['title' => 'Modifier']
			);
			echo " " . CHtml::link(
				CHtml::image($imgUrl . '/delete.png', 'Supprimer'),
				['/collection/delete', 'id' => $c->id]
			);
			if ($c->type === Collection::TYPE_TEMPORAIRE) {
				echo " (temporaire)";
			}
			echo "</td>";
		}

		// actions sur les abonnements
		if ($canAct) {
			echo "<td>";
			$this->renderPartial('_abonnement', ['model' => $c, 'abonnements' => $abonnements]);
			echo "</td>";
		}

		echo "</tr>\n";
	} ?>
		</tbody>
	</table>
	<?php
	Yii::app()->clientScript->registerScript(
		'ressource-description',
		<<<'EOL'
			$('#ressource-collections').on('click', '.collection-description', function() {
				$(this).toggleClass('ellipsis');
			});
			EOL
	);
} else {
	// Ressource sans collection
	?>
	<table class="table table-condensed" id="ressource-collections">
		<thead>
			<th></th>
			<?= ($instituteId > 0 ? '<th></th>' : '') ?>
			<th>Revues</th>
			<?= ($canAct ? '<th>Actions/partenaire</th>' : '') ?>
		</thead>
		<tbody>
			<tr>
				<td><em><?= CHtml::encode($model->nom) ?></em></td>
				<?php
				if ($instituteId > 0) {
					echo "<td>"
						. (new AbonnementDecorator($abonnements->readStatus($model)))
							->getHtmlStatus(Yii::app()->user->getState('instituteShortname'))
						. "</td>\n";
				} ?>
				<td><?= $numR ?> revue<?= $numR > 1 ? "s" : "" ?></td>
				<?php
				if ($canAct) {
					echo "<td>";
					$this->renderPartial('_abonnement', ['model' => $model, 'abonnements' => $abonnements]);
					echo "</td>";
				} ?>
			</tr>
		</tbody>
	</table>
	<?php
}

if ($numR && !$isGuest) {
	$this->renderPartial('_exportServices', ['ressourceId' => $model->id]);
}
?>

<?php
if ($actus) {
	?>
	<section>
		<h2>Actualités dans Mir@bel</h2>
		<ul class="breves">
			<?php
			$limit = \controllers\ressource\ViewAction::MAX_ACTUS;
			foreach ($actus as $block) {
				$limit--;
				if ($limit < 0) {
					echo '<li id="actu' . $block->id . '">'
						. CHtml::link(
							"→ Toute l'actualité de <em>" . CHtml::encode($model->nom) . "</em> dans Mir@bel",
							['/site/actualite', 'q' => ['ressourceId' => $model->id]]
						)
						. "</li>\n";
					break;
				}
				echo '<li id="actu' . $block->id . '">' . $block->toHtml() . "</li>\n";
			}
			?>
		</ul>
	</section>
	<?php
}
?>

<h2>Suivi</h2>
<?php
$partenairesSuivant = $model->getPartenairesSuivant();
if ($partenairesSuivant) {
	echo '<ul>';
	foreach ($partenairesSuivant as $partenaireSuivant) {
		echo '<li>' . $partenaireSuivant->getSelfLink() . ' suit cette ressource dans Mir@bel.</li>';
	}
	echo "</ul>\n";
} else {
	echo "<p>Cette ressource n'est pas suivie globalement dans Mir@bel.</p>";
}


$this->renderPartial('/global/_modif-verif', ['target' => $model]);

?>
<div class="modal hide" id="modal-abonnement-proxyurl">
  <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<strong>URL du proxy pour cet abonnement</strong>
  </div>
  <div class="modal-body">
	<form action="/abonnement/updateProxyurl" method="POST">
	  <input type="hidden" name="id" class="abonnement-id" />
	  <input type="text" placeholder="ex. 'https://a.com?q=URL' où le terme URL sera remplacé" name="proxyurl" class="abonnement-proxyurl input-block-level" />
	</form>
  </div>
  <div class="modal-footer">
	<a href="#" class="btn" class="close" data-dismiss="modal">Annuler</a>
	<a href="#" class="btn btn-primary">Enregistrer</a>
  </div>
</div>
<?php
Yii::app()->clientScript->registerScript(
	'modal-abonnement-proxyurl',
	<<<'EOL'
	$("#modal-abonnement-proxyurl .btn-primary").on("click", function() {
		$('#modal-abonnement-proxyurl form').submit();
	})
	$("#modal-abonnement-proxyurl form").on("submit", function() {
		var proxyUrl = $('#modal-abonnement-proxyurl .abonnement-proxyurl').val();
		if (proxyUrl.indexOf('URL') === -1) {
			return window.confirm("Le proxy ne contient pas \"URL\" et sera donc indépendant de l'accès en ligne sélectionné. Confirmez-vous que l'adresse sera constante pour toute la ressource ?");
		}
		return true;
	});
	EOL
);
