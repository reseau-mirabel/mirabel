<?php

/** @var Controller $this */
/** @var Ressource[] $ressources */
/** @var bool $withDescription */
assert($this instanceof Controller);

?>
<table class="items table">
	<thead>
		<tr>
			<th>Accès site web</th>
			<th>Nom</th>
			<th>Revues liées</th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($ressources as $ressource) {
			$img = $ressource->getLogoImg(true);
			if ($withDescription) {
				$description = empty($ressource->description) ?
					""
					: "<div><em> " . $ressource->description . "</em></div>";
			} else {
				$description = '';
			}
			?>
		<tr>
			<td><?= (empty($ressource->url) ? $img : CHtml::link($img, $ressource->url)) ?>
			</td>
			<td>
				<div>
					<?= CHtml::link(
						CHtml::encode($ressource->nom),
						['/ressource/view', 'id' => $ressource->id, 'nom' => Norm::urlParam($ressource->nom)]
					) ?>
				</div>
				<?php if ($withDescription) { ?>
				<div><?= $description ?></div>
				<?php } ?>
			</td>
			<td>
				<?php
				$count = $ressource->countRevues();
				if ($count) {
					echo CHtml::link((string) $count, ['revue/search', 'SearchTitre[ressourceId][]' => $ressource->id]);
				} else {
					echo "Aucune";
				}
				?>
			</td>
		</tr>
		<?php
		}
		?>
	</tbody>
</table>
