<?php

/** @var Controller $this */
/** @var Ressource $model */
/** @var bool $deletable */
/** @var string $msg */
assert($this instanceof Controller);

$this->pageTitle = $model->getFullName() . " - suppression";

$this->breadcrumbs = [
	'Ressources' => ['index'],
	$model->getFullName() => ['view', 'id' => $model->id],
	"suppression",
];

\Yii::app()->getComponent('sidebar')->menu = [
	['label' => 'Liste', 'url' => ['index']],
	['label' => 'Créer', 'url' => ['create']],
	['label' => 'Modifier', 'url' => ['update', 'id' => $model->id]],
	['label' => 'Supprimer', 'url' => '#',
		'linkOptions' => ['submit' => ['delete', 'id' => $model->id], ],
	],
	['label' => 'Collections', 'itemOptions' => ['class' => 'nav-header']],
	['label' => 'Créer une collection', 'url' => ['collection/create', 'ressourceId' => $model->id]],
	['label' => 'Vérification', 'itemOptions' => ['class' => 'nav-header']],
	[
		'encodeLabel' => false,
		'label' => $this->widget(
			'bootstrap.widgets.BootButton',
			[
				'label' => 'Indiquer que la ressource a été vérifiée',
				'url' => ['verify', 'id' => $model->id],
				'type' => 'primary', 'size' => 'small',
			],
			true
		),
	],
];

?>

<h1>Ressource <em><?= CHtml::encode($model->getFullName()) ?></em> - suppression</h1>

<div class="hero-unit">
	<p class="alert alert-<?= $deletable ? "info" : "danger" ?>"><?= $msg; ?></p>
	<div><?php
		echo CHtml::form()
			. CHtml::hiddenField('confirm', "1")
			. ($deletable ? "" : "<div>En tant qu'administrateur, vous pouvez forcer la suppression de cette ressource et des données liées.</div>")
			. CHtml::submitButton("Confirmer la suppression", ['class' => 'btn btn-danger'])
			. CHtml::endForm();
		echo CHtml::link("Annuler la suppression", ['view', 'id' => $model->id], ['class' => 'btn']);
	?></div>
</div>
