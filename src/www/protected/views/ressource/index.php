<?php

/** @var Controller $this */
/** @var \components\sphinx\DataProvider $dataProvider */
/** @var \processes\ressource\ListFilter $filter */
/** @var ?\components\paginationalpha\ListViewAlpha $listView */
/** @var string $searchHash */

use components\HtmlHelper;

assert($this instanceof Controller);

$this->pageTitle = 'Ressources';
$this->breadcrumbs=[
	'Ressources',
];
\Yii::app()->getComponent('sidebar')->menu = [
	['label' => 'Créer une ressource', 'url' => ['create']],
];
?>

<p>
	Dans Mir@bel, une ressource est un site proposant un ou des accès en ligne à des contenus de revues.
	Par exemple un bouquet de revues, un catalogue d'éditeur, une base de sommaires, une base de données bibliographiques ou le site web de la revue…
</p>

<div class="legende">
	<h3>Légende</h3>
	<ul>
		<?php if (!Yii::app()->user->checkAccess("avec-partenaire")) { ?>
		<li class="ressource suivi-other">Suivie par un partenaire</li>
		<?php } else { ?>
		<li class="ressource suivi-self">Suivie par son propre partenaire</li>
		<li class="ressource suivi-other">Suivie par un autre partenaire</li>
		<?php } ?>
	</ul>
</div>

<h1>
	Ressources
	<?php
	if ($filter->suivi > 0) {
		echo " suivies par le partenaire <em>" . CHtml::encode($filter->suiviPartenaire->nom) . "</em>";
	} elseif ($filter->suivi < 0) {
		echo " suivies par un partenaire";
	} elseif ($filter->exhaustif) {
		echo " exhaustives";
	} elseif ($filter->exhaustifcollections) {
		echo " avec des collections exhaustives";
	} elseif ($filter->import === 'ressource') {
		echo " mises à jour automatiquement";
	} elseif ($filter->import === 'collection') {
		echo " ayant des collections mises à jour automatiquement";
	} elseif ($filter->lettre !== null && $filter->lettre !== '') {
		echo " — " . ($filter->lettre === "0" ? "#0-9" : $filter->lettre);
	}
	?>
</h1>

<?php if ($filter->suivi) { ?>
<p>
	<?= $dataProvider->getTotalItemCount() ?> ressources suivies dans Mir@bel.
</p>
<?php } elseif ($filter->import === 'ressource') { ?>
<p>
	<?= $dataProvider->getTotalItemCount() ?> ressources mises à jour automatiquement dans Mir@bel.
</p>
<?php } elseif ($filter->import === 'collection') { ?>
<p>
	<?= $dataProvider->getTotalItemCount() ?> ressources pour lesquelles une partie des collections sont mises à jour
	automatiquement dans Mir@bel (mais pas toutes).
	Vous pouvez en parallèle consulter la liste des ressources dont toutes les collections sont
	<?= CHtml::link("mises à jour automatiquement", ['/ressource/index', 'import' => 'ressource']) ?>.
</p>
<?php } ?>
<?php if ($filter->exhaustif) { ?>
<p>
	<?= $dataProvider->getTotalItemCount() ?> ressources exhaustives (toutes leurs revues sont signalées dans Mir@bel).
</p>
<?php } ?>
<?php if ($filter->exhaustifcollections) { ?>
<p>
	<?= $dataProvider->getTotalItemCount() ?> ressources pour lesquelles une partie des collections sont complètes dans Mir@bel
	(pour ces collections, toutes les revues sont présentes dans Mir@bel).
	Vous pouvez en parallèle consulter la liste des ressources dont
	<?= CHtml::link("toutes les collections sont complètes", ['/ressource/index', 'exhaustif' => 1]) ?>.
	</p>
<?php } ?>

<?php
if ($listView !== null) {
	$listView->renderLetterSelector();
	$listView->renderPager();
}
?>

<div>
	<?= CHtml::link(
		CHtml::image(Yii::app()->baseUrl . "/images/plus.png", "Ajout de ressource"),
		['create'],
		[
			'style' => 'float: right; margin-right: 40px;',
			'title' => 'Proposer de créer une nouvelle ressource',
		]
	) ?>
</div>

<table class="items table list-results">
	<thead>
		<tr>
			<th>Nom</th>
			<th>Revues</th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($dataProvider->getData() as $result) {
			echo '<tr class="' . HtmlHelper::linkItemClass('ressource', $result->getSuiviIds()) . '"><td>'
				. CHtml::link(
					CHtml::encode($result->nomcomplet),
					['view', 'id' => $result->id, 'nom' => Norm::urlParam($result->nomcomplet)]
				)
				 . ($result->web ? ' (site web)' : '')
				. '</td><td>'
				. CHtml::link(
					$result->nbrevues . ' revue' . ($result->nbrevues > 1 ? 's' : ''),
					['/revue/search', 'SearchTitre[ressourceId][]' => $result->id]
				) . '</td></tr>';
		}
		?>
	</tbody>
</table>

<?php
if ($listView !== null) {
	$listView->renderLetterSelector();
	$listView->renderPager();
}
?>

<div>
	<?php
	if ($listView !== null) {
		$url = [
			'/ressource/index',
			'lettre' => $filter->lettre,
			'noweb' => !$filter->noweb,
		];
		if ($filter->noweb) {
			echo CHtml::link("Afficher aussi le type <i>site web de la revue</i>", $url, ['class' => 'btn']);
		} else {
			echo CHtml::link("Masquer le type <i>site web de la revue</i>", $url, ['class' => 'btn']);
		}
	}

if (!Yii::app()->user->checkAccess("avec-partenaire")) {
	echo " ", CHtml::link("Lister les ressources partenaires", ['partenaire/index', '#' => 'tab-ressources'], ['class' => 'btn', 'title' => "Ressources qui sont aussi des partenaires de Mir@bel"]);
} else {
	echo '<div style="margin-top: .5ex">Listes restreintes aux ressources… ';
	echo CHtml::link("… partenaires", ['partenaire/index', '#' => 'partenaires-ressources'], ['class' => 'btn'])
	, " "
	, CHtml::link("… suivies", ['index', 'suivi' => -1], ['class' => 'btn', 'title' => "Ressources suivies dans Mir@bel par un partenaire"])
	, " "
	, CHtml::link("… mises à jour automatiquement", ['index', 'import' => 'ressource'], ['class' => 'btn', 'title' => "Ressources avec un import automatique, même partiel"])
	, " "
	, CHtml::link("… exhaustives", ['index', 'exhaustif' => 1], ['class' => 'btn', 'title' => "Ressources exhaustives"])
	, " "
	, ($listView === null ? CHtml::link("Toutes !", ['index'], ['class' => 'btn', 'title' => "Toutes les ressources"]) : "");
	echo "</div>";
	echo '<div style="margin-top: .5ex">… avec des collections … ';
	echo CHtml::link("… mises à jour automatiquement", ['index', 'import' => 'collection'], ['class' => 'btn', 'title' => "Ressources ayant au moins une collection qui soit importée automatiquement"])
	, " "
	, CHtml::link("… exhaustives", ['index', 'exhaustifcollections' => 1], ['class' => 'btn', 'title' => "Ressources ayant au moins une collection exhaustive"])
	, " ";
	echo "</div>";
}
?>
</div>
<?php
$maxPageRank = $dataProvider->getPagination()->getPageCount();
(new \widgets\SearchNavigationInit('ressource', $dataProvider->getData(), $maxPageRank))->run();
