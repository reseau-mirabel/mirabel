<?php

/** @var Controller $this */
/** @var string[] $pages */
/** @var string[] $pdfs */
assert($this instanceof Controller);

$this->breadcrumbs = [
	'Doc technique',
];
?>

<h1>Documentation technique</h1>

<p>
	Pour le développement logiciel et l'installation de Mir@bel,
	consulter le site Gitlab <a href="https://gitlab.com/reseau-mirabel/mirabel">gitlab.com/reseau-mirabel/mirabel</a>.
</p>

<h2>Pages HTML</h2>
<ul>
<?php
foreach ($pages as $page) {
	echo "<li>"
		. CHtml::link(
			CHtml::encode(str_replace('_', ' ', $page)),
			['view', 'view' => $page]
		)
		. "</li>\n";
}
?>
</ul>

<h2>Documents PDF</h2>
<ul>
<?php
foreach ($pdfs as $pdf) {
	echo "<li>"
		. CHtml::link(
			CHtml::encode(str_replace('_', ' ', $pdf)),
			['pdf', 'file' => $pdf]
		)
		. "</li>\n";
}
?>
</ul>
