<?php

/** @var Controller $this */
/** @var Sourcelien $model */
assert($this instanceof Controller);

$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'sourcelien-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => false,
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'hints' => Hint::model()->getMessages('Sourcelien'),
		'htmlOptions' => ['class' => 'well'],
	]
);
/** @var BootActiveForm $form */
?>

<?php
echo $form->errorSummary($model);

echo $form->textFieldRow($model, 'nom', ['class' => 'span5', 'hint' => "Non-vide et unique. Affiché à côté de l'icône"]);
echo $form->textFieldRow($model, 'nomlong', ['class' => 'span6', 'hint' => "Nom affiché au survol de l'icône"]);
echo $form->textFieldRow($model, 'nomcourt', ['class' => 'span5', 'hint' => "Le logo sera cherché dans src/www/images/liens/{nomcourt}.png. Il est déconseillé de le modifier par la suite, car ce nom court peut être référencé dans le code."]);
echo $form->textFieldRow($model, 'url', ['class' => 'span7', 'hint' => "Pour la détection des liens pré-existant.<br /> Uniquement le nom du domaine, sans le http://."]);
echo $form->checkBoxRow($model, 'import', ['hint' => "Cocher si les données sont importées périodiquement."]);
echo $form->textFieldRow($model, 'urlRegex', ['hint' => 'Expression rationnelle (regexp) pour validation des URLs. Non modifiable dans l’interface web.', 'disabled' => 'true']);
echo $form->textFieldRow($model, 'nbimport', ['hint' => 'Nombre de lignes dans les fichiers lors du dernier import, pour information.', 'disabled' => 'true']);
?>

<div class="form-actions">
	<button type="submit" class="btn btn-primary">Enregistrer</button>
</div>

<?php
$this->endWidget();
?>
