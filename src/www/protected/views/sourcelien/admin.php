<?php

use models\searches\SourcelienSearch;

/** @var Controller $this */
/** @var SourcelienSearch $model */

assert($this instanceof Controller);

$this->breadcrumbs = [
	'Sources de lien',
];
?>

<h1>Sources de liens</h1>

<p>
	<?= CHtml::link("Ajouter une nouvelle source…", ['create'], ['class' => 'btn btn-primary']); ?>
</p>

<?php
$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	[
		'id' => 'partenaire-grid',
		'dataProvider' => $model->search(),
		'filter' => $model, // null to disable
		'columns' => [
			'nom',
			'nomlong',
			'nomcourt',
			[
				'name' => 'import',
				'type' => 'boolean',
				'filter' => ["N" => 'Non', "O" => 'Oui'],
			],
			[
				'header' => 'Logo',
				'name' => 'logo',
				'type' => 'raw',
				'value' => function (SourcelienSearch $data) {
					return ($data->logo ? $data->nomcourt : '<b>Non</b>');
				},
				'filter' => [0 => 'Tous', 1 => 'Sans'],
			],
			'hdateCrea:datetime',
			[
				'class' => 'BootButtonColumn', // CButtonColumn
				'template' => '{update}{delete}',
				'deleteButtonOptions' => ['title' => "Supprimer cette source"],
				'deleteConfirmation' => "Voulez-vous vraiment supprimer cette source ?",
				'updateButtonOptions' => ['title' => "Modifier cette source"],
			],
		],
		'ajaxUpdate' => false,
	]
);
