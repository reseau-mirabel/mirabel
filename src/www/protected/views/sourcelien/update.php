<?php

/** @var Controller $this */
/** @var Sourcelien $model */
assert($this instanceof Controller);

$this->breadcrumbs = [
	'Sources de liens' => ['admin'],
	$model->nom,
];
?>

<h1>Modifier la source <em><?php echo CHtml::encode($model->nom); ?></em></h1>

<?php
echo $this->renderPartial('_form', ['model' => $model]);
?>
