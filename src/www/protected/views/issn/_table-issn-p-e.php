<?php

/** @var IssnController $this */
/** @var iterable $rows */
/** @var ?array $extraColumns */

if (empty($rows)) {
	return;
}

if (!isset($extraColumns)) {
	$extraColumns = [];
}
?>
<table class="table table-striped table-bordered table-condensed exportable">
<thead>
	<tr>
		<th>Ligne</th>
		<th>ISSN-P</th>
		<th>ISSN-E</th>
		<th>Revue</th>
		<?php
		foreach ($extraColumns as $name) {
			echo "<th>$name</th>";
		}
		?>
	</tr>
</thead>
<tbody>
	<?php
	foreach ($rows as $row) {
		echo "<tr>";
		echo CHtml::tag('td', [], $row['position']);
		echo CHtml::tag('td', [], $row['issnp']);
		echo CHtml::tag('td', [], $row['issne']);
		if (isset($row['revueId'])) {
			echo CHtml::tag(
				'td',
				[],
				CHtml::link(
					CHtml::encode($row['titre']),
					['/revue/view', 'id' => $row['revueId']]
				)
			);
		} else {
			echo "<td></td>";
		}
		foreach (array_keys($extraColumns) as $col) {
			echo CHtml::tag('td', [], CHtml::encode($row[$col]));
		}
		echo "</tr>";
	}
	?>
</tbody>
</table>
