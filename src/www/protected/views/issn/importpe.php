<?php

/** @var IssnController $this */
/** @var \models\forms\IssnImportpe $model */
/** @var \processes\issn\Importpe $import */

assert($this instanceof Controller);

$this->pageTitle = Yii::app()->name . " - Import correspondances Papier - Electronique";
$this->breadcrumbs = [
	"Import ISSN P-E",
];
?>

<h1>Importer des ISSN par correspondance Papier - Électronique</h1>

<section>
	<h2>Dépôt de fichier</h2>
	<p class="alert alert-info">
		Déposer ici le fichier au format <em>csv</em>.
		La première colonne de contenu ISSN-P et la seconde ISSN-E.
	</p>

	<?php
	/** @var BootActiveForm */
	$form = $this->beginWidget(
		'BootActiveForm',
		[
			'type' => BootActiveForm::TYPE_HORIZONTAL,
			'enableClientValidation' => false,
			'method' => 'POST',
			'htmlOptions' => ['enctype' => 'multipart/form-data'],
		]
	);
	echo $form->errorSummary($model);
	echo $form->fileFieldRow($model, 'csv', ['class' => 'input-small']);
	if ($import->isEmpty()) {
		echo "<p style=\"margin-left: 180px\">Dans un premier temps, les données seront analysées, sans écriture en base.</p>";
	} else {
		echo $form->checkBoxRow($model, 'simulation', ['label' => "Simulation (ne pas enregistrer les modifications)"]);
	}
	?>
	<div class="form-actions">
		<button type="submit" class="btn btn-primary">Importer ce fichier</button>
	</div>
	<?php $this->endWidget(); ?>
</section>

<?php
if ($import->isEmpty()) {
	return;
}
?>
<section>
	<h2>Compte-rendu</h2>

	<?php
	$errors = $import->listErrors();
	if ($errors) {
		echo "<h3>Erreurs en tentant d'insérer des ISSN-E</h3>";
		echo \components\HtmlTable::build($errors, ["Ligne", "ISSN-P", "ISSN-E", "Erreur"])->toHtml();
	}
	?>

	<?php
	$inserted = $import->listInserts();
	if ($inserted) {
		echo "<h3>Paires insérées (ISSN-E ajoutés)</h3>";
		$this->renderPartial('_table-issn-p-e', ['rows' => $inserted]);
	} else {
		echo "<h3>Paires existantes (confirmées)</h3>";
		$this->renderPartial('_table-issn-p-e', ['rows' => $import->listExisting()]);

		echo "<h3>Paires nouvelles (ISSN-E à insérer)</h3>";
		$this->renderPartial('_table-issn-p-e', ['rows' => $import->listToInsert(), 'extraColumns' => ['mirabelIssns' => "ISSN-e déjà présents"]]);
	}
	?>

	<h3>ISSN-p inconnus</h3>
	<?php
	$this->renderPartial('_table-issn-p-e', ['rows' => $import->listUnknown()]);
	?>
</section>
