<?php

/** @var Controller $this */
/** @var models\sphinx\Titres $data */
/** @var ?string $hash */

use components\HtmlHelper;

echo HtmlHelper::titleLinkItem($data);
