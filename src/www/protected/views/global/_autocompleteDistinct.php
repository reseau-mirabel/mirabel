<?php

/** @var Controller $this */
/** @var CActiveRecord $model */
/** @var string $attribute */
/** @var BootActiveForm $form */
/** @var ?array $htmlOptions */

use components\HtmlHelper;

assert($this instanceof Controller);

if (!isset($htmlOptions)) {
	$htmlOptions = ['class' => 'span6'];
}
$error = '';
if ($model->hasErrors($attribute)) {
	$htmlOptions['class'] .= ' error';
	$error = ' error';
}

$params = [
	'model' => $model,
	'attribute' => $attribute,
	'source' => Yii::app()->db
		->createCommand("SELECT DISTINCT {$attribute} FROM " . $model->tableName() . " ORDER BY {$attribute}")
		->queryColumn(),
	// additional javascript options for the autocomplete plugin
	// See <http://jqueryui.com/demos/autocomplete/#options>
	'options' => [
		// min letters typed before we try to complete
		'minLength' => '0',
	],
	'htmlOptions' => $htmlOptions,
];
CHtml::resolveNameID($model, $attribute, $htmlOptions);

?>
<div class="control-group<?php echo $error; ?>">
	<label class="control-label required" for="<?php echo $htmlOptions['id']; ?>">
		<?php echo $model->getAttributeLabel($attribute); ?>
	</label>
	<div class="controls">
		<?php
		$this->widget('zii.widgets.jui.CJuiAutoComplete', $params);
		echo $form->error($model, $attribute);
		echo HtmlHelper::getPopoverHint($form->hints[$attribute] ?? '', $model->getAttributeLabel($attribute));
		?>
	</div>
</div>
