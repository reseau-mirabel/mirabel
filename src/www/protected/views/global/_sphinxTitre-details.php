<?php

/** @var Controller $this */
/** @var Sourcelien[] $sourceLiens */
/** @var models\sphinx\Titres $data */
/** @var ?string $hash */

use components\HtmlHelper;

if (in_array(31, $data->lien)) { // ID for Open policy finder
	$hasPublication = true; // This is a hack to speed up this case.
} else {
	$hasPublication = \processes\titres\PolitiquePublication::hasInternalPolicy($data->id);
}

$sourceLiensToDisplay = [];
foreach ($sourceLiens as $source) {
	if (in_array((int) $source->id, $data->lien, true)) {
		$sourceLiensToDisplay[] = $source;
	}
}

$detailsClass = '';
if ($hasPublication || $sourceLiensToDisplay) {
	if (count($sourceLiensToDisplay) <= 2) {
		$detailsClass = "revue-columns peu-de-liens";
	} else {
		$detailsClass = "revue-columns";
	}
}
?>
<div class="revue-details">
	<div class="title">
		<?= HtmlHelper::titleLinkItem($data) ?>
	</div>
	<div class="frise">
		<?php
		$revue = Revue::model()->findByPk($data->revueid);
		$display = new DisplayServices($revue->getServices(), Yii::app()->user->getState('instituteId'));
		$display->setDisplayAll(false); // Fusionne les accès
		$strip = new \widgets\SvgTimeAccess(Yii::app()->user->getInstitute(), true);
		echo $strip->run($data->revueid, $display);
		?>
	</div>
	<div class="<?= $detailsClass ?>">
		<div>
			<?php
			$editeurIds = $data->getAttribute('editeuractuelid');
			if ($editeurIds) {
				$editeurs = \Yii::app()->db
					->createCommand("SELECT nom FROM Editeur WHERE id IN ($editeurIds) ORDER BY nom")
					->queryColumn();
				?>
				<div title="Structures éditoriales">
					<?= CHtml::encode(join(" ; ", $editeurs))?>
				</div>
			<?php } ?>

			<?php
			$langues = $data->getLangues();
			if ($langues) {
				?>
				<div title="Langues de publication">
					[<small><?= \models\lang\Convert::codesToFullNames($langues) ?></small>]
				</div>
			<?php } ?>

			<div class="categories-labels">
				<?php
				$categorieIds = $data->getAttribute('revuecategorie');
				if ($categorieIds) {
					$categories = Categorie::model()->findAllBySql(
						"SELECT id, categorie, description FROM Categorie WHERE id IN ($categorieIds) AND role = 'public' ORDER BY categorie"
					);
					foreach ($categories as $c) {
						/** @var Categorie $c */
						$catTitle = CHtml::link(CHtml::encode($c->categorie), $c->getSelfUrl());
						if ($c->description) {
							echo '<abbr title="' . CHtml::encode($c->description) . '" class="public">' . $catTitle . "</abbr>  ";
						} else {
							echo '<span class="public">' . $catTitle . "</span>  ";
						}
					}
				}
				?>
			</div>
		</div>
		<div style="text-align: right">
			<div>
				<?php
				if ($hasPublication) {
					echo CHtml::link('Politique de publication', ['/titre/publication', 'id' => $data->id], ['class' => 'btn-small politique-publication']);
				}
				?>
			</div>
			<ul class="referencement">
				<?php
				foreach ($sourceLiensToDisplay as $source) {
					if (in_array((int) $source->id, $data->lien, true)) {
						printf('<li title="%s">', CHtml::encode($source->nomlong));
						echo CHtml::image("/images/liens/{$source->nomcourt}.png", $source->nomcourt, ['width' => '16', 'height' => '16']);
						echo " " . CHtml::encode($source->nom);
						echo "</li>";
					}
				}
				?>
			</ul>
		</div>
	</div>
</div>
