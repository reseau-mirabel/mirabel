<?php

/** @var Controller $this */
/** @var models\sphinx\Editeurs $data */
/** @var ?string $hash */

use components\HtmlHelper;

assert($this instanceof Controller);

echo HtmlHelper::editeurLinkItem($data);
