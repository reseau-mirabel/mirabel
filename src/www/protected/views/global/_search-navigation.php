<?php

/** @var Controller $this */
/** @var ?SearchNavigation $searchNavigation */
/** @var int $currentId */
/** @var string $attr */
/** @var string $anchor */

assert($this instanceof Controller);

if (!$searchNavigation) {
	return;
}

[$prev, $next] = $searchNavigation->getNeighbours($currentId);
if (empty($prev->{$attr}) && empty($next->{$attr})) {
	return;
}

if (empty($anchor)) {
	$anchor = '';
}
?>
<div id="search-navigation">
<?php
if ($prev) {
	$prevId = (int) $prev->id;
	if ($prev instanceof \models\sphinx\Titres) {
		$prevId = (int) $prev->revueid;
	}
	echo CHtml::link(
		'<i class="icon-chevron-left" id="search-nav-prev"></i>',
		['view', 'id' => $prevId, 's' => $searchNavigation->getPreviousHash()],
		['title' => "Résultat précédent : " . $prev->{$attr}]
	);
} else {
	$prevPage = $searchNavigation->getPreviousPage();
	if ($prevPage) {
		echo CHtml::link(
			'<i class="icon-chevron-left" id="search-nav-prev"></i>',
			$prevPage,
			['title' => "Page précédente des résultats"]
		);
	}
}
echo CHtml::link(
	' <i class="icon-list" id="search-nav-up"></i> ',
	$searchNavigation->getSearchUrl($anchor),
	['title' => "Retour aux résultats de la recherche"]
);
if ($next) {
	$nextId = (int) $next->id;
	if ($next instanceof \models\sphinx\Titres) {
		$nextId = (int) $next->revueid;
	}
	echo CHtml::link(
		' <i class="icon-chevron-right" id="search-nav-next"></i>',
		['view', 'id' => $nextId, 's' => $searchNavigation->getNextHash()],
		['title' => "Résultat suivant : " . $next->{$attr}]
	);
} else {
	$nextPage = $searchNavigation->getNextPage();
	if ($nextPage) {
		echo CHtml::link(
			' <i class="icon-chevron-right" id="search-nav-next"></i>',
			$nextPage,
			['title' => "Page suivante des résultats"]
		);
	}
} ?>
</div>
