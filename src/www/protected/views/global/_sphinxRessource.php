<?php

/** @var Controller $this */
/** @var models\sphinx\Ressources $data */
/** @var ?string $hash */

use components\HtmlHelper;

assert($this instanceof Controller);

echo HtmlHelper::ressourceLinkItem($data);
