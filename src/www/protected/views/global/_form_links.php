<?php

/** @var Controller $this */
/** @var BootActiveForm $form */
/** @var Titre|Editeur $model */
/** @var bool $embedded */

use components\HtmlHelper;

assert($this instanceof Controller);

$modelName = CHtml::modelName($model);
if (!$embedded) {
	(new \widgets\LinksForm($modelName))->run();
}
?>
<div id="<?= $modelName ?>-saisie-liens">
<div class="control-group saisie-liens">
	<label class="control-label">Autres liens externes</label>
	<?php
	$i = 0;
	if ($model->getLiensExternes()) {
		foreach ($model->getLiensExternes() as $lien) {
			/** @var Lien $lien */ ?>
			<div class="controls">
				<?= $form->errorSummary($lien); ?>
				<label class="subelement">Source : </label>
				<?php
				echo $form->textField(
				$lien,
				'src',
				['name' => $modelName . "[liens][$i][src]", 'class' => 'source']
			); ?>
				<span style="white-space: nowrap">
					<label class="subelement">Adresse :</label>
					<input name="<?= $modelName ?>[liens][<?= $i ?>][url]" type="text" value="<?= CHtml::encode($lien->url) ?>"
						class="url input-xlarge" autocomplete="off" />
					<?= HtmlHelper::displayLinksHint($form, "Liens externes", 'liensExternes', 0, $i); ?>
				</span>
			</div>
			<?php
			$i++;
		}
	}
	$numExtFields = $i + 2;
	for (; $i < $numExtFields; $i++) {
		?>
		<div class="controls">
			<label class="subelement">Source : </label>
			<input name="<?= $modelName . "[liens][$i]" ?>[src]" type="text" class="source" />
			<span style="white-space: nowrap">
				<label class="subelement">Adresse : </label>
				<input name="<?= $modelName . "[liens][$i]" ?>[url]" type="text" size="80" value="" class="url input-xlarge" autocomplete="off" />
				<?= HtmlHelper::displayLinksHint($form, "Liens externes", 'liensExternes', 0, $i); ?>
			</span>
		</div>
		<?php
	}
	?>
	<div class="controls">
		<button type="button" class="btn btn-default add-input">+</button>
	</div>
</div>

<div class="control-group saisie-liens">
	<label class="control-label">Liens internes (Titres liés)</label>
	<?php
	$startRank = $i;
	if ($model->getLiensInternes()) {
		foreach ($model->getLiensInternes() as $lien) {
			/** @var Lien $lien */ ?>
			<div class="controls">
				<?php
				if ($lien->hasErrors()) {
					echo $form->errorSummary($lien);
				} ?>
				<label class="subelement">Source : </label>
				<?= CHtml::textField($modelName . "[liens][$i][src]", $lien->src, ['class' => 'source']); ?>
				<span style="white-space: nowrap">
					<label class="subelement">Adresse : </label>
					<input name="<?= $modelName ?>[liens][<?= $i ?>][url]" type="text"
						value="<?= CHtml::encode($lien->url) ?>" class="url input-xlarge" autocomplete="off" />
					<?= HtmlHelper::displayLinksHint($form, "Liens internes", 'liensInternes', $startRank, $i); ?>
				</span>
			</div>
			<?php
			$i++;
		}
	}
	$numIntFields = $i + 2;
	for (; $i < $numIntFields; $i++) {
		?>
		<div class="controls">
			<label class="subelement">Source : </label>
			<input type="text" name=<?= $modelName ?>[liens][<?= $i ?>][src]" class="source" />
			<span style="white-space: nowrap">
				<label class="subelement">Adresse : </label>
				<input name="<?= $modelName ?>[liens][<?= $i ?>][url]" type="text" size="80" value="" class="url input-xlarge" autocomplete="off" />
				<?= HtmlHelper::displayLinksHint($form, "Liens internes", 'liensInternes', $startRank, $i) ?>
			</span>
		</div>
		<?php
	}
	?>
	<div class="controls">
		<button type="button" class="btn btn-default add-input">+</button>
	</div>
</div>
</div>
