<?php

/** @var Controller $this */
/** @var CActiveRecord $model */
/** @var string $attribute */
/** @var string $value */
/** @var string $url */
/** @var array $urlParams */
/** @var ?array $htmlOptions */
/** @var BootActiveForm $form */
/** @var ?string $prefix */

use components\HtmlHelper;

assert($this instanceof Controller);

\Yii::app()->clientScript->registerCoreScript('jquery.ui');

if (!isset($htmlOptions)) {
	$htmlOptions = ['class' => 'span6'];
}
if (!empty($prefix)) {
	$attributeWithPrefix = "{$prefix}[{$attribute}Complete]";
} elseif (preg_match('/^(\[[^\]]*\])(\w.+)$/', $attribute, $m)) {
	$attributeWithPrefix = CHtml::modelName($model) . $m[1] . "[{$m[2]}Complete]";
} else {
	$attributeWithPrefix = CHtml::modelName($model) . "[{$attribute}Complete]";
}
CHtml::resolveNameID($model, $attribute, $htmlOptions);
$error = '';
if ($model->hasErrors($attribute)) {
	$htmlOptions['class'] .= ' error';
	$error = ' error';
}

if (empty($urlParams)) {
	$urlParams = [];
}
$urlParams['term'] = 'js:request.term';

$params = [
	'name' => $attributeWithPrefix,
	'source' => 'js:function(request, response) {
	$.ajax({
	url: "' . $this->createUrl($url) . '",
	data: ' . CJavaScript::encode($urlParams) . ',
	success: function(data) { response(data); }
	});
}',
	// additional javascript options for the autocomplete plugin
	// See <http://jqueryui.com/demos/autocomplete/#options>
	'options' => [
		// min letters typed before we try to complete
		'minLength' => '3', // Sphinx conf has `min_prefix_len = 3`
		'select' => "js:
function(event, ui) {
	jQuery('#{$htmlOptions['id']}').val(ui.item.id);
}",
	],
	'htmlOptions' => [
		'class' => $htmlOptions['class'],
		//'id' => $htmlOptions['id'],
		//'placeholder' => 'Saisissez le début du terme'
	],
];
if (!empty($value)) {
	$params['value'] = $value;
}

?>
<div class="control-group<?= $error ?>">
	<label class="control-label required" for="<?= $htmlOptions['id'] ?>">
		<?php
		$attributeNoSuffix = preg_replace('/\[.+\]$/', '', $attribute);
		echo $model->getAttributeLabel($attributeNoSuffix);
		if ($model->isAttributeRequired($attributeNoSuffix)) {
			echo '<span class="required">*</span>';
		}
		?>
	</label>
	<div class="controls">
		<?php
		echo $form->hiddenField($model, $attribute, $htmlOptions);
		$this->widget('zii.widgets.jui.CJuiAutoComplete', $params);
		echo $form->error($model, $attribute);
		echo HtmlHelper::getPopoverHint($form->hints[$attribute] ?? '', $model->getAttributeLabel($attribute));
		?>
	</div>
</div>
<?php
Yii::app()->clientScript->registerCoreScript('jquery.ui');
Yii::app()->getClientScript()->registerScript(
	"jui-autocomplete-custom-{$htmlOptions['id']}",
	<<<EOJS
		const instance = $("#{$htmlOptions['id']}Complete").autocomplete("instance");
		instance.menu.options.items = '> li:not(.ui-menu-item-disabled)';
		instance._renderItem = function(ul, item) {
			if (item.hasOwnProperty('forbid') && item.forbid !== '') {
				return $('<li class="ui-menu-item-disabled">')
					.append("<div>" + item.label + '</div><div class="item-forbid">' + item.forbid + "</div>")
					.appendTo(ul);
			}
			return $('<li>')
				.append("<div>" + item.label + "</div>")
				.appendTo(ul);
		};
		EOJS
);
