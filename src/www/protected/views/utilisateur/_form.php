<?php

/** @var Controller $this */
/** @var \models\forms\User $model */

assert($this instanceof Controller);

/** @var BootActiveForm $form */
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'utilisateur-form',
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'enableAjaxValidation' => false,
		'hints' => Hint::model()->getMessages('Utilisateur'),
		'htmlOptions' => ['class' => 'well'],
	]
);
$isAdmin = Yii::app()->user->access()->toUtilisateur()->admin();
$partenaire = $model->getPartenaire();
?>

<?php
echo $form->errorSummary($model);

if (Yii::app()->user->access()->toUtilisateur()->disable($model->record)) {
	echo '<p class="help-block">Les champs avec <span class="required">*</span> sont obligatoires.</p>';
	echo $form->checkBoxRow($model, 'actif', ['label-position' => 'left']);
}
if ($partenaire === null) {
	if ($isAdmin) {
		echo $form->dropDownListRow(
			$model,
			'partenaireId',
			\components\SqlHelper::sqlToPairs("SELECT id, nom FROM Partenaire WHERE type='normal' ORDER BY nom"),
			['class' => 'span6', 'empty' => '']
		);
	}
} else {
	echo CHtml::tag(
		'div',
		['class' => 'control-group'],
		'<label class="control-label">Partenaire</label> <div class="controls" style="padding: 5px 0">'
			. CHtml::encode($partenaire->nom)
			. '</div>'
	);
}
?>
<fieldset class="control-group">
	<legend>Profil</legend>
	<?php
	if ($isAdmin) {
		if (empty($model->partenaireId) || empty($partenaire->editeurId)) {
			?>
			<p>
				Si l'authentification (login + mot de passe) passe par LDAP,
				adresse électronique, nom et prénom seront réinitialisés à la prochaine connexion.
			</p>
		<?php
		}
	}
	if (Yii::app()->user->access()->toUtilisateur()->update($model->record)) {
		echo $form->textFieldRow($model, 'nom');
		echo $form->textFieldRow($model, 'prenom');
		echo $form->textFieldRow($model, 'nomComplet', ['class' => 'span6']);
		if ($isAdmin || ($model->hasIndependantLogin() && $model->getValidators('login'))) {
			echo $form->textFieldRow($model, 'login', ['class' => 'span6']);
		}
		Yii::app()->clientScript->registerCoreScript('jquery');
		if (!$model->id) {
			Yii::app()->clientScript->registerScript(
				'auto-login',
				'const autoLoginUrl = ' . json_encode($this->createAbsoluteUrl('/utilisateur/suggest-login'), JSON_UNESCAPED_UNICODE) . ";"
			);
		}
		Yii::app()->clientScript->registerScript('nomcomplet', '
$("#User_nom, #User_prenom").on("input", function() {
	$("#User_nomComplet").val($("#User_prenom").val() + " " + $("#User_nom").val());
	if (typeof autoLoginUrl !== "undefined") {
		$.ajax({
			url: autoLoginUrl,
			data: { nom: $("#User_nom").val(), prenom: $("#User_prenom").val() }
		}).done(function(suggests) {
			if (suggests) {
				$("#User_login").val(suggests);
			}
		});
	}
});
');
	}
	if (Yii::app()->user->access()->toUtilisateur()->annotate($model->record)) {
		echo $form->textAreaRow($model, 'notes', ['class' => 'span6', 'placeholder' => "Notes privées, réservées aux admins."]);
	}
	echo $form->textFieldRow($model, 'email', ['class' => 'span6', 'type' => 'email']);
	?>
</fieldset>

<?php if (Yii::app()->user->access()->toUtilisateur()->setPassword($model->record)) { ?>
<fieldset class="control-group">
	<legend>Authentification</legend>
	<p>
		Nouveau mot de passe à enregistrer, à saisir deux fois.
		<?php if ($model->id) {
			echo "Laisser vide pour conserver le mot de passe actuel.";
		} ?>
	</p>
	<?php
	if ($isAdmin && (empty($model->partenaireId) || $partenaire->type === Partenaire::TYPE_NORMAL)) {
		echo $form->checkBoxRow($model, 'authmethod', ['label-position' => 'left', 'value' => Utilisateur::AUTHMETHOD_LDAP]);
	}
	echo $form->passwordFieldRow($model, 'motdepasse', ['class' => 'span6 password-strength']);
	(new \widgets\PasswordStrength)->run(
		'password-strength',
		[$model->nom, $model->prenom, $model->email, $model->login]
	);
	$model->passwordDuplicate = '';
	echo $form->passwordFieldRow($model, 'passwordDuplicate', ['class' => 'span6']);
	?>
</fieldset>
<?php } ?>

<?php if ($isAdmin) { ?>
<fieldset class="control-group">
	<legend>Permissions</legend>
	<?php
	echo $form->checkBoxRow($model, 'permAdmin');
	echo $form->checkBoxRow($model, 'permImport');
	echo $form->checkBoxRow($model, 'permPartenaire');
	echo $form->checkBoxRow($model, 'permIndexation');
	echo $form->checkBoxRow($model, 'permRedaction');
	echo $form->checkBoxRow($model, 'permPolitiques');
	Yii::app()->clientScript->registerScript('permAdmin_confirm', <<<JS
		document.querySelector('#User_permAdmin').addEventListener('change', function(e) {
			const message = "Le rôle d'administrateur donne accès à tout Mir@bel, y compris les opérations dangereuses. Confirmez-vous l'attribution de ce rôle ?";
			if (e.target.checked && !confirm(message)) {
				e.preventDefault();
				e.target.checked = false;
				return false;
			}
			return true;
		});
		JS
	);
	?>
</fieldset>
<fieldset class="control-group">
	<legend>Suivi (droits individuels)</legend>
	<?php
	echo $form->checkBoxRow($model, 'suiviEditeurs');
	echo $form->checkBoxRow($model, 'suiviNonSuivi');
	echo $form->checkBoxRow($model, 'suiviPartenairesEditeurs');
	?>
</fieldset>
<?php } ?>

<?php
if (Yii::app()->user->access()->toUtilisateur()->listesDiffusion($model->record)) {
	?>
	<fieldset class="control-group">
		<legend>Listes de diffusion</legend>
		<?= $form->checkBoxRow($model, 'listeDiffusion', ['label' => "Abonné à la liste [mirabel_contenu]"]); ?>
		<?= $form->checkBoxRow($model, 'listeFranciliens', ['label' => "Abonné à la liste [mirabel_franciliens]"]); ?>
		<?= $form->checkBoxRow($model, 'listeLyonnais', ['label' => "Abonné à la liste [mirabel_lyonnais]"]); ?>
	</fieldset>
	<?php
}
?>

<div class="form-actions">
	<button type="submit" class="btn btn-primary"><?= $model->id ? 'Enregistrer' : 'Créer' ?></button>
</div>

<?php
$this->endWidget();
?>
