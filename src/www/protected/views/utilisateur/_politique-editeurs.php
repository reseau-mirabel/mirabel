<?php

/** @var Controller $this */
/** @var Utilisateur $user */

assert($this instanceof Controller);

$relations = $user->getEditeursRoles();
?>
<section id="utilisateur-editeurs">
	<h2>Responsabilités de déclaration de politiques</h2>

	<?php
	if ($relations) {
		?>
		<p>
			Cet utilisateur prend en charge la déclaration dans Mir@bel de la politique de publication pour les éditeurs :
		</p>
		<ul>
			<?php
			$frRoles = [
				'pending' => 'en attente de confirmation',
				'owner' => 'responsable',
				'guest' => 'délégué',
			];
			foreach ($relations as $row) {
				echo '<li>',
					$row['editeur']->getSelfLink(),
					" " . CHtml::tag("span", ['class' => 'label'], $frRoles[$row['role']] ?? $row['role'] ?: "(vide)"),
					" (" . CHtml::link("politiques", ['/politique', 'editeurId' => $row['editeur']->id]) . ")",
					'</li>';
			}
			?>
		</ul>
		<?php
	} else {
		echo '<p class="alert alert-danger">Aucun éditeur.</p>';
	}
	?>
</section>
