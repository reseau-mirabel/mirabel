<?php

/** @var Controller $this */
/** @var \models\forms\User $model */

use components\SqlHelper;

assert($this instanceof Controller);

$isAdmin = Yii::app()->user->access()->toUtilisateur()->admin();
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'utilisateur-form',
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'enableAjaxValidation' => false,
		'htmlOptions' => ['class' => 'well'],
	]
);
/** @var BootActiveForm $form */
?>

<?= $form->errorSummary($model) ?>

<?php if ($isAdmin) { ?>
<fieldset class="control-group">
	<legend>Statut du compte <em>politiques</em></legend>
	<?php
	echo $form->checkBoxRow($model, 'actif', ['label-position' => 'left']);
	echo $form->dropDownListRow(
		$model,
		'partenaireId',
		SqlHelper::sqlToPairs("SELECT id, CONCAT(IF(type = 'editeur', '(Éd) ', ''), nom) AS nom FROM Partenaire WHERE type IN ('normal', 'editeur') ORDER BY nom"),
		['class' => 'span6', 'empty' => '', 'hint' => "Rattachement du compte purement consacré aux politiques d'éditeurs à un partenaire de Mir@bel."]
	);
	?>
</fieldset>
<?php } ?>

<fieldset class="control-group">
	<legend>Profil</legend>
	<?php
	echo $form->textFieldRow($model, 'nom');
	echo $form->textFieldRow($model, 'prenom');
	echo $form->textFieldRow($model, 'nomComplet', ['class' => 'span6', 'required' => true]);
	Yii::app()->clientScript->registerCoreScript('jquery');
	Yii::app()->clientScript->registerScript('nomcomplet', '
$("#User_nom, #User_prenom").on("input", function() {
$("#User_nomComplet").val($("#User_prenom").val() + " " + $("#User_nom").val());
if (typeof autoLoginUrl !== "undefined") {
	$.ajax({
		url: autoLoginUrl,
		data: { nom: $("#User_nom").val(), prenom: $("#User_prenom").val() }
	}).done(function(suggests) {
		if (suggests) {
			$("#User_login").val(suggests);
		}
	});
}
});
');
	echo $form->emailFieldRow($model, 'email', ['class' => 'span6', 'required' => true]);
	?>
</fieldset>

<?php if ($isAdmin || $model->id > 0) { ?>
<fieldset class="control-group">
	<legend>Authentification</legend>
	<p>
		Nouveau mot de passe à enregistrer, à saisir deux fois.
		Laisser vide pour ne rien changer.
	</p>
	<?php
	echo $form->passwordFieldRow($model, 'motdepasse', ['class' => 'span6 password-strength']);
	(new \widgets\PasswordStrength)->run(
		'password-strength',
		[$model->nom, $model->prenom, $model->email, $model->login]
	);
	$model->passwordDuplicate = '';
	echo $form->passwordFieldRow($model, 'passwordDuplicate', ['class' => 'span6']);
	?>
</fieldset>
<?php } ?>


<div class="form-actions">
	<button type="submit" class="btn btn-primary">Enregistrer</button>
</div>

<?php
$this->endWidget();
?>
