<?php

/** @var Controller $this */
/** @var \models\forms\UtilisateurMultiForm $model */

assert($this instanceof Controller);
$this->breadcrumbs[] = 'Création par lot';
?>

<h1>Création d'utilisateurs par lot (démonstration/tests)</h1>

<?php
/** @var BootActiveForm $form */
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'utilisateurmulti-form',
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'enableAjaxValidation' => false,
		'htmlOptions' => ['class' => 'well'],
	]
);
?>

<p>
	Les comptes créés utiliseront les préfixes saisis, et seront suffixés de 01 à N, le nombre choisi.
	Le mot de passe sera commun.
</p>

<?php
echo $form->errorSummary($model);
?>
<fieldset class="control-group">
	<legend>Partenaire</legend>
<?php
	echo $form->textFieldRow($model, 'partenaire', ['placeholder' => 'Nouveau partenaire']);
	echo $form->inputRow('number', $model, 'nombre', null, ['placeholder' => 'Nombre de comptes', 'min' => 1, 'max' => 99]);
?>
</fieldset>
<fieldset class="control-group">
	<legend>Profil</legend>
	<?php
	echo $form->textFieldRow($model, 'nomPrefix');
	echo $form->textFieldRow($model, 'prenomPrefix');
	echo $form->textFieldRow($model, 'loginPrefix');
	echo $form->textFieldRow($model, 'motdepasse');
	?>
</fieldset>

<div class="form-actions">
	<button type="submit" class="btn btn-primary">Créer</button>
</div>

<?php
$this->endWidget();
?>
