<?php

/** @var Controller $this */
/** @var Utilisateur $user */
/** @var \models\forms\UserPolitique $model */
/** @var array $roles */

use components\HtmlHelper;

assert($this instanceof Controller);

$this->pageTitle = "Responsabilités sur les politiques des éditeurs pour";
$this->breadcrumbs[] = 'Responsabilités sur des politiques';

Yii::app()->getClientScript()->registerCssFile('/css/glyphicons.css');
?>

<h1><?= CHtml::encode($this->pageTitle); ?> <em><?= CHtml::encode($user->nomComplet) ?></em></h1>

<h2>Responsabilités actuelles</h2>

<p>
	Ce compte déclare les politiques des revues appartenant aux éditeurs :
</p>
<?php
if ($roles) {
	$editeurs = $user->editeurs;
	$frRoles = [
		'guest' => "délégué",
		'owner' => "responsable",
		'pending' => "en attente de confirmation",
	];
	echo "<ul>";
	foreach ($roles as $r) {
		$frRole = $frRoles[$r['role']] ?? $r['role'];
		echo '<li>';
		echo $editeurs[$r['editeurId']]->getSelfLink() . " ({$frRole})";
		if (Yii::app()->user->access()->toPolitique()->removePublisher($user, (int) $r['editeurId'])) {
			echo ' <div style="display: inline-block">' . HtmlHelper::postButton(
				'<span class="glyphicon glyphicon-trash"></span>',
				['/utilisateur/remove-editeur', 'id' => $user->id],
				['editeurId' => $r['editeurId']],
				['class' => 'btn btn-danger btn-small', 'onclick' => 'return confirm("Supprimer définitivement ?")']
			) . '</div>';
		}
		echo '</li>';
	}
	echo "</ul>";
} else {
	echo "<p><em>Aucun éditeur.</em></p>";
}
?>

<h2>Responsabilités nouvelles</h2>

<?php
if (!Yii::app()->user->access()->toPolitique()->validate()) {
	?>
	<p>
		L'équipe de Mir@bel devra valider cette demande avant que les droits complets soient ouverts
		sur les politiques de ces éditeurs.
	</p>
	<?php
}
?>

<?php
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'utilisateur-politiques-form',
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'enableAjaxValidation' => false,
	]
);
/** @var BootActiveForm $form */

?>

<input type="hidden" name="utilisateurId" value="<?= $user->id ?>" />
<?= (new widgets\PolitiqueSelectPublishers($model))->run() ?>

<div class="form-actions">
	<button type="submit" class="btn btn-primary">
		Enregistrer
		<?= Yii::app()->user->access()->toPolitique()->validate() ? "" : "ces demandes" ?>
	</button>
</div>

<?php
$this->endWidget();
