<?php

/** @var Controller $this */
/** @var Utilisateur $model */

use components\HtmlHelper;

assert($this instanceof Controller);

$isAdmin = Yii::app()->user->access()->toUtilisateur()->admin();

$this->pageTitle = "Utilisateur " . CHtml::encode($model->login);
if ($isAdmin) {
	$this->breadcrumbs = [
		'Utilisateurs' => ['/utilisateur/admin'],
		$model->login,
	];
} else {
	$this->breadcrumbs = [
		'Mon profil',
	];
}
?>

<h1>Utilisateur <em><?= CHtml::encode("{$model->nomComplet} ({$model->login})") ?></em></h1>

<?php
if (Yii::app()->user->id == $model->id) {
	echo "<p>" . CHtml::link("Modifier mon profil…", ['/utilisateur/update', 'id' => $model->id], ['class' => 'btn btn-small']) . "</p>";
}

if (!Yii::app()->user->access()->toUtilisateur()->view($model)) {
	$attributes = [
		'login',
		'email' => [
			'name' => 'email',
			'type' => 'email',
			'cssClass' => 'email',
			'visible' => $model->actif,
		],
		[
			'name' => 'actif',
			'type' => 'boolean',
			'cssClass' => ($model->actif ? '' : 'warning'),
			'visible' => $isAdmin,
		],
		'nomComplet',
		'nom',
		'prenom',
	];
} else {
	$attributes = [
		[
			'name' => 'id',
			'visible' => $isAdmin,
		],
		'login',
		'email' => [
			'name' => 'email',
			'type' => 'email',
			'cssClass' => 'email',
		],
		[
			'name' => 'actif',
			'type' => 'boolean',
			'cssClass' => ($model->actif ? '' : 'warning'),
			'visible' => $isAdmin,
		],
		'nomComplet',
		'nom',
		'prenom',
		[
			'name' => 'message',
			'type' => 'ntext',
			'visible' => $isAdmin,
		],
		[
			'name' => 'notes',
			'type' => 'ntext',
			'visible' => Yii::app()->user->access()->toUtilisateur()->annotate($model),
		],
		'derConnexion:datetime',
		'hdateCreation:datetime',
		[
			'name' => 'hdateModif',
			'value' => Yii::app()->format->formatDatetime($model->hdateModif),
			'visible' => $model->hdateCreation !== $model->hdateModif,
		],
	];
}
$this->widget(
	'bootstrap.widgets.BootDetailView',
	[
		'data' => $model,
		'attributes' => $attributes,
	]
);
?>

<?= $this->renderPartial('_politique-editeurs', ['user' => $model]) ?>

<?php if (Yii::app()->user->id == $model->id) { ?>
<section style="margin-top: 5ex">
	<h2>Opérations dangereuses</h2>
	<div style="margin: 2ex 0 0 0">
		<?= HtmlHelper::postButton(
				"Supprimer mon compte",
				['/utilisateur/delete', 'id' => $model->id],
				[],
				[
					'class' => "btn btn-danger",
					'onclick' =>
						<<<EOJS
						return confirm("L'opération est irréversible. Êtes-vous certain de vouloir supprimer votre compte ?")
						EOJS
				]
			)
		?>
		<p>
			Les politiques créées seront conservées, mais votre nom et votre adresse électronique seront effacés.
		</p>
	</div>
</section>
<?php } ?>
