<?php

/** @var Controller $this */
/** @var \models\forms\User $model */

assert($this instanceof Controller);
?>
<p class="info">
	Vous êtes authentifié par LDAP.
	Par conséquent, votre profil modifiable est limité aux listes de diffusion.
</p>
<?php
/** @var BootActiveForm $form */
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'utilisateur-form',
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'enableAjaxValidation' => false,
		'hints' => Hint::model()->getMessages('Utilisateur'),
		'htmlOptions' => ['class' => 'well'],
	]
);
$isAdmin = Yii::app()->user->access()->toUtilisateur()->admin();
$partenaire = $model->getPartenaire();
?>

<?php
echo $form->errorSummary($model);

if ($partenaire !== null) {
	echo CHtml::tag(
		'div',
		['class' => 'control-group'],
		'<label class="control-label">Partenaire</label> <div class="controls" style="padding: 5px 0">'
			. CHtml::encode($partenaire->nom)
			. '</div>'
	);
}

if (Yii::app()->user->access()->toUtilisateur()->listesDiffusion($model->record)) {
	?>
	<fieldset class="control-group">
		<legend>Listes de diffusion</legend>
		<?= $form->checkBoxRow($model, 'listeDiffusion', ['label' => "Abonné à la liste [mirabel_contenu]"]); ?>
		<?= $form->checkBoxRow($model, 'listeLyonnais', ['label' => "Abonné à la liste [mirabel_lyonnais]"]); ?>
	</fieldset>
	<?php
}
?>

<div class="form-actions">
	<button type="submit" class="btn btn-primary"><?= $model->id ? 'Enregistrer' : 'Créer' ?></button>
</div>

<?php
$this->endWidget();
?>
