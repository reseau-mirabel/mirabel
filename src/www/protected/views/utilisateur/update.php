<?php

/** @var Controller $this */
/** @var \models\forms\User $model */
assert($this instanceof Controller);

$this->pageTitle = "Utilisateur - modifier";
$this->breadcrumbs[] = 'Modifier';
?>

<h1>Modifier l'utilisateur <em><?= CHtml::encode($model->login); ?></em></h1>

<?php
if ($model->authmethod == Utilisateur::AUTHMETHOD_LDAP && !Yii::app()->user->access()->toUtilisateur()->admin()) {
	if (Yii::app()->user->access()->toUtilisateur()->listesDiffusion($model->record)) {
		echo $this->renderPartial('_form-ldap', ['model' => $model]);
	} else {
		echo '<p class="info">Vous êtes authentifié par LDAP. Votre profil n\'est pas modifiable.</p>';
	}
} elseif (!$model->partenaireId) {
	echo $this->renderPartial('_form-politiques', ['model' => $model]);
} else {
	echo $this->renderPartial('_form', ['model' => $model]);
}
?>
