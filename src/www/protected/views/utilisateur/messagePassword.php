<?php

use models\forms\PasswordMailForm;

/** @var Controller $this */
/** @var Utilisateur $model */
/** @var PasswordMailForm $formData */

assert($this instanceof Controller);

$this->pageTitle = Yii::app()->name . ' - Email MdP';
$this->breadcrumbs[] = 'Email mdp';
?>

<h1><?= CHtml::encode($model->nomComplet) ?> : envoi d'un mot de passe</h1>

<?php
$form = $this->beginWidget(
	'BootActiveForm',
	[
		'id' => 'contact-form',
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'enableClientValidation' => false,
	]
);
/** @var BootActiveForm $form */
?>

<?php
echo $form->errorSummary($formData);
echo $form->textFieldRow($formData, 'subject', ['class' => 'span6']);
echo $form->textAreaRow($formData, 'body', ['rows' => 25, 'cols' => 80, 'style' => 'width: auto;']);

echo '<div class="form-actions">';
$this->widget(
	'bootstrap.widgets.BootButton',
	[
		'buttonType' => 'submit',
		'type' => 'primary',
		'label' => 'Envoyer',
	]
);
echo '</div>';

$this->endWidget();
?>
