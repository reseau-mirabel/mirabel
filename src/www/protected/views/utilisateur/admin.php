<?php

/** @var Controller $this */
/** @var UtilisateurSearch $model */
/** @var array $politiques Utilisateur.id => #Editeur */

use components\SqlHelper;

$this->containerClass = "container full-width";
assert($this instanceof UtilisateurController);

$this->pageTitle = "Administration des utilisateurs";
$this->breadcrumbs = [
	"Admin" => '/admin',
	"utilisateurs",
];
\Yii::app()->getComponent('sidebar')->menu[] = [
	'label' => 'Utilisateurs-politiques',
	'url' => ['/admin/politique-utilisateurs'],
];
?>

<h1>Administration des utilisateurs</h1>
<?php if (!array_filter($model->attributes)) { ?>
<p>
	Par défaut, l'affichage ne présente que les utilisateurs
	des membres du réseau (partenaires-veilleurs, correspondant ici au filtre de type "normal").
	Les filtres permettent d'afficher aussi les autres utilisateurs
	(partenaires-éditeurs ou sans partenaires).
</p>
<?php } ?>
<p>
	Vous pouvez saisir un opérateur de comparaison
	(<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
	or <b>=</b>) au début de chaque zone de recherche afin de désigner le mode de comparaison.
</p>

<?php
$partenairesList = SqlHelper::sqlToPairs("SELECT id, nom FROM Partenaire ORDER BY nom", ['-1' => "*sans partenaire*"]);
$columns = [
	[
		'name' => 'login',
		'type' => 'raw',
		'value' => function (UtilisateurSearch $data) {
			return CHtml::link(CHtml::encode($data->login), ['view', "id" => $data->id]);
		},
	],
	'nom',
	'prenom',
	'email',
	[
		'name' => 'partenaireId',
		'value' => function (UtilisateurSearch $data) {
			return $data->partenaireId ? $data->partenaire->getFullName() : '';
		},
		'filter' => $partenairesList,
	],
	[
		'name' => 'actif',
		'type' => 'boolean',
		'filter' => ['Non', 'Oui'],
	],
	'derConnexion:datetime',
	'hdateModif:datetime',
	[
		'name' => 'special',
		'value' => function (UtilisateurSearch $data) use ($politiques) {
			return $data->getSpecialities()
				. (isset($politiques[$data->id]) ? " " . CHtml::tag('abbr', ['title' => "politiques de {$politiques[$data->id]} éditeurs"], "{$politiques[$data->id]}pol") : '');
		},
		'type' => 'raw',
		'filter' => UtilisateurSearch::$specialValues,
	],
	[
		'name' => 'partenaireType',
		'type' => 'text',
		'filter' => ["editeur" => "éditeur", "normal" => "normal", "sans" => "sans"],
	],
	[
		'name' => 'listes',
		'type' => 'raw',
		'filter' => UtilisateurSearch::BRIEFLISTS,
		'value' => function (UtilisateurSearch $data) {
			$res = $data->listeDiffusion ? '<span title="Contenus">C</span> ' : '';
			$res .= $data->listeFranciliens ? '<span title="Franciliens">F</span> ' : '';
			$res .= $data->listeLyonnais ? '<span title="Lyonnais">L</span> ' : '';
			return $res;
		},
	],
	[
		'class' => 'BootButtonColumn', // CButtonColumn
		'deleteButtonOptions' => ['title' => "Désactiver cet utilisateur"],
		'deleteConfirmation' => "Voulez-vous vraiment désactiver cet utilisateur ?",
		'header' => '',
		'updateButtonOptions' => ['title' => "Modifier ce compte"],
	],
];

$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	[
		'id' => 'utilisateur-grid',
		'dataProvider' => $model->search(),
		'filter' => $model, // null to disable
		'columns' => $columns,
		'ajaxUpdate' => false,
	]
);
?>
