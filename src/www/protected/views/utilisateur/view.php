<?php

/** @var Controller $this */
/** @var Utilisateur $model */
assert($this instanceof Controller);

$this->pageTitle = "Utilisateur {$model->login}";
?>

<h1>Utilisateur <em><?= CHtml::encode("{$model->nomComplet} ({$model->login})"); ?></em></h1>

<?php
if (Yii::app()->user->id == $model->id) {
	echo "<p>" . CHtml::link("Modifier mon profil…", ['/utilisateur/update', 'id' => $model->id], ['class' => 'btn btn-small']) . "</p>";
}

if (Yii::app()->user->access()->toUtilisateur()->view($model)) {
	$attributes = [
		'id',
		[
			'label' => $model->getAttributeLabel('partenaireId'),
			'type' => 'raw',
			'value' => $model->partenaire->getSelfLink(),
		],
		'login',
		[
			'name' => 'actif',
			'type' => 'boolean',
			'cssClass' => ($model->actif ? '' : 'warning'),
		],
		'nomComplet',
		'nom',
		'prenom',
		'email' => 'email:email',
		[
			'name' => 'authmethod',
			'value' => $model->authmethod == Utilisateur::AUTHMETHOD_LDAP ? "ldap" : "mot de passe",
		],
		[
			'name' => 'notes',
			'type' => 'ntext',
			'visible' => Yii::app()->user->access()->toUtilisateur()->annotate($model),
		],
		'derConnexion:datetime',
		'hdateCreation:datetime',
		'hdateModif:datetime',
		'permAdmin:boolean',
		'permImport:boolean',
		'permPartenaire:boolean',
		'permIndexation:boolean',
		'permRedaction:boolean',
		'permPolitiques:boolean',
		'suiviEditeurs:boolean',
		'suiviNonSuivi:boolean',
		'suiviPartenairesEditeurs:boolean',
		[
			'name' => 'listeDiffusion',
			'type' => 'boolean',
			'visible' => Yii::app()->user->access()->toUtilisateur()->listesDiffusion($model),
			'cssClass' => 'liste-contenus',
		],
		[
			'name' => 'listeFranciliens',
			'type' => 'boolean',
			'visible' => Yii::app()->user->access()->toUtilisateur()->listesDiffusion($model),
		],
		[
			'name' => 'listeLyonnais',
			'type' => 'boolean',
			'visible' => Yii::app()->user->access()->toUtilisateur()->listesDiffusion($model),
		],
	];
} else {
	$attributes = [
		[
			'label' => $model->getAttributeLabel('partenaireId'),
			'type' => 'raw',
			'value' => $model->partenaire->getSelfLink(),
		],
		'login',
		[
			'name' => 'actif',
			'type' => 'boolean',
			'cssClass' => ($model->actif ? '' : 'warning'),
		],
		'nomComplet',
		'nom',
		'prenom',
		[
			'name' => 'email',
			'type' => 'email',
			'visible' => $model->actif,
		],
	];
}
$this->widget(
	'bootstrap.widgets.BootDetailView',
	[
		'data' => $model,
		'attributes' => $attributes,
	]
);

if ($model->editeurs) {
	echo $this->renderPartial('_politique-editeurs', ['user' => $model]);
}
