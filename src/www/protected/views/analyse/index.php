<?php

/** @var Controller $this */

assert($this instanceof Controller);

$this->pageTitle = "Analyse de données";
$this->breadcrumbs = [
	'Analyse',
];
?>

<h1><?= $this->pageTitle ?></h1>

<ul>
	<li><?= CHtml::link("Comparaison de revues par ISSN", ['/analyse/compare-issn']) ?></li>
	<?php if (Yii::app()->user->checkAccess('admin')) { ?>
	<li>
		<form class="form-inline" action="/analyse/create-token">
			<label for="date">Inviter jusqu'à</label>
			<input type="date" name="date" class="input-medium" placeholder="jj/mm/aaaa" value="<?= date('Y-m-d', time()+7*86400) ?>">
			<button type="submit" class="btn">Créer un lien d'accès temporaire</button>
			à toutes les pages <code>/analyse/*</code>
		</form>
	</li>
	<?php } ?>
</ul>
