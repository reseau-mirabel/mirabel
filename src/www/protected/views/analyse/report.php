<?php

/** @var Controller $this */
/** @var processes\analyse\Compare $process */
/** @var processes\analyse\CompareResult[] $matches */
/** @var stdClass $metadata */
/** @var ?Partenaire $partenaire */
/** @var string $file */

assert($this instanceof Controller);

$this->pageTitle = "Comparaison d'une liste de revues par ISSN";
$this->breadcrumbs = [
	'Analyse' => ['/analyse/index'],
	"Revues par ISSN" => ['/analyse/compare-issn'],
	$metadata->filename,
];
?>

<h1><?= $this->pageTitle ?></h1>


<section id="synthese" style="clear: right">
	<ul>
		<li>
			Fichier
			<strong><?php
			echo CHtml::link(
				CHtml::encode($metadata->filename),
				['/analyse/download', 'file' => $file, 'name' => $metadata->filename]
			);
			?></strong>.
		</li>
		<li>
			<strong><?= $process->getNumRows() ?> lignes</strong> contenant des identifiants ISSN.
		</li>
		<li>
			<strong><?= $process->getNumMatches() ?></strong> de ces lignes ont au moins un titre correspondant dans Mir@bel,
			pour un total de <strong><?= $process->getNumTitres() ?></strong> titres
			et <strong><?= $process->getNumRevues() ?></strong> revues de Mir@bel liés à ce fichier.
		</li>
		<li>
			<strong><?= $process->getNumIssns() ?> ISSN distincts</strong> dans le fichier,
			dont <strong><?= $process->getNumIssns() - $process->getNumIssnMissing() ?></strong> présents dans Mir@bel
			et <strong><?= $process->getNumIssnMissing() ?></strong> absents de Mir@bel.
		</li>
		<li>
			<?php if ($partenaire === null) { ?>
			La colonne <em>Possession</em> est relative à l'établissement, pour les utilisateurs authentifiés dans Mir@bel.
			Elle est vide pour les utilisateurs-invités.
			<?php } else { ?>
			La colonne <em>Possession</em> est relative à l'établissement <em><?= CHtml::encode($partenaire->getFullName()) ?></em>.
			<?php } ?>
		</li>
		<li>
			L'URL de cette page peut être partagée et réutilisée pendant <?= controllers\analyse\CompareIssnAction::KEEP_FILE_DAYS ?> jours
			(durée de conservation du résultat de l'analyse).
		</li>
</section>

<section id="correspondances">
	<button type="button" id="cycle-matching-rows" class="btn btn-default btn-sm" data-state="hide-nothing">
		N'afficher que les lignes avec correspondances
	</button>

	<table class="table table-striped table-bordered table-condensed exportable">
		<thead>
			<tr>
				<th><abbr title="Numéro de ligne du fichier">Ligne</abbr></th>
				<th><abbr title="Liste des ISSN présent sur cette ligne du fichier">ISSN en entrée</abbr></th>
				<th><abbr title="Liste des ISSN de Mir@bel correspondants à ceux en entrée">ISSN de M</th>
				<th><abbr title="Titre dans Mir@bel">Titre</abbr></th>
				<th>Titre ID</th>
				<th>Revue ID</th>
				<th>URL M</th>
				<th>Suivi</th>
				<th><abbr title="Signale les lignes où les ISSN en entrée correspondent à plusieurs titres ou revues de Mir@bel">Unicité</abbr></th>
				<th><abbr title="Vide si le titre n'est pas une possession de votre établissement. Sinon, affiche l'identifiant de possession, ou à défaut ¨oui¨">Possession</abbr></th>
				<th><abbr title="Texte complet de la ligne lue dans le fichier">Source</abbr></th>
			</tr>
		</thead>
		<tbody>
			<?php
			$display = new processes\analyse\DisplayMissingIssn($process->getIssnsMissingFromMirabel());
			foreach ($matches as $row) {
				$unique = '';
				if ($row->numRevues > 1) {
					$unique = "{$row->numRevues} revues de M";
				} elseif ($row->numTitres > 1) {
					$unique = "{$row->numTitres} titres de M";
				}
				$htmlClass = trim(
					$display->getHtmlClass($row->num)
					. " " . ($row->issnMatching ? "matching" : "non-matching")
				);
				echo CHtml::tag('tr', ['class' => $htmlClass],
					"<td>{$row->num}</td>"
					. "<td>" . $display->formatList($row->num, $row->issnInput) . "</td>"
					. "<td>{$row->issnMatching}</td>"
					. "<td>" . CHtml::encode($row->titre) ."</td>"
					. "<td>" . ($row->titreId ?: "") . "</td>"
					. "<td>" . ($row->revueId > 0 ? CHtml::link((string) $row->revueId, $row->url) : '') . "</td>"
					. "<td>" . CHtml::encode($row->url) . "</td>"
					. "<td>" . ($row->suivi ? "oui" : "") . "</td>"
					. "<td>$unique</td>"
					. "<td>" . CHtml::encode($row->possession) . "</td>"
					. '<td class="source">' . CHtml::encode($row->input) . "</td>"
				);
			} ?>
		</tbody>
	</table>
</section>

<?php
// hide-nothing -> hide-non-matching -> hide-matching
Yii::app()->getClientScript()->registerScript('cycle-matching-rows', <<<EOJS
	$('#cycle-matching-rows').on('click', function() {
		const state = this.getAttribute('data-state')
		const table = $('#correspondances table.exportable');
		console.log(state)
		switch (state) {
			case 'hide-nothing':
				this.setAttribute('data-state', 'hide-non-matching');
				this.innerHTML = "N'afficher que les lignes sans correspondances";
				table.addClass('hide-non-matching').removeClass('hide-matching');
				break;
			case 'hide-non-matching':
				this.setAttribute('data-state', 'hide-matching');
				this.innerHTML = "Afficher toutes les lignes";
				table.addClass('hide-matching').removeClass('hide-non-matching');
				break;
			case 'hide-matching':
				this.setAttribute('data-state', 'hide-nothing');
				this.innerHTML = "N'afficher que les lignes avec correspondances";
				table.removeClass('hide-non-matching').removeClass('hide-matching');
				break;
		}
	});
	EOJS
);
