<?php

/** @var Controller $this */
/** @var \processes\analyse\CompareForm $formData */
/** @var string $maxUpload */

assert($this instanceof Controller);

$this->pageTitle = "Comparaison d'une liste de revues par ISSN";
$this->breadcrumbs = [
	'Analyse' => ['/analyse/index'],
	"Revues par ISSN",
];
?>

<h1><?= $this->pageTitle ?></h1>

<p>
	Chaque ligne du fichier sera comparée avec les ISSN présents dans Mir@bel pour vous permettre de savoir si ces titres sont présents dans Mir@bel.
</p>

<section id="upload">
	<?php
	/** @var BootActiveForm */
	$form = $this->beginWidget(
		'bootstrap.widgets.BootActiveForm',
		[
			'enableAjaxValidation' => false,
			'enableClientValidation' => true,
			'type' => BootActiveForm::TYPE_HORIZONTAL,
			'htmlOptions' => ['enctype' => 'multipart/form-data'],
		]
	);

	echo $form->fileFieldRow($formData, 'uploadedFile');
	?>
	<p style="margin-left: 5ex; margin-top: -2ex;">
		Le serveur est configuré pour une taille maximale de <?= CHtml::encode($maxUpload) ?>.
		Le fichier doit être soit textuel (extension <code>csv, tsv, txt</code>, par exemple KBART)
		soit tableur (extension <code>ods, xlsx</code>).
		Pour un fichier tableur, seule la première feuille est analysée.
		<br>
		Pour chaque ligne, les ISSN présents (sous la forme "1234-567X", où "X" est une clé de contrôle) seront comparés à ceux de Mir@bel.
	</p>
	<p>
		Pensez à supprimer dans le fichier tous les autres éléments qui sont de la forme <em>XXXX-XXXX</em> et qui pourraient être perçus à tort <em>comme des ISSN</em>.
		Par exemple s'il y a une colonne avec les périodes de couverture sous la forme <code>1950-2021</code> !
	</p>


	<div class="form-actions">
		<button type="submit" class="btn btn-primary">Envoyer le fichier</button>
	</div>
	<?php
	$this->endWidget();
	?>
</section>
