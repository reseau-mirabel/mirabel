<?php

use components\HtmlHelper;

/** @var Controller $this */
/** @var processes\cron\ar\Task $task */
/** @var processes\cron\ar\History[] $history */
/** @var ?processes\cron\ar\History $historyNotEmpty dernière ligne avec un message non vide de la table cronHistory */

$fqdn = $task->fqdn;
$modelName = substr($fqdn, strrpos($fqdn, '\\Model') + 6);
$this->breadcrumbs = [
	'Admin' => ['/admin'],
	'Cron' => ['/cron/index'],
	"Modèle {$modelName}" => ['index', 'q' => ['fqdn' => $modelName]],
	$task->name,
];
$this->pageTitle = "Cron Tâche {$task->name}";
?>
<h1>Tâche <em><?= CHtml::encode($task->name) ?></em></h1>

<div class="pull-right">
	<div style="display: inline-block">
	<?php
	if ($task->forceRun) {
		echo HtmlHelper::postButton("Annuler le démarrage forcé", ['/cron/force-run', 'id' => $task->id, 'status' => 0], [], ['class' => 'btn']);
	} else {
		echo HtmlHelper::postButton(
			"Démarrage forcé",
			['/cron/force-run', 'id' => $task->id, 'status' => 1],
			[],
			['class' => 'btn', 'title' => "Cette tâche sera lancée au prochain passage du cron (prochaine minute), même si elle est marquée inactive."]
		);
	}
	?>
	</div>
	<div style="display: inline-block">
		<?= HtmlHelper::postButton("Supprimer", ['/cron/delete', 'id' => $task->id], [], ['class' => 'btn btn-danger', 'onclick' => 'return confirm("Supprimer définitivement ?")']) ?>
	</div>
	<?= CHtml::link("Modifier cette tâche", ['/cron/update', 'id' => $task->id], ['class' => 'btn']) ?>
</div>

<table class="detail-view table table-striped table-condensed">
	<tbody>
		<tr>
			<th>Nom</th>
			<td><?= CHtml::encode($task->name) ?></td>
		</tr>
		<tr>
			<th>Modèle</th>
			<td>
				<strong><?= CHtml::encode($modelName) ?></strong>
				<div><?= $fqdn::getDescription() ?></div>
			</td>
		</tr>
		<?php if ($task->description) { ?>
		<tr>
			<th>Remarques</th>
			<td><?= nl2br(CHtml::encode($task->description)) ?></td>
		</tr>
		<?php } ?>
		<tr>
			<th>Activée ?</th>
			<td>
				<label class="switch-checkbox">
					<input type="checkbox" data-label-unchecked="x" <?= $task->active ? "checked" : "" ?> id="cron-task-active" data-id="<?= $task->id ?>"><span class="toggler"></span>
					<span>tâche active</span>
				</label>
				<?php
				\Yii::app()->clientScript->registerScript('cron-task-active', <<<JS
					document.getElementById('cron-task-active')
						.addEventListener('change', function() {
							fetch('/cron/toggle-active/' + this.getAttribute('data-id'), {method: "POST", credentials: "include"})
								.catch((x) => { alert("Erreur lors de l'enregistrement"); });
						});
					JS);
				if ($task->forceRun) {
					echo '<div class="alert alert-warning"><strong>démarrage forcé au prochain passage du cron</strong></div>';
				}
				?>
			</td>
		</tr>
		<?php if ($task->config !== '{}') { ?>
		<tr>
			<th>Configuration</th>
			<td>
				<details>
					<summary>
						<ul style="display: inline-block" class="unstyled">
						<?php
						try {
							foreach ($task->getTaskInstance()->getConfigSummary() as $k => $v) {
								echo "<li><strong>$k</strong> : " . (is_string($v) ? $v : join("<br>", $v)) . "</li>";
							}
						} catch (\Exception $e) {
							echo CHtml::tag('div', ['class' => "alert alert-danger"], CHtml::encode($e->getMessage()));
						}
						?>
						</ul>
					</summary>
					<pre><?= CHtml::encode(json_encode(json_decode($task->config), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT)) ?></pre>
				</details>
			</td>
		</tr>
		<?php } ?>
		<tr>
			<th>Planification</th>
			<td>
				<?php
				$schedule = $task->getScheduleObject();
				echo "<details>"
					. '<summary class="schedule">' . CHtml::encode($schedule->toString()) . '</summary>'
					. "<pre>" . CHtml::encode(json_encode($schedule, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT)) . "</pre>"
					. "</details>";
				?>
			</td>
		</tr>
		<tr>
			<th>Courriel</th>
			<td><?= nl2br(CHtml::encode($task->emailTo)) ?></td>
		</tr>
	</tbody>
</table>

<section>
	<h2>Prochains lancements</h2>
	<ul>
		<?php
		if ($task->forceRun) {
			echo "<li>Prochain passage du cron (<strong>lancement forcé</strong>)</li>";
		}

		$scheduler = new processes\cron\Scheduler();
		$dateFormatter = new IntlDateFormatter("fr-FR", IntlDateFormatter::FULL, IntlDateFormatter::SHORT);
		$runs = $scheduler->listNextRuns($task->getScheduleObject(), 3);
		$first = true;
		foreach ($runs as $datetime) {
			$content = $dateFormatter->format($datetime);
			if ($first) {
				$content .= " (<strong>" . components\DateTimeHelper::toRelativeFr($datetime) . "</strong>)";
				$first = false;
			}
			if (!$task->active) {
				$content = "<del>$content</del>";
			}
			echo CHtml::tag('li', [], $content);
		}
		?>
	</ul>
</section>

<section>
	<h2>Historique</h2>
	<p>Historique limité aux 5 derniers lancements</p>
	<?php
	$hasLock = \Yii::app()->db->createCommand("SELECT 1 FROM CronLock WHERE crontaskId = {$task->id}")->queryScalar();
	if ($hasLock) {
		echo '<div class="alert alert-info">Cette tâche est en cours..</div>';
	}
	?>
	<ul>
		<?php foreach ($history as $hrow) { ?>
		<li><?= $this->renderPartial('_history-row', ['hrow' => $hrow, 'task' => $task]); ?>
		</li>
		<?php } ?>
	</ul>
	<?php
	if ($historyNotEmpty instanceof processes\cron\ar\History) {
		echo '<p>Dernier message non vide :</p>';
		echo $this->renderPartial('_history-row', ['hrow' => $historyNotEmpty, 'task' => $task]);
	}
	?>
</section>
