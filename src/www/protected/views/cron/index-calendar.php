<?php

/** @var Controller $this */
/** @var array $launches */
/** @var DateTimeImmutable $start */
/** @var DateTimeImmutable $end */

$this->breadcrumbs = [
	'Admin' => ['/admin'],
	'Cron' => ['/cron/index'],
	'Calendrier',
];
$this->pageTitle = "Cron - Calendrier";
?>
<h1>Calendrier des tâches</h1>

<?php
$day = new DateInterval('P1D');
$week = new DateInterval('P7D');

$weekEnd = $start->add($week);
echo '<div class="calendar-week">';

$dayStart = DateTime::createFromImmutable($start);
$dayEnd = $start->add($day);
$now = new DateTime();
$launchPos = 0;
$taskSeen = [];
while ($dayEnd <= $end) {
	echo '<div class="calendar-day"' . ($now >= $dayStart && $now < $dayEnd ? ' id="today"' : "") . '">';
	$dayOfMonth = IntlDateFormatter::formatObject($dayStart, 'MMMM d', 'fr-FR');
	echo '<div class="calendar-dayofMonth">' . $dayOfMonth . '</div>';
	while ($launches[$launchPos]['time'] < $dayEnd) {
		echo '<div class="run">';
		echo CHtml::tag('span', ['class' => 'time'], $launches[$launchPos]['time']->format('H:i'));
		echo " ";
		$taskHtml = CHtml::encode($launches[$launchPos]['task']->name);
		if (!$launches[$launchPos]['task']->active) {
			$taskHtml = "<del>$taskHtml</del>";
		}
		if (isset($taskSeen[$launches[$launchPos]['task']->id])) {
			echo $taskHtml;
		} else {
			echo CHtml::link($taskHtml, ['/cron/view', 'id' => $launches[$launchPos]['task']->id]);
			$taskSeen[$launches[$launchPos]['task']->id] = true;
		}
		if ($launches[$launchPos]['task']->getScheduleObject()->periodicity == "hourly") {
			echo '<span style="color: #EB6907; "> (chaque heure)</span>';
		}
		echo '</div>';
		$launchPos++;
		if ($launchPos >= count($launches)) {
			break;
		}
	}
	echo '</div>';
	$dayStart->add($day);
	$dayEnd = $dayEnd->add($day);
	if ($dayEnd > $weekEnd) {
		echo '</div>';
		echo '<div class="calendar-week">';
		$weekEnd = $weekEnd->add($week);
	}
}

echo '</div>';
