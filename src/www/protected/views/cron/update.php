<?php

/** @var Controller $this */
/** @var \processes\cron\ar\Task $task */
/** @var \processes\cron\widgets\TaskFormWidget $widget */

$this->breadcrumbs = [
	'Admin' => ['/admin'],
	'Cron' => ['/cron/index'],
	$task->name => ['/cron/view', 'id' => $task->id],
	"Modifier",
];
$this->pageTitle = "Cron - Modifier une tâche";
?>
<h1>Cron : modifier <em><?= CHtml::encode($task->name) ?></em></h1>

<?= $widget->run() ?>