<?php

/** @var Controller $this */
/** @var \processes\cron\widgets\TaskFormWidget $widget */

$this->breadcrumbs = [
	'Admin' => ['/admin'],
	'Cron' => ['/cron/index'],
	"Nouvelle tâche",
];
$this->pageTitle = "Cron - Nouvelle tâche";
?>
<h1>Nouvelle tâche</h1>

<?= $widget->run() ?>