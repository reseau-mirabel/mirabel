<?php

use processes\cron\ar\TaskSearch;

/** @var Controller $this */
/** @var \processes\cron\ar\TaskSearch $taskSearch */
/** @var \CActiveDataProvider $provider */

$this->breadcrumbs = ['Admin' => ['/admin']];
if ($taskSearch->isEmpty()) {
	$this->breadcrumbs[] = "Cron";
} else {
	$this->breadcrumbs["Cron : Toutes les tâches"] = ['/cron/index'];
	$this->breadcrumbs[] = "Tâches filtrées";
}
$this->pageTitle = "Cron - Tâches";
?>
<div class="pull-right">
	Vue par calendrier
	<?php
	$daysSinceMonday = (date('w') + 6) % 7;
	$weekStart = (new DateTimeImmutable)->sub(new DateInterval("P{$daysSinceMonday}D"))->format('Y-m-d');
	?>
	<ul>
		<li><?= CHtml::link("aujourd'hui", ['/cron/index-calendar', 'start' => date('Y-m-d'), 'span' => 'P1D']) ?></li>
		<li><?= CHtml::link("cette semaine", ['/cron/index-calendar', 'start' => $weekStart, 'span' => 'P7D']) ?></li>
	</ul>
	<?= CHtml::link("Nouvelle tâche", ['/cron/create']) ?>
</div>

<h1>Cron : liste des tâches</h1>

<?php
\Yii::app()->clientScript->registerScript('cron-task-active', <<<JS
	$('#cron-tasks-grid').on('change', 'input.cron-task-active', function() {
		fetch('/cron/toggle-active/' + this.getAttribute('data-id'), {method: "POST", credentials: "include"})
			.catch((x) => { alert("Erreur lors de l'enregistrement"); });
	});
	JS);
$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	[
		'id' => 'cron-tasks-grid',
		'dataProvider' => $provider,
		'filter' => $taskSearch,
		'filterSelector' => '{filter}, .task-search',
		'ajaxUpdate' => false,
		'columns' => [
			[
				'name' => 'name',
				'type' => 'raw',
				'value' => function (TaskSearch $data) {
					return CHtml::link(CHtml::encode($data->name), ['/cron/view', 'id' => $data->id]);
				},
			],
			[
				'name' => 'fqdn',
				'value' => function (TaskSearch $data) {
					return substr($data->fqdn, strrpos($data->fqdn, '\\Model') + 6);
				},
				'filter' => $taskSearch->listClassNames(),
			],
			[
				'name' => 'lastRunTime',
				'type' => 'datetime',
				'filter' => false,
			],
			[
				'name' => 'lastRunMessage',
				'header' => '<abbr title="silencieux = la dernière exécution de la tâche a produit un rapport vide, sans erreur">silencieux</abbr>',
				'value' => function (TaskSearch $data) {
					if ($data->lastError) {
						return "erreur";
					}
					if ($data->lastRunTime && !$data->lastRunMessage) {
						return "oui";
					}
					return "";
				},
				'filter' => array_combine(TaskSearch::FILTER_MESSAGE, TaskSearch::FILTER_MESSAGE),
			],
			[
				'name' => 'emailTo',
				'type' => 'raw',
				'value' => function (TaskSearch $data) {
					if (strlen(trim($data->emailTo)) < 25) {
						return "<b>1</b> " . \CHtml::encode($data->emailTo);
					}
					$count = substr_count($data->emailTo, '@');
					$pos = min(1 + strpos($data->emailTo, '@'), 20);
					return \CHtml::tag(
						'abbr',
						['title' => $data->emailTo],
						"<b>$count</b> " . \CHtml::encode(substr($data->emailTo, 0, $pos)) . '…'
					);
				},
			],
			[
				'name' => 'lastUpdate',
				'value' => function (TaskSearch $data) {
					return date('Y-m-d', $data->lastUpdate);
				},
				'filter' => false,
			],
			[
				'name' => 'active',
				'type' => 'raw',
				'value' => function (TaskSearch $data) {
					$checked = $data->active ? "checked" : "";
					return <<<HTML
						<label class="switch-checkbox">
							<input type="checkbox" data-label-unchecked="x" $checked class="cron-task-active" data-id="{$data->id}"><span class="toggler"></span>
						</label>
						HTML;
				},
				'filter' => ['1' => 'actif', '0' => 'inactif'],
			],
		],
	]
);
