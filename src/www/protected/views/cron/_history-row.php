<?php
/** @var Controller $this */
/** @var processes\cron\ar\Task $task */
/** @var processes\cron\ar\History $hrow */
?>
			<table class="table table-striped table-condensed">
				<tbody>
					<tr>
						<th>Début → Fin</th>
						<td><?= date('Y-m-d H:i', $hrow->startTime) ?> → <?= date('Y-m-d H:i', $hrow->endTime) ?></td>
					</tr>
					<tr>
						<th>Durée</th>
						<td>
							<?php
							$duration = ($hrow->endTime - $hrow->startTime);
							if ($duration > 3600) {
								echo round($duration / 3600, 1) . " heures";
							} elseif ($duration > 60) {
								echo round($duration / 60) . " minutes";
							} else {
								echo "$duration secondes";
							}
							?>
						</td>
					</tr>
					<?php if ($hrow->config !== $task->config) { ?>
					<tr>
						<th>Configuration</th>
						<td><code><?= CHtml::encode($hrow->config) ?></code></td>
					</tr>
					<?php }?>
					<?php if ($hrow->error) { ?>
					<tr>
						<th>Erreur</th>
						<td><?= nl2br(CHtml::encode($hrow->error)) ?></td>
					</tr>
					<?php } else { ?>
					<tr>
						<th>Résultat</th>
						<td>
							<?= \processes\cron\widgets\HistoryReport::toHtml($hrow) ?>
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
