<?php

/** @var Controller $this */
/** @var Config $model */
assert($this instanceof Controller);

$this->breadcrumbs = [
	'Admin' => ['/admin/index'],
	'Paramètres' => ['admin'],
	$model->key,
];

\Yii::app()->getComponent('sidebar')->menu = [
	['label' => 'Créer un paramètre', 'url' => ['create']],
	['label' => 'Modifier le paramètre', 'url' => ['update', 'id' => $model->id]],
	['label' => 'Supprimer le paramètre', 'url' => '#', 'linkOptions' =>
		['submit' => ['delete', 'id' => $model->id], 'confirm' => 'Êtes-vous sûr de vouloir supprimer ce paramètre ?'],
	],
	['label' => 'Liste des paramètres', 'url' => ['admin']],
];
?>

<h1>Paramètre <em><?= $model->key; ?></em></h1>

<?php
$pencilIcon = '<span class="glyphicon glyphicon-pencil"></span>';
echo '<div class="pull-right">' . CHtml::link($pencilIcon, ['update', 'id' => $model->id]) . '</div>';

$this->widget(
	'bootstrap.widgets.BootDetailView',
	[
		'data' => $model,
		'attributes' => [
			'id',
			'category',
			'key',
			[
				'name' => 'value',
				'type' => 'raw',
				'value' => function (Config $data) {
					if ($data->type === 'html') {
						return $data->decode();
					}
					return CHtml::encode(print_r($data->decode(), true));

				},
			],
			'type',
			'descriptionraw',
			'createdOn:datetime',
			'updatedOn:datetime',
		],
	]
);
?>
