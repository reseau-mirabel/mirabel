<?php

/** @var ConfigController $this */
/** @var Config $model */
/** @var bool $valueOnly */

assert($this instanceof ConfigController);

/** @var BootActiveForm */
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'config-form',
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'enableAjaxValidation' => false,
		'hints' => $model->attributeHints(),
	]
);

if (empty($valueOnly)) {
	$htmlOptions = [];
} else {
	$htmlOptions = ['disabled' => 'disabled'];
}
?>

<?php
echo $form->errorSummary($model);

$fileParam = Yii::app()->params->itemAt($model->key);
if ($fileParam !== null) {
	echo '<p class="alert alert-warning">Ce réglage est forcé à <code>'
		, CHtml::encode($fileParam)
		, '</code> par le fichier de configuration "local.php". La valeur en base de données sera ignorée.</p>';
}

echo $form->textFieldRow($model, 'category', $htmlOptions);
echo $form->textFieldRow($model, 'key', $htmlOptions);
switch ($model->type) {
	case 'html':
		new \widgets\CkEditor();
		echo $form->textAreaRow($model, 'value', ['rows' => 10, 'cols' => 74, 'class' => 'input-xxlarge htmleditor']);
		break;
	case 'assoc':
	case 'csv':
	case 'text':
		echo $form->textAreaRow($model, 'value', ['rows' => 10, 'cols' => 74, 'class' => 'input-xxlarge']);
		break;
	case 'string':
		echo $form->textFieldRow($model, 'value', ['class' => 'input-xlarge']);
		break;
	case 'boolean':
		echo $form->dropDownListRow($model, 'value', ['Non', 'Oui']);
		break;
	default:
		echo $form->textFieldRow($model, 'value');
		break;
}
if (!empty($valueOnly)) {
	?>
	<div class="control-group">
		<div class="controls">
			<?= $model->description ?>
		</div>
	</div>
	<?php
} else {
		echo $form->textAreaRow($model, 'description', array_merge(['rows' => 5, 'cols' => 50, 'class' => 'input-xxlarge'], $htmlOptions));
	}
echo $form->dropDownListRow($model, 'type', Config::ENUM_TYPE, $htmlOptions);
?>
<div class="form-actions">
	<?php
	$this->widget(
	'bootstrap.widgets.BootButton',
	[
		'buttonType' => 'submit',
		'type' => 'primary',
		'label' => $model->isNewRecord ? 'Créer' : 'Enregistrer',
	]
);
	?>
</div>

<?php
$this->endWidget();
?>
