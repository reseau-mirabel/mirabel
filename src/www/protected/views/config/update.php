<?php

/** @var Controller $this */
/** @var Config $model */
assert($this instanceof Controller);

$this->breadcrumbs = [
	'Paramètres' => ['admin'],
	$model->key => ['view', 'id' => $model->id],
	'Modifier',
];

\Yii::app()->getComponent('sidebar')->menu = [
	['label' => 'Créer un paramètre', 'url' => ['create']],
	['label' => 'Liste des paramètres', 'url' => ['admin']],
];
?>

<h1>Modifier la configuration de <em><?= CHtml::encode($model->key) ?></em></h1>

<?php
echo $this->renderPartial('_form', ['model' => $model]);
?>
