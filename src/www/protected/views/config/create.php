<?php

/** @var Controller $this */
/** @var Config $model */
assert($this instanceof Controller);

$this->breadcrumbs = [
	'Paramètres' => ['admin'],
	'Créer',
];

\Yii::app()->getComponent('sidebar')->menu = [
	['label' => 'Liste des paramètres', 'url' => ['admin']],
];
?>

<h1>Créer un paramètre de configuration</h1>

<?php
echo $this->renderPartial('_form', ['model' => $model]);
?>
