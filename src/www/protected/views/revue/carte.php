<?php

/** @var Controller $this */
/** @var array $mapConfig */
/** @var bool $vivantes */
/** @var int $nbRevues */
/** @var string $stamen */
assert($this instanceof Controller);

$this->pageTitle = "Carte des revues";

$this->breadcrumbs = [
	'Revues' => ['index'],
	'Carte',
];
?>

<h1>Localisation des revues recensées dans Mir@bel</h1>

<figure id="carte-revues" class="carte">
	<?= (new \widgets\WorldMap($mapConfig, $stamen))->run() ?>
	<figcaption>Carte des <?= $nbRevues ?> revues<?= $vivantes ? " vivantes" : "" ?>, selon le pays de leurs éditeurs</figcaption>
</figure>

<p>
	Cliquer sur un pays vous permet d'afficher les détails
	puis de rebondir sur les listes de revues de ce pays.
</p>

<p>
	<?php
	if ($vivantes) {
		echo CHtml::link(
			"Afficher aussi les revues mortes",
			['/revue/carte', 'vivantes' => 0],
			['class' => 'btn btn-primary', 'title' => "Comptabiliser aussi les revues dont la parution a cessé."]
		);
	} else {
		echo CHtml::link(
			"N'afficher que les revues vivantes",
			['/revue/carte', 'vivantes' => 1],
			['class' => 'btn btn-primary', 'title' => "Ne pas comptabiliser les revues dont la parution a cessé."]
		);
	}
	?>
</p>

<p>
	Une revue qui a plusieurs éditeurs dans son historique de titres peut être comptabilisée dans plusieurs pays.
</p>
