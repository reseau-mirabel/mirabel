<?php

use \processes\revue\RevueTitreTableContent;

/** @var Controller $this */
/** @var Revue $revue */
/** @var Titre[] $titres (titreId => Titre) */
/** @var bool $direct */
/** @var Titre $activeTitle */

assert($this instanceof Controller);

RevueTitreTableContent::resetEtatCollection(); // global state over instances of RevueTitreTableContent
$revueIssnGroups = $revue->getIssnGroups();
$isGuest = !Yii::app()->user->checkAccess("avec-partenaire");

$instituteId = Yii::app()->user->getState('instituteId');
$institute = Yii::app()->user->getInstitute();
$tableContent = [];
foreach ($titres as $titre) {
	$tableContent[] = new RevueTitreTableContent($titre, $activeTitle, $revueIssnGroups, $isGuest, $direct, $institute);
}

Yii::app()->getClientScript()->registerScript('titre-details-popover',
	<<<EOJS
	$(".titre-details-popover").popover({
		html: true,
		content: function() {
			let titreId = this.getAttribute('data-titreid');
			$.ajax({
				url: '/revue/ajax-titre/' + titreId,
			}).then(function(data) {
				$(".popover-title").prepend('<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>');
				$("#popover-titre-" + titreId).html(data);
			});
			return '<div id="popover-titre-' + titreId + '">Chargement en cours…</div>';
		},
		placement: 'left',
		container: '#container-full-width',
	});
	$("#container-full-width").on("click", ".close", function() {
		$(".titre-details-popover").popover("hide");
	});
	EOJS
);
?>
<div id="container-full-width"></div>
<table class="table table-striped table-bordered table-condensed">
	<thead>
		<tr>
			<th>Titre <?= CHtml::link(
				CHtml::image(Yii::app()->baseUrl . '/images/plus.png', "+titre"),
				['/titre/create-by-issn', 'revueId' => $revue->id],
				[
					'title' => ($direct ? 'Ajouter' : 'Proposer') . " un nouveau titre",
					'class' => "pull-right",
				]
			) ?>
			<?php
			if (\Yii::app()->user->access()->toTitre()->createDirect($revue->id) && (count($titres) > 1)) {
				echo CHtml::link(
					'<span class="glyphicon glyphicon-sort"></span>',
					['trier-titre', 'id' => $revue->id],
					[
						'title' => 'Trier les titres',
						'class' => "pull-right",
						'style' => 'margin: 1px;',
					],
				);
			} ?></th>
			<?= $revueIssnGroups['issnp'] ? '<th title="International Standard Serial Number">ISSN</th>' : "" ?>
			<?= $revueIssnGroups['issne'] ? '<th title="International Standard Serial Number - version électronique">ISSN-E</th>' : "" ?>
			<th>Années</th>
			<th>Éditeurs</th>
			<?php
			if ($institute && RevueTitreTableContent::hasNonEmptyEtatcollection()) {
				echo CHtml::tag('th', [], $institute->getShortname() ?: "Localisation");
			}
			?>
			<th><em>Action</em></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($tableContent as $row) { ?>
			<tr itemscope itemtype="http://schema.org/Periodical" data-titreid="<?= $row->titreId ?>">
				<td><?= $row->titre ?></td>
				<?php
				if ($revueIssnGroups['issnp']) {
					echo CHtml::tag('td', ['class' => 'issn'], $row->issn);
				}
				if ($revueIssnGroups['issne']) {
					echo CHtml::tag('td', ['class' => 'issn'], $row->issne);
				}
				?>
				<td><?= $row->dates ?></td>
				<td><?= $row->editeurs ?></td>
				<?php
				if (RevueTitreTableContent::hasNonEmptyEtatcollection()) {
					echo CHtml::tag('td', [], $row->local);
				}
				?>
				<td><?= $row->actions ?></td>
			</tr>
		<?php
		}
		?>
	</tbody>
</table>
