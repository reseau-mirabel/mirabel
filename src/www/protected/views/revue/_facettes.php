<?php

/**
 * Create 3 blocs of checkboxes (to refine the base search) and many hidden inputs (the base search).
 *
 * Checkboxes fields are named like `f[paysid][]`, where "f" is hard-coded.
 * Hidden fields are named like `q[titre]`, where "q" is hard-coded.
 */

/** @var Controller $this */
/** @var SearchTitre $base */
/** @var SearchTitre $refine */
/** @var components\sphinx\FacetInterface[] $facets */
/** @var \components\sphinx\DataProvider $results */
?>
<form id="facets">
	<?php
	// Export the base criteria
	$searchAttributes = $base->exportAttributes();
	foreach ($searchAttributes->toArray() as $k => $v) {
		if (is_array($v)) {
			foreach ($v as $kk => $vv) {
				echo CHtml::hiddenField("q[$k][$kk]", $vv);
			}
		} else {
			echo CHtml::hiddenField("q[$k]", $v);
		}
	}
	?>

	<h3>Filtrer les résultats</h3>

	<?php
	$export = $refine->exportAttributes();
	if (!$searchAttributes->accesLibre && !$searchAttributes->acces) {
		// only "Texte intégral"
		$isChecked = in_array(2, $export->acces, true);
		$count = $facets['acces']->listFilters()[2][1] ?? 0; // 1 is the position of the count
		if ($count > 0) {
			?>
			<fieldset class="facet facet-acces checkbox">
				<legend>Accès</legend>
					<label class="checkbox">
						<input type="checkbox" name="f[acces][]" <?= $isChecked ? "checked" : "" ?> value="2">
					<span>
						<strong>Texte intégral</strong>
						<span class="count"><?= $count ?></span>
					</span>
				</label>
			</fieldset>
			<?php
		}
	}
	$this->renderPartial(
		'_facette_checkboxes',
		[
			'attribute' => "categorie",
			'checked' => $export->categorie,
			'facet' => $facets['revuecategorierec']->exclude($searchAttributes->categorie),
			'legend' => "Thématique",
		]
	);
	$this->renderPartial(
		'_facette_checkboxes',
		[
			'attribute' => "langues",
			'checked' => $export->langues,
			'facet' => $facets['langue']->exclude($searchAttributes->langues),
			'legend' => "Langue",
		]
	);
	$this->renderPartial(
		'_facette_checkboxes',
		[
			'attribute' => "paysId", // Property from SearchTitre
			'checked' => $export->paysId,
			'facet' => $facets['paysid']->exclude($searchAttributes->paysId),
			'legend' => "Pays",
		]
	);
	?>
</form>

<?php
Yii::app()->clientScript->registerScript('facets', <<<EOJS
	const facetsForm = document.querySelector('#facets');
	facetsForm.addEventListener('change', function(e) {
		facetsForm.submit();
	});
	EOJS
);
