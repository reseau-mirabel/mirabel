<?php

/** @var Controller $this */
/** @var processes\revue\View $process */
/** @var TitreDecorator $activeTitleDec */

$encodedTitle = CHtml::encode($process->getActiveTitle()->getFullTitle());
$encodedDescription = CHtml::encode($this->pageDescription);

$publishers = [];
foreach ($process->getActiveTitle()->editeurs as $editeur) {
	$publishers[] = $editeur->prefixe . $editeur->nom . ($editeur->sigle ? " " . $editeur->sigle : "");
}
$encodedPublishers = CHtml::encode(join(", ", $publishers));

$this->appendToHtmlHead(
	Controller::linkEmbeddedFeed('rss2', "Revue " . $process->revue->getFullTitle(), ['revue' => $process->revue->id])
);
$this->appendToHtmlHead(
	<<<EOL
	<meta name="description" content="$encodedDescription" lang="fr" />
	<meta name="keywords" content="revue, accès en ligne, texte intégral, sommaire, périodique, $encodedPublishers $encodedTitle" lang="fr" />
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/" />
	<meta name="dcterms.title" content="Mirabel : revue $encodedTitle" />
	<meta name="dcterms.subject" content="revue, accès en ligne, texte intégral, sommaire, périodique, $encodedPublishers $encodedTitle" />
	<meta name="dcterms.language" content="fr" />
	<meta name="dcterms.creator" content="Mirabel" />
	<meta name="dcterms.publisher" content="Sciences Po Lyon" />
	EOL
);

if ($activeTitleDec->hasFront()) {
	Yii::app()->clientScript->registerMetaTag($activeTitleDec->getFrontUrl(true), 'og:image', null, ['property' => 'og:image'], 'og:image');
}
