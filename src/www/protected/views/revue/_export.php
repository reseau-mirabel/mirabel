<?php

/** @var Controller $this */
/** @var SearchTitre $search */
/** @var int $totalCount */

assert($this instanceof Controller);
?>
<div id="export-results">
	<h4>Exporter les résultats</h4>
	<?= $this->renderPartial('_export-titles', ['search' => $search, 'totalCount' => $totalCount], true) ?>
	<?= $this->renderPartial('_export-services', ['search' => $search], true) ?>
</div>
