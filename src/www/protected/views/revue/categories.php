<?php

/** @var Controller $this */
/** @var \models\forms\RevueCategoriesForm $formData */
/** @var array $categorieLinks */
/** @var array $categoriesPubliques categorieId => Categorie */
/** @var bool $propositions */
/** @var bool $directAccess */
assert($this instanceof RevueController);

$fullTitle = $formData->revue->getFullTitle();
$this->pageTitle = 'Thématique pour ' . $fullTitle;

$this->breadcrumbs = [
	'Revues' => ['index'],
	$fullTitle => ['view', 'id' => $formData->revue->id],
	'Thématique',
];

\Yii::app()->getComponent('sidebar')->menu = [
	['label' => 'Liste des revues', 'url' => ['index']],
	['label' => 'Cette revue', 'url' => ['view', 'id' => $formData->revue->id]],
	['label' => 'Arbre thématique', 'url' => ['categorie/index']],
	['label' => 'Administrer les thèmes', 'url' => ['categorie/admin'], 'visible' => Yii::app()->user->checkAccess('indexation')],
];
?>

<h1>
	Thématique pour la revue <?= CHtml::encode($fullTitle); ?>
</h1>

<noscript>
	<div class="alert alert-error">
		<h2>Erreur</h2>
		<p>Cette page ne fonctionnera que si JavaScript est activé.</p>
	</div>
</noscript>

<h2>Thèmes attribués directement (manuellement) à cette revue</h2>
<?php
if ($propositions) {
	?>
	<p class="alert alert-error">
		<strong>Attention</strong>, des propositions de thèmes sont déjà en attente de validation pour cette revue.
	</p>
	<?php
}
?>

<div class="thematique thematique-publique">
<?php
if ($categorieLinks) {
	$dataProvider = new CArrayDataProvider(
		$categorieLinks,
		['keyField' => false, 'pagination' => false, ]
	);
	$this->widget(
		'ext.bootstrap.widgets.BootGridView',
		[
			'id' => 'categorierevue-grid',
			'dataProvider' => $dataProvider,
			'filter' => null,
			'ajaxUpdate' => false,
			'summaryText' => '',
			'columns' => [
				'categorie.categorie:text:Thème',
				'categorie.role:text:Rôle',
				'hdateModif:datetime:Attribué le',
				'modifiePar.selfLink:raw:Attribué par',
				[
					'header' => '',
					'type' => 'raw',
					'value' => function (CategorieRevue $data, $row) use ($formData) {
						return CHtml::link(
							"X", // CHtml::image($this->grid->baseScriptUrl . '/delete.png', "Supprimer"),
							Yii::app()->createUrl('/revue/retire-categorie', ['id' => $formData->revue->id, 'categorieId' => $data->categorieId]),
							[
								'class' => 'delete',
								'title' => "Supprimer cette association",
								'data-categorie' => json_encode($data->categorie->getAttributes(), JSON_UNESCAPED_UNICODE),
							]
						);
					},
				],
			],
		]
	);
} else {
	?>
	<p>
		Aucun thème n'est actuellement affecté directement (manuellement) à cette revue.<br />
		<strong>Attention</strong>, si vous ajoutez manuellement des thèmes,
		les thèmes attribués automatiquement, par import, ne seront plus pris en compte.
		Vous devez donc remettre manuellement ceux que vous voulez voir affichés.
	</p>
	<?php
}
?>
</div>

<h2>Thèmes attribués automatiquement à des titres de cette revue</h2>
<?php
$themesTitres = $formData->revue->getThemesTitres();
if (!$themesTitres) {
	echo "<p>Aucun.</p>";
} else {
	?>
	<p>
		Ces thèmes ont été importés automatiquement à partir du vocabulaire d'une ressource.
		Ces thématiques importées ne peuvent pas être supprimées manuellement.
	</p>
	<p>
		<?php
		if ($categorieLinks) {
			echo "<em>Ces thèmes ne sont pas publiquement visibles.</em>"
				. " Ils n'apparaissent pas au public car il y a des thèmes attribués directement à cette revue (manuellement)."
				. " <strong>Les thèmes publiquement visibles sont en gras.</strong>";
		} else {
			echo "<strong>Ces thèmes sont publiquement visibles.</strong>"
				. " Ils n'apparaissent au public QUE s'il n'y a pas de thèmes attribués directement à cette revue (manuellement).";
		} ?>
	</p>
	<div class="thematique <?= ($categorieLinks ? "thematique-privee" : "thematique-publique") ?>">
	<?php
	$dataProviderT = new CArrayDataProvider(
		$themesTitres,
		['keyField' => false, 'pagination' => false, ]
	);
	$this->widget(
		'ext.bootstrap.widgets.BootGridView',
		[
			'id' => 'categorietitre-grid',
			'dataProvider' => $dataProviderT,
			'filter' => null,
			'ajaxUpdate' => false,
			'summaryText' => '',
			'columns' => [
				'categorie:text:Thème',
				'role:text:Rôle',
				'titre:text:Titre',
				'vocabulaire:text:Vocabulaire',
				[
					'name' => 'alias',
					'header' => 'Mot-clé de ce vocabulaire',
					'htmlOptions' => ['class' => 'alias'],
				],
			],
			'rowCssClassExpression' => function ($row, $data) use ($categoriesPubliques) {
				if (isset($categoriesPubliques[$data['id']])) {
					return "publique";
				}
				return "privee";
			},
		]
	);
	?>
	</div>
	<?php
}
?>

<h2>Modifier les thèmes de cette revue</h2>

<?php
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'revue-categories',
		'enableAjaxValidation' => false,
		'type' => BootActiveForm::TYPE_HORIZONTAL,
	]
);
/** @var BootActiveForm $form */
Yii::app()->clientScript->registerScriptFile(
	Yii::app()->assetManager->publish(__DIR__ . '/js/categories.js')
);
echo CHtml::hiddenField('revueId', (string) $formData->revue->id);
?>

<div class="alert">
	<?php
	if ($directAccess) {
		if ($formData->hasExternalTracker()) {
			echo $form->checkBoxRow(
				$formData,
				'forceProposition',
				['label' => "Passer par l'état en attente au lieu d'une modification directe en tant qu'admin", 'name' => 'forceProposition']
			);
			echo "La proposition sera traitée par le partenaire suivant cette revue.";
		} else {
			echo "Les modifications seront appliquées immédiatement.";
			$formData->forceProposition = 0;
			echo $form->hiddenField($formData, 'forceProposition');
		}
	} else {
		echo "Les modifications que vous proposez devront être validées "
			, "par un partenaire de Mi@bel suivant cette revue avant d'être appliquées.";
	}
	?>
</div>

<div class="alert alert-info">
	<div>
		Pour une revue, nous conseillons de mettre au maximum 3 thèmes de <em>Disciplines</em>
		et au maximum 5 thèmes au total.
	</div>
	Vous pouvez :
	<ul>
		<li>ajouter des thèmes en les sélectionnant, soit par auto-complétion, soit par clic dans l'arbre ;</li>
		<li>retirer des thèmes déjà affectés ;</li>
	</ul>
	<p>
		Ensuite, le bouton placé sous la liste des opérations permet d'enregistrer ces changements.
	</p>
	<p>
		En cas de besoin contacter le groupe de travail de Mir@bel sur les thématiques :
		<a href="mailto:mirabel_indexation@listes.sciencespo-lyon.fr">mirabel_indexation@listes.sciencespo-lyon.fr</a>
	</p>
</div>

<div class="form-actions">
	<strong>Opérations sur la revue <?= CHtml::encode($fullTitle) ?></strong>
	<ul id="rc-modifications">
		<li class="nop">Aucune opération à réaliser</li>
	</ul>
	<?php
	$this->widget(
		'bootstrap.widgets.BootButton',
		[
			'buttonType' => 'submit',
			'type' => 'primary',
			'label' => "Enregistrer ces " . ($directAccess ? "modifications" : "propositions"),
		]
	);
	?>
</div>

<h3>Ajouter par autocomplétion</h3>
<div class="control-group">
	<label class="control-label" for="CategorieRevue_categorieId">
		 <br />
		Thème
	</label>
	<div class="controls">
		<input class="span6" name="CategorieRevue[categorieId]" id="CategorieRevue_categorieId" type="hidden" />
		<input class="span6" id="CategorieRevue_categorieIdComplete" type="text" name="CategorieRevue[categorieIdComplete]"
			data-role="<?= (Yii::app()->user->checkAccess('indexation') ? '' : 'public') ?>" />
		<div class="span6">
			<div id="categorie-fullname"> </div>
			<div id="categorie-description"></div>
		</div>
	</div>
</div>
<?php
$cs = Yii::app()->getClientScript();
$cs->registerCoreScript('jquery.ui');
$cs->registerCssFile($cs->getCoreScriptUrl() . '/jui/css/base/jquery-ui.css');
?>

<?php
$this->endWidget(); // form
?>

<h3 style="clear: left;">Ajouter par choix dans l'arbre</h3>
<?php
$jstreeConfig = new \widgets\jstree\JsTreeCore();
$jstreeConfig->data = [
	'url' => $this->createUrl(
		'/categorie/ajaxNode',
		['extra' => 1, 'role' => (Yii::app()->user->checkAccess('indexation') ? '' : 'public')]
	),
];
$jstreeConfig->translateToFr();
$jstree = new widgets\jstree\JsTree($jstreeConfig, ["search", "sort", "types"]);
$jstree->types = [
	'root' => [
		'icon' => '/images/icons/glyphicons-318-tree-deciduous.png',
	],
	'candidat' => [
		'icon' => '/images/icons/glyphicons-195-circle-question-mark.png',
	],
];
$jstree->init(".jstree");
?>
<div class="row-fluid">
	<div id="categorie-tree" class="jstree span6"></div>
	<div id="categorie-description" class="span6"></div>
</div>
