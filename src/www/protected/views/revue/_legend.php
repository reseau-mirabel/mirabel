<div class="legende" id="revue-legend">
	<h3>Légende</h3>
	<ul>
		<?php
		if (Yii::app()->user->checkAccess("avec-partenaire")) {
			?>
		<li class="titre suivi-self">Suivi par son propre partenaire</li>
		<li class="titre suivi-other">Suivi par un autre partenaire</li>
		<?php
		} else {
			?>
		<li class="titre suivi-other">Suivi par un partenaire</li>
		<?php
		}
		?>
	</ul>
</div>
<div style="clear:left;"></div>
