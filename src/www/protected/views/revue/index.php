<?php

/** @var Controller $this */
/** @var string $lettre */
/** @var \components\paginationalpha\ListViewAlpha $listView */
/** @var string $searchHash */
/** @var int $totalRevues */

use components\HtmlHelper;

assert($this instanceof Controller);

$this->pageTitle = "Revues - $lettre";
$this->breadcrumbs = [
	'Revues',
];
\Yii::app()->getComponent('sidebar')->menu = [
	['label' => 'Créer une revue', 'url' => ['/titre/create-by-issn', 'revueId' => 0]],
];

$this->renderPartial('_legend');
?>

<h1><?= CHtml::encode($this->pageTitle) ?></h1>

<figure class="link-to-map">
	<?=
	CHtml::link(
		CHtml::image('/public/carte_revues_vignette.png', "Carte des $totalRevues revues de Mir@bel"),
		['/revue/carte']
	) ?>
	<figcaption>Carte des <?= $totalRevues ?> revues</figcaption>
</figure>
<div>
	<?= CHtml::link("Recherche avancée de revues", ['/revue/search']) ?>
</div>

<div id="before-list-results">
	<?php
	$listView->renderLetterSelector();
	$listView->renderPager();
	?>
</div>

<div>
	<?php
	if (!\Yii::app()->user->isGuest) {
		echo CHtml::link(
			CHtml::image(Yii::app()->baseUrl . "/images/plus.png", "Nouvelle revue"),
			['/titre/create-by-issn', 'revueId' => 0],
			[
				'style' => 'float: right; margin-right: 40px;',
				'title' => 'Créer une nouvelle revue',
			]
		);
	}
	?>
</div>

<div class="list-results">
	<?php
	foreach ($listView->dataProvider->getData() as $result) {
		echo HtmlHelper::titleLinkItem($result);
	}
	?>
</div>

<div id="after-list-results">
	<?php
	$listView->renderLetterSelector();
	$listView->renderPager();
	?>
</div>
<?php
$maxPageRank = $listView->dataProvider->getPagination()->getPageCount();
(new \widgets\SearchNavigationInit('revue', $listView->dataProvider->getData(), $maxPageRank))->run();
