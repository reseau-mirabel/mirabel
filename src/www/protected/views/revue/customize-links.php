<?php

/** @var Controller $this */
/** @var \models\forms\CustomizeLinksForm $formData */
/** @var bool $ajax */

$this->pageTitle = "Personnaliser les liens";

$this->breadcrumbs = [
	'Revues' => ['index'],
	$this->pageTitle,
];

?>
<?php if ($ajax) { ?>
<div style="display: flex; flex-direction: row; justify-content: space-between;">
	<h3><?= $this->pageTitle ?></h3>
	<button type="button" class="close" data-dismiss="modal">×</button>
</div>
<?php } else { ?>
	<h1><?= $this->pageTitle ?></h1>
<?php } ?>

<?php
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'customize-links-form',
		'enableAjaxValidation' => false,
		'htmlOptions' => ['class' => 'well'],
		'type' => BootActiveForm::TYPE_VERTICAL,
	]
);
/** @var BootActiveForm $form */

echo $form->errorSummary($formData);

echo $form->checkBoxListRow($formData, 'selection', $formData->listCandidates());
?>
<div>
	<button type="button" class="btn btn-small" onclick="for (let i of this.closest('form').querySelectorAll('input:checked')) { i.checked = false; }">
		Aucun
	</button>
	<button type="button" class="btn btn-small" onclick="for (let i of this.closest('form').querySelectorAll('input[type=checkbox]')) { i.checked = true; }">
		Tous
	</button>
</div>

<div class="form-actions">
	<button type="submit" class="btn btn-primary">Enregistrer</button>
</div>
<?php
$this->endWidget();
