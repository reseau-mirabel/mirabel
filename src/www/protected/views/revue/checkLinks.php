<?php

/** @var Controller $this */
/** @var Revue $revue */
/** @var array $checks */
/** @var bool $hasWikidata */

use components\HtmlHelper;

assert($this instanceof Controller);

$fullTitle = $revue->getFullTitle();
$this->pageTitle = $fullTitle;

$this->breadcrumbs = [
	'Revues' => ['index'],
	$fullTitle => ['view', 'id' => $revue->id],
	'Vérification des liens',
];

\Yii::app()->getComponent('sidebar')->menu = [
	['label' => 'Liste des revues', 'url' => ['index']],
	['label' => 'Cette revue', 'url' => ['view', 'id' => $revue->id]],
];

$checks['urls'][''] = true;
$buildTr = HtmlHelper::builderCheckLinks($checks['urls']);
?>

<h1>
	Vérification des liens <?= ($hasWikidata ? ' et données Wikidata' : '') ?>
	pour <i><?= CHtml::encode($fullTitle) ?></i>
</h1>

<h2>Titres</h2>
<table class="table table-bordered table-condensed">
	<thead>
		<tr>
			<th>Titre</th>
			<th>URL</th>
			<th>Résultat</th>
		</tr>
	</thead>
	<tbody>
		<?php
		/** @var Titre $titre */
		foreach ($checks['checked']['Titres'] as $titre) {
			$link = CHtml::link($titre->getFullTitle(), ['titre/update', 'id' => $titre->id]);
			echo $buildTr($titre->url, $link);
		}
		?>
	</tbody>
</table>

<?php
$otherLinks = '';
foreach ($checks['checked']['Titres'] as $titre) {
	/** @var Titre $titre */
	if ($titre->liensJson) {
		foreach ($titre->getLiens() as $lien) {
			/** @var Lien $lien */
			$link = CHtml::link($titre->getFullTitle() . ' — ' . $lien->src, ['titre/update', 'id' => $titre->id]);
			$otherLinks .= $buildTr($lien->url, $link);
		}
	}
}
if ($otherLinks) {
	?>
<h2>Titres — Autres liens</h2>
<table class="table table-bordered table-condensed">
	<thead>
		<tr>
			<th>Titre</th>
			<th>URL</th>
			<th>Résultat</th>
		</tr>
	</thead>
	<tbody>
		<?= $otherLinks; ?>
	</tbody>
</table>
<?php
}
?>

<h2>Accès en ligne</h2>
<table class="table table-bordered table-condensed">
	<thead>
		<tr>
			<th>Accès</th>
			<th>URL</th>
			<th>Résultat</th>
		</tr>
	</thead>
	<tbody>
		<?php
		/** @var Service $service */
		foreach ($checks['checked']['Accès en ligne'] as $service) {
			$link = CHtml::link(
				"{$service->type} / {$service->acces}",
				['service/update', 'id' => $service->id]
			);
			echo $buildTr($service->url, $link);
			foreach (['alerteRssUrl', 'alerteMailUrl', 'derNumUrl'] as $field) {
				if ($service->{$field}) {
					echo $buildTr($service->{$field}, "");
				}
			}
		}
		?>
	</tbody>
</table>

<?php
if ($hasWikidata && Yii::app()->db->schema->getTable('Wikidata')) {
?>
<section id="revue-wikidatabox">
	<?= $this->renderPartial('_wikidatabox', ['revue' => $revue]) ?>
</section>
<?php
}
?>
