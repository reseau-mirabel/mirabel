<?php

/** @var Controller $this */
/** @var processes\revue\View $process */
/** @var array $titres (titreId => Titre) */
/** @var array $services (Service) */
/** @var array $partenaires (Partenaire) */
/** @var SearchNavigation $searchNavigation */

assert($this instanceof RevueController);

Yii::app()->getClientScript()->registerCssFile('/css/glyphicons.css');

// Pour afficher revue/view, il faut interroger 12 tables !

$revue = $process->revue;

$activeTitleDec = new TitreDecorator($process->getActiveTitle());

$imgUrl = Yii::app()->getAssetManager()
	->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview';

if ($process->getTitles()) {
	$currentTitle = $process->getActiveTitle()->getFullTitle();
} else {
	throw new Exception("La revue d'ID {$revue->id} n'a aucun titre");
}
$this->pageTitle = $currentTitle;
$this->pageDescription = 'Accès en ligne aux contenus de la revue ' . $currentTitle;


$this->breadcrumbs = [
	'Revues' => ['index'],
	$process->getActiveTitle()->getPrefixedTitle(),
];

$this->renderPartial('_view_meta', ['process' => $process, 'activeTitleDec' => $activeTitleDec]);

$sidebar = \Yii::app()->getComponent('sidebar');
assert($sidebar instanceof \components\WebSidebar);
if (Yii::app()->user->checkAccess("avec-partenaire")) {
	$sidebar->menu = $process->getSidebarMenu(
		Yii::app()->user->isMonitoring($revue),
		Yii::app()->user->checkAccess('verify', $revue)
	);

	if (Yii::app()->user->checkAccess('possession/update')) {
		$owned = Yii::app()->db->createCommand("SELECT 1 FROM Partenaire_Titre WHERE partenaireId = :pid AND titreId = :tid")
			->queryScalar([':tid' => $process->getActiveTitle()->id, ':pid' => Yii::app()->user->partenaireId]);
		if ($owned) {
			$addToPoss = "Le titre actif est dans mes possessions";
		} else {
			$addToPoss = CHtml::form(['/partenaire/possession', 'id' => Yii::app()->user->partenaireId])
				. CHtml::hiddenField('Possession[titreId]', (string) $process->getActiveTitle()->id)
				. CHtml::hiddenField('Possession[nosave]', "1")
				. CHtml::hiddenField('returnUrl', $this->createUrl('view', ['id' => $revue->id]))
				. CHtml::htmlButton('Ajouter à mes possessions', ['type' => 'submit', 'class' => "btn btn-small"])
				. CHtml::endForm();
		}
		array_push(
			$sidebar->menu,
			['label' => 'Mes possessions', 'itemOptions' => ['class' => 'nav-header']],
			['label' => $addToPoss, 'encodeLabel' => false, 'title' => "Ajouter le dernier titre de cette revue à mes possessions en tant que Partenaire Mir@bel"]
		);
	}
}

/** @var Service $service */
/** @var Partenaire $partenaire */

$this->renderPartial('/global/_interventionLocal', ['model' => $revue]);
?>

<?= (new \widgets\SearchNavigationDisplay())->run(); ?>

<h1>
	<?= CHtml::encode($process->getActiveTitle()->getFullTitle()); ?>
	<?= $this->renderPartial('/titre/_logos-attributs', ['titre' => $process->getActiveTitle()]) ?>
</h1>

<?php
$display = new DisplayServices($services, Yii::app()->user->getState('instituteId'));
$display->setDisplayAll($process->isPublic());

$strip = new \widgets\SvgTimeAccess(Yii::app()->user->getInstitute(), true);
echo $strip->run($revue->id, $display);
?>

<?php
$categories = $revue->getThemes();
if ($categories) {
	?>
<section id="revue-categories">
	<table class="detail-view table table-bordered table-condensed">
		<tbody>
			<tr>
				<th title="Thématique attribuée par le réseau Mir@bel à cette revue">Thématique</th>
				<td class="categories-labels">
					<?php
					foreach ($categories as $c) {
						/** @var Categorie $c */
						if ($c->role === "public" || Yii::app()->user->checkAccess('indexation')) {
							$class = ($c->role === "public" ? '' : ' class="candidate"');
							$catTitle = CHtml::link(CHtml::encode($c->categorie), $c->getSelfUrl());
							if ($c->description) {
								echo '<abbr title="' . CHtml::encode($c->description) . '"' . $class . '>' . $catTitle . "</abbr>  ";
							} else {
								echo "<span$class>" . $catTitle . "</span>  ";
							}
						}
					} ?>
				</td>
			</tr>
		</tbody>
	</table>
</section>
<?php
}
?>

<section id="revue-titres">
	<?php
	$this->renderPartial('_titres', ['revue' => $revue, 'titres' => $titres, 'direct' => $process->directUpdate, 'activeTitle' => $process->getActiveTitle()]);
	?>
</section>

<section id="revue-titre-actif">
	<?php
	$this->renderPartial(
		'_titre-actif',
		[
			'monoTitle' => $process->isMonoTitle(),
			'activeTitle' => $process->getActiveTitle(),
			'activeTitleDec' => $activeTitleDec,
			'forceRefresh' => $process->forceRefresh,
			'isPopup' => false,
		]
	);
	?>
</section>

<section id="revue-acces">
	<div class="actions">
		<?php
		$canAdd = true;
		if ($process->isPublic() || $display->hasMerged()) {
			if ($process->isPublic()) {
				echo CHtml::link(
					"Regrouper les accès",
					['view', 'id' => $revue->id, 'public' => 0],
					[
						'title' => "Fusionner les accès similaires"
							. (Yii::app()->user->getState('instituteId') > 0 ? "" : " en fonction de mon établissement (abonnements)"),
						'class' => 'btn',
					]
				);
			} else {
				$canAdd = false;
				echo CHtml::link(
					"Afficher le détail",
					['view', 'id' => $revue->id, 'public' => 1],
					['title' => "Afficher chaque accès séparément, en séparant par collection", 'class' => 'btn']
				);
			}
		}
		if ($canAdd) {
			echo CHtml::link(
				CHtml::image(Yii::app()->baseUrl . "/images/plus.png", "Ajout d'un accès"),
				['service/create', 'revueId' => $revue->id],
				['title' => $process->directUpdate ? 'Nouvel accès' : 'Proposer un nouvel accès']
			);
		}
		?>
	</div>
	<h2>Accès en ligne</h2>
	<?php
	$this->renderPartial(
		'/service/_summaryTable',
		[
			'display' => $display,
			'directAccess' => $process->directUpdate,
			'partenaireId' => Yii::app()->user->getState('instituteId'),
			'publicView' => $process->isPublic(),
			'publicViewUrl' => ['view', 'id' => $revue->id, 'public' => 1],
		]
	);
	?>
</section>

<section id="revue-suivi">
	<h2>Suivi</h2>
	<?php
	if ($partenaires) {
		echo '<ul class="objets-suivis">';
		foreach ($partenaires as $partenaire) {
			echo '<li>' . $partenaire->getSelfLink() . ' suit cette revue dans Mir@bel</li>';
		}
		echo "</ul>\n";
	} else {
		$collectionsImp = $revue->getCollectionsImport();
		if ($collectionsImp && $revue->hasRessourceImport()) {
			$ressources = $revue->getRessourcesNamesWithImport();
			echo "<p>Cette revue n'est pas suivie par un partenaire de Mir@bel "
				. "mais les informations d'accès en ligne fournies par "
				. (count($collectionsImp) > 1 ? "les collections " : "la collection ")
				. join(
					', ',
					array_map(
						function ($x) {
							return CHtml::tag('em', [], CHtml::encode($x));
						},
						$collectionsImp
					)
				)
				. " et "
				. (count($ressources) > 1 ? "les ressources " : "la ressource ")
				. join(
					', ',
					array_map(
						function ($x) {
							return CHtml::tag('em', [], CHtml::encode($x));
						},
						$ressources
					)
				)
				. " sont traitées automatiquement chaque jour. ";
		} elseif ($collectionsImp) {
			echo "<p>Cette revue n'est pas suivie par un partenaire de Mir@bel "
				. "mais les informations d'accès en ligne fournies par "
				. (count($collectionsImp) > 1 ? "les collections " : "la collection ")
				. join(
					', ',
					array_map(
						function ($x) {
							return CHtml::tag('em', [], CHtml::encode($x));
						},
						$collectionsImp
					)
				)
				. " sont traitées automatiquement chaque jour. ";
		} elseif ($revue->hasRessourceImport()) {
			$ressources = $revue->getRessourcesNamesWithImport();
			echo "<p>Cette revue n'est pas suivie par un partenaire de Mir@bel "
				. "mais les informations d'accès en ligne fournies par "
				. (count($ressources) > 1 ? "les ressources " : "la ressource ")
				. join(
					', ',
					array_map(
						function ($x) {
							return CHtml::tag('em', [], CHtml::encode($x));
						},
						$ressources
					)
				)
				. " sont traitées automatiquement chaque jour. ";
		} else {
			echo "<p>Cette revue est répertoriée par Mir@bel mais n'est pas encore suivie par un partenaire. "
				. "La mise à jour des informations n'est pas assurée. ";
		}
		echo "Les icônes " . CHtml::image(Yii::app()->baseUrl . "/images/plus.png", "Ajouter")
			. (!Yii::app()->user->checkAccess("avec-partenaire") && $revue->hasRessourceImport() ? ', <i class="icon-envelope"></i>' : '')
			. " et " . CHtml::image($imgUrl . '/update.png', 'Modifier')
			. " vous permettent de proposer des modifications.</p>";
	}

	$this->renderPartial('/global/_modif-verif', ['target' => $revue]);
	?>
</section>
