<?php

/** @var Controller $this */
/** @var Revue $model */
/** @var Titre[] $titre */
/** @var int[] $duplicate */
/** @var int[] $titreIdWithDateProblem */
/** @var int[] $notInRevue */
/** @var bool $hasDateProblem */
/** @var bool $hasObsoleteParProblem */

assert($this instanceof Controller);
Yii::app()->clientScript->registerCoreScript('jquery.ui');
$this->pageTitle = "Trier les titres";
$this->breadcrumbs = [
	'Revues' => ['index'],
	$model->getFullTitle() => ['revue/view', 'id' => $model->id],
	'Trier les titres de la revue',
];

if (!empty($duplicate)) {
	echo '<div class="alert alert-warning">';
	foreach ($duplicate as $id) {
		echo "<strong>Attention :</strong> Le titre $id rend obsolète plusieurs titres.<br>";
	}
	echo "</div>";
}
if (!empty($notInRevue)) {
	echo '<div class="alert alert-warning">';
	foreach ($notInRevue as $id) {
		echo "<strong>Attention :</strong> Le titre $id n'appartient pas à la revue mais rend un de ses titres obsolètes.<br>";
	}
	echo "</div>";
}
if ($hasDateProblem) {
	echo <<<HTML
		<div class="alert alert-warning">
			<strong>Attention :</strong>
			Il semble y avoir des incohérences de date entre l'ordre d'obsolescence et les périodes de publication des différents titres.
		</div>
		HTML;
}
?>

<h1>Trier les titres de la revue</h1>
<p class="alert alert-info">
	Veuillez réorganiser les éléments par glisser-déposer.
	<strong>Après enregistrement, chaque titre de la liste aura pour successeur le titre placé au-dessus de lui.</strong>
</p>
<p>
	La liste est initialement triée selon l'ordre d'affichage sur la page d'une revue.
	Les valeurs <em>Obsolète Par</em> (doublon ou hors-revue) et les dates de publication incohérentes sont signalées en <strong class="text-error">gras</strong>.
</p>

<?php
$tableContent = [];
$revueIssnGroups = $model->getIssnGroups();
foreach ($model->getTitresOrderedByObsolete() as $titre) {
	$row = (object) [
		'id' => "",
		'obsoletePar' => "",
		'titre' => "",
		'issn' => "",
		'issne' => "",
		'local' => "",
		'dates' => "",
	];
	$row->id = $titre->id;

	$obsoletePar = $titre->obsoletePar;
	if (in_array($obsoletePar, $duplicate) || in_array($obsoletePar, $notInRevue)) {
		$row->obsoletePar = '<strong class="text-error">' . $titre->obsoletePar . '</strong>';
	} else {
		$row->obsoletePar = $titre->obsoletePar;
	}

	$row->titre = '<span itemprop="name">' . CHtml::link($titre->titre, ['/titre/view', 'id' => $titre->id], ['title'=>'Détails de ce titre']) . '</span>';

	if ($revueIssnGroups['issnp']) {
		if (isset($revueIssnGroups['issnp'][$titre->id])) {
			foreach ($revueIssnGroups['issnp'][$titre->id] as $issn) {
				/** @var Issn $issn */
				$printableIssn = $issn->issn ? $issn->getHtml(true) : "";
				$row->issn .= CHtml::tag(
					'div',
					[],
					($issn->sudocPpn ? $issn->getSudocLink() . " " : "") . $printableIssn
				);
			}
		}
	}

	if ($revueIssnGroups['issne']) {
		if (isset($revueIssnGroups['issne'][$titre->id])) {
			foreach ($revueIssnGroups['issne'][$titre->id] as $issn) {
				/** @var Issn $issna */
				$printableIssn = $issn->issn ? $issn->getHtml(true) : "";
				$row->issne .= CHtml::tag(
					'div',
					[],
					($issn->sudocPpn ? $issn->getSudocLink() . " " : "") . $printableIssn
				);
			}
		}
	}
	$periode = $titre->getPeriode();
	if ($periode === "") {
		$periode = '… – …';
	}
	if (in_array($titre->id, $titreIdWithDateProblem)) {
		$row->dates = '<strong class="text-error">' . $periode . '</strong>';
	} else {
		$row->dates = $periode;
	}

	$tableContent[] = $row;
}
?>
<form method="post" id="reorder">
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th></th>
				<th>Titre</th>
				<?= $revueIssnGroups['issnp'] ? '<th title="International Standard Serial Number">ISSN</th>' : "" ?>
				<?= $revueIssnGroups['issne'] ? '<th title="International Standard Serial Number - version électronique">ISSN-E</th>' : "" ?>
				<th>Années</th>
				<th>Id</th>
				<th>obsolète par</th>

			</tr>
		</thead>
		<tbody id="sortable-rows">
			<?php foreach ($tableContent as $row) { ?>
				<tr itemscope itemtype="http://schema.org/Periodical" data-id="<?= $row->id ?>">
					<td>
						<span class="glyphicon glyphicon-menu-hamburger"></span>
						<input type="hidden" name="order[]" value="<?= $row->id ?>">
					</td>
					<td><?= $row->titre ?></td>
					<?php
					if ($revueIssnGroups['issnp']) {
						echo CHtml::tag('td', ['class' => 'issn'], $row->issn);
					}
					if ($revueIssnGroups['issne']) {
						echo CHtml::tag('td', ['class' => 'issn'], $row->issne);
					}
					?>
					<td><?= $row->dates ?></td>
					<td><?= $row->id ?></td>
					<td><?= $row->obsoletePar ?></td>
				</tr>
			<?php
			}
			?>
		</tbody>
	</table>
	<div class="btn-container">
		<?php if ($hasObsoleteParProblem || $hasDateProblem): ?>
			<button type="submit" id="saveButton" class="btn btn-primary">Enregistrer cet ordre de titres</button>
		<?php else: ?>
			<a href="<?= $this->createUrl('revue/view', ['id' => $model->id]) ?>" id="cancelButton" class="btn btn-default">Conserver cet ordre de titres</a>
			<button type="submit" id="saveButton" class="btn btn-primary" style="display: none;">Enregistrer cet ordre de titres</button>
		<?php endif; ?>
	</div></form>

<?php
$alwaysShowFormSubmitButtonJs = json_encode($hasObsoleteParProblem || $hasDateProblem);
Yii::app()->clientScript->registerScript(
	'sortable',
	<<<JS
		$(function() {
			let initialOrder;
			const alwaysShowFormSubmitButton = {$alwaysShowFormSubmitButtonJs}; // Toujours proposer de valider l'ordre si problème d'obsoletePar
			$("#sortable-rows").sortable({
				items: "tr",
				axis: "y",
				create: function(event, ui) {
					initialOrder = $(this).sortable("toArray", {attribute: "data-id"});
				},
				update: function(event, ui) {
					if (!alwaysShowFormSubmitButton){
						let currentOrder = $("#sortable-rows").sortable("toArray", {attribute: "data-id"});
						let orderChanged = JSON.stringify(initialOrder) !== JSON.stringify(currentOrder);
						$("#saveButton").toggle(orderChanged);
						$("#cancelButton").toggle(!orderChanged);
					}
				}
			}).disableSelection();
		});
		JS,
);
?>
