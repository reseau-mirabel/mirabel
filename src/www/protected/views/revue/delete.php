<?php

/** @var Controller $this */
/** @var Revue $model */

assert($this instanceof Controller);
$titre = $model->activeTitle;
$domain = "reseau-mirabel.info";

$this->pageTitle = 'Supprimer la revue ' . $titre->titre;
$this->breadcrumbs = [
	'Revues' => ['/revue/index'],
	$titre->getFullTitle() => ['/revue/view', 'id' => $model->id],
	"Supprimer",
];
?>

<h1><?= $this->pageTitle ?></h1>

<div class="alert alert-warning">
	<p>
		Vous vous apprêtez à supprimer une revue dans Mir@bel.
		Cette opération est <strong>possible</strong> mais reste <strong>rare et exceptionnelle</strong>
		en raison des alignements de données avec d'autres systèmes d'information.
	</p>
</div>

<?= CHtml::beginForm('', 'post', ['id' => 'confirm-delete']) ?>
<?= CHtml::hiddenField('confirm', '1') ?>
<label class="annoying">Écrivez « Je confirme » dans le champ ci-dessous</label>
<?= CHtml::textField('annoying', '', ['placeholder' => "Je confirme", 'size' => '20', 'class' => 'annoying']) ?>
<div class="form-actions">
	<button class="btn btn-danger" type="submit">
		Supprimer définitivement cette revue
	</button>
	<?= CHtml::link("Annuler et revenir à la page de la revue", $model->getSelfUrl(), ['class' => 'btn']) ?>
</div>
<?= CHtml::endForm() ?>

<?php
Yii::app()->getClientScript()->registerScript('confirm-delete', <<<EOJS
	$('#confirm-delete').on('submit', function(e) {
		if ($('input.annoying').val() !== "Je confirme") {
			e.preventDefault();
			$("label.annoying").css('font-weight', 'bold');
			return false;
		}
		return true;
	});
	EOJS
);
