<?php

/** @var Controller $this */
/** @var bool $monoTitle */
/** @var ?Titre $activeTitle */
/** @var ?TitreDecorator $activeTitleDec */
/** @var bool $forceRefresh */
/** @var bool $isPopup */

assert($this instanceof Controller);

if ($activeTitle === null) {
	echo "<div>Titre non trouvé</div>";
	return;
}

if (empty($activeTitleDec)) {
	$activeTitleDec = new TitreDecorator($activeTitle);
}

$attributes = [
	[
		'label' => $isPopup ? "Titre" : "Titre actuel",
		'value' => $activeTitle->getPrefixedTitle(),
		'visible' => !$monoTitle,
	],
	[
		'label' => 'Site web',
		'type' => 'url',
		'value' => $activeTitle->url,
		'visible' => ($activeTitle->url != ''),
	],
	'periodicite',
	[
		'name' => 'langues',
		'value' => \models\lang\Convert::codesToFullNames(json_decode($activeTitle->langues)),
	],
	[
		'name' => 'electronique',
		'type' => 'boolean',
		'visible' => (bool) $activeTitle->electronique,
	],
];

$editeursActifs = \Editeur::model()->findAllBySql(<<<EOSQL
	SELECT e.*
	FROM Editeur e
	  JOIN Titre_Editeur te ON te.editeurId = e.id AND IFNULL(te.ancien, 0) = 0
	WHERE te.titreId = :tid
	ORDER BY e.dateDebut DESC, e.nom ASC
	EOSQL,
	[':tid' => $activeTitle->id]
);
if ($editeursActifs) {
	$attributes[] = [
		'label' => count($editeursActifs) > 1 ? "Éditeurs" : "Éditeur",
		'type' => 'raw',
		'value' => join(' ', array_map(function (\Editeur $x) {return '<div>' . $x->getSelfLink(\Editeur::NAME_DATES) . '</div>'; }, $editeursActifs)),
	];
}

$editeursAnciens = \Editeur::model()->findAllBySql(<<<EOSQL
	SELECT e.*
	FROM Editeur e
	  JOIN Titre_Editeur te ON te.editeurId = e.id AND te.ancien = 1
	WHERE te.titreId = :tid
	ORDER BY e.dateDebut DESC, e.nom ASC
	EOSQL,
	[':tid' => $activeTitle->id]
);
if ($editeursAnciens) {
	$attributes[] = [
		'label' => count($editeursAnciens) > 1 ? "Éditeurs précédents" : "Éditeur précédent",
		'type' => 'raw',
		'value' => join(' ', array_map(function ($x) {return '<div>' . $x->getSelfLink(\Editeur::NAME_DATES) . '</div>'; }, $editeursAnciens)),
	];
}

$countries = \Yii::app()->db
	->createCommand("SELECT p.nom FROM Issn i JOIN Pays p ON p.code2 = i.pays WHERE i.titreId = {$activeTitle->id} GROUP BY p.id ORDER BY p.nom")
	->queryColumn();
if ($countries) {
	$attributes[] = [
		'label' => "Pays de publication",
		'value' => join(' ', $countries),
	];
}

$attributes[] = [
	'label' => "Politique de publication",
	'type' => 'raw',
	'value' => $activeTitle->getLinksEditorial(),
];

$attributeTexts = \processes\attribut\DisplayedAttributes::getTexts($activeTitle);
if ($attributeTexts) {
	$attributes[] = [
		'name' => "Labellisation",
		'type' => 'raw',
		'value' => CHtml::tag('ul', ['class' => 'labellisation'], join("", array_map(
			function (string $x) { return "<li>$x</li>"; },
			$attributeTexts
		))),
	];
}

$titleAttr = \processes\attribut\DisplayedAttributes::getValues($activeTitle);
if (isset($titleAttr['apc'])) {
	$attributes[] = [
		'label' => "Frais de publication",
		'type' => 'raw',
		'value' => ($titleAttr['apc']
			? 'Avec frais pour l’auteur (<abbr title="Article Processing Charge">APC</abbr>)'
			: "Sans frais pour l’auteur (modèle diamant)"),
	];
}

$attributes[] = 'linksOther:raw:Autres liens';
if ($activeTitle->getLiensInternes()->getContent()) {
	$attributes[] = 'liensInternes:raw';
}

?>
<div class="active-title <?= ($activeTitleDec->hasFront() ? "with-front" : "no-front") ?>">
	<div class="title-front">
		<?= $activeTitleDec->displayFront($forceRefresh); ?>
	</div>
	<div class="title-attributes">
		<?php
		Yii::app()->getClientScript()->registerScript(
			'popover',
			<<<EOJS
				$(".popover-toggle").popover();
			EOJS
		);
		$this->widget(
			'ext.bootstrap.widgets.BootDetailView',
			[
				'data' => $activeTitle,
				'attributes' => $attributes,
				'hideEmptyLines' => true,
			]
		);
		?>
	</div>
</div>
