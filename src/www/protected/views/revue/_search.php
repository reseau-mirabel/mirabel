<?php

/** @var Controller $this */
/** @var SearchTitre $model */
/** @var int $partenaireId */

use components\HtmlHelper;
use components\SqlHelper;

assert($this instanceof Controller);

$isAuthUser = Yii::app()->user->checkAccess("avec-partenaire");

/** @var BootActiveForm $form */
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'action' => Yii::app()->createUrl('/revue/search'),
		'method' => 'get',
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'hints' => Hint::model()->getMessages('SearchTitre'),
	]
);

echo $form->errorSummary($model);

echo $form->textFieldRow($model, 'titre', ['class' => 'span8']);
echo $form->checkBoxRow($model, 'vivant', ['uncheckValue' => '']);
echo $form->textFieldRow($model, 'issn');
$accessTypes = [2 => 'Texte intégral', 3 => 'Résumé', 4 => 'Sommaire', 5 => 'Indexation'];
echo $form->checkBoxListInlineRow($model, 'acces', $accessTypes, ['uncheckValue' => null]);
echo $form->checkBoxRow($model, 'accesLibre', ['uncheckValue' => '']);
if ($isAuthUser) {
	echo $form->checkBoxRow($model, 'sansAcces', ['uncheckValue' => '', 'class' => 'not-guest']);
}

$this->widget(
	'ext.MultiAutoComplete.MultiAutoComplete',
	[
		'model' => $model,
		'attribute' => 'editeurId',
		'foreignModelName' => 'Editeur',
		'foreignAttribute' => 'fullName',
		'ajaxUrl' => $this->createUrl('/editeur/ajax-complete'),
		'options' => [
			// min letters typed before we try to complete
			'minLength' => '3',
		],
		'htmlOptions' => ['hint' => $form->getHint($model, 'editeurId')],
	]
);

if ($model->aediteurId) {
	$aediteurIds = (is_array($model->aediteurId) ? join(",", $model->aediteurId) : $model->aediteurId);
	$allPublishers = SqlHelper::sqlToPairs("SELECT id, nom FROM Editeur WHERE id IN ($aediteurIds)");
	echo $form->checkBoxListRow($model, 'aediteurId', $allPublishers, ['class' => 'block', 'uncheckValue' => '', 'checked' => true]);
}

$pays = SqlHelper::sqlToPairs("SELECT p.id, p.nom FROM Pays p JOIN Editeur e ON e.paysId = p.id GROUP BY p.id ORDER BY p.code2 = 'ZZ' DESC, p.nom");
echo $form->dropDownListRow(
	$model,
	'paysId',
	$pays,
	['empty' => '-']
);
echo $form->dropDownListRow(
	$model,
	'issnpaysid',
	$pays,
	['empty' => '-']
);

$this->widget(
	'ext.MultiAutoComplete.MultiAutoComplete',
	[
		'model' => $model,
		'attribute' => 'ressourceId',
		'foreignModelName' => 'Ressource',
		'foreignAttribute' => 'fullName',
		'ajaxUrl' => $this->createUrl('/ressource/ajax-complete'),
		'options' => [
			// min letters typed before we try to complete
			'minLength' => '3',
		],
		'htmlOptions' => ['hint' => $form->getHint($model, 'ressourceId')],
	]
);

// Cannot use ->getFullname() because ordering Collection instances would be hard,
// so we recreate it in pure SQL.
$collections = SqlHelper::sqlToPairs(<<<EOSQL
	SELECT c.id, CONCAT(IF(r.sigle = '', r.nom, r.sigle), ' — ', c.nom)
	FROM Collection c
		JOIN Ressource r ON c.ressourceId = r.id
	WHERE c.visible > 0
	ORDER BY IF(r.sigle = '', r.nom, r.sigle) ASC, c.nom ASC
	EOSQL
);
echo $form->dropDownListRow(
	$model,
	'collectionId',
	$collections,
	['empty' => '-']
);
?>

<div class="control-group">
	<label class="control-label" for="SearchTitre_categorie">
		Thématique
	</label>
	<div class="controls">
		<div id="categorie-liste" class="categories-labels"
			title="Cliquer sur un terme pour le retirer de la recherche ; cliquer sur le R pour activer/désactiver la recherche récursive sur les sous-thèmes.">
			<?php
			if ($model->categorie) {
				foreach ($model->getCategories() as $categorie) {
					echo '<span class="categorie">' . CHtml::encode($categorie->categorie)
						. '<input name="SearchTitre[categorie][]" type="hidden" value="'
						. $categorie->id . '" />';
					if ($categorie->profondeur < 3) {
						$nonRec = in_array($categorie->id, $model->cNonRec);
						echo ' <i class="categorie-recursive' . ($nonRec ? " disabled" : "")
							. '" title="inclure les sous-thèmes">R'
							. ($nonRec ? '<input type="hidden" name="SearchTitre[cNonRec][]" value="' . $categorie->id . '" />' : '')
							. '</i>';
					}
					echo '</span> ';
				}
			}
			?>
		</div>
		<div id="categories-combine" class="<?= (count($model->categorie) > 1 ? '' : 'hidden') ?>">
			<label>
				<input name="SearchTitre[categoriesEt]" type="checkbox" value="1" <?= ($model->categoriesEt ? 'checked="checked"' : "") ?> />
				Revues ayant tous ces thèmes à la fois
			</label>
		</div>
		<input class="span6" id="SearchTitre_categorieComplete" type="text" name="SearchTitre[categorieComplete]"
			data-role="<?= (Yii::app()->user->checkAccess('indexation') ? '' : 'public') ?>" />
		<?= HtmlHelper::getPopoverHint($form->getHint($model, 'categorie') ?? '', "Thématique") ?>
	</div>
</div>
<?php
Yii::app()->clientScript->registerScript('categorie-complete', file_get_contents(__DIR__ . '/js/categories-autocomplete.js'));

if ($isAuthUser) {
	echo $form->checkBoxRow($model, 'sanscategorie', ['uncheckValue' => '', 'class' => 'not-guest']);
}
?>

<?php
echo $this->renderPartial(
	'/global/_autocompleteLang',
	['model' => $model, 'attribute' => 'langues', 'form' => $form]
);
?>

<?= (new \widgets\DynamicLinkFilter())->run($model->lien) ?>

<?php
if ($isAuthUser) {
	echo (new \widgets\DynamicAttributeFilter)->run($model->attribut);
}
?>

<?php if (!empty($partenaireId)) { ?>
<fieldset id="criteres-partenaire">
	<legend><?= CHtml::encode((string) Partenaire::model()->findByPk($partenaireId)?->nom) ?></legend>
	<?php
	echo $form->checkBoxRow($model, 'owned', ['uncheckValue' => '']);

	$hasAbo = Yii::app()->db->createCommand("SELECT 1 FROM Abonnement WHERE partenaireId = :pid LIMIT 1")
		->queryScalar([':pid' => $partenaireId]);
	if ($hasAbo) {
		$hasCombined = ($model->aboCombine !== 'NONE');
		echo '<div id="abocombine" class="controls' . ($hasCombined ? '' : ' hide') . '" style="margin-top:-24px; position:relative;">'
			, $form->dropDownList(
				$model,
				'aboCombine',
				[
					"ET" => "ET - intersection",
					"OU" => "OU - union"
				],
				[
					'disabled' => !$hasCombined,
				]
			)
			, ' <span class="help-inline"></span>'
			, '</div>';
		echo $form->checkBoxRow($model, 'abonnement', ['uncheckValue' => '']);
		Yii::app()->clientScript->registerScript('combine-abo', <<<EOJS
			function combineInstituteFilters() {
				const owned = document.querySelector('#SearchTitre_owned').checked;
				const abo = document.querySelector('#SearchTitre_abonnement').checked;
				const combineDiv = document.querySelector('#abocombine');
				const combineSel = combineDiv.firstElementChild;
				if (owned && abo) {
					const onlyOpenAccess = document.querySelector('#SearchTitre_accesLibre').checked;
					const selAnd = combineSel.querySelector('option[value="ET"]');
					const selOr = combineSel.querySelector('option[value="OU"]');
					if (onlyOpenAccess) {
						selOr.selected = true;
						selAnd.disabled = true;
						combineDiv.querySelector('span').innerHTML = 'La combinaison avec "accès libres" est forcément cumulative.';
					} else {
						selAnd.disabled = false;
						if (selOr.hasAttribute('selected')) {
							selOr.selected = true;
						} else {
							selAnd.selected = true;
						}
						combineDiv.querySelector('span').innerHTML = '';
					}
					combineSel.disabled = false;
					combineDiv.classList.remove('hide');
				} else {
					combineSel.disabled = true;
					combineDiv.classList.add('hide');
				}
			}
			$('#criteres-partenaire').on('click', 'input[type="checkbox"]', combineInstituteFilters);
			$('#SearchTitre_accesLibre').on('click', combineInstituteFilters);
			combineInstituteFilters();
			EOJS
		);
	}

	if ($isAuthUser) {
		echo $form->checkBoxRow($model, 'monitoredByMe', ['uncheckValue' => '', 'class' => 'not-guest']);
	}
	?>
</fieldset>
<?php } ?>

<fieldset id="criteres-mirabel">
	<legend>Dans Mir@bel</legend>
	<?php
	echo $form->textFieldRow($model, 'hdateModif');
	echo $form->textFieldRow($model, 'hdateVerif');
	if ($isAuthUser) {
		echo $form->radioButtonListRow(
			$model,
			'monitored',
			[
				SearchTitre::MONIT_YES => "Seulement les revues suivies",
				SearchTitre::MONIT_NO => "Seulement les revues non suivies",
				SearchTitre::MONIT_IGNORE => "Toutes les revues", ],
			['class' => 'not-guest']
		);
	}
	if ($model->detenu && !$model->owned) {
		?>
		<div class="control-group ">
			<label class="control-label" for="SearchTitre[detenu]">Disponible à </label>
			<div class="controls">
				<label class="checkbox">
					<input type="checkbox" name="SearchTitre[detenu]" checked value="<?= CHtml::encode(join(',', $model->detenu)) ?>">
					<em><?= CHtml::encode($model->getPartenairesNames('detenu')) ?></em>
				</label>
			</div>
		</div>
		<?php
	}
	if (!$model->monitoredByMe && !$model->monitored && $model->suivi) {
		?>
		<div class="control-group ">
			<label class="control-label" for="SearchTitre[suivi]">Suivi par </label>
			<div class="controls">
				<label class="checkbox">
					<input type="checkbox" name="SearchTitre[suivi]" checked value="<?= CHtml::encode(join(',', $model->suivi)) ?>">
					<em><?= CHtml::encode($model->getPartenairesNames('suivi')) ?></em>
				</label>
			</div>
		</div>
		<?php
	}
	if ($model->grappe > 0) {
		$gname = Grappe::model()->findByPk($model->grappe)->nom;
		?>
		<div class="control-group ">
			<label class="control-label" for="SearchTitre[grappe]">Grappe</label>
			<div class="controls">
				<label class="checkbox">
					<input type="checkbox" name="SearchTitre[grappe]" checked value="<?= $model->grappe ?>">
					<em><?= CHtml::encode($gname) ?></em>
					<?= CHtml::link(
						'<span class="micon-new-window"></span>',
						['/grappe/view', 'id' => $model->grappe],
						['style' => 'padding-left:1ex', 'target' => '_blank', 'title' => "Voir la grappe \"$gname\"dans une nouvelle page"]
					) ?>
				</label>
			</div>
		</div>
		<?php
	}
	?>
</fieldset>

<div class="form-actions" style="display: flex; justify-content: space-between;">
	<div>
		<button type="submit" class="btn btn-default">Rechercher</button>
		<br>
		<small>avec les critères de ce formulaire</small>
	</div>
	<div>
		<?= CHtml::link("Nouvelle recherche", ['/revue/search'], ['class' => "btn btn-default"]) ?>
		<br>
		<small>vider le formulaire</small>
	</div>
</div>

<?php $this->endWidget(); ?>
