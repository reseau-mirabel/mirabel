<?php

/** @var Controller $this */
/** @var SearchTitre $search */
/** @var int $totalCount */

if ($totalCount > SearchTitre::MAX_IDS / 2) {
	// Before downloading the CSV export,
	// first check if the results are truncated
	// because the number of exported results is at max.
	$maxIds = SearchTitre::MAX_IDS;
	\Yii::app()->clientScript->registerScript('truncated', <<<JS
		const exportJournalsForm = document.getElementById('export-journals-to-csv');
		exportJournalsForm.addEventListener('submit', function() {
			const data = new FormData(exportJournalsForm);
			const params = new URLSearchParams(data)
			const url = exportJournalsForm.action + '?' + params.toString() + '&count=ON';
			console.log(url);
			fetch(url, {credentials: 'same-origin'})
				.then(response => response.json())
				.then(function(count) {
					if (count >= $maxIds) {
						alert("Le CSV téléchargé est tronqué, car l'export est limité à $maxIds lignes.");
					}
				});
			return false;
		});
		JS
	);
}
?>
<details>
	<summary title="Export au format tableur CSV de la liste des revues recherchées">
		les revues
	</summary>
	<form action="<?= $this->createUrl('/revue/export'); ?>" method="get" class="form-horizontal" id="export-journals-to-csv">
		<?php
		foreach ($search->attributes as $k => $value) {
			if ($value) {
				if (is_array($value)) {
					foreach ($value as $subvalue) {
						echo CHtml::hiddenField("q[$k][]", $subvalue);
					}
				} else {
					echo CHtml::hiddenField("q[$k]", $value);
				}
			}
		}
		if (Yii::app()->user->checkAccess("avec-partenaire")) {
			echo CHtml::label(
				CHtml::checkBox('extended') . " Infos de possession",
				'extended',
				["title" => "Le CSV contiendra toutes les informations de possession de ces revues par le partenaire (identifiant local, etc)"]
			);
		}
		?>
		<button type="submit" class="btn btn-block">Exporter<br /> les revues</button>
	</form>
</details>
