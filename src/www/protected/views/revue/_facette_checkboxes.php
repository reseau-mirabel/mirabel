<?php

/** @var Controller $this */
/** @var string $attribute Property from SearchTitre, e.g. "paysId" */
/** @var array $checked List of values that are active (checked boxes for the facet or hidden inputs) */
/** @var components\sphinx\FacetInterface $facet */
/** @var string $legend */

use processes\search\facets\WithExtraResults;

Yii::app()->clientScript->registerScript('expand-facets', <<<EOJS
	$('button.facet-expand').on('click', function() {
		this.closest('fieldset').classList.add('with-extra');
	});
	EOJS
);

$values = $facet->listFilters();
if (!$values) {
	if ($checked) {
		echo CHtml::hiddenField("q[$attribute]", join('_', $checked), ['id' => false]);
	}
	return;
}
?>
<fieldset class="facet facet-<?= $facet->getName() ?> checkbox">
	<legend><?= $legend ?></legend>
	<?php
	$checkedById = array_combine($checked, $checked);
	$position = 0;
	foreach ($values as $id => [$name, $count]) {
		$position++;
		$isChecked = isset($checkedById[$id]);
		if ($isChecked) {
			$visible = "";
			unset($checkedById[$id]);
		} else {
			$visible = ($position > 5) ? "extra-facet" : "";
		}
		?>
		<label class="checkbox <?= $visible ?>">
			<?php
			echo CHtml::checkBox("f[$attribute][]", $isChecked, ['value' => $id, 'id' => false]);
			?>
			<span>
				<?= CHtml::tag('strong', [], CHtml::encode($name)) ?>
				<?= CHtml::tag('span', ['class' => 'count'], $count) ?>
			</span>
		</label>
		<?php
	}
	if ($facet instanceof WithExtraResults) {
		if ($facet->hasMoreResults()) {
			printf('<div class="extra-facet"><abbr title="Seules les %d premières valeurs sont affichées.">…</abbr></div>', WithExtraResults::MAX_RESULTS);
		}
		if ($position > 5) {
			echo '<button type="button" class="btn btn-mini btn-default facet-expand">+ de valeurs</button>';
		}
	}
	?>
</fieldset>
