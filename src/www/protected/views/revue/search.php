<?php

/** @var Controller $this */
/** @var SearchTitre $base */
/** @var SearchTitre $refine */
/** @var ?\components\sphinx\DataProvider $results */
/** @var components\sphinx\FacetInterface[] $facets */
/** @var string $searchHash */
/** @var widgets\CustomizePager $customizePager */

$titleSuffix = $base->getTitleSuffix();
$this->pageTitle = 'Recherche de revues' . $titleSuffix;

if ($results !== null) {
	Yii::app()->clientScript->registerMetaTag('robots', 'noindex');
}

$this->breadcrumbs = [
	'Revues' => ['index'],
	'Recherche',
];

/** @var \components\WebSidebar */
$sidebar = \Yii::app()->getComponent('sidebar');
if (Yii::app()->user->checkAccess("avec-partenaire")) {
	array_push(
		$sidebar->menu,
		[
			'label' => 'Liste',
			'url' => ['/revue/index'],
		],
		[
			'label' => 'Carte',
			'url' => ['/revue/carte'],
		],
		[
			'label' => 'Nouvelle revue',
			'url' => ['/titre/create-by-issn'],
		],
	);
}
if ($results instanceof \components\sphinx\DataProvider) {
	if ($customizePager->getViewMode() === \widgets\CustomizePager::VIEWMODE_SIMPLE) {
		echo $this->renderPartial('_legend', null, true);
	}
}
?>

<h1>Recherche de revues<?= $titleSuffix; ?></h1>

<?php if ($results === null) { ?>
	<p>
		Il est aussi possible de <?= CHtml::link("chercher des éditeurs", ['/editeur/search']) ?>
		ou de <?= CHtml::link("naviguer dans les thématiques", ['/categorie']) ?>.
	</p>
<?php } else { ?>
<?php } ?>

<div id="extended-search-results" <?= Yii::app()->params->itemAt('facettes') !== false ? 'class="with-facettes"' : '' ?>>
<?php
if (isset($results)) {
	?>
	<div>
		<div id="customize-links">
			<div  style="text-align: right">
				<?php
				if (Yii::app()->user->hasCustomizedLinksUser()) {
					echo '<span class="glyphicon glyphicon-star"></span> ';
				}
				?>
				<?= CHtml::link("Personnaliser les liens affichés", ['/revue/customize-links']) ?>
			</div>
			<dialog style="padding: 4px;"></dialog>
			<?php
			Yii::app()->clientScript->registerScript('customize-links', <<<JS
				const dialogCustomizeLinks = document.querySelector('#customize-links dialog');
				document.querySelector('#customize-links > div > a').addEventListener("click", (event) => {
					event.preventDefault();
					fetch('/revue/customize-links?ajax=1')
						.then((response) => response.json())
						.then((data) => loadModalHtml(data.html));
					return false;
				});
				function loadModalHtml(html) {
					dialogCustomizeLinks.innerHTML = html;
					dialogCustomizeLinks.showModal();
					dialogCustomizeLinks.querySelector("button.close").addEventListener("click", () => dialogCustomizeLinks.close());
					const form = dialogCustomizeLinks.querySelector('form');
					form.addEventListener("submit", (event) => {
						event.preventDefault();
						fetch('/revue/customize-links?ajax=1', {method:'post', body: new FormData(form)})
							.then((response) => response.json())
							.then((data) => {
								if (data.html === '') {
									window.location.reload(true);
								} else {
									dialogCustomizeLinks.innerHTML = data.html;
								}
							});
					});
				}
				JS
			);
			?>
		</div>
		<details style="margin-top: 1em">
			<summary class="contains-block">
				<h2 id="new-search">
					<div class="pull-right" style="font-weight: normal; font-family: sans-serif;">
						<?= CHtml::link("Nouvelle recherche", ['/revue/search'], ['class' => 'btn']) ?>
					</div>
					Modifier la recherche
				</h2>
			</summary>
			<div>
				<?php
				if (!$refine->isEmpty()) {
					echo '<div class="alert alert-info">Attention, modifier la recherche mettra à zéro les filtres par facettes.</div>';
				}
				$baseSearch = new SearchTitre();
				$baseSearch->setAttributes($base->getAttributes(), false);
				$this->renderPartial(
					'_search',
					['model' => $baseSearch, 'partenaireId' => Yii::app()->user->getState('instituteId')]
				);
				?>
			</div>
		</details>

		<div id="search-summary">
			<div class="search-base"><strong>Critères :</strong> <?= $base->htmlSummary() ?></div>
			<?php
			if (!$refine->isEmpty()) {
				echo "<div class=\"facets\"><strong>Filtré par :</strong> {$refine->htmlSummary()}</div>";
			}
			?>
		</div>

		<?php
		$itemViewSuffix = ($customizePager->getViewMode() === \widgets\CustomizePager::VIEWMODE_SIMPLE ? 'simple' : 'details');
		$customizingBlock = $results->itemCount > 0 ? $customizePager->run() : "";
		$displayedLinksCriteria = new CDbCriteria();
		$displayedLinksCriteria->addInCondition('nomcourt', Yii::app()->user->getCustomizedLinks() ?? Sourcelien::DEFAULT_SEARCH_DISPLAY);
		$displayedLinksCriteria->select = "id, nom, nomcourt, nomlong";
		$displayedLinksCriteria->order = "nom";
		$this->widget(
			'bootstrap.widgets.BootListView',
			[
				'dataProvider' => $results,
				'itemView' => "/global/_sphinxTitre-{$itemViewSuffix}",
				'template' => <<<EOHTML
					<div class="pagination-block">{summary}{$customizingBlock}</div>
					{sorter}
					{items}
					<div class="pagination-block">{pager}{$customizingBlock}</div>
					EOHTML,
				'viewData' => [
					'hash' => $searchHash,
					'sourceLiens' => Sourcelien::model()->findAll($displayedLinksCriteria),
				],
			]
		);
		?>
	</div>
	<?php
	if ($results->itemCount > 0 && Yii::app()->params->itemAt('facettes') !== false) {
		echo '<aside>';
		$this->renderPartial(
			'_facettes',
			['base' => $base, 'refine' => $refine, 'facets' => $facets]
		);
		if ($refine->isEmpty()) {
			// We can only export results for a base search without facets.
			$this->renderPartial('_export', ['search' => $base, 'totalCount' => $results->getTotalItemCount()]);
		}
		echo '</aside>';
	}
} else {
?>
	<div>
		<h2 id="new-search">Nouvelle recherche</h2>
		<?php
		$this->renderPartial(
			'_search',
			['model' => $base, 'partenaireId' => Yii::app()->user->getState('instituteId')]
		);
		?>
	</div>
<?php } ?>
</div>
<?php
if (isset($results)) {
	$maxPageRank = $results->getPagination()->getPageCount();
	(new \widgets\SearchNavigationInit('revue', $results->getData(), $maxPageRank))->run();
}
