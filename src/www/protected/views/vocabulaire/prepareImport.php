<?php

/** @var Controller $this */
assert($this instanceof Controller);

$this->pageTitle = "Thématique - Import Vocab";
$this->breadcrumbs = [
	'Thématique' => ['/categorie/index'],
	'Vocabulaires' => ['index'],
	'Import, étape 1/2',
];
?>

<h1>Importer des vocabulaires thématiques 1/2</h1>

<div class="alert alert-info">
	<h2>Fonctionnement</h2>
	<ul>
		<li>Déposer un fichier au format CSV en indiquant le séparateur de colonnes.</li>
		<li>Le fichier doit être encodé en UTF-8.</li>
		<li>
			La première ligne donne les en-têtes ;
			<ul>
				<li>il doit y avoir une colonne numérique <strong>ID</strong> ou textuelle <strong>Thème</strong>
					(ou ces deux colonnes, celle avec ID étant prioritaire).</li>
				<li>Chaque autre colonne sera proposée comme Vocabulaire.</li>
			</ul>
		</li>
		<li>
			Un même thème peut être répété sur plusieurs lignes, ce qui permet de multiplier ses alias
			dans un même vocabulaire.
		</li>
		<li>
			<strong>Le plus pratique</strong> est normalement d'exporter les vocabulaires, de modifier ce fichier CSV, et de l'importer ensuite.
		</li>
	</ul>
	<h2>Exemple</h2>
	<pre>
ID;Thème;Rameau;Persée
3;;Psychologie clinique;
;Archéologie;Paléographie;Antiquités et archéologie
;Archéologie;Paléontologie;
	</pre>
	<ul>
		<li>
			Le chargement de ce CSV proposera de créer ou modifier deux vocabulaires :
			<em>Rameau</em> et <em>Persée</em>.
		</li>
		<li>
			Le thème d'identifiant 3 aura l'alias <em>Psychologie clinique</em> dans le vocabulaire <em>Rameau</em>.
		</li>
		<li>
			Le thème nommé <em>Antiquités</em> dans Mir@bel aura
			les alias <em>Paléographie</em> et <em>Paléontologie</em> dans le vocabulaire <em>Rameau</em>,
			et l'alias <em>Antiquités et archéologie</em> dans le vocabulaire <em>Persée</em>.
		</li>
	</ul>
</div>

<form enctype="multipart/form-data" action="" method="POST">
	<input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
	<div>
		<label>Fichier CSV</label>
		<input name="csvfile" type="file" />
	</div>
	<div>
		<label>Séparateur</label>
		<select name="separator">
			<option value=";">;</option>
			<option value=",">,</option>
			<option value="	">Tabulation</option>
			<option value="|">|</option>
		</select>
	</div>
	<div>
		<button type="submit">Envoyer le fichier</button>
	</div>
</form>
