<?php

/** @var Controller $this */
/** @var CActiveDataProvider $dataProvider */
assert($this instanceof Controller);

$this->pageTitle = "Thématique - Vocabulaires";
$this->breadcrumbs = [
	'Thématique' => ['/categorie/index'],
	'Vocabulaires',
];
?>
<h1>Vocabulaires</h1>
<p>
	Les vocabulaires sont des groupes d'homonymes de thèmes de Mir@bel.
	Par exemple, un vocabulaire peut faire la correspondance entre l'indexation Rameau et la thématique de Mir@bel.
</p>

<?php
$columns = [
	'titre',
	'aliasCount',
	'indexationCount',
	[
		'class' => 'CButtonColumn',
		'template' => '{update}',
	],
];

$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	[
		'id' => 'vocabulaire-grid',
		'dataProvider' => $dataProvider,
		'filter' => null,
		'columns' => $columns,
		'ajaxUpdate' => false,
	]
);
?>
