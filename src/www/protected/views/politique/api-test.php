<?php

/** @var Controller $this */
/** @var array $records */

$this->pageTitle = "API demo on mock data";
$this->containerClass = "container full-width";

$baseUrl = "/" . $this->getRoute();
?>
<div id="api-yaml">
	Download the description as
	<?= CHtml::link("OpenApi JSON", ['/files/politique-api/openapi.json']) ?>
	or <?= CHtml::link("YAML", ['/files/politique-api/openapi.yaml']) ?>
</div>

<h1><?= CHtml::encode($this->pageTitle) ?></h1>

<p>
	The data used for this demo of the API was forked from the real data then slightly modified.
	See <a href="/politique/api">/politique/api</a> for querying on the real and up to date data.
</p>

<h2>Valid requests</h2>

<p>
	The requests below will return a HTTP 200, while other requests will return HTTP 404 or other error codes.
</p>

<h3>/changes</h3>

<ul>
	<li><a href="<?= $baseUrl ?>/changes">changes</a></li>
</ul>

<h3>For each triple returned by /changes…</h3>

<p>
	This table is available in a JSON format,
	either by requesting the page without the HTTP header<code>Accept: text/html</code>,
	or by appending the parameter <code>?format=json</code> to the URL.
</p>

<table class="table">
	<thead>
		<tr>
			<th>/publication</th>
			<th>/publisher</th>
			<th>/policy</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($records as $record) { ?>
		<tr>
			<td><a href="<?= $baseUrl ?>/publication/<?= $record['publication_id'] ?>"><?= CHtml::encode($record['publication']) ?></a></td>
			<td><a href="<?= $baseUrl ?>/publisher/<?= $record['publisher_id'] ?>"><?= CHtml::encode($record['publisher']) ?></a></td>
			<td><a href="<?= $baseUrl ?>/policy/<?= $record['policy_id'] ?>"><?= CHtml::encode($record['policy_id']) ?></a></td>
			<td><?= CHtml::encode($record['description']) ?></td>
		</tr>
		<?php } ?>
	</tbody>
</table>

<div>
	All of the IDs linked above are reserved for the test data.
	The production data in Mir@bel cannot contain IDs in these ranges:
	<ul>
		<li>policy: 230 ≤ policy_id ≤ 300</li>
		<li>publication: 20455 to 20500</li>
		<li>publisher: 8330 to 8400</li>
	</ul>
</div>

<h3>Publications without policies</h3>

<p>
	A few publications have been included because they are part of a journal history where the active publication has a policy.
</p>
<p>
	For instance,
	<a href="<?= $baseUrl ?>/publication/20464">(ID 20464)</a>,
	<a href="<?= $baseUrl ?>/publication/20468">(ID 20468)</a>,
	<a href="<?= $baseUrl ?>/publication/20467">(ID 20467)</a>,
	<a href="<?= $baseUrl ?>/publication/20466">(ID 20466)</a> are ancestors of
	<a href="<?= $baseUrl ?>/publication/20463">(ID 20463) Annales : Histoire, Sciences Sociales</a> which has a policy.
	Most of them are linked to the publisher <a href="<?= $baseUrl ?>/publisher/8353">Éditions de l'EHESS</a>, where their IDs are listed.
</p>

<h2>Invalid requests</h2>

<ul>
	<li>
		HTTP 404 when the requested object is not found, e.g.
		<a href="<?= $baseUrl ?>/publication/9999">wrong publication ID</a>,
		<a href="<?= $baseUrl ?>/publisher/9999">wrong publisher ID</a>,
		<a href="<?= $baseUrl ?>/policy/9999">wrong policy ID</a>.
	</li>
	<li>
		HTTP 401 when the authentication is missing in the real API, e.g. <a href="/politique/api/publisher/3">/politique/api/publisher/3</a>.
	</li>
	<li>
		HTTP 400 for a <a href="<?= $baseUrl ?>/blabla">bad request path</a>.
	</li>
</ul>
