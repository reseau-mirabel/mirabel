<?php
/** @var Editeur $editeur */

Yii::app()->getClientScript()->registerScript(
	'email-obfus',
	'var contactInfo = ' . json_encode([
		'email' => 'f' . Config::read('contact.visible.email'),
		'subject' => rawurlencode("revue à associer à " . $editeur->nom),
		'body' => rawurlencode("Il manque une revue dans les revues listées associées à \"{$editeur->nom}\".\nMerci d'ajouter ou vérifier cette revue :\n[préciser Nom et ISSN de la revue]"),
	])
	. ';
	$("a.contact").on("click", function() {
		$(this).attr("href", "mailto:" + contactInfo.email.substring(1) + "?subject=" + contactInfo.subject + "&body=" + contactInfo.body);
	})
');
?>
<p class="alert alert-info">
	Si une revue ne figure pas dans cette liste,
	<a class="contact" href="#email">contactez l'équipe Mir@bel</a>
	pour qu'elle crée cette revue.
</p>
