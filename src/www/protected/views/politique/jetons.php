<?php

/** @var Controller $this */
/** @var PolitiqueJeton[] $jetons */

assert($this instanceof Controller);

$this->pageTitle = "Jetons d'accès à l'API politiques";

$this->breadcrumbs = [
	"Politiques" => ['/admin/politique'],
	"Jetons d'accès API",
];
?>
<h1>
	<?= CHtml::encode($this->pageTitle) ?>
</h1>

<p>
	Effacer le nom ou le contenu d'un jeton pour le supprimer.
	Le <em>nom</em> est purement indicatif, seul le champ <em>jeton</em> permet d'accéder à l'API.
</p>
<p>
	L'utilisation d'un jeton est par paramètre d'URL <code>?api_key=<em>jeton</em></code>
	ou par entête HTTP <code>X-Api-Key: <em>jeton</em></code>.
	La spécification OpenApi, téléchargeable sur la page de documentation, contient plus de détails.
</p>

<div class="row">
	<div class="span5"><strong>Nom</strong></div>
	<div class="span5"><strong>Jeton</strong></div>
</div>
<?php
foreach ($jetons as $jeton) {
	$form = $this->beginWidget(
		'bootstrap.widgets.BootActiveForm',
		[
			'enableAjaxValidation' => false,
			'enableClientValidation' => false,
			'type' => BootActiveForm::TYPE_HORIZONTAL,
		]
	);
	/** @var BootActiveForm $form */
	?>
	<div class="row" id="record-<?= (string) $jeton->id ?>">
		<div class="span5">
			<?= $form->errorSummary($jeton) ?>
			<?= $form->textField($jeton, "name", ['class' => 'name input-xlarge']) ?>
		</div>
		<div class="span5">
			<?= $form->textField($jeton, "token", ['class' => 'token input-xlarge', 'placeholder' => "alphanumérique"]) ?>
			<button type="button" class="btn btn-default rand" data-id="<?= $jeton->id ?>">Générer</button>
		</div>
		<div class="span2">
			<button type="submit" name="PolitiqueJeton[id]" value="<?= $jeton->id ?>" class="btn btn-primary">
				<?= $jeton->id ? "Modifier" : "Créer" ?>
			</button>
		</div>
	</div>
	<?php
	$this->endWidget(); // form
}
?>

<?php
Yii::app()->getClientScript()->registerScript('rand', <<<EOJS
	$("button.rand").on("click", function() {
		var id = $(this).attr('data-id');
		$("#record-" + id + " input.token").val(getRandomString());
	});
	// low security, but it's not important in this context
	function getRandomString() {
		var alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		return shuffleString(alphabet.repeat(8)).substr(0, 32);
	}
	function shuffleString(s) {
		var a = s.split("");
		var n = a.length;

		for(var i = n - 1; i > 0; i--) {
			var j = Math.floor(Math.random() * (i + 1));
			var tmp = a[i];
			a[i] = a[j];
			a[j] = tmp;
		}
		return a.join("");
	}
	EOJS
);
?>
