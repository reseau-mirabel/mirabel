<?php

/** @var PolitiqueController $this */
/** @var Politique $politique */

assert($this instanceof PolitiqueController);

$this->pageTitle = "Annoter une politique de publication";
$this->breadcrumbs = [
	"Modération des politiques" => ['/politique/moderation'],
	$politique->name => ['/politique/update', 'id' => $politique->id],
	"Annoter",
];
?>
<h1>
	<?= CHtml::encode($this->pageTitle) ?>
</h1>

<form method="POST" class="form-horizontal">
	<div class="control-group">
		<label class="control-label" for="notesAdmin">Notes d'admin</label>
		<div class="controls">
			<textarea name="notesAdmin" rows="10" style="width:100%"><?= CHtml::encode($politique->notesAdmin) ?></textarea>
		</div>
	</div>
	<div class="control-group">
		<div class="controls">
			<button type="submit" class="btn btn-primary">Enregistrer</button>
		</div>
	</div>
</form>

