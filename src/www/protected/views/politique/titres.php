<?php

/** @var Controller $this */
/** @var Politique $politique */
/** @var models\forms\PolitiqueTitresForm $politiqueTitresForm */
/** @var string $mode */

assert($this instanceof PolitiqueController);

$titres = $politiqueTitresForm->getTitres();

$this->pageTitle = "Affecter la politique aux titres";
$this->breadcrumbs = [
	"Politiques de publication" => ['/politique/index', 'editeurId' => $politique->editeurId],
	"Titres",
];
?>
<h1>
	<?= CHtml::encode($this->pageTitle) ?>
</h1>

<div class="alert alert-info">
	<p>
		Vous pouvez ici affecter la politique <strong><?= CHtml::encode($politique->name) ?></strong>
		à des titres de l'éditeur <strong><?= CHtml::encode($politique->editeur->nom) ?></strong>.
		<?php if ($mode === 'transmission') { ?>
		Cette politique sera transmise à « Open policy finder » pour tous les titres sélectionnés.
		<?php } ?>
	</p>
	<?= $this->renderPartial('_contact', ['editeur' => $politique->editeur]) ?>
</div>

<h2><?= $politique->editeur->nom ?></h2>

<p>
	<a href="<?= $this->createUrl('/politique', ['editeurId' => $politique->editeurId]) ?>" class="btn btn-default">Retour à la page principale</a>
</p>

<?php
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'politique-titres-form',
		'enableAjaxValidation' => false,
		'htmlOptions' => ['class' => 'well'],
		'type' => BootActiveForm::TYPE_HORIZONTAL,
	]
);
/** @var BootActiveForm $form */

echo $form->errorSummary($politiqueTitresForm);
?>

<p>
	Les titres ayant déjà une politique portent une <span class="label">étiquette</span>.
	Si la case est cochée, la politique <strong><?= CHtml::encode($politique->name) ?></strong> leur sera affectée en remplacement de la précédente.
</p>

<div class="control-group">
	<label class="control-label" for="pt_ids">Titres actuels</label>
	<div class="controls">
		<?php
		foreach ($titres['vivants'] as $id => $t) {
			$isChecked = in_array($id, $politiqueTitresForm->ids);
			echo '<label class="checkbox titre-' . $id . '"><input value="' . $id . '" type="checkbox" name="pt[ids][]"' . ($isChecked ? ' checked' : '') . ' />'
				. $t
				. '</label>';
		}
		?>
	</div>
</div>
<div class="control-group">
	<label class="control-label" for="pt_ids">Anciens titres</label>
	<div class="controls">
		<?php
		foreach ($titres['morts'] as $id => $t) {
			$isChecked = in_array($id, $politiqueTitresForm->ids);
			echo '<label class="checkbox titre-' . $id . '"><input value="' . $id . '" type="checkbox" name="pt[ids][]"' . ($isChecked ? ' checked' : '') . ' />'
				. $t
				. '</label>';
		}
		?>
	</div>
</div>

<?php
\Yii::app()->clientScript->registerScript(
	'politique-titres',
	<<<EOJS
	var hasConfirmed = false;
	$("#politique-titres-form").on("click", function(event) {
		if (event.target.tagName === 'A') {
			event.stopPropagation();
		}
		return true;
	});
	$("#politique-titres-form").on("change", "label.checkbox", function(event) {
		if (hasConfirmed || $("span.label", this).length === 0) {
			return true;
		}
		if (confirm("Ce titre a déjà une politique attribuée. Confirmez-vous la remplacer ?")) {
			hasConfirmed = true;
			return true;
		}
		$(this).find('input[type=checkbox]').prop('checked', false);
		event.stopPropagation();
		return false;
	});
	EOJS
);
?>

<div class="form-actions">
	<button type="submit" class="btn btn-primary">
		Enregistrer
		<?php if ($mode === 'transmission') { ?>
		<strong>et transmettre à Open policy finder.</strong>
		<?php } ?>
	</button>
</div>

<?php $this->endWidget(); ?>

