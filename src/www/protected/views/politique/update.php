<?php

/** @var PolitiqueController $this */
/** @var Politique $politique */
/** @var models\forms\PolitiqueForm $politiqueForm */

assert($this instanceof PolitiqueController);

$this->pageTitle = "Politique de publication - vue détaillée";
$this->breadcrumbs = [
	"Politiques de publication" => ['/politique/index', 'editeurId' => $politique->editeurId],
	$politique->name,
];
?>
<h1>
	<?= CHtml::encode($this->pageTitle) ?>
</h1>

<?php
if (Yii::app()->user->access()->toPolitique()->isAdmin()) {
	?>
	<div style="text-align: right">
		Statut : <strong><?= Politique::translateStatus($politique->status) ?></strong>
	</div>
	<section style="margin-bottom: 3ex">
		<h3>Actions de modération</h3>
		<div style="display: flex; gap: 2ex;">
			<?php
			if ($politique->status === Politique::STATUS_PENDING) {
				echo CHtml::link("Validation des comptes déclarant des politiques…", ['/admin/politique-validation'], ['class' => 'btn btn-primary']);
			}
			if ($politique->status === Politique::STATUS_DRAFT) {
				echo "<p>Politique en cours de rédaction par son auteur.</p>";
				echo components\HtmlHelper::postButton(
					'<span class="glyphicon glyphicon-share"></span> Forcer la pré-publication',
					['/politique/publish', 'id' => $politique->id, 'multi' => 1],
					[],
					['class' => "btn btn-success", 'title' => "La politique sera en attente de validation pour être publiée."]
				);
			}
			if ($politique->status === Politique::STATUS_UPDATED) {
				echo "<p>Politique déjà publiée, mais en cours de modification par son auteur.</p>";
				echo components\HtmlHelper::postButton(
					'<span class="glyphicon glyphicon-share"></span> Forcer la pré-publication',
					['/politique/publish', 'id' => $politique->id, 'multi' => 1],
					[],
					['class' => "btn btn-success", 'title' => "La politique sera en attente de validation pour être publiée."]
				);
			}
			if ($politique->status === Politique::STATUS_TOPUBLISH) {
				echo components\HtmlHelper::postButton(
					'<span class="glyphicon glyphicon-share"></span> Publier vers Sherpa',
					['/politique/publish', 'id' => $politique->id, 'multi' => 1],
					[],
					['class' => "btn btn-success", 'title' => "La politique sera visible dans les données que récupère Sherpa."]
				);
				echo components\HtmlHelper::postButton(
					'<span class="glyphicon glyphicon-backward"></span> Rejeter (demande de corrections)',
					['/politique/reject', 'id' => $politique->id],
					[],
					[
						'class' => "btn btn-warning",
						'onclick' => 'return confirm("Vous allez refuser la publication de cette politique. Il faudra contacter l´auteur pour lui expliquer cette décision. Confirmez-vous ?")',
						'title' => "Renvoie la politique à l'état antérieur où son auteur doit la modifier.",
					]
				);
			}
			if ($politique->status === Politique::STATUS_PUBLISHED) {
				echo "<p>Politique publiée. En cas de modification, elle devra passer par un nouveau cycle de validation.</p>";
			}
			echo components\HtmlHelper::postButton(
				'<span class="glyphicon glyphicon-warning-sign"></span> Supprimer définitivement',
				['/politique/delete', 'id' => $politique->id],
				[],
				[
					'class' => "btn btn-danger",
					'onclick' => 'return confirm("Supprimer définitivement cette politique ?")',
					'title' => "Supprime définitivement cette politique.",
				]
			);
			?>
		</div>
	</section>
	<?php
}
?>

<section style="margin-bottom: 3ex">
	<h3>Titres appliquant cette politique</h3>
	<ul>
		<?php
		$titresAssigned = $politique->getTitresAssigned();
		$titresPending = $politique->getTitresPending();
		if (!$titresAssigned && !$titresPending) {
			echo "<li>Aucune revue associée</li>";
		} else {
			foreach ($titresAssigned as $titre) {
				echo '<li class="journal">'
					. $titre->getFullTitle()
					. ' '
					. CHtml::link(
						CHtml::image('/images/logo-mirabel-16px.png', $titre->getFullTitle()),
						$titre->getRevueUrl(),
						['title' => "Voir cette revue dans Mir@bel"]
					)
					. "</li>";
			}
			foreach ($titresPending as $titre) {
				echo '<li class="journal unvalidated">'
					. $titre->getFullTitle()
					. ' '
					. CHtml::link(
						CHtml::image('/images/logo-mirabel-16px.png', $titre->getFullTitle()),
						$titre->getRevueUrl(),
						['title' => "Voir cette revue dans Mir@bel"]
					)
					. "</li>";
			}
		}
		?>
	</ul>
</section>

<?= (new \widgets\PolitiqueExpert($politique))->run() ?>
