<?php

/** @var Controller $this */
/** @var Editeur $editeur */
/** @var bool $isRolePending Pending role on this publisher? */
/** @var \processes\politique\TitreEtc[] $titres */

?>
<ol>
<?php foreach ($titres as $t) { ?>
	<li class="titre-<?= $t->titre->id ?>">
		<div>
			<?= $t->titre->dateFin ? "&dagger; " : "" ?>
			<?= CHtml::encode($t->titre->titre) ?>
			<?php if ($t->politique === null && $t->titre->getLiensEditoriaux() !== null) { ?>
				<abbr title="Pas de politique déclarée ici, mais il en existe une dans Open policy finder" style="margin-right:2ex">
					<span class="glyphicon glyphicon-exclamation-sign"></span>
				</abbr>
			<?php } ?>
			<?= $t->getLabel() ?>
			<?php if (count($t->titre->editeurs) > 1) { ?>
			<div style="margin: 6px 0 0 30px">
				publié avec :
				<?php
				$editeurs = [];
				foreach ($t->titre->editeurs as $e) {
					if ((int) $e->id === (int) $editeur->id) {
						continue;
					}
					$editeurs[] = $e->getSelfLink();
				}
				echo join(" + ", $editeurs);
				?>
			</div>
			<?php } ?>
			<div style="margin: 6px 0 0 30px">
				Politique :
				<span class="politique-actions">
					<?php
					if ($t->politique) {
						echo $t->getActions();
					} else {
						echo CHtml::link(
							'<span class="glyphicon glyphicon-plus" title="Créer la politique"></span>',
							['/politique/assistant', 'editeurId' => $editeur->id, 'titreId' => $t->titre->id],
							['class' => 'policy-create btn btn-small btn-default', 'title' => "Créer une nouvelle politique pour ce titre"]
						);
						if (!$isRolePending) {
							echo '<button type="button" class="policy-assign btn btn-small btn-default" data-titreid="' . $t->titre->id . '">'
								, '<span class="glyphicon glyphicon-link" title="Associer une politique existante…"></span>'
								. '</button>';
						}
					}
					?>
				</span>
			</div>
		</div>
	</li>
<?php } ?>
</ol>

<?php
Yii::app()->getClientScript()->registerScript('policy-assign',
	<<<EOJS
	$(document).on('click', 'button.policy-assign', function() {
		let titreId = $(this).attr('data-titreid');
		let parent = $(this).parent();
		$.ajax({
			method: 'GET',
			url: '/politique/ajax-candidates?editeurId={$editeur->id}&titreId=' + titreId
		}).then(function(data) {
			if (data.status === 'success') {
				parent.html(data.html);
			} else if (data.status === 'warning') {
				let message = $('<span class="alert alert-warning"></span>').html(data.message);
				parent.append(message)
			} else {
				alert(data.message);
			}
		});
	});

	function post(url) {
		const form = document.createElement('form');
		form.action = url;
		form.method = 'POST';
		document.body.appendChild(form);
		form.submit();
	}

	$(document).on('change', 'select.policy-assign', function() {
		const titreId = this.getAttribute('data-titreid');
		const choice = this.value;
		if (choice === 'sherpa') {
			post('/politique/load-sherpa?editeurId={$editeur->id}&titreId=' + titreId);
		} else if (parseInt(choice) > 0) {
			post('/politique/assign?politiqueId=' + choice + '&titreId=' + titreId);
		}
	});
	EOJS
);
Yii::app()->getClientScript()->registerScript('policy-publish',
	<<<EOJS
	$(document).on('click', 'button.policy-publish', function() {
		let politiqueId = $(this).attr('data-politiqueid');
		let parent = $(this).parent();
		$.ajax({
			method: 'POST',
			url: '/politique/ajax-publish/' + politiqueId
		}).then(
			// success
			function(data) {
				console.log(data);
				if (data.status === 200) {
					parent.text(data.message);
				} else if (data.status === 302) {
					window.location = data.location;
				} else {
					alert(data.message);
				}
			},
			// failure
			function() {
				alert("Une erreur interne s'est produite. Merci de contacter les administrateurs du site.");
			}
		);
	});
	EOJS
);
