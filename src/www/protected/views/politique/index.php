<?php

/** @var Controller $this */
/** @var processes\politique\Index $model */

assert($this instanceof PolitiqueController);

$this->pageTitle = "Déclaration des politiques de diffusion d'articles en accès ouvert";
$this->breadcrumbs = [
	"Politiques de publication",
];

Yii::app()->getClientScript()->registerCssFile('/css/glyphicons.css');
?>
<h1>
	<?= CHtml::encode($this->pageTitle) ?>
</h1>

<p>
	Déclarer une politique en quelques étapes : <a href="/site/page/politiques-aide">aide et explications</a>.<br />
	<a href="/site/politiques">Présentation du service</a>
</p>

<section id="editeurs" style="margin-top: 2em">
	<h2>Éditeur</h2>
	<p class="alert alert-info">
		<?php
		switch (count($model->editeurList)) {
			case 0:
				echo "Votre compte ne vous permet pas de proposer une politique.";
				break;
			case 1:
				echo "Votre compte vous permet de proposer une politique uniquement pour l'éditeur sélectionné.";
				break;
			default:
				echo "Votre compte vous permet de proposer une politique pour différents éditeurs.";
				break;
		}
		?>
	</p>

	<?php
	if ($model->editeur === null) {
		if ($model->editeurList) {
			if (count($model->editeurList) > 1) {
				echo "<form>"
					. "Choisir un éditeur déjà associé à mon compte : "
					. CHtml::dropDownList('editeurId', '', CHtml::listData($model->editeurList, 'id', 'nom'), ['empty' => '…', 'style' => "vertical-align: top"])
					. '<button type="submit" class="btn btn-sm btn-primary">OK</button>'
					. "</form>";
			} else {
				?>
				Choisir l'éditeur déjà associé à mon compte :
				<a class="btn btn-default" href="<?= $this->createUrl('/politique', ['editeurId' => $model->editeurList[0]->id]) ?>">
					<?= CHtml::encode($model->editeurList[0]->nom) ?>
				</a>
				<?php
			}
		}
	} else {
		?>
		<p>
			<span class="glyphicon glyphicon-ok"></span>
			<strong><?= $model->editeur->getSelfLink() ?></strong>
		</p>
		<?php if (count($model->editeurList) > 1) { ?>
		<p>
			<?= CHtml::link("Changer d'éditeur", ['/politique/index', 'editeurId' => 0], ['class' => "btn btn-default"]) ?>
		</p>
		<?php
		}
	}
	?>

	<p>
		<?php
		$verb = Yii::app()->user->access()->toPolitique()->validate() ? "Ajouter" : "Demander";
		switch (count($model->editeurList)) {
			case 0:
				echo CHtml::link("$verb l'accès", ['/utilisateur/politiques-editeurs']) . " pour le compte d'un éditeur.";
				break;
			case 1:
				echo CHtml::link("$verb l'accès", ['/utilisateur/politiques-editeurs']) . " pour le compte d'autres éditeurs.";
				break;
			default:
				if ($model->editeur === null) {
					echo "Un éditeur n'apparait pas ? " . CHtml::link("$verb l'accès", ['/utilisateur/politiques-editeurs']) . " pour le compte d'autres éditeurs.";
				}
				break;
		}
		?>
	</p>
</section>

<?php
if ($model->editeur === null) {
	return;
}

$isRolePending = ($model->getRoleOnPublisher() === components\access\Politique::ROLE_EDITEUR_PENDING);
echo $this->renderPartial('_index-titres', ['isRolePending' => $isRolePending, 'model' => $model]);
if ($model->hasManyJournals()) {
	echo $this->renderPartial('_index-politiques', ['isRolePending' => $isRolePending, 'model' => $model]);
}

