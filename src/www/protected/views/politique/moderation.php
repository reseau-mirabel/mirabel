<?php

/** @var Controller $this */
/** @var \models\searches\PolitiqueSearch $model */

assert($this instanceof Controller);

$this->pageTitle = "Modération des politiques de publication";

$this->breadcrumbs = [
	"Politiques" => ['/admin/politique'],
	"Modération des politiques",
];
Yii::app()->getClientScript()->registerCssFile('/css/glyphicons.css');
?>
<h1>
	<?= CHtml::encode($this->pageTitle) ?>
</h1>

<details>
	<summary>Aide à la recherche…</summary>
	<?= Cms::getBlock('politique-moderation')->toHtml() ?>
</details>

<?php
if (!$model->validate()) {
	echo CHtml::errorSummary($model);
}

$this->widget(
	'ext.bootstrap.widgets.BootGridView',
	[
		'id' => 'politique-grid',
		'dataProvider' => $model->search(),
		'filter' => $model,
		'columns' => [
			[
				'name' => 'creator',
				'header' => 'Auteur',
				'type' => 'raw',
				'value' => function (\Politique $p): string {
					$createur = $p->createur;
					if ($createur) {
						return CHtml::link(CHtml::encode($createur->nomComplet), ['/utilisateur/view', 'id' => $createur->id]);
					}
					return "{$p->utilisateurId} (supprimé)";
				},
			],
			[
				'name' => 'publisher',
				'header' => 'Éditeur',
				'value' => function (\Politique $p): string {
					$editeur = $p->editeur;
					if ($editeur) {
						return (string) $p->editeur->nom;
					}
					return "{$p->editeurId} (supprimé)";
				},
			],
			[
				'name' => 'status',
				'value' => function (\Politique $p): string {
					return $p->status;
				},
				'filter' =>
					<<<EOHTML
					<input name="q[status]" value="{$model->status}" type="text" list="status-options" multiple />
					<datalist id="status-options">
						<option value="pending">
						<option value="draft">
						<option value="topublish">
						<option value="published">
						<option value="updated">
						<option value="todelete">
					</datalist>
					EOHTML,
			],
			[
				'name' => 'date',
				'header' => 'Der modif',
				'value' => function (\Politique $p): string {
					return date('Y-m-d', $p->lastUpdate);
				},
			],
			[
				'header' => 'Politique',
				'type' => 'raw',
				'value' => function (\Politique $p): string {
					$html = CHtml::link(CHtml::encode($p->name), ['/politique/update', 'id' => $p->id]);
					if ($p->notesAdmin) {
						$html .= " " . CHtml::tag('span', ['class' => "glyphicon glyphicon-alert", 'title' => "## Notes d'admin ##\n {$p->notesAdmin}"]);
					}
					return $html;
				},
			],
			[
				'header' => 'Titres',
				'type' => 'raw',
				'value' => function (\Politique $p): string {
					$titres = Yii::app()->db
						->createCommand(<<<EOSQL
							SELECT
								t.id, t.revueId, t.titre, t.prefixe, t.sigle, t.liensJson,
								JSON_EXTRACT(sp.content, '$.publisher_policy[0].uri') AS sherpaPolicyUrl,
								JSON_EXTRACT(sp.content, '$.system_metadata.uri') AS sherpaJournalUrl
							FROM Politique_Titre pt
								JOIN Titre t ON pt.titreId = t.id
								LEFT JOIN SherpaPublication sp ON sp.titreId = t.id
							WHERE pt.politiqueId = {$p->id}
							ORDER BY t.titre
							EOSQL
						)->queryAll();
					if (!$titres) {
						return '';
					}
					$hasRoles = Yii::app()->db->createCommand(<<<EOSQL
						SELECT MIN(IFNULL(IF(te.role <> '', te.role, e.role), '') <> '')
						FROM Titre_Editeur te
							JOIN Editeur e ON te.editeurId = e.id
						WHERE te.titreId = :tid
						EOSQL
					);
					$isPartOfCairn = Yii::app()->db->createCommand(<<<EOSQL
						SELECT 1
						FROM Service s
							JOIN Service_Collection sc ON sc.serviceId = s.id
							JOIN Collection c ON sc.collectionId = c.id AND c.nom NOT IN ('Cairn International', 'Cairn Ouvrages')
						WHERE s.titreId = :tid AND s.ressourceId = 3 AND s.type = 'Intégral'
						LIMIT 1
						EOSQL
					);
					$output = [];
					foreach ($titres as $row) {
						$t = \Titre::model()->populateRecord($row);
						$alert = "";
						if (!$hasRoles->queryScalar([':tid' => $t->id])) {
							$alert = " " . CHtml::link(
								'<span class="glyphicon glyphicon-alert"></span>',
								['/titre/editeurs', 'id' => $t->id],
								[
									'class' => 'noexternalicon',
									'title' => "Le rôle de l'éditeur n'est déclaré ni sur l'éditeur ni sur le lien à son titre."
										. " Compléter les relations du titres à son ou ses éditeurs avant de valider la politique."
										. " (nouvel onglet)",
									'target' => '_blank',
								]
							);
						}
						if ($isPartOfCairn->queryScalar([':tid' => $t->id])) {
							$alert .= " " . CHtml::link(
								'<span class="glyphicon glyphicon-alert"></span>',
								['/titre/view', 'id' => $t->id],
								[
									'class' => 'noexternalicon',
									'title' => "Ce titre est diffusé sur Cain.info. Un accès en ligne en texte intégral est dans une collection de Cairn.info hors Ouvrages et International.",
									'target' => '_blank',
								]
							);
						}
						$output[] = "<li>"
							. $t->getSelfLink()
							. ($t->getLiens()->containsSource("openpolicyfinder") ? ' <img src="/images/liens/openpolicyfinder.png" alt="Open policy finder">' : '')
							. ($row['sherpaPolicyUrl'] ? " <a href={$row['sherpaJournalUrl']} class=label title=\"Politique sur le site d'Open policy finder\">P</a>" : "")
							. $alert
							. "</li>";
					}
					return "<ul>" . join("", $output) . "</ul>";
				}
			],
			[
				'header' => 'actions',
				'type' => 'raw',
				'value' => function(\Politique $p) {
					switch ($p->status) {
					case Politique::STATUS_TOPUBLISH:
						return CHtml::htmlButton(
							'<span class="glyphicon glyphicon-share"></span>',
							[
								'type' => "button",
								'class' => "btn btn-small btn-default policy-publish",
								'title' => "Transmettre la politique à Open policy finder",
								'data-politiqueid' => $p->id,
							]
						);
					case Politique::STATUS_TODELETE:
						return CHtml::htmlButton(
							'<span class="glyphicon glyphicon-trash"></span>',
							[
								'type' => "button",
								'class' => "btn btn-small btn-danger policy-delete",
								'title' => "Supprimer définitivement cette politique inutilisée. Elle ne pourra pas être restaurée.",
								'data-politiqueid' => $p->id,
							]
						);
					default:
						return '';
					}
				}
			],
		],
		'ajaxUpdate' => false,
	]
);

Yii::app()->getClientScript()->registerScript('policy-actions',
	<<<EOJS
	$(document).on('click', 'button.policy-publish', function() {
		let politiqueId = $(this).attr('data-politiqueid');
		let parent = $(this).parent();
		$.ajax({
			method: 'POST',
			url: '/politique/ajax-publish/' + politiqueId + '?multi=1'
		}).then(
			// success
			function(data) {
				console.log(data);
				if (data.status === 200) {
					parent.text(data.message);
				} else {
					alert(data.message);
				}
			},
			// failure
			function() {
				alert("Une erreur interne s'est produite. Merci de contacter les administrateurs du site.");
			}
		);
	});
	$(document).on('click', 'button.policy-delete', function() {
		let politiqueId = $(this).attr('data-politiqueid');
		let parent = $(this).parent();
		if (!confirm("Supprimer cette politique ? La suppression sera ensuite propagée auprès d'Open policy finder")) {
			return false;
		}
		$.ajax({
			method: 'POST',
			url: '/politique/delete/' + politiqueId
		}).then(
			// success
			function(data) {
				if (data.status === 200) {
					parent.text(data.message);
				} else {
					alert(data.message);
				}
			},
			// failure
			function() {
				alert("Une erreur interne s'est produite. Merci de contacter les administrateurs du site.");
			}
		);
	});
	EOJS
);
