<?php

/** @var Controller $this */
/** @var bool $isRolePending Pending role on this publisher? */
/** @var processes\politique\Index $model */

$nbTitres = count($model->titresActuels) + count($model->titresAnciens);

?>
<section id="politiques" style="margin-top: 2em">
	<h2>Politique de publication</h2>
	<?php
	if ($isRolePending && $nbTitres > 1) {
		?>
		<p class="alert alert-info">
			L'équipe de Mir@bel n'a pas encore validé vos droits sur cet éditeur.
			Vous pouvez dès à présent créer une politique pour une revue.
			Après validation de votre rôle, vous pourrez appliquer cette politique à d'autres titres avant de demander sa transmission à Open policy finder.
		<?php
	}
	if (!$model->politiques) {
		echo "<p>Aucune politique n'est encore définie pour cet éditeur.</p>";
	}
	?>
	<?php
	if ($model->politiques) {
		foreach ($model->politiques as $politique) {
			?>
			<div class="politique well">
				<div>
					<strong><?= CHtml::encode($politique->name) ?></strong>
				</div>
				<ul>
					<?php
					$titresAssigned = $politique->getTitresAssigned();
					$titresPending = $politique->getTitresPending();

					if (!$titresAssigned && !$titresPending) {
						echo "<li>Aucune revue associée</li>";
					} else {
						switch (count($titresAssigned)) {
							case 0:
								break;
							case 1:
							case 2:
								foreach ($titresAssigned as $titre) {
									echo '<li class="journal">'
										. $titre->getFullTitle()
										. ' '
										. CHtml::link(
											CHtml::image('/images/logo-mirabel-16px.png', $titre->getFullTitle()),
											$titre->getRevueUrl(),
											['title' => "Voir cette revue dans Mir@bel"]
										)
										. "</li>";
								}
								break;
							default:
								echo "<li><strong>" . count($titresAssigned) . "</strong> revues associées</li>";
						}

						switch (count($titresPending)) {
							case 0:
								break;
							case 1:
							case 2:
								foreach ($titresPending as $titre) {
									echo '<li class="journal unvalidated">'
										. $titre->getFullTitle()
										. ' '
										. CHtml::link(
											CHtml::image('/images/logo-mirabel-16px.png', $titre->getFullTitle()),
											$titre->getRevueUrl(),
											['title' => "Voir cette revue dans Mir@bel"]
										)
										. "</li>";
								}
								break;
							default:
								echo '<li class="unvalidated">' . count($titresPending) . " revues associées</li>";
						}
					}
					?>
				</ul>
				<div>
					<?php
					if (\Yii::app()->user->access()->toPolitique()->update($politique)) {
						echo '<a class="btn btn-sm btn-default" href="' . $this->createUrl('/politique/update', ['id' => $politique->id]) . '">Voir et modifier…</a>';
					}

					if ($nbTitres > 1) {
						if ($isRolePending) {
							echo '<button class="btn btn-sm btn-default disabled" type="button" title="Accessible après validation du compte par Mir@bel">Associer à d\'autres revues…</button>';
						} else {
							echo '<a class="btn btn-sm btn-default" href="' . $this->createUrl('/politique/titres', ['id' => $politique->id]) . '">Associer à d\'autres revues…</a>';
						}
					}

					if ($titresAssigned) {
						echo CHtml::htmlButton(
							'<span class="glyphicon glyphicon-share"></span>',
							[
								'type' => "button",
								'disabled' => !\Yii::app()->user->access()->toPolitique()->publish($politique),
								'class' => "btn btn-small btn-default policy-publish",
								'title' => "Transmettre la politique à Open policy finder",
								'data-politiqueid' => $politique->id,
							]
						);
					}
				?>
				</div>
				<ul style="list-style: none; margin: 2ex 0 0 1ex;">
					<?php
					if ($politique->status === Politique::STATUS_PENDING) {
						echo "<li><em>en attente de validation</em></li>";
					}

					$u = $politique->createur;
					if ($u) {
						$creatorName = CHtml::encode($u->nomComplet);
					} else {
						$creatorName = "(compte supprimé)";
					}
					echo "<li>créée par : $creatorName</li>";

					if (!$isRolePending) {
						$lastUpdate = date('Y-m-d', $politique->lastUpdate);
						echo "<li>dernière modification : $lastUpdate</li>";
					}
					?>
				</ul>
			</div>
			<?php
		}
		echo "</ul>";
	}
	?>
</section>
<?php
Yii::app()->getClientScript()->registerScript('policy-publish',
	<<<EOJS
	$(document).on('click', 'button.policy-publish', function() {
		let politiqueId = $(this).attr('data-politiqueid');
		let parent = $(this).parent();
		$.ajax({
			method: 'POST',
			url: '/politique/ajax-publish/' + politiqueId
		}).then(
			// success
			function(data) {
				console.log(data);
				if (data.status === 200) {
					parent.text(data.message);
				} else if (data.status === 302) {
					window.location = data.location;
				} else {
					alert(data.message);
				}
			},
			// failure
			function() {
				alert("Une erreur interne s'est produite. Merci de contacter les administrateurs du site.");
			}
		);
	});
	EOJS
);
