<?php

/** @var Controller $this */
/** @var Politique[] $policies */
/** @var \models\sherpa\Item $sherpaItem */
/** @var int $titreId */

?>
<select class="policy-assign" data-titreid="<?= $titreId ?>">
	<option>Choisir une politique…</option>
	<?php if ($sherpaItem !== null) { ?>
	<option value="sherpa">Importer depuis Open policy finder</option>
	<?php } ?>
	<?php foreach ($policies as $p) { ?>
	<option value="<?= $p->id ?>"><?= CHtml::encode($p->name) ?></option>
	<?php } ?>
</select>
