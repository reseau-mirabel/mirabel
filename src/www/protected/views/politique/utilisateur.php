<?php

/** @var Controller $this */
/** @var models\forms\UserPolitique $model */

assert($this instanceof PolitiqueController);

$this->pageTitle = "Création de compte";
$this->breadcrumbs = [
	"Politiques des éditeurs",
];

Yii::app()->getClientScript()->registerCssFile('/css/glyphicons.css');
?>
<h1>
	<?= CHtml::encode($this->pageTitle) ?>
</h1>

<div class="info">
	<?= \Cms::getBlock("politique.creation-utilisateur")->toHtml() ?>
</div>

<?php
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'politique-utilisateur-form',
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'enableAjaxValidation' => false,
	]
);
/** @var BootActiveForm $form */
?>

<?= $form->errorSummary($model, null, null, ['encode' => false]) ?>

<fieldset>
	<legend>Profil</legend>
	<?php
	echo $form->textFieldRow($model, 'email', ['class' => 'span6', 'required' => true, 'type' => 'email']);
	echo $form->textFieldRow($model, 'nom');
	echo $form->textFieldRow($model, 'prenom');

	Yii::app()->clientScript->registerCoreScript('jquery');
	Yii::app()->clientScript->registerScript('nomcomplet', '
$("#UserPolitique_nom, #UserPolitique_prenom").on("input", function() {
	$("#UserPolitique_nomComplet").val($("#UserPolitique_prenom").val() + " " + $("#UserPolitique_nom").val());
});
');
	?>
</fieldset>

<fieldset>
	<legend>Éditeur(s)</legend>
	<div>
	<?= \Cms::getBlock("politique.creation-utilisateur.editeurs")->toHtml() ?>
	<?= (new widgets\PolitiqueSelectPublishers($model))->run() ?>
</fieldset>

<fieldset>
	<legend>Commentaire libre</legend>
	<?= $form->textAreaRow($model, 'message', ['class' => 'span6', 'placeholder' => "Commentaire éventuel pour aider l'équipe de Mir@bel à valider votre rôle sur ces éditeurs (maximum de 1000 caractères)."]) ?>
</fieldset>

<?php if (!defined('YII_ENV') || YII_ENV !== 'test') { ?>
<fieldset>
	<legend>Anti-spam</legend>
	<?= $form->hiddenField($model, 'spam1') ?>
	<?= $form->textFieldRow($model, 'spam2') ?>
<?php } ?>

<div class="form-actions">
	<button type="submit" class="btn btn-primary">Créer ce compte</button>
</div>

<?php
$this->endWidget();
