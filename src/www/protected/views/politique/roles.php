<?php

/** @var Controller $this */
/** @var processes\politique\Roles $model */

assert($this instanceof PolitiqueController);

$this->pageTitle = "Rôles de l'éditeur " . $model->editeur->nom;
$this->breadcrumbs = [
	"Politique de publication" => ['/politique/index', 'editeurId' => $model->editeur->id],
	"Rôles",
];

Yii::app()->getClientScript()->registerCssFile('/css/glyphicons.css');
?>
<h1>
	Rôles de l'éditeur <em><?= CHtml::encode($model->editeur->nom) ?></em>
</h1>

<?php
$form = $this->beginWidget(
	'bootstrap.widgets.BootActiveForm',
	[
		'id' => 'politique-roles-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => false,
		'type' => BootActiveForm::TYPE_HORIZONTAL,
		'hints' => Hint::model()->getMessages('TitreEditeur'),
		'htmlOptions' => ['class' => 'well'],
	]
);
/** @var BootActiveForm $form */
?>

<?php
$roleValues = ['' => "indéterminé"] + TitreEditeur::getPossibleRoles();
$counter = (count($model->titres) > 1 ? 1 : 0);
foreach ($model->titres as $titre) {
	$te = $model->roles[$titre->id];
	echo '<fieldset class="' . ($te->isComplete() ? "complete" : "incomplete") . '">';
	echo '<legend style="display: flex">';
	echo '<div style="flex: 1">'
		. CHtml::encode($titre->getFullTitle())
		. '  <span class="toggle glyphicon glyphicon-' . ($te->isComplete() ? "plus" : "minus") . '" aria-hidden="true""></span>'
		. "</div>";
	if ($counter) {
		echo '<span class="badge" style="align-self: center; flex: 0 1;">' . "{$counter}</span> ";
		$counter++;
	}
	echo "</legend>";
	echo '<div class="fields">';
	echo $form->radioButtonListRow($te, "[{$titre->id}]ancien", ['' => "indéterminé", '1' => "ancien éditeur", '0' => "éditeur en cours"], ['uncheckValue' => null]);
	echo $form->radioButtonListRow($te, "[{$titre->id}]commercial", ['' => "indéterminé", '1' => "rôle commercial", '0' => "non"], ['uncheckValue' => null]);
	echo $form->radioButtonListRow($te, "[{$titre->id}]intellectuel", ['' => "indéterminé", '1' => "rôle intellectuel", '0' => "non"], ['uncheckValue' => null]);
	echo $form->dropDownListRow($te, "[{$titre->id}]role", $roleValues);
	echo '</div>';
	echo "</fieldset>";
}
?>

<div class="form-actions">
	<button type="submit" class="btn btn-primary">Enregistrer les changements</button>
</div>

<?php
$this->endWidget();

Yii::app()->getClientScript()->registerScript(
	'politique-roles',
	<<<EOJS
	$("fieldset.complete").each(function() {
		$(".fields", this).hide(0);
	});
	$("form").on('click', '.toggle', function() {
		$(this).closest('fieldset').find(".fields").toggle();
		if ($(this).hasClass('glyphicon-minus')) {
			$(this).removeClass('glyphicon-minus').addClass('glyphicon-plus');
		} else {
			$(this).removeClass('glyphicon-plus').addClass('glyphicon-minus');
		}
	})
	EOJS
);
