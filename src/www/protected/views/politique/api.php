<?php

/** @var Controller $this */
/** @var components\openapi\Specification $api */

use components\openapi\Schema;

$this->pageTitle = $api->info->title;

$markdown = Yii::app()->getComponent('markdown');
assert($markdown instanceof \League\CommonMark\ConverterInterface);
?>

<div id="api-yaml">
	Download the description as
	<?= CHtml::link("OpenApi JSON", ['/files/politique-api/openapi.json']) ?>
	or <?= CHtml::link("YAML", ['/files/politique-api/openapi.yaml']) ?>
</div>

<h1><?= CHtml::encode($this->pageTitle) ?></h1>

<section id="api-info">
	<div class="api-description">
		<?= $markdown->convert($api->info->description)->getContent() ?>
	</div>
	<div>
		<strong>Version</strong>
		<?= CHtml::encode($api->info->version) ?>
	</div>
	<?php
	if (isset($api->info->termsOfService)) {
		?>
		<div class="api-terms">
			<?= $markdown->convert($api->info->termsOfService)->getContent() ?>
		</div>
		<?php
	}
	?>
	<?php
	if (isset($api->info->license)) {
		?>
		<div class="api-license">
			<h2>License of contents</h2>
			<?= CHtml::encode($api->info->license->name) ?> :
			<?= CHtml::link(CHtml::encode($api->info->license->url), $api->info->license->url) ?>
		</div>
		<?php
	}
	?>
	<?php
	if (isset($api->info->contact)) {
		?>
		<div class="api-contact">
			<h2>Contact</h2>
			<ul>
				<li>
					<strong><?= CHtml::encode($api->info->contact->name) ?></strong>
					<?php
					if (isset($api->info->contact->url)) {
						echo "<div>" . CHtml::link(CHtml::encode($api->info->contact->url), $api->info->contact->url) . "</div>";
					}
					?>
					<?php
					if (isset($api->info->contact->email)) {
						echo "<div>" . CHtml::encode($api->info->contact->email) . "</div>";
					}
					?>
			</ul>
		</div>
		<?php
	}
	?>
	<div class="api-format">
		<h2>Format of API responses</h2>
		All responses use the JSON format.
	</div>
</section>

<section class="api" id="api-paths">
	<h2>Available paths</h2>

	<?php
	if (isset($api->security)) {
		?>
		<div class="alert alert-warning" id="api-security">
			API access is controlled by authentication tokens.
			Please request such tokens through the contact page.
		</div>
		<?php
	}
	?>

	<?php
	if ($api->basePath) {
		echo "<p>All paths must be prefixed with <code>" . CHtml::encode($api->basePath) . "</code>.</p>";
	}
	?>
	<ul>
		<?php
		foreach ($api->paths as $path => $methods) {
			foreach ($methods as $method => $details) {
				$htmlMethod =  CHtml::tag('span', ['class' => 'api-http-method'], strtoupper($method));
				echo CHtml::tag(
					'li',
					[],
					CHtml::link(
						CHtml::encode($path) . " " . $htmlMethod,
						'#path-' . $method . str_replace('/', '-', $path)
					)
					. " "
					. CHtml::tag('span', ['class' => 'api-summary'], $details->summary)
				);
			}
		}
		?>
	</ul>
</section>

<section class="api" id="api-details">
	<h2>Detail of actions/paths</h2>

	<?php
	$baseUrl = $api->getBaseUrl();
	foreach ($api->paths as $path => $methods) {
		echo '<h3 class="api-path">' . CHtml::encode($path) . '</h3>';
		foreach ($methods as $method => $details) {
			$pathUrl = $baseUrl . $path; ?>
			<article id="path-<?= CHtml::encode($method . str_replace('/', '-', $path)) ?>">
				<h4><?= CHtml::encode(strtoupper($method) . " " . $path) ?></h4>
				<dl>
					<dt>Titre</dt>
					<dd><?= $details->summary ?></dd>
					<?php if (!empty($details->description)) { ?>
						<dt>Description</dt>
						<dd><?= $markdown->convert($details->description)->getContent() ?></dd>
					<?php } ?>
					<dt>URL</dt>
					<dd><?= CHtml::encode(strtoupper($method) . " " . $pathUrl) ?></dd>
					<?php if (!empty($details->parameters)) { ?>
						<dt>Paramètres</dt>
						<dd class="api-parameters">
							<table class="table table-compact">
								<thead>
									<tr>
										<th>Name</th>
										<th>Description</th>
										<th>Place</th>
										<th>Required?</th>
										<th>Type, format</th>
									</tr>
								</thead>
								<tbody>
									<?php
									foreach ($details->parameters as $parameter) {
										echo "<tr>"
											. CHtml::tag('td', [], CHtml::encode($parameter->name))
											. CHtml::tag('td', [], isset($parameter->description) ? $markdown->convert($parameter->description)->getContent() : '')
											. CHtml::tag('td', [], CHtml::encode($parameter->in))
											. CHtml::tag('td', [], empty($parameter->required) ? "Yes" : "No")
											. CHtml::tag('td', [], CHtml::encode($parameter->schema->type)
												. (isset($parameter->format) ? ', ' . CHtml::encode($parameter->format) : '')
											)
											. "</tr>\n";
									}
									?>
								</tbody>
							</table>
						</dd>
					<?php } ?>
					<?php if (!empty($details->responses)) { ?>
						<dt>Responses</dt>
						<dd class="api-responses">
							<table class="table table-compact">
								<thead>
									<tr>
										<th>HTTP code</th>
										<th>Description</th>
										<th>Schema</th>
									</tr>
								</thead>
								<tbody>
									<?php
									foreach ($details->responses as $code => $response) {
										$content = (array) $response->content;
										echo "<tr>"
											. CHtml::tag('td', [], CHtml::encode($code))
											. CHtml::tag('td', [], $markdown->convert($response->description)->getContent())
											. CHtml::tag('td', [], new Schema($content['application/json']->schema))
											. "</tr>\n";
									}
									?>
								</tbody>
							</table>
						</dd>
					<?php } ?>
				</dl>
				<?php
				if (empty($details->parameters)) {
					echo '<p> '
						. CHtml::link("Try this action", $pathUrl, ['class' => 'btn btn-default', 'target' => '_blank'])
						. '</p>';
				} ?>
			</article>
			<?php
		}
	}
	?>
</section>


<section class="api" id="api-definitions">
	<h2>Data models</h2>

	<?php
	foreach ($api->components->schemas as $name => $definition) {
		echo '<article id="definition-' . CHtml::encode($name) . '">';
		echo '<h3 class="api-definition">' . CHtml::encode($name) . '</h3>';
		if ($definition->type === 'object') {
			if (isset($definition->description)) {
				echo CHtml::tag("div", [], $markdown->convert($definition->description)->getContent());
			} ?>
			<table class="table table-compact">
				<tbody>
					<?php
					foreach ($definition->properties as $propName => $propDetails) {
						echo "<tr>"
							. CHtml::tag('td', [], CHtml::encode($propName))
							. CHtml::tag('td', [], new Schema($propDetails))
							. "</tr>\n"
							;
					} ?>
				</tbody>
			</table>
			<?php
		}
		echo "</article>\n";
	}
	?>
</section>
