<?php

/** @var Controller $this */
/** @var bool $isRolePending Pending role on this publisher? */
/** @var processes\politique\Index $model */

?>
<section id="titres" style="margin-top: 2em">
	<h2>Revues</h2>
	<?= $this->renderPartial('_contact', ['editeur' => $model->editeur]) ?>

	<?php
	if (!$model->titresActuels && !$model->titresAnciens) {
		echo "<p class=\"alert alert-danger\">Aucun titre n'est associé à cet éditeur.</p>";
	} else {
		if ($model->titresActuels) {
			?>
			<h3>Titres actuels</h3>
			<?= $this->renderPartial('_index-titres-list', ['editeur' => $model->editeur, 'titres' => $model->titresActuels, 'isRolePending' => $isRolePending]) ?>
			<?php
		}

		if ($model->titresAnciens) {
			?>
			<h3>Titres anciens</h3>
			<?= $this->renderPartial('_index-titres-list', ['editeur' => $model->editeur, 'titres' => $model->titresAnciens, 'isRolePending' => $isRolePending]) ?>
			<?php
		}
	}
	?>
</section>
