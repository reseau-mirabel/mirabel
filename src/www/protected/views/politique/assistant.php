<?php

/** @var PolitiqueController $this */
/** @var ?Politique $politique */
/** @var ?Titre $titre */
/** @var Editeur $editeur */

use components\SqlHelper;

assert($this instanceof PolitiqueController);

$this->pageTitle = "Politique de publication";
$this->breadcrumbs = [
	"Politiques de publication" => ['/politique/index', 'editeurId' => $editeur->id],
	"Définir la politique",
];
?>
<h1>
	<?= CHtml::encode($this->pageTitle) ?>
</h1>

<?php
if (!$titre) {
	$titres = SqlHelper::sqlToPairs("SELECT t.id, t.titre FROM Titre t JOIN Titre_Editeur te ON te.titreId = t.id WHERE te.editeurId = {$editeur->id} ORDER BY titre");
	?>
	<h2>Sélection du titre</h2>
	<p>
		Sélectionnez le titre de <em><?= CHtml::encode($editeur->nom) ?></em> auquel s'appliquera cette politique.
		Vous pourrez plus tard étendre son application à d'autres titres.
	</p>
	<?php
	echo CHtml::dropDownList("titre", "", $titres, ['empty' => '', 'onchange' => 'window.location = window.location.href + "&titreId=" + this.value']);
	return;
}
?>
<h2>Revue <?= $titre->getSelfLink() ?></h2>

<?php
if ($politique) {
	echo '<p class="alert alert-danger">
		Une politique éditoriale est déjà attribuée à ce titre.
		Elle sera remplacée par la politique produite par cet assistant.
	</p>';
}
?>

<div class="form-horizontal">
	<?= (new \widgets\PolitiqueAssistant(
		$editeur->id,
		$titre->id,
		['/politique/index', 'editeurId' => $editeur->id])
	)->run() ?>
</div>
