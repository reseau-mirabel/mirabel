<?php

declare(strict_types=1);

namespace components;

use Exception;

/**
 * Store files in separate containers.
 * API inspired by Yii2's CacheInterface.
 *
 * $store = new FileStore("myname");
 * $store->set("mykey.csv", $pathToFile);
 * file_get_content($store->get("mykey.csv");
 * $store->flushOlderThan(86400);
 */
class FileStore
{
	private const BASE_DIR = DATA_DIR . '/filestore';

	private const PERM_DIR = 0775;

	private const PERM_FILE = 0664;

	private string $basePath = "";

	private string $storeName;

	public function __construct(string $storeName)
	{
		if (!preg_match('/^[\w-]+$/', $storeName)) {
			throw new Exception("Invalid store name: only alphanum caracters are allowed.");
		}
		$this->storeName = $storeName;
	}

	public function delete(string $key): bool
	{
		$inStorePath = $this->buildPath($key);
		if (!file_exists($inStorePath)) {
			return true;
		}
		return unlink($inStorePath);
	}

	public function exists(string $key): bool
	{
		$inStorePath = $this->buildPath($key);
		return file_exists($inStorePath);
	}

	public function flush(): void
	{
		$this->flushOlderThan(0);
	}

	public function flushOlderThan(int $seconds): void
	{
		$now = time();
		foreach (glob($this->buildPath('*')) as $filename) {
			if ($seconds === 0 || ($now - filemtime($filename)) > $seconds) {
				unlink($filename);
			}
		}
	}

	/**
	 * Return the path to the file named "$key". If not found, the path is "".
	 */
	public function get(string $key): string
	{
		$inStorePath = $this->buildPath($key);
		if (!file_exists($inStorePath)) {
			return "";
		}
		if (!is_readable($inStorePath)) {
			throw new Exception("File not readable: $inStorePath");
		}
		return $inStorePath;
	}

	/**
	 * The path to the directory that hods the files.
	 */
	public function getBasePath(): string
	{
		if ($this->basePath === '') {
			$this->basePath = self::createDirectory($this->storeName, "");
		}
		return $this->basePath;
	}

	/**
	 * Move a file into the store, under the name "$key".
	 */
	public function set(string $key, string $path, bool $copy = false): bool
	{
		if (!is_readable($path)) {
			throw new Exception("File not found or not readble: $path");
		}
		$dest = $this->buildPath($key);
		if (file_exists($dest)) {
			unlink($dest);
		}
		if ($copy) {
			$success = copy($path, $dest);
		} else {
			$success = rename($path, $dest);
		}
		if ($success) {
			@chmod($dest, self::PERM_FILE | fileperms($dest));
		}
		return $success;
	}

	/**
	 * Create a file in the store, under the name "$key".
	 */
	public function setByContent(string $key, string $content): bool
	{
		$filePath = $this->buildPath($key);
		$success = file_put_contents($filePath, $content) !== false;
		@chmod($filePath, self::PERM_FILE);
		return $success;
	}

	/**
	 * The root path is where the store will create its directory.
	 */
	public function setRootPath(string $path): void
	{
		if (!is_dir($path)) {
			throw new Exception("The directory '{$path}' must exist before becoming the root of the FileStore.");
		}
		$this->basePath = self::createDirectory($this->storeName, $path);
	}

	/**
	 * Returns the path to the filestore (a sub-directory of the root path).
	 */
	private function buildPath(string $key): string
	{
		if ($this->basePath === '') {
			$this->basePath = self::createDirectory($this->storeName, "");
		}
		return "{$this->basePath}/$key";
	}

	private static function createDirectory(string $storeName, string $rootPath): string
	{
		$parentPath = self::getRootDir($rootPath);
		$storePath = "$parentPath/$storeName";
		if (!is_dir($storePath)) {
			if (!@mkdir($storePath, self::PERM_DIR)) {
				throw new Exception("Cannot create the directory '{$storePath}'");
			}
		}
		if (!is_writable($storePath)) {
			throw new Exception("This directory must be writable: '{$storePath}'");
		}
		return $storePath;
	}

	private static function getRootDir(string $path): string
	{
		// PHP's mkdir() is flawed:
		// on a recursive mkdir(), the permissions are not set on the parent directories.
		// So we have to use mkdir() on the 2 levels.
		if ($path) {
			$rootPath = $path;
		} else {
			$rootPath = self::BASE_DIR;
		}
		if (!is_dir($rootPath)) {
			if (!@mkdir($rootPath, self::PERM_DIR, true)) {
				throw new Exception("Cannot create the directory '{$rootPath}'");
			}
		}
		return $rootPath;
	}
}
