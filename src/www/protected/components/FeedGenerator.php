<?php

namespace components;

use ezcFeed;
use Config;
use Yii;
use processes\intervention\Formatter;

/**
 * Doc: http://zetacomponents.org/documentation/trunk/Feed/tutorial.html
 */
class FeedGenerator
{
	public $maxItems = 50;

	public $privateFeed = false;

	public $ownUrl = '';

	public $feedTitle = [];

	/** @var ezcFeed */
	private $feed;

	private $type = 'atom';

	private $baseUrl;

	/** @var string */
	private $sql;

	private $where = [""];

	/**
	 * Constructor. Initializes a few properties.
	 */
	public function __construct(string $type)
	{
		if (in_array($type, ['atom', 'rss1', 'rss2'])) {
			$this->type = $type;
		}

		$this->baseUrl = Yii::app()->getBaseUrl(true);
		$this->ownUrl = empty($_SERVER['REQUEST_URI']) ?
			Yii::app()->params->itemAt('baseUrl') . '/feeds.php'
			: "https://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		$this->sql = <<<EOSQL
			SELECT
				i.id, statut, description AS title, '' AS description, hdateProp, hdateVal,
				revueId, ressourceId, editeurId, contenuJson, action
			FROM Intervention i
			WHERE
				description <> ''
				AND (revueId IS NOT NULL OR ressourceId IS NOT NULL OR editeurId IS NOT NULL)
			EOSQL;
		$this->maxItems = Config::read('rss.limite.interventions');
		if (!$this->maxItems) {
			$this->maxItems = 50;
		}

		$this->initFeed();
	}

	/**
	 * Tri-state value: null => show all, true => only validated, false => only not validated
	 *
	 * @param bool|null $state
	 */
	public function showValidatedOnly(?bool $state = true): void
	{
		if ($state === null) {
			$this->where[0] = '';
		} else {
			$this->where[0] = ($state ? "hdateVal  > 0 AND statut = 'accepté'" : "statut = 'attente'");
		}
	}

	/**
	 * Adds a criteria (revue|ressource|editeur) to the search of items.
	 *
	 * @param string $name (revue|ressource|editeur)Id
	 * @param string|int $id Integer ID or string expr ("IS NULL")
	 */
	public function addCriteria(string $name, $id): void
	{
		if (is_int($id) || ctype_digit((string) $id)) {
			$this->where[] = $name . " = " . $id;
			$name = preg_replace('/Id$/', '', ucfirst($name));
			$object = call_user_func([$name, 'model'])->findByPk($id);
			if ($object) {
				$this->feedTitle[] = $name . " " . $object->nom;
			}
		} else {
			$this->where[] = $name . " " . $id;
		}
	}

	/**
	 * Add a criteria on the "Partenaire" that has "Suivi" on the objects.
	 *
	 * @param int $pId
	 */
	public function addFilterMonitored(int $pId): void
	{
		$pId = (int) $pId;
		$this->sql = <<<EOSQL
			SELECT
				i.id, statut, description AS title, '' AS description, hdateProp, hdateVal,
				revueId, ressourceId, editeurId, contenuJson
			FROM Intervention i
				JOIN Suivi s ON (
					s.partenaireId = $pId
					AND (
						(s.cibleId=i.revueId AND s.cible='Revue')
						OR (s.cibleId=i.ressourceId AND s.cible='Ressource')
						OR (s.cibleId=i.editeurId AND s.cible='Editeur')
					)
				)
			WHERE
				description <> ''
				AND (revueId IS NOT NULL OR ressourceId IS NOT NULL OR editeurId IS NOT NULL)
			EOSQL;
		$name = htmlspecialchars((string) Yii::app()->db->createCommand("SELECT nom FROM Partenaire")->queryScalar());
		$this->feedTitle[] = "[P] $name";
		$this->feed->description .= " Ce flux ne contient que les objets suivis par le partenaire $name.";
	}

	/**
	 * Outputs the result (HTTP header included).
	 */
	public function run(): void
	{
		$this->addItems();
		if ($this->feedTitle) {
			$this->feed->title->text .= " — " . htmlspecialchars(join(" / ", $this->feedTitle));
		}
		$processingInstructions = [
			[
				'xml-stylesheet',
				'type="text/xsl" href="' . $this->baseUrl . '/xslt/' . $this->type . '.xsl"',
			],
		];
		$xml = $this->feed->generate($this->type, $processingInstructions);
		header('Content-Type: text/xml; charset=utf-8');
		echo $xml;
	}

	/**
	 * Generates a feed for an object to be deleted.
	 *
	 * @return string XML
	 */
	public function generateDeletionFeed(string $modelName): string
	{
		$this->addDeletionItem($modelName);
		$this->addItems();
		return $this->feed->generate($this->type);
	}

	/**
	 * Writes in files the XML feeds of an object.
	 *
	 * @param string $modelName Revue|Ressource|Editeur.
	 * @param int $id
	 */
	public static function saveFeedsToFile(string $modelName, int $id): void
	{
		$path = DATA_DIR . "/cache/feeds/$modelName";
		if (!is_dir($path)) {
			mkdir($path, 0777, true);
		}
		foreach (['rss1', 'rss2', 'atom'] as $type) {
			$gen = new FeedGenerator($type);
			$gen->showValidatedOnly(true);
			$gen->addCriteria(strtolower($modelName) . 'Id', $id);
			$xml = $gen->generateDeletionFeed($modelName);
			$filename = sprintf('%s/%06d_%s.xml', $path, $id, $type);
			file_put_contents($filename, $xml);
		}
	}

	/**
	 * Deletes the files where the XML feeds were written.
	 *
	 * @param string $modelName Revue|Ressource|Editeur.
	 * @param int $id
	 * @return bool Success?
	 */
	public static function removeFeedsFile(string $modelName, int $id): bool
	{
		$path = DATA_DIR . "/cache/feeds/$modelName";
		if (!is_dir($path)) {
			return false;
		}
		foreach (['rss1', 'rss2', 'atom'] as $type) {
			$file = sprintf('%s/%06d_%s.xml', $path, $id, $type);
			if (file_exists($file)) {
				unlink($file);
			}
		}
		return true;
	}

	/**
	 * Outputs the feed from the XML file that matches (modelName, id) IF produced today.
	 *
	 * @param string $modelName Revue|Ressource|Editeur.
	 * @param int $id
	 */
	public function findFeedInFiles(string $modelName, int $id): void
	{
		$tableName = ucfirst($modelName);
		$path = DATA_DIR . "/cache/feeds/$tableName" . sprintf('/%06d_%s.xml', $id, $this->type);
		if (!file_exists($path) || filemtime($path) < mktime(0, 0, 0)) {
			return;
		}
		header('Content-Type: application/' . ($this->type == 'atom' ? 'atom' : 'rss') . '+xml; charset=utf-8');
		readfile($path);
		exit();
	}

	/* ******** private functions ********* */

	private function initFeed(): void
	{
		$this->feedTitle = [];
		$this->feed = new ezcFeed();
		$this->feed->title = Yii::app()->name;
		$this->feed->description = "Suivi des mises à jour de Mir@bel, avec les {$this->maxItems} dernières modifications.";
		$this->feed->published = time();
		$this->feed->updated = time();
		if ($this->type == 'atom') {
			$this->feed->id = htmlspecialchars($this->ownUrl);
		} else {
			$this->feed->id = $this->ownUrl;
		}
		$this->feed->language = 'fr';

		$author = $this->feed->add('author');
		$author->name = htmlspecialchars(Yii::app()->name);

		$link = $this->feed->add('link');
		if ($this->type == 'atom') {
			$link->href = $this->ownUrl;
			$link->rel = 'self';
		} else {
			$link->href = htmlspecialchars($this->ownUrl);
		}
	}

	/**
	 * Adds items to the feed using the $sql property.
	 * Each row must have the keys: id, title, description, updatetime, (revue|ressource|editeur)Id.
	 */
	private function addItems(): void
	{
		$where = join(' AND ', array_filter($this->where));
		$sql = $this->sql . ($where ? " AND $where" : '')
			. " ORDER BY hdateVal DESC, hdateProp DESC LIMIT " . (int) $this->maxItems;
		$result = Yii::app()->db->createCommand($sql)->query();
		if ($result->rowCount === 0) {
			return;
		}
		$formatter = new Formatter();
		$formatter->showRestrictedInfo = $this->privateFeed;
		foreach ($result as $row) {
			$item = $this->feed->add('item');
			/** @var \ezcFeedEntryElement $item */
			$item->id = sprintf('%s/intervention/%d', $this->baseUrl, $row['id']);
			$description = $item->add('description');
			$description->type = 'html';
			switch ($row['statut']) {
				case 'attente':
					$item->title = "[?] ";
					$description->text = "Cette proposition est en attente de validation. ";
					break;
				case 'refusé':
					$item->title = "[!] ";
					$description->text = "Cette proposition a été refusée. ";
					break;
				default:
					$item->title = "";
					if ($this->privateFeed) {
						if ($row['hdateProp'] == $row['hdateVal']) {
							$description->text = "Modification directe. ";
						} else {
							$description->text = sprintf(
								"Modification proposée le %s et validée le %s. ",
								\Yii::app()->dateFormatter->format("d MMMM yyyy 'à' HH:mm", $row['hdateProp']),
								\Yii::app()->dateFormatter->format("d MMMM yyyy 'à' HH:mm", $row['hdateVal'])
							);
						}
					}
			}
			$item->title .= htmlspecialchars($row['title']);
			if ($row['description']) {
				$description->text .= htmlspecialchars('<p>' . $row['description'] . '</p>');
			}
			if ($row['contenuJson']) {
				$contenu = json_decode($row['contenuJson'], true);
				$details = $formatter->formatContents($contenu['content'], $row['action'] ?? '');
				if ($details) {
					$description->text .= $details;
				} elseif ($row['action'] === 'revue-I') {
					// empty "catégorisation" message => remove it
					$this->feed->pop("item");
					continue;
				} elseif ($row['action'] === 'service-U') {
					$this->feed->pop("item");
					continue;
				}
			}
			$item->published = $row['hdateVal'];
			$item->updated = $row['hdateVal'];
			// add the first link among 3 candidates
			foreach (['revue', 'ressource', 'editeur'] as $o) {
				if (!empty($row[$o . 'Id'])) {
					$link = $item->add('link');
					$link->href = sprintf('%s/%s/%d', $this->baseUrl, $o, $row[$o . 'Id']);
					$link->rel = "alternate";
					break;
				}
			}
		}
	}

	/**
	 * Add an item to the feed for a deleted object.
	 *
	 * PHP types are wrongly declared in zeta-components' code
	 * @psalm-suppress InvalidPropertyAssignmentValue
	 */
	private function addDeletionItem(string $modelName): void
	{
		$row = Yii::app()->db->createCommand("SHOW TABLE STATUS LIKE 'Intervention'")->queryRow(true);
		$url = $this->baseUrl . '/' . strtolower($modelName);
		unset($row);
		/** @var \ezcFeedEntryElement */
		$item = $this->feed->add('item');
		$item->id = $url;
		$msg = 'Suppression de ' . ($modelName == 'Editeur' ? "l'éditeur" : "la " . strtolower($modelName))
			. " dans Mir@bel";
		$description = $item->add('description');
		$description->text = $msg;
		$item->title = $msg;
		$item->published = (int) $_SERVER['REQUEST_TIME'];
		$item->updated = (int) $_SERVER['REQUEST_TIME'];
		$link = $item->add('link');
		$link->href = $url;
		$link->rel = "alternate";
	}
}
