<?php

/**
 * @codeCoverageIgnore
 */
class ProfileLogRoute extends CProfileLogRoute
{
	/**
	 * Displays the log messages.
	 *
	 * @param array $logs list of log messages
	 * @return void
	 */
	public function processLogs($logs)
	{
		if (!self::isHtmlResponse()) {
			return;
		}
		parent::processLogs($logs);
	}

	private static function isHtmlResponse(): bool
	{
		foreach (headers_list() as $header) {
			if (strncasecmp($header, "Content-Type:", 13) === 0) {
				return strpos($header, "text/html", 13) !== false;
			}
		}
		return false;
	}
}
