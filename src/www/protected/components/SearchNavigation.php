<?php

class SearchNavigation extends CComponent
{
	/**
	 * @var array
	 */
	private $search;

	private $hash = '';

	/**
	 * @var ?int
	 */
	private $rank;

	/**
	 * @param string $action controller/action
	 * @param ?\CPagination $pagination
	 */
	public function save(string $action, array $criteria, string $keyName, array $results, $pagination = null): string
	{
		foreach ($results as $r) {
			if (!is_object($r)) {
				throw new \Exception("SearchNavigation::save() expects results to be an array of objects.");
			}
			$r->{$keyName} = (int) $r->{$keyName};
		}

		$previousPage = '';
		$nextPage = '';
		if ($pagination) {
			$pageRank = $pagination->getCurrentPage(false); // web param 'page' is $pageRank+1
			if ($pageRank > 0) {
				$previousPage = Yii::app()->createUrl(
					$action,
					array_merge($criteria, [$pagination->pageVar => $pageRank])
				);
			}
			$maxPageRank = $pagination->getPageCount() - 1;
			if ($pageRank < $maxPageRank) {
				$nextPage = Yii::app()->createUrl(
					$action,
					array_merge($criteria, [$pagination->pageVar => $pageRank + 2])
				);
			}
		}

		$actionParameters = array_filter(
			array_map(
				function ($x) {
					if (is_array($x)) {
						return array_filter($x);
					}
					return $x;
				},
				$criteria
			)
		);
		if ($pagination && $pagination->getCurrentPage(false) > 0) {
			$actionParameters[$pagination->pageVar] = $pagination->getCurrentPage(false) + 1;
		}

		$this->search = [
			'action' => $action,
			'criteria' => $actionParameters,
			'keyname' => $keyName,
			'results' => $results,
			'previousPage' => $previousPage,
			'nextPage' => $nextPage,
		];
		$hash = $this->buildHash($this->search);
		Yii::app()->session->add($hash, $this->search);
		$lasts = Yii::app()->session->get('last_searches', []);
		if (!in_array($hash, $lasts)) {
			$lasts[] = $hash;
			Yii::app()->session->add('last_searches', $lasts);
		}
		$this->hash = $hash;
		return $hash;
	}

	public function loadByHash(string $hash): \SearchNavigation
	{
		$parts = explode('-', $hash);
		$this->hash = $parts[0];
		if (isset($parts[1])) {
			$this->rank = (int) $parts[1];
		} else {
			$this->rank = null;
		}
		$this->search = Yii::app()->session->get($this->hash);
		return $this;
	}

	public function isEmpty(): bool
	{
		return empty($this->search);
	}

	/**
	 * @param int $current ID
	 * @return mixed Search result, as saved.
	 */
	public function getNextResult($current = null)
	{
		if (isset($this->rank)) {
			if (isset($this->search['results'][$this->rank + 1])) {
				return $this->search['results'][$this->rank + 1];
			}
			return null;
		}
		return $this->getNeighbours($current)[1];
	}

	/**
	 * @param int $current ID
	 * @return mixed Search result, as saved.
	 */
	public function getPreviousResult($current = null)
	{
		if (isset($this->rank)) {
			if ($this->rank > 0 && isset($this->search['results'][$this->rank - 1])) {
				return $this->search['results'][$this->rank - 1];
			}
			return null;
		}
		return $this->getNeighbours($current)[0];
	}

	/**
	 * @param int $id
	 * @return array [prev, next]
	 */
	public function getNeighbours($id = 0): array
	{
		if (isset($this->rank)) {
			return [$this->getPreviousResult(), $this->getNextResult()];
		}
		if (!$id) {
			return [null, null];
		}
		$found = false;
		$prev = null;
		$next = null;
		$keyname = $this->search['keyname'] ?? '';
		if (!$keyname) {
			return [null, null];
		}
		$results = $this->search['results'];
		foreach ($results as $r) {
			if ($found) {
				$next = $r;
				break;
			}
			if ((int) $r->{$keyname} === $id) {
				$found = true;
				continue;
			}
			$prev = $r;
		}
		if (!$found) {
			return [null, null];
		}
		return [$prev, $next];
	}

	public function getPreviousPage()
	{
		if ($this->getPreviousResult() === null) {
			return $this->search['previousPage'];
		}
		return null;
	}

	public function getNextPage()
	{
		if ($this->getNextResult() === null) {
			return $this->search['nextPage'];
		}
		return null;
	}

	public function getHash(?int $pos = null): string
	{
		if (isset($pos)) {
			return $this->hash . "-" . $pos;
		}
		return $this->hash;
	}

	public function getNextHash(): string
	{
		if (isset($this->rank)) {
			return $this->hash . "-" . ($this->rank + 1);
		}
		return $this->hash;
	}

	public function getPreviousHash(): string
	{
		if ($this->rank > 0) {
			return $this->hash . "-" . ($this->rank - 1);
		}
		return $this->hash;
	}

	/**
	 * @param string $anchor e.g. "id-something" (with or without "#")
	 */
	public function getSearchUrl(string $anchor = ""): array
	{
		$params = $this->search['criteria'];
		array_unshift($params, $this->search['action']);
		if ($anchor) {
			$params['#'] = ltrim($anchor, "#");
		}
		return $params;
	}

	private function buildHash(array $search): string
	{
		return base_convert(
			hash('crc32b', Yii::app()->name . ' search ' . $search['keyname'] . serialize($search['criteria'])),
			16,
			36
		);
	}
}
