<?php

namespace components;

use models\validators\DateValidator;

class DateTimeHelper
{
	public static function diffToRelativeFr(\DateTimeInterface $from, \DateTimeInterface $to): string
	{
		$i = $from->diff($to);
		$prefix = ($i->invert ? "il y a" : "dans");
		if ($i->days > 1) {
			return "$prefix {$i->days} jours";
		}
		if ($i->d === 0 && $i->h < 10) {
			if ($i->h === 0) {
				return "$prefix {$i->i} minutes";
			}
			return "$prefix {$i->h} heures";
		}
		if ($i->invert) {
			$lastMidnight = \DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $from->format('Y-m-d') . ' 00:00:00');
			if ($to < $lastMidnight) {
				if ($to->diff($lastMidnight)->days > 0) {
					return "il y a 2 jours";
				}
				return "hier à " . $to->format("H:i");
			}
			return "aujourd'hui à " . $to->format("H:i");
		}
		$tonight = \DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $from->format('Y-m-d') . ' 23:59:59');
		if ($to > $tonight) {
			if ($tonight->diff($to)->days > 0) {
				return "dans 2 jours";
			}
			return "demain à " . $to->format("H:i");
		}
		return "aujourd'hui à " . $to->format("H:i");
	}

	/**
	 * Return a French description to the time interval between now and $t.
	 */
	public static function toRelativeFr(\DateTimeInterface $t): string
	{
		return self::diffToRelativeFr(new \DateTimeImmutable, $t);
	}

	/**
	 * Read a text like "2007 2008-06" and return timestamps like [TS(2007-01-01), TS(2008-06-30)].
	 *
	 * @param string|null $range
	 * @return array|null
	 */
	public static function parseDateInterval(?string $range): ?array
	{
		$dates = self::parseIntervalIntoDates($range ?? '');
		if ($dates === null) {
			return null;
		}
		if (!$dates) {
			return [];
		}
		return [
			$dates[0] ? DateValidator::convertdateToTs($dates[0], false) : 0,
			$dates[1] ? DateValidator::convertdateToTs($dates[1], true) : 0,
		];
	}

	/**
	 * @return ?array Null on error
	 */
	private static function parseIntervalIntoDates(string $range): ?array
	{
		$interval = trim($range);
		if ($interval === '') {
			return [];
		}
		$firstChar = $interval[0];
		if ($firstChar === '<' || $firstChar === '-') {
			$date = self::parseDate(preg_replace('/^[<-]\s*/', '', $interval));
			if ($date) {
				return ['', $date];
			}
			return null;
		}
		if ($firstChar === '>' || $interval[strlen($interval) - 1] === '-') {
			$date = self::parseDate(preg_replace(['/^>\s*/', '/\s*-$/'], '', $interval));
			if ($date) {
				return [$date, ''];
			}
			return null;
		}
		if (strpos($interval, " ") !== false) {
			$split = preg_split('/\s+/', $interval, 2);
			$start = self::parseDate($split[0]);
			$end = self::parseDate($split[1]);
			if ($start && $end) {
				return [$start, $end];
			}
			return null;
		}
		$date = self::parseDate($interval);
		if ($date) {
			return [$date, $date];
		}
		return null;
	}

	private static function parseDate(string $date): string
	{
		$m = [];
		if (preg_match('/^(\d{4}(?:-\d\d){0,2})$/', $date, $m)) {
			return $m[1];
		}
		if (preg_match('#^(\d\d)/(\d\d)/(\d{4})$#', $date, $m)) {
			return "{$m[3]}-{$m[2]}-{$m[1]}";
		}
		if (preg_match('#^(\d\d)/(\d{4})$#', $date, $m)) {
			return "{$m[2]}-{$m[1]}";
		}
		return '';
	}
}
