<?php

namespace components;

class Csv
{
	public static function guessCsvFieldDelimiter(string $file): string
	{
		$chars = [ord("\t"), ord(";"), ord(",")];
		$lastSeparators = [];

		$position = 0;
		$fh = fopen($file, 'r');
		while ($position < 10) {
			$line = fgets($fh);
			if ($line === false) {
				if (count($lastSeparators) > 2) {
					break;
				}
				throw new \Exception("EOF sans trouver de séparateur CSV/TSV");
			}
			if (trim($line) === '') {
				continue; // skip an empty line
			}
			$counts = count_chars($line, 1);
			$separators = [
				$counts[$chars[0]] ?? 0,
				$counts[$chars[1]] ?? 0,
				$counts[$chars[2]] ?? 0,
			];
			if ($position === 0) {
				$lastSeparators = $separators;
			} else {
				if ($lastSeparators[0] > 0
					&& $lastSeparators[0] === $separators[0]
					&& $lastSeparators[1] !== $separators[1]
					&& $lastSeparators[2] !== $separators[2]
				) {
					fclose($fh);
					return "\t";
				}
				if ($lastSeparators[1] > 0
					&& $lastSeparators[0] !== $separators[0]
					&& $lastSeparators[1] === $separators[1]
					&& $lastSeparators[2] !== $separators[2]
				) {
					fclose($fh);
					return ";";
				}
				if ($lastSeparators[2] > 0
					&& $lastSeparators[0] !== $separators[0]
					&& $lastSeparators[1] !== $separators[1]
					&& $lastSeparators[2] === $separators[2]
				) {
					fclose($fh);
					return ",";
				}
			}
			$position++;
		}
		fclose($fh);
		if ($lastSeparators[0] > 0) {
			return "\t";
		}
		if ($lastSeparators[1] > 0) {
			return ";";
		}
		if ($lastSeparators[2] > 0) {
			return ",";
		}
		throw new \Exception("Aucun séparateur trouvé dans le CSV/TSV");
	}
}
