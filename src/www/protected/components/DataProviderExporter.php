<?php

namespace components;

use CDataProvider;
use CDataColumn;

/**
 * Export the content of a DataProvider into CSV.
 */
class DataProviderExporter
{
	private CDataProvider $datap;

	/**
	 * @var CDataColumn[]
	 */
	private array $columns = [];

	public function __construct(CDataProvider $dataProvider, array $columns)
	{
		$this->datap = $dataProvider;
		foreach ($columns as $c) {
			if (is_string($c)) {
				$column = self::createDataColumnFromString($c);
				if ($column !== null) {
					$this->columns[] = $column;
				}
			} elseif ($c instanceof CDataColumn) {
				$this->columns[] = $c;
			} elseif (is_array($c)) {
				if (isset($c['class']) && $c['class'] !== 'CDataColumn') {
					continue;
				}
				if (!isset($c['class'])) {
					$c['class'] = 'CDataColumn';
				}
				$this->columns[] = \Yii::createComponent($c, null);
			}
		}
	}

	/**
	 * @param false|resource $stream
	 */
	public function writeCsv($stream): void
	{
		fputcsv($stream, $this->getHeaders(), ';', '"', '\\');
		foreach ($this->datap->getData() as $row) {
			$rowCounter = 1;
			$toExport = [];
			foreach ($this->columns as $c) {
				$toExport[] = self::evalRow($row, $c, $rowCounter);
				$rowCounter++;
			}
			fputcsv($stream, $toExport, ';', '"', '\\');
		}
	}

	/**
	 * @psalm-suppress NullArgument
	 */
	private static function createDataColumnFromString(string $str): ?\CDataColumn
	{
		$matches = [];
		if (!preg_match('/^([\w\.]+)(:(\w*))?(:(.*))?$/', $str, $matches)) {
			return null;
		}
		$column = new CDataColumn(null);
		$column->name = $matches[1];
		if (!empty($matches[3])) {
			$column->type = $matches[3];
		}
		if (!empty($matches[5])) {
			$column->header = $matches[5];
		}
		return $column;
	}

	private function getHeaders(): array
	{
		$toExport = [];
		foreach ($this->columns as $c) {
			if ($c->header) {
				$header = $c->header;
			} elseif ($c->name !== null) {
				if (isset($this->datap->model)) {
					$header = $this->datap->model->getAttributeLabel($c->name);
				} else {
					$header = $c->name;
				}
			} else {
				$header = "...";
			}
			$toExport[] = strip_tags($header);
		}
		return $toExport;
	}

	private function evalRow($data, CDataColumn $c, int $rowCounter)
	{
		if ($c->value !== null) {
			$value = strip_tags($c->evaluateExpression($c->value, ['data' => $data, 'row' => $rowCounter]));
		} elseif ($c->name !== null) {
			if (is_array($data)) {
				$value = $data[$c->name];
			} else {
				$value = $data->{$c->name};
			}
		} else {
			$value = "";
		}
		if ($value && $c->type !== 'html' && $c->type !== 'text' && $c->type !== 'ntext') {
			$value = \Yii::app()->format->format($value, $c->type);
		}
		return $value ?? "";
	}
}
