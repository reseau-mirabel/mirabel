<?php

use components\access\Control;

/**
 * Yii::app()->user will return an instance of this class for the current user.
 *
 * Persistent properties are set by 2 ways:
 * 1. At login, by `UserIdentity::setState('zzz', $value).
 *    When the login is successful, CWebUser stores this IUserIdentity state.
 * 2. At any time, by `Yii::app()->user->setstate('zzz', $value)`.
 *
 * To fetch a value from this persistent state:
 *     Yii::app()->user->getState('zzz')
 * or
 *     Yii::app()->user->zzz // NOT RECOMMENDED, because it's hard to trace
 *
 * States are stored in the PHP session of the current user.
 *
 * @property ?int $partenaireId Partenaire.id related to the authenticated User
 * @property string $partenaireType
 * @property string $login
 * @property bool $actif
 * @property int $derConnexion timestamp
 * @property int $hdateCreation timestamp
 * @property int $hdateModif timestamp
 * @property bool $permAdmin
 * @property bool $permImport
 * @property bool $permPartenaire
 * @property bool $permIndexation
 * @property bool $suiviEditeurs
 * @property bool $suiviNonSuivi
 * @property array $suivi
 * @property ?int $instituteId Partenaire.id chosen by the user (with the footer dropdown list)
 * @property string $instituteName
 * @property string $instituteShortname
 */
class WebUser extends CWebUser
{
	private ?Control $accessControl = null;

	/**
	 * @var Partenaire|null The *selected* Partenaire, not always related to the User record.
	 */
	private ?Partenaire $institute = null;

	/**
	 * Called by the constructor, when Yii::app()->user is used for the first time in the request.
	 */
	public function init()
	{
		parent::init(); // Opens the PHP session, so that $this->getState() will answer.

		if (Yii::app() instanceof \CWebApplication) {
			$instituteCookie = Yii::app()->request->cookies->itemAt('institute');
			if ($instituteCookie && preg_match('/^set-(\d+)$/', $instituteCookie, $m)) {
				// The user has just chosen an institute with a fresh cookie.
				$this->setInstituteById((int) $m[1]);
				Yii::app()->request->cookies->remove('institute');
				setcookie('institute', '', time() - 3600);
			} elseif ($this->getState('instituteId') === null) {
				// The user has no institute, so we will guess one (or none).
				$id = isset($_SERVER['REMOTE_ADDR']) ? Partenaire::findInstituteByIp($_SERVER['REMOTE_ADDR']) : 0;
				$this->setInstituteById($id);
				if ($id > 0) {
					$this->setState('instituteByIp', true);
				}
			} // else the user already has an institute, or has chosen not to have one.
		}

		if (!$this->hasState('partenaireType')) {
			$this->setState('partenaireType', '');
		}
	}

	public function access(): Control
	{
		if ($this->accessControl === null) {
			$this->accessControl = new Control($this);
		}
		return $this->accessControl;
	}

	/**
	 * Return the list of Sourcelien.id to display from the personal profile or the Partenaire profile.
	 *
	 * If no preference is configured, return null.
	 *
	 * @return array|null
	 */
	public function getCustomizedLinks(): ?array
	{
		$cookie = \Yii::app()->request->getCookies()->itemAt('customized-links');
		if ($cookie) {
			$dec = json_decode($cookie, true, 2);
			return is_array($dec) ? $dec : explode(';', $cookie);
		}
		$pId = $this->getstate('instituteId');
		if ($pId > 0) {
			$default = Yii::app()->db->createCommand("SELECT persoRecherche FROM Partenaire WHERE id = {$pId}")->queryScalar();
			if ($default) {
				return json_decode($default, true, 2);
			}
		}
		return null;
	}

	public function hasCustomizedLinksUser(): bool
	{
		return (bool) \Yii::app()->request->getCookies()->itemAt('customized-links');
	}

	/**
	 * @param string[] $selection
	 */
	public function setCustomizedLinks(array $selection): void
	{
		\Yii::app()->request->getCookies()->add(
			'customized-links',
			new CHttpCookie('customized-links', json_encode($selection))
		);
	}

	public function getPartenaireId(): ?int
	{
		$id = $this->getState('partenaireId');
		return $id ? (int) $id : null;
	}

	public function getInstitute(): ?Partenaire
	{
		if ($this->institute !== null) {
			return $this->institute;
		}
		$id = $this->getState('instituteId');
		if ($id > 0) {
			$this->institute = Partenaire::model()->findByPk((int) $id);
		}
		return $this->institute;
	}

	/**
	 * @param int|null $id null, no choice. 0, none.
	 */
	public function setInstituteById(?int $id): void
	{
		if ($id === null) {
			$this->setInstitute(null);
		} else {
			$this->setInstitute(Partenaire::model()->findByPk($id) ?? false);
		}
	}

	/**
	 * @param null|false|Partenaire $institute
	 */
	public function setInstitute(mixed $institute): void
	{
		if ($institute) {
			$this->setState('instituteId', (int) $institute->id);
			$this->setState('instituteName', $institute->nom);
			$this->setState('instituteShortname', $institute->getShortName());
		} else {
			// no choice or choice of "none"
			$this->setState('instituteId', $institute === false ? 0 : null);
			$this->setState('instituteName', null);
			$this->setState('instituteShortname', null);
		}
		$this->institute = $institute ?: null;
		$this->setState('instituteByIp', null);
	}

	/**
	 * Returns true if the current user has "Suivi" on this object.
	 *
	 * @param mixed $object
	 */
	public function isMonitoring($object): bool
	{
		if ($this->isGuest) {
			return false;
		}
		if (is_object($object)) {
			$model = get_class($object);
			$id = $object->id;
		} else {
			$model = $object['model'];
			$id = $object['id'];
		}
		if (isset($this->suivi[$model]) && in_array((int) $id, $this->suivi[$model])) {
			return true;
		}
		return false;
	}

	/**
	 * Overrides a Yii method that is used for roles in controllers (accessRules).
	 *
	 * @param string $operation Name of the operation required (import, suivi...).
	 * @param mixed $params (opt) Parameters for this operation, usually the object to access.
	 * @param bool $allowCaching
	 * @return bool Permission granted?
	 */
	public function checkAccess($operation, $params = [], $allowCaching = true)
	{
		if (empty($this->id)) {
			// Not identified => no rights
			return false;
		}
		if ($operation === 'tester') {
			$testers = \Yii::app()->getParams()->itemAt('testingUsers');
			if (!$testers) {
				// No config, so the access is granted by default to admins
				return (bool) $this->getState("permAdmin");
			}
			return in_array((int) $this->id, $testers, true) ?: in_array($this->login, $testers, true);
		}
		if ($this->getState("permAdmin")) {
			return true;
		}
		$operationName = strtolower($operation);
		switch ($operationName) {
			case 'avec-partenaire':
				return $this->getPartenaireId() > 0;
			case 'abonnement/update': // params: Partenaire|partenaireId
				// $params is the ID of the Partenaire to modify, or the full object
				if (empty($params)) {
					return false;
				}
				if (is_array($params)) {
					$params = $params[0];
				}
				if (is_object($params) && $params instanceof Partenaire) {
					$partenaireId = (int) $params->id;
					$model = $params;
				} else {
					$partenaireId = (int) $params;
					$model = Partenaire::model()->findByPk($partenaireId);
				}
				// partenaire-éditeur ?
				if ($model->editeurId) {
					return false;
				}
				// partenaire de l'utilisateur courant ?
				return (int) $this->getState('partenaireId') === $partenaireId;

			case 'collection/create':
			case 'collection/update':
				if (!$this->getPartenaireId()) {
					return false;
				}
				return Suivi::checkDirectAccess($params); // Ressource

			case 'import':
				return $this->getState('permImport');

			case 'menu-veilleur':
				if (!$this->getPartenaireId()) {
					return false;
				}
				return $this->getState('partenaireType') === 'normal';

			case 'partenaire:admin':
				return $this->access()->toPartenaire()->admin($params['Partenaire'] ?? null);

			case 'possession/update':
				if (isset($params['Partenaire']) && $params['Partenaire'] instanceof Partenaire) {
					return $params['Partenaire']->type === 'normal'
						&& $this->getState('partenaireId') == $params['Partenaire']->id;
				}
				return $this->getState('partenaireType') === 'normal';

			case 'redaction/aide':
				return $this->getState('permRedaction');
			case 'redaction/editorial':
				return $this->getState('permRedaction');

			case 'ressource/create-direct':
				if (!$this->getPartenaireId()) {
					return false;
				}
				return $this->getState('partenaireType') !== 'editeur';
			case 'ressource/update-direct':
				if (empty($params['Ressource']) || !($params['Ressource'] instanceof Ressource)) {
					throw new Exception("Coding error: expected Ressource instance, not " . print_r($params, true));
				}
				if (!$this->getPartenaireId()) {
					return false;
				}
				return Suivi::checkDirectAccess($params['Ressource']);

			case 'revue/update-direct':
				if (empty($params['Revue']) || !($params['Revue'] instanceof Revue)) {
					throw new Exception("Coding error: expected Revue instance, not " . print_r($params, true));
				}
				return Suivi::checkDirectAccess($params['Revue']);

			case 'service/admin':
				return false;
			case 'service/create-direct':
				if (!$this->getPartenaireId()) {
					return false;
				}
				return Suivi::checkDirectAccessByIds($params['revueId'], $params['ressourceId']);
			case 'service/update-direct':
				if (empty($params['Service']) || !($params['Service'] instanceof Service)) {
					throw new Exception("Coding error: expected Service instance, not " . print_r($params, true));
				}
				if (!$this->getPartenaireId()) {
					return false;
				}
				return Suivi::checkDirectAccess($params['Service']);

			case 'stats/partenaire':
				if (empty($params['Partenaire'])) {
					return false;
				}
				return (int) $params['Partenaire']->id === (int) $this->getState('partenaireId');
			case 'stats/suivi':
				return false;

			case 'suivi': // params: Partenaire | Suivi
				if ($params instanceof Partenaire) {
					return $this->getState('partenaireId') == $params->id;
				}
				if ($params instanceof Suivi) {
					return $this->getState('partenaireId') == $params->partenaireId;
				}
				// idem 'Suivi'
				// $params is the target object to verify
				if ($this->getState('partenaireType') === 'normal' && !Suivi::isTracked($params)) {
					return true;
				}
				return $this->isMonitoring($params);

			case 'titre:admin':
				return $this->access()->toTitre()->admin();

			case 'utilisateur:admin':
				return $this->access()->toUtilisateur()->admin();

			case 'validate':
				return Suivi::checkDirectAccess($params);
			case 'verify':
				// $params is the target object to verify
				if ($this->getState('partenaireType') === 'normal' && !Suivi::isTracked($params)) {
					return true;
				}
				return $this->isMonitoring($params);

			default:
				if ($this->hasState('perm' . ucfirst($operationName))) {
					return $this->getState('perm' . ucfirst($operationName));
				}
				if ($this->hasState($operation)) {
					return $this->getState($operation);
				}
				Yii::log("checkAccess() pour une opération inconnue '$operationName'", CLogger::LEVEL_WARNING);
		}
		return false;
	}

	protected function afterLogin($fromCookie)
	{
		if ($this->accessControl !== null) {
			$this->accessControl = new Control($this);
		}
		parent::afterLogin($fromCookie);
	}
}
