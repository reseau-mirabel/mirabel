<?php

namespace components;

use PDO;
use Yii;

class SqlHelper
{
	/**
	 * Returns an assoc array (column => Object).
	 *
	 * @param string $sql SQL.
	 * @param string $className
	 * @param string $colName (opt) column used as key (defaults to 'id').
	 * @return array assoc array (column => Object).
	 */
	public static function sqlToPairsObject(string $sql, string $className, string $colName = 'id'): array
	{
		$rows = Yii::app()->db->createCommand($sql)->queryAll();
		$model = call_user_func([$className, 'model']);
		assert($model instanceof \CActiveRecord);
		$records = [];
		foreach ($rows as $attributes) {
			$record = $model->populateRecord($attributes, true);
			if ($record !== null) {
				$records[$attributes[$colName]] = $record;
			}
		}
		return $records;
	}

	public static function dateIntervalToSqlCondition(?string $range, string $columnName): string
	{
		$ts = DateTimeHelper::parseDateInterval($range);
		if (!$ts) {
			return "";
		}
		if (empty($ts[0])) {
			return "$columnName < {$ts[1]}";
		}
		if (empty($ts[1])) {
			return "$columnName > {$ts[0]}";
		}
		return "$columnName BETWEEN {$ts[0]} AND {$ts[1]}";
	}

	/**
	 * Returns an assoc array (col1 => col2).
	 *
	 * @param string $sql SQL.
	 * @param array $content (opt) Initial content.
	 * @return array assoc array (col1 => col2).
	 */
	public static function sqlToPairs(string $sql, array $content = []): array
	{
		Yii::app()->getDb()->setActive(true); // activate the PDO connection if necessary
		$rows = Yii::app()->getDb()->getPdoInstance()->query($sql, PDO::FETCH_NUM);
		foreach ($rows as $row) {
			$content[$row[0]] = $row[1];
		}
		return $content;
	}
}
