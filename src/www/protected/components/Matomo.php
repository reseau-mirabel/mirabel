<?php

namespace components;

use CLogger;
use MatomoTracker;
use Yii;

class Matomo extends \CComponent
{
	/**
	 * @var string
	 */
	private $rootUrl = '';

	/**
	 * @var int
	 */
	private $siteId = 0;

	/**
	 * @var string For messages sent from the web server (useless with JS sends).
	 */
	private $tokenAuth = '';

	/**
	 * @var ?MatomoTracker
	 */
	private $tracker;

	/**
	 * @var string
	 */
	private $matomoUserName = '';

	public function init()
	{
		if (!$this->siteId) {
			// No config => read the config stored in 'params'
			$this->readParamsConfig();
		}

		if ($this->rootUrl) {
			$this->rootUrl = rtrim($this->rootUrl, '/') . '/';
		}
	}

	public function getJsCommand(): string
	{
		if (!$this->isActive()) {
			return '';
		}
		$rootUrl = json_encode($this->rootUrl);
		return <<<EOJS
			(function() {
				var u = {$rootUrl};
				_paq.push(['setTrackerUrl', u+'matomo.php']);
				_paq.push(['setSiteId', {$this->siteId}]);
				var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
				g.type = 'text/javascript';
				g.async = true;
				g.src = u + 'matomo.js';
				s.parentNode.insertBefore(g, s);
			})();
			EOJS;
	}

	public function getNoScript(): string
	{
		if (!$this->isActive()) {
			return '';
		}
		$rootUrl = htmlspecialchars($this->rootUrl);
		return <<<EOHTML
			<noscript>
			<img src="{$rootUrl}matomo.php?idsite={$this->siteId}&amp;rec=1" style="border:0;" alt="" />
			</noscript>
			EOHTML;
	}

	public function getTracker(): MatomoTracker
	{
		if ($this->tracker === null) {
			MatomoTracker::$URL = $this->rootUrl;
			$this->tracker = new MatomoTracker($this->siteId);
			$this->tracker->setTokenAuth($this->tokenAuth);
		}
		return $this->tracker;
	}

	public function isActive(): bool
	{
		return $this->siteId > 0 && !empty($this->rootUrl);
	}

	public function sendFromServer(): void
	{
		if (!$this->isActive() || empty($this->tokenAuth)) {
			return;
		}
		// after the response is ready, send a record to Matomo
		Yii::app()->attachEventHandler('onEndRequest', function () {
			flush();
			fastcgi_finish_request();
			$urlWithoutParameters = preg_replace('#\?.*$#', '', $_SERVER['REQUEST_URI']);
			$urlAction = preg_replace('#^/api#', '', $urlWithoutParameters);
			try {
				$this->getTracker()->doTrackEvent("API", $urlAction, $this->matomoUserName ?: false);
			} catch (\Exception $e) {
				Yii::log("API Matomo error ($urlAction): " . $e->getMessage(), CLogger::LEVEL_WARNING);
			}
		});
	}

	public function setRootUrl(string $s): void
	{
		$this->rootUrl = $s;
	}

	public function setSiteId(int $id): void
	{
		$this->siteId = $id;
	}

	public function setTokenAuth(string $s): void
	{
		$this->tokenAuth = $s;
	}

	public function setWebUserName(string $name): self
	{
		$this->matomoUserName = $name;
		return $this;
	}

	private function readParamsConfig(): void
	{
		$matomoConfig = Yii::app()->params->itemAt('matomo');
		if (!$matomoConfig || empty($matomoConfig['siteId'])) {
			return;
		}
		$this->rootUrl = $matomoConfig['rootUrl'] ?? '';
		$this->siteId = $matomoConfig['siteId'] ?? 0;
		$this->tokenAuth = $matomoConfig['tokenAuth'] ?? '';
	}
}
