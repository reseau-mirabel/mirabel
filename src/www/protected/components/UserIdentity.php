<?php

/**
 * UserIdentity handles the authentication and the related user data.
 *
 * It contains the authentication method that checks if the provided
 * data can identify the user.
 *
 * When the login is successful, the instance is passed to CWebUser::login()
 * which extract state and stores it into the PHP session.
 * At logout, this state will be removed from the session.
 */
class UserIdentity extends CUserIdentity
{
	public const ERROR_MAINTENANCE_NOTADMIN = 3;

	private $_id;

	/**
	 * Make 'id' a read-only attribute on '_id'.
	 *
	 * @return string Current user id.
	 */
	public function getId()
	{
		return $this->_id;
	}

	/**
	 * Authenticates a user.
	 *
	 * @return bool whether authentication succeeds.
	 */
	public function authenticate()
	{
		try {
			$user = Utilisateur::model()->find('actif = 1 AND login = :l', [':l' => $this->username]);
		} catch (\Exception $e) {
			$this->errorCode = self::ERROR_USERNAME_INVALID;
			$this->errorMessage = 'Nom ou mot de passe mal formé.';
			return false;
		}
		if ($user === null) {
			$this->errorCode = self::ERROR_USERNAME_INVALID;
			$this->errorMessage = 'Nom ou mot de passe incorrect.';
		} elseif (!$user->validatePassword($this->password)) {
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
			$this->errorMessage = 'Nom ou mot de passe incorrect.';
		} elseif (!$user->permAdmin && \Config::read('maintenance.authentification.inactive')) {
			$this->errorCode = self::ERROR_MAINTENANCE_NOTADMIN;
			$this->errorMessage = 'En maintenance : connexion restreinte aux administrateurs';
		} else {
			// valid user and login
			$this->errorCode = self::ERROR_NONE;
			$this->startUserSession($user);
		}
		return ($this->errorCode === self::ERROR_NONE);
	}

	public function getMessage(): string
	{
		return $this->errorMessage;
	}

	private function startUserSession(\Utilisateur $user)
	{
		$this->_id = (string) $user->id;
		// Add all User fields as persistent attributes of  Yii::app()->user
		foreach ($user->attributes as $field => $value) {
			if ($field !== 'id' && $field !== 'motdepasse') {
				$this->setState($field, $value);
			}
		}
		if ($user->partenaireId > 0) {
			$partenaire = $user->partenaire;
			$this->setState('suivi', $partenaire->getRightsSuivi(true));
			$this->setState('partenaireType', $partenaire->type);
			$this->setState('proxyUrl', $partenaire->proxyUrl);
			// The log-in resets the selected 'institute'
			Yii::app()->user->setInstitute($partenaire);
		} else {
			$this->setState('suivi', []);
			$this->setState('partenaireType', '');
			$this->setState('proxyUrl', '');
		}
	}
}
