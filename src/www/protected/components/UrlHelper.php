<?php

namespace components;

class UrlHelper
{
	public static function extractHostname(string $url): string
	{
		if (!$url || $url[0] === '/') {
			return '';
		}
		return (string) parse_url($url, PHP_URL_HOST);
	}

	/**
	 * Up to level-2, e.g. "http://sure.i.like.it/x" would return ["sure.i.like.it", "i.like.it", "like.it"]
	 *
	 * @return string[]
	 */
	public static function extractAllDomains(string $url): array
	{
		$host = self::extractHostname($url);
		if (!$host) {
			return [];
		}
		if (substr_count($host, ".") < 2) {
			return [$host];
		}
		$result = [$host];
		$parts = explode(".", $host);
		while (count($parts) > 2) {
			array_shift($parts);
			$result[] = join(".", $parts);
		}
		return $result;
	}

	/**
	 * Return the level-2 domain, e.g. "http://i.like.it" => "like.it".
	 */
	public static function extractDomain(string $url): string
	{
		$hostname = self::extractHostname($url);
		if (substr_count($hostname, '.') < 2) {
			return $hostname;
		}
		$m = [];
		if (preg_match('/(?:^|\.)([^.]+\.[^.]+)$/', $hostname, $m)) {
			return $m[1];
		}
		return '';
	}
}
