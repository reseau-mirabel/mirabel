<?php

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class CaptchaSumAction extends CCaptchaAction
{
	/**
	 * @inheritdoc
	 */
	public function validate($input, $caseSensitive)
	{
		$code = $this->getVerifyCode();
		$this->getVerifyCode(true); // cannot reuse the same code
		$m = [];
		if (!preg_match('/^(\d+)\+(\d+)$/', $code, $m)) {
			return false;
		}
		$sum = (int) $m[1] + (int) $m[2];
		return $sum === (int) $input;
	}

	protected function generateVerifyCode()
	{
		return rand(2, 50) . '+' . rand(1, 9);
	}
}
