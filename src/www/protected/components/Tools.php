<?php

namespace components;

use Normalizer;

/**
 * Contains some static helper methods.
 */
class Tools
{
	public static function readCsv(string $csv, string $separator): array
	{
		$result = [];
		foreach (explode("\n", $csv) as $line) {
			if (trim($line) !== "") {
				$result[] = str_getcsv($line, $separator, '"', '\\');
			}
		}
		return $result;
	}

	/**
	 * Return true if one of the models has an error.
	 *
	 * @param \CModel[] $array
	 */
	public static function hasErrors(array $array): bool
	{
		if ($array) {
			foreach ($array as $a) {
				if ($a->hasErrors()) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Check if an IP matches a CIDR mask.
	 *
	 * @param int|string $ip IP to check.
	 * @param int|string $maskIp Radical of the mask (e.g. 192.168.0.0).
	 * @param int $maskBits Size of the mask (e.g. 24).
	 * @returns boolean matches?
	 */
	public static function matchIpMask($ip, $maskIp, int $maskBits): bool
	{
		$mask = ~(pow(2, 32 - $maskBits) - 1);
		if (false === is_int($ip)) {
			$ip = ip2long($ip);
		}
		if (!is_int($maskIp)) {
			if (\ctype_digit($maskIp)) {
				$maskIp = (int) $maskIp;
			} else {
				$maskIp = ip2long($maskIp);
			}
		}
		if (($ip & $mask) === ($maskIp & $mask)) {
			return true;
		}
		return false;
	}

	public static function matchIpFilter(string $ip, ?string $filterString): bool
	{
		$filters = preg_split('/[,\s]+/', $filterString, -1, PREG_SPLIT_NO_EMPTY);
		// expand ranges
		$expandedFilters = [];
		foreach ($filters as $filter) {
			if (strpos($filter, '-') === false) {
				$expandedFilters[] = $filter;
			} else {
				$packs = explode('.', $filter);
				$expanded = [[]];
				foreach ($packs as $pack) {
					if (preg_match('/^(\d+)-(\d+)$/', $pack, $m)) {
						$a = (int) $m[1];
						$b = (int) $m[2];
						$size = count($expanded);
						for ($i = 0; $i < $size; $i++) {
							if ($a < $b) {
								$e = $expanded[$i];
								$expanded[$i][] = $a;
								for ($r = $a + 1; $r <= $b; $r++) {
									$f = $e;
									$f[] = $r;
									$expanded[] = $f;
								}
							}
						}
					} else {
						for ($i = 0; $i < count($expanded); $i++) {
							$expanded[$i][] = $pack;
						}
					}
				}
				foreach ($expanded as $e) {
					$expandedFilters[] = join('.', $e);
				}
			}
		}
		foreach ($expandedFilters as $filter) {
			// normal or incomplete IPv4
			if (preg_match('/^\d+\.(\d{1,3}\.)?(\d{1,3}\.)?(\d{1,3}|\*)?$/', $filter)) {
				if (substr($filter, -1) === '.' || substr($filter, -1) === '*') {
					$filter = rtrim($filter, '*');
					if (strncmp($ip, $filter, strlen($filter)) === 0) {
						return true;
					}
				} else {
					if (strcmp($ip, $filter) === 0) {
						return true;
					}
				}
			} elseif (preg_match('#^([\d.]+)/(\d+)$#', $filter, $match)) {  // CIDR
				if (Tools::matchIpMask($ip, $match[1], (int) $match[2])) {
					return true;
				}
			} elseif ($ip === $filter) {                                     // IPv6
				return true;
			}
		}
		return false;
	}

	public static function normalizeText(?string $text): string
	{
		if ($text === null || $text === '') {
			return '';
		}
		static $chars = [
			"\x00", "\x01", "\x02", "\x03", "\x04", "\x05", "\x06", "\x07", "\x08", "\x09",
			"\x0B", "\x0C", "\x0E", "\x0F",
			"\x10", "\x11", "\x12", "\x13", "\x14", "\x15", "\x16", "\x17", "\x18", "\x19",
			"\x1A", "\x1B", "\x1C", "\x1D", "\x1E", "\x1F",
			"\xe2\x80\x8e", // left-to-right
		];
		if (extension_loaded('intl')) { // UTF-8 normalization
			$return = Normalizer::normalize(str_replace($chars, '', $text));
		} else {
			$return = str_replace($chars, '', $text);
		}
		if ($return === false) {
			return "";
		}
		return $return;
	}
}
