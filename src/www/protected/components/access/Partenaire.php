<?php

namespace components\access;

class Partenaire extends DefaultControl
{
	public function admin(?\Partenaire $p = null): bool
	{
		$special = $this->getSpecialCase();
		if ($special !== null) {
			return $special;
		}
		if ($p === null) {
			return false;
		}
		if ($this->parent->hasPermission('partenaire') && $this->parent->isSamePartenaire((int) $p->id)) {
			return true;
		}
		if ($p->editeurId) {
			return $this->parent->hasSuivi('partenairesEditeurs');
		}
		return false;
	}

	public function create(): bool
	{
		return $this->isAdmin();
	}

	public function customize(\Partenaire $p): bool
	{
		$special = $this->getSpecialCase();
		if ($special !== null) {
			return $special;
		}
		// granted if the current user is a member of this Partenaire
		return $this->parent->isSamePartenaire((int) $p->id);
	}

	public function editorial(?int $partenaireId): bool
	{
		$special = $this->getSpecialCase();
		if ($special !== null) {
			return $special;
		}
		if (!$this->parent->hasPermission('partenaire')) {
			return false;
		}
		if ($partenaireId) {
			// Can update this Partenaire
			return $this->parent->isSamePartenaire($partenaireId);
		}
		// Can update a partenaire (their own)
		return true;
	}

	public function logo(int $partenaireId): bool
	{
		$special = $this->getSpecialCase();
		if ($special !== null) {
			return $special;
		}
		// granted if member of this Partenaire, with U.permPartenaire
		return $this->parent->isSamePartenaire($partenaireId) && $this->parent->hasPermission('partenaire');
	}

	/**
	 * Modifier les suivis d'un partenaire.
	 */
	public function suivi(\Partenaire $p): bool
	{
		$special = $this->getSpecialCase();
		if ($special !== null) {
			return $special;
		}
		$acteur = $this->parent->getPartenaire();
		if (!$acteur || $acteur->statut !== 'actif') {
			return false;
		}
		if ($this->admin($p)) {
			return true;
		}
		if ($p->type === 'import') {
			return false;
		}
		if ($this->parent->isSamePartenaire((int) $p->id)) {
			return true;
		}
		if ($acteur->editeurId) {
			return false;
		}
		if ($p->editeurId) {
			return $this->parent->hasSuivi('partenairesEditeurs');
		}
		return false;
	}

	public function update(\Partenaire $p): bool
	{
		$special = $this->getSpecialCase();
		if ($special !== null) {
			return $special;
		}
		if ($this->admin($p)) {
			return true;
		}
		if ($this->parent->hasPermission('partenaire')) {
			return $this->parent->isSamePartenaire((int) $p->id);
		}
		if ($p->editeurId) {
			// Modification d'un partenaire-éditeur, avec suiviPartenairesEditeurs ?
			return $this->parent->hasSuivi('partenairesEditeurs');
		}
		return false;
	}

	public function upload(): bool
	{
		$special = $this->getSpecialCase();
		if ($special !== null) {
			return $special;
		}
		return $this->parent->hasPermission('partenaire');
	}

	public function view(\Partenaire $p): bool
	{
		$special = $this->getSpecialCase();
		if ($special !== null) {
			return $special;
		}
		return $this->parent->isSamePartenaire((int) $p->id);
	}
}
