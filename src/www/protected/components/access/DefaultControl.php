<?php

namespace components\access;

abstract class DefaultControl
{
	/**
	 * @var Control
	 */
	protected $parent;

	public function __construct(Control $c)
	{
		$this->parent = $c;
	}

	protected function getSpecialCase(): ?bool
	{
		if (!$this->parent->isAuthenticated()) {
			return false;
		}
		if ($this->isAdmin()) {
			return true;
		}
		return null;
	}

	protected function isAdmin(): bool
	{
		return $this->parent->hasPermission('admin');
	}
}
