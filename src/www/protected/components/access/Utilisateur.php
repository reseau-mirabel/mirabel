<?php

namespace components\access;

use Partenaire;

class Utilisateur extends DefaultControl
{
	public function admin(): bool
	{
		return $this->isAdmin();
	}

	public function annotate(\Utilisateur $target): bool
	{
		return $this->isAdmin() || $this->hasSuiviPartenaireEditeur($target);
	}

	public function create(?Partenaire $p): bool
	{
		if ($this->isAdmin()) {
			return true;
		}
		if ($p !== null && ($p->type === Partenaire::TYPE_EDITEUR || $p->editeurId > 0)) {
			return $this->parent->hasSuivi('partenairesEditeurs');
		}
		return false;
	}

	public function delete(\Utilisateur $target): bool
	{
		if (empty($target->partenaireId) && $this->parent->isSameUser($target->id)) {
			// Un utilisateur sans partenaire (donc "U-politiques") peut effacer son compte
			return true;
		}
		return $this->isAdmin();
	}

	public function disable(\Utilisateur $target): bool
	{
		$special = $this->getSpecialCase();
		if ($special !== null) {
			return $special;
		}
		if ($this->hasSuiviPartenaireEditeur($target)) {
			return true;
		}
		if (empty($target->partenaireId) && $this->parent->isSameUser($target->id)) {
			// Un utilisateur sans partenaire (donc "U-politiques") peut désactiver son compte
			return true;
		}
		return false;
	}

	public function listesDiffusion(\Utilisateur $target): bool
	{
		$special = $this->getSpecialCase();
		if ($special !== null) {
			return $special;
		}
		if (!empty($target->partenaireId)
			&& $target->partenaire->type === Partenaire::TYPE_NORMAL
			&& $this->parent->isSameUser($target->id)) {
			// "normal" user updating themselves
			return true;
		}
		return false;
	}

	public function messagePassword(\Utilisateur $target)
	{
		return $this->annotate($target);
	}

	public function setPassword(\Utilisateur $target)
	{
		$special = $this->getSpecialCase();
		if ($special !== null) {
			return $special;
		}
		if ($this->parent->isSameUser($target->id)) {
			return true;
		}
		return false;
	}

	public function update(\Utilisateur $target): bool
	{
		if ($target->getIsNewRecord()) {
			return $this->create($target->partenaire);
		}
		$special = $this->getSpecialCase();
		if ($special !== null) {
			return $special;
		}
		if ($this->hasSuiviPartenaireEditeur($target)) {
			return true;
		}
		if ($this->parent->isSameUser($target->id)) {
			return true; // updating themselves
		}
		return false;
	}

	public function view(\Utilisateur $target): bool
	{
		$special = $this->getSpecialCase();
		if ($special !== null) {
			return $special;
		}
		if ($this->parent->isSameUser($target->id)) {
			return true; // view themselves
		}
		if (empty($target->partenaireId)) {
			return false;
		}
		if ($this->parent->isSamePartenaire($target->partenaireId)) {
			return true;
		}
		if ($this->hasSuiviPartenaireEditeur($target)) {
			return true;
		}
		return false;
	}

	private function hasSuiviPartenaireEditeur(\Utilisateur $target)
	{
		if (!$target->partenaireId) {
			return false;
		}
		$p = $target->partenaire;
		return ($p->type === Partenaire::TYPE_EDITEUR || $p->editeurId > 0)
			&& $this->parent->hasSuivi('partenairesEditeurs');
	}
}
