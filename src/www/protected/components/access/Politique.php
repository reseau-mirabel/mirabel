<?php

namespace components\access;

use Yii;

class Politique extends DefaultControl
{
	public const ROLE_EDITEUR_NONE = '';

	public const ROLE_EDITEUR_ADMIN = 'admin';

	public const ROLE_EDITEUR_PENDING = 'pending';

	public const ROLE_EDITEUR_GUEST = 'guest';

	public const ROLE_EDITEUR_OWNER = 'owner';

	/**
	 * Grant or request a role on a publisher.
	 */
	public function addPublisher(\Utilisateur $u): bool
	{
		$special = $this->getSpecialCase();
		if ($special !== null) {
			return $special;
		}
		if ($this->parent->isSameUser((int) $u->id)) {
			// Users can grant/request a new role for themselves
			return true;
		}
		// Only those that can validate users can grant roles to other users.
		return $this->validate();
	}

	/**
	 * Remove a role on a publisher.
	 */
	public function removePublisher(\Utilisateur $u, int $eid): bool
	{
		$special = $this->getSpecialCase();
		if ($special !== null) {
			// global guest, global admin, admin politiques
			return $special;
		}
		$uid = (int) $u->id;
		if ($this->parent->isSameUser($uid)) {
			// Users can remove their own role.
			return true;
		}
		if ($this->getRole($eid) === self::ROLE_EDITEUR_OWNER && $this->getRole($eid, $uid) === self::ROLE_EDITEUR_GUEST) {
			// Current user "owns" the publisher that will be removed from a "non-owner".
			return true;
		}
		return false;
	}

	/**
	 * Can assign an existing policy to a journal of this publisher.
	 */
	public function assign(int $editeurId): bool
	{
		$special = $this->getSpecialCase();
		if ($special !== null) {
			return $special;
		}
		$role = $this->getRole((int) $editeurId);
		return $role !== self::ROLE_EDITEUR_NONE && $role !== self::ROLE_EDITEUR_PENDING;
	}

	/**
	 * Create a policy related to a publisher.
	 */
	public function create(\Editeur $e): bool
	{
		$special = $this->getSpecialCase();
		if ($special !== null) {
			return $special;
		}
		$role = $this->getRole((int) $e->id);
		return $role !== self::ROLE_EDITEUR_NONE;
	}

	public function delete(\Politique $p): bool
	{
		$special = $this->getSpecialCase();
		if ($special !== null) {
			return $special;
		}
		// From here, authentified but not an admin
		$role = $this->getRole((int) $p->editeurId);
		if ($role === self::ROLE_EDITEUR_NONE) {
			return false;
		}
		if ($role === self::ROLE_EDITEUR_PENDING) {
			if (!$this->parent->isSameUser((int) $p->utilisateurId)) {
				// Current user is NOT the author
				return false;
			}
			return ($p->status === \Politique::STATUS_PENDING);
		}
		// Real roles can mark their poilicies as TODELETE or DELETED
		return true;
	}

	public function publish(\Politique $p): bool
	{
		$role = $this->getRole((int) $p->editeurId);
		if ($role === self::ROLE_EDITEUR_NONE || $role === self::ROLE_EDITEUR_PENDING) {
			return false;
		}
		if ($p->status === \Politique::STATUS_DRAFT || $p->status === \Politique::STATUS_UPDATED) {
			// role: "admin" | "guest" | "owner"
			return true;
		}
		if ($role === self::ROLE_EDITEUR_ADMIN && $p->status === \Politique::STATUS_TOPUBLISH) {
			// role: "admin"
			return true;
		}
		return false;
	}

	public function requestRole(): bool
	{
		// Even guest can select their roles when creating their account
		return true;
	}

	public function update(\Politique $p): bool
	{
		$special = $this->getSpecialCase();
		if ($special !== null) {
			return $special;
		}
		if ($this->parent->isSameUser((int) $p->utilisateurId)) {
			// Current user is the author
			return true;
		}
		$role = $this->getRole((int) $p->editeurId);
		return $role !== self::ROLE_EDITEUR_NONE && $role !== self::ROLE_EDITEUR_PENDING;
	}

	/**
	 * Confirm or grant the role of an user on a publisher.
	 */
	public function validate(): bool
	{
		return $this->isAdmin();
	}

	/**
	 * View all the Politique records.
	 */
	public function view(int $editeurId): bool
	{
		return $this->assign($editeurId);
	}

	/**
	 * @return ""|"admin"|"guest"|"owner"|"pending" AKA self::ROLE_EDITEUR_*
	 */
	public function getRole(int $editeurId, ?int $utilisateurId = null): string
	{
		if ($this->isAdmin()) {
			return self::ROLE_EDITEUR_ADMIN;
		}
		if (!$utilisateurId) {
			$utilisateurId = $this->parent->getUtilisateurId();
		}
		$row = Yii::app()->db
			->createCommand("SELECT role, confirmed FROM Utilisateur_Editeur WHERE utilisateurId = :uid AND editeurId = :eid")
			->queryRow(true, [':uid' => $utilisateurId, ':eid' => $editeurId]);
		if (empty($row)) {
			return self::ROLE_EDITEUR_NONE;
		}
		return ($row['confirmed'] > 0 ? (string) $row['role'] : self::ROLE_EDITEUR_PENDING);
	}

	/**
	 * Overwrite the default so that permPolitiques includes every authorization related to Politique.
	 */
	public function isAdmin(): bool
	{
		return $this->parent->hasPermission('admin') || $this->parent->hasPermission('politiques');
	}
}
