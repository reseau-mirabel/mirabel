<?php

namespace components\access;

class Control
{
	private const PERM_FIELDS = ['admin', 'import', 'indexation', 'partenaire', 'politiques', 'redaction'];

	private const SUIVI_FIELDS = ['editeurs', 'nonSuivi', 'partenairesEditeurs'];

	/**
	 * @var ?int
	 */
	protected $utilisateurId;

	/**
	 * @var ?int
	 */
	protected $partenaireId;

	private $permissions = [];

	private $suivi = [];

	private $cache = [];

	public function __construct(\WebUser $u)
	{
		if ($u->id) {
			$this->utilisateurId = (int) $u->id;
			$this->partenaireId = $u->getPartenaireId();
			foreach (self::PERM_FIELDS as $k) {
				$this->permissions[$k] = (bool) $u->getState('perm' . ucfirst($k), false);
			}
			foreach (self::SUIVI_FIELDS as $k) {
				$this->suivi[$k] = (bool) $u->getState('suivi' . ucfirst($k), false);
			}
		} else {
			foreach (self::PERM_FIELDS as $k) {
				$this->permissions[$k] = false;
			}
			foreach (self::SUIVI_FIELDS as $k) {
				$this->suivi[$k] = false;
			}
		}
	}

	public function toEditeur(): Editeur
	{
		if (empty($this->cache['editeur'])) {
			$this->cache['editeur'] = new Editeur($this);
		}
		return $this->cache['editeur'];
	}

	public function toPartenaire(): Partenaire
	{
		if (empty($this->cache['partenaire'])) {
			$this->cache['partenaire'] = new Partenaire($this);
		}
		return $this->cache['partenaire'];
	}

	public function toPolitique(): Politique
	{
		if (empty($this->cache['politique'])) {
			$this->cache['politique'] = new Politique($this);
		}
		return $this->cache['politique'];
	}

	public function toTitre(): Titre
	{
		if (empty($this->cache['titre'])) {
			$this->cache['titre'] = new Titre($this);
		}
		return $this->cache['titre'];
	}

	public function toUtilisateur(): Utilisateur
	{
		if (empty($this->cache['utilisateur'])) {
			$this->cache['utilisateur'] = new Utilisateur($this);
		}
		return $this->cache['utilisateur'];
	}

	public function hasPermission(string $name, bool $includeAdmin = true): bool
	{
		if ($includeAdmin && $this->permissions['admin']) {
			// global admin
			return true;
		}
		if (in_array($name, self::PERM_FIELDS)) {
			return $this->permissions[$name];
		}
		\Yii::log("Wrong permission: '$name'", \CLogger::LEVEL_ERROR);
		throw new \Exception("Coding error, wrong 'perm*' name");
	}

	public function hasSuivi(string $name): bool
	{
		if (in_array($name, self::SUIVI_FIELDS)) {
			return $this->suivi[$name];
		}
		\Yii::log("Wrong suivi: '$name'", \CLogger::LEVEL_ERROR);
		throw new \Exception("Coding error, wrong 'suivi*' name");
	}

	public function getPartenaire(): ?\Partenaire
	{
		if (!$this->partenaireId) {
			return null;
		}
		return \Partenaire::model()->findByPk($this->partenaireId);
	}

	public function getPartenaireId(): ?int
	{
		return $this->partenaireId;
	}

	public function getUtilisateurId(): ?int
	{
		return $this->utilisateurId;
	}

	public function isAuthenticated(): bool
	{
		return $this->utilisateurId > 0;
	}

	public function isSamePartenaire(?int $partenaireId): bool
	{
		return $partenaireId > 0 && (int) $partenaireId === $this->partenaireId;
	}

	public function isSameUser(?int $utilisateurId): bool
	{
		return $utilisateurId > 0 && (int) $utilisateurId === $this->utilisateurId;
	}
}
