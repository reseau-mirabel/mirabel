<?php

namespace components\access;

use Partenaire;
use Suivi;

class Editeur extends DefaultControl
{
	public function createDirect(): bool
	{
		$special = $this->getSpecialCase();
		if ($special !== null) {
			return $special;
		}
		$partenaire = $this->parent->getPartenaire();
		return ($partenaire instanceof Partenaire) && $partenaire->type === \Partenaire::TYPE_NORMAL; // partenaire-veilleur
	}

	public function delete(\Editeur $e): bool
	{
		$special = $this->getSpecialCase();
		if ($special !== null) {
			return $special;
		}
		if (Partenaire::model()->findByAttributes(['editeurId' => $e->id])) {
			// éditeur-partenaire => special permission required
			return (bool) $this->parent->hasSuivi('partenairesEditeurs');
		}
		return $this->parent->hasSuivi('editeurs') || Suivi::checkDirectAccess($e);
	}

	/**
	 * Update the logo. Ticket Mantis #4612
	 */
	public function logo(\Editeur $e): bool
	{
		$special = $this->getSpecialCase();
		if ($special !== null) {
			return $special;
		}
		$p = Partenaire::model()->findByAttributes(['editeurId' => $e->id]);
		if (!$p) {
			// permission editeur/logo applies only to a partenaire-editeur
			return false;
		}
		if ($this->parent->hasSuivi('partenairesEditeurs')) {
			return true;
		}
		return $this->parent->isSamePartenaire((int) $p->id);
	}

	public function roles(): bool
	{
		$special = $this->getSpecialCase();
		if ($special !== null) {
			return $special;
		}
		return $this->parent->getPartenaireId() > 0;
	}

	public function updateDirect(\Editeur $e): bool
	{
		if ($e->partenaire !== null && $this->parent->isSamePartenaire((int) $e->partenaire->id)) {
			// membre d'un partenaire-éditeur sur son éditeur
			return true;
		}
		return $this->delete($e);
	}

	public function viewPolitiques(\Editeur $e): bool
	{
		$special = $this->getSpecialCase();
		if ($special !== null) {
			return $special;
		}
		if ($e->partenaire !== null) {
			if ($this->parent->isSamePartenaire((int) $e->partenaire->id)) {
				// membre d'un partenaire-éditeur sur son éditeur
				return true;
			}
			if ($this->parent->hasSuivi('partenairesEditeurs')) {
				return true;
			}
		}
		if ($this->parent->hasSuivi('editeurs')) {
			return true;
		}
		return $this->parent->toPolitique()->view((int) $e->id);
	}
}
