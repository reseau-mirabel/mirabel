<?php

namespace components\access;

use Suivi;
use Yii;

class Titre extends DefaultControl
{
	public function admin(): bool
	{
		return $this->isAdmin();
	}

	public function createDirect(?int $revueId): bool
	{
		$special = $this->getSpecialCase();
		if ($special !== null) {
			return $special;
		}
		if (!$this->parent->getPartenaireId()) {
			return false;
		}
		if ($revueId > 0) {
			// check if someone else is in charge of this Revue
			return Suivi::checkDirectAccessByIds($revueId, null);
		}
		$partenaire = $this->parent->getPartenaire();
		if (!$partenaire || $partenaire->editeurId > 0) {
			// partenaire-éditeur
			return false;
		}
		return true; // auth user can create a Revue
	}

	public function delete(\Titre $titre): bool
	{
		$special = $this->getSpecialCase();
		if ($special !== null) {
			return $special;
		}
		return ($this->parent->getPartenaireId() > 0);
	}

	/**
	 * Accès aux attributs de la relation : ancien, commercial, intellectuel, role
	 */
	public function relationEditeurs(\Titre $titre): bool
	{
		$special = $this->getSpecialCase();
		if ($special !== null) {
			return $special;
		}
		if ($this->parent->hasSuivi('editeurs')) {
			return true;
		}
		if ($this->parent->hasSuivi('partenairesEditeurs')) {
			return true;
		}
		if ($this->parent->hasPermission('politiques')) {
			return true;
		}
		if ($this->isInChargeOfPolicy((int) $titre->id) && $this->parent->getPartenaireId()) {
			$p = $this->parent->getPartenaire();
			$isAmongPublishers = \Yii::app()->db->createCommand("SELECT 1 FROM Titre_Editeur WHERE editeurId = :eid AND titreId = " . (int) $titre->id);
			if ($p->editeurId > 0 && $isAmongPublishers->queryScalar([':eid' => $p->editeurId])) {
				// L'utilisateur actuel est membre d'un partenaire-éditeur
				// dont l'éditeur participe à ce titre,
				// et l'utilisateur est en charge de la politique de cet éditeur.
				return true;
			}
		}
		return false;
	}

	public function updateDirect(\Titre $titre): bool
	{
		$special = $this->getSpecialCase();
		if ($special !== null) {
			return $special;
		}
		return ($this->parent->getPartenaireId() > 0)
			&& Suivi::checkDirectAccess($titre);
	}

	public function updateEditeurs(\Titre $titre): bool
	{
		$special = $this->getSpecialCase();
		if ($special !== null) {
			return $special;
		}
		if ($this->parent->getPartenaireId()) {
			// Current user is NOT an utilisateur-politiques => grant access
			return true;
		}
		return $this->isInChargeOfPolicy((int) $titre->id);
	}

	public function updateEditeursDirect(\Titre $titre): bool
	{
		$special = $this->getSpecialCase();
		if ($special !== null) {
			return $special;
		}
		if ($this->parent->getPartenaireId()) {
			// Current user is NOT an utilisateur-politiques => who's in charge of this Titre?
			return Suivi::checkDirectAccess($titre);
		}
		return $this->isInChargeOfPolicy((int) $titre->id);
	}

	/**
	 * Current utilisateur-politiques has a role on an Editeur that has a relation to this Titre.
	 */
	private function isInChargeOfPolicy(int $titreId): bool
	{
		return (bool) Yii::app()->db
			->createCommand(
				<<<EOSQL
				SELECT 1
				FROM Titre_Editeur te JOIN Utilisateur_Editeur ue USING(editeurId)
				WHERE te.titreId = :tid AND ue.utilisateurId = :uid AND ue.role IN ('guest', 'owner')
				LIMIT 1
				EOSQL
			)->queryScalar([':tid' => $titreId, ':uid' => $this->parent->getUtilisateurId()]);
	}
}
