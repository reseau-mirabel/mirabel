<?php

use components\app\WebApplication;

/**
 * @property \WebUser $user
 */
class Yii extends \YiiBase
{
	public static function createWebApplication($config = null): WebApplication
	{
		$app = new WebApplication($config);
		if ($app->getComponent('session', false) !== null) {
			self::initSessions();
		}
		return $app;
	}

	public static function app(): ?WebApplication
	{
		$app = parent::app();
		if (!($app instanceof WebApplication)) {
			return null;
		}
		return $app;
	}

	private static function initSessions(): void
	{
		$dir = DATA_DIR . '/sessions';
		if (!$dir) {
			throw new \Exception("Wrong session config.");
		}
		if (!is_dir($dir)) {
			@mkdir($dir);
		}
	}
}
