<?php

namespace components;

use CHtml;

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class HtmlTable
{
	public array $data = [];

	/**
	 * @var array Titles displayed at the top (by default, keys of the first row, if not numeric).
	 */
	public array $header = [];

	/**
	 * @var array Titles to display at the beginning of each line.
	 */
	public array $rowHeaders = [];

	public array $classes = [
		"table table-striped table-bordered table-condensed",
	];

	public array $htmlAttributes = [];

	private string $footnote = '';

	private bool $numberedColumn = false;

	/**
	 * @param ?array $header If empty, the header line will use the first row of data: extract the keys if it's an associative array.
	 * @param  array $rowHeaders If set, each row will start with a TH coming from this array.
	 */
	public function __construct(array $data = [], ?array $header = null, $rowHeaders = [])
	{
		$this->data = $data;
		if ($header === null) {
			$firstRow = reset($this->data);
			if (is_object($firstRow)) {
				$firstRow = (array) $firstRow;
			}
			if ($firstRow && !isset($firstRow[0])) {
				$header = array_keys($firstRow);
			}
		}
		if ($rowHeaders) {
			if ($header) {
				array_unshift($header, '');
			}
			$this->rowHeaders = $rowHeaders;
		}
		$this->header = $header ?? [];
	}

	public function __toString()
	{
		return $this->toHtml(true);
	}

	/**
	 * @param ?array $header If empty, the header line will use the first row of data: extract the keys if it's an associative array.
	 * @param  array $rowHeaders If set, each row will start with a TH coming from this array.
	 */
	public static function build(array $data, ?array $header = null, $rowHeaders = []): HtmlTable
	{
		return new self($data, $header, $rowHeaders);
	}

	public static function buildFromAssoc(array $data, bool $htmlEncode = true): HtmlTable
	{
		$rows = [];
		$rowHeaders = [];
		foreach ($data as $key => $value) {
			if (is_scalar($value)) {
				$value = (string) $value;
			} elseif (is_object($value) && method_exists($value, '__toString')) {
				$value = (string) $value;
			} else {
				$value = (string) json_encode($value);
			}
			$rowHeaders[] = $key;
			if ($htmlEncode) {
				$rows[] = [CHtml::encode($value)];
			} else {
				$rows[] = [$value];
			}
		}
		return new self($rows, [], $rowHeaders);
	}

	public function enableNumberedColumn(): self
	{
		$this->numberedColumn = true;
		$this->addClass('numbered');
		return $this;
	}

	/**
	 * @param string[] $headers
	 */
	public function setColumnHeaders(array $headers): HtmlTable
	{
		$this->header = $headers;
		return $this;
	}

	/**
	 * Set a footnote that will be displayed with %d replaced by the numer of lines.
	 *
	 * @param string $html
	 * @return HtmlTable
	 */
	public function setFootnote(string $html): HtmlTable
	{
		$this->footnote = $html;
		return $this;
	}

	public function addClass(string|array $class): HtmlTable
	{
		if (is_array($class)) {
			$this->classes = array_unique(array_merge($this->classes, $class));
		} else {
			$this->classes[] = $class;
		}
		return $this;
	}

	public function setHtmlAttributes(array $attributes): HtmlTable
	{
		$this->htmlAttributes = $attributes;
		return $this;
	}

	public function toHtml(bool $escapeData = false): string
	{
		if (!$this->data) {
			return "<em>Aucune donnée</em>";
		}
		$first = reset($this->data);
		if (!$first) {
			return '';
		}
		$html = '';
		if (!empty($this->header)) {
			$html .= '<thead><tr>';
			if ($this->numberedColumn) {
				$html .= "<th>#</th>";
			}
			foreach ($this->header as $colHead) {
				if (is_array($colHead)) {
					$v = array_shift($colHead);
					$html .= CHtml::tag('th', $colHead, CHtml::encode($v));
				} else {
					$html .= '<th>' . CHtml::encode($colHead) . '</th>';
				}
			}
			$html .= "</tr></thead>\n";
		}
		$html .= '<tbody>';
		$count = 0;
		foreach ($this->data as $row) {
			$html .= '<tr>';
			if ($this->numberedColumn) {
				$html .= sprintf("<td>%d</td>", $count + 1);
			}
			if ($this->rowHeaders) {
				$html .= "<td><strong>" . $this->rowHeaders[$count] . "</strong></td>";
			}
			if (is_scalar($row)) {
				$text = (string) $row;
				$html .= '<td>' . ($escapeData ? CHtml::encode($text) : $text) . '</td>';
			} else {
				if (is_object($row) && !is_iterable($row)) {
					$row = (array) $row;
				}
				foreach ($row as $v) {
					$text = (string) $v;
					$html .= '<td>' . ($escapeData ? CHtml::encode($text) : $text) . '</td>';
				}
			}
			$html .= '</tr>';
			$count++;
		}
		$html .= '</tbody>';

		$htmlOptions = array_merge($this->htmlAttributes, ['class' => join(' ', $this->classes)]);
		$table = CHtml::tag('table', $htmlOptions, $html);

		if ($this->footnote) {
			$table .= sprintf('<div>' . $this->footnote . '</div>', $count);
		}
		return $table;
	}
}
