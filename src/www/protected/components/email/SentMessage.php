<?php

namespace components\email;

use Symfony\Component\Mailer\SentMessage as SymfonySentMessage;

/**
 * A helper class to extract fields from a Symfony SentMessage.
 */
class SentMessage
{
	private SymfonySentMessage $message;

	public function __construct(SymfonySentMessage $message)
	{
		$this->message = $message;
	}

	public function getFirstRecipient(): string
	{
		$recipients = $this->message->getEnvelope()->getRecipients();
		assert(count($recipients) > 0);
		return $recipients[0]->toString();
	}

	/**
	 * @return string[] E.g. ['bibi@a.com', '"Jane Doe" <jd@b.com>']
	 */
	public function getRecipients(): array
	{
		$recipients = $this->message->getEnvelope()->getRecipients();
		return array_map(
			fn ($x) => $x->toString(),
			$recipients
		);
	}

	public function getBody(): string
	{
		$originalMessage = $this->message->getOriginalMessage();
		if ($originalMessage instanceof \Symfony\Component\Mime\Message) {
			$encoded = $originalMessage->getBody()->bodyToString();
			return str_replace(
				"\r\n",
				"\n",
				quoted_printable_decode($encoded)
			);
		}
		return "";
	}

	public function getSubject(): string
	{
		$originalMessage = $this->message->getOriginalMessage();
		if ($originalMessage instanceof \Symfony\Component\Mime\Message) {
			$encoded = $originalMessage->getHeaders()->get('subject')->getBodyAsString();
			return self::decode($encoded);
		}
		return "";
	}

	private static function decode(string $str): string
	{
		// BUG in mb_decode_mimeheader(), fixed with PHP 8.3
		// See https://www.php.net/manual/en/migration83.other-changes.php#migration83.other-changes.functions.mbstring
		if (PHP_VERSION_ID < 80300) {
			$str =  strtr($str, '_', ' ');
		}
		return mb_decode_mimeheader($str);
	}
}
