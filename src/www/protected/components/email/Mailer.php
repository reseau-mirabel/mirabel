<?php

namespace components\email;

use Config;
use Symfony\Component\Mailer\Mailer as SymfonyMailer;
use Symfony\Component\Mailer\Transport;
use Symfony\Component\Mailer\Transport\TransportInterface;
use Yii;

/**
 * Helper class for easier use of Symfony Mail.
 */
class Mailer
{
	protected static ?SymfonyMailer $mailer;

	private static ?TransportInterface $transport = null;

	/**
	 * Check if the configuration is complete.
	 */
	public static function checkConfig(): bool
	{
		if (!Config::read('email.from')) {
			Yii::log('Mailer: "email.from" is not configured', "error", 'mail');
			return false;
		}
		return true;
	}

	/**
	 * Sample:
	 *
	 *		$message = Mailer::newMail()
	 *			->subject($model->subject)
	 *			->from($model->email)
	 *			->setTo(array(Config::read('contact.email')))
	 *			->body($model->body);
	 */
	public static function newMail(): Message
	{
		$instance = new Message();
		$returnPath = Config::read('email.returnPath');
		if ($returnPath) {
			$instance->returnPath($returnPath);
		}
		return $instance;
	}

	/**
	 * Sends a message, built with ::newMail(), with the configured mail transport.
	 */
	public static function sendMail(Message $message): bool
	{
		$mailer = self::getMailer();
		try {
			$mailer->send($message);
		} catch (\Throwable $e) {
			Yii::log("Erreur d'envoi de courriel : {$e->getMessage()}", 'error', 'email');
			return false;
		}
		return true;
	}

	/**
	 * Use only when ::sendMail() is not suitable, e.g. sending multiple mail at once.
	 */
	public static function getMailer(?TransportInterface $transport = null): SymfonyMailer
	{
		if ($transport === null) {
			if (isset(self::$mailer)) {
				return self::$mailer;
			}
			$transport = self::getTransport();
		}
		self::$mailer = new SymfonyMailer($transport);
		return self::$mailer;
	}

	public static function getTransport(): TransportInterface
	{
		if (self::$transport === null) {
			if (defined('YII_ENV') && YII_ENV === 'test') {
				self::$transport = new TestTransport();
			} else {
				$mailHost = trim(Config::read('email.host'));
				if (!$mailHost) {
					self::$transport = new Transport\SendmailTransport();
				} elseif ($mailHost === 'debug') {
					Yii::log("Mails will be deleted and NOT SENT", "warning");
					self::$transport = new Transport\NullTransport();
				} else {
					self::$transport = Transport::fromDsn("smtp://$mailHost");
				}
			}
		}
		return self::$transport;
	}
}
