<?php

namespace components\email;

use Symfony\Component\Mailer\SentMessage;

class TestTransport extends \Symfony\Component\Mailer\Transport\AbstractTransport
{
	private int $sentCount = 0;

	/**
	 * @var SentMessage[]
	 */
	private array $sentMessages = [];

	public function __toString(): string
	{
		return 'test://';
	}

	public function getLastSentMessage(int $fromLast = 0): ?SentMessage
	{
		if (!$this->sentMessages) {
			return null;
		}
		return $this->sentMessages[count($this->sentMessages) - 1 - $fromLast] ?? null;
	}

	/**
	 * Return the overall number of recipients since the start of the script.
	 */
	public function getSentCount(): int
	{
		return $this->sentCount;
	}

	/**
	 * @return SentMessage[]
	 */
	public function getSentMessages(): array
	{
		return $this->sentMessages;
	}

	public function reset(): void
	{
		$this->sentCount = 0;
		$this->sentMessages = [];
	}

	protected function doSend(SentMessage $message): void
	{
		$this->sentCount += count($message->getEnvelope()->getRecipients());
		$this->sentMessages[] = $message;
	}
}
