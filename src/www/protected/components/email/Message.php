<?php

namespace components\email;

use Symfony\Component\Mime\Address;

/**
 * Wraps the symfony Email.
 */
class Message extends \Symfony\Component\Mime\Email
{
	/**
	 * Parse email addresses.
	 *
	 * Valid arguments (upstream):
	 * ("jean@machin.org")
	 * (["jean@machin.org"])
	 * (["jean@machin.org", "karim@machin.org"])
	 * (["jean@machin.org" => "Jean", "karim@machin.org" => "Karim"])
	 * ("jean@machin.org", "Jean") // 2 string arguments. Unspecified if first arg is an array
	 *
	 * New valid arguments:
	 * (["jean@machin.org\nkarim@machin.org"])
	 * (["jean@machin.org\nkarim@machin.org\n"])
	 * (["jean@machin.org Jean\nKarim karim@machin.org\nShinji <shinji@koto.org>"])
	 */
	public function setTo($addresses, ?string $name = null): Message
	{
		if (is_string($addresses)) {
			$addresses = trim($addresses, " \n");
			if (strpos($addresses, "\n") === false) {
				$to = [new Address($addresses, $name ?? '')];
			} elseif ($name !== null) {
				throw new \Exception("Multiple addresses with a single name");
			}
			$to = self::splitEmails($addresses);
		} elseif (is_array($addresses)) {
			$to = [];
			foreach ($addresses as $k => $v) {
				if ($v instanceof Address) {
					$to[] = $v;
				} elseif (strpos($k, '@') !== false) {
					$to[] = new Address($k, $v ?? '');
				} else {
					$to = array_merge($to, self::splitEmails($v));
				}
			}
		} else {
			throw new \Exception("Invalid addresses.");
		}
		foreach ($to as $a) {
			$this->addTo($a);
		}
		return $this;
	}

	/**
	 * Split the email string (one email per line) into a format suitable for Email::to().
	 *
	 * @return Address[]
	 */
	private static function splitEmails(string $addresses): array
	{
		$emails = array_filter(
			array_map('trim', explode("\n", $addresses)),
			function ($x) {
				return strpos($x, "@") !== false;
			}
		);
		$to = [];
		$matches = [];
		foreach ($emails as $line) {
			if (preg_match('/^(.+)\s+<(.+@.+\..+)>/', $line, $matches)) {
				$to[] = new Address($matches[2], trim($matches[1], ' "'));
			} elseif (preg_match('/^<(.+@.+\..+)>\s+(.+)$/', $line, $matches)) {
				$to[] = new Address($matches[1], trim($matches[2], ' "'));
			} elseif (preg_match('/^(.+)\s+(\S+@\S+\.\S+)$/', $line, $matches)) {
				$to[] = new Address($matches[2], trim($matches[1], ' "'));
			} elseif (preg_match('/^(\S+@\S+\.\S+)\s+(.+)$/', $line, $matches)) {
				$to[] = new Address($matches[1], trim($matches[2], ' "'));
			} else {
				$to[] = new Address($line);
			}
		}
		return $to;
	}
}
