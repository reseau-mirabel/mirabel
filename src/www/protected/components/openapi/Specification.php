<?php

namespace components\openapi;

class Specification
{
	public $info;

	public $host;

	public $schemes;

	public $security;

	public $basePath;

	public $produces;

	public $paths;

	public $components;

	public function __construct($values)
	{
		foreach ($values as $k => $v) {
			$k = str_replace('$', '', $k);
			if (property_exists($this, $k)) {
				$this->{$k} = $v;
			}
		}
	}

	public function getBaseUrl(): string
	{
		$url = '';
		if (!empty($this->schemes) && !empty($this->host)) {
			$url = $this->schemes[0] . '://';
		}
		if (!empty($this->host)) {
			if (empty($url)) {
				$url = "//";
			}
			$url .= $this->host;
		}
		if (!empty($this->basePath)) {
			if ($url) {
				$url .= ltrim($this->basePath, '/');
			} else {
				$url = $this->basePath;
			}
		}
		return $url;
	}
}
