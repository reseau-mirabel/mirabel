<?php

namespace components\sphinx;

class Criteria extends \CDbCriteria
{
	public array $options = [];

	/**
	 * @var array<string, FacetInterface>
	 */
	private array $facets = [];

	/**
	 * @inheritDoc
	 */
	public function addCondition($condition, $operator = 'AND')
	{
		if ($condition === '' || $condition === []) {
			return $this;
		}
		if (is_array($condition)) {
			$condition = '(' . implode(') ' . $operator . ' (', $condition) . ')';
		}
		if ($this->condition === '') {
			$this->condition = $condition;
		} else {
			$this->condition = "{$this->condition} $operator ($condition)";
		}
		return $this;
	}

	public function addFacet(FacetInterface $facet): void
	{
		$this->facets[$facet->getName()] = $facet;
	}

	/**
	 * @return array<string, FacetInterface>
	 */
	public function getFacets(): array
	{
		return $this->facets;
	}
}
