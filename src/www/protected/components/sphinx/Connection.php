<?php

namespace components\sphinx;

class Connection extends \CDbConnection
{
	public $driverMap = [
		'mysql' => Schema::class,
	];

	/**
	 * @param mixed $query
	 * @return Command
	 */
	public function createCommand($query = null)
	{
		$this->setActive(true);
		return new Command($this, $query);
	}
}
