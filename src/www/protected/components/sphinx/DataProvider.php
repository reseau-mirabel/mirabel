<?php

namespace components\sphinx;

class DataProvider extends \CActiveDataProvider
{
	/**
	 * Sphinx Search has an implicit 0,20 limit that we must overwrite.
	 */
	private const PAGINATION_MAX = 10000;

	/**
	 * @var ?\CActiveRecord If set, this ActiveRecord model will be used to fetch source records from \Yii::app()->db
	 */
	public $source;

	/**
	 * @var FacetInterface[]
	 */
	private array $facets = [];

	/**
	 * @inheritdoc
	 */
	public function fetchData(): array
	{
		$data = $this->fetchDataInner();

		if ($this->source && $data) {
			for ($i = 0; $i < count($data); $i += 500) {
				$this->fillData($data, $i, $i+500);
			}
		}
		return $data;
	}

	public function getFacet(string $name): ?FacetInterface
	{
		return $this->facets[$name] ?? null;
	}

	/**
	 * @return FacetInterface[]
	 */
	public function getFacets(): array
	{
		if (!$this->facets) {
			$this->getData();
		}
		return $this->facets;
	}

	/**
	 * @inheritdoc
	 */
	public function getId()
	{
		return '';
	}

	/**
	 * @inheritdoc
	 */
	public function setPagination($value): void
	{
		if ($value === false) {
			$value = ['pageSize' => self::PAGINATION_MAX];
		}
		parent::setPagination($value);
	}

	protected function fetchDataInner(): array
	{
		$criteria = clone $this->getCriteria();
		$baseCriteria = $this->model->getDbCriteria(false);

		$pagination = $this->getPagination();
		if ($pagination !== false) {
			$pagination->setItemCount(10000);
			$pagination->getCurrentPage(true);
			$pagination->applyLimit($criteria);
		}

		$sort = $this->getSort();
		if ($sort !== false) {
			// set model criteria so that CSort can use its table alias setting
			if ($baseCriteria !== null) {
				$c = clone $baseCriteria;
				$c->mergeWith($criteria);
				$this->model->setDbCriteria($c);
			} else {
				$this->model->setDbCriteria($criteria);
			}
			$sort->applyOrder($criteria);
		}

		$this->model->setDbCriteria($baseCriteria !== null ? clone $baseCriteria : null);
		$data = $this->findAll($this->model, $criteria);
		$total = $this->fetchTotalItemCount();
		$this->setTotalItemCount($total);

		if ($pagination !== false) {
			$pagination->setItemCount($total);
			$pagination->getCurrentPage(true);
		}

		$this->model->setDbCriteria($baseCriteria);  // restore original criteria
		return $data;
	}

	/**
	 * @inheritdoc
	 */
	protected function calculateTotalItemCount()
	{
		// fetchData() receives the total item count just after reading the data.
		$this->fetchData();
		return $this->getTotalItemCount(false);
	}

	protected function fetchTotalItemCount(): int
	{
		$row = $this->model->commandBuilder->createSqlCommand("SHOW META LIKE 'total_found'")->queryRow(false);
		return (int) ($row[1] ?? 0);
	}

	/**
	 * For each item in the Sphinx results (e.g. Titres), fill it with the record from the DB (e.g. Titre).
	 */
	private function fillData(array &$data, int $from, int $to): void
	{
		$ids = [];
		if ($to > count($data)) {
			$to = count($data);
		}
		for ($i = $from; $i < $to; $i++) {
			$ids[] = (int) $data[$i]->id;
		}
		$criteria = new Criteria();
		$criteria->addCondition('id IN (' . join(',', $ids) . ')');
		$criteria->index = 'id';
		$sources = $this->source->findAll($criteria);
		for ($i = $from; $i < $to; $i++) {
			if (isset($sources[$data[$i]->id])) {
				$data[$i]->setRecord($sources[$data[$i]->id]);
			}
		}
	}

	private function findAll(\CActiveRecord $model, \CDbCriteria $criteria): array
	{
		$command = $model->getDbConnection()->getCommandBuilder()
			->createFindCommand($model->getTableSchema(), $criteria);
		assert($command instanceof Command);
		$command->prepare();
		$statement = $command->getPdoStatement();
		if (!$command->params) {
			$statement->execute();
		} else {
			$statement->execute($command->params);
		}
		$statement->setFetchMode(\PDO::FETCH_ASSOC);
		$main = $model->populateRecords($statement->fetchAll(), true, $criteria->index);
		if ($criteria instanceof Criteria) {
			foreach ($criteria->getFacets() as $facetName => $facet) {
				$statement->nextRowset();
				$facet->loadResults($statement->fetchAll(\PDO::FETCH_NUM));
				$this->facets[$facetName] = $facet;
			}
		}
		return $main;
	}
}
