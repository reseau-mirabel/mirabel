<?php

namespace components\sphinx;

class Command extends \CDbCommand
{
	protected $options = [];

	public function addOption(string $name, $value): self
	{
		$this->options[$name] = $value;
		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function group($columns)
	{
		if (is_array($columns) && !isset($columns[0])) {
			$groups = [];
			foreach ($columns as $col => $sql) {
				/*
				 * The CDbCommand API is insane.
				 * It has private attributes, so overload is impossible.
				 * CDbCommand::group() tries to guess if the parameter (array|object|scalar) has to be escaped.
				 * The only way to pass a raw GROUP BY value that does not contain "("
				 *  is to wrap it inside an object.
				 */
				$groups[] = new class("$col $sql") {
					private $group;

					public function __construct(string $gs)
					{
						$this->group = $gs;
					}

					public function __toString()
					{
						return $this->group;
					}
				};
			}
			$columns = $groups;
		}
		return parent::group($columns);
	}

	public function buildQuery($query)
	{
		$sql = parent::buildQuery($query);
		$builder = $this->getConnection()->getCommandBuilder();
		assert($builder instanceof CommandBuilder);
		return $builder->applyOptions($sql, $this->options);
	}
}
