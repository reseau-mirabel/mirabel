<?php

namespace components\sphinx;

class Schema extends \CMysqlSchema
{
	public function quoteTableName($name)
	{
		if ($name === '') {
			return '';
		}
		return parent::quoteColumnName($name);
	}

	public static function quoteFulltext(?string $value)
	{
		if ($value === null) {
			return null;
		}

		$from = ['\\', '(', ')', '|', '-', '!', '@', '~', '"', '&', '/', '^', '$', '=', '<', "'"];
		$to   = [ ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '];
		return str_replace($from, $to, $value);
	}

	protected function createCommandBuilder()
	{
		return new CommandBuilder($this);
	}

	/**
	 * Collects the table column metadata.
	 *
	 * @param \CMysqlTableSchema $table the table metadata
	 * @return bool whether the table exists in the database
	 */
	protected function findColumns($table)
	{
		$sql = 'DESCRIBE ' . $table->rawName;
		try {
			$columns = $this->getDbConnection()->createCommand($sql)->queryAll();
		} catch (\Exception $_) {
			return false;
		}
		if (isset($columns[0]["Agent"]) && ($columns[0]["Type"] ?? '') === 'local') {
			$columns = $this->getDbConnection()->createCommand("DESCRIBE {$columns[0]["Agent"]}")->queryAll();
			;
		}
		foreach ($columns as $column) {
			try {
				$c = $this->createColumn($column);
				if ($c instanceof \CMysqlColumnSchema) {
					$table->columns[$c->name] = $c;
					if ($c->isPrimaryKey) {
						$table->primaryKey = $c->name;
					}
				}
			} catch (\Throwable $_) {
			}
		}
		return true;
	}

	/**
	 * Creates a table column.
	 * @param array $column column metadata
	 * @return \CDbColumnSchema normalized column metadata
	 */
	protected function createColumn($column)
	{
		$c = new \CMysqlColumnSchema;
		$c->name = $column['Field'];
		$c->rawName = $this->quoteColumnName($c->name);
		$c->isPrimaryKey = ($column['Field'] === 'id');
		switch ($column['Type']) {
			case 'bigint':
				$c->init('int', 0);
				break;
			case 'field':
				throw new \Exception("Cannot read from a 'field' column that is search-only.");
			case 'mva':
				$c->dbType = 'mva';
				$c->allowNull = false;
				$c->defaultValue = [];
				$c->type = 'int[]';
				break;
			case 'timestamp':
				$c->init('int', 0);
				break;
			case 'uint':
				$c->init('unsigned int', 0);
				break;
			default:
				$c->init('text', '');
				break;
		}
		return $c;
	}

	/**
	 * Collects the foreign key column details for the given table.
	 * @param \CMysqlTableSchema $table the table metadata
	 */
	protected function findConstraints($table)
	{
	}

	protected function resolveTableNames($table, $name)
	{
		parent::resolveTableNames($table, $name);
		$table->rawName = $table->name;
	}
}
