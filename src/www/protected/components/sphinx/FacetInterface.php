<?php

namespace components\sphinx;

interface FacetInterface
{
	/**
	 * Return the facet name, usually a numeric field name like 'editeurid'.
	 */
	public function getName(): string;

	/**
	 * Return the (usually empty) additional SQL at the end of the FACET line.
	 */
	public function getExtraSql(): string;

	/**
	 * Remove some values from the facet.
	 */
	public function exclude(array $values): self;

	/**
	 * List the filters [value => [name, count]] induced by the facets values.
	 */
	public function listFilters(): array;

	/**
	 * Load the raw Sphinx results for this facets (a rowset after the main results).
	 */
	public function loadResults(array $results): void;
}
