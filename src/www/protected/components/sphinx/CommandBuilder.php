<?php

namespace components\sphinx;

class CommandBuilder extends \CDbCommandBuilder
{
	private $extraOptions = [
		// Better estimation of the number of results, when the query uses GROUP BY.
		// (default 1000)
		'max_matches' => 1500,
	];

	public function applyFacets(string $sql, array $facets): string
	{
		foreach ($facets as $f) {
			$sql .= " FACET {$f->getName()} {$f->getExtraSql()}";
		}
		return $sql;
	}

	/**
	 * @inheritdoc
	 */
	public function applyLimit($sql, $limit, $offset)
	{
		if ($limit >= 0) {
			if ($offset > 0) {
				$sql .= ' LIMIT ' . (int) $offset . ',' . (int) $limit;
			} else {
				$sql .= ' LIMIT ' . (int) $limit;
			}
			if ($offset + $limit > $this->extraOptions['max_matches']) {
				$this->extraOptions['max_matches'] = $offset + $limit + 1;
			}
		}
		return $sql;
	}

	public function applyOptions(string $sql, array $options): string
	{
		if ($options || $this->extraOptions) {
			$optionsSql = [];
			foreach (array_merge($options, $this->extraOptions) as $k => $v) {
				$optionsSql[] = "$k=$v";
			}
			$sql .= " OPTION " . join(", ", $optionsSql);
		}
		return $sql;
	}

	/**
	 * Creates a SELECT command for a single table. __Ignores the alias.__
	 *
	 * @param mixed $table the table schema or the table name: string|\CDbTableSchema
	 * @param \CDbCriteria $criteria the query criteria
	 * @param string $alias the alias name of the primary table. Defaults to 't'.
	 * @return \CDbCommand query command.
	 */
	public function createFindCommand($table, $criteria, $alias = 't')
	{
		if ($table instanceof \CDbTableSchema) {
			$tableName = $table->rawName;
		} else {
			$tableName = $table;
		}
		$select = is_array($criteria->select) ? implode(', ', $criteria->select) : $criteria->select;
		$sql = ($criteria->distinct ? 'SELECT DISTINCT' : 'SELECT') . " {$select} FROM {$tableName}";
		$sql = $this->applyJoin($sql, $criteria->join);
		$sql = $this->applyCondition($sql, $criteria->condition);
		$sql = $this->applyGroup($sql, $criteria->group);
		$sql = $this->applyHaving($sql, $criteria->having);
		$sql = $this->applyOrder($sql, $criteria->order);
		$sql = $this->applyLimit($sql, $criteria->limit, $criteria->offset);
		if ($criteria instanceof Criteria) {
			$sql = $this->applyOptions($sql, $criteria->options);
			$sql = $this->applyFacets($sql, $criteria->getFacets());
		} else {
			$sql = $this->applyOptions($sql, []);
		}
		$command = $this->dbConnection->createCommand($sql);
		$this->bindValues($command, $criteria->params);
		return $command;
	}

	public function createColumnCriteria($table, $columns, $condition='', $params=[], $prefix=null)
	{
		return parent::createColumnCriteria($table, $columns, $condition, $params, '');
	}

	public function createCountCommand($table, $criteria, $alias='t')
	{
		return parent::createCountCommand($table, $criteria, '');
	}

	public function createInCondition($table, $columnName, $values, $prefix = null)
	{
		return parent::createInCondition($table, $columnName, $values, '');
	}
}
