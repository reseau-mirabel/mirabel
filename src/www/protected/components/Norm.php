<?php

/**
 * Static methods to normalize strings.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class Norm
{
	/**
	 * Shorten an UTF-8 text, replacing the truncated part with "…".
	 */
	public static function shortenText(string $text, int $maxSize): string
	{
		$normed = (string) \Normalizer::normalize($text, \Normalizer::FORM_C);
		if (\mb_strlen($normed) > $maxSize) {
			return \mb_substr($normed, 0, $maxSize - 1) . "…";
		}
		return $normed;
	}

	public static function urlParam(string $urlPart): string
	{
		return trim(
			preg_replace(
				['/(\w)@(\w)/', '/[^\w_-]/', '/--+/'],
				['{$1}a{$2}', '', '-'],
				str_replace(
					[' & ', ' -- ', ' : ', ' ', "'", ' !', ' ?', ' ;', ';', '/', '.'],
					['-et-', '-', '-', '-', "-", '', '', '-', '-', '-', '-'],
					iconv(
						'UTF-8',
						'ASCII//TRANSLIT//IGNORE',
						self::unaccent($urlPart)
					))
			),
			"_-"
		);
	}

	public static function unaccent(string $text): string
	{
		return (string) preg_replace(
			'/[\x{0300}-\x{036f}]/u',
			'',
			(string) \Normalizer::normalize($text, Normalizer::FORM_D)
		);
	}

	/**
	 * Normalize the UTF-8 (keeping diacritics) and the various '´
	 */
	public static function text(string $text): string
	{
		if ($text === '') {
			return '';
		}
		if (extension_loaded('intl')) {
			return str_replace(
				["’", "´"],
				["'", "'"],
				(string) \Normalizer::normalize($text) // ext intl
			);
		}
		return str_replace(
			["’", "´"],
			["'", "'"],
			$text
		);
	}
}
