<?php

namespace components;

use Yii;

class YiiAccessToken extends AccessToken
{
	public const COOKIE_NAME = 'access-token';

	/**
	 * Default values are the current Yii route (/revue/view) and the current time.
	 */
	public function __construct(string $path = "", int $time = 0)
	{
		if ($path === '') {
			$path = Yii::app()->getController()->getRoute();
		}
		if ($time === 0) {
			$time = (int) $_SERVER['REQUEST_TIME'];
		}
		parent::__construct($path, $time);
	}

	/**
	 * Check if the current request has a cookie that grants access to this route.
	 *
	 * The result matche one one the RES_* constants.
	 */
	public function hasToken(): int
	{
		$request = Yii::app()->getRequest();
		$accessToken = $request->getCookies()[self::COOKIE_NAME] ?? '';
		return $this->checkToken($accessToken);
	}
}
