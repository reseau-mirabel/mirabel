<?php

namespace components;

/**
 * Display details about logs at the bottom of HTML pages.
 *
 * This class replaces CWebLogRoute which is buggy with non HTML requests,
 * e.g. expecting a JSON response or sent by JS fetch().
 * It is meant to be active only in dev mode, see config/debug.php.
 */
class WebLogRoute extends \CLogRoute
{
	/**
	 * @inheritdoc
	 */
	public function processLogs($logs)
	{
		$this->render($logs);
	}

	/**
	 * @param array $data data to be passed to the view
	 */
	protected function render($data): void
	{
		$app = \Yii::app();
		$isAjax = $app->getRequest()->getIsAjaxRequest();

		if (!($app instanceof \CWebApplication) || $isAjax || !self::isHtmlResponse()) {
			return;
		}

		$viewFile = YII_PATH . "/views/log.php";
		include($app->findLocalizedFile($viewFile, 'en'));
	}

	private static function isHtmlResponse(): bool
	{
		foreach (headers_list() as $header) {
			if (strncasecmp($header, "Content-Type:", 13) === 0) {
				return strpos($header, "text/html", 13) !== false;
			}
		}
		return false;
	}
}
