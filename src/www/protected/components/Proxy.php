<?php

/**
 * static proxy methods.
 */
class Proxy
{
	public const PATTERN_GP = 'GPURL';

	public const PATTERN_EZ = 'URL';

	public static function proxifyUrl(string $url, string $proxyUrl): string
	{
		if (strpos($proxyUrl, self::PATTERN_GP) !== false) {
			$m = [];
			if (!preg_match('!^(https?)://(.*)$!', $url, $m)) {
				return $url;
			}
			[, $protocol, $address] = $m;
			return str_replace(self::PATTERN_GP, "$protocol/$address", $proxyUrl);
		}
		return str_replace(self::PATTERN_EZ, urlencode($url), $proxyUrl);
	}
}
