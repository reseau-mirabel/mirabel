<?php

namespace components\app;

use CFileHelper;

class StaticFileRoute
{
	public static function getPathToFile(string $webPath): string
	{
		if (str_starts_with($webPath, 'assets/')) {
			return self::tryFiles($webPath, ["cache"]);
		}
		if (str_starts_with($webPath, 'public/images/')) {
			return self::tryFiles($webPath, [""]);
		}
		if (preg_match('#^public/[^/]+$#', $webPath)) {
			// File directly under public/ (no subdir)
			return self::tryFiles($webPath, ["", "upload"]); // /public/X, then /upload/public/X
		}
		if (str_starts_with($webPath, 'images/videos/')) {
			$webPath = str_replace('images/videos/', '', $webPath);
			return self::tryFiles($webPath, ["upload/videos"]);
		}
		if (str_starts_with($webPath, 'images/logos/h55/')) {
			$webPath = str_replace('images/logos/h55/', 'images/partenaires/', $webPath);
			return self::tryFiles($webPath, ["public"]);
		}
		if (str_starts_with($webPath, 'images/ressources-logos/h55/')) {
			$webPath = str_replace('images/ressources-logos/h55/', 'images/ressources/', $webPath);
			return self::tryFiles($webPath, ["public"]);
		}
		return self::tryFiles($webPath, ["public"]);
	}

	public static function serve(string $serverPath): void
	{
		// Guess the mime type, then send the appropriate header.
		$mime = CFileHelper::getMimeTypeByExtension($serverPath);
		if (!$mime) {
			$mime = CFileHelper::getMimeType($serverPath);
		}
		if (!$mime) {
			$mime = '';
		}
		self::sendHttpHeader($mime, $serverPath);

		readfile($serverPath);
	}

	private static function sendHttpHeader(string $mime, string $file): void
	{
		$encFile = rawurlencode(basename($file));

		if (!$mime) {
			header('Content-Type: application/force-download');
			header('Content-Disposition: attachment;filename=' . $encFile);
			return;
		}

		$char = '';
		if (strncmp($mime, 'text/', 5)) {
			$char = '; charset="UTF-8"';
		}
		header('Content-Type: ' . $mime . $char);
		if ($mime === 'application/pdf') {
			header("Content-Disposition: inline; filename=" . $encFile);
		}
	}

	private static function tryFiles(string $webPath, array $dirs): string
	{
		foreach ($dirs as $dir) {
			$base = rtrim(DATA_DIR . "/" . trim($dir, '/'), '/');
			$target = realpath("$base/$webPath");
			if ($target && is_file($target) && str_starts_with($target, $base)) {
				return $target;
			}
		}
		return "";
	}
}
