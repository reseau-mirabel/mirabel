<?php

namespace components\app;

/**
 * @property \Bootstrap $bootstrap
 * @property \Controller $controller The currently active controller.
 * @property \extensions\format\ExtFormatter $format
 * @property \WebUser $user The user session information.
 */
class WebApplication extends \CWebApplication
{
	use \components\app\ComponentClosureTrait;

	private bool $ended = false;

	public function __construct($config = null)
	{
		parent::__construct($config);
		\Yii::setPathOfAlias('approot', dirname(__DIR__, 2));
		\Yii::setPathOfAlias('webroot', dirname(__DIR__, 3));
	}

	public function end($status = 0, $exit = true)
	{
		$this->ended = true;
		parent::end($status, $exit);
	}

	public function hasEnded(): bool
	{
		return $this->ended;
	}

	/**
	 * @inheritdoc
	 *
	 * Modify parent method to normalize "controller/action-name" into "controller/actionName".
	 */
	public function processRequest()
	{
		// Prepend a hook for serving static files from data/public.
		// Si le chemin correspond à un fichier de données public, on court-circuite Yii.
		// (Équivalent de try_files dans Nginx, ce que ne permet pas Apache ?
		$pathInfo = $this->request->getPathInfo();
		if (is_string($pathInfo) && $pathInfo !== '') {
			$path = StaticFileRoute::getPathToFile($pathInfo);
			if ($path) {
				StaticFileRoute::serve($path);
				exit(0);
			}
		}

		if (is_array($this->catchAllRequest) && isset($this->catchAllRequest[0])) {
			$route = $this->catchAllRequest[0];
			foreach (array_splice($this->catchAllRequest, 1) as $name => $value) {
				$_GET[$name] = $value;
			}
		} else {
			$route = $this->getUrlManager()->parseUrl($this->getRequest());
		}

		$this->runController($route);
	}
}
