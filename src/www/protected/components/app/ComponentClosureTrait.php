<?php

namespace components\app;

trait ComponentClosureTrait
{
	/**
	 * @var array<string, callable|object> Array of functions, that completes the "regular" components.
	 */
	private array $callableComponents = [];

	/**
	 * @psalm-suppress LessSpecificImplementedReturnType
	 * @return null|\IApplicationComponent|object
	 */
	public function getComponent($id, $createIfNull = true)
	{
		$key = (string) $id;
		if (isset($this->callableComponents[$key])) {
			$component = $this->callableComponents[$key];
			if (is_object($component) && ($component instanceof \Closure)) { // a function ?
				// Replace the component function by the result of its call.
				$this->callableComponents[$key] = call_user_func($component);
			}
			if (is_object($this->callableComponents[$key])) {
				return $this->callableComponents[$key];
			}
			throw new \Exception("A Yii application component is not a CComponent instance: $id");
		}
		return parent::getComponent($id, $createIfNull);
	}

	/**
	 * @param \Closure|array|\IApplicationComponent|null $component application component
	 */
	public function setComponent($id, $component, $merge = true)
	{
		if ($component !== null && is_object($component) && ($component instanceof \Closure)) {
			// Store the function.
			$this->callableComponents[(string) $id] = $component;
		} else {
			parent::setComponent($id, $component, $merge);
		}
	}
}
