<?php

namespace components;

use CHtml;
use LinkChecker;
use Norm;
use Yii;
use models\sphinx\Editeurs;
use models\sphinx\Ressources;
use models\sphinx\Titres;

/**
 * Contains static functions that produce HTML.
 */
class HtmlHelper
{
	public static function titleLinkItem(Titres $result, string $hash = ""): string
	{
		$url = ['/revue/view', 'id' => $result->revueid, 'nom' => Norm::urlParam($result->titrecomplet)];
		if ($hash) {
			$url['s'] = $hash;
		}
		return '<div class="' . self::linkItemClass('titre', $result->getSuiviIds()) . '">'
			. CHtml::link(
				CHtml::encode($result->titrecomplet),
				$url
			)
			. "</div>";
	}

	public static function ressourceLinkItem(Ressources $result, ?string $hash = null): string
	{
		$url = ['/ressource/view', 'id' => $result->id, 'nom' => Norm::urlParam($result->nomcomplet)];
		if ($hash) {
			$url['s'] = $hash;
		}
		return '<div class="' . self::linkItemClass('ressource', $result->getSuiviIds()) . '">'
			. CHtml::link(
				CHtml::encode($result->nomcomplet),
				$url
			)
			. "</div>";
	}

	public static function editeurLinkItem(Editeurs $result, ?string $hash = null): string
	{
		$url = ['/editeur/view', 'id' => $result->id, 'nom' => Norm::urlParam($result->nomcomplet)];
		if ($hash) {
			$url['s'] = $hash;
		}
		return '<div class="' . self::linkItemClass('editeur', $result->getSuiviIds()) . '">'
			. CHtml::link(
				CHtml::encode($result->nomcomplet),
				$url
			)
			. "</div>";
	}

	public static function linkItemClass(string $default, array $suivi): string
	{
		if (empty($suivi)) {
			return "$default suivi-none";
		}

		if (!Yii::app()->user->checkAccess("avec-partenaire")) {
			$suiviSelf = false;
		} else {
			if (!isset($suivi['model'])) {
				$suiviSelf = in_array(Yii::app()->user->partenaireId, $suivi);
			} else {
				$suiviSelf = Yii::app()->user->isMonitoring($suivi);
			}
		}
		return $default . ($suiviSelf ? ' suivi-self' : ' suivi-other');
	}

	public static function table(array $a, string $classes = ""): string
	{
		$first = reset($a);
		if (!$first) {
			return '';
		}
		$html = '<table class="table table-striped table-bordered table-condensed ' . $classes . '">'
			. '<thead><tr>';
		foreach (array_keys($first) as $colHead) {
			$html .= '<th>' . CHtml::encode($colHead) . '</th>';
		}
		$html .= '</tr></thead><tbody>';
		foreach ($a as $row) {
			$html .= '<tr>';
			foreach ($row as $v) {
				$html .= '<td>' . $v . '</td>';
			}
			$html .= '</tr>';
		}
		$html .= '</tbody></table>';
		return $html;
	}

	public static function displayLinksHint(\BootActiveForm $form, string $title, string $name, int $rank, int $position): string
	{
		if ($position === $rank) {
			return self::getPopoverHint($form->hints[$name] ?? '', $title);
		}
		return '';
	}

	public static function getPopoverHint(string|array $hint, string $title = ''): string
	{
		if ($hint === '') {
			return '';
		}
		if (is_array($hint)) {
			$hintTitle = (string) $hint[1];
			$hintContent = (string) $hint[0];
		} else {
			$hintTitle = $title ?: "Aide";
			$hintContent = (string) $hint;
		}
		return CHtml::tag(
			'a',
			[
				'href' => '#',
				'data-title' => $hintTitle,
				'data-content' => $hintContent,
				'data-html' => true,
				'data-trigger' => 'hover',
				'rel' => 'popover',
			],
			'?'
		);
	}

	public static function postButton(string $label, string|array $url, array $hiddenParams = [], array $htmlOptions = []): string
	{
		$htmlOptions['type'] = 'submit';
		return CHtml::form($url, 'post', ['style' => 'margin:0'])
			. join(
				"\n",
				array_map(
					function ($k, $v) {
						return CHtml::hiddenField($k, $v);
					},
					array_keys($hiddenParams),
					array_values($hiddenParams)
				)
			)
			. CHtml::htmlButton($label, $htmlOptions)
			. CHtml::endForm();
	}

	public static function definitionList(array $list, ?array $labelCallback = null): string
	{
		if (!$list) {
			return "";
		}
		$html = '<dl>';
		foreach ($list as $k => $v) {
			if ($labelCallback) {
				$label = call_user_func($labelCallback, $k);
			} else {
				$label = $k;
			}
			$html .= '<dt>' . CHtml::encode($label) . '</dt>'
				. '<dd>' . CHtml::encode($v) . '</dd>';
		}
		$html .= "</dl>\n";
		return $html;
	}

	/**
	 * Return a function(title, url) that creates the TR for each URL.
	 *
	 * @param array $checks Assoc array [ URL => true|false ]
	 * @return callable
	 */
	public static function builderCheckLinks(array $checks)
	{
		return function ($url, $title = null) use ($checks) {
			[$success, $msg] = $checks[$url];
			$class = '';
			if ($url) {
				if ($success === LinkChecker::RESULT_SUCCESS) {
					$msg = 'OK';
					$class = 'success';
				} elseif ($success === LinkChecker::RESULT_IGNORED) {
					$msg = 'non-testable<div>Cette URL ne peut être testée automatiquement (protection anti-robots)</div>';
					$class = 'alert-info';
				} else {
					$msg = 'Erreur<div>' . CHtml::encode($msg) . '</div>';
					$class = 'error';
				}
			}
			return "<tr class=\"$class\">"
				. (isset($title) ? "<td>" . $title . "</td>" : '')
				. "<td>" . CHtml::link(CHtml::encode($url), $url) . "</td>"
				. "<td>$msg</td>"
				. "</tr>\n";
		};
	}

	/**
	 * Like CHtml::errorSummary, with 3 differences:
	 * - Bootstrap CSS classes
	 * - Does not HTML encode the errors
	 * - Special conversion of the DuplicatesValidator error message.
	 *
	 * @param \CModel[]|\CModel $model
	 * @param string|null $header
	 * @param string|null $footer
	 * @param array $htmlOptions
	 * @return string HTML
	 */
	public static function errorSummary(mixed $model, ?string $header = null, ?string $footer = null, array $htmlOptions = []): string
	{
		if (!isset($htmlOptions['class'])) {
			// Bootstrap error class as default
			$htmlOptions['class'] = 'alert alert-block alert-error';
		}

		$content = '';
		if (!is_array($model)) {
			$model = [$model];
		}
		if (isset($htmlOptions['firstError'])) {
			$firstError = $htmlOptions['firstError'];
			unset($htmlOptions['firstError']);
		} else {
			$firstError = false;
		}
		$pattern = \models\validators\DuplicatesValidator::PATTERN;
		foreach ($model as $m) {
			foreach ($m->getErrors() as $field => $errors) {
				foreach ($errors as $error) {
					if (strncmp($error, $pattern, strlen($pattern)) === 0) {
						$content .= "<li>{$m->getAttributeLabel($field)} : vous devez confirmer qu'il ne s'agit pas d'un doublon.</li>\n";
					} elseif ($error != '') {
						$content .= "<li>$error</li>\n";
					}
					if ($firstError) {
						break;
					}
				}
			}
		}
		if ($content !== '') {
			if ($header === null) {
				$header = '<p>' . Yii::t('yii', 'Please fix the following input errors:') . '</p>';
			}
			return CHtml::tag('div', $htmlOptions, $header . "\n<ul>\n$content</ul>" . $footer);
		}
		return '';
	}

	/**
	 * Returns an HTML table with 2 columns.
	 *
	 * @codeCoverageIgnore
	 *
	 * @param array $table
	 * @return string HTML table.
	 */
	public static function htmlAssocTable(array $table): string
	{
		$html = '<table class="table table-striped table-bordered table-condensed"><tbody>';
		foreach ($table as $key => $value) {
			if (!is_scalar($value)) {
				$value = json_encode($value);
			}
			$html .= '<tr><th>' . CHtml::encode($key) . '</th><td>'
				. CHtml::encode((string) $value) . "</td></tr>\n";
		}
		$html .= "</tbody></table>\n";
		return $html;
	}

	/**
	 * Returns an HTML table with 2 columns.
	 *
	 * @codeCoverageIgnore
	 *
	 * @param array $table
	 * @param bool $thFirst (opt, true)
	 * @param bool $encode HTML encode (opt, true)
	 * @return string HTML table.
	 */
	public static function htmlTable(array $table, bool $thFirst = true, bool $encode = true): string
	{
		$html = '<table class="table table-striped table-bordered table-condensed"><tbody>';
		foreach ($table as $values) {
			$html .= '<tr>';
			$th = $thFirst;
			foreach ($values as $name => $value) {
				if (!is_scalar($value)) {
					if (is_object($value) && method_exists($value, 'toHtml')) {
						$content = $value->toHtml();
					} else {
						$content = CHtml::encode(json_encode($value));
					}
				} elseif (preg_match('/url$/i', $name)) {
					$content = CHtml::link((string) $value, (string) $value);
				} elseif ($encode) {
					$content = CHtml::encode((string) $value);
				} else {
					$content = (string) $value;
				}
				if ($th) {
					$html .= '<th>' . $content . '</th>';
					$th = false;
				} else {
					$html .= '<td>' . $content . '</td>';
				}
			}
			$html .= "</tr>\n";
		}
		$html .= "</tbody></table>\n";
		return $html;
	}

	/**
	 * retourne un nom standardisé pour les tables exportables en csv, attribut "data-exportable-filename"
	 * @staticvar string $base
	 */
	public static function exportableName(string $name): string
	{
		static $base = '';
		if ($base === '') {
			$base = \Norm::urlParam(\Yii::app()->name) . '_{{name}}_' . date('Y-m-d') . '.csv';
		}
		return (string) str_replace('{{name}}', $name, $base);
	}

	public static function exportableNameAttribute(string $name): string
	{
		return 'data-exportable-filename="' . CHtml::encode(self::exportableName($name)) . '"';
	}
}
