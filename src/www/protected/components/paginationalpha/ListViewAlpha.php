<?php

namespace components\paginationalpha;

/**
 * ListViewAlpha has:
 *
 * 1. a "pager" section like CListView (using BootPager, applied to `$this->dataProvider->getPagination()`),
 * 2. a "letterSelector" section (using PagerAlpha, applied to `$this->getLetterPagination()`).
 */
class ListViewAlpha extends \CBaseListView
{
	/**
	 * @var string the view used for rendering each data item.
	 *
	 * This property value will be passed as the first parameter to either {@link CController::renderPartial}
	 * or {@link CWidget::render} to render each data item.
	 * In the corresponding view template, the following variables can be used in addition to those declared in {@link viewData}:
	 *
	 * - $this: refers to the owner of this list view widget (e.g. the controller if the widget is in the view of a controller).
	 * - $data: refers to the data item currently being rendered.
	 * - $index: refers to the zero-based index of the data item currently being rendered.
	 * - $widget: refers to this list view widget instance.
	 */
	public $itemView;

	/**
	 * @var array additional data to be passed to {@link itemView} when rendering each data item.
	 *   This array will be extracted into local PHP variables that can be accessed in the {@link itemView}.
	 */
	public $viewData = [];

	public $pager = [
		'class' => \BootPager::class,
	];

	public $pagerCssClass = "pagination subpagination";

	public $template = "{summary}\n{letterSelector}\n{pager}\n{items}\n{letterSelector}\n{pager}";

	private $letterPager = [
		'class' => PagerAlpha::class,
		'titleTemplate' => "", // e.g. "{count} objects"
	];

	private $letterPagination = [
		'class' => PaginationAlpha::class,
		'countByLetter' => [],
	];

	private ?PagerAlpha $pagerAlpha = null;

	private ?PaginationAlpha $paginationAlpha = null;

	public function getLetterPagination(): PaginationAlpha
	{
		if ($this->paginationAlpha === null) {
			if (empty($this->letterPagination['countByLetter'])) {
				throw new \Exception("letterPagination.countByLetter is empty");
			}
			$this->paginationAlpha = \Yii::createComponent($this->letterPagination);
		}
		return $this->paginationAlpha;
	}

	/**
	 * @inheritdoc
	 */
	public function renderItems()
	{
		echo '<div class="list-results">';
		$data = $this->dataProvider->getData();
		if (count($data) > 0) {
			$owner = $this->getOwner();
			$viewFile = $owner->getViewFile($this->itemView);
			foreach ($data as $i => $item) {
				$data = $this->viewData;
				$data['index'] = $i;
				$data['data'] = $item;
				$data['widget'] = $this;
				$owner->renderFile($viewFile, $data);
			}
		} else {
			$this->renderEmptyText();
		}
		echo "</div>\n";
	}

	/**
	 * Renders a row with a link to each letter, up to 27.
	 */
	public function renderLetterSelector()
	{
		$pager = $this->getLetterPager();
		echo '<div class="pagination pagination-alpha">';
		$pager->run();
		echo '</div>';
	}

	public function setCountByLetter(array $count): void
	{
		$this->letterPagination['countByLetter'] = $count;
	}

	public function setLetterTitleTemplate(string $template): void
	{
		if (strpos($template, '{count}') === false) {
			throw new \Exception("setLetterTitleTemplate() expects parameter to contain '{count}'");
		}
		$this->letterPager['titleTemplate'] = $template;
	}

	private function getLetterPager(): PagerAlpha
	{
		if ($this->pagerAlpha === null) {
			$this->pagerAlpha = \Yii::createComponent($this->letterPager);
			$this->pagerAlpha->setPages($this->getLetterPagination());
			$this->pagerAlpha->init();
		}
		return $this->pagerAlpha;
	}
}
