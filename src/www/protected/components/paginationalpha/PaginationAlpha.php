<?php

namespace components\paginationalpha;

/**
 * PaginationAlpha is used by ListViewAlpha to display a list a links to each letter.
 *
 * @property PaginationAlpha $pages
 */
class PaginationAlpha extends \CPagination
{
	private const ORD_TO_A = 64;

	private const DEFAULT_RANK = 1; // A

	/**
	 * @var array Must be set before reading or setting the page/letter.
	 */
	public array $countByLetter = [];

	/**
	 * @var string Default is different from the parent's "page".
	 */
	public $pageVar = 'lettre';

	/**
	 * @var ?array Default is empty (only $this->pageVar) instead of parent's null (keep all the GET params).
	 */
	public $params = [];

	/**
	 * @inheritdoc
	 */
	public function createPageUrl($controller, $page)
	{
		$params = $this->params ?? $_GET;
		if (empty($this->countByLetter[$page])) {
			// No URL when there is no matching content.
			return '';
		}
		$letter = $this->getPageLetter($page);
		if ($letter === '') {
			// Current page is the default, no need for a param in the URL
			unset($params[$this->pageVar]);
		} else {
			$params[$this->pageVar] = $letter;
		}
		return $controller->createUrl($this->route, $params);
	}

	/**
	 * @inheritdoc
	 */
	public function getCurrentPage($recalculate = true)
	{
		if (isset($_GET[$this->pageVar])) {
			$backup = $_GET[$this->pageVar];
			$_GET[$this->pageVar] = self::letterToRank($_GET[$this->pageVar]) + 1;
		} else {
			$_GET[$this->pageVar] = self::DEFAULT_RANK + 1;
		}
		$page = parent::getCurrentPage($recalculate);
		if (isset($backup)) {
			$_GET[$this->pageVar] = $backup;
		} else {
			unset($_GET[$this->pageVar]);
		}
		if (empty($this->countByLetter[$page])) {
			return self::DEFAULT_RANK;
		}
		return $page;
	}

	public function getPageCount()
	{
		return 27;
	}

	public function getPageLetter(int $page): string
	{
		if ($page === 0) {
			return "0";
		}
		return chr(self::ORD_TO_A + $page);
	}

	/**
	 * @param int|string $value the zero-based index of the current page, or the letter.
	 */
	public function setCurrentPage($value)
	{
		if (is_string($value)) {
			$page = self::letterToRank($value);
		} else {
			$page = (int) $value;
		}
		if ($page < 0 || $page > 26 || empty($this->countByLetter[$page])) {
			$page = self::DEFAULT_RANK;
		}

		parent::setCurrentPage($page);
		$_GET[$this->pageVar] = $this->getPageLetter($page); // not `$value+1` as the parent does
	}

	private static function letterToRank(string $letter): int
	{
		if ($letter === '') {
			return self::DEFAULT_RANK;
		}
		if ($letter[0] === '0') {
			return 0;
		}
		$rank = ord(strtoupper($letter[0])) - self::ORD_TO_A;
		if ($rank < 0 || $rank > 26) {
			return self::DEFAULT_RANK;
		}
		return $rank;
	}
}
