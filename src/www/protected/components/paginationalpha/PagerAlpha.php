<?php

namespace components\paginationalpha;

use CHtml;

/**
 * PagerAlpha is used by ListViewAlpha to display a list a links to each letter.
 *
 * @property PaginationAlpha $pages
 */
class PagerAlpha extends \CBasePager
{
	private const ORD_TO_A = 64;

	public array $htmlOptions = [];

	/**
	 * @var string E.g. "{count} revues" or "Afficher les {count} éditeurs".
	 */
	public string $titleTemplate = "";

	/**
	 * Executes the widget by displaying the HTML of the paginated navigation.
	 */
	public function run()
	{
		$buttons = $this->createPageButtons();
		if (empty($buttons)) {
			return;
		}
		echo CHtml::tag('ul', $this->htmlOptions, implode("\n", $buttons));
	}

	/**
	 * Creates the page buttons.
	 *
	 * @return string[] a list of HTML page buttons.
	 */
	protected function createPageButtons(): array
	{
		$currentPage = $this->getCurrentPage();
		$countByLetter = $this->pages->countByLetter;
		$buttons = [];
		for ($i = 0; $i <= 26; $i++) {
			$url = $this->createPageUrl($i);
			if ($url === '') {
				continue;
			}
			$htmlOptions = ($i === $currentPage ? ['class' => 'active'] : []);
			if ($this->titleTemplate) {
				$htmlOptions['title'] = str_replace("{count}", $countByLetter[$i], $this->titleTemplate);
			}
			$label = ($i > 0 ? chr(self::ORD_TO_A + $i) : "#0-9");
			$buttons[] = CHtml::tag('li', $htmlOptions, CHtml::link($label, $url));
		}
		return array_filter($buttons);
	}

	protected function createPages(): PaginationAlpha
	{
		return new PaginationAlpha();
	}
}
