<?php

namespace components;

use Yii;

/**
 * This class is an alternative to the default CAssetManager of Yii 1.1.
 *
 * Features:
 * - Create a .gz file along the normal css/js file. See $gzipExtensions.
 */
class AssetManager extends \CApplicationComponent
{
	/**
	 * Default web accessible base path for storing private files
	 */
	public const DEFAULT_BASEPATH = 'assets';

	/**
	 * @var bool If set, (1) the published path will contain the source name,
	 * and (2) the published files are always updated, even if already present.
	 */
	public bool $debug = false;

	/**
	 * @var string[] List of directories and files which should be excluded from the publishing process.
	 */
	public array $excludeFiles = ['.git', '.gitignore', 'node_modules'];

	/**
	 * @var string[] Only files with these extensions will be gzipped statically.
	 */
	public array $gzipExtensions = ['css', 'js'];

	/**
	 * @var int If smaller than this size in bytes, the file will not be gzipped.
	 */
	public int $gzipMinSize = 5_000;

	/**
	 * @var int The permission passed to chmod() for newly generated asset files.
	 */
	public int $newFileMode = 0666;

	/**
	 * @var int The permission passed to chmod() for newly generated asset directories.
	 */
	public int $newDirMode = 0777;

	/**
	 * @var string Base web accessible path for storing private files
	 */
	private string $basePath = '';

	/**
	 * @var string Base URL for accessing the publishing directory.
	 */
	private string $baseUrl = '';

	/**
	 * @var array published assets
	 */
	private array $published = [];

	/**
	 * @return string the root directory storing the published asset files. Defaults to 'WebRoot/assets'.
	 */
	public function getBasePath(): string
	{
		if ($this->basePath === '') {
			$request = Yii::app()->getRequest();
			$this->setBasePath(dirname($request->getScriptFile()) . DIRECTORY_SEPARATOR . self::DEFAULT_BASEPATH);
		}
		return $this->basePath;
	}

	/**
	 * Sets the root directory storing published asset files.
	 * @param string $value the root directory storing published asset files
	 * @throws \CException if the base path is invalid
	 */
	public function setBasePath(string $value): void
	{
		$basePath = realpath($value);
		if ($basePath !== false && is_dir($basePath) && is_writable($basePath)) {
			$this->basePath = $basePath;
		} else {
			throw new \CException(Yii::t('yii', 'CAssetManager.basePath "{path}" is invalid. Please make sure the directory exists and is writable by the Web server process.', ['{path}' => $value]));
		}
	}

	/**
	 * @return string the base url that the published asset files can be accessed.
	 * Note, the ending slashes are stripped off. Defaults to '/assets'.
	 */
	public function getBaseUrl(): string
	{
		if ($this->baseUrl === '') {
			$this->setBaseUrl('/' . self::DEFAULT_BASEPATH);
		}
		return $this->baseUrl;
	}

	/**
	 * @param string $value the base url that the published asset files can be accessed
	 */
	public function setBaseUrl(string $value): void
	{
		$this->baseUrl = rtrim($value, '/');
	}

	/**
	 * Publishes a file or a directory.
	 * This method will copy the specified asset to a web accessible directory
	 * and return the URL for accessing the published asset.
	 * <ul>
	 * <li>If the asset is a file, its file modification time will be checked
	 * to avoid unnecessary file copying;</li>
	 * <li>If the asset is a directory, all files and subdirectories under it will
	 * be published recursively.</li>
	 * </ul>
	 *
	 * Note: On rare scenario, a race condition can develop that will lead to a
	 * one-time-manifestation of a non-critical problem in the creation of the directory
	 * that holds the published assets. This problem can be avoided altogether by 'requesting'
	 * in advance all the resources that are supposed to trigger a 'publish()' call, and doing
	 * that in the application deployment phase, before system goes live. See more in the following
	 * discussion: https://code.google.com/p/yii/issues/detail?id=2579
	 *
	 * @param string $path The asset path (file or directory) to be published.
	 * @throws \CException if the asset to be published does not exist.
	 * @return string An absolute URL to the published asset.
	 */
	public function publish(string $path): string
	{
		if (!isset($this->published[$path])) {
			$updated = $this->publishPath($path);
			if ($updated && $this->gzipExtensions) {
				$dir = $this->generatePath(realpath($path));
				$dstDir = $this->getBasePath() . \DIRECTORY_SEPARATOR . $dir;
				$this->gzipDirectory($dstDir);
			}
		}
		return $this->published[$path] ?? '';
	}

	/**
	 * Returns the published path of a file path.
	 * This method does not perform any publishing. It merely tells you
	 * if the file or directory is published, where it will go.
	 * @param string $path directory or file path being published
	 * @return string the published file path. '' if the file or directory does not exist
	 */
	public function getPublishedPath(string $path): string
	{
		$realpath = realpath($path);
		if ($realpath ===false) {
			return '';
		}
		$base = $this->getBasePath() . DIRECTORY_SEPARATOR . $this->generatePath($path);
		return is_file($path) ? $base . DIRECTORY_SEPARATOR . basename($path) : $base;
	}

	/**
	 * Returns the URL of a published file path.
	 * This method does not perform any publishing. It merely tells you
	 * if the file path is published, what the URL will be to access it.
	 * @param string $path directory or file path being published
	 * @return string the published URL for the file or directory. '' if the file or directory does not exist.
	 */
	public function getPublishedUrl(string $path): string
	{
		if (isset($this->published[$path])) {
			return $this->published[$path];
		}
		$realpath = realpath($path);
		if ($realpath === false) {
			return '';
		}
		$base = $this->getBaseUrl() . '/' . $this->generatePath($realpath);
		return is_file($realpath) ? $base . '/' . basename($realpath) : $base;
	}

	/**
	 * Generates path segments relative to basePath.
	 */
	private function generatePath(string $file): string
	{
		$pathForHashing = is_file($file) ? dirname($file) : $file;
		$hash = hash("crc32c", $pathForHashing . filemtime($file) . \Yii::getVersion());

		$suffix = "";
		if ($this->debug) {
			$suffix = "_" . preg_replace('/[^\w]/', '', basename($file));
		}

		return $hash . $suffix;
	}

	private function gzipDirectory(string $dir): void
	{
		if (!is_dir($dir)) {
			throw new \Exception("Pas un répertoire");
		}
		$iterator = new \RecursiveDirectoryIterator($dir, \FilesystemIterator::CURRENT_AS_FILEINFO | \FilesystemIterator::KEY_AS_PATHNAME);
		foreach (new \RecursiveIteratorIterator($iterator) as $path => $file) {
			assert($file instanceof \SplFileInfo);
			if (in_array($file->getExtension(), $this->gzipExtensions, true) && $file->getSize() >= $this->gzipMinSize) {
				if (self::gzipFile($path, $file->getMTime())) {
					// Stop the whole gzip process if a gzipped file is already up to date.
					break;
				}
			}
		}
	}

	private static function gzipFile(string $path, int $mtime): bool
	{
		$dest = "$path.gz";
		if (file_exists($dest) && @filemtime($dest) >= $mtime) {
			return true;
		}
		$contents = file_get_contents($path);
		if ($contents === false) {
			return false;
		}
		$encoded = gzencode($contents);
		if ($encoded === false) {
			return false;
		}
		if (file_put_contents($dest, $encoded) === false) {
			return false;
		}
		touch($dest, $mtime);
		return false;
	}

	/**
	 * @param string $path Path to publish
	 * @return bool True if the published files were updated.
	 */
	private function publishPath(string $path): bool
	{
		$src = realpath($path);
		if ($src === false) {
			throw new \CException(Yii::t('yii', 'The asset "{asset}" to be published does not exist.', ['{asset}' => $path]));
		}
		$dir = $this->generatePath($src);
		$dstDir = $this->getBasePath() . DIRECTORY_SEPARATOR . $dir;
		$updated = false;
		if (is_file($src)) {
			$fileName = basename($src);
			$dstFile = $dstDir . DIRECTORY_SEPARATOR . $fileName;
			if (!is_dir($dstDir)) {
				mkdir($dstDir, $this->newDirMode, true);
				@chmod($dstDir, $this->newDirMode);
			}

			if (@filemtime($dstFile) < @filemtime($src)) {
				copy($src, $dstFile);
				@chmod($dstFile, $this->newFileMode);
				$updated = true;
			}
			$this->published[$path] = $this->getBaseUrl() . "/$dir/$fileName";
		} elseif (is_dir($src)) {
			if (!is_dir($dstDir) || $this->debug) {
				\CFileHelper::copyDirectory($src, $dstDir, [
					'exclude' => $this->excludeFiles,
					'level' => -1,
					'newDirMode' => $this->newDirMode,
					'newFileMode' => $this->newFileMode,
				]);
				$updated = true;
			}
			$this->published[$path] = $this->getBaseUrl() . '/' . $dir;
		}
		return $updated;
	}
}
