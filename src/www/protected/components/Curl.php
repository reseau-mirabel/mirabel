<?php

namespace components;

use components\curl\NetworkException;
use components\curl\NetworkTimeout;
use Exception;

/**
 * Basic Usage:
 * $c = new Curl();
 * $rawResponse = $c->get($url)->getContent();
 *
 * Advanced Usage:
 * $c = new Curl();
 * try {
 *     $c->get($url);
 * } catch (\Throwable $e) {
 *     // an error occured
 * }
 * if ($c->getHttpCode() !== 200) {
 *     // ...
 * }
 * $rawResponse = $c->getContent();
 *
 * @todo Split into CurlRequest and CurlResponse?
 */
class Curl
{
	private string $content = '';

	private $curl;

	/**
	 * @var array List of raw header lines.
	 */
	private array $headerData = [];

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->headerData = [];
		$this->curl = curl_init();
		curl_setopt_array(
			$this->curl,
			[
				\CURLOPT_FOLLOWLOCATION => true,    // follow redirection (301, etc)
				\CURLOPT_MAXREDIRS => 4,
				\CURLOPT_SSL_VERIFYHOST => false,
				\CURLOPT_SSL_VERIFYPEER => false,
				\CURLOPT_COOKIEFILE => "",          // store cookies
				\CURLOPT_USERAGENT => 'Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0',
				\CURLOPT_HEADER => false,
				\CURLOPT_CONNECTTIMEOUT => 5, // seconds
				\CURLOPT_TIMEOUT => 5, // seconds
				\CURLOPT_HEADERFUNCTION => [$this, 'addHeaderLine'],
			]
		);
	}

	/**
	 * Destructor
	 */
	public function __destruct()
	{
		if ($this->curl !== null) {
			curl_close($this->curl);
		}
	}

	/**
	 * Set an CURL option
	 *
	 * @param int $option CURLOPT_foobar
	 * @param mixed $value
	 */
	public function setopt(int $option, $value): self
	{
		if (curl_setopt($this->curl, $option, $value) === false) {
			throw new Exception("curl_setopt() failed. Probably a code error.");
		}
		return $this;
	}

	/**
	 * Fetch an URL. Then use getContent() to read the response's body.
	 *
	 * @param string $url
	 * @throws Exception
	 * @return $this
	 */
	public function get(string $url): self
	{
		$ok = curl_setopt_array(
			$this->curl,
			[
				\CURLOPT_FILE => null,
				\CURLOPT_HTTPGET => true,
				\CURLOPT_RETURNTRANSFER => true,
				\CURLOPT_URL => $url,
			]
		);
		if (!$ok) {
			throw new Exception("Erreur en configurant curl.");
		}
		$this->content = '';
		$content = curl_exec($this->curl);
		if ($content === false || curl_errno($this->curl) !== 0) {
			throw new NetworkException("Erreur de téléchargement : " . curl_error($this->curl), curl_errno($this->curl));
		}
		$this->content = (string) $content;
		return $this;
	}

	/**
	 * Fetch an URL into a file handler.
	 *
	 * @param string $url
	 * @param resource $fh
	 * @throws Exception
	 * @return $this
	 */
	public function getFile(string $url, $fh): self
	{
		if (!is_resource($fh)) {
			throw new Exception("Le second paramètre de Curl::getFile() doit être un file handler.");
		}
		$ok = curl_setopt_array(
			$this->curl,
			[
				\CURLOPT_RETURNTRANSFER => true, // ORDER MATTERS! (required for FILE, though undocumented)
				\CURLOPT_FILE => $fh,
				\CURLOPT_HTTPGET => true,
				\CURLOPT_TIMEOUT => 120,
				\CURLOPT_URL => $url,
			]
		);
		if (!$ok) {
			throw new Exception("Erreur en configurant curl.");
		}
		curl_exec($this->curl);
		if (curl_errno($this->curl) !== 0) {
			if (curl_errno($this->curl) === 28) {
				throw new NetworkTimeout(curl_error($this->curl));
			}
			throw new NetworkException("Erreur de téléchargement : " . curl_error($this->curl));
		}
		return $this;
	}

	/**
	 * Return the timestamp of the remote file, or null if unknown.
	 *
	 * @param string $url
	 * @return int|null
	 */
	public function getFileTime(string $url): ?int
	{
		$ok = curl_setopt_array(
			$this->curl,
			[
				\CURLOPT_RETURNTRANSFER => true, // ORDER MATTERS! (if missing, header will be printed on stdout)
				\CURLOPT_NOBODY => true,
				\CURLOPT_FILETIME => true,
				\CURLOPT_TIMEOUT => 5, // seconds
				\CURLOPT_URL => $url,
			]
		);
		if (!$ok) {
			throw new Exception("Erreur en configurant curl.");
		}
		curl_exec($this->curl);
		curl_setopt_array(
			$this->curl,
			[
				\CURLOPT_NOBODY => false,
				\CURLOPT_FILETIME => false,
			]
		);
		$ts = curl_getinfo($this->curl, \CURLINFO_FILETIME);
		if ($ts < 0) {
			return null;
		}
		return $ts;
	}

	public function getContent(): string
	{
		return $this->content;
	}

	public function getContentType(): string
	{
		return (string) curl_getinfo($this->curl, \CURLINFO_CONTENT_TYPE);
	}

	public function getHttpCode() : int
	{
		return (int) curl_getinfo($this->curl, \CURLINFO_HTTP_CODE);
	}

	public function getRemoteFilename(): string
	{
		foreach ($this->headerData as $headerLine) {
			if (strncasecmp($headerLine, "content-disposition:", 20) !== 0) {
				continue;
			}
			$line = trim(substr($headerLine, 20), "\r ");
			if (preg_match('/\bfilename="(.+?)(?<!\\))"(?: |$)/', $line, $m)) {
				// Cf RFC 6266, 4.2. Disposition Type
				// https://httpwg.org/specs/rfc6266.html#header.field.definition
				return $m[1];
			}
			if (preg_match('/\bfilename=([^"]\S+)(?: |\r|\n|$)/', $line, $m)) {
				// Should not happen if header apply RFC2616 which mandates to wrap values with "".
				// https://www.rfc-editor.org/rfc/rfc2616.html#section-2.2
				return $m[1];
			}
		}

		// No Content-Disposition, so we parse the (post-redirection) URL.
		$url = curl_getinfo($this->curl, \CURLINFO_EFFECTIVE_URL);
		if (preg_match('#/([^/]+)(?:\?|$)#', $url, $m)) {
			return $m[1];
		}

		return "";
	}

	public function getSize(): int
	{
		return (int) curl_getinfo($this->curl, \CURLINFO_SIZE_DOWNLOAD);
	}

	/**
	 * Internally used to parse the response header.
	 *
	 * It has to be a public method (usage as a cURL callback requires it),
	 * but it should not be called.
	 *
	 * @param resource $curl
	 * @param string $line
	 * @return int
	 */
	public function addHeaderLine($curl, $line)
	{
		$this->headerData[] = $line;
		return strlen($line);
	}
}
