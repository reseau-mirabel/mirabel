<?php

namespace components\db;

/**
 * Stack of data that will be inserted into MySQL as a batch.
 * Sends SQL requests like "INSERT INTO mytable (a, b) VALUES (a1,b1), ... (a100,b100)".
 *
 * Example :
 * <pre>
 * $buffer = new InsertBuffer("INSERT INTO mytable (a, b) VALUES ");
 * $buffer->setBatchSize(100);
 *
 * // FACULTATIF La connexion à \Yii::app()->db est coupée jusqu'à l'appel à commit() ou close().
 * // Nécessaire pour éviter une coupure automatique du réseau.
 * $buffer->stopDbConnection();
 *
 * foreach ($data as [$a, $b]) {
 *     $buffer->add([$a, $b]);
 * }
 *
 * // Les données résiduelles sont commitées,
 * // et la connexion à la base par défaut est restaurée.
 * $buffer->close();
 * </pre>
 */
class InsertBuffer
{
	/**
	 * @var list<array>
	 */
	private array $data = [];

	private int $inserted = 0;

	private string $sql = '';

	private int $batchSize = 100;

	private string $rowPattern = '';

	public function __construct(string $sql)
	{
		$this->sql = $sql;
	}

	/**
	 * Stopping the connection is necessary for long operations that could endure timeouts.
	 *
	 * The method commit() will automatically reconnect to the DB if necessary.
	 */
	public function stopDbConnection(): void
	{
		if (defined('YII_TARGET') && YII_TARGET === 'test') {
			// Stopping the connection would mess with transactions.
			return;
		}
		\Yii::app()->db->setActive(false);
		\Yii::app()->db->autoConnect = false;
	}

	public function setBatchSize(int $maxSize): void
	{
		$this->batchSize = $maxSize;
	}

	/**
	 * Optional. Defaults to "(?, ?, ...)" with as many '?' as necessary.
	 *
	 * Required if the insert has to use SQL functions, e.g. "(?, MD5(?))"
	 */
	public function setRowPattern(string $pattern): void
	{
		$this->rowPattern = $pattern;
	}

	public function add(array $row): void
	{
		$this->data[] = $row;
		if (count($this->data) >= $this->batchSize) {
			$this->commit();
		}
	}

	public function countInserted(): int
	{
		return $this->inserted;
	}

	public function close(): void
	{
		$this->commit();
		if (\Yii::app()->db) {
			\Yii::app()->db->autoConnect = true;
		}
	}

	private function commit(): void
	{
		if (!$this->data) {
			return;
		}

		$rowSize = count($this->data[0]);
		if ($this->rowPattern === '') {
			$this->rowPattern = "(" . join(',', array_fill(0, $rowSize, '?')) . ")";
		}
		$sqlInserts = join(", ", array_fill(0, count($this->data), $this->rowPattern));

		$isActive = \Yii::app()->db->active;
		\Yii::app()->db->setActive(true);
		$this->inserted += (int) \Yii::app()->db
			->createCommand("{$this->sql} $sqlInserts")
			->execute(
				array_merge(...$this->data) // flatten the data
			);
		$this->data = [];
		\Yii::app()->db->setActive($isActive);
	}
}
