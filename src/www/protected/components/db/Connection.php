<?php

namespace components\db;

/**
 * Extends CDbConnection so that nested transactions are allowed,
 * and applying commit and rollback to each of them is possible.
 *
 * Required for Codeception tests with Yii1 transactions.
 */
class Connection extends \CDbConnection
{
	/**
	 * @var \CDbTransaction[] Newest (deepest) first.
	 */
	protected $transactions = [];

	/**
	 * @inheritdoc
	 */
	public function beginTransaction()
	{
		if (!$this->getActive()) {
			$this->setActive(true);
			$this->resetTransactions();
		}
		if (count($this->transactions) === 0) {
			$transaction = $this->beginTransactionMain();
		} else {
			$transaction = $this->beginTransactionSub();
		}
		array_push($this->transactions, $transaction);
		return $transaction;
	}

	/**
	 * @inheritdoc
	 */
	public function getCurrentTransaction()
	{
		if ($this->getPdoInstance() === null) {
			$this->transactions = [];
			return null;
		}
		// If the main transaction is inactive, drop the savepoints (sub transactions).
		if (!$this->transactions || !$this->transactions[0]->getActive()) {
			$this->resetTransactions();
			return null;
		}
		// Remove inactive transactions until an active one is found.
		while ($this->transactions) {
			$lastIndex = count($this->transactions) - 1;
			if ($this->transactions[$lastIndex]->getActive()) {
				return $this->transactions[$lastIndex];
			}
			array_pop($this->transactions);
		}
		return null;
	}

	private function beginTransactionMain(): TransactionMain
	{
		$this->getPdoInstance()->beginTransaction();
		return new TransactionMain($this);
	}

	private function beginTransactionSub(): TransactionSub
	{
		$level = count($this->transactions);
		$pdo = $this->getPdoInstance();
		if ($pdo->exec("SAVEPOINT level$level") === false) {
			throw new \CDbException("SAVEPOINT failed: " . print_r($pdo->errorInfo(), true));
		}
		return new TransactionSub($this, "level$level");
	}

	private function resetTransactions(): void
	{
		foreach ($this->transactions as $t) {
			if ($t instanceof TransactionSub) {
				$t->drop();
			}
		}
		$this->transactions = [];
	}
}
