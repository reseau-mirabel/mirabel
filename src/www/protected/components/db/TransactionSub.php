<?php

namespace components\db;

use CDbException;
use Yii;

class TransactionSub extends \CDbTransaction
{
	/**
	 * @var string
	 */
	private $transactionName;

	public function __construct(\CDbConnection $connection, string $name)
	{
		parent::__construct($connection);
		$this->transactionName = $name;
	}

	/**
	 * @inheritdoc
	 */
	public function commit()
	{
		// Do not commit, just release the savepoint placed inside the higher level transaction.
		try {
			if ($this->getActive() && $this->getConnection()->getActive()) {
				$this->getConnection()->getPdoInstance()->exec("RELEASE SAVEPOINT {$this->transactionName}");
			}
		} catch (\Exception $e) {
			Yii::log("Failed to commit SQL savepoint: " . $e->getMessage(), 'db', 'warning');
		} finally {
			$this->setActive(false);
			$this->getConnection()->getCurrentTransaction();
		}
	}

	public function drop()
	{
		$this->setActive(false);
	}

	/**
	 * @inheritdoc
	 */
	public function rollback()
	{
		if ($this->getActive() && $this->getConnection()->getActive()) {
			Yii::trace('Rolling back sub-transaction', 'system.db.CDbTransaction');
			$this->setActive(false);
			$this->getConnection()->getPdoInstance()->exec("ROLLBACK TO SAVEPOINT {$this->transactionName}");
		} else {
			throw new CDbException(Yii::t('yii', 'CDbTransaction is inactive and cannot perform commit or roll back operations.'));
		}
		$this->getConnection()->getCurrentTransaction();
	}
}
