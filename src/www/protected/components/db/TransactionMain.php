<?php

namespace components\db;

class TransactionMain extends \CDbTransaction
{
	/**
	 * @inheritdoc
	 */
	public function commit()
	{
		if ($this->getActive() && $this->getConnection()->getActive() && !$this->getConnection()->getPdoInstance()->inTransaction()) {
			\Yii::log("The transaction was already implicitly commited.", 'db', 'warning');
		}
		parent::commit();
		$this->getConnection()->getCurrentTransaction();
	}

	/**
	 * @inheritdoc
	 */
	public function rollback()
	{
		parent::rollback();
		$this->getConnection()->getCurrentTransaction();
	}
}
