<?php

/**
 * Controller is the customized base controller class.
 *
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string[] List of: type => Mime http header
	 */
	protected const CONTENTTYPE_HEADERS = [
		'json' => 'Content-Type: application/json; charset="UTF-8"',
		'text' => 'Content-Type: text/plain; charset="UTF-8"',
		'xml' => 'Content-Type: text/xml; charset="UTF-8"',
		'csv' => 'Content-Type: text/csv; charset="UTF-8"; header=present',
		'tsv' => 'Content-Type: text/tab-separated-values; charset="UTF-8"; header=present',
		'pdf' => 'Content-Type: application/pdf',
	];

	public $containerClass = 'container';

	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout = '//layouts/column1';

	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs = [];

	/**
	 * @var string
	 */
	public $pageDescription = "";

	/**
	 * @var ?Partenaire
	 */
	public $partenaire;

	/**
	 * @var bool If true, display leaves on the background LEFT.
	 */
	public $backgroundL = true;

	/**
	 * @var bool If true, display leaves on the background RIGHT.
	 */
	public $backgroundR = false;

	/**
	 * @var string
	 */
	protected $canonicalUrl = "";

	private $htmlHead = '';

	public function init()
	{
		$accessToken = \Yii::app()->request->getQuery('access-token', '');
		if ($accessToken) {
			$expiration = \components\AccessToken::getExpiration($accessToken);
			if ($expiration) {
				\Yii::app()->request->cookies->add(
					'access-token',
					new CHttpCookie('access-token', $accessToken, ['expire' => $expiration])
				);
			}
		}

		$user = Yii::app()->user;
		assert($user instanceof \WebUser);
		if ($user->checkAccess("avec-partenaire") && isset($user->partenaireId)) {
			$this->partenaire = Partenaire::model()->findByPk($user->partenaireId);
		}

		if ($this->getModule() !== null) {
			parent::init();
			return;
		}

		if (strpos(\Yii::app()->request->url, 'ajax') === false) {
			self::addSecurityHeaders();
		}

		// use special base name in some CActiveForm instances, e.g. q[id] instead of InterventionSearch[id]
		CHtml::setModelNameConverter(function ($m) {
			$special = [
				'models\cms\BreveSearchForm' => 'q',
				'models\forms\ContactForm' => 'ContactForm',
				'models\forms\ContactServiceForm' => 'ContactServiceForm',
				'models\forms\IndexationImportForm' => 'IndexationImportForm',
				'models\forms\Login' => 'LoginForm',
				'models\forms\StatsForm' => 'filter',
				'models\forms\PasswordMailForm' => 'form',
				'models\searches\EditeurSearch' => 'q',
				'models\searches\InterventionSearch' => 'q',
				'models\searches\PolitiqueSearch' => 'q',
				'models\searches\SourcelienSearch' => 'q',
				'models\searches\UtilisateurPolitique' => 'q',
				'models\forms\User' => 'User',
				'models\forms\UserPolitique' => 'UserPolitique',
				'models\forms\PolitiqueTitresForm' => 'pt',
				'processes\analyse\CompareForm' => 'q',
				'processes\attribut\ImportSourceForm' => 'q',
				'processes\attribut\ImportParsingForm' => 'q',
				'processes\grappe\GrappeForm' => 'Grappe',
			];
			$name = is_object($m) ? get_class($m) : (string) $m;
			$pos = strrpos($name, '\\');
			return $special[$name] ?? ($pos !== false ? substr($name, $pos + 1) : $name);
		});

		if ($user->checkAccess("avec-partenaire")) {
			if (\Config::read('maintenance.message.authentifie')) {
				$user->setFlash('warning', \Config::read('maintenance.message.texte'));
			}
			if (\Config::read('bandeau.authentifie') && !isset($_COOKIE['bandeau-vu'])) {
				$user->setFlash('info', '<div class="session-bandeau">' . \Config::read('bandeau.texte') . '</div>');
			}
		} else {
			if (\Config::read('maintenance.message.invite')) {
				$user->setFlash('warning', \Config::read('maintenance.message.texte'));
			}
			if (\Config::read('bandeau.invite') && !isset($_COOKIE['bandeau-vu'])) {
				$user->setFlash('info', '<div class="session-bandeau">' . \Config::read('bandeau.texte') . '</div>');
			}
		}

		$this->initDefaultMetadata();
	}

	public function createAction($actionID)
	{
		if ($actionID === '') {
			$actionID = $this->defaultAction;
		}
		if (method_exists($this, "action$actionID") && strcasecmp($actionID, 's')) {
			// we have an action method
			return new CInlineAction($this, $actionID);
		}
		$action = $this->createActionFromMap($this->actions(), $actionID, $actionID);
		if ($action === null && preg_match('#^[a-zA-Z]+-[a-zA-Z-]+#', $actionID)) {
			$newActionID = preg_replace_callback(
				'#^([a-zA-Z]+)-([a-zA-Z-]+)#',
				function ($x) {
					return $x[1] . join('', array_map('ucfirst', explode('-', $x[2])));
				},
				$actionID
			);
			if ($newActionID !== $actionID) {
				return $this->createAction($newActionID);
			}
		}
		if ($action !== null && !method_exists($action, 'run')) {
			throw new CException(Yii::t('yii', 'Action class {class} must implement the "run" method.', ['{class}'=>get_class($action)]));
		}
		return $action;
	}

	/**
	 * Sends to the browser the HTTP header for this type of data.
	 *
	 * @param string $type Among: text, json, xml, nocache. Or the full header.
	 */
	public static function header(string $type): void
	{
		if (headers_sent()) {
			throw new CHttpException(500, "HTTP headers were already sent.");
		}
		$typelow = strtolower($type);
		if ($typelow === 'nocache') {
			header("Expires: Sun, 25 Jul 1997 12:00:00 GMT");
			header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
			header("Cache-Control: no-store, no-cache, must-revalidate");
			header("Cache-Control: post-check=0, pre-check=0", false);
		} elseif (isset(self::CONTENTTYPE_HEADERS[$typelow])) {
			header(self::CONTENTTYPE_HEADERS[$typelow]);
		} else {
			header($type);
		}
	}

	/**
	 * Loads a JS file named like the view, in the sub-directory js/ of the view path.
	 *
	 * @param string $viewFile Path to the view or to a JS file.
	 * @param int $position (opt) Defaults to CClientScript::POS_READY.
	 *   See http://www.yiiframework.com/doc/api/1.1/CClientScript/#registerScript-detail
	 */
	public function loadViewJs($viewFile, $position=CClientScript::POS_READY)
	{
		$pathinfo = pathinfo($viewFile);
		if (empty($pathinfo['extension'])) {
			Yii::log("loadViewJs expects a file with an extension", CLogger::LEVEL_ERROR);
			return;
		}
		switch ($pathinfo['extension']) {
			case 'js':
				$jsFile = $viewFile;
				break;
			case 'php':
				$jsFile = $pathinfo['dirname'] . '/js/' . $pathinfo['filename'] . '.js';
				break;
			default:
				Yii::log("loadViewJs expects a js file name, or `__FILE__`", CLogger::LEVEL_ERROR);
				return;
		}

		if (!file_exists($jsFile) || !is_readable($jsFile)) {
			Yii::log("loadViewJs could not read {$jsFile}", CLogger::LEVEL_ERROR);
			return;
		}
		Yii::app()->getClientScript()->registerScript(
			$pathinfo['filename'] . '-js',
			file_get_contents($jsFile),
			$position
		);
	}

	public static function linkEmbeddedFeed(string $type, ?string $title = null, array $parameters = [])
	{
		if (!$title) {
			$title = Yii::app()->name;
		} else {
			$title = Yii::app()->name . " - " . $title;
		}
		$title .= " ($type)";
		$types = [
			'rss1' => 'application/rss+xml',
			'rss2' => 'application/rss+xml',
			'atom' => 'application/atom+xml',
		];
		if (!isset($types[$type])) {
			throw new CHttpException(500, "Unknown feed type (rss1, rss2 atom)");
		}
		if ($type != 'rss2') {
			$parameters['type'] = $type;
		}
		$url = Yii::app()->baseUrl . '/feeds.php'
			. ($parameters ? '?' . http_build_query($parameters) : '');
		return '<link rel="alternate" type="' . $types[$type]
			. '" href="' . CHtml::encode($url)
			. '" title="' . CHtml::encode($title)
			. '" />';
	}

	/**
	 * @return string HTML
	 */
	public function getSearchForm(string $class = "navbar-search"): string
	{
		return <<<EOHTML
			<form class="$class form-search" action="{$this->createUrl('/site/search')}">
				<div class="input-append">
					<input type="search" name="global" class="search-query" placeholder="Recherche" aria-label="recherche" />
					<button type="submit" class="btn"><i class="icon-search"></i></button>
				</div>
			</form>
			EOHTML;
	}

	/**
	 * @param string $text Must be already encoded with CHtml::encode() and such
	 * @param string $unless (opt) PCRE pattern, will not append text if the pattern matches
	 */
	public function appendToHtmlHead(string $text, string $unless = ''): void
	{
		if (!$unless || !preg_match($unless, $this->htmlHead)) {
			$this->htmlHead .= $text;
		}
	}

	/**
	 * Renvoie le HTML des entêtes meta pour insertion dans <HEAD>.
	 *
	 * Si la vue de la page remplit les propriétés "pageTitle", "pageDescription", ou "canonicalUrl",
	 * ces valeurs sont utilisées en complément/remplacement des valeurs par défaut.
	 */
	public function getHtmlHead(): string
	{
		$m = [];
		if ($this->pageDescription) {
			$description = $this->pageDescription;
		} elseif (preg_match('/<meta name="description" content="(.+?)" lang/', $this->htmlHead, $m)) {
			$description = html_entity_decode($m[1]);
		} else {
			$description = "Description des accès en ligne aux contenus des revues";
		}
		$title = $this->pageTitle; // déjà préfixé par le nom de l'instance par layout/main.php

		// Insert a HTML block of meta, if it was not already set in the page view.
		$encTitle = CHtml::encode($title);
		$encDescription = CHtml::encode($description);
		$this->appendToHtmlHead(
			<<<EOL
				<meta name="author" content="Mirabel" />
				<meta name="keywords" content="revue, accès en ligne, texte intégral, sommaire, périodique" lang="fr" />
				<meta name="description" content="{$encDescription}" lang="fr" />
				<link rel="schema.dcterms" href="http://purl.org/dc/terms/" />
				<meta name="dcterms.title" content="{$encTitle}" />
				<meta name="dcterms.subject" content="revue, accès en ligne, texte intégral, sommaire, périodique" />
				<meta name="dcterms.language" content="fr" />
				<meta name="dcterms.creator" content="Mirabel" />
				<meta name="dcterms.publisher" content="Sciences Po Lyon" />
				EOL,
			'/<meta\s+name="description"/'
		);

		// Overwrites previous calls to registerMetaTag() for the same last parameter.
		Yii::app()->clientScript->registerMetaTag($title, 'og:title', null, ['property' => 'og:title'], 'og:title');
		Yii::app()->clientScript->registerMetaTag($description, 'og:description', null, ['property' => 'og:description'], 'og:description');
		if ($this->canonicalUrl) {
			Yii::app()->clientScript->registerMetaTag($this->canonicalUrl, 'og:url', null, ['property' => 'og:url'], 'og:url');
		}

		return (string) $this->htmlHead;
	}

	public function validateIntervention(Intervention $i, bool $directAccess): bool
	{
		if (!$i->validate(null, false)) {
			return false;
		}
		if ($directAccess) {
			$i->contenuJson->confirm = true;
			if ($i->accept(false)) {
				Yii::app()->user->setFlash('success', "Modification enregistrée.");
				if (!$i->editeurId && isset($i->contenuJson->lastInsertId['Editeur'])) {
					$i->editeurId = (int) $i->contenuJson->lastInsertId['Editeur'];
				}
				if (!$i->ressourceId && isset($i->contenuJson->lastInsertId['Ressource'])) {
					$i->ressourceId = (int) $i->contenuJson->lastInsertId['Ressource'];
				}
				if (!$i->titreId && isset($i->contenuJson->lastInsertId['Titre'])) {
					$i->titreId = (int) $i->contenuJson->lastInsertId['Titre'];
					if (!$i->revueId && isset($i->contenuJson->lastInsertId['Revue'])) {
						$i->revueId = (int) $i->contenuJson->lastInsertId['Revue'];
					} elseif (!$i->revueId && !empty($i->contenuJson[0]['id'])) {
						$i->revueId = (int) $i->contenuJson[0]['id'];
					}
				}
				return $i->save(false);
			}
		} else {
			if ($i->save()) {
				Yii::app()->user->setFlash(
					'success',
					"Proposition enregistrée. Merci d'avoir contribué à Mir@bel."
				);
				return true;
			}
		}
		return false;
	}

	/**
	 * If the completion was not done asynchronously, try to do it on the server side.
	 *
	 * @param string $textField
	 * @param string $className
	 * @param string $method 'REQUEST' (default), 'GET' or 'POST'
	 * @return int|null ID found, or null
	 */
	public function autoCompleteOffline($textField, $className, $method = 'REQUEST'): ?int
	{
		$method = '_' . strtoupper($method);
		if (empty($GLOBALS[$method][$textField])) {
			return null;
		}
		$text = $GLOBALS[$method][$textField];
		$completer = \processes\completion\Completer::type($className);
		$completed = $completer->findExactTerm($text);
		if (count($completed) === 0) {
			$completed = $completer->completeTerm($text);
		}
		if (count($completed) !== 1) {
			return null;
		}
		return (int) $completed[0];
	}

	/**
	 * @param array $r ['status' => X, 'message' => Y, 'url' => Z]
	 */
	public function flashAndRedirect(array $r): bool
	{
		if (empty($r)) {
			return false;
		}
		if (!empty($r['message'])) {
			Yii::app()->user->setFlash($r['status'], $r['message']);
		}
		$url = $_POST['returnUrl'] ?? $r['url'] ?? '';
		if ($url) {
			$this->redirect($url);
		} else {
			$this->refresh();
		}
		return true;
	}

	public function loadSearchNavigation(): ?SearchNavigation
	{
		if (isset($_GET['s'])) {
			$searchNavigation = new SearchNavigation();
			$searchNavigation->loadByHash($_GET['s']);
			return ($searchNavigation->isEmpty() ? null : $searchNavigation);
		}
		return null;
	}

	public function setCanonicalUrl(array $url): void
	{
		$route = array_shift($url);
		$this->canonicalUrl = $this->createAbsoluteUrl($route, $url);
	}

	public function getCanonicalUrl(): string
	{
		return $this->canonicalUrl;
	}

	protected function beforeAction($action)
	{
		if (isset($_GET['ppp'])) {
			Yii::app()->user->setInstituteById((int) $_GET['ppp']);
			$params = $_GET;
			unset($params['ppp']);
			if ($this->route === 'site/index') {
				$url = Yii::app()->baseUrl . "/" . ($params ? "?" . http_build_query($params) : '');
			} else {
				$url = Yii::app()->createAbsoluteUrl("/" . $this->route, $params);
			}
			$this->redirect($url);
			return false;
		}
		return parent::beforeAction($action);
	}

	protected function addSecurityHeaders(): void
	{
		// https://developer.mozilla.org/en-US/docs/Web/Security/Practical_implementation_guides/MIME_types
		header("X-Content-Type-Options: nosniff");
		// https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP
		$baseUrl = \Yii::app()->getBaseUrl(true);
		header("Reporting-Endpoints: csp-endpoint=\"$baseUrl/site/csp-report\"");
		$directive = Yii::app()->params->itemAt('security-csp-directive') ?? "Content-Security-Policy";
		header(join("; ", [
			"$directive: default-src 'self' 'unsafe-inline'",
			"connect-src *", // fetch, XMLHttpRequest, etc
			"font-src *",
			"img-src * data:",
			"media-src *",
			"script-src 'self' 'unsafe-inline' cdn.jsdelivr.net maps.stamen.com stats.sciencespo-lyon.fr",
			"worker-src 'self' blob", // requis pour jstree
			"frame-ancestors 'self'",
			//"upgrade-insecure-requests",
			"report-to csp-endpoint",
		]));
	}

	private function initDefaultMetadata(): void
	{
		$logoCarre = Yii::app()->getBaseUrl(true) . '/images/logo-mirabel-carre.png';
		Yii::app()->clientScript->registerMetaTag($logoCarre, 'og:image', null, ['property' => 'og:image'], 'og:image');
		Yii::app()->clientScript->registerMetaTag("@mirabel_revues", 'og:site_name', null, ['property' => 'og:site_name'], 'og:site_name');
		Yii::app()->clientScript->registerMetaTag("website", 'og:type', null, ['property' => 'og:type'], 'og:type');
	}
}
