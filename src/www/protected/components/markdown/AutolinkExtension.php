<?php

declare(strict_types=1);

namespace components\markdown;

use League\CommonMark\Environment\EnvironmentBuilderInterface;
use League\CommonMark\Extension\Autolink\UrlAutolinkParser;
use League\CommonMark\Extension\ConfigurableExtensionInterface;
use League\Config\ConfigurationBuilderInterface;
use Nette\Schema\Expect;

/**
 * Replace commonmark's homonym extension, and does not register EmailAutolinkParser.
 */
final class AutolinkExtension implements ConfigurableExtensionInterface
{
	public function configureSchema(ConfigurationBuilderInterface $builder): void
	{
		$builder->addSchema('autolink', Expect::structure([
			'allowed_protocols' => Expect::listOf('string')->default(['http', 'https', 'ftp'])->mergeDefaults(false),
			'default_protocol' => Expect::string()->default('http'),
		]));
	}

	public function register(EnvironmentBuilderInterface $environment): void
	{
		$environment->addInlineParser(new UrlAutolinkParser(
			$environment->getConfiguration()->get('autolink.allowed_protocols'),
			$environment->getConfiguration()->get('autolink.default_protocol'),
		));
	}
}
