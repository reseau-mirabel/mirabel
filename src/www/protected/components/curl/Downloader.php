<?php

namespace components\curl;

/**
 * Download a remote file into a local file while checking its mime-type.
 */
class Downloader
{
	private const GENERIC_TYPES = ['binary/octet-stream'];

	private const TYPES = [
		'tableur' => [
			'application/vnd.oasis.opendocument.spreadsheet' => 'ods',
			'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => 'xlsx',
			'text/csv' => 'csv',
			'text/tab-separated-values' => 'tsv',
			'text/plain' => 'txt',
		],
		'kbart' => [
			'text/csv' => 'txt',
			'text/tab-separated-values' => 'txt',
			'text/plain' => 'txt',
		],
		'*' => ['*'],
	];

	private string $localFilepath = '';

	private string $remoteFilename = '';

	private string $type;

	/**
	 * @param "kbart"|"tableur"|"*" $type
	 */
	public function __construct(string $type)
	{
		if (!isset(self::TYPES[$type])) {
			throw new \Exception("Wrong constructor call.");
		}
		$this->type = $type;
	}

	public function download(string $url): void
	{
		if (!preg_match('#^(https?|ftp)://#', $url)) {
			throw new \Exception("L'URL '$url' n'est pas du http(s):// ni ftp://");
		}

		$path = tempnam(sys_get_temp_dir(), "mirabel_");
		$fh = fopen($path, 'w');
		if ($fh === false) {
			throw new \Exception("Impossible d'écrire dans le fichier temporaire '$path'.");
		}

		$curl = new \components\Curl();
		$curl->getFile($url, $fh);
		if ($curl->getSize() === 0) {
			throw new \Exception("Le téléchargement n'a aucun contenu.");
		}
		fclose($fh);
		if (stat($path)['size'] < 1) {
			throw new \Exception("Le fichier téléchargé est vide.");
		}
		\Yii::log("Téléchargé " . stat($path)['size'] . " octets", 'info');

		$extension = $this->getExtensionFromMimetype($curl->getContentType(), $path);
		$this->localFilepath = "$path.$extension";
		if (file_exists($this->localFilepath)) {
			\Yii::log("Téléchargé '$url' en écrivant par-dessus {$this->localFilepath}", 'warning');
		}
		rename($path, $this->localFilepath);
		chmod($this->localFilepath, 0660);
		$this->remoteFilename = $curl->getRemoteFilename();
	}

	public function getLocalFilepath(): string
	{
		return $this->localFilepath;
	}

	public function getRemoteFilename(): string
	{
		return $this->remoteFilename;
	}

	private function getExtensionFromMimetype(string $mimeType, string $path): string
	{
		if ($this->type === '*') {
			// Any non-empty mime-type is authorized.
			$mimeTypes = require(\Yii::getPathOfAlias('system.utils.fileExtensions') . '.php');
			if (isset($mimeTypes[$mimeType])) {
				return $mimeTypes[$mimeType];
			}
			return \CFileHelper::getExtensionByMimeType($path) ?? "";
		}

		if (!$mimeType || self::matchStart($mimeType, SELF::GENERIC_TYPES) === '') {
			$mimeType = \CFileHelper::getMimeType($path);
		}
		$allowedTypes = self::TYPES[$this->type];
		$matchingMime = self::matchStart($mimeType, $allowedTypes);
		if ($matchingMime === '') {
			throw new \Exception("Le fichier téléchargé n'est pas du type '{$this->type}', il est déclaré '$mimeType'.");
		}
		return $matchingMime;
	}

	private static function matchStart(string $needle, array $stack): string
	{
		$pos = strpos($needle, ';');
		if ($pos !== false) {
			$needle = substr($needle, 0, $pos);
		}
		return $stack[$needle] ?? '';
	}
}
