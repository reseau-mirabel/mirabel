<?php

namespace components;

/**
 * Handles an access token (to be stored in a cookie) that contains:
 * - a regexp on routes which is inserted in "#^…#",
 *   e.g. "revue/" matches any action in the "revue" controller,
 *   and 'titre/(view|verify)$' matches titre/view and titre/verify
 * - an expiration timestamp.
 * A control key is transparently added to the token, and checked when decoding the token.
 */
class AccessToken
{
	public const RES_NONE = 0;

	public const RES_OK = 1;

	public const RES_EXPIRED = 2;

	private const PACK_FORMAT = "a16Qa*"; // key timestamp path-regexp

	private const UNPACK_FORMAT = "a16key/Quntil/a*pathRegexp";

	private int $time;

	private string $path;

	public function __construct(string $path, int $time = 0)
	{
		$this->path = $path;
		if ($time === 0) {
			$time = time();
		}
		$this->time = $time;
	}

	public function checkToken(string $token): int
	{
		if ($token === '') {
			return self::RES_NONE;
		}
		$decoded = self::decode($token);
		if (!isset($decoded['key'])) {
			return self::RES_NONE;
		}
		if ($decoded['key'] !== self::computeKey($decoded)) {
			return self::RES_NONE;
		}

		if (!preg_match("#^{$decoded['pathRegexp']}#", $this->path)) {
			return self::RES_NONE;
		}
		if ($decoded['until'] < $this->time) {
			return self::RES_EXPIRED;
		}
		return self::RES_OK;
	}

	public function createToken(): string
	{
		return self::encode($this->path, $this->time);
	}

	public static function getExpiration(string $token): int
	{
		$d = self::decode($token);
		return $d['until'] ?? 0;
	}

	private static function computeKey(array $decoded): string
	{
		$str = sprintf("%u %s", $decoded['until'], $decoded['pathRegexp']);
		return md5($str, true);
	}

	private static function decode(string $encoded): array
	{
		$unbased = base64_decode($encoded, true);
		if ($unbased === false) {
			return [];
		}
		$values = unpack(self::UNPACK_FORMAT, $unbased);
		if ($values === false) {
			return [];
		}
		return $values;
	}

	private static function encode(string $pathRegexp, int $timestamp): string
	{
		$key = self::computeKey(['pathRegexp' => $pathRegexp, 'until' => $timestamp]);
		$based = base64_encode(pack(self::PACK_FORMAT, $key, $timestamp, $pathRegexp));
		return rtrim($based, '=');
	}
}
