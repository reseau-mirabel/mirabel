<?php

namespace components;

use Yii;

class WebSidebar extends \CApplicationComponent
{
	/**
	 * @var array Context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public array $menu = [];

	/**
	 * @var array Alert menu items, under the main sidebar menu.
	 */
	public array $menuExt = [];

	/**
	 * @var string Raw HTML displayed just under the main sidebar menu.
	 */
	public string $sidebarInsert = "";

	/**
	 * @var ?\BootMenu
	 */
	private $bottomMenu = null;

	/**
	 * @var ?\BootMenu
	 */
	private $topMenu = null;

	public function isEmpty(): bool
	{
		if (strlen($this->sidebarInsert) > 0) {
			return false;
		}
		if (Yii::app()->user->checkAccess("avec-partenaire")) {
			$this->prepareWidgets();
			if ($this->topMenu && count($this->topMenu->items) > 1) {
				return false;
			}
			if ($this->bottomMenu && count($this->bottomMenu->items) > 0) {
				return false;
			}
		}
		return true;
	}

	public function render(): void
	{
		if ($this->topMenu === null) {
			$this->prepareWidgets();
		}
		if ($this->topMenu && count($this->topMenu->items) > 1) {
			$this->topMenu->run();
		}
		if ($this->sidebarInsert) {
			echo $this->sidebarInsert;
		}
		if ($this->bottomMenu && count($this->bottomMenu->items) > 1) {
			$this->bottomMenu->run();
		}
		$this->reset();
	}

	private function reset(): void
	{
		$this->menu = [];
		$this->menuExt = [];
		$this->bottomMenu = null;
		$this->topMenu = null;
		$this->sidebarInsert = "";
	}

	private function prepareWidgets(): void
	{
		$controller = Yii::app()->getController();
		if ($this->menu) {
			$menu = array_merge(
				[
					['label' => 'Opérations', 'itemOptions' => ['class' => 'nav-header']],
				],
				$this->menu
			);
			$topMenu = $controller->createWidget(
				'bootstrap.widgets.BootMenu',
				[
					'type' => 'list',
					'items' => $menu,
					'htmlOptions' => ['class' => 'well'],
				]
			);
			assert($topMenu instanceof \BootMenu);
			$this->topMenu = $topMenu;
		}
		if ($this->menuExt) {
			$bottomMenu = $controller->createWidget(
				'bootstrap.widgets.BootMenu',
				[
					'type' => 'list',
					'items' => $this->menuExt,
					'htmlOptions' => ['class' => 'alert', 'style' => 'margin-top: 2ex'],
				]
			);
			assert($bottomMenu instanceof \BootMenu);
			$this->bottomMenu = $bottomMenu;
		}
	}
}
