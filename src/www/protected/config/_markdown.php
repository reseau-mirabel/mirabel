<?php

use components\markdown\AutolinkExtension;
use League\CommonMark\Environment\Environment;
use League\CommonMark\Extension\CommonMark\CommonMarkCoreExtension;
use League\CommonMark\Extension\CommonMark\Node\Block\Heading;
use League\CommonMark\Extension\CommonMark\Node\Inline\Link;
use League\CommonMark\Extension\DefaultAttributes\DefaultAttributesExtension;
use League\CommonMark\Extension\Strikethrough\StrikethroughExtension;
use League\CommonMark\Extension\Table\Table;
use League\CommonMark\Extension\Table\TableExtension;
use League\CommonMark\Extension\TaskList\TaskListExtension;
use League\CommonMark\MarkdownConverter;
use League\CommonMark\Node\Block\Paragraph;
use League\CommonMark\Node\RawMarkupContainerInterface;
use League\CommonMark\Node\StringContainerHelper;

return static function () {
	$environment = new Environment([
		'allow_unsafe_links' => false,
		'html_input' => 'allow', // HTML should be filtered through HtmlPurifier
		'max_nesting_level' => 20,
		'default_attributes' => [
			// Overwrite defaults and set Heading.id.
			Heading::class => [
				'class' => null,
				'id' => static function (Heading $node) {
					$text = StringContainerHelper::getChildText($node, [RawMarkupContainerInterface::class]);
					$asciiText = preg_replace(
						'/[^\w-]+/',
						'',
						str_replace(
							' ',
							'-',
							transliterator_transliterate('Any-Latin; Latin-ASCII; Lower()', trim($text))
						)
					);
					$slug = substr($asciiText, 0, 50);
					return "h{$node->getLevel()}-{$slug}";
				},
			],
			Link::class => [
				'class' => null,
				'target' => null,
			],
			Paragraph::class => [
				'class' => null,
			],
			Table::class => [
				'class' => null,
			],
		],
	]);
	$environment->addExtension(new CommonMarkCoreExtension());
	$environment->addExtension(new AutolinkExtension());
	$environment->addExtension(new StrikethroughExtension());
	$environment->addExtension(new TableExtension());
	$environment->addExtension(new TaskListExtension());
	$environment->addExtension(new DefaultAttributesExtension());
	return new MarkdownConverter($environment);
};
