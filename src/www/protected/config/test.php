<?php

/**
 * Return a valid configuration for Yii::app() in functional testing.
 */

if (!is_dir('/tmp/mirabel-tests-assets')) {
	@mkdir('/tmp/mirabel-tests-assets');
}

defined('YII_ENV') or define('YII_ENV', 'test');

$mainConfig = require __DIR__ . '/main.php';
unset($mainConfig['components']['log'], $mainConfig['components']['session']);
$testConfig = [
	'components' => [
		'assetManager' => [
			'basePath' => '/tmp/mirabel-tests-assets',
		],
		'db' => [
			'class' => \components\db\Connection::class,
			'connectionString' => 'mysql:host=' . (getenv('MIRABEL_MYSQL_HOST') ?: 'localhost') . ';dbname=mirabel2_tests',
			'username' => (getenv('MIRABEL_MYSQL_USER') ?: 'mirabel'),
			'password' => (getenv('MIRABEL_MYSQL_PASSWORD') ?: 'mirabel'),
			'charset' => 'utf8mb4',
			'enableProfiling' => 0,
			'enableParamLogging' => 0,
			'schemaCachingDuration' => 3600,
			'autoConnect' => false,
			'initSQLs' => ["SET sql_mode = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION'"],
		],
		'cache' => [
			'class' => 'CFileCache',
		],
		'matomo' => [
			'siteId' => 0, // 0 => disabled
			'tokenAuth' => '', // Disable the tracking within the API.
		],
		'sphinx' => [
			'connectionString' => 'mysql:unix_socket=' . getenv('MIRABEL_MANTICORE_SOCKET'),
			'tablePrefix' => 'test_',
		],
	],
	'name' => 'Mir@bel TEST',
	'params' => [
		'baseUrl' => 'http://localhost',
		'doc-path' => 'tests/_data/doc-interne',
		'email.host' => 'debug', // overwrite the DB Config: never send an email
		'matomo' => [
			'siteId' => 0,
			'rootUrl' => '',
			'tokenAuth' => '',
		],
		'auto-inscription-affiche-mdp' => false,
		'utilisateurs-par-lot' => true,
	],
];

return \CMap::mergeArray($mainConfig, $testConfig);
