<?php

/**
 * Do not modify this file unless you're a developper.
 * For configuring the application, use "local.php" in the same directory.
 */

setlocale(LC_ALL, 'fr_FR.utf8');
ini_set('date.timezone', 'Europe/Paris');
umask(002);

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return [
	'basePath' => dirname(__DIR__),
	'language' => 'fr',
	'charset' => 'UTF-8',

	// preloading components
	'preload' => ['bootstrap', 'log'],

	// autoloading model and component classes
	'import' => [
		'application.models.*',
		'application.models.abstract.*',
		'application.models.behaviors.*',
		'application.models.decorators.*',
		'application.models.forms.*',
		'application.models.import.*',
		'application.models.interfaces.*',
		'application.models.searches.*',
		'application.models.serialized.*',
		'application.models.traits.*',
		'application.components.*',
		'ext.bootstrap.widgets.*',
		'ext.validators.*',
	],

	'modules' => [
		'api',
	],

	// application components
	'components' => [
		'assetManager' => [
			'class' => \components\AssetManager::class,
			'basePath' => DATA_DIR . '/cache/assets',
		],
		'authManager' => [
			'enabled' => false,
		],
		'bootstrap' => [
			'class' => 'ext.bootstrap.components.Bootstrap',
			'responsiveCss' => true,
			'plugins' => [
				'popover' => [
					'suffix' => '.on("click", function(e) { e.preventDefault(); return false; })',
				],
				'tooltip' => [
					'selector' => 'a.tooltip', // bind the plugin tooltip to anchor tags with the 'tooltip' class
					'options' => [
						'placement' => 'bottom', // place the tooltips below instead
					],
				],
				// To prevent a plugin from being loaded set it to false as demonstrated below
				'transition' => false, // disable CSS transitions
				// If you need help with configuring the plugins, please refer to Bootstrap's own documentation:
				// https://getbootstrap.com/2.3.2/
			],
		],
		'cache' => [
			'class' => 'CFileCache',
			'cachePath' => DATA_DIR . '/cache/yii',
		],
		'coreMessages' => [
			'basePath' => null,
		],
		'db' => [
			'class' => \CDbConnection::class,
			'enableProfiling' => false,
			'enableParamLogging' => false,
			'schemaCachingDuration' => 3600,
			'initSQLs' => ["SET sql_mode = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION'"],
			'attributes' => [
				//\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => false, // When true, the MySQL driver always fetches all the data at once
				\PDO::ATTR_STRINGIFY_FETCHES => true, // Backward compatibility, required for PHP8.1+
			],
		],
		'errorHandler' => [
			// use 'site/error' action to display errors
			'errorAction' => 'site/error',
		],
		'format' => [
			'class' => '\extensions\format\ExtFormatter',
			'dateFormat' => 'd/m/Y',
			'datetimeFormat' => 'd/m/Y H:i',
			'timeFormat' => 'H:i',
			'booleanFormat' => ['Non', 'Oui'],
			'numberFormat' => ['decimals' => 2, 'decimalSeparator' => ',', 'thousandSeparator' => ' '],
		],
		'htmlpurifier' => function () {
			// Default config, but CMS uses its own config.
			$config = \HTMLPurifier_HTML5Config::createDefault();
			$config->set('Core.Encoding', 'UTF-8');
			$config->set('Core.EscapeNonASCIICharacters', false);
			$purifier = new \CHtmlPurifier();
			$purifier->setOptions($config);
			return $purifier;
		},
		'importLogger' => [
			'class' => '\processes\kbart\Logger',
		],
		'log' => [
			'class' => 'CLogRouter',
			'routes' => [
				[
					'class' => 'CFileLogRoute',
					'levels' => 'error',
					'logFile' => 'errors.log',
					'logPath' => DATA_DIR . '/logs',
					'except' => ['exception.CHttpException.400', 'exception.CHttpException.403', 'exception.CHttpException.404', 'exception.DetailedHttpException.404'],
				],
				[
					'class' => 'CFileLogRoute',
					'levels' => 'error',
					'logFile' => 'errors_4XX.log',
					'logPath' => DATA_DIR . '/logs',
					'categories' => ['exception.CHttpException.400', 'exception.CHttpException.403', 'exception.CHttpException.404', 'exception.DetailedHttpException.404'],
				],
				[
					'class' => 'CFileLogRoute',
					'levels' => 'warning',
					'logPath' => DATA_DIR . '/logs',
					'logFile' => 'warnings.log',
				],
			],
		],
		'markdown' => require __DIR__ . '/_markdown.php',

		// user tracking
		'matomo' => [
			'class' => '\components\Matomo',
		],

		'session' => [
			'class' => \CHttpSession::class,
			'gcProbability' => 1, // 1% chance of removing old sessions
			'savePath' => DATA_DIR . '/sessions', // \Yii::getPathOfAlias() is not yet available,
			'timeout' => 1800, // seconds
			'cookieParams' => [
				'secure' => true,
				'httponly' => true,
			],
		],
		'sidebar' => [
			'class' => \components\WebSidebar::class,
		],
		'sphinx' => [
			'class' => \components\sphinx\Connection::class,
			'autoConnect' => false,
			'enableProfiling' => (YII_ENV === 'dev'),
			'enableParamLogging' => (YII_ENV === 'dev'),
			'schemaCachingDuration' => (YII_ENV === 'dev' ? 0 : 3600),
			'emulatePrepare' => true,
			'driverMap' => [
				'mysql' => '\components\sphinx\Schema',
			],
		],
		'urlManager' => [
			'urlFormat' => 'path',
			'appendParams' => false,
			'showScriptName' => false,
			'rules' => [
				'site/page/<p:.+>' => 'site/page',
				'revue/<by:titre|titre-id|issn|sudoc|worldcat>/<id:.+>' => 'revue/viewBy',
				'upload/view/<file:.+>' => 'upload/view',
				'editeur/idref/<idref:\d+.>' => 'editeur/idref',
				'editeur/pays/<paysId:[A-Z-]{2,5}>/<paysNom:.+>' => 'editeur/pays',
				'editeur/pays/<paysId:.+>' => 'editeur/pays',
				'theme/<id:\d+>/<name:.+>' => 'categorie/revues',
				'doc/<view:.+>.md' => 'doc/view',
				// API
				'api/openapi.json' => 'api/default/openapiJson',
				'api/openapi.yaml' => 'api/default/openapiYaml',
				'api/editeurs/idref/<id>' => 'api/editeurs/idref',
				'api/themes' => 'api/themes/index',
				'api/themes/revue/<id:\d+>' => 'api/themes/revue',
				'api/themes/titre/<id:\d+>' => 'api/themes/titre',
				['api/<controller>/index', 'pattern' => 'api/mes/<controller:acces|titres>', 'defaultParams' => ['partenaire' => -1]],
				['api/<controller>/<action>', 'pattern' => 'api/mes/<controller:acces>/<action:changes>', 'defaultParams' => ['partenaire' => -1]],
				'api/<controller:\w+>/<id:\d+>' => 'api/<controller>/view',
				// Generic rules for pretty URLs
				'<controller:\w+>/<id:\d+>/<nom:[^\/]+>' => '<controller>/view',
				'<controller:\w+>/<id:\d+>' => '<controller>/view',
				'<controller:\w+>/<action:[\w-]+>/<id:\d+>' => '<controller>/<action>',
				'<controller:\w+>/<action:[\w-]+>' => '<controller>/<action>',
			],
		],
		'user' => [
			'class' => 'WebUser',
			'autoUpdateFlash' => false,
		],
	],
	'params' => [
		'version' => 'v2.5',
		// Mantis Project ID
		'mantisId' => 28,
		// Doc technique : chemin absolu, ou relatif à la racine du projet
		'doc-path' => 'doc/doc-interne',
	],
	'runtimePath' => DATA_DIR . '/cache/yii',
];
