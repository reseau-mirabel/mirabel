<?php

/**
 * This file completes and overwrites the configuration set in "main.php".
 */

defined('DATA_DIR') or define('DATA_DIR', dirname(__DIR__, 4) . '/data');

/**
 * If true, then the debug mode will be disabled (see "debug.php").
 * The configuration below also uses this setting
 */
defined('APP_PRODUCTION_MODE')
	or define('APP_PRODUCTION_MODE', true);

/**
 * Read and update each setting to suit your instance.
 * Settings in local.php will overwrite any setting from "main.php".
 */
return [
	// default: "Mir@bel"
	'name' => 'Mir@bel local',

	'components' => [
		// rw access to MariaDB/MySQL
		'db' => [
			'connectionString' => 'mysql:host=localhost;dbname=mirabel',
			'username' => 'mirabeluser',
			'password' => 'mirabelpass',
			'charset' => 'utf8mb4',
			'enableProfiling' => !APP_PRODUCTION_MODE,
			'enableParamLogging' => !APP_PRODUCTION_MODE,
			'schemaCachingDuration' => (APP_PRODUCTION_MODE ? 3600 : 0),
		],

		// user tracking
		'matomo' => [
			'siteId' => 0, // 0 => disabled
			'rootUrl' => 'https://matomo.localhost/', // trailing slash
			'tokenAuth' => '', // For tracking API. Generated with Matomo, Personal, Security, Auth tokens
		],

		'sphinx' => [
			'connectionString' => 'mysql:unix_socket=/tmp/sphinx-mirabel-mysql.socket', // by unix socket
			'tablePrefix' => 'devel_',
		],
	],

	// application-level parameters that can be accessed
	// with the syntax Yii::app()->params['paramName']
	'params' => [
		// Main emails. Other settings are on the configuration web page.
		'adminEmail' => 'contact@example.org',
		'emailAlerteEditeurs' => 'alerte@example.org',
		'email.host' => 'debug', // never send an email (overwrite the DB Config)

		// Useful for dev/test of Politique users.
		'auto-inscription-affiche-mdp' => false,

		// required for building URLs in console commands
		'baseUrl' => 'http://mirabel.localhost',

		// FEATURES
		// arbre thématique en page d'accueil
		'displayThemesOnHomePage' => true,
		// Display the roles of publishers
		'editeurs-roles' => true,
		// Display facets in the advanced search for journals
		'facettes' => true,
		// If true, an admin can create a batch of demo users
		'utilisateurs-par-lot' => false,

		// Uncomment the following line if CSP violations should not be enforced but just reported.
		//'security-csp-directive' => 'Content-Security-Policy-Report-Only',

		// To access Sherpa, a key is needed.
		// See https://v2.sherpa.ac.uk/api/
		'sherpa' => [
			'api-key' => '',
			'popover' => true,
		],

		/*
		 * If set, the API will use a distinct connection to the same DB
		 */
		'api.db' => [
			/*
			'connectionString' => 'mysql:host=localhost;dbname=mirabel',
			'username' => 'mirabelro',
			'password' => 'mir-read-only',
			'enableProfiling' => false,
			'enableParamLogging' => false,
			 */
		],

		/*
		// Used by the CLI script that uploads KBART onto a Bacon server.
		'bacon' => [
			'url' => 'https://cloud.abes.fr/...',
			'username' => '',
			'password' => '',
		],
		 */

		// do not change manually, should be filled up from the VCS when deployed
		'vcsVersion' => '',
	],
];
