<?php

// This is the configuration for yiic console application.

// It is loaded after "main.php" and "local.php"

return [
	'basePath' => dirname(__DIR__),
	'commandMap' => [
		'migrate' => [
			'class' => 'system.cli.commands.MigrateCommand',
			'migrationTable' => 'tbl_migration', // default value
			'migrationPath' => 'application.migrations',
			'templateFile' => 'application.migrations.template',
		],
	],
];
