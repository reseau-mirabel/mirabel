<?php

/**
 * Configuration for running the tests in Gitlab-CI. See .gitlab-ci.yml
 */

defined('APP_PRODUCTION_MODE')
	or define('APP_PRODUCTION_MODE', true);

return [
	'name' => 'Mir@bel local',

	'components' => [
		// rw access to MariaDB/MySQL
		'db' => [
			'connectionString' => sprintf('mysql:host=%s;database=mirabel2_tests', getenv('MIRABEL_MYSQL_HOST')),
			'username' => (getenv('MIRABEL_MYSQL_USER') ?: 'mirabel'),
			'password' => (getenv('MIRABEL_MYSQL_PASSWORD') ?: 'mirabel'),
			'charset' => 'utf8mb4',
			'enableProfiling' => false,
			'enableParamLogging' => false,
			'schemaCachingDuration' => 3600,
		],

		// user tracking
		'matomo' => [
			'siteId' => 0, // 0 => disabled
			'rootUrl' => 'https://matomo.localhost/', // trailing slash
			'tokenAuth' => '', // For tracking API. Generated with Matomo, Personal, Security, Auth tokens
		],

		'sphinx' => [
			'connectionString' => getenv('MIRABEL_SPHINX_SOCKET')
				? sprintf('mysql:unix_socket=%s', getenv('MIRABEL_SPHINX_SOCKET'))
				: sprintf('mysql:host=%s;unix_socket=%d', getenv('MIRABEL_SPHINX_HOST'), getenv('MIRABEL_SPHINX_PORT')),
			'tablePrefix' => 'test_',
		],
	],

	// application-level parameters that can be accessed
	// with the syntax Yii::app()->params['paramName']
	'params' => [
		// Main emails. Other settings are on the configuration web page.
		'adminEmail' => 'contact@example.org',
		'emailAlerteEditeurs' => 'alerte@example.org',
		'email.host' => 'debug', // never send an email (overwrite the DB Config)

		// Useful for dev/test of Politique users.
		'auto-inscription-affiche-mdp' => false,

		// required for building URLs in console commands
		'baseUrl' => 'http://mirabel.localhost',

		// FEATURES
		// arbre thématique en page d'accueil
		'displayThemesOnHomePage' => true,
		// Display the roles of publishers
		'editeurs-roles' => true,
		// Display facets in the advanced search for journals
		'facettes' => true,
		// If true, an admin can create a batch of demo users
		'utilisateurs-par-lot' => false,

		// Uncomment the following line if CSP violations should not be enforced but just reported.
		'security-csp-directive' => 'Content-Security-Policy-Report-Only',

		// To access Sherpa, a key is needed.
		// See https://v2.sherpa.ac.uk/api/
		'sherpa' => [
			'api-key' => '',
			'popover' => true,
		],

		'api.db' => [
		],

		// do not change manually, should be filled up from the VCS when deployed
		'vcsVersion' => '',
	],
];
