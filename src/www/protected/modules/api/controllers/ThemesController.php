<?php

use modules\api\models\actions\Themes;
use modules\api\models\actions\ThemesRevue;
use modules\api\models\actions\ThemesTitre;
use modules\api\models\Api;

class ThemesController extends CController
{
	public function actionIndex()
	{
		Api::processDefault(new Themes());
	}

	public function actionRevue(string $id)
	{
		Api::readParameters(Yii::app()->request);
		$engine = new ThemesRevue();
		$engine->revueId = (int) $id;
		$api = new Api($engine);
		$api->printResponse();
	}

	public function actionTitre(string $id)
	{
		Api::readParameters(Yii::app()->request);
		$engine = new ThemesTitre();
		$engine->titreId = (int) $id;
		$api = new Api($engine);
		$api->printResponse();
	}
}
