<?php

use modules\api\models\actions\AccesChanges;
use modules\api\models\actions\AccesPossessions;
use modules\api\models\actions\AccesRevue;
use modules\api\models\actions\AccesTitre;
use modules\api\models\Api;

class AccesController extends CController
{
	public function actionChanges()
	{
		$engine = new AccesChanges();
		$parameters = Api::readParameters(Yii::app()->request);
		$engine->setAttributes($parameters);
		$api = new Api($engine);
		$api->printResponse();
		//Api::processDefault(new AccesChanges);
	}

	public function actionIndex()
	{
		Api::processDefault(new AccesPossessions);
	}

	public function actionRevue()
	{
		Api::processDefault(new AccesRevue);
	}

	public function actionTitres()
	{
		Api::processDefault(new AccesTitre);
	}
}
