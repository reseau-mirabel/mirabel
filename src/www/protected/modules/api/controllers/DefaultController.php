<?php

use components\openapi\Specification;
use modules\api\models\actions\Aide;
use modules\api\models\Api;

class DefaultController extends Controller
{
	/**
	 * Web service help page
	 */
	public function actionIndex()
	{
		$decQuery = Api::readParameters(Yii::app()->request);
		$result = [];
		if (empty($decQuery->action)) {
			return $this->displayHelp();
		}
		if ($decQuery->action === 'find-one' && ($decQuery->model ?? '') === 'Titre' && !empty($decQuery->parameters)) {
			if (strncmp($_SERVER['REMOTE_ADDR'], '193.49.39.', 10) !== 0 && strncmp($_SERVER['REMOTE_ADDR'], '127.0.0.1', 9) !== 0) {
				throw new CHttpException(403, "Accès restreint par IP.");
			}
			$titre = Titre::model()->findByAttributes((array) $decQuery->parameters);
			if ($titre) {
				$result["response"] = $titre->attributes;
			} else {
				$result["response"] = null;
				$result["error"] = "No model matches these parameters";
			}
		}

		Controller::header('json');
		echo json_encode($result);
	}

	public function actionAide()
	{
		$engine = new Aide();
		$parameters = Api::readParameters(Yii::app()->request);
		$engine->setAttributes($parameters);
		$api = new Api($engine);
		$api->printResponse();
	}

	public function actionOpenapiJson()
	{
		$file = Yii::getPathOfAlias('webroot') . '/files/api/openapi.json';
		header('Access-Control-Allow-Origin: *');
		header('Content-Type: application/json; charset="UTF-8"');
		Yii::app()->getRequest()->sendFile("Mirabel_openapi.json", file_get_contents($file));
	}

	public function actionOpenapiYaml()
	{
		$file = Yii::getPathOfAlias('webroot') . '/files/api/openapi.yaml';
		header('Access-Control-Allow-Origin: *');
		header('Content-Type: text/x-yaml; charset="UTF-8"');
		Yii::app()->getRequest()->sendFile("Mirabel_openapi.yaml", file_get_contents($file));
	}

	private function displayHelp()
	{
		$file = Yii::getPathOfAlias('webroot') . '/files/api/openapi.json';
		if (!file_exists($file)) {
			throw new CHttpException(404, "La documentation de l'API est absente.");
		}
		if (!is_readable($file)) {
			throw new CHttpException(404, "La documentation de l'API n'est pas accessible.");
		}
		$api = new Specification(json_decode(file_get_contents($file)));

		return $this->render('aide', ['api' => $api]);
	}
}
