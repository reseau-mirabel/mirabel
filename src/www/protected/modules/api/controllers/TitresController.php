<?php

use modules\api\models\actions\Titre;
use modules\api\models\actions\Titres;
use modules\api\models\Api;

class TitresController extends CController
{
	public function actionIndex()
	{
		Api::processDefault(new Titres);
	}

	public function actionView()
	{
		Api::processDefault(new Titre);
	}
}
