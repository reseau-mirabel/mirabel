<?php

use modules\api\models\actions\Editeur;
use modules\api\models\Api;

class EditeursController extends CController
{
	public function actionIdref(string $id)
	{
		Api::readParameters(Yii::app()->request);
		$engine = new Editeur();
		$parameters = ['idref' => $id];
		$engine->setAttributes($parameters);
		$api = new Api($engine);
		$api->printResponse();
	}

	public function actionView(string $id)
	{
		Api::readParameters(Yii::app()->request);
		$engine = new Editeur();
		$engine->setAttributes(['id' => (int) $id]);
		$api = new Api($engine);
		$api->printResponse();
	}
}
