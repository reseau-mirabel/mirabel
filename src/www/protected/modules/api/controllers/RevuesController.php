<?php

use modules\api\models\actions\Revue;
use modules\api\models\actions\Revues;
use modules\api\models\Api;

class RevuesController extends CController
{
	public function actionIndex()
	{
		Api::processDefault(new Revues());
	}

	public function actionView()
	{
		Api::processDefault(new Revue());
	}
}
