<?php

namespace modules\api\models;

interface ActionInterface
{
	public function getAttributes();

	public function setAttributes($attributes);

	public function validate();

	public function getErrors();

	public function apply(): array;
}
