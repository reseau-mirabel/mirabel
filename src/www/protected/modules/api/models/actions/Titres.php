<?php

namespace modules\api\models\actions;

use modules\api\models\ActionInterface;
use modules\api\models\TitreFormater;
use Yii;

/**
 * En cas de modification de la structure, tenir à jour la documentation de l'API dans data/openapi.json.
 */
class Titres extends \CModel implements ActionInterface
{
	use \modules\api\models\FailOnUnknownParameter;
	use \modules\api\models\ValidatorsApi;
	use \modules\api\models\WithPartenaireAttribute;

	/**
	 * @var int|array
	 */
	public $id;

	/**
	 * @var int|array
	 */
	public $revueid;

	public $attributid = '';

	/**
	 * @var int|array
	 */
	public $worldcat;

	public $issn;

	public $sudoc;

	public $titre;

	/**
	 * @var bool
	 */
	public $actif;

	/**
	 * @var bool
	 */
	public $abonnement;

	/**
	 * @var bool
	 */
	public $possession;

	public $grappeid;

	public $themeid;

	public $offset = 0;

	public function afterConstruct()
	{
		$this->setScenario('search');
	}

	public function attributeNames()
	{
		return [
			'id', 'revueid', 'issn', 'worldcat', 'sudoc', 'titre', 'actif', 'attributid', 'grappeid', 'themeid',
			'partenaire', 'abonnement', 'possession',
			'offset',
		];
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['id, revueid, worldcat', 'validateSequence'],
			['sudoc, titre, attributid', 'safe'],
			['issn', 'validateIssns'],
			['partenaire', 'validatePartenaire'],
			['actif, abonnement, possession', 'boolean'],
			['offset, grappeid, themeid', 'numerical', 'integerOnly' => true],
		];
	}

	public function afterValidate()
	{
		if (($this->abonnement || $this->possession) && !$this->partenaireId) {
			$this->addError('partenaire', "Les filtres de possession et d'abonnement ne sont accessibles qu'avec le critère \"partenaire\".");
		}
		if ($this->titre) {
			$crit = str_replace(['%', '_'], ['', ''], $this->titre);
			if (strlen($crit) < 3) {
				$this->addError('titre', "Le filtre par titre doit porter sur au moins 3 caractères.");
			}
		}
		if ($this->attributid) {
			if (!$this->partenaireId) {
				$this->addError('attributid', "Le filtre par attribut n'est accessible qu'avec le critère \"partenaire\".");
				$this->attributid = 0;
			} else {
				$this->attributid = (int) Yii::app()->db
					->createCommand("SELECT id FROM Sourceattribut WHERE id = :id OR identifiant = :ident LIMIT 1")
					->queryScalar([':id' => $this->attributid, ':ident' => $this->attributid]);
				if (!$this->attributid) {
					$this->addError('attributid', "Le filtre par attribut doit utiliser le numéro ou l'identifiant textuel d'un attribut présent dans Mir@bel.");
				}
			}
		}
		if ($this->grappeid > 0) {
			$this->validateGrappe((int) $this->grappeid);
		}
		return parent::afterValidate();
	}

	public function apply(): array
	{
		$titresQuery = $this->search();
		$copy = clone $titresQuery;
		$copy->offset($this->offset)->limit(1000);
		$titres = $copy->queryAll();
		if (!$titres) {
			return [];
		}
		$formater = new TitreFormater();
		// add ISSN data, with a sub-query if many journals were found
		if (count($titres) < 50) {
			$formater->loadIssns($titres);
		} else {
			$formater->loadIssns($titresQuery);
		}
		return $formater->formatAll($titres);
	}

	public function search(): \CDbCommand
	{
		$command = Yii::app()->db->createCommand()
			->select("t.*, GROUP_CONCAT(DISTINCT Editeur.nom SEPARATOR '\t') AS editeurs, (1) AS noquotes")
			->from('Titre t')
			->leftJoin('Titre_Editeur te', "te.titreId = t.id AND ancien = 0")
			->leftJoin('Editeur', "te.editeurId = Editeur.id")
			->group("t.id")
			->order("t.titre ASC, t.revueId");

		if ($this->partenaireId) {
			$partenaireId = (int) $this->partenaireId;
			$command->setSelect($command->select . ", pt.identifiantLocal, pt.bouquet");
			$extraUnion = "";
			if ($this->possession) {
				if ($this->abonnement) {
					$command->leftJoin('Partenaire_Titre pt', "pt.titreId = t.id AND pt.partenaireId = $partenaireId");
					$extraUnion = "UNION SELECT titreId, 'possession' AS diffusion, 1 AS proxy, NULL AS proxyURL FROM Partenaire_Titre WHERE partenaireId = $partenaireId";
				} else {
					$command->join('Partenaire_Titre pt', "pt.titreId = t.id AND pt.partenaireId = $partenaireId");
					$command->join(
						"(SELECT titreId, 'possession' AS diffusion, 1 AS proxy, NULL AS proxyUrl FROM Partenaire_Titre WHERE partenaireId = $partenaireId) AS abo",
						"abo.titreId = t.id"
					);
				}
			} else {
				// LEFT JOIN just for local identifiers (ID of the title local to the partner)
				$command->leftJoin('Partenaire_Titre pt', "pt.titreId = t.id AND pt.partenaireId = $partenaireId");
			}
			if ($this->abonnement) {
				$aboId = \Abonnement::ABONNE;
				$command
					->join(
						<<<EOSQL
							(
							  SELECT s.titreId, 'abonné' AS diffusion, a.proxy, a.proxyUrl
							  FROM Abonnement a
							    JOIN Service s ON s.ressourceId = a.ressourceId
							  WHERE a.partenaireId = $partenaireId AND a.mask = $aboId AND a.collectionId IS NULL
							UNION
							  SELECT s.titreId, 'abonné' AS diffusion, a.proxy, a.proxyUrl
							  FROM Abonnement a
							    JOIN Service_Collection sc ON sc.collectionId = a.collectionId
							    JOIN Service s ON sc.serviceId = s.id
							  WHERE a.partenaireId = $partenaireId AND a.mask = $aboId
							$extraUnion
							) AS abo
							EOSQL,
						"t.id = abo.titreId"
					);
			}
			if ($this->attributid) {
				$command->join("AttributTitre", "t.id = AttributTitre.titreId AND AttributTitre.sourceattributId = {$this->attributid}");
			}
		}
		if ($this->actif) {
			$command->andWhere("t.obsoletePar IS NULL");
		}
		if ($this->id) {
			if (is_array($this->id)) {
				$command->andWhere('t.id IN(' . join(',', array_map('intval', $this->id)) . ")");
			} else {
				$command->andWhere('t.id = ' . (int) $this->id);
			}
		}
		if ($this->revueid) {
			if (is_array($this->revueid)) {
				$command->andWhere('t.revueId IN(' . join(',', array_map('intval', $this->revueid)) . ")");
			} else {
				$command->andWhere('t.revueId = ' . (int) $this->revueid);
			}
			$command->order("t.revueId, (t.obsoletePar IS NULL) DESC, t.dateDebut DESC");
		}
		if ($this->themeid > 0) {
			$command->join('Categorie_Revue cr', "t.revueId = cr.revueId")
				->andWhere("cr.categorieId = " . (int) $this->themeid);
		}
		if ($this->grappeid > 0) {
			$command->join('Grappe_Titre gt', "t.id = gt.titreId")
				->andWhere("gt.grappeId = " . (int) $this->grappeid);
		}
		if ($this->titre) {
			if (strpos($this->titre, "%") === false) {
				$command->andWhere(
					't.titre IN (:titre, :nopref) OR t.sigle = :sigle',
					[':titre' => $this->titre, ':nopref' => self::removePrefix($this->titre), ':sigle' => $this->titre]
				);
			} else {
				$title = str_replace(
					['%', '_'],
					['\%', '\_'],
					self::removePrefix(trim($this->titre, '%'))
				);
				$command->andWhere(
					't.titre LIKE :titre',
					[':titre' => preg_match('/^%.+%/', $this->titre) ? "%$title%" : "$title%"]
				);
			}
		}

		if ($this->issn || $this->sudoc || $this->worldcat) {
			$command->leftJoin("Issn i", "i.titreId = t.id")
				->group("t.id");
			if ($this->worldcat) {
				if (is_array($this->worldcat)) {
					$command->andWhere('i.worldcatOcn IN(' . join(',', array_map('intval', $this->worldcat)) . ")");
				} else {
					$command->andWhere('i.worldcatOcn = :worldcat', [':worldcat' => (int) $this->worldcat]);
				}
			}
			if ($this->sudoc) {
				$command->andWhere('i.sudocPpn = :sudoc', [':sudoc' => $this->sudoc]);
			}
			if ($this->issn) {
				if (is_array($this->issn)) {
					$in = join(
						"','",
						array_filter(
							$this->issn,
							function ($x) {
								return preg_match('/^[\dX-]+$/', $x);
							}
						)
					);
					$command->andWhere("i.issn IN('$in') OR i.issnl IN('$in')");
				} else {
					$command->andWhere(
						"i.issn = :i1 OR i.issnl = :i2",
						[':i1' => $this->issn, ':i2' => $this->issn]
					);
				}
			}
		}

		return $command;
	}

	protected static function removePrefix(string $title): string
	{
		$prefixes = array_filter(explode('/', (string) \Config::read('import.prefixes')));
		if (!$prefixes) {
			return $title;
		}
		foreach ($prefixes as $pref) {
			if (strncasecmp($title, $pref, strlen($pref)) === 0) {
				return substr($title, strlen($pref));
			}
		}
		return $title;
	}

	private function validateGrappe(int $grappeId): void
	{
		$grappe = \Grappe::model()->findByPk((int) $this->grappeid);
		if (!($grappe instanceof \Grappe)) {
			$this->addError('grappeid', "Aucune grappe publique n'a cet identifiant $grappeId.");
			return;
		}
		if (in_array((int) $grappe->diffusion, [\Grappe::DIFFUSION_PARTAGE, \Grappe::DIFFUSION_PUBLIC], true)) {
			return;
		}
		if (!$this->partenaireId) {
			$this->addError('grappeid', "Aucune grappe publique n'a cet identifiant $grappeId.");
		}
		if ($grappe->diffusion === \Grappe::DIFFUSION_AUTHENTIFIE) {
			return;
		}
		// DIFFUSION_PARTENAIRE
		if ($grappe->partenaireId !== $this->partenaireId) {
			$this->addError('grappeid', "Aucune grappe visible n'a cet identifiant $grappeId.");
		}
	}
}
