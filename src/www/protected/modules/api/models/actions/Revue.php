<?php

namespace modules\api\models\actions;

use modules\api\models\ActionInterface;
use modules\api\models\TitreFormater;
use Yii;

/**
 * En cas de modification de la structure, tenir à jour la codumentation de l'API dans data/openapi.json.
 * Éventuellement, utiliser editor.swagger.io pour rédiger.
 */
class Revue extends \CModel implements ActionInterface
{
	use \modules\api\models\FailOnUnknownParameter;

	public $id;

	public function attributeNames()
	{
		return ['id'];
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['id', 'numerical', 'integerOnly' => true, 'min' => 1],
		];
	}

	public function apply(): array
	{
		$revue = Yii::app()->db
			->createCommand("SELECT * FROM Revue WHERE statut = 'normal' AND id = :id")
			->queryRow(true, [':id' => (int) $this->id]);
		if (!$revue) {
			$cid = (int) Yii::app()->db
				->createCommand("SELECT cibleId FROM RedirectionRevue WHERE sourceId = :id")
				->queryScalar([':id' => (int) $this->id]);
			if ($cid) {
				return [
					'code' => 301,
					'nouvelid' => $cid,
					'url' => Yii::app()->createAbsoluteUrl(sprintf('/api/revues/%d', $cid)),
				];
			}
			return [
				'code' => 404,
				'message' => "Aucune revue ne correspond.",
			];
		}

		$revue['dermodif'] = Yii::app()->db
			->createCommand(
				"SELECT hdateVal FROM Intervention WHERE revueId = {$revue['id']} ORDER BY id DESC LIMIT 1"
			)
			->queryScalar();
		$apiTitre = new Titres();
		$apiTitre->revueid = $revue['id'];
		$titres = $apiTitre->search()
			->order("(t.obsoletePar IS NULL) DESC, t.dateDebut DESC")
			->queryAll();
		if (empty($titres)) {
			return [];
		}
		return self::formatResult($revue, $titres);
	}

	public static function formatResult($attributes, array $titres): array
	{
		$result = [];
		if (is_array($attributes)) {
			$attributes = (object) $attributes;
		}

		$integers = [
			// new-name => DB-name
			'id' => 'id',
			'dermodif' => 'dermodif',
			'derverif' => 'hdateVerif',
		];
		foreach ($integers as $new => $old) {
			if (isset($attributes->{$old})) {
				$result[$new] = (int) $attributes->{$old};
			} else {
				$result[$new] = null;
			}
		}

		$formater = new TitreFormater();
		$formater->loadIssns($titres);
		$result['titres'] = $formater->formatAll($titres);

		return $result;
	}
}
