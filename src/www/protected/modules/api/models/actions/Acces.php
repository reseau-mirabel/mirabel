<?php

namespace modules\api\models\actions;

use models\import\Normalize;
use modules\api\models\ActionInterface;

/**
 * En cas de modification de la structure, tenir à jour la codumentation de l'API dans data/openapi.json.
 * Éventuellement, utiliser editor.swagger.io pour rédiger.
 */
class Acces extends \CModel implements ActionInterface
{
	use \modules\api\models\FailOnUnknownParameter;
	use \modules\api\models\WithPartenaireAttribute;

	public $titreIds = [];

	public $displayIssns = false;

	protected $titreSql = "";

	public function afterConstruct()
	{
		$this->setScenario('search');
	}

	public function attributeNames()
	{
		return ['partenaire', 'titreIds'];
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [];
	}

	public function apply(): array
	{
		return array_map(
			[$this, 'formatAccess'],
			$this->search()->queryAll()
		);
	}

	public function formatAccess(array $row)
	{
		$struct = array_filter(
			[
				"id" => (int) $row["id"],
				"url" => $row["url"],
				"ressource" => $row["ressource"],
				"ressourceid" => (int) $row["ressourceid"],
				"ressourcesigle" => $row["ressourcesigle"],
				"revueid" => (int) $row['revueid'],
				"titreid" => (int) $row["titreid"],
				"contenu" => $row["contenu"],
				"diffusion" => $row["diffusion"],
				"lacunaire" => (bool) $row["lacunaire"],
				"selection" => (bool) $row["selection"],
				"rssurl" => $row["rssurl"] ?: null,
				"mailurl" => $row["mailurl"] ?: null,
				"dernumurl" => $row["dernumurl"] ?: null,
				"voldebut" => $row["voldebut"] > 0 ? (int) $row["voldebut"] : null,
				"nodebut" => $row["nodebut"] > 0 ? (int) $row["nodebut"] : null,
				"numerodebut" => $row["numerodebut"] ?: null,
				"volfin" => $row["volfin"] > 0 ? (int) $row["volfin"] : null,
				"nofin" => $row["nofin"] > 0 ? (int) $row["nofin"] : null,
				"numerofin" => $row["numerofin"] ?: null,
				"datedebut" => $row["datedebut"],
				"datefin" => $row["datefin"] ?: null,
				"dateinfo" => $row["dateinfo"] ?: ($row["embargoinfo"] ? Normalize::getReadableKbartEmbargo($row["embargoinfo"]) : null),
				"embargoinfo" => $row["embargoinfo"] ?: null,
			],
			function ($x) {
				return $x !== null;
			}
		);
		if ($this->partenaireId && !empty($row['proxy']) && !empty($row['url'])) {
			$proxys = array_filter([$row['proxyUrl'], $this->part->proxyUrl]); // internal first, so local first
			if ($proxys) {
				$url = $row['url'];
				foreach ($proxys as $proxy) {
					$url = \Proxy::proxifyUrl($url, $proxy);
				}
				$struct['urlproxy'] = $url;
			}
		}
		if (isset($row['identifiantpartenaire'])) {
			$struct["identifiantpartenaire"] = $row['identifiantpartenaire'];
		}
		if (isset($row['issn'])) {
			$struct["issn"] = explode(' ', $row['issn']);
		}
		ksort($struct);
		return $struct;
	}

	public function search()
	{
		$select = <<<EOSQL
			s.id, IF(s.url <> '', s.url, r.url) AS url, s.titreId AS titreid, s.ressourceId AS ressourceid, s.type AS contenu,
			 s.lacunaire, s.selection,
			 s.alerteRssUrl AS rssurl, s.alerteMailUrl AS mailurl, s.derNumUrl AS dernumurl,
			 s.volDebut AS voldebut, s.noDebut AS nodebut, s.numeroDebut AS numerodebut,
			 s.volFin AS volfin, s.noFin AS nofin, s.numeroFin AS numerofin,
			 s.dateBarrDebut AS datedebut, s.dateBarrFin AS datefin, s.dateBarrInfo AS dateinfo, s.embargoInfo AS embargoinfo
			, r.nom AS ressource, r.sigle AS ressourcesigle
			, Titre.revueId AS revueid
			EOSQL;
		$cmd = \Yii::app()->db->createCommand()
			->select($select)
			->from('Service s')
			->join('Ressource r', "r.id = s.ressourceId")
			->join('Titre', "Titre.id = s.titreId")
			->group('s.id');
		if ($this->titreIds) {
			$titreIds = join(", ", array_map('intval', $this->titreIds));
			if ($this->displayIssns) {
				$select .= ", t.issn AS issn";
				$cmd->leftJoin(<<<SQL
					(SELECT Titre.id, GROUP_CONCAT(issn SEPARATOR ' ') AS issn
					FROM Titre JOIN Issn ON Titre.id = Issn.titreId
					WHERE Titre.id IN ($titreIds)
					GROUP BY Titre.id
					) AS t
					SQL,
					"s.titreId = t.id"
				);
			}
			$cmd->where("s.titreId IN ($titreIds)")
				->order("titreId, s.id");
		}
		if ($this->titreSql) {
			$cmd->join("({$this->titreSql}) AS sub1", "s.titreId = sub1.id");
		}
		if ($this->partenaireId) {
			// identifier local to the partner
			$select .= ", pt.identifiantLocal AS identifiantpartenaire";
			$cmd->leftJoin('Partenaire_Titre pt', "pt.titreId = s.titreId AND pt.partenaireId = {$this->partenaireId}");

			// diffusion, either public or specific to the partner
			if ($this->titreSql) {
				$select .= ", sub1.diffusion, sub1.proxy, sub1.proxyUrl";
			} else {
				$select .= ", IF(s.acces = 'Libre' OR MAX(a.id) IS NULL, LOWER(s.acces), 'abonné') AS diffusion"
					. ", MAX(a.proxy) AS proxy"
					. ", MAX(a.proxyUrl) AS proxyUrl";
				$cmd->leftJoin(
					"Service_Collection sc",
					"sc.serviceId = s.id"
				)
					->leftJoin(
						'Abonnement a',
						"((sc.collectionId IS NULL AND s.ressourceId = a.ressourceId) OR (sc.collectionId IS NOT NULL AND sc.collectionId = a.collectionId))"
						. " AND a.mask = " . \Abonnement::ABONNE . " AND a.partenaireId = {$this->partenaireId}"
					);
			}
		} else {
			$select .= ", LOWER(s.acces) AS diffusion";
		}
		$cmd->select($select);
		return $cmd;
	}
}
