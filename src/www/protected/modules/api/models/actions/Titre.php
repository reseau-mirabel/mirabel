<?php

namespace modules\api\models\actions;

use modules\api\models\ActionInterface;
use modules\api\models\TitreFormater;

/**
 * En cas de modification de la structure, tenir à jour la codumentation de l'API dans data/openapi.json.
 * Éventuellement, utiliser editor.swagger.io pour rédiger.
 */
class Titre extends Titres implements ActionInterface
{
	public function attributeNames()
	{
		return ['id'];
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['id', 'numerical', 'integerOnly' => true, 'min' => 1],
		];
	}

	public function apply(): array
	{
		$titres = $this->search()->queryAll();
		if ($titres) {
			$formater = new TitreFormater();
			$formater->loadIssns($titres);
			$result = $formater->format($titres[0]);
			$result['relations_editeurs'] = self::getRelations((int) $titres[0]['id']);
			return $result;
		}
		$cid = (int) \Yii::app()->db
			->createCommand("SELECT cibleId FROM RedirectionTitre WHERE sourceId = :id")
			->queryScalar([':id' => (int) $this->id]);
		if ($cid) {
			return [
				'code' => 301,
				'nouvelid' => $cid,
				'url' => \Yii::app()->createAbsoluteUrl(sprintf('/api/titres/%d', $cid)),
			];
		}
		return [
			'code' => 404,
			'message' => "Aucun titre ne correspond.",
		];
	}

	private static function getRelations(int $titreId): array
	{
		$relations = \Yii::app()->db
			->createCommand(<<<EOSQL
				SELECT te.editeurId, te.ancien, te.commercial, te.intellectuel, te.role, e.nom
				FROM Titre_Editeur te
					JOIN Editeur e ON te.editeurId = e.id
				WHERE te.titreId = {$titreId}
				ORDER BY e.nom, te.editeurId
				EOSQL
			)
			->query();
		$result = [];
		foreach ($relations as $r) {
			$result[] = Editeur::formatRelation($r);
		}
		return $result;
	}
}
