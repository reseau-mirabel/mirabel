<?php

namespace modules\api\models\actions;

use modules\api\models\ActionInterface;

class ThemesTitre implements ActionInterface
{
	public int $titreId = 0;

	private ?\Revue $revue;

	public function getAttributes()
	{
		return [
			'titreId' => $this->titreId,
		];
	}

	public function setAttributes($attributes)
	{
		if (isset($attributes['titreId'])) {
			$this->titreId = (int) $attributes['titreId'];
		}
	}

	public function validate()
	{
		if ($this->titreId > 0) {
			$this->revue = \Revue::model()->findbySql(
				"SELECT r.* FROM Revue r JOIN Titre t ON t.revueId = r.id WHERE t.id = :id",
				[':id' => $this->titreId]
			);
		}
		return ($this->revue instanceof \Revue);
	}

	public function getErrors()
	{
		if ($this->revue instanceof \Revue) {
			return [];
		}
		return [
			'id' => "L'identifiant de titre est absent ou incorrect.",
		];
	}

	public function apply(): array
	{
		$themes = $this->revue->getThemes();
		$result = [];
		foreach ($themes as $t) {
			$result[] = [
				'id' => (int) $t->id,
				'parentid' => (int) $t->parentId,
				'nom' => $t->categorie,
			];
		}
		return $result;
	}
}
