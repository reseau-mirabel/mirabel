<?php

namespace modules\api\models\actions;

use models\validators\IssnValidator;

/**
 * En cas de modification de la structure, tenir à jour la codumentation de l'API dans data/openapi.json.
 * Éventuellement, utiliser editor.swagger.io pour rédiger.
 */
class AccesRevue extends AccesTitre
{
	public $revueid;

	public $issn;

	public $worldcat;

	public $sudoc;

	private array $issnList = [];

	public function attributeNames()
	{
		return ['revueid', 'issn', 'partenaire', 'worldcat', 'sudoc'];
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['partenaire', 'validatePartenaire'],
			['worldcat', 'numerical', 'integerOnly' => true],
			['revueid', \ArrayOfIntValidator::class],
			['sudoc', 'safe'],
			['issn', 'match', 'pattern' => '/^[0-9Xx,-]+$/', 'message' => "Champ 'issn' non valide. Valeur attendue: liste d'ISSN (tiret et clé facultatifs) séparés par ',' sans espace."],
		];
	}

	public function afterValidate()
	{
		if (!$this->hasErrors() && !array_filter($this->attributes)) {
			$this->addError('revueid', "Au moins un critère doit être non-vide. Voir l'aide HTML ou les spécifications OpenAPI pour la liste des paramètres.");
		}
		if ($this->issn) {
			$v = new IssnValidator();
			$v->allowEmpty = false;
			$v->allowEmptyChecksum = true;
			$v->allowMissingCaret = true;
			$v->specialValues = [];
			$this->issnList = [];
			foreach (array_filter(explode(',', strtoupper($this->issn))) as $issn) {
				if (!$v->validateString($issn)) {
					$this->addError('issn', "Champ 'issn' non valide. Valeur attendue: liste d'ISSN (tiret et clé facultatifs) séparés par ',' sans espace.");
					return;
				}
				$this->issnList[] = $v->lastIssn;
			}
		}
		return parent::afterValidate();
	}

	public function apply(): array
	{
		$titreIds = $this->searchTitres();
		if ($titreIds) {
			return $this->searchServices($titreIds);
		}
		return [
			'code' => 404,
			'message' => "Aucun titre ne correspond.",
		];
	}

	/**
	 * @return int[] Array of titreId
	 */
	protected function searchTitres(): array
	{
		if (empty($this->revueid)) {
			$command = \Yii::app()->db->createCommand()
				->select('t.revueId')
				->from('Titre t')
				->leftJoin("Issn i", "i.titreId = t.id");

			if ($this->issnList) {
				$joinedIssns = "'" . join("','", $this->issnList) . "'";
				$command->andWhere("i.issn IN ($joinedIssns)");
				$revueIds = array_map(intval(...), $command->queryColumn());
			} else {
				if ($this->worldcat) {
					$command->andWhere('i.worldcatOcn = :worldcat', [':worldcat' => $this->worldcat]);
				}
				if ($this->sudoc) {
					$command->andWhere('i.sudocPpn = :sudoc', [':sudoc' => $this->sudoc]);
				}
				$revueIds = [(int) $command->queryScalar()];
			}
		} else {
			// Parse "8,a,12,-1,0," into [8,12]
			$revueIds = array_filter($this->revueid, fn ($x) => $x > 0);
		}

		if (!$revueIds) {
			return [];
		}
		$ids = join(',', $revueIds);
		return \Yii::app()->db->createCommand("SELECT id FROM Titre WHERE revueId IN ($ids)")->queryColumn();
	}
}
