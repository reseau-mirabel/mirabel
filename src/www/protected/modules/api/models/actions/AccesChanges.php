<?php

namespace modules\api\models\actions;

/**
 * En cas de modification de la structure, tenir à jour la codumentation de l'API dans data/openapi.json.
 * Éventuellement, utiliser editor.swagger.io pour rédiger.
 */
class AccesChanges extends AccesPossessions
{
	public $depuis;

	public function attributeNames()
	{
		return ['partenaire', 'depuis', 'possession', 'abonnement'];
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['partenaire, depuis', 'required'],
			['partenaire', 'validatePartenaire'],
			[['abonnement', 'possession'], 'boolean'],
			['depuis', 'numerical', 'integerOnly' => true, 'min' => 10000],
		];
	}

	public function beforeValidate()
	{
		// allow &depuis=2019-01-22+12:30:00
		if ($this->depuis !== null && !ctype_digit($this->depuis)) {
			$this->depuis = strtotime($this->depuis);
		}
		return parent::beforeValidate();
	}

	public function apply(): array
	{
		$this->depuis = (int) $this->depuis;
		return [
			'ajouts' => $this->searchAdditions(),
			'modifications' => $this->searchUpdates(),
			'suppressions' => $this->searchDeletions(),
		];
	}

	protected function searchAdditions()
	{
		$command = parent::search();
		$command->andWhere("s.hdateCreation > {$this->depuis}");
		return $command->queryAll();
	}

	protected function searchUpdates()
	{
		$command = parent::search();
		$command->andWhere("s.hdateCreation < {$this->depuis} AND s.hdateModif > {$this->depuis}");
		return $command->queryAll();
	}

	protected function searchDeletions()
	{
		$t = new Titres();
		$t->abonnement = $this->abonnement;
		$t->possession = $this->possession;
		$command = $t->search();
		$command->join("Intervention", "Intervention.titreId = t.id")
			->andWhere("Intervention.hdateVal > {$this->depuis}")
			->select("Intervention.id, Intervention.hdateVal, Intervention.contenuJson");
		$list = [];
		foreach ($command->queryAll() as $row) {
			$content = json_decode($row['contenuJson'], true)['content'];
			foreach ($content as $c) {
				if ($c['model'] === 'Service' && $c['operation'] === 'delete') {
					$list[] = (int) $c['before']['id'];
				}
			}
		}
		return array_unique($list);
	}
}
