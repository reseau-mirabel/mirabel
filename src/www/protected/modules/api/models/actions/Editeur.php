<?php

namespace modules\api\models\actions;

use models\validators\PpnValidator;
use modules\api\models\ActionInterface;

class Editeur extends \CModel implements ActionInterface
{
	public $id = 0;

	public $idref = '';

	public function attributeNames()
	{
		return ['id', 'idref'];
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['id', 'numerical', 'integerOnly' => true],
			['idref', 'safe'],
		];
	}

	public function afterValidate()
	{
		if (!$this->hasErrors()) {
			if ($this->id > 0) {
				$this->id = (int) $this->id;
				$this->idref = '';
			} elseif (!empty($this->idref)) {
				if (!PpnValidator::check($this->idref)) {
					$this->addError('idref', "Ceci n'est pas un IdRef (PPN) valable.");
				}
				$this->id = 0;
			} else {
				$this->addError('id', "Fournir un critère identifiant numérique ou un IdRef.");
			}
		}
		return parent::afterValidate();
	}

	public function apply(): array
	{
		$data = $this->search();
		if (!$data) {
			$cid = (int) \Yii::app()->db
				->createCommand("SELECT cibleId FROM RedirectionEditeur WHERE sourceId = :id")
				->queryScalar([':id' => (int) $this->id]);
			if ($cid) {
				return [
					'code' => 301,
					'nouvelid' => $cid,
					'url' => \Yii::app()->createAbsoluteUrl(sprintf('/api/editeurs/%d', $cid)),
				];
			}
			return [
				'code' => 404,
				'message' => "Aucun éditeur ne correspond.",
			];
		}
		return [
			'editeur' => self::formatEditeur($data),
			'relations_titres' => self::getRelations((int) $data['id']),
		];
	}

	public static function formatRelation(array $r): array
	{
		$relation = [
			'precedent' => (bool) $r['ancien'],
			//'direction_redaction' => (bool) $r['intellectuel'],
			//'publication_diffusion' => (bool) $r['intellectuel'],
		];
		if (!empty($r['role'])) {
			$relation['role'] = $r['role'];
		}
		if (isset($r['editeurId'])) {
			$relation['editeur_id'] = (int) $r['editeurId'];
			$relation['editeur'] = $r['nom'];
		}
		if (isset($r['titreId'])) {
			$relation['titre_id'] = (int) $r['titreId'];
			$relation['titre'] = $r['titre'];
		}
		ksort($relation);
		return $relation;
	}

	private function search(): array
	{
		$query = \Yii::app()->db->createCommand()
			->from("Editeur e")
			->leftJoin("Pays p", "e.paysId = p.id")
			->leftJoin("LienEditeur le", "le.editeurId = e.id")
			->group('e.id')
			->select(
				"e.id, e.idref, e.nom, e.prefixe, e.role, e.url, e.sigle"
				. ", p.code AS pays3"
				. ", GROUP_CONCAT(le.url ORDER BY le.url SEPARATOR ' | ') AS liens"
			)
			->limit(1);
		if ($this->id) {
			$query->where("e.id = {$this->id}");
		} else {
			$query->where("e.idref = :idref", [':idref' => $this->idref]);
		}
		return $query->queryRow() ?: [];
	}

	private static function formatEditeur(array $e): array
	{
		return array_filter([
			'id' => (int) $e['id'],
			'identifiants' => $e['idref'] ? [['idref', (string) $e['idref']]] : [],
			'liens' => array_filter(explode(' | ', $e['liens'] ?? '')),
			'nom' => (string) $e['nom'],
			'prefixe' => (string) $e['prefixe'],
			'role' => (string) $e['role'],
			'url' => (string) $e['url'],
			'url_mirabel' => \Yii::app()->createAbsoluteUrl(
				'/editeur/view',
				['id' => $e['id'], 'nom' => \Norm::urlParam($e['nom'] . ($e['sigle'] ? " — {$e['sigle']}" : ''))]
			),
			'pays' => (string) $e['pays3'],
		], function ($x) {
			return $x !== '';
		});
	}

	private function getRelations(int $editeurId): array
	{
		$relations = \Yii::app()->db
			->createCommand(<<<EOSQL
				SELECT te.titreId, te.ancien, te.commercial, te.intellectuel, te.role, t.titre
				FROM Titre_Editeur te
					JOIN Titre t ON te.titreId = t.id
				WHERE te.editeurId = {$editeurId}
				ORDER BY t.titre, te.titreId
				EOSQL
			)
			->query();
		$result = [];
		foreach ($relations as $r) {
			$result[] = self::formatRelation($r);
		}
		return $result;
	}
}
