<?php

namespace modules\api\models\actions;

use modules\api\models\ActionInterface;

class ThemesRevue implements ActionInterface
{
	public int $revueId = 0;

	private ?\Revue $revue;

	public function getAttributes()
	{
		return [
			'revueId' => $this->revueId,
		];
	}

	public function setAttributes($attributes)
	{
		if (isset($attributes['revueId'])) {
			$this->revueId = (int) $attributes['revueId'];
		}
	}

	public function validate()
	{
		if ($this->revueId > 0) {
			$this->revue = \Revue::model()->findByPk($this->revueId);
		}
		return ($this->revue instanceof \Revue);
	}

	public function getErrors()
	{
		if ($this->revue instanceof \Revue) {
			return [];
		}
		return [
			'id' => "L'identifiant de revue est absent ou incorrect.",
		];
	}

	public function apply(): array
	{
		$themes = $this->revue->getThemes();
		$result = [];
		foreach ($themes as $t) {
			$result[] = [
				'id' => (int) $t->id,
				'parentid' => (int) $t->parentId,
				'nom' => $t->categorie,
			];
		}
		return $result;
	}
}
