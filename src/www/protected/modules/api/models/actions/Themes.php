<?php

namespace modules\api\models\actions;

use modules\api\models\ActionInterface;

class Themes implements ActionInterface
{
	public function getAttributes()
	{
		return [];
	}

	public function setAttributes($attributes)
	{

	}

	public function validate()
	{
		return true;
	}

	public function getErrors()
	{
		return [];
	}

	public function apply(): array
	{
		$c = \Yii::app()->db->createCommand("SELECT id, parentId, chemin, categorie FROM Categorie WHERE role = 'public' ORDER BY chemin, categorie");
		$result = [];
		foreach ($c->query() as $row) {
			$result[] = [
				'id' => (int) $row['id'],
				'parentid' => (int) $row['parentId'],
				'nom' => $row['categorie'],
			];
		}
		return $result;
	}
}
