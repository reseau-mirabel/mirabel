<?php

namespace modules\api\models;

use CHttpException;
use CHttpRequest;
use Exception;
use Yii;

/**
 * Applies an instance of actions\* and outputs the result.
 */
class Api
{
	/**
	 * @var ActionInterface
	 */
	private $action;

	/**
	 * @var string
	 */
	private static $format = 'json';

	private static $formats = [
		'json' => 'Content-Type: application/json; charset="UTF-8"',
		'xml' => 'Content-Type: text/xml; charset="UTF-8"',
		'csv' => 'Content-Type: text/csv; charset="UTF-8"; header=present',
	];

	public function __construct(ActionInterface $action)
	{
		$this->action = $action;
	}

	/**
	 * @param string $format json, xml, csv
	 * @throws Exception
	 */
	public static function setFormat(string $format): void
	{
		if (!isset(self::$formats)) {
			throw new CHttpException(400, "Invalid format");
		}
		self::$format = $format;
	}

	/**
	 * @param int $code HTTP code
	 * @param string $url redirection Location if code=3xx
	 */
	public function header(int $code, string $url): void
	{
		if (headers_sent()) {
			throw new CHttpException(500, "Headers sent");
		}
		http_response_code($code);
		header(self::$formats[self::$format]);
		if ($code >= 300 && $code < 400 && $url !== '') {
			header("Location: $url");
		}
		header('Access-Control-Allow-Origin: *');
	}

	public function getResponse(): array
	{
		if (!$this->action->validate()) {
			$response = [
				'code' => 400,
				'message' => "Arguments non-valides",
				'fields' => join(", ", array_keys($this->action->getErrors())),
				'details' => $this->action->getErrors(),
			];
		} else {
			$data = $this->action->apply();
			$errors = $this->action->getErrors();
			if ($errors) {
				$response = [
					'code' => 500,
					'message' => "Erreur de traitement",
					'fields' => join(", ", array_keys($errors)),
					'details' => $errors,
				];
			} else {
				$response = $data;
			}
		}
		return $response;
	}

	/**
	 * Prints the full response, HTTP header included.
	 */
	public function printResponse(): void
	{
		try {
			$response = $this->getResponse();
			$this->header($response['code'] ?? 200, $response['url'] ?? '');
			echo $this->encode($response);
		} catch (CHttpException $e) {
			if (!headers_sent()) {
				http_response_code($e->statusCode);
			}
			echo $this->encode(
				[
					'code' => (int) $e->statusCode,
					'message' => $e->getMessage(),
				]
			);
		} catch (Exception $e) {
			echo $this->encode(
				[
					'code' => 500,
					'message' => "Erreur de traitement : " . $e->getMessage(),
				]
			);
		}
	}

	/**
	 * A simplified process when the action does not have any parameter.
	 */
	public static function processDefault(ActionInterface $engine): void
	{
		$parameters = self::readParameters(Yii::app()->request);
		$engine->setAttributes($parameters);
		$api = new self($engine);
		$api->printResponse();
	}

	/**
	 * Read the request parameter from the body, the URL query=JSON, and the URL parameters.
	 *
	 * Side-effects: sets the output format (JSON/XML).
	 *
	 * @return mixed Body JSON parameters, or ?query=JSON, or URL .../k/v, or GET parameters
	 */
	public static function readParameters(CHttpRequest $request)
	{
		$format = $request->getQuery('format');
		if ($format) {
			self::setFormat($format);
			unset($_GET['format']);
		}

		$data = [];
		try {
			$restParams = $request->getRestParams();
			if ($restParams) {
				$data[] = $restParams;
			}
		} catch (\Throwable $e) {
			\Yii::log("request->getRestParams() failed: {$e->getMessage()}", 'warning');
		}
		$rawQuery = $request->getParam('query', null);
		if ($rawQuery !== null) {
			$data[] = json_decode($rawQuery);
		}
		$data[] = (empty($_GET) ? $_POST : $_GET);
		// merge these sources, with precedence at the end.
		return call_user_func_array('array_merge', $data);
	}

	/**
	 *
	 * @param mixed $response
	 * @throws Exception
	 * @return string
	 */
	private function encode($response): string
	{
		$jsonOptions = JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_THROW_ON_ERROR;
		if (YII_DEBUG) {
			$jsonOptions |= JSON_PRETTY_PRINT;
		}
		if (self::$format === 'json') {
			return json_encode($response, $jsonOptions);
		}
		if (self::$format === 'xml') {
			$serializer = \JMS\Serializer\SerializerBuilder::create()->build();
			return $serializer->serialize(json_decode(json_encode($response), false), 'xml');
		}
		throw new Exception("I don't know how to encode into this format.");
	}
}
