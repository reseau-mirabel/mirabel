<?php

namespace modules\api\models;

use Yii;

/**
 * Format a Titre record into a structure that the API will publish as JSON.
 *
 * The formatter can load data about ISSNs and attributes.
 * This is a separate step, to avoid querying the DB for each Titre.
 */
class TitreFormater
{
	private array $issns = [];

	private array $labels = [];

	public function format($attributes): array
	{
		$result = [];
		if (is_array($attributes)) {
			$attributes = (object) $attributes;
		}

		$integers = [
			// new-name => DB-name
			'id' => 'id',
			'revueid' => 'revueId',
			'obsoletepar' => 'obsoletePar',
		];
		foreach ($integers as $new => $old) {
			if (isset($attributes->{$old})) {
				$result[$new] = (int) $attributes->{$old};
			} else {
				$result[$new] = null;
			}
		}

		$strings = [
			'titre' => 'titre',
			'prefixe' => 'prefixe',
			'sigle' => 'sigle',
			'datedebut' => 'dateDebut',
			'datefin' => 'dateFin',
			'url' => 'url',
			'periodicite' => 'periodicite',
			'identifiantpartenaire' => 'identifiantLocal',
			'bouquetpartenaire' => 'bouquet',
		];
		foreach ($strings as $new => $old) {
			if (!empty($attributes->{$old})) {
				$result[$new] = $attributes->{$old};
			} else {
				$result[$new] = null;
			}
		}
		$result['editeurs'] = $attributes->editeurs ? explode("\t", $attributes->editeurs) : [];

		if (!empty($attributes->langues)) {
			$result['langues'] = json_decode($attributes->langues);
		} else {
			$result['langues'] = [];
		}
		$result['liens'] = (
			empty($attributes->liensJson)
				? []
				: array_map(
					function ($l) {
						return substr($l['url'], 0, 1) === '/' ? Yii::app()->params->itemAt('baseUrl') . $l['url'] : $l['url'];
					},
					json_decode($attributes->liensJson, true, 512, \JSON_OBJECT_AS_ARRAY) ?? []
				)
		);

		$result['url_revue_mirabel'] = Yii::app()->controller->createAbsoluteUrl('/revue/view', ['id' => (int) $attributes->revueId]);

		if (isset($this->issns[$result['id']])) {
			$result['issns'] = $this->issns[$result['id']];
		}
		if (isset($this->labels[$result['id']])) {
			$result['labellisation'] = $this->labels[$result['id']];
		}

		return $result;
	}

	public function formatAll(iterable $all): array
	{
		$result = [];
		foreach ($all as $r) {
			$result[] = $this->format($r);
		}
		return $result;
	}

	/**
	 * @param array|\CDbCommand $titres
	 */
	public function loadIssns($titres): void
	{
		$this->issns = [];
		if (is_array($titres)) {
			if (!$titres) {
				return;
			}
			$ids = array_map(function ($t) {
				return (int) $t['id'];
			}, $titres);
			$joinedIds = join(',', $ids);
			$params = [];
		} else {
			assert($titres instanceof \CDbCommand);
			$titres->setText("");
			$titres->select("t.id");
			$titres->order = "";
			$joinedIds = $titres->getText();
			$params = $titres->params;
		}
		$sql = <<<EOSQL
			SELECT
			    titreId
			    , bnfArk
			    , issn
			    , issnl
			    , CASE statut WHEN 0 THEN 'valide' WHEN 2 then 'en cours' WHEN 4 THEN 'sans' END AS statut
			    , sudocNoHolding
			    , sudocPpn
			    , support
			    , worldcatOcn
			FROM Issn
			WHERE titreId IN ($joinedIds) AND statut IN (0, 2, 4)
			ORDER BY titreId ASC, id ASC
			EOSQL;
		$issns = Yii::app()->db
			->createCommand($sql)
			->queryAll(true, $params);
		foreach ($issns as $issn) {
			$tid = (int) $issn['titreId'];
			if (isset($this->issns[$tid])) {
				$this->issns[$tid][] = self::formatIssn($issn);
			} else {
				$this->issns[$tid] = [self::formatIssn($issn)];
			}
		}

		$this->loadLabels($joinedIds, $params);
	}

	private static function formatIssn($issn): \stdClass
	{
		$i = (object) $issn;
		return (object) array_filter(
			[
				"bnfark" => $i->bnfArk ?: null,
				"issn" => $i->issn ?: null,
				"issnl" => $i->issnl ?: null,
				"statut" => $i->statut,
				"sudocnoholding" => $i->sudocPpn ? (bool) $i->sudocNoHolding : null,
				"sudocppn" => $i->sudocPpn ?: null,
				"support" => $i->support,
				"worldcatocn" => $i->worldcatOcn ? (int) $i->worldcatOcn : null,
			],
			function ($x) {
				return $x !== null;
			}
		);
	}

	private function loadLabels(string $inQuery, array $params): void
	{
		$this->labels = [];
		$attributes = Yii::app()->db
			->createCommand("SELECT identifiant, id FROM Sourceattribut WHERE identifiant IN ('doaj-sceau', 'latindex-sceau')")
			->queryAll();
		$ids = [];
		/**
		 * @see \processes\attribut\DisplayedAttributes
		 */
		$when = "";
		foreach ($attributes as $a) {
			$ids[] = (int) $a['id'];
			if ($a['identifiant'] === 'doaj-sceau') {
				$when .= " WHEN {$a['id']} THEN IF(valeur = 'Yes', 'DOAJ seal', NULL) ";
			} elseif ($a['identifiant'] === 'latindex-sceau') {
				$when .= " WHEN {$a['id']} THEN IF(valeur = '1', 'Catálogo 2.0 Latindex', NULL) ";
			}
		}
		if (!$when) {
			return;
		}
		$case = "CASE sourceattributId $when END";
		$joinedIds = join(',', $ids);
		$sql = <<<EOSQL
			SELECT titreId, GROUP_CONCAT($case SEPARATOR ', ') AS attributs
			FROM AttributTitre
			WHERE sourceattributId IN ($joinedIds) AND titreId IN ($inQuery)
			GROUP BY titreId
			EOSQL;
		foreach (Yii::app()->db->createCommand($sql)->queryAll(false, $params) as $row) {
			$this->labels[$row[0]] = explode(', ', $row[1]);
		}
	}
}
