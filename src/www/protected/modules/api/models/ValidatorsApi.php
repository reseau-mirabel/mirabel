<?php

namespace modules\api\models;

use models\validators\IssnValidator;

trait ValidatorsApi
{
	public function validateIssns(string $name): void
	{
		if (!isset($this->{$name})) {
			return;
		}
		$value = $this->{$name};
		if (is_array($value)) {
			$this->{$name} = array_filter($value, function ($x) {
				return preg_match('/^\d{4}-\d{3}[\dX]$/', $x);
			});
		} elseif (preg_match('/^[\dX,-]+$/', $value)) {
			$list = array_filter(explode(',', $value));
			if (count($list) === 0) {
				$this->{$name} = null;
			} elseif (count($list) === 1) {
				$v = new IssnValidator();
				$v->allowEmpty = false;
				$v->allowEmptyChecksum = true;
				$v->allowMissingCaret = true;
				$v->specialValues = [];
				if ($v->validateString($list[0])) {
					$this->{$name} = $v->lastIssn;
				} else {
					$this->addError($name, "Cet ISSN n'est pas valide.");
				}
			} else {
				$this->{$name} = $list;
			}
		} else {
			$this->addError($name, "Not a ISSN, nor a sequence of ISSN joined by commas");
		}
	}

	public function validateSequence(string $name): void
	{
		if (isset($this->{$name})) {
			$value = $this->{$name};
			if (is_array($value)) {
				$this->{$name} = array_map('intval', $value);
			} elseif (ctype_digit($value)) {
				$this->{$name} = (int) $value;
			} elseif (preg_match('/^[\d,]+$/', $value)) {
				$this->{$name} = array_map('intval', array_filter(explode(',', $value)));
			} else {
				$this->addError($name, "Not a number, nor a sequence of numbers joined by commas");
			}
		}
	}
}
