<?php

class ApiModule extends CWebModule
{
	/**
	 * This method is called when the module is being created.
	 */
	public function init()
	{
		parent::init();
		// use an distinct MySQL account if it exists
		$dbconfig = Yii::app()->params->itemAt('api.db');
		if ($dbconfig) {
			// merge the API specific config onto the default one
			Yii::app()->setComponent('db', $dbconfig, true);
			if (Yii::app()->db->isInitialized) {
				Yii::app()->setComponent('db', null);
			}
		}
	}

	/**
	 * This method is called after the controller action is performed.
	 *
	 * @param CController $controller
	 * @param CAction $action
	 * @return void
	 */
	public function afterControllerAction($controller, $action)
	{
		// write an event in Matomo/Matomo, except for the HTML main page
		if ($controller->id !== 'default' && $action->id !== 'index') {
			$partenaire = isset($_GET['partenaire']) ?
				Partenaire::model()->findByAttributes(['hash' => $_GET['partenaire']])
				: null;
			$matomo = Yii::app()->getComponent('matomo');
			assert($matomo instanceof \components\Matomo);
			$matomo->setWebUserName($partenaire ? $partenaire->getShortName() : '')
				->sendFromServer();
		}

		parent::afterControllerAction($controller, $action);
	}

	/**
	 * This method is called before any module controller action is performed.
	 *
	 * @param CController $controller
	 * @param CAction $action
	 * @return bool
	 */
	public function beforeControllerAction($controller, $action)
	{
		if ($controller->id !== 'default' && $action->id !== 'index' && !headers_sent()) {
			// Allow JS calls from external sites, except for the HTML main page
			header("Access-Control-Allow-Origin: *");
			// Do not compress the response, which would wait for the process end
			header("Content-Encoding: none");
		}
		return parent::beforeControllerAction($controller, $action);
	}
}
