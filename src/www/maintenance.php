<?php

/** @var ?string $message */

http_response_code(503);
header('Content-Type: text/html; charset=utf-8');
?>
<!doctype html>
<html lang="fr">
	<head>
		<title>Mir@bel en maintenance</title>
	</head>
	<body>
		<h1>Mir@bel est indisponible</h1>
		<p><?= isset($message) ? $message : "Mir@bel est temporairement indisponible." ?></p>
		<div>
			<img src="/images/maintenance.png" alt="Mir@bel est indisponible" />
		</div>
	</body>
</html>
<?php
exit();
