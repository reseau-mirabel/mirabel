<?php

// $message = "Mir@bel est en cours de migration vers une nouvelle version.";
// $message = "Mir@bel est en maintenance.";
// $message .= " Retour prévu vers 18h.";
// require __DIR__ . '/maintenance.php';

$localConfig = require __DIR__ . '/protected/config/local.php';
if (empty($localConfig)) {
	header("Content-Type: text/plain");
	echo "Please configure this application.";
	exit(1);
}
defined('YII_ENV') or define('YII_ENV', APP_PRODUCTION_MODE ? 'prod' : 'dev');
$mainConfig = require __DIR__ . '/protected/config/main.php';
include __DIR__ . '/protected/config/debug.php';

define('VENDOR_PATH', dirname(__DIR__, 2) . '/vendor');
require_once VENDOR_PATH . '/autoload.php';
spl_autoload_register(array('YiiBase','autoload'));
require_once __DIR__ . '/protected/components/Yii.php';

$config = CMap::mergeArray($mainConfig, $localConfig);
unset($mainConfig, $localConfig);

if (YII_DEBUG) {
	$app = Yii::createWebApplication($config)->run();
} else {
	try {
		Yii::createWebApplication($config)->run();
	} catch (CDbException $e) {
		$log = sprintf("%d:%d %s\n%s\n------------\n", $e->getFile(), $e->getLine(), $e->getMessage(), $e->getTraceAsString());
		Yii::log($log, CLogger::LEVEL_ERROR);
		$message = "Mir@bel est temporairement indisponible.
			Veuillez nous excuser pour le désagrément.";
		require __DIR__ . '/maintenance.php';
	}
}
