let state = {
	editeurIds: [],
	editeurNames: [],
	roles: [],
}

window.PolitiqueSelectPublishers = {
	init(htmlId, data) {
		// find the root HTML element
		const root = document.getElementById(htmlId)
		if (!root) {
			alert("JS bug, root element #" + htmlId + " was not found.")
			return;
		}

		state = data
		m.mount(root, Main)
	}
}

const STATUS_EMPTY = 1
const STATUS_HIDDEN = 2
const STATUS_FILLED = 3

function addStateStatuses() {
	state.statuses = []
	let firstVisible = true
	for(let i = 0; i < 3; i++) {
		if (state.editeurIds[i] > 0) {
			state.statuses[i] = STATUS_FILLED
		} else if (firstVisible) {
			state.statuses[i] = STATUS_EMPTY
			firstVisible = false
		} else {
			state.statuses[i] = STATUS_HIDDEN
		}
	}
}

const Main = {
	view() {
		addStateStatuses()
		return m('div',
			m(Selector, this.getSelectorData(state, 0)),
			m(Selector, this.getSelectorData(state, 1)),
			m(Selector, this.getSelectorData(state, 2)),
		)
	},
	getSelectorData(data, position) {
		return {
			key: position,
			editeurId: data.editeurIds[position] || 0,
			editeurName: data.editeurNames[position] || "",
			userRole: data.roles[position] || "",
			status: data.statuses[position],
		}
	},
}

function Selector(initialVnode) {
	const localstate = {
		confirmed: (initialVnode.key === 0),
		loading: false,
		suggestions: [],
		hasOwner: false,
	}
	function onNameInput(oldName, position) {
		return (event) => {
			const newName = event.target.value
			if (newName === oldName) {
				return true;
			}
			state.editeurIds[position] = 0
			state.editeurNames[position] = newName
			state.roles[position] = ""
			if (newName.length < 3) {
				localstate.suggestions = []
				return true;
			}
			const publisherId = matchSuggestions(newName)
			if (publisherId > 0) {
				localstate.suggestions = []
				state.editeurIds[position] = publisherId
				localstate.hasOwner = false
				loadRole(publisherId).then((hasOwner) => {
					state.roles[position] = (hasOwner ? 'guest' : 'owner')
				})
			} else {
				suggest(newName)
			}
			return false
		}
	}
	function matchSuggestions(term) {
		if (localstate.suggestions.length === 0) {
			return 0;
		}
		for (let x of localstate.suggestions) {
			if (term === x.label) {
				return x.id
			}
		}
		return 0
	}
	function suggest(term) {
		localstate.loading = true
		return m.request({
			method: "GET",
			url: '/editeur/ajax-complete',
			params: {
				term,
				// do not suggest selected publishers
				//excludeIds: state.editeurIds.filter((x) => x > 0).join(','),
			},
			withCredentials: true,
		})
		.then((result) => {
			localstate.suggestions = result
		})
		.finally(() => {
			localstate.loading = false
		})
	}
	function loadRole(publisherId) {
		localstate.loading = true
		return m.request({
			method: "GET",
			url: '/editeur/ajax-has-policy-owner',
			params: { id: publisherId },
			withCredentials: true,
		})
		.then((result) => {
			localstate.hasOwner = result
			return localstate.hasOwner
		})
		.finally(() => {
			localstate.loading = false
		})
	}
	return {
		view(vnode) {
			const {key, editeurId, editeurName, userRole, status} = vnode.attrs
			if (key === 0) {
				localstate.confirmed = true
			}
			if (status === STATUS_HIDDEN) {
				return null
			}
			if (status === STATUS_EMPTY && !localstate.confirmed) {
				return m('button',
					{
						onclick() { localstate.confirmed = true },
						type: "button",
					},
					"Demander l'accès pour le compte d'un autre éditeur"
				)
			}
			return m('div.control-group.editeur',
				m('label.control-label', "Éditeur"),
				m('div.controls.controls-row',
					m('div.span8',
						m('input.editeur-id', {
							name: `UserPolitique[editeurIds][${key}]`,
							value: editeurId,
							type: 'hidden',
						}),
						m('input.editeur-name.span11', {
							value: editeurName,
							type: 'text',
							autocomplete: 'off',
							list: `publishers-list-${key}`,
							oninput: onNameInput(editeurName, key),
						}),
						m(DataList, {
							id: `publishers-list-${key}`,
							options: localstate.suggestions,
						}),
						editeurId > 0 ? m('span.span1.glyphicon.glyphicon-ok') : '',
						localstate.loading ? m('span.glyphicon.glyphicon-glass') : '',
					),
					m(Role, {
						editeurId,
						hasOwner: localstate.hasOwner,
						role: userRole,
					}),
				),
			)
		},
	}
}

const DataList = {
	view(vnode) {
		const { id, options } = vnode.attrs
		return m('datalist',
			{id},
			options.map((o) => m('option', {value: o.label}))
		)
	}
}

const Role = {
	onupdate(vnode) {
		const { editeurId, hasOwner } = vnode.attrs
		$(vnode.dom).find(`a[rel="popover"]`).popover('destroy')
		let msg = "Préciser votre rôle pour cet éditeur : soit responsable officiel, soit agissant par délégation."
		if (hasOwner) {
			msg += `<br /><strong>(!)</strong> attention, un responsable de la politique est déjà présent pour cet éditeur.`
		}
		$(vnode.dom).find(`a[rel="popover"]`).popover({
			animation: false,
			html: true,
			placement: 'left',
			trigger: 'hover',
			title: "Rôle pour cet éditeur",
			content: msg,
			container: 'body',
		})
	},
	view(vnode) {
		const { editeurId, hasOwner, role } = vnode.attrs
		const hidden = (editeurId === 0 ? '.hidden' : '')
		const ownerText = "responsable" + (hasOwner ? " (!)" : "")
		return m('div.span4' + hidden, {style: "white-space: pre"},
			m('select', { name: `UserPolitique[roles][${editeurId}]` },
				m('option', {value: "owner", selected: (role === 'owner')}, ownerText),
				m('option', {value: "guest", selected: (role === 'guest')}, "délégué"),
			),
			m('a', {href: "#", rel: "popover"}, "?"),
		)
	}
}
