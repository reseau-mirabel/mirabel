import Policy from './policy.js'

const initialStep = {id: 'Model'}

// Assign a title to a step, for navigating the history.
const stepTitles = new Map([
	['Model', "modèle de publication"],

	['DroitsAuteurs', "droits d'auteurs"],
	['LicenceCC', "licence CC ?"],
	['LicenceSelection', "choix de la licence"],
	['Depot.published', "dépôt / version publiée"],
	['Depot.accepted', "dépôt / version acceptée"],
	['Urls', "URL de la politique"],

	['EmbargoDroitsAuteurs', "droits d'auteurs"],
	['EmbargoDepot', "dépôt"],
	['EmbargoAcceptedDuration', "embargo / manuscript accepté"],
	['EmbargoPublishedDuration', "embargo"],
	['EmbargoLrn', "embargo LRN"],

	['Done', "Confirmation"],
])

const emptyPolicy = {
	policy: {
		permitted_oa: []
	},
	defaultOa: {}
}

/**
 * The API to the global state of the assistant.
 *
 * @returns {State.self}
 */
const State = function() {
	let state = {
		editeurId: 0,
		titreId: 0,
		model: null,
		past: [], // [{step, policy, title}]
		position: -1,
		returnUrl: "",
	}
	let currentPolicy;

	function pushToBrowserHistory(title) {
		if (typeof global === 'undefined' || !('specify' in global)) {
			// probably not running in a test env
			if (state.model === null) {
				window.history.replaceState(state, title)
			} else {
				window.history.pushState(state, title)
			}
		}
	}

	const self = {
		set model(m) {
			state.model = m
		},

		get policy() {
			return currentPolicy
		},

		get returnUrl() {
			return state.returnUrl
		},

		get titreId() {
			return state.titreId
		},

		get step() {
			return state.past[state.position].step
		},
		set step(st) {
			// add this step to `past` with a copy of the current policy
			if (state.past.length > 1 + state.position) {
				state.past.length = 1 + state.position
			}
			state.past.push({
				step: st,
				policy: JSON.stringify(currentPolicy.export()),
				title: stepTitles.has(st.id) ? stepTitles.get(st.id) : st.id,
			})
			state.position++
			pushToBrowserHistory(st.id)
		},

		getHistory() {
			return state
		},
		setStep(id, params) {
			if (typeof params === 'undefined') {
				self.step = {id}
			} else {
				self.step = {id, params}
			}
		},
		init(editeurId, titreId, url) {
			self.reset()
			state.editeurId = editeurId
			state.titreId = titreId
			state.returnUrl = url
			pushToBrowserHistory("Mir@bel - assistant / éditeur")
		},
		goto(p) {
			state.position = p
			currentPolicy = new Policy()
			currentPolicy.load(JSON.parse(state.past[state.position].policy))
		},
		load(savedState) {
			if (savedState === null || !savedState.hasOwnProperty('model')) {
				console.error("Cannot load an empty state")
				return;
			}
			state = savedState
			currentPolicy = new Policy()
			currentPolicy.load(JSON.parse(state.past[state.position].policy))
		},
		reset() {
			state = {
				model: null,
				past: [{
					step: initialStep,
					policy: JSON.stringify(emptyPolicy),
					title: stepTitles.get(initialStep.id),
				}],
				position: 0,
			}
			currentPolicy = new Policy()
		},
		export() {
			return {
				editeurId: state.editeurId,
				titreId: state.titreId,
				model: state.model,
				publisher_policy: state.past[state.position].policy,
			}
		},
	}
	return self
}
Object.freeze(State)

export default (new State())
