import state from "./state.js"
import {loadTranslations} from "../common/translate.js"
import Assistant from "./components/assistant.js"

window.addEventListener('popstate', function(event) {
	if (event.state !== null) {
		state.load(event.state)
	}
	m.redraw()
})

window.PolitiqueAssistant = {
	init(htmlId, data) {
		// find the root HTML element
		const root = document.getElementById(htmlId)
		if (!root) {
			alert("JS bug, root element #" + htmlId + " was not found.")
			return;
		}

		// load the translations and item lists
		loadTranslations(data.translations)

		state.init(data.editeurId, data.titreId, data.returnUrl || '/politique')

		m.mount(root, Assistant)
	}
}
