import state from "./state.js"

let copyrightOwners = '';

const Transitions = {
	droitsauteurs: {
		onyes() {
			copyrightOwners = 'journal'
			state.policy.updateDefaultOa({
				copyright_owner: 'journal',
			});
			state.setStep("LicenceCC", 'journal');
		},
		onno() {
			copyrightOwners = 'authors'
			state.policy.updateDefaultOa({
				copyright_owner: 'authors',
			});
			state.setStep("LicenceCC", 'authors');
		}
	},
	licenceCC: {
		onyes() {
			state.setStep("LicenceSelection");
		},
		onno() {
			if (copyrightOwners === 'journal') {
				state.setStep("DepotJournal");
			} else {
				state.setStep("DepotAuthors");
			}
		}
	},
	licenceSelection(choice) {
		// Une précédente étape a fixé le "copyright_owner" par défaut,
		if (choice === 'CC BY') {
			setLicenseCcby(copyrightOwners, "");
		} else if (choice === 'CC BY-ND') {
			setLicenseCcby(copyrightOwners, "_nd");
		} else if (choice === 'CC BY-SA') {
			setLicenseCcby(copyrightOwners, "_sa");
		} else if (choice === 'CC BY-NC') {
			setLicenseCcbync(copyrightOwners, "");
		} else if (choice === 'CC BY-NC-SA') {
			setLicenseCcbync(copyrightOwners, "_sa");
		} else if (choice === 'CC BY-NC-ND') {
			setLicenseCcbync(copyrightOwners, "_nd");
		} else {
			console.error("choice:", choice);
			alert("erreur, valeur inconnue");
		}
		state.setStep("Urls");
	},
	depot: {
		authors: {
			onyes() {
				state.policy.addOa({
					article_version: [
						"published",
					],
					location: {
						location: [
							"this_journal",
							"non_commercial_website",
							"non_commercial_repository",
						],
					},
					conditions: [
						"Published source must be acknowledged with citation",
					],
					public_notes: [
						"Authors retain all the rights ; no license fixed"
					],
				});
				state.policy.addOa({
					article_version: [
						"accepted",
						"submitted"
					],
					location: {
						location: [
							"any_website",
						],
					},
					conditions: [
						"Published source must be acknowledged with citation",
					],
					public_notes: [
						"Authors retain all the rights ; no license fixed"
					],
				});
				state.setStep("Urls");
			},
			onno() {
				state.policy.addOa({
					article_version: [
						"accepted",
						"submitted"
					],
					location: {
						location: [
							"any_website",
						],
					},
					conditions: [
						"Published source must be acknowledged with citation",
					],
					public_notes: [
						"Authors retain all the rights ; no license fixed"
					]
				});
				state.setStep("Urls");
			},
		},
		journal(published, accepted) {
			if (published) {
				state.policy.addOa({
					article_version: [
						"published",
					],
					location: {
						location: [
							"this_journal",
							"authors_homepage",
							"institutional_repository",
							"non_commercial_repository",
							"named_repository"
						],
						named_repository: [
							"HAL"
						],
					},
					conditions: [
						"Published source must be acknowledged with citation"
					]
				});
			} else {
				state.policy.addOa({
					article_version: [
						"published",
					],
					location: {
						location: [
							"this_journal",
						],
						named_repository: [
							"HAL"
						],
					},
					prerequisites: {
						prerequisites: [
							"requires_publisher_permission"
						]
					}
				});
			}

			if (accepted) {
				state.policy.addOa({
						article_version: [
							"accepted"
						],
						location: {
							location: [
								"authors_homepage",
								"institutional_repository",
								"non_commercial_repository",
								"named_repository"
							],
							named_repository: [
								"HAL"
							],
						},
						conditions: [
							"Published source must be acknowledged with citation"
						]
				});
				state.policy.addOa({
					article_version: [
						"submitted"
					],
					location: {
						location: [
							"authors_homepage",
							"preprint_repository"
						],
					},
					conditions: [
						"Published source must be acknowledged with citation"
					]
				});
			}

			state.setStep("Urls")
		},
	},
	diffusionCommerciale: {
		onyes() {
			state.policy.updateDefaultOa({
				conditions: [
					"With the express agreement of authors",
					"Under the conditions set by the authors"
				],
				public_notes: [
					"Authors retain all the rights ; no license fixed"
				]
			});
			state.policy.addOa({
				article_version: [
					"published",
				],
				location: {
					location: [
						"this_journal",
						"non_commercial_website",
						"non_commercial_repository",
					],
				},
			});
			state.policy.addOa({
				article_version: [
					"accepted",
					"submitted"
				],
				location: {
					location: [
						"authors_homepage",
						"non_commercial_website",
						"non_commercial_repository",
					],
				},
			});
			state.setStep("Urls");
		},
		onno() {
			state.policy.addOa({
				article_version: [
					"published",
					"accepted",
					"submitted"
				],
				location: {
					location: [
						"any_website",
					],
				},
				conditions: [
					"With the express agreement of authors",
					"Under the conditions set by the authors"
				],
				public_notes: [
					"Authors retain all the rights ; no license fixed"
				]
			});
			state.setStep("Urls");
		}
	},
};
Object.freeze(Transitions)


function setLicenseCcby(copyrightOwner, suffix) {
	state.policy.updateDefaultOa({
		license: [{
			license: "cc_by" + suffix,
			version: "4.0"
		}],
		conditions: [
			"Published source must be acknowledged with citation"
		]
	});
	if (copyrightOwner === 'journal') {
		state.policy.addOa({
			article_version: ["published"],
			location: {
				location: [
					"any_website",
					"this_journal",
					"named_repository"
				],
				named_repository: [
					"HAL"
				],
			},
		});
		state.policy.addOa({
			article_version: ["accepted"],
			location: {
				location: [
					"any_website",
					"any_repository",
					"named_repository"
				],
				named_repository: [
					"HAL"
				],
			},
		});
		state.policy.addOa({
			article_version: ["submitted"],
			location: {
				location: [
					"any_website",
					"preprint_repository"
				],
			},
		});
	} else if (copyrightOwner === 'authors') {
		state.policy.addOa({
			article_version: ["published"],
			location: {
				location: [
					"this_journal",
					"any_website",
				],
			},
		});
		state.policy.addOa({
			article_version: ["accepted", "submitted"],
			location: {
				location: [
					"any_website",
				],
			},
		});
	} else {
		console.log("Erreur, copyright_owner inconnu:", copyrightOwner);
	}
}

function setLicenseCcbync(copyrightOwner, suffix) {
	state.policy.updateDefaultOa({
		license: [{
			license: "cc_by_nc" + suffix,
			version: "4.0"
		}],
		conditions: [
			"Published source must be acknowledged with citation"
		]
	});
	if (copyrightOwner === 'journal') {
		state.policy.addOa({
			article_version: ["published"],
			location: {
				location: [
					"this_journal",
					"authors_homepage",
					"non_commercial_website",
					"non_commercial_repository",
					"named_repository",
				],
				named_repository: [
					"HAL"
				],
			},
		});
		state.policy.addOa({
			article_version: ["accepted"],
			location: {
				location: [
					"authors_homepage",
					"non_commercial_website",
					"non_commercial_repository",
					"named_repository",
				],
				named_repository: [
					"HAL"
				],
			},
		});
		state.policy.addOa({
			article_version: ["submitted"],
			location: {
				location: [
					"authors_homepage",
					"preprint_repository"
				],
			},
		});
	} else if (copyrightOwner === 'authors') {
		state.policy.addOa({
			article_version: [
				"published",
			],
			location: {
				location: [
					"this_journal",
					"non_commercial_website",
					"non_commercial_repository",
				],
			},
		});
		state.policy.addOa({
			article_version: [
				"accepted",
				"submitted"
			],
			location: {
				location: [
					"authors_homepage",
					"non_commercial_website",
					"non_commercial_repository",
				],
			},
		});
	} else {
		console.log("Erreur, copyright_owner inconnu:", copyrightOwner);
	}
}

export default Transitions
