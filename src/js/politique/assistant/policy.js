/**
 * Constructor of Policy objects.
 *
 * All the operations go through this API,
 * then the data is stored in the global state with a call to export()
 *
 * @returns {Policy.self}
 */
function Policy() {
	let policy = {
		permitted_oa: []
	}
	let defaultOa = {}
	const self = {
		get defaultOa() {
			return Object.assign({}, defaultOa)
		},
		get() {
			return policy
		},
		load(backup) {
			policy = backup.policy
			defaultOa = backup.defaultOa
		},
		export() {
			return {policy, defaultOa}
		},
		update(kv) {
			Object.assign(policy, kv)
		},
		addOa(oa) {
			const newOa = Object.assign({}, defaultOa, oa)
			policy.permitted_oa.push(newOa)
		},
		updateOa(kv) {
			// find matching article_version
			const pattern = JSON.stringify(kv.article_version)
			for (let i = 0; i < policy.permitted_oa.length; i++) {
				if (pattern === JSON.stringify(policy.permitted_oa[i])) {
					Object.assign(policy.permitted_oa[i], kv)
					return;
				}
			}
			console.log("no matching article_version was found, so adding the rule:", kv)
			const newOa = Object.assign({}, defaultOa, kv)
			policy.permitted_oa.push(newOa)
		},
		updateDefaultOa(kv) {
			Object.assign(defaultOa, kv)
			for (let k in defaultOa) {
				if (defaultOa[k] === null) {
					delete(defaultOa[k])
				}
			}
		},
	}
	return self
}
Object.freeze(Policy)

export default Policy
