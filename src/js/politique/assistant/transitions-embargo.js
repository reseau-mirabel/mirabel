import state from "./state.js"

// states kept from one step to a later one
let owner = "";
let published;

function setPublished(choice) {
	if (choice === "après embargo") {
		published = true;
		state.policy.addOa({
			article_version: ['published'],
			location: {
				location: [
					"this_journal",
					"authors_homepage",
					"institutional_repository",
					"non_commercial_repository",
					"named_repository"
				],
				named_repository: [
					"HAL"
				],
			},
			conditions: [
				"Published source must be acknowledged with citation"
			]
		})
	} else if (choice === "jamais") {
		published = false;
		state.policy.addOa({
			article_version: ['published'],
			location: {
				location: [
					"this_journal",
				],
			},
			prerequisites: {
				prerequisites: [
					"requires_publisher_permission"
				]
			}
		})
	} else if (choice === "sans embargo") {
		published = false;
		state.policy.addOa({
			article_version: ['published'],
			location: {
				location: [
					"this_journal",
				],
			},
			conditions: [
				"Published source must be acknowledged with citation"
			]
		})
		state.policy.addOa({
			article_version: ['published'],
			location: {
				location: [
					"authors_homepage",
					"institutional_repository",
					"non_commercial_repository",
					"named_repository"
				],
				named_repository: [
					"HAL"
				],
			},
			conditions: [
				"Published source must be acknowledged with citation"
			]
		})
		delete(state.policy.get().permitted_oa[1].embargo) // ugly hack
	} else {
		console.error("choice:", choice)
		alert("erreur, valeur inconnue")
	}
}

function setAccepted(choice) {
	if (choice === "jamais") {
		state.setStep("Urls")
	} else if (choice === "après embargo") {
		if (published) {
			state.policy.addOa({
				article_version: ['accepted'],
				embargo: Object.assign({}, state.policy.get().permitted_oa[0].embargo),
				location: {	location: ['any_website'] },
			})
		} else {
			state.policy.addOa({
				article_version: ['accepted'],
				embargo: Object.assign({}, state.policy.get().permitted_oa[0].embargo),
				location: {
					location: [
						"authors_homepage",
						"institutional_repository",
						"non_commercial_repository",
						"named_repository"
					],
					named_repository : [
						"HAL"
					],
				},
				conditions: [
					"Published source must be acknowledged with citation"
				],
			})
		}
		// Une précédente étape a fixé le "copyright_owner" par défaut,
		if (owner === 'journal') {
			state.setStep("EmbargoLrn")
		} else {
			state.setStep("Urls")
		}
	} else if (choice === "sans embargo") {
		state.policy.updateDefaultOa({embargo: null})
		state.policy.addOa({
			article_version: ['accepted'],
			location: {
				location: [
					"authors_homepage",
					"institutional_repository",
					"non_commercial_repository",
					"named_repository"
				],
				named_repository : [
					"HAL"
				],
			},
			conditions: [
				"Published source must be acknowledged with citation"
			],
			public_notes: [
				"This pathway allows for a non-embargoed deposit strictly in locations and under the conditions indicated."
			],
		})
		state.setStep("Urls")
	} else {
		console.error("choice:", choice)
		alert("erreur, valeur inconnue")
	}
}

const Transitions = {
	publishedDuration(amount, units) {
		state.policy.updateDefaultOa({
			embargo: {
				amount,
				units
			},
		})
		state.setStep("EmbargoDroitsAuteurs")
	},
	droitsauteurs: {
		onyes() {
			owner = 'journal'
			state.policy.updateDefaultOa({
				copyright_owner: 'journal',
			})
			state.setStep("EmbargoDepot")
		},
		onno() {
			owner = 'authors'
			state.policy.updateDefaultOa({
				copyright_owner: 'authors',
			})
			state.setStep("EmbargoDepot")
		}
	},
	depot(choices) {
		setPublished(choices.published)
		setAccepted(choices.accepted)
	},
	lrn: {
		onyes() {
			state.setStep("EmbargoAcceptedDuration")
		},
		onno() {
			state.setStep("Urls")
		}
	},
	acceptedDuration(amount, units) {
		// option LRN => durée spécifique à cette seconde option "accepted"
		state.policy.addOa({
			article_version: ['accepted'],
			embargo: {
				amount,
				units
			},
			prerequisites: {
				"prerequisites": [
				   "when_required_by_law"
				]
			},
			location: {
				location: [
					"institutional_repository",
					"non_commercial_repository",
					"named_repository"
				],
				named_repository: [
					"HAL"
				],
			},
			conditions: [
			   "Applies to authors of articles whose research is funded at least half by a French or European public institution",
			   "Published source must be acknowledged with citation"
			],
			public_notes: [
				"This pathway is strictly in accordance with Article 30 of the Law for a Digital Republic of October 7, 2016."
			],
		})
		state.setStep("Urls")
	},
}
Object.freeze(Transitions)

export default Transitions
