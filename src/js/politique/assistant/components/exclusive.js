const steps = new Set()

function ExclusiveComponent(id) {
	if (steps.has(id)) {
		console.error("This step has already been added:", id)
	}
	steps.add(id)
	this.id = id
	this.view = (vnode) => {
		if (vnode.attrs.state.id !== this.id) {
			return null
		}
		return this.contents(vnode)
	}
}

export {
	ExclusiveComponent as default
}
