import ExclusiveComponent from "./exclusive.js"
import {YesNo} from "./input.js"
import Transitions from "../transitions-standard.js"

const DepotAuthors = new ExclusiveComponent("DepotAuthors")
DepotAuthors.contents = function() {
	return m('section.depot',
		m('h2', "Autorisations de dépôt"),
		m('div.depot-published',
			m('p', m.trust("Les auteurs peuvent déposer la version <strong>publiée</strong> (version de l'éditeur) dans une archive ouverte ?")),
			m(YesNo, Transitions.depot.authors),
		)
	)
}
Object.freeze(DepotAuthors)

/**
 * Local state to store the incomplete answer.
 * When complete, it will be sent to the transition.
 */
const choices = {
	published: null,
	accepted: null,
}

/**
 * If complete, submit the answer to the transition
 */
function submit() {
	if (choices.published === null || choices.accepted === null) {
		return;
	}
	Transitions.depot.journal(choices.published, choices.accepted)
	choices.published = null
	choices.accepted = null
}

const DepotJournal = new ExclusiveComponent("DepotJournal")
DepotJournal.contents = function() {
	return m('section.depot',
		m('h2', "Autorisations de dépôt"),
		m('div.depot-published',
			m('p', m.trust("Les auteurs peuvent déposer la version <strong>publiée</strong> (version de l'éditeur) dans une archive ouverte ?")),
			m(YesNo, {
				onyes() {
					choices.published = true
					submit()
				},
				onno() {
					choices.published = false
					submit()
				},
			}),
		),
		m('div.depot-accepted',
			m('p', m.trust("Les auteurs peuvent déposer la version finale du manuscrit <strong>accepté</strong> dans une archive ouverte ?")),
			m(YesNo, {
				onyes() {
					choices.accepted = true
					submit()
				},
				onno() {
					choices.accepted = false
					submit()
				},
			}),
		)
	)
}
Object.freeze(DepotJournal)

export {
	DepotAuthors,
	DepotJournal,
}
