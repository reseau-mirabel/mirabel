import ExclusiveComponent from "./exclusive.js"
import {Select, YesNo} from "./input.js"
import EmbargoDepot from "./step-embargo-depot.js"
import {EmbargoDroitsAuteurs} from "./step-copyright-owner.js"
import Transitions from "../transitions-embargo.js"

let amount = 0;
let unit = 'months';

function isValid() {
	return amount > 0;
}

const EmbargoDuration = {
	view(vnode) {
		const context = vnode.attrs;
		return m("section",
			m('h2', context.title),
			m('p', m.trust(context.description)),
			m('input', {
				type: "number",
				min: "0",
				value: amount,
				oninput(e) {
					amount = parseInt(e.target.value);
				}
			}),
			m(Select, {
				onchange(e) {
					unit = e.target.value;
				},
				options: [
					['days', "jours"],
					['months', "mois"],
					['years', "années"],
				],
				selected: unit,
			}),
			m('button.btn.btn-primary', {
				type: "button",
				disabled: !isValid(),
				onclick() {
					context.onclick.call(this, amount, unit);
				}
			}, "OK")
		);
	}
};

const EmbargoPublishedDuration = new ExclusiveComponent("EmbargoPublishedDuration")
EmbargoPublishedDuration.contents = function() {
	return m(EmbargoDuration, {
		title: "Embargo",
		description: "Après quelle durée d'embargo les articles sont-ils diffusés en accès ouvert ?",
		onclick: Transitions.publishedDuration
	})
}

const EmbargoLrn = new ExclusiveComponent("EmbargoLrn")
EmbargoLrn.contents = function() {
	return m("section",
		m('h2', "Option avec embargo réduit LRN"),
		m('p', m.trust("La revue (ou l'éditeur) reconnaît-elle explicitement le droit des auteurs à déposer la version acceptée selon les termes de la <em>Loi pour une République numérique (Art. 30)</em> ?")),
		m(YesNo, Transitions.lrn)
	);
}

const EmbargoAcceptedDuration = new ExclusiveComponent("EmbargoAcceptedDuration")
EmbargoAcceptedDuration.contents = function() {
	return m(EmbargoDuration, {
		title: "Embargo du manuscript accepté",
		description: "Après quelle durée d'embargo les auteurs peuvent-ils déposer la version finale du manuscrit accepté ?",
		onclick: Transitions.acceptedDuration
	});
}

const Embargo = {
	view(vnode) {
		const step = vnode.attrs.state;
		if (!step.id.startsWith('Embargo')) {
			return null;
		}
		return m.fragment(
			m(EmbargoPublishedDuration, {state: step}),
			m(EmbargoDroitsAuteurs, {state: step}),
			m(EmbargoDepot, {state: step}),
			m(EmbargoLrn, {state: step}),
			m(EmbargoAcceptedDuration, {state: step}),
		);
	}
};

export default Embargo
