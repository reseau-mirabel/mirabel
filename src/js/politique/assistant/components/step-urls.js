import ExclusiveComponent from "./exclusive.js"
import Transitions from "../transitions.js"
import state from "../state.js"

const UrlField = {
	view(vnode) {
		const field = vnode.attrs.field
		return m("div", {style: "margin-bottom: 10px"},
			m("div.url.input-prepend",
				m("span.add-on", "URL"),
				m("input.input-xxlarge", {
					type: "url",
					value: field.url,
					oninput(e) {
						field.url = e.target.value
					},
				})
			),
			m("div.description.input-prepend",
				m("span.add-on", "Nom de la page"),
				m("input.input-xlarge", {
					type: "text",
					value: field.description,
					placeholder: "utilisé pour afficher le lien hypertexte",
					oninput(e) {
						field.description = e.target.value
					},
				})
			)
		)
	}
}

const CairnOption = {
	cairnUrl: "",
	oninit(vnode) {
		if (state.titreId === 0 || typeof state.titreId === 'undefined') {
			return;
		}
		m.request({
			method: "GET",
			url: '/politique/ajax-cairn-url/' + state.titreId,
			withCredentials: true,
		}).then(function(cairnUrl) {
			CairnOption.cairnUrl = cairnUrl
		})
	},
	view(vnode) {
		if (CairnOption.cairnUrl === "") {
			return null
		}
		if (CairnOption.hasInputCairnUrl(vnode.attrs.urls)) {
			return null
		}
		return m("div.control-group",
			m("button.btn.btn-primary", {
				type: "button",
				onclick: function() {
					const event = new CustomEvent("AddURL", {
						bubbles: true,
						detail: { newUrl: CairnOption.cairnUrl + "?contenu=politique"}
					})
					this.dispatchEvent(event)
				}
			}, "Générer une page d'information publique sur Cairn.info")
		)
	},
	hasInputCairnUrl(urls) {
		for (let x of urls) {
			if (x.url.startsWith("https://www.cairn.info/")) {
				return true
			}
		}
		return false
	}
}

const Urls = new ExclusiveComponent("Urls")
Urls.oninit = function(vnode) {
	vnode.state.urls = [{
		url: "",
		description: "",
	}]
	vnode.state.hasEventHandler = false
}
Urls.onupdate = function(vnode) {
	if (vnode.dom && !vnode.state.hasEventHandler) {
		vnode.state.hasEventHandler = true
		vnode.dom.addEventListener("AddURL", function(e) {
			vnode.state.urls.push({
				url: e.detail.newUrl,
				description: "Politique Cairn.info",
			})
		}, false)
	}
}
Urls.contents = function(vnode) {
	return m("section.urls",
		m('h2', "Lien vers la politique affichée publiquement"),
		m('p', "Indiquer la ou les pages qui présentent la politique de la revue en matière de diffusion en accès ouvert, droits d'auteurs, licence et autoarchivage."),
		m('p', "Si cette page n'existe pas, laisser vide et cliquer sur OK, mais il est recommandé de créer une telle page."),
		m('p', "La présentation de la politique sur le site de la revue ou de l'éditeur est un critère essentiel pour l'inclusion dans la base Open policy finder (anciennement Sherpa Romeo)."),
		m("div.control-group",
			m("div.controls",
				vnode.state.urls.map(function(field, index) {
					return m(UrlField, {field})
				}),
				m("div",
					m("button.btn", {
						type: "button",
						onclick() {
							vnode.state.urls.push({
								url: "",
								description: "",
							})
						}},
						m("span.icon.icon-plus"),
						" lien supplémentaire",
					),
				)
			),
		),
		m(CairnOption, {urls: vnode.state.urls}),
		m(".form-actions",
			m('button.btn.btn-primary', {
				type: "button",
				onclick() {
					Transitions.urls.call(this, vnode.state.urls)
				}
			}, "OK"),
		)
	)
}

export default Urls
