import state from "../state.js"
import History from "./history.js"
import Model from "./step-model.js"
import {DroitsAuteurs} from "./step-copyright-owner.js"
import {DepotAuthors, DepotJournal} from "./step-depot.js"
import Embargo from "./step-embargo.js"
import Licence from "./step-license.js"
import Urls from "./step-urls.js"
import Done from "./step-done.js"

export default {
	view() {
		const step = state.step
		return m("div#politique-assistant",
			m("div.well",
				m(Model, {state: step}),
				m(DroitsAuteurs, {state: step}),
				m(Licence, {state: step}),
				m(DepotAuthors, {state: step}),
				m(DepotJournal, {state: step}),
				m(Embargo, {state: step}),
				m(Urls, {state: step}),
				m(Done, {state: step}),
			),
			m(History, {state}),
		);
	}
}
