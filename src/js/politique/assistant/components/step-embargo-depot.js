import ExclusiveComponent from "./exclusive.js"
import {ButtonList} from "./input.js"
import Transitions from "../transitions-embargo.js"

const localState = {
	published: null,
	accepted: null,
}

function submit() {
	if (localState.published !== null && localState.accepted !== null) {
		Transitions.depot(localState)
		localState.published = null
		localState.accepted = null
	}
}

const EmbargoDepotPublished = {
	view() {
		return m("div.ao-published",
			m('p', m.trust("Les auteurs peuvent-ils déposer la version <strong>publiée</strong> (version de l'éditeur) dans une archive ouverte, accessible en texte intégral après la fin de l'embargo ?")),
			m(ButtonList, {
				titles: [
					"sans embargo",
					"après embargo",
					"jamais",
				],
				onclick: function(choice) {
					localState.published = choice
					submit()
				},
			})
		);
	},
}

const EmbargoDepotAccepted = {
	view () {
		return m("div.ao-accepted",
			m('p', m.trust("À quelles conditions les auteurs peuvent-ils déposer la version finale du manuscrit <strong>accepté</strong> dans une archive ouverte, accessible en texte intégral ?")),
			m(ButtonList, {
				titles: [
					"sans embargo",
					"après embargo",
					"jamais",
				],
				onclick: function(choice) {
					localState.accepted = choice
					submit()
				},
			})
		);
	},
}

const EmbargoDepot = new ExclusiveComponent("EmbargoDepot")
EmbargoDepot.contents = function() {
	return m("section",
		m('h2', "Autorisations de dépôt"),
		m(EmbargoDepotPublished),
		m(EmbargoDepotAccepted),
	);
}

export default EmbargoDepot
