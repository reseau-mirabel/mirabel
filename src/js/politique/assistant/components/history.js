function getClassname(index, position) {
	if (index < position) {
		return 'past'
	}
	if (index > position) {
		return 'future'
	}
	return 'present'
}

const HistoryLine = {
	view(vnode) {
		const point = vnode.attrs.point
		const index = vnode.attrs.index
		const className = getClassname(index, vnode.attrs.position)
		return m("li.savepoint", {"class": className + " index-" + index},
			m("a", {
				href: "#",
				onclick() {
					vnode.attrs.state.goto(index)
				},
			}, point.title)
		)
	}
}

export default {
	view(vnode) {
		const state = vnode.attrs.state
		const stateHistory = state.getHistory()
		return m("div#history",
			m("ol",
				stateHistory.past.map(function(x, index) {
					return m(HistoryLine, {
						point: x,
						position: stateHistory.position,
						index,
						state,
					})
				})
			)
		)
	}
}
