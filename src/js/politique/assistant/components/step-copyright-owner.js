import ExclusiveComponent from "./exclusive.js"
import {YesNo} from "./input.js"
import TransitionsEmbargo from "../transitions-embargo.js"
import TransitionsStandard from "../transitions-standard.js"

function constructor(name, transitions) {
	const Droits = new ExclusiveComponent(name)
	Droits.oninit = function(vnode) {
		vnode.state.level = 0
	}
	Droits.contents = function(vnode) {
		return m("section",
			m('h2', "Examiner les droits d'auteurs"),
			m('div.level-0',
				m('p', "Existe-t-il un contrat de cession de droits avec l'auteur ?"),
				m(YesNo, {
					onyes() {
						vnode.state.level = 1
					},
					onno: transitions.onno,
				})
			),
			m('div.level-1', {class: (vnode.state.level < 1 ? 'hidden' : ''), style: "margin-top: 2ex"},
				m('p', "S'agit-il d'une cession de droits à titre exclusif ?"),
				m(YesNo, transitions)
			),
		)
	}
	return Droits
}

const DroitsAuteurs = constructor("DroitsAuteurs", TransitionsStandard.droitsauteurs)
const EmbargoDroitsAuteurs = constructor("EmbargoDroitsAuteurs", TransitionsEmbargo.droitsauteurs)

export {
	DroitsAuteurs,
	EmbargoDroitsAuteurs,
}
