import ExclusiveComponent from "./exclusive.js"
import Policy from "../../common/components/policy-summary.js"
import state from "../state.js"
import transitions from "../transitions.js"

const Done = new ExclusiveComponent("Done")
Done.contents = function(vnode) {
	return m("section.assistant-complete",
		m("h2", vnode.attrs.state.params || "Confirmation et enregistrement"),
		m("p", "Voici la politique saisie. Si vous la confirmez, elle sera enregistrée."),
		m("p", m.trust("Si vous souhaitez modifier cette politique, vous pouvez revenir à une étape précédente de l'assistant." +
			" Pour des changements complexes, vous devez passer par la <em>vue détaillée</em> après enregistrement.")),
		m(Policy, {policy: state.policy.get()}),
		m("div.form-actions",
			m("button.btn.btn-primary", {
				type: "button",
				onclick() {
					transitions.save().then(function(result) {
						if (result.success) {
							window.location = state.returnUrl
						} else {
							alert(result.message)
							console.error(result.errors)
						}
					})
				}
			}, "Enregistrer")
		)
	)
}

export default Done

