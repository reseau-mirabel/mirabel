import ExclusiveComponent from "./exclusive.js"
import {ButtonList} from "./input.js"
import Transitions from "../transitions.js"

const Model = new ExclusiveComponent("Model")
Model.contents = function() {
	return m("section",
		m('h2', "Identification du modèle de revue"),
		m(ButtonList, {
			titles: [
				[
					"Modèle 1",
					m.trust("<strong>Revue en accès ouvert immédiat sans frais et sans embargo</strong>"),
				],
				[
					"Modèle 2",
					m.trust("<p><strong>Revue en accès ouvert immédiat avec frais</strong> de type « auteur-payeur »</p>"),
					m.trust("<p>Le modèle « auteur-payeur » implique le paiement par l'auteur ou son institution de frais de publication dits aussi <em>Article Processing Charges</em> (APC).</p>"),
				],
				[
					"Modèle 3",
					m.trust("<strong>Revue hybride</strong> : accès sur abonnement et frais optionnels OA aux choix des auteurs"),
				],
				[
					"Modèle 4",
					m.trust("<strong>Revue sur abonnement avec barrière mobile</strong> : accès gratuit aux articles après un délai"),
				],
				[
					"Modèle 5",
					m.trust("<strong>Revue sur abonnement sans accès gratuit aux numéros nouveaux ou anciens</strong>"),
				],
			],
			onclick: Transitions.model
		}),
	)
}

export default Model
