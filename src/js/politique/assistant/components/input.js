/**
 * ```
 * m(ButtonList, {
 *     titles: [
 *         "A simple choice",
 *         // With an optional description aside the button:
 *         ["Another button", "This is a help text displayed aside the button"]
 *         // The first element is the title, the next elements are the aside description.
 *         ["click me", m("p.alert.alert-warning", "Aside"), m('p', "A description....")]
 *     ]
 *     // Will be called on click, e.g. with ("A simple choice", 0) or ("click me", 2).
 *     onclick: (title, rank) => console.log("btn title & number", title, rank)
 * })
 * ```
 */
const ButtonList = {
	view(vnode) {
		return m("div.button-list",
			vnode.attrs.titles.map(function(row, index) {
				let title, description = null
				if (row instanceof Array) {
					title = row.shift()
					description = m('div', row)
				} else {
					title = row
					description = m('div', "")
				}
				return m('div.choice',
					m('button.btn.btn-default',
						{
							type: "button",
							class: "choice-" + index,
							onclick() {
								vnode.attrs.onclick.call(this, title, index);
							}
						},
						title
					),
					description,
				)
			})
		)
	}
}

const Select = {
	view(vnode) {
		const onchange = vnode.attrs.onchange;
		const options = vnode.attrs.options;
		const selected = vnode.attrs.selected || null;
		return m("select", { onchange },
			options.map(function(o) {
				return m('option', {
					value: o[0],
					selected: (o[0] === selected)
				}, o[1]);
			})
		);
	}
};

const YesNo = {
	view(vnode) {
		return m("div.yesno",
			m('button.btn.btn-default',
				{
					type: "button",
					class: "yesno-yes",
					onclick: vnode.attrs.onyes
				},
				(vnode.attrs.yes ? vnode.attrs.yes : "Oui")
			),
			m('button.btn.btn-default',
				{
					type: "button",
					class: "yesno-no",
					onclick: vnode.attrs.onno
				},
				(vnode.attrs.no ? vnode.attrs.no : "Non")
			)
		);
	}
};

export {
	ButtonList,
	Select,
	YesNo,
}
