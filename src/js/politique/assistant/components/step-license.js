import ExclusiveComponent from "./exclusive.js"
import {ButtonList, YesNo} from "./input.js"
import Transitions from "../transitions-standard.js"

const LicenceCC = new ExclusiveComponent("LicenceCC")
LicenceCC.contents = function(vnode) {
	const context = vnode.attrs.state.params
	const messages = {
		journal: "La revue qui possède les droits exclusifs diffuse-t-elle les articles sous licence CC, Creative Commons ?",
		authors: "La revue possède t-elle, à titre non exclusif, le droit de diffuser les articles en licence CC ?",
	}
	return m("section",
		m('h2', "Déterminer la licence"),
		m('p', messages[context]),
		m(YesNo, Transitions.licenceCC),
	)
}

const LicenceSelection = new ExclusiveComponent("LicenceSelection")
LicenceSelection.contents = function() {
	// Le droit au dépôt de la version publiée est intrinsèque aux licences CC
	return m("section",
		m('h2', "Choix de la licence CC"),
		m("div.licencecc-selection",
			m('div.alert.alert-info',
				m('p', m.trust(`Vous pouvez essayer le <a href="https://chooser-beta.creativecommons.org">sélectionneur de licence</a> (en anglais).`)),
			),
			m(ButtonList, {
				titles: [
					[
						"CC BY",
						m('p', "La version peut être librement utilisée, à la condition de l'attribuer à l'auteur en citant son nom."),
					],
					[
						"CC BY-ND",
						m('p', "La version peut être diffusée tant que l'auteur est cité. Diffuser une version modifiée est interdit."),
					],
					[
						"CC BY-SA",
						m('p', "La version peut être diffusée et modifiée, tant que l'auteur est cité et que la version dérivée reste sous licence CC BY-SA."),
					],
					[
						"CC BY-NC",
						m('p', "La version peut être diffusée et modifiée, tant que l'auteur est cité et que l'utilisation n'est pas commerciale."),
					],
					[
						"CC BY-NC-SA",
						m('p', "La version peut être diffusée et modifiée, tant que l'auteur est cité, que l'utilisation n'est pas commerciale et que la version dérivée reste sous licence CC BY-NC-SA."),
					],
					[
						"CC BY-NC-ND",
						m('p', "La version peut être diffusée, tant que l'auteur est cité et que l'utilisation n'est pas commerciale. Diffuser une version modifiée est interdit."),
					],
				],
				onclick: Transitions.licenceSelection
			})
		)
	)
}

const Licence = {
	view(vnode) {
		const state = vnode.attrs.state
		if (state.id !== "LicenceCC" && state.id !== "LicenceSelection") {
			return null
		}
		return m.fragment(
			m(LicenceCC, {state}),
			m(LicenceSelection, {state}),
		)
	}
}

export default Licence
