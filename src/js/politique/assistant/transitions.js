import state from "./state.js"

/**
 * Contains the common part of the transitions.
 */
const Transitions = {
	// first step
	model(name, index) {
		state.model = index
		if (index === 0 || index === 1) {
			state.policy.update({open_access_prohibited: "no"})
			state.policy.updateDefaultOa({
				additional_oa_fee: 'no',
			})
			state.setStep("DroitsAuteurs")
		} else if (index === 2) {
			state.setStep("Urls")
		} else if (index === 3) {
			state.policy.update({open_access_prohibited: "no"})
			state.policy.updateDefaultOa({
				additional_oa_fee: 'no',
				embargo: 'yes'
			})
			state.setStep("EmbargoPublishedDuration")
		} else if (index === 4) {
			state.setStep("Urls")
		}
	},
	// last step, before confirming and saving on the server
	urls(urls) {
		const clean = [];
		for (let x of urls) {
			if (x.url.trim() !== '') {
				if (x.description.trim() === '') {
					clean.push({
						url: x.url.trim(),
					})
				} else {
					clean.push({
						description: x.description.trim(),
						url: x.url.trim(),
					})
				}
			}
		}
		if (clean.length > 0) {
			state.policy.update({urls: clean})
		}
		state.setStep("Done")
	},
	// AJAX save, returns a promise
	save() {
		const data = state.export()
		const policy = JSON.parse(data.publisher_policy).policy
		const body = new FormData()
		body.append("editeurId", data.editeurId)
		body.append("titreId", data.titreId)
		body.append("model", data.model)
		body.append("publisher_policy", JSON.stringify(policy))
		return m.request({
			method: 'POST',
			url: '/politique/save',
			body,
		})
	},
}
Object.freeze(Transitions)

export default Transitions
