import actions from './actions.js'
import state from './state.js'
import Main from './components/main.js'

window.PolitiqueValidation = {
	init(htmlId, data) {
		// find the root HTML element
		const root = document.getElementById(htmlId)
		if (!root) {
			alert("JS bug, root element #" + htmlId + " was not found.")
			return;
		}

		if (!data.users || data.users.length === 0) {
			return;
		}

		state.users = new Map()
		actions.init(data.users, data.roles)
		state.owners = new Map(data.owners)

		m.mount(root, Main);
	}
}
