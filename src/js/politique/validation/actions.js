import state from './state.js'

function init(users, roles) {
	state.users = new Map()
	if (users && users.length > 0) {
		for (let u of users) {
			state.users.set(u.id, u);
		}
	}

	if (roles && roles.length > 0) {
		roles[0].hideUser = false
		if (roles.length > 1) {
			for (let i = 1; i < roles.length; i++) {
				roles[i].hideUser = (roles[i].utilisateurId === roles[i-1].utilisateurId)
			}
		}
		state.roles = roles
	} else {
		state.roles = []
	}
}

function post(role, action) {
	const body = new window.FormData()
	body.append("action", action)
	body.append("utilisateurId", role.utilisateurId)
	body.append("editeurId", role.editeur.id)
	const p = m.request({
		method: 'POST',
		url: "/admin/politique-validation",
		body: body,
		background: true,
	}).catch(function() {
		window.alert("Une erreur de réseau s'est produite. Merci de réessayer")
	})
	if (typeof global !== 'undefined') {
		// We are testing, so pass the promise
		global.waitingFor = p
	}
	return p
}

function setStatus(role, status) {
	role.status = status
}

export default {
	init,
	post,
	setStatus,
}
