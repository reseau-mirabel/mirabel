import state from '../state.js'
import TrRole from './tr-role.js'

export default {
	view() {
		return m.fragment({},
			state.roles.map(function(role) {
				return m(TrRole, {role})
			})
		)
	}
}
