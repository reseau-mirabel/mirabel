import state from '../state.js'
import Actions from './actions.js'

function toIsoDate(ts) {
	if ((typeof ts === 'undefined') || !ts) {
		return '';
	}
	if ((typeof ts === 'string') && ts.match(/^\d{4}-\d\d-\d\d/)) {
		return ts.substring(0, 10);
	}
	return (new Date(ts * 1000)).toISOString().substring(0, 10);
}

const UserLink = {
	view(vnode) {
		const user = vnode.attrs.user
		const userName = (user.nomComplet ? user.nomComplet : user.prenom + " " + user.nom)
		return m('span',
			m('a', {href: '/utilisateur/' + user.id}, userName),
			(user.partenaireId > 0 ? m('sup', " [P]") : null),
		)
	}
}

const UserMessage = {
	view(vnode) {
		const user = vnode.attrs.user
		if (user.message === "") {
			return null
		}
		return m("div",
			m("abbr", {title: user.message}, "[message]")
		)
	}
}

const Username = {
	view(vnode) {
		if (vnode.attrs.hide) {
			return m('td.user-name', null)
		}

		const user = vnode.attrs.user
		return m('td.user-name',
			m(UserLink, {user}),
			m(UserMessage, {user}),
		)
	}
}

const Owners = {
	view(vnode) {
		const users = vnode.attrs.owners
		if (users === undefined) {
			return null
		}
		return users.map((u) => {
			if (u.id > 0) {
				return m('div', m('a', {href: '/utilisateur/' + u.id}, u.nomComplet))
			}
			return m('div', u.nomComplet)
		})
	}
}

const RoleFr = {
	view(vnode) {
		const role = vnode.attrs.role
		const user = state.users.get(role.utilisateurId)
		const roleToFr = {
			guest: "délégué",
			owner: "responsable",
		}
		return m("div", roleToFr[role.role])
	}
}

export default {
	view(vnode) {
		const role = vnode.attrs.role
		const user = state.users.get(role.utilisateurId)
		return m('tr.pending-role.role-' + role.utilisateurId + "-" + role.editeur.id,
			m(Username, {user, hide: role.hideUser}),
			m('td.user-email', m('a', {href: "mailto:" + user.email}, user.email)),
			m('td.publisher',
				m('a', {href: "/editeur/" + role.editeur.id}, role.editeur.nom),
				(role.pays && role.pays !== "France") ? m("div", role.pays) : "",
			),
			m('td.owners',
				m(Owners, {owners: state.owners.get(role.editeur.id)}),
			),
			m('td', role.politiques > 0 ?
				m("a", {href: '/politique/index?editeurId=' + role.editeur.id}, "oui")
				: "non"),
			m('td.hdate-creation', toIsoDate(role.lastUpdate)),
			m('td', m(RoleFr, {role})),
			m(Actions, {role: role})
		)
	}
}
