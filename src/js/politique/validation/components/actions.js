import actions from '../actions.js'
import state from '../state.js'

const Overwrites = {
	view(vnode) {
		if (!vnode.attrs.overwrites) {
			return null
		}
		return m('ul',
			vnode.attrs.overwrites.map(function(over) {
				return m('li',
					"Remplacera ",
					m('a', {href: '/politique/update/' + over.id}, over.name),
					" pour le titre ",
					m('a', {href: '/titre/' + over.titreId}, over.titreName),
				)
			})
		)
	}
}

const ButtonOwner = {
	view(vnode) {
		const role = vnode.attrs.role;
		return m('button.owner', {
			type: "button",
			class: "btn btn-success btn-small",
			title: "Accepte le lien à l'éditeur",
			onclick(e) {
				e.redraw = false
				actions.post(role, 'owner').then(function(r) {
					if (r.success) {
						actions.setStatus(role, "accepté (responsable)")
						const name = state.users.get(role.utilisateurId).nomComplet
						let owners = state.owners.get(role.editeur.id)
						if (owners === undefined) {
							state.owners.set(role.editeur.id, [{id: role.utilisateurId, nomComplet: name}])
						} else {
							owners.push({id: role.utilisateurId, nomComplet: name})
							state.owners.set(role.editeur.id, owners)
						}
					} else {
						window.alert(r.message)
					}
				}).finally(m.redraw)
			}},
			"→ responsable")
	},
}

const ButtonOwnerDisabled = {
	view(vnode) {
		const role = vnode.attrs.role;
		return m('button.owner.disabled', {
				type: "button",
				class: "btn btn-success btn-small",
				title: "Cet éditeur a déjà un responsable.",
			},
			"→ responsable")
	},
}

const ButtonGuest = {
	view(vnode) {
		const role = vnode.attrs.role;
		return m('button.guest',
			{
				type: "button",
				class: "btn btn-success btn-small",
				title: "Accepte le lien à l'éditeur, sans possibilité de déléguer",
				onclick(e) {
					e.redraw = false
					actions.post(role, 'guest').then(function(r) {
						if (r.success) {
							actions.setStatus(role, "accepté (délégué)")
						} else {
							window.alert(r.message)
						}
					}).finally(m.redraw)
				}
			},
			"→ délégué")
	},
}

const ButtonReject = {
	view(vnode) {
		const role = vnode.attrs.role;
		return m('button.reject',
			{
				type: "button",
				class: "btn btn-danger btn-small",
				title: "Supprime la demande",
				onclick(e) {
					e.redraw = false
					if (!window.confirm("Refuser cette demande et la supprimer ?")) {
						return;
					}
					actions.post(role, 'delete').then(function(r) {
						if (r.success) {
							actions.setStatus(role, "supprimé")
						} else {
							window.alert(r.message)
						}
					}).catch(function(r) {
						window.alert(r.message)
					})
					.finally(m.redraw)
				}
			},
			"refuser")
	},
}


export default {
	view(vnode) {
		const role = vnode.attrs.role;
		if ('status' in role) {
			return m('td.status', m('em', role.status));
		}
		return m('td.actions',
			m(Overwrites, {overwrites: role.overwrites}),
			(state.owners.get(role.editeur.id) !== undefined ?
				m(ButtonOwnerDisabled, {role})
				: m(ButtonOwner, {role})),
			m(ButtonGuest, {role}),
			m(ButtonReject, {role}),
		)
	}
}
