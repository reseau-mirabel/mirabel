const state = {
	users: null, // Map[uid] => Utilisateur
	roles: null, // [ {utilisateurId: 121, politiques: 3, role: "owner", pays: "", editeur: Editeur... } ]
	owners: null, // Map[Editeur.id] => [{ id: X, nomComplet: Y }]
}

export default state
