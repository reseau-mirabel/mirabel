const state = {
	editingOa: null, // null, or index of oa, or -1 for common
	policy: {
		open_access_prohibited: "no",
		permitted_oa: [],
	},
	politique: {}, // id, editeurId ...
	readonly: false,

	get oa() {
		if (state.editingOa !== null && state.editingOa >= 0) {
			return state.policy.permitted_oa[state.editingOa]
		}
		return null
	},
	set oa(newValue) {
		if (state.editingOa !== null && state.editingOa >= 0) {
			state.policy.permitted_oa[state.editingOa] = newValue
		} else {
			window.alert("Not editing")
		}
	},

	isValid() {
		return state.getErrors().size === 0
	},
	getErrors() {
		const errors = new Map()
		const oa = state.oa
		if (oa !== null) {
			// editing this OA, so check its consistency
			if (!oa.hasOwnProperty('article_version') || oa.article_version.length === 0) {
				errors.set('article_version', "Au moins une version doit être choisie.")
			}
		}
		return errors
	},

	toJSON() {
		// return the data to serialize
		return {
			editingOa: state.editingOa,
			policy: state.policy,
			politique: state.politique,
		}
	},
}
Object.seal(state)

export default state
