import state from "../state.js"
import actions from "../actions.js"

import View from "../../common/components/policy-summary.js"

const ButtonAddOa = {
	view() {
		return m("div",
			m('button.btn.btn-default', {
				type: 'button',
				onclick() {
					state.policy.permitted_oa.push({})
					actions.startEditMode(state.policy.permitted_oa.length - 1)
				}
			}, "Ajouter une nouvelle modalité")
		)
	}
}

const ButtonExport = {
	view() {
		return m('button.btn.btn-default', {
				type: 'button',
				onclick() {
					window.print()
				},
				style: "float: right"
			}, "Exporter / imprimer"
		)
	}
}

const ButtonGoaway = {
	view() {
		return m("button.btn.btn-primary", {
				type: "button",
				onclick() {
					window.location = '/politique/index?editeurId=' + state.politique.editeurId
				},
			}, "Retourner à la liste des politiques"
		)
	}
}

export default {
	view() {
		const params = {
			model: state.politique.model,
			journal: state.politique.titre,
			policy: state.policy,
			politique: state.politique,
		}
		if (!state.readonly) {
			params.edit = {
				delete: actions.delete,
				update: actions.startEditMode,
			}
		}
		return m("div.politique-view",
			m(View, params),
			state.readonly ?
				m('p.alert.alert-warning', ["Seul l'éditeur ", m('em', state.politique.editeur), " peut modifier cette politique."])
				: m(ButtonAddOa),
			m("div.form-actions",
				m(ButtonGoaway),
				m(ButtonExport),
			),
		)
	}
}
