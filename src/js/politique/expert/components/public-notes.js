import {getTermList} from "../../common/translate.js"
import CheckboxList from "./checkbox-list.js"

export default {
	view(vnode) {
		const oa = vnode.attrs.oa
		const selected = oa.hasOwnProperty('public_notes') ? oa.public_notes : []
		return m("div.public-notes",
			m(CheckboxList, {
				label: "Remarques publiques (facultatif)",
				name: "public_notes",
				checked: selected,
				candidates: getTermList('public_notes'),
				onchange(event, result) {
					oa.public_notes = result
				}
			})
		)
	}
}




