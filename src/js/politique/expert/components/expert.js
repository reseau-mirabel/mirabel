import state from "../state.js"

import Update from "./expert-update.js"
import View from "./expert-view.js"

export default {
	view() {
		if (!state.readonly && state.editingOa !== null) {
			return m(Update)
		}
		return m(View)
	}
}
