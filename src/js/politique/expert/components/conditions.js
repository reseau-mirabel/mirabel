import {getTermList} from "../../common/translate.js"
import CheckboxList from "./checkbox-list.js"

export default {
	view(vnode) {
		const oa = vnode.attrs.oa
		return m("div.conditions",
			m(CheckboxList, {
				label: "Conditions (facultatif)",
				name: "conditions",
				checked: oa.conditions || [],
				candidates: getTermList('conditions'),
				onchange(event, result) {
					oa.conditions = result
				}
			})
		)
	}
}
