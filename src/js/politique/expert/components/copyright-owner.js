import Dropdown from "./dropdown.js"
import {getTermList} from "../../common/translate.js"

export default {
	view(vnode) {
		const oa = vnode.attrs.oa
		return m("div.copyright-owner",
			m(Dropdown, {
				label: "Détenteur des droits",
				name: "copyright_owner",
				value:  oa.copyright_owner || "",
				options: getTermList('copyright_owner'),
				onchange(event) {
					oa.copyright_owner = event.target.value
				}
			})
		)
	}
}
