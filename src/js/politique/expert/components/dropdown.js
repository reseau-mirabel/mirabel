import Hint from "../../common/components/hint.js"

export default {
	view(vnode) {
		const value = vnode.attrs.value
		const entries = (vnode.attrs.options instanceof Array) ?
			vnode.attrs.options
			: Object.entries(vnode.attrs.options)
		const name = vnode.attrs.name || null
		return m("div.control-group",
			m("label.control-label", {"for" : name},
				vnode.attrs.label,
				m(Hint, {name: name})
			),
			m("div.controls",
				m("select", {name: name, onchange: vnode.attrs.onchange},
					entries.map(function(o) {
						if (o instanceof Array) {
							return m(
								"option",
								{
									value: o[0],
									selected: o[0] === value,
									title: (o.length > 2 ? o[2] : null),
								},
								o[1]
							);
						} else {
							return m("option", {value: o.value, selected: o.value === value}, o.text);
						}
					})
				),
			),
		)
	}
}
