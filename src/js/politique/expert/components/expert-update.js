import state from "../state.js"
import actions from "../actions.js"

import ArticleVersion from "./article-version.js"
import Conditions from "./conditions.js"
import CopyrightOwner from "./copyright-owner.js"
import Embargo from "./embargo.js"
import Fee from "./fee.js"
import License from "./license.js"
import Location from "./location.js"
import PolicyName from "./policy-name.js"
import Prerequisites from "./prerequisites.js"
import PublicNotes from "./public-notes.js"
import Urls from "./urls.js"

function save() {
	actions.save().then(function(result) {
		if (result.success) {
			actions.stopEditMode()
		} else {
			alert(result.message)
			console.error(result.errors)
		}
	})
}

const ButtonsSaveCancel = {
	view() {
		return m('div.form-actions',
			m('button.btn.btn-primary', {
				type: 'button',
				onclick: save,
				disabled: !state.isValid(),
			}, "Enregistrer les modifications"),
			" ",
			m('button.btn.btn-danger', {
				type: 'button',
				onclick() {
					if (confirm("Abandonner les changements ?")) {
						actions.stopEditMode()
					}
				}
			}, "Annuler"),
		)
	}
}


const UpdateCommon = {
	view() {
		return m("div#politique-expert.form-horizontal",
			m("h2", "Modification de l'identification"),
			m(PolicyName, {policy: state.policy}),
			m(Urls, {policy: state.policy}),
			m(ButtonsSaveCancel)
		)
	}
}
const UpdateOa = {
	view() {
		return m("div#politique-expert.form-horizontal",
			m("h2", "Modification de la modalité de diffusion " + (1+state.editingOa) + "/" + state.policy.permitted_oa.length),
			m(ArticleVersion, {oa: state.oa}),
			m(Prerequisites, {oa: state.oa}),
			m(CopyrightOwner, {oa: state.oa}),
			m(Conditions, {oa: state.oa}),
			m(Embargo, {oa: state.oa}),
			m(Fee, {oa: state.oa}),
			m(Location, {oa: state.oa}),
			m(License, {oa: state.oa}),
			m(PublicNotes, {oa: state.oa}),
			m(ButtonsSaveCancel),
		)
	}
}

export default {
	view() {
		if (state.editingOa >= 0) {
			return m(UpdateOa)
		} else {
			return m(UpdateCommon)
		}
	}
}
