import Hint from "../../common/components/hint.js"

let policy
let urls = []

const empty = {
	description: "",
	url: "",
}

function updateUrls() {
	policy.urls = urls.filter(x => x.url !== '')
}

const UrlField = {
	view(vnode) {
		const field = vnode.attrs.field
		return m("div", {style: "margin-bottom: 10px"},
			m("div.url.input-prepend",
				m("span.add-on", "URL"),
				m("input.input-xxlarge", {
					type: "url",
					value: field.url,
					oninput(e) {
						field.url = e.target.value
						updateUrls()
					},
				})
			),
			m("div.description.input-prepend",
				m("span.add-on", "Nom de la page"),
				m("input.input-xlarge", {
					type: "text",
					value: field.description,
					placeholder: "utilisé pour afficher le lien hypertexte",
					oninput(e) {
						field.description = e.target.value
						updateUrls()
					},
				})
			)
		)
	}
}

export default {
	oninit(vnode) {
		policy = vnode.attrs.policy
		urls = []
		if (vnode.attrs.policy.urls) {
			for (let x of vnode.attrs.policy.urls) {
				urls.push(Object.assign({}, x))
			}
		}
		urls.push(Object.assign({}, empty))
	},
	view(vnode) {
		const policy = vnode.attrs.policy
		return m("section.urls",
			m("div.control-group",
				m('label.control-label',
					"URL publique",
					m(Hint, {name: "policy-urls"}),
				),
				m('div.controls',
					urls.map(function(field) {
						return m(UrlField, {field})
					}),
					m("div",
						m("button.btn", {
							type: "button",
							onclick() {
								urls.push(Object.assign({}, empty))
							}},
							m("span.icon.icon-plus"),
							" lien supplémentaire",
						),
					)
				),
			),
		)
	}
}
