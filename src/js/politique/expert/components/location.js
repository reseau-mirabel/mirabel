import {getTermList} from "../../common/translate.js"
import CheckboxList from "./checkbox-list.js"

let currentLocation = []

function updateLocation(oa) {
	if (Archive.selected.length > 0) {
		if (!currentLocation.includes("named_repository")) {
			currentLocation.push("named_repository")
			currentLocation.sort()
		}
	} else {
		currentLocation = currentLocation.filter(x => x !== "named_repository")
	}
	if (currentLocation.length === 0) {
		if ('location' in oa) {
			delete(oa.location)
		}
	} else if (Archive.selected.length > 0) {
		oa.location = {
			location: currentLocation,
			named_repository: Archive.selected,
		}
	} else {
		oa.location = {
			location: currentLocation.filter(x => x !== "named_repository"),
		}
	}
}

const Option = {
	view(vnode) {
		const c = vnode.attrs.option
		return m(
			"option",
			{
				value: c[0],
				title: (c.length > 2 ? c[2] : null),
			},
			(c.length > 1 ? c[1] : c[0])
		)
	}
}

function getArchiveCompletions(excludes) {
	const completions = []
	for (let x of getTermList('location.named_repository')) {
		if (excludes.includes(x[0])) {
			continue;
		}
		if (x.length > 1 && x[1] !== '') {
			completions.push(x)
		} else {
			completions.push([x[0], x[0]])
		}
	}
	return completions
}

const Archive = {
	selected: [],
	oninit(vnode) {
		const oa = vnode.attrs.oa
		if (oa.location && 'named_repository' in oa.location) {
			Archive.selected = oa.location.named_repository.slice()
		} else {
			Archive.selected = []
		}
	},
	view(vnode) {
		const oa = vnode.attrs.oa
		const candidates = Archive.selected.slice()
		if (!candidates.includes("HAL")) {
			candidates.unshift("HAL")
		}
		return m("div.location-namedrepository",
			m(CheckboxList, {
				label: "Archive nommée",
				name: "named_repository",
				checked: Archive.selected,
				candidates: candidates,
				onchange(event, result) {
					Archive.selected = result
					updateLocation(oa)
				},
			}),
			m('div.control-group', m('div.controls',
				m('label', "Autre :"),
				m("input#namedrepository-free", {
					type: "text",
					list: "namedrepository-list"
				}),
				m("datalist#namedrepository-list",
					getArchiveCompletions(Archive.selected).map((c) => m(Option, {option: c}))
				),
				m('button.btn.btn-default#namedrepository-add', {
					type: "button",
					onclick(e) {
						const input = e.target.closest('div').querySelector('#namedrepository-free')
						const name = input.value
						if (input.value.trim() !== '') {
							Archive.selected.push(name)
							updateLocation(oa)
						}
						input.value = ""
					}
				}, "+")
			))
		)
	}
}

export default {
	view(vnode) {
		const oa = vnode.attrs.oa
		if (oa.location && oa.location.location) {
			currentLocation = oa.location.location.filter(x => x !== "named_repository")
		} else {
			currentLocation = []
		}
		const locationTerms = getTermList('location').filter(x => x[0] !== "named_repository")
		return m("div.location",
			m("div.location-location",
				m(CheckboxList, {
					label: "Site de diffusion",
					name: "location",
					checked: currentLocation,
					candidates: locationTerms,
					onchange: function(event, result) {
						currentLocation = result
						updateLocation(oa)
					}
				}),
			),
			m(Archive, {oa}),
		)
	}
}
