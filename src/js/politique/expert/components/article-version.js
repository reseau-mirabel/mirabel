import state from "../state.js"
import {getTermList} from "../../common/translate.js"
import CheckboxList from "./checkbox-list.js"

const isNode = (typeof global !== 'undefined')

let confirmed = false

export default {
	view(vnode) {
		const oa = vnode.attrs.oa
		const errors = state.getErrors()
		const error = errors.has('article_version') ? errors.get('article_version') : ''
		return m("div.version",
			m(CheckboxList, {
				label: "Appliquée à",
				name: "article_version",
				checked: oa.article_version || [],
				candidates: getTermList('article_version'),
				error,
				onchange(event, selectedValues) {
					if (isNode || !oa.article_version || confirmed || window.confirm("Êtes-vous certain de vouloir modifier la portée de cet accès ouvert ?")) {
						confirmed = true
						oa.article_version = selectedValues
					}
				}
			}),
		)
	}
}
