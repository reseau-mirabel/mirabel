import Hint from "../../common/components/hint.js"

let amount = 0
let units = "months"

function setEmbargo(oa) {
	if (amount > 0) {
		oa.embargo = { amount, units }
	} else if ('embargo' in oa) {
		delete(oa.embargo)
	}
}

const Dropdown = {
	view(vnode) {
		const value = vnode.attrs.value
		const entries = (vnode.attrs.options instanceof Array) ?
			vnode.attrs.options
			: Object.entries(vnode.attrs.options)
		const name = vnode.attrs.name || null
		return m("select", {name: name, onchange: vnode.attrs.onchange},
			entries.map(function(o) {
				return m(
					"option",
					{
						value: o[0],
						selected: o[0] === value,
						title: (o.length > 2 ? o[2] : null),
					},
					o[1]
				);
			})
		)
	}
}

export default {
	view(vnode) {
		const oa = vnode.attrs.oa;
		if (oa.hasOwnProperty('embargo')) {
			amount = oa.embargo.amount
			units = oa.embargo.units
		} else {
			amount = 0
			units = "months"
		}
		return m("div.embargo.control-group",
			m('label.control-label',
				"Embargo",
				m(Hint, {name: "embargo"}),
			),
			m("div.controls",
				m('input', {
					type: "number",
					min: "0",
					value: amount,
					onchange(e) {
						amount = parseInt(e.target.value)
						setEmbargo(oa)
					}
				}),
				m(Dropdown, {
					options: [
						['days', "jours"],
						['months', "mois"],
						['years', "années"],
					],
					value: units,
					onchange(e) {
						units = e.target.value
						setEmbargo(oa)
					},
				}),
			)
		)
	}
}
