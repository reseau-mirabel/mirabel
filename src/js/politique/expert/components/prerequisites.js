import {getTermList} from "../../common/translate.js"
import CheckboxList from "./checkbox-list.js"

export default {
	view(vnode) {
		const oa = vnode.attrs.oa
		const selected = oa.hasOwnProperty('prerequisites') ? oa.prerequisites.prerequisites : []
		return m("div.prerequisites",
			m(CheckboxList, {
				label: "Prérequis (facultatif)",
				name: "prerequisites",
				checked: selected,
				candidates: getTermList('prerequisites'),
				onchange(event, result) {
					oa.prerequisites = {prerequisites: result}
				}
			})
		)
	}
}


