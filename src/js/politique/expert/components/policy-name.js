import Hint from "../../common/components/hint.js"

export default {
	view(vnode) {
		const policy = vnode.attrs.policy
		return m("section.policy-name",
			m("div.control-group",
				m('label.control-label',
					"Nom",
					m(Hint, {name: "policy-name"}),
				),
				m('div.controls',
					m("input", {
						type: "text",
						value: policy.internal_moniker,
						oninput(event) {
							policy.internal_moniker = event.target.value
						}}
					),
				),
			),
		)
	}
}
