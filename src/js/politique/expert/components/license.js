import {getTermList} from "../../common/translate.js"
import CheckboxList from "./checkbox-list.js"

export default {
	oninit(vnode) {
		const oa = vnode.attrs.oa
		vnode.state.version = oa.license ? oa.license[0].version : "4.0"
	},
	view(vnode) {
		const oa = vnode.attrs.oa
		const values = oa.license ? oa.license.map(x => x.license) : []
		return m("div.license",
			m(CheckboxList, {
				label: "Licence autorisée",
				name: "license", // used by Hint
				checked: values,
				format: 'raw',
				candidates: getTermList('license'),
				onchange(event, selectedValues) {
					oa.license = selectedValues.map(x => ({license: x, version: vnode.state.version}))
				},
			}),
			m("div.control-group",
				m("div.controls",
					m("label", {for: "license-cc-version"}, "Version de la licence CC :"),
					m('input', {
						type: "text",
						id: "license-cc-version",
						name: "license-cc-version",
						value: vnode.state.version,
						oninput() {
							vnode.state.version = this.value
							for (let i = 0; i < oa.license.length; i++) {
								oa.license[i].version = vnode.state.version
							}
						}
					})
				)
			)
		);
	}
}
