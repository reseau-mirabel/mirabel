import Hint from "../../common/components/hint.js"

export default {
	view: function(vnode) {
		const label = vnode.attrs.label
		const name = vnode.attrs.name || null
		return m("div.control-group", vnode.attrs.error ? {class: 'error'} : {},
			m("label.control-label", label, (name ? m(Hint, {name}) : null)),
			m("div.controls",
				vnode.attrs.candidates.map(function (c, index) {
					let value
					let display
					if (c instanceof Array) {
						value = c[0]
						display = c.length > 1 && c[1] !== '' ? c[1] : c[0]
					} else {
						value = c
						display = c
					}
					return m("div",
						m("label.checkbox",
							m("input", {
								type: "checkbox",
								name: name + '_' + index,
								checked: vnode.attrs.checked.includes(value),
								value: value,
								onchange(event) {
									if (!vnode.attrs.onchange) {
										return true
									}
									let result = []
									let checkboxes = event.target.closest("div.controls").querySelectorAll('input[type="checkbox"]')
									for (let c of checkboxes) {
										if (c.checked) {
											result.push(c.value)
										}
									}
									vnode.attrs.onchange.call(this, event, result)
								}
							}),
							" ",
							(typeof display === 'string' ? m.trust(display) : display)
						),
						(c instanceof Array && c.length > 2 ? m(Hint, {contents: c[2]}) : null)
					)
				}),
				m('span.help-block', vnode.attrs.error),
			),
		)
	}
}


