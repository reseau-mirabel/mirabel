import Dropdown from "./dropdown.js"

export default {
	view(vnode) {
		const oa = vnode.attrs.oa;
		return m("div.fee",
			m(Dropdown, {
				label: "Frais d'option OA",
				name: "fee",
				value: oa.additional_oa_fee || "no",
				options: [["yes", "Oui"], ["no", "Non"]],
				onchange(event) {
					oa.additional_oa_fee = event.target.value;
				}
			})
		)
	}
}
