import state from "./state.js"

function pushState(title) {
	if (typeof global === 'undefined' || !('specify' in global)) {
		// probably not running in a test env
		const backup = JSON.stringify(state.toJSON())
		window.history.pushState(backup, title)
	}
}

export default {
	delete(oaRank) {
		pushState("Résumé")
		state.policy.permitted_oa.splice(oaRank, 1)
	},
	// AJAX save, returns a promise
	save() {
		const body = new FormData()
		body.append("editeurId", state.politique.editeurId)
		body.append("politiqueId", state.politique.id)
		body.append("publisher_policy", JSON.stringify(state.policy))
		return m.request({
			method: 'POST',
			url: '/politique/save',
			body,
		})
	},
	startEditMode(oaRank) {
		if (typeof oaRank !== 'number') {
			console.error("Invalid OA rank (not a number):", oaRank)
			return;
		}
		if (oaRank < -1 || oaRank > state.policy.permitted_oa.length) {
			console.error("Out of range OA rank:", oaRank)
			return;
		}
		pushState("Résumé")
		state.editingOa = oaRank
	},
	stopEditMode() {
		pushState("Modification")
		state.editingOa = null
	},
}
