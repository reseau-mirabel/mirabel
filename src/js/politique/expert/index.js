import state from "./state.js"
import Expert from "./components/expert.js"
import {loadHints} from "../common/hints.js"
import {loadTranslations} from "../common/translate.js"

window.onpopstate = function(event) {
	Object.assign(state, JSON.parse(event.state));
	m.redraw();
}

window.PolitiqueExpert = (function() {
	return {
		init(htmlId, data) {
			// find the root HTML element
			const root = document.getElementById(htmlId)
			if (!root) {
				alert("JS bug, root element #" + htmlId + " was not found.")
				return;
			}

			loadHints(data.hints)
			loadTranslations(data.translations)

			if (data.policy) {
				state.policy = data.policy
				for (let oa of state.policy.permitted_oa) {
					if (oa.license) {
						oa.license = oa.license.filter(x => x !== null)
					}
				}
			}
			state.politique = data.politique
			state.readonly = data.readonly

			m.mount(root, Expert)
		}
	}
})()
