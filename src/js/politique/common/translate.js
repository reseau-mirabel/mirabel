let lists = new Map()
let translations = {}

function getTermList(domain) {
	if (lists.has(domain)) {
		return lists.get(domain)
	}
	return []
}

function loadTranslations(data) {
	for (let domain in data) {
		translations[domain] = new Map()
		if (data[domain] instanceof Array) {
			lists.set(domain, data[domain])
			for (let v of data[domain]) {
				translations[domain].set(v[0], v.length > 1 ? v[1] : v[0])
			}
		} else {
			const l = []
			for (let k in data[domain]) {
				l.push([k, data[domain][k]])
				translations[domain].set(k, data[domain][k])
			}
			lists.set(domain, l)
		}
	}
}

function translate(text, category) {
	if (text instanceof Array) {
		return text.map(t => translate(t, category))
	}
	if (category in translations && translations[category].has(text)) {
		return translations[category].get(text)
	}
	return text
}

export {
	getTermList,
	loadTranslations,
	translate
}
