import {getHint} from "../hints.js"

export default {
	oncreate(vnode) {
		if (vnode.state.contents) {
			$(vnode.dom).popover()
		}
	},
	view(vnode) {
		vnode.state.contents = ''
		if (vnode.attrs.hasOwnProperty('contents')) {
			vnode.state.contents = vnode.attrs.contents
		} else if (vnode.attrs.hasOwnProperty('name') && vnode.attrs.name !== null) {
			vnode.state.contents = getHint(vnode.attrs.name)
		}
		if (typeof vnode.state.contents === 'undefined' || !vnode.state.contents) {
			return null
		}
		return m(
			"a.hint",
			{
				href: "#",
				'data-title': "Aide",
				'data-content': vnode.state.contents,
				'data-html': "true",
				'data-trigger': "hover",
				'data-container': 'body',
				rel: "popover"
			},
			"?"
		)
	}
}
