import Hint from "./hint.js"
import {translate} from "../../common/translate.js"
import Common from "./policy-summary/common.js"
import Moderation from "./policy-summary/moderation.js"
import Table from "./policy-summary/table.js"
import Ul from "./policy-summary/ul.js"

const Location = {
	view(vnode) {
		return m("ul",
			vnode.attrs.location.map(function(l) {
				const sublist = (l === 'named_repository' && vnode.attrs.named.length)
				return m("li",
					translate(l, "location"),
					sublist ? m(Ul, {items: vnode.attrs.named}) : null,
				)
			})
		)
	}
}

const OaButtons = {
	view(vnode) {
		if (!vnode.attrs.edit) {
			return null
		}
		const edit = vnode.attrs.edit
		return m("div.edit-buttons", {style: "float: right"},
			m('button.btn.btn-danger',
				{
					type: "button",
					onclick() {
						if (typeof window.confirm !== 'function' || window.confirm("Supprimer cette modalité ?")) {
							edit.delete.call(this, vnode.attrs.index)
						}
					},
				},
				"Supprimer"
			),
			m('button.btn.btn-default',
				{
					type: "button",
					onclick() {
						edit.update.call(this, vnode.attrs.index)
					},
				},
				"Modifier…"
			),
		)
	}
}

function getLabel(text, hintName) {
	return [text, m(Hint, {name: hintName})]
}

const Oa = {
	view(vnode) {
		const oa = vnode.attrs.oa
		const rows = [
			[
				getLabel("Détenteur des droits", 'copyright_owner'),
				translate(oa.copyright_owner, "copyright_owner")
			],
		]
		if (oa.additional_oa_fee === "yes") {
			rows.push([
				getLabel("Frais d'option OA", 'fee'),
				oa.additional_oa_fee === "yes" ? "oui" : "non"
			])
		}
		if (oa.license) {
			rows.push([
				getLabel("Licence", 'license'),
				m(Ul, {
					items: oa.license.map(
						x => translate(x.license, "license") + (x.version === '4.0' ? '' : " (" + x.version + ")")
					).map(
						x => m.trust(x))
				})
			])
		}
		if (oa.embargo) {
			rows.push([
				getLabel("Embargo", 'embargo'),
				oa.embargo.amount + (oa.embargo.units === 'months' ? " mois" : (
					oa.embargo.units === 'years' ? " années" : " jours"))
			])
		}
		if (oa.prerequisites) {
			rows.push([
				getLabel("Prérequis", 'prerequisites'),
				m(Ul, {items: translate(oa.prerequisites.prerequisites, "prerequisites")})
			])
		}
		if (oa.location && oa.location.location.length > 0) {
			rows.push([
				getLabel("Emplacements autorisés", 'location'),
				m(Location, {location: oa.location.location, named: oa.location.named_repository || []})
			])
		}
		if (oa.conditions) {
			rows.push([
				getLabel("Conditions", 'conditions'),
				m(Ul, {items: translate(oa.conditions, "conditions")})
			])
		}
		if (oa.public_notes) {
			rows.push([
				getLabel("Remarques publiques", 'public_notes'),
				m(Ul, {items: translate(oa.public_notes, "public_notes")})
			])
		}
		return m("div.oa",
			m(OaButtons, {edit: vnode.attrs.edit, index: vnode.attrs.index}),
			m("h4",
				"Modalité de diffusion n", m("sup", "o "),
				vnode.attrs.index + 1,
				" : version ",
				// Liste des versions traduites, par ex. "<em>soumise</em> ou <em>publiée</em>"
				translate(oa.article_version, "article_version")
					.map(function(x, index) {
						if (index === 0) {
							return m("em", x);
						} else {
							return [" ou ", m("em", x)];
						}
					})
			),
			m(Table, {rows})
		)
	}
}

export default {
	view(vnode) {
		const policy = vnode.attrs.policy
		const politique = vnode.attrs.politique || null
		const edit = 'edit' in vnode.attrs ? vnode.attrs.edit : null
		const model = 'model' in vnode.attrs ? vnode.attrs.model : null
		const journal = 'journal' in vnode.attrs ? vnode.attrs.journal : ""
		return m("div.politique",
			m(Moderation, {policy, politique, edit, model, journal}),
			m(Common, {policy, edit, model, journal}),
			policy.permitted_oa.map(
				(oa, index) => m(Oa, {oa, index, edit}),
			),
			policy.permitted_oa.length > 0 ? null
				: m("div.alert.alert-error", "Cette politique n'a encore aucune modalité de diffusion."),
		)
	},
}

