import Table from "./table.js"
import Ul from "./ul.js"

const Url = {
	view(vnode) {
		return m("span.url",
			vnode.attrs.description,
			" ",
			m('a', {href: vnode.attrs.url}, vnode.attrs.url),
		)
	}
}

const CommonEdit = {
	view(vnode) {
		if (!vnode.attrs.edit) {
			return null
		}
		return m("div.edit-buttons", {style: "float: right"},
			m('button.btn.btn-default',
				{
					type: "button",
					onclick() {
						vnode.attrs.edit.update.call(this, -1)
					},
				},
				"Modifier…"
			)
		)
	}
}

export default {
	view(vnode) {
		const policy = vnode.attrs.policy
		const rows = []
		if ('internal_moniker' in policy && policy.internal_moniker) {
			rows.push(["Nom", policy.internal_moniker])
		}
		if ('urls' in policy && policy.urls) {
			let row = ["URL", m(Ul, {items: policy.urls.map(x => m(Url, x))})]
			row.class = 'urls'
			rows.push(row)
		}
		if (policy.open_access_prohibited && policy.open_access_prohibited === "yes") {
			rows.push(["Accès ouverts possibles", "non"])
		}
		return m("div.common",
			m(CommonEdit, {edit: vnode.attrs.edit}),
			m('h3', "Identification de la politique"),
			m(Table, {rows})
		)
	}
}
