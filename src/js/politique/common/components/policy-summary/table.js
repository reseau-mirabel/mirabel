export default {
	view(vnode) {
		const rows = vnode.attrs.rows
		return m("table.table",
			m('tbody',
				rows.map(row => m('tr', { class: row.class || null },
					row.map(cell => m('td', cell))
				))
			)
		)
	}
}