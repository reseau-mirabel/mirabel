import Table from "./table.js"

/*
 * {
 *    par_politique: {
 *      id: 1,
 *      nom: "A",
 *    },
 *    par_titres: [
 *      {
 *        id: 1,
 *        nom: "A",
 *        role: "éditeur universitaire", // global
 *        roles: ["co-éditeur"], // dans le contexte des titres
 *      },
 */
let loadPublishers = null

const statusDisplay = {
	pending: 'provisoire',
	draft: 'brouillon',
	topublish: 'à publier',
	published: 'publié',
	updated: 'modifié',
	todelete: 'à supprimer',
	deleted: 'supprimé',
}

const Publisher = {
	view(vnode) {
		const publisher = vnode.attrs.publisher
		return [
			m("a", {href: `/editeur/${publisher.id}`}, publisher.nom),
			" ",
			publisher.role ? m("span.label", publisher.role) : "",
		]
	}
}

const Publishers = {
	view(vnode) {
		const publishers = vnode.attrs.publishers
		return m("ul", publishers.map((e) => m('li', m(Publisher, {publisher: e}))))
	}
}

export default {
	oninit(vnode) {
		const politique = vnode.attrs.politique || null
		if (politique !== null) {
			m.request(`/politique/ajax-publishers/${politique.id}`).then(function(data) {
				loadPublishers = data
			})
		}
	},
	view(vnode) {
		const policy = vnode.attrs.policy
		const politique = vnode.attrs.politique
		const rows = []
		if (politique) {
			if (politique.status) {
				let statusTxt = m('span.badge.badge-info', statusDisplay[politique.status])
				if (politique.statusDate) {
					statusTxt = [statusTxt, ` (depuis ${politique.statusDate})`]
				}
				rows.push(["Statut", statusTxt])
			}
			if (politique.notesAdmin) {
				rows.push([
					"Notes d'admin",
					[
						m('a.pull-right', {href: `/politique/annotate/${politique.id}`}, m('i.icon-pencil')),
						m('div.alert.alert-info', {style: "margin-bottom: 0; white-space: pre;"}, politique.notesAdmin)
					]
				])
			}
		}
		if (typeof vnode.attrs.model !== 'undefined' && vnode.attrs.model !== null) {
			const models = [
				"Revue en accès ouvert immédiat sans frais et sans embargo",
				"Revue en accès ouvert immédiat avec frais de type « auteur-payeur »",
				"Revue hybride : accès sur abonnement et frais optionnels OA aux choix des auteurs",
				"Revue sur abonnement avec barrière mobile : accès libre aux articles après un délai",
				"Revue sur abonnement sans accès libre aux numéros nouveaux ou anciens",
			]
			rows.push(["Modèle de la politique", models[vnode.attrs.model]])
		}
		if (typeof vnode.attrs.journal !== 'undefined' && vnode.attrs.journal !== "") {
			rows.push(["Titre lors de la création", vnode.attrs.journal])
		}
		if (loadPublishers) {
			if (loadPublishers.par_politique !== null) {
				rows.push([
					"Éditeur auteur de la politique",
					m("a", {href: `/editeur/${loadPublishers.par_politique.id}`}, loadPublishers.par_politique.nom)
				])
				if (loadPublishers.par_politique.utilisateurId > 0) {
					rows.push([
						"Utilisateur auteur",
						m("a", {href: `/utilisateur/${loadPublishers.par_politique.utilisateurId}`}, loadPublishers.par_politique.utilisateur)
					])
				}
				rows.push([
					"Dernière modification", loadPublishers.par_politique.lastUpdate
				])
			}
			if (loadPublishers.par_titres.length > 0) {
				rows.push([
					"Éditeurs des titres", m(Publishers, { publishers: loadPublishers.par_titres })
				])
			}
		}
		return m("div.common",
			m('h3', "Modération (données non transmises)"),
			m(Table, {rows})
		)
	}
}