export default {
	view(vnode) {
		return m("ul",
			vnode.attrs.items.map(function(item) {
				return m("li", item)
			})
		)
	}
}
