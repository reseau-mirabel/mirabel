let hints = new Map()

function loadHints(o) {
	hints = new Map(Object.entries(o))
}

function getHint(name) {
	if (!hints.has(name)) {
		return null
	}
	const hint = hints.get(name)
	if (hint instanceof Array) {
		return hint[0]
	}
	return hint
}

export {
	getHint,
	loadHints,
}
