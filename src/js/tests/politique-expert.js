const assert = require('assert');
const Expert = require('../politique/expert/components/expert').default;
const state = require('../politique/expert/state').default;
const {loadHints} = require('../politique/common/hints.js');
const {loadTranslations} = require('../politique/common/translate');

/* global global */
// emulate jQuery popover (Bootstrap CSS, used by the Hint component)
let popoverCalled = 0;
global.$ = function () {
	return {
		popover() {
			popoverCalled++
		}
	}
}

state.policy = {
	internal_moniker: "Default Policy",
	open_access_prohibited: "no",
	permitted_oa: [],
	urls: [
		{
			description: "Journal Policies",
			url: "http://www.springernature.com/gp/open-research/policies/journal-policies"
		}
	],
}

describe('politique-expert', function() {
	it("common block", function() {
		state.editingOa = null
		var out = mq(Expert)

		out.should.not.have('#politique-expert')
		out.should.have('.politique')
		out.should.have('h3:contains(Identification de la politique)')
		out.should.have('.politique .urls')
		out.should.have('.urls :contains(http://www.springernature.com/gp/open-research/policies/journal-policies)')

		out.click(".politique .common .edit-buttons button.btn-default")
		out.should.have("h2:contains(Modification)")
		out.should.have("h2:contains(identification)")
	})

	it("toggle editing mode", function() {
		state.policy.permitted_oa[0] = {
			"article_version": [
				"published",
			],
		}
		state.editingOa = null
		var out = mq(Expert)

		out.should.not.have('#politique-expert')
		out.should.have('.politique')

		out.click(".politique .oa .edit-buttons button.btn-default")
		assert.equal(state.editingOa, 0)
		out.should.have('#politique-expert')
		out.should.not.have('.politique')
	})

	it("delete oa", function() {
		state.policy.permitted_oa[0] = {
			"article_version": [
				"published",
			],
		}
		state.policy.permitted_oa.push({
			"article_version": [
				"submitted",
			],
		})
		state.editingOa = null
		var out = mq(Expert)
		out.should.not.have('.alert-error')
		assert.equal(state.policy.permitted_oa.length, 2)

		out.click(".politique .oa .edit-buttons button.btn-danger")
		out.should.not.have('.alert-error')
		assert.equal(state.policy.permitted_oa.length, 1)
		assert.equal(state.editingOa, null)

		out.click(".politique .oa .edit-buttons button.btn-danger")
		out.should.have('.alert-error')
		out.should.not.have('.politique .oa')
		assert.equal(state.policy.permitted_oa.length, 0)
	})

	it("validates OA consistency", function() {
		state.policy.permitted_oa = [{
			"article_version": [
				"published",
			],
		}]
		state.editingOa = 0
		loadTranslations({article_version: [
			['published', "publiée"],
			['accepted', "accepté"],
			['submitted', "soumise"],
		]})
		const out = mq(Expert)

		out.should.have(1, '.version input:checked')
		out.should.not.have('.form-actions .btn[disabled]')
		out.rootEl.querySelector('.version input[value=published]').checked = false
		out.trigger('.version input[value=published]', 'change')
		out.should.have(1, '.form-actions .btn[disabled]')
		out.should.have(1, '.version .error')
	});

	it("article version", function() {
		state.editingOa = 0
		state.oa = {
			"article_version": [
				"published",
			],
		}
		loadTranslations({article_version: [
			['published', "publiée"],
			['accepted', "accepté"],
			['submitted', "soumise"],
		]})
		var out = mq(Expert)

		out.should.have('.version')
		out.should.have(3, '.version input')
		out.should.have(1, '.version input:checked')
		out.should.have('.version input[value=published]:checked')

		// To emulate a chebox selection, first act on the DOM, then trigger the 'change' event.
		out.rootEl.querySelector('.version input[value=accepted]').checked = true
		out.trigger('.version input[value=accepted]', 'change')
		assert.deepEqual(state.oa.article_version, ["published", "accepted"])
	});

	it("license", function() {
		state.editingOa = 0
		state.oa = {
			"license": [
				{
					"license": "cc_by",
					"version": "4.0"
				}
			],
		}
		loadTranslations({license: {
			cc_by: "CC BY",
			cc_by_sa: "CC BY SA",
			cc_by_nc: "CC BY NC",
		}})
		var out = mq(Expert)

		out.should.have('.license')
		out.should.have(3, '.license input[type="checkbox"]')
		out.should.have(1, '.license input:checked')
		out.should.have('.license input[value=cc_by]:checked')

		out.rootEl.querySelector('.license input[value=cc_by_sa]').checked = true
		out.trigger('.license input[value=cc_by_sa]', 'change')
		out.should.have(2, '.license input:checked')
		assert.deepEqual(
			state.oa.license,
			[
				{
					"license": "cc_by",
					"version": "4.0"
				},
				{
					"license": "cc_by_sa",
					"version": "4.0"
				},
			]
		)

		out.setValue('#license-cc-version', "2.0")
		assert.deepEqual(
			state.oa.license,
			[
				{
					"license": "cc_by",
					"version": "2.0"
				},
				{
					"license": "cc_by_sa",
					"version": "2.0"
				},
			]
		)
	});

	it("conditions", function() {
		state.editingOa = 0
		state.oa = {
			"conditions": [
				"Published source must be acknowledged with citation"
			],
		}
		loadTranslations({
			conditions: {
				"Must link to publisher version": "Un lien vers la version publiée doit apparaitre",
				"Published source must be acknowledged with citation": "La version publiée doit être citée avec la source originale",
			},
			prerequisites: {
				"requires_publisher_permission": "Sous réserve d'accord de l'éditeur",
			}
		})
		var out = mq(Expert)

		out.should.have('.conditions')
		out.should.have(2, '.conditions input')
		out.should.have(1, '.conditions input:checked')
		out.should.have('.conditions input[value=Published source must be acknowledged with citation]:checked')
		assert.equal(out.rootEl.querySelector('.conditions input[value=Published source must be acknowledged with citation]').checked, true)

		out.rootEl.querySelector('.conditions input[value=Must link to publisher version]').checked = true
		out.rootEl.querySelector('.conditions input[value=Published source must be acknowledged with citation]').checked = false
		out.trigger('.conditions input', 'change')
		out.should.have(1, '.conditions input:checked')
		assert.deepEqual(
			state.oa.conditions,
			[
				"Must link to publisher version",
			]
		)

		out.rootEl.querySelector('.prerequisites input[value=requires_publisher_permission]').checked = true
		out.trigger('.prerequisites input', 'change')
		assert.deepEqual(
			state.oa.prerequisites.prerequisites,
			[
				"requires_publisher_permission",
			]
		)
	});

	it("copyright-owner", function() {
		state.editingOa = 0
		state.oa = {
			copyright_owner: "authors",
		}
		loadTranslations({copyright_owner: [
			["authors", "Auteurs", "Aide des auteurs"],
			["journal", "Revue", "Aide des revues"],
			["publishers", "Éditeurs"],
		]})
		var out = mq(Expert)

		out.should.have('.copyright-owner')
		out.should.have(3, '.copyright-owner option')
		out.should.have(1, '.copyright-owner option[selected]')
		out.should.have('.copyright-owner option[selected][value=authors]')
		out.should.have('.copyright-owner option[title]')

		out.setValue('.copyright-owner select', 'journal')
		assert.equal(
			state.oa.copyright_owner,
			"journal"
		)
	});

	it("fee", function() {
		state.editingOa = 0
		state.oa = {
			additional_oa_fee: "yes",
		}
		var out = mq(Expert)

		out.should.have('.fee')
		out.should.have(2, '.fee option')
		out.should.have(1, '.fee option[selected]')
		out.should.have('.fee option[selected][value=yes]')

		out.setValue('.fee select', 'no')
		assert.equal(
			state.oa.additional_oa_fee,
			"no"
		)
	});

	it("location", function() {
		state.editingOa = 0
		state.oa = {
			location: {
				location: [
					"any_website",
					"this_journal",
					"named_repository",
				],
				named_repository: [
					"HALtofeu",
				],
			}
		}
		loadTranslations({
			location: {
				"academic_social_network": "Réseau social académique",
				"any_website": "Tout site web",
				"named_repository": "Archive spécifique",
				"preprint_repository": "Archive d'articles prépubliés",
				"subject_repository": "Archive thématique",
				"this_journal": "Accès ouvert sur le site de l'éditeur",
			},
			"location.named_repository": [
				["arXiv"],
				["PubMed Central"],
				["XXX"],
				["YYY"],
				["ZZZ"],
			]
		})
		var out = mq(Expert)

		out.should.have('.location')
		out.should.have(5, '.location-location input')
		out.should.have(2, '.location-location input:checked')
		out.should.have('.location-location input[value=this_journal]:checked')
		out.should.have(2, '.location-namedrepository input[type=checkbox]')
		out.should.have(1, '.location-namedrepository input[type=checkbox]:checked')
		out.should.have(5, '.location-namedrepository option')

		out.rootEl.querySelector('.location-location input[value=preprint_repository]').checked = true
		out.rootEl.querySelector('.location-location input[value=subject_repository]').checked = true
		out.trigger('.location-location input', 'change')
		out.rootEl.querySelector('.location-namedrepository input[type=checkbox]:checked').checked = false
		out.trigger('.location-namedrepository input', 'change')
		out.should.have(4, '.location-location input:checked')
		out.should.have(0, '.location-namedrepository input[type=checkbox]:checked')
		assert.deepEqual(
			state.oa.location,
			{
				location: [
					"any_website",
					"preprint_repository",
					"subject_repository",
					"this_journal",
				],
			}
		)

		out.rootEl.querySelector('.location-namedrepository input[type=checkbox]').checked = true // HAL
		out.trigger('.location-namedrepository input', 'change')
		out.should.have(4, '.location-location input:checked')
		out.should.have(1, '.location-namedrepository input[type=checkbox]:checked')
		assert.deepEqual(
			state.oa.location,
			{
				location: [
					"any_website",
					"named_repository",
					"preprint_repository",
					"subject_repository",
					"this_journal",
				],
				named_repository: [
					"HAL",
				],
			}
		)

		// add a custom "named repository"
		out.setValue('.location #namedrepository-free', "Chez moi")
		out.click('.location #namedrepository-add')
		out.should.have(4, '.location-location input:checked')
		out.should.have(2, '.location-namedrepository input[type=checkbox]')
		out.should.have(2, '.location-namedrepository input[type=checkbox]:checked')
		assert.deepEqual(
			state.oa.location,
			{
				location: [
					"any_website",
					"named_repository",
					"preprint_repository",
					"subject_repository",
					"this_journal",
				],
				named_repository: [
					"HAL",
					"Chez moi",
				],
			}
		)

		out.rootEl.querySelector('.location-namedrepository input[value=HAL]').checked = false
		out.trigger('.location-namedrepository input', 'change')
		out.should.have(2, '.location-namedrepository input[type=checkbox]')
		out.should.have(1, '.location-namedrepository input[type=checkbox]:checked')
		assert.deepEqual(
			state.oa.location.named_repository,
			[
				"Chez moi",
			],
		)
	})

	it("embargo (création)", function() {
		state.editingOa = 0
		state.oa = {}
		var out = mq(Expert)

		out.should.have('.embargo')
		assert.equal(out.rootEl.querySelector('.embargo input').value, "0")
		assert.equal(out.rootEl.querySelector('.embargo option[selected]').value, "months")

		out.setValue('.embargo input', 3)
		assert.deepEqual(
			state.oa.embargo,
			{
				amount: 3,
				units: "months",
			}
		)
	});

	it("embargo (modification)", function() {
		state.editingOa = 0
		state.oa = {
			embargo: {
				amount: 36,
				units: "months",
			}
		}
		var out = mq(Expert)

		out.should.have('.embargo')
		out.should.have(1, '.embargo input')
		out.should.have(1, '.embargo select')
		assert.equal(out.rootEl.querySelector('.embargo input').value, "36")
		assert.equal(out.rootEl.querySelector('.embargo option[selected]').value, "months")

		out.setValue('.embargo input', 32)
		assert.deepEqual(
			state.oa.embargo,
			{
				amount: 32,
				units: "months",
			}
		)

		out.setValue('.embargo input', 3)
		out.setValue('.embargo select', 'years')
		assert.equal(out.rootEl.querySelector('.embargo option[selected]').value, "years")
		assert.deepEqual(
			state.oa.embargo,
			{
				amount: 3,
				units: "years",
			}
		)
	});

	it("prérequis", function() {
		state.editingOa = 0
		state.oa = {
			prerequisites: {
				prerequisites: [
				   "when_required_by_law",
				],
			},
		}
		loadTranslations({prerequisites: {
			"requires_publisher_permission": "Sous réserve d'accord de l'éditeur",
			"when_required_by_funder": "Si demandé par le financeur",
			"when_required_by_institution": "Si demandé par l'institution",
			"when_required_by_law": "Si imposé par la loi",
		}})
		var out = mq(Expert)

		out.should.have('.prerequisites')
		out.should.have(4, '.prerequisites input')
		out.should.have(1, '.prerequisites input:checked')
		out.should.have('.prerequisites input[value=when_required_by_law]:checked')
		assert.equal(out.rootEl.querySelector('.prerequisites input[value=when_required_by_law]').checked, true)

		out.rootEl.querySelector('.prerequisites input[value=requires_publisher_permission]').checked = true
		out.rootEl.querySelector('.prerequisites input[value=when_required_by_institution]').checked = true
		out.rootEl.querySelector('.prerequisites input[value=when_required_by_law]').checked = false
		out.trigger('.prerequisites input', 'change')
		out.should.have(2, '.prerequisites input:checked')
		assert.deepEqual(
			state.oa.prerequisites,
			{
				prerequisites: [
				   "requires_publisher_permission",
					"when_required_by_institution",
				],
			}
		)
	});
});
