const assert = require('assert')
const Assistant = require('../politique/assistant/components/assistant').default
const state = require('../politique/assistant/state').default
const expectedPolicies = require("./expected-policies-embargo.json")

describe('politique-assistant, scénarios "embargo"', function() {
	it('droits exclusifs + autorisation VOR même embargo', function() {
		state.reset();
		var output = mq(Assistant);
		output.click('button.choice-3'); // 0..4

		output.should.contain("Embargo");
		output.should.have(1, 'button.btn-primary');
		output.setValue('input', "18");
		output.click('button');

		output.should.contain("Examiner les droits")
		output.should.have('div.level-0')
		output.click('.level-0 button.yesno-yes')
		output.click('.level-1 button.yesno-yes')

		output.should.contain("Autorisations de dépôt");
		output.click('.ao-published button.choice-1'); // 0..2, 1 => après embargo
		output.click('.ao-accepted button.choice-2'); // 0..2, 2 => jamais

		assert.deepEqual(
			state.policy.get(),
			expectedPolicies["droits exclusifs + autorisation VOR même embargo"]
		);
	});

	it('droits exclusifs + autorisation VOR sans embargo', function() {
		state.reset();
		var output = mq(Assistant);
		output.click('button.choice-3'); // 0..4

		output.should.contain("Embargo");
		output.should.have(1, 'button.btn-primary');
		output.setValue('input', "18");
		output.click('button');

		output.should.contain("Examiner les droits")
		output.should.have('div.level-0')
		output.click('.level-0 button.yesno-yes')
		output.click('.level-1 button.yesno-yes')

		output.should.contain("Autorisations de dépôt")
		output.click('.ao-published button.choice-0'); // 0..2, 0 => sans embargo
		output.click('.ao-accepted button.choice-2'); // 0..2, 2 => jamais

		const oas = state.policy.get().permitted_oa
		assert.equal(oas.length, 2)
		assert.deepEqual(
			state.policy.get(),
			expectedPolicies["droits exclusifs + autorisation VOR sans embargo"]
		);
	});

	it('droits exclusifs + non-autorisation VOR', function() {
		state.reset();
		var output = mq(Assistant);
		output.click('button.choice-3'); // 0..4

		output.should.contain("Embargo");
		output.should.have(1, 'button.btn-primary');
		output.setValue('input', "36");
		output.click('button');

		output.should.contain("Examiner les droits")
		output.click('.level-0 button.yesno-no')

		output.should.contain("Autorisations de dépôt")
		output.click('.ao-published button.choice-2'); // 0..2, 2 => jamais
		output.click('.ao-accepted button.choice-2'); // 0..2, 2 => jamais

		assert.deepEqual(
			state.policy.get(),
			expectedPolicies["droits exclusifs + non-autorisation VOR"]
		);
	});

	it('Cas 1 : droits exclusifs + autorisation VOR + autorisation MAA avec embargo identique', function() {
		state.reset();
		var output = mq(Assistant);
		output.click('button.choice-3'); // 0..4

		output.should.contain("Embargo");
		output.should.have(1, 'button.btn-primary');
		output.setValue('input', "18");
		output.click('button');

		output.should.contain("Examiner les droits")
		output.click('.level-0 button.yesno-yes')
		output.click('.level-1 button.yesno-yes')

		output.should.contain("Autorisations de dépôt")
		output.click('.ao-published button.choice-1') // 0..2, 1 => après embargo
		output.click('.ao-accepted button.choice-1'); // 0..2, 1 => après embargo

		output.should.contain("embargo réduit LRN");
		output.click('button.yesno-no');

		output.should.contain("URL");
		assert.equal(state.policy.get().permitted_oa.length, 2)
		assert.deepEqual(
			state.policy.get(),
			expectedPolicies["Cas 1 : droits exclusifs + autorisation VOR + autorisation MAA avec embargo identique"]
		);
	});

	it('Cas 2 : droits exclusifs + autorisation VOR + autorisation MAA avec embargo identique et embargo réduit LRN', function() {
		state.reset();
		var output = mq(Assistant);
		output.click('button.choice-3'); // 0..4

		output.should.contain("Embargo");
		output.should.have(1, 'button.btn-primary');
		output.setValue('input', "36");
		output.click('button');

		output.should.contain("Examiner les droits")
		output.click('.level-0 button.yesno-yes')
		output.click('.level-1 button.yesno-yes')

		output.should.contain("Autorisations de dépôt")
		output.click('.ao-published button.choice-1') // 0..2, 1 => après embargo
		output.click('.ao-accepted button.choice-1'); // 0..2, 1 => après embargo

		output.should.contain("embargo réduit LRN");
		output.click('button.yesno-yes');

		output.should.contain("Embargo du manuscript accepté");
		output.setValue('input[type="number"]', "12");
		output.click('button');

		output.should.contain("URL");
		assert.equal(state.policy.get().permitted_oa.length, 3)
		assert.deepEqual(
			state.policy.get(),
			expectedPolicies["Cas 2 : droits exclusifs + autorisation VOR + autorisation MAA avec embargo identique et embargo réduit LRN"]
		);
	});

	it('Cas 3 : droits exclusifs + autorisation VOR + autorisation MAA sans embargo', function() {
		state.reset();
		var output = mq(Assistant);
		output.click('button.choice-3'); // 0..4

		output.should.contain("Embargo");
		output.should.have(1, 'button.btn-primary');
		output.setValue('input', "18");
		output.click('button');

		output.should.contain("Examiner les droits")
		output.click('.level-0 button.yesno-yes')
		output.click('.level-1 button.yesno-yes')

		output.should.contain("Autorisations de dépôt")
		output.click('.ao-published button.choice-1') // 0..2, 1 => après embargo
		output.click('.ao-accepted button.choice-0'); // 0..2, 0 => sans embargo

		output.should.contain("URL");
		assert.equal(state.policy.get().permitted_oa.length, 2)
		assert.deepEqual(
			state.policy.get(),
			expectedPolicies["Cas 3 : droits exclusifs + autorisation VOR + autorisation MAA sans embargo"]
		);
	});

	it('Cas 4 : droits exclusifs + non-autorisation VOR + autorisation MAA avec embargo identique', function() {
		state.reset();
		var output = mq(Assistant);
		output.click('button.choice-3'); // 0..4

		output.should.contain("Embargo");
		output.should.have(1, 'button.btn-primary');
		output.setValue('input', "36");
		output.click('button');

		output.should.contain("Examiner les droits")
		output.click('.level-0 button.yesno-yes')
		output.click('.level-1 button.yesno-yes')

		output.should.contain("Autorisations de dépôt")
		output.click('.ao-published button.choice-2') // 0..2, 2 => jamais
		output.click('.ao-accepted button.choice-1'); // 0..2, 1 => après embargo

		output.should.contain("embargo réduit LRN");
		output.click('button.yesno-no');

		output.should.contain("URL");
		assert.equal(state.policy.get().permitted_oa.length, 2)
		assert.deepEqual(
			state.policy.get().permitted_oa[1],
			expectedPolicies["Cas 4 : droits exclusifs + non-autorisation VOR + autorisation MAA avec embargo identique"].permitted_oa[1]
		);
	});

	it('Cas 5 : droits exclusifs + Non autorisation VOR + autorisation MAA avec embargo identique et embargo réduit LRN', function() {
		state.reset();
		var output = mq(Assistant);
		output.click('button.choice-3'); // 0..4

		output.should.contain("Embargo");
		output.should.have(1, 'button.btn-primary');
		output.setValue('input', "36");
		output.click('button');

		output.should.contain("Examiner les droits")
		output.click('.level-0 button.yesno-yes')
		output.click('.level-1 button.yesno-yes')

		output.should.contain("Autorisations de dépôt")
		output.click('.ao-published button.choice-2') // 0..2, 2 => jamais
		output.click('.ao-accepted button.choice-1'); // 0..2, 1 => après embargo

		output.should.contain("embargo réduit LRN");
		output.click('button.yesno-yes');

		output.should.contain("Embargo du manuscript accepté");
		output.setValue('input[type="number"]', "12");
		output.click('button');

		output.should.contain("URL");
		assert.equal(state.policy.get().permitted_oa.length, 3)
		assert.deepEqual(
			state.policy.get(),
			expectedPolicies["Cas 5 : droits exclusifs + Non autorisation VOR + autorisation MAA avec embargo identique et embargo réduit LRN"]
        )
	});

	it('Cas 6 : droits exclusifs + Non autorisation VOR + autorisation MAA sans embargo', function() {
		state.reset();
		var output = mq(Assistant);
		output.click('button.choice-3'); // 0..4

		output.should.contain("Embargo");
		output.should.have(1, 'button.btn-primary');
		output.setValue('input', "36");
		output.click('button');

		output.should.contain("Examiner les droits")
		output.click('.level-0 button.yesno-yes')
		output.click('.level-1 button.yesno-yes')

		output.should.contain("Autorisations de dépôt")
		output.click('.ao-published button.choice-2') // 0..2, 2 => jamais
		output.click('.ao-accepted button.choice-0'); // 0..2, 0 => sans embargo

		output.should.contain("URL");
		assert.equal(state.policy.get().permitted_oa.length, 2)
		assert.deepEqual(
			state.policy.get().permitted_oa[1],
			expectedPolicies["Cas 6 : droits exclusifs + Non autorisation VOR + autorisation MAA sans embargo"].permitted_oa[1]
		);
	});

	it('Cas 7 : droits exclusifs + Non autorisation VOR + Non autorisation MAA', function() {
		state.reset();
		var output = mq(Assistant);
		output.click('button.choice-3'); // 0..4

		output.should.contain("Embargo");
		output.should.have(1, 'button.btn-primary');
		output.setValue('input', "36");
		output.click('button');

		output.should.contain("Examiner les droits")
		output.click('.level-0 button.yesno-yes')
		output.click('.level-1 button.yesno-yes')

		output.should.contain("Autorisations de dépôt")
		output.click('.ao-published button.choice-2') // 0..2, 2 => jamais
		output.click('.ao-accepted button.choice-2'); // 0..2, 2 => jamais

		output.should.contain("URL");
		assert.deepEqual(
			state.policy.get(),
			expectedPolicies["Cas 7 : droits exclusifs + Non autorisation VOR + Non autorisation MAA"]
		)
	});
});
