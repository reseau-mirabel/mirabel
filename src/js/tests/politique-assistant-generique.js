const assert = require('assert')
const Assistant = require('../politique/assistant/components/assistant').default
const state = require('../politique/assistant/state').default

describe('politique-assistant, affichage', function() {
	it('display the history', function() {
		state.reset();
		var output = mq(Assistant);

		output.should.not.have('#history .past');

		output.click('button.choice-0'); // 0..4
		output.should.have(1, '#history .past');
	});

	it('go back in the history', function() {
		state.reset();
		var output = mq(Assistant);

		output.should.not.have('#history .past');

		output.click('button.choice-0'); // 0..4
		output.should.have(1, '#history .past');

		output.click('button.yesno-no');
		output.should.have(2, '#history .past');

		output.click('button.yesno-no');
		output.should.have(3, '#history .past');

		// go back in history
		output.click('#history .savepoint.index-0 a');
		output.should.not.have('#history .past');
		output.should.have(3, '#history .future');
	});

	it("reset after going back then filling in", function() {
		state.reset();
		let output = mq(Assistant);
		// Modèle ? revue en accès ouvert immédiat sans frais et sans embargo
		output.click('button.choice-0'); // 0..4

		// Droits exclusifs ? non
		output.should.contain("Examiner les droits");
		output.click('button.yesno-no');

		// Licence CC ? oui
		output.should.contain("licence CC");
		output.click('button.yesno-yes');
		assert.equal(state.policy.get().permitted_oa.length, 0)

		// Quelle licence ? CC-BY
		output.should.have(6, '.button-list button');
		output.click('button.choice-1'); // 0..5
		assert.equal(state.policy.get().permitted_oa.length, 2)

		// go back in history
		output.click('#history .savepoint.index-3 a');
		output.should.contain("licence CC");
		assert.equal(state.policy.get().permitted_oa.length, 0)

		// advance again
		output.should.have(6, '.button-list button');
		output.click('button.choice-0'); // 0..5

		// Résultat final
		output.should.have('section.urls')
		assert.equal(state.policy.get().permitted_oa.length, 2)
		assert.deepEqual(
			state.policy.get().permitted_oa[0],
			{
				"additional_oa_fee": "no",
				"copyright_owner": "authors",
				"article_version": [
					"published",
				],
				"license": [
					{
						"license": "cc_by",
						"version": "4.0"
					}
				],
				"location": {
					location: [
						"this_journal",
						"any_website",
					],
				},
				"conditions": [
					"Published source must be acknowledged with citation"
				]
			}
		);
	});
});
