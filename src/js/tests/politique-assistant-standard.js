const assert = require('assert')
const Assistant = require('../politique/assistant/components/assistant').default
const state = require('../politique/assistant/state').default
const expectedPolicies = require("./expected-policies-standard.json")

describe('politique-assistant, scénarios "revue en accès ouvert immédiat sans frais et sans embargo"', function() {
	it("Cas 1, droits exclusifs + licence CC-BY", function() {
		state.reset();
		let output = mq(Assistant);
		// Modèle ? revue en accès ouvert immédiat sans frais et sans embargo
		output.click('button.choice-0'); // 0..4

		// Droits exclusifs ? oui + oui
		output.should.contain("Examiner les droits")
		output.should.have('.yesno-yes')
		output.click('.level-0 button.yesno-yes')
		output.click('.level-1 button.yesno-yes')

		// Licence CC ? oui
		output.should.contain("licence CC");
		output.should.have('button.yesno-yes');
		output.should.not.have('.button-list');
		output.click('button.yesno-yes');

		// Quelle licence ? CC-BY
		output.should.have(6, '.button-list button');
		output.click('button.choice-0'); // 0..5

		// URLs ?
		output.should.have(2, 'input')
		output.setValue('.url input', "https://example.com")
		output.setValue('.description input', "exemple")
		output.click('button.btn-primary')

		// Résultat final
		output.should.have('.assistant-complete');
		assert.deepEqual(
			state.policy.get(),
			expectedPolicies["Cas 1, droits exclusifs + licence CC-BY"]
		);
	});

	it("Cas 2, droits exclusifs + licence CC-BY-NC-ND", function() {
		state.reset();
		let output = mq(Assistant);
		// Modèle ? revue en accès ouvert immédiat sans frais et sans embargo
		output.click('button.choice-0'); // 0..4

		// Droits exclusifs ? oui
		output.should.contain("Examiner les droits")
		output.click('.level-0 button.yesno-yes')
		output.click('.level-1 button.yesno-yes')

		// Licence CC ? oui
		output.should.contain("licence CC");
		output.click('button.yesno-yes');

		// Quelle licence ? CC-BY-NC-ND
		output.click('button.choice-5'); // 0..5

		// Résultat final
		output.should.have('section.urls');
		assert.deepEqual(
			state.policy.get(),
			expectedPolicies["Cas 2, droits exclusifs + licence CC-BY-NC-ND"]
		);
	});

	it("Cas 3, droits exclusifs + autorisation VOR + autorisation MAA", function() {
		state.reset();
		let output = mq(Assistant);
		// Modèle ? revue en accès ouvert immédiat sans frais et sans embargo
		output.click('button.choice-0'); // 0..4

		// Droits exclusifs ? oui
		output.should.contain("Examiner les droits")
		output.click('.level-0 button.yesno-yes')
		output.click('.level-1 button.yesno-yes')

		// Licence CC ? non
		output.should.contain("licence CC");
		output.click('button.yesno-no');

		output.should.contain("Autorisations de dépôt");
		// Dépôt AO pour version publiée ? oui
		output.click('.depot-published button.yesno-yes');
		// Dépôt AO pour version acceptée ? oui
		output.click('.depot-accepted button.yesno-yes');

		// Résultat final
		output.should.have('section.urls');
		assert.deepEqual(
			state.policy.get(),
			expectedPolicies["Cas 3, droits exclusifs + autorisation VOR + autorisation MAA"]
		);
	});

	it("Cas 4, droits exclusifs + autorisation VOR + non-autorisation MAA", function() {
		state.reset();
		let output = mq(Assistant);
		// Modèle ? revue en accès ouvert immédiat sans frais et sans embargo
		output.click('button.choice-0'); // 0..4

		// Droits exclusifs ? oui
		output.should.contain("Examiner les droits")
		output.click('.level-0 button.yesno-yes')
		output.click('.level-1 button.yesno-yes')

		// Licence CC ? non
		output.should.contain("licence CC");
		output.click('button.yesno-no');

		output.should.contain("Autorisations de dépôt");
		// Dépôt AO pour version publiée ? oui
		output.click('.depot-published button.yesno-yes');
		// Dépôt AO pour version acceptée ? non
		output.click('.depot-accepted button.yesno-no');

		// Résultat final
		output.should.have('section.urls');
		assert.deepEqual(
			state.policy.get(),
			expectedPolicies["Cas 4, droits exclusifs + autorisation VOR + non-autorisation MAA"]
		);
	});

	it("Cas 5, droits exclusifs + non-autorisation VOR + autorisation MAA", function() {
		state.reset();
		let output = mq(Assistant);
		// Modèle ? revue en accès ouvert immédiat sans frais et sans embargo
		output.click('button.choice-0'); // 0..4

		// Droits exclusifs ? oui
		output.should.contain("Examiner les droits")
		output.click('.level-0 button.yesno-yes')
		output.click('.level-1 button.yesno-yes')

		// Licence CC ? non
		output.should.contain("licence CC");
		output.click('button.yesno-no');

		output.should.contain("Autorisations de dépôt");
		// Dépôt AO pour version publiée ? non
		output.click('.depot-published button.yesno-no');
		// Dépôt AO pour version acceptée ? oui
		output.click('.depot-accepted button.yesno-yes');

		// Résultat final
		output.should.have('section.urls');
		assert.deepEqual(
			state.policy.get(),
			expectedPolicies["Cas 5, droits exclusifs + non-autorisation VOR + autorisation MAA"]
		);
	});

	it("Cas 6, droits exclusifs + non-autorisation VOR + non-autorisation MAA", function() {
		state.reset();
		let output = mq(Assistant);
		// Modèle ? revue en accès ouvert immédiat sans frais et sans embargo
		output.click('button.choice-0'); // 0..4

		// Droits exclusifs ? oui
		output.should.contain("Examiner les droits")
		output.click('.level-0 button.yesno-yes')
		output.click('.level-1 button.yesno-yes')

		// Licence CC ? non
		output.should.contain("licence CC");
		output.click('button.yesno-no');

		output.should.contain("Autorisations de dépôt");
		// Dépôt AO pour version publiée ? non
		output.click('.depot-published button.yesno-no');
		// Dépôt AO pour version acceptée ? oui
		output.click('.depot-accepted button.yesno-no');

		// Résultat final
		output.should.have('section.urls');
		assert.deepEqual(
			state.policy.get(),
			expectedPolicies["Cas 6, droits exclusifs + non-autorisation VOR + non-autorisation MAA"]
		);
	});

	it("Cas 7, droits non-exclusifs + licence CC-BY", function() {
		state.reset();
		let output = mq(Assistant);
		// Modèle ? revue en accès ouvert immédiat sans frais et sans embargo
		output.click('button.choice-0'); // 0..4

		// Droits exclusifs ? non
		output.should.contain("Examiner les droits")
		output.click('.level-0 button.yesno-no')

		// Licence CC ? oui
		output.should.contain("licence CC");
		output.click('button.yesno-yes');

		// Quelle licence ? CC-BY
		output.should.have(6, '.button-list button');
		output.click('button.choice-0'); // 0..5

		// Résultat final
		output.should.have('section.urls');
		assert.deepEqual(
			state.policy.get(),
			expectedPolicies["Cas 7, droits non-exclusifs + licence CC-BY"]
		);
	});

	it("Cas 8, droits non-exclusifs + licence CC-BY-NC-CD", function() {
		state.reset();
		let output = mq(Assistant);
		// Modèle ? revue en accès ouvert immédiat sans frais et sans embargo
		output.click('button.choice-0'); // 0..4

		// Droits exclusifs ? non
		output.should.contain("Examiner les droits")
		output.click('.level-0 button.yesno-yes')
		output.click('.level-1 button.yesno-no')

		// Licence CC ? oui
		output.should.contain("licence CC");
		output.click('button.yesno-yes');

		// Quelle licence ? CC-BY-NC-ND
		output.should.have(6, '.button-list button');
		output.click('button.choice-5'); // 0..5

		// Résultat final
		output.should.have('section.urls');
		assert.deepEqual(
			state.policy.get(),
			expectedPolicies["Cas 8, droits non-exclusifs + licence CC-BY-NC-CD"]
		);
	});

	it("Cas 9, droits non-exclusifs + autorisation dépôt version publiée", function() {
		state.reset();
		let output = mq(Assistant);
		// Modèle ? revue en accès ouvert immédiat sans frais et sans embargo
		output.click('button.choice-0'); // 0..4

		// Droits exclusifs ? non
		output.should.contain("Examiner les droits")
		output.click('.level-0 button.yesno-no')

		// Licence CC ? non
		output.should.contain("licence CC");
		output.click('button.yesno-no');

		// Dépôt AO pour version publiée ? oui
		output.should.contain("Autorisations de dépôt");
		output.click('button.yesno-yes');

		// Résultat final
		output.should.have('section.urls');
		assert.deepEqual(
			state.policy.get(),
			expectedPolicies["Cas 9, droits non-exclusifs + autorisation dépôt version publiée"]
		);
	});

	it("Cas 10, droits non-exclusifs + non-autorisation dépôt version publiée", function() {
		state.reset();
		let output = mq(Assistant);
		// Modèle ? revue en accès ouvert immédiat sans frais et sans embargo
		output.click('button.choice-0'); // 0..4

		// Droits exclusifs ? non
		output.should.contain("Examiner les droits")
		output.click('.level-0 button.yesno-no')

		// Licence CC ? non
		output.should.contain("licence CC");
		output.click('button.yesno-no');

		// Dépôt AO pour version publiée ? non
		output.should.contain("Autorisations de dépôt");
		output.click('button.yesno-no');

		// Résultat final
		output.should.have('section.urls');
		assert.deepEqual(
			state.policy.get(),
			expectedPolicies["Cas 10, droits non-exclusifs + non-autorisation dépôt version publiée"]
		);
	});
});
