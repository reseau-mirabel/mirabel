const assert = require('assert')
const Validation = require('../politique/validation/components/main').default
const state = require('../politique/validation/state').default
const actions = require('../politique/validation/actions').default

/* global global */

/*
 * XHR POST calls to /admin/politique-validation will always return HTTP 200
 * with JSON { success: true }.
 * See src/js/tests/mocha-hooks.js for details.
 */

function initState() {
	state.owners = new Map()
	actions.init(
		[
			{
				id: 14,
				nomComplet: "Jacques Martin",
				email: "jm@alix.com",
				hdateCreation: 1611476760, // 2021-01-24 09:26
				derConnexion: 1620196943, // 2021-05-05 08:42
			},
			{
				id: 32,
				nomComplet: "Jean Giraud",
				email: "jg@blueberry.com",
				hdateCreation: 1611476700, // 2021-01-24 09:25
				derConnexion: 1620196883, // 2021-05-05 08:41
			},
		],
		[
			{
				utilisateurId: 14,
				politiques: 2,
				editeur: {
					id: 10,
					nom: "Dargaud",
				},
			},
			{
				utilisateurId: 14,
				politiques: 1,
				editeur: {
					id: 11,
					nom: "Mosquito",
				},
			},
			{
				utilisateurId: 32,
				politiques: 4,
				editeur: {
					id: 113,
					nom: "Steinkis",
				},
			},
		]
	)
}

describe('politique-validation', function() {
	it("see owner role is disabled", function() {
		initState()
		state.owners = new Map([
			[10, [{id: 14, nomComplet: "Proprio 1"}]],
			[113, [{id: 15, nomComplet: "Proprio 2"}]],
		])
		var out = mq(Validation)

		out.should.have(3, 'tr button.owner')
		out.should.have(2, 'tr button.owner.disabled')
	})

	it("accept first role", function(done) {
		initState()

		var out = mq(Validation)

		out.should.have('tr.role-14-10 td.user-name:contains(Jacques Martin)')
		out.should.not.have('tr.role-14-11 td.user-name:contains(Jacques Martin)')

		out.should.have(3, 'tr.pending-role')
		out.click('tr.role-14-10 td.actions button.owner')

		// trick for testing an async event
		global.waitingFor.then(function() {
			out.redraw()
			assert.equal(state.roles[0].status, "accepté (responsable)")
			out.should.have('td.status')
			out.should.not.have('tr.role-14-10 td.actions')
			done()
			global.waitingFor = null
		})
	})
	it("accept second role", function(done) {
		initState()
		var out = mq(Validation)

		assert.equal(typeof state.roles[1].status, "undefined")
		out.click('tr.role-14-11 td.actions button.guest')
		global.waitingFor.then(function() {
			out.redraw()
			assert.equal(state.roles[1].status, "accepté (délégué)")
			out.should.have('td.status')
			out.should.have('tr.role-14-10 td.actions')
			out.should.not.have('tr.role-14-11 td.actions')
			done()
			global.waitingFor = null
		})
	})

	it("accept both roles", function(done) {
		initState()
		var out = mq(Validation)

		out.should.have(3, 'tr button.owner')
		out.should.not.have('tr button.owner.disabled')

		assert.equal(typeof state.roles[0].status, "undefined")
		assert.equal(typeof state.roles[1].status, "undefined")
		out.should.have(3, 'tr td.actions')

		out.click('tr.role-14-10 td.actions button.owner')
		global.waitingFor.then(function() {
			out.click('tr.role-14-11 td.actions button.guest')
			global.waitingFor.then(function() {
				out.redraw()
				assert.equal(state.roles[0].status, "accepté (responsable)")
				assert.equal(state.roles[1].status, "accepté (délégué)")
				out.should.have(2, 'td.status')
				out.should.have(1, 'tr td.actions')
				done()
				global.waitingFor = null
			})
		})
	})

	it("reject second role", function(done) {
		initState()
		var out = mq(Validation)

		assert.equal(typeof state.roles[1].status, "undefined")
		out.click('tr.role-14-11 td.actions button.reject')
		global.waitingFor.then(function() {
			out.redraw()
			assert.equal(state.roles[1].status, "supprimé")
			out.should.have('td.status')
			out.should.have('tr.role-14-10 td.actions')
			out.should.not.have('tr.role-14-11 td.actions')
			done()
			global.waitingFor = null
		})
	})
})
