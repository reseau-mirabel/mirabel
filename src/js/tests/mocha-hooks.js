/* global global */

exports.mochaHooks = {
	beforeEach(done) {
		// Prepare the test environment that emulates a browser.
		const jsdom = require("jsdom")
		const dom = new jsdom.JSDOM("", {
			// So we can get `requestAnimationFrame`
			pretendToBeVisual: true,
		})

		// Fill in the globals Mithril.js needs to operate.
		global.window = dom.window
		global.window.alert = function() { return true; }
		global.window.confirm = function() { return true; }
		global.document = dom.window.document
		global.requestAnimationFrame = dom.window.requestAnimationFrame

		const xhrmock = require("xhr-mock").default
		xhrmock.setup()
		xhrmock.post(
			'/admin/politique-validation',
			(req, res) => {
				return res.status(200).body(JSON.stringify({success: true}))
			}
		)
		global.xhrmock = xhrmock

		// cannot `import` since we have to wait for the global `window`
		global.m = require('mithril')
		global.mq = require('mithril-query')

		done();
	},
	afterEach(done) {
		global.xhrmock.teardown()
		global.window.close()

		done();
	}
};
